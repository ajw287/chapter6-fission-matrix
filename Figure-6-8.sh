#!/usr/bin/env bash
cd ./code/figure-6-8
time python3 pygmo_micro_array_sep_ax.py figure-6-8 ../../data/6-9/200721-fmtx-burnup-ndf3/
cp figure-6-8-graph* ../../
echo "Output files generated in execution directory as SVG and PNG: 'figure-6-8-graph'"
