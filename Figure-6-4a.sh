#!/usr/bin/env bash
cd code/figure-6-4a
echo ""
echo "***************************************************"
echo "  WARNING: This code could take a number of days!  "
echo "***************************************************"
echo ""
echo "PLease ensure that you have set up ssh keys to a "
echo "suitable machine called 'lise' in ~/.ssh/sshconfig."
echo "The 'lise' machine will require a directory at:"
echo " ~/pygmo/ex2_fmtx_dso/"
echo " and a working Serpent implementation that can be run as: "
echo "sss2"
echo ""
read -n 1 -s -r -p "       Press any key to continue                                                             "
echo ""
echo ""
time python3 pygmo_micro.py ./figure-3453422
cp figure-3453422-graph.svg ../../figure-6-4a.svg
cd ../..
