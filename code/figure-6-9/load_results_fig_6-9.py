#!/usr/bin/env python3
#
#                                                 Copyright 2019 Andrew Whyte
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#  FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  e.g. python3 load_results_fig_6-9.py graph_base_name
# n.b. graph_base_name is not used
import sys, os
import pickle
import matplotlib.pyplot as plt
import numpy as np
import pareto
from decimal import Decimal
import problem_dir_micro
import matplotlib.pyplot as plt
from pygmo import fast_non_dominated_sorting, population
from ndtest import ks2d2s

import matplotlib.font_manager as font_manager
# Dark2 is found at: https://matplotlib.org/tutorials/colors/colormaps.html done badly here! TODO:fix
dark2 =["#1b9e77", "#d95f02", "#7570b3", "#e7298a", "#66a61e", "#e6ab02", "#a6761d", "#666666", "#c61e11"]
fig_font = {'fontname':'Liberation Serif'}
font = font_manager.FontProperties(family='Liberation Serif')

class plotDataOut:
    pass

class save_data:
    pass

def num_last_chars( x):
    return(x[-4:])

def num_4chars( x):
    return(x[4:8])

def feq(a,b):
    if abs(a-b)<0.0001:
        return True
    else:
        return False

def aw_round(x, base=0.2):
    return base * round(x/base)

def up_round(x, base=0.2):
    if feq(Decimal(x) % Decimal(base), Decimal(0.2) ):
        return x
    elif feq(Decimal(x) % Decimal(base), Decimal(0) ):
        return round(x,1)
    elif feq(Decimal(x) % Decimal(base), Decimal(0.1) ):
        return round(x+0.1, 1)
    else:
        print ("Error invalid rounding value..." +str(x))
        print (str(Decimal(x) % Decimal(base)) )
        print(str(Decimal(0.1)))
        print(Decimal(Decimal(x) % Decimal(base)) == Decimal(0.1))
        exit()

def get_hot_pin_ppf( d):
    full = np.block([[d[0], d[1], d[2]],
                     [d[3], d[4], d[5]],
                     [d[6], d[7], d[8]] ])
    #print(np.shape(full))
    hot_pin = np.unravel_index(full.argmax(), full.shape)
    max = np.max(full)
    full[full == 0.0] = np.nan
    mean_no_zero = np.nanmean(full.ravel())
    ppf = max / mean_no_zero
    #print(str(mean_no_zero))
    #print(str(ppf))
    return (list(hot_pin) + [ppf])

    # takes a list of assembly powers and finds
    # the ppf and the position of the hot pin7

def main():
    graph_base_name = sys.argv[1]
    if len(sys.argv)>2:
        if(sys.argv[2] == 'show'):
            show = True
        else:
            show=False
    else:
        show=False
#    file = "./pop_0060.pickle"
#    file_list = ["/media/andrew/BackupDisk1/200604_burnup_run/pop_0060.pickle", "/media/andrew/BackupDisk1/200604_burnup_run/pop_0000.pickle"]
#    color_list = ["green", "purple"]
#    label_list = ["initial population ", "final population" ]
    file_list = ["../../data/6-9/pop_0060.pickle"]
    color_list = ["purple"]
    label_list = ["Final Evaluations" ]
    fig, ax = plt.subplots()
######################## DSO ###########################
    for file, color, label in zip(file_list, color_list, label_list):
        # Print the inputs to a file
        inputs = []
        outputs = []
        pop = []
        with open(file,'rb') as pickle_file:
            var = pickle.load(pickle_file, encoding='latin1')
            inputs = var.inputs#
            outputs = var.outputs
            inputs = np.concatenate((inputs,outputs), axis=1)
            pop = var.pop
        ndf, dl, dc, ndl = fast_non_dominated_sorting(outputs)
        ndf_simplified = [] # copy.deepcopy(ndf[0])
        #print (inputs)
        for idx in ndf[0]:
            x = inputs[idx]
            new_row = [aw_round(val) for val in x]
            new_row[-1] = x[-1]
            new_row[-2] = x[-2]
            ndf_simplified.append(new_row)
        #ndf_simplified = list(set(ndf_simplified))
        ndf_no_repeat = np.unique(ndf_simplified, axis=0)
        #print(str(ndf_no_repeat))

        line = "input_test_list = ["
        for vals in ndf_no_repeat:
            line +="["
            for v in vals:
                line += str(v)+", "
            line += "],\n"
        line += "]\n"
        print(line)
#        ndf, dl, dc, ndl = fast_non_dominated_sorting(pop.get_f())
#        for idx in ndf[0]:
#            f = pop.get_f()[idx]
#            print("f: "+str(f))
#        print("NDF len:" + str(len(ndf[0])))


    ############# initial pop #################
    #    x_vals_i = [(row[0] + row[1])/2.0 for row in initial_outputs]
#        x_vals_i = [out[0] for out in outputs]#[dist(row[0], row[1]) for row in initial_outputs]
#        y_vals_i = [out[1] for out in outputs]#[row[2] * 2.0 for row in initial_outputs]
#        ax.scatter(x_vals_i, y_vals_i, c=color, marker="o", alpha=0.6, label=label)

#    dat_dir = "../fmtx_burnup/results-3453425/gen_0000/"
    dat_dir = "../../data/6-9/200721-fmtx-burnup-ndf3/" #/media/andrew/BackupDisk1/200619-fmtx-burnup-ndf/"
    outs = []
    ins = []
    full_dir_list = os.listdir(dat_dir)
    file_list = [f for f in full_dir_list if f.endswith(".pickle")]
    # extract the data from the files.
    for i,filename in enumerate(sorted(file_list, key = num_4chars) ):
        with open(dat_dir+'/'+filename,'rb') as pickle_file:
            var = pickle.load(pickle_file, encoding='latin1')
            # assert that:  var.enrichments = input_data[i]
#TODO: Puth this back in and fix.
#                for truth_val in np.isclose(var.enrichments, input_data[i]).tolist():
#                    if truth_val != True:
#                        print("input data not same as expected:")
#                        print("from pickle: "+str(var.enrichments))
#                        print("from input : "+str(input_data[i]))
#                        input("enter to proceed, ctrl+C to exit")

#                results = self.get_hot_pin_ppf(var.detector_data)
#                radial_dist = self.dist(results[0], results[1])

            #use these for the burnup study.
            inputs = var.enrichments
            results = get_hot_pin_ppf(var.detector_data)
            #variance = np.var(var.detector_data, ddof=1)  #  not used since MLP model doesn't
            cyc = var.cycle_length_bu * -1.0
            outs.append([cyc, results[-1]])   #[radial_dist, results[2]]) # cycle length (in burnup) vs ppf
            ins.append(inputs + [cyc, results[-1]])

    line = "input_test_list = ["
    for vals in ins:
        line +="["
        for v in vals:
            line += str(v)+", "
        line += "],\n"
    line += "]\n"
    print(line)
#    x_vals_j = [out[0] for out in outs]#[dist(row[0], row[1]) for row in initial_outputs]
#    y_vals_j = [out[1] for out in outs]#[row[2] * 2.0 for row in initial_outputs]
#    ax.scatter(x_vals_j, y_vals_j, c="green", marker="o", alpha=0.6, label='Initial evaluations')
#
#    ###################    Bells and whistles ##################
#    ax.set_title('DSO of cycle length vs pin power variance evaluated in Serpent', **fig_font)
#    ax.set_ylabel('PPF', **fig_font)
#    ax.set_xlabel('-1 * cycle length/ MWd kg^-1 U', **fig_font)
#
#    # horrible re-order code from: https://stackoverflow.com/questions/22263807/how-is-order-of-items-in-matplotlib-legend-determined
#    handles, labels = plt.gca().get_legend_handles_labels()
#    #order = [2,3,4,0,1,5]
#    print(labels)
#    #ax.legend([handles[idx] for idx in order],[labels[idx] for idx in order],loc=2, prop=font)
#    ax.legend(loc=2, prop=font)
#    print(show)
#    if(show):
#        print("\a")
#        plt.show()
#        fig.savefig(graph_base_name+'-graph.svg')
#    else:
#        # if not debugging save the figure
#        fig.savefig(graph_base_name+'-graph.svg')
#        fig.savefig(graph_base_name+'-graph.png')
if __name__ == "__main__":
    main()
