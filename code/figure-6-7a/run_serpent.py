#!/usr/bin/env python3
#
#                                                 Copyright 2019 Andrew Whyte
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#  FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import sys, os
import re
import numpy as np
import pickle
import fcntl
import time
import socket

from data_store import data_store

def main():
    if 3 != len(sys.argv):
        exit()
    index = sys.argv[1]
    serpent_dir = sys.argv[2]
    # host specific settings
    host = socket.gethostname()
    if host == 'lux' or host == 'lise' or host == 'ray':
        serpent = 'sss2'
    else:
        serpent = '/home/ajw287/rds/hpc-work/software/Serpent/sss2' #
    working_file  = "./hpc_{:04d}".format(int(index))
    os.chdir(serpent_dir)
    os.system(serpent+" -omp 5 "+working_file)
    os.system("python3 parse_detectors.py " + working_file)
    os.chdir("..")
    exit()

if __name__ == "__main__":
		main()
