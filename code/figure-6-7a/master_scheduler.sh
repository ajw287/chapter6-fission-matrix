#!/bin/bash
path=/home/ajw287/rds/hpc-work/ex6_burn_fmtx/

declare -a paramArray=("mut" "cross" ) #"pop" "gen"

# Iterate the string array using for loop
for param in ${StringArray[@]}; do
  echo $param
  for dir in /$param/*/     # list directories in the form "/$param/dirname/"
  do
      dir=${dir%*/}      # remove the trailing "/"
      echo ${dir##*/}    # print everything after the final "/"
  done
done
exit

for seed in $(eval echo {$1..$2})
do
echo "Seed number: $seed "
python3 pygmo_micro.py s$seed
wait           # ensure that the gen_all.zip file has downloaded
ssh hpc:$path  # delete the previous gen_all.zip file
mkdir ./results-$seed
mv pop_00* ./results-$seed/
mv gen_00* ./results-$seed/
mv s$seed* ./results-$seed/
ssh hpc:$path  # do zip up
ssh hpc "cd $path && zip -r gen_all.zip gen_0* path.txt" &
wait
ssh hpc "cd $path && rm -r gen_00*"&
scp hpc:$path/gen_all.zip  ./results-$seed/ & # download the zip file
done
echo "script reached last line of code $seed"
exit 0
