#!/bin/bash
python3 run_serpent.py $1 $2 &
python3 run_serpent.py $3 $4 &
python3 run_serpent.py $5 $6 &
python3 run_serpent.py $7 $8 &
python3 run_serpent.py $9 ${10} &
python3 run_serpent.py ${11} ${12} &
wait
