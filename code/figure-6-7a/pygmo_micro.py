#!/usr/bin/env python3
#
#                                                 Copyright 2019 Andrew Whyte
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#  FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import sys, os
#from problem_smicro import problem_smicro
import copy
#import problem_susmicro as psm
import problem_dir_micro as pdm
import pickle
import matplotlib.pyplot as plt
import numpy as np
import time
from pygmo import population, problem, algorithm, plot_non_dominated_fronts, moead, nsga2, hypervolume, fast_non_dominated_sorting, plot_non_dominated_fronts, bfe, member_bfe, hypervolume
import re

class plotDataOut:
    pass

class save_data:
    pass

def dist(a,b):
    return numpy.sqrt(numpy.sum((a-b)**2, axis=1))

def aw_round(x, base=0.2):
    return base * round(x/base)

def main():
    generation = 0
    generation_step = 5
    max_generations = 60
    if len(sys.argv)>2:
        if(sys.argv[2] == 'show'):
            show = True
        else:
            show=False
    else:
        show=False
    file_base_name = sys.argv[1]
    algo_seed = int(re.findall('\d+', sys.argv[1])[0])
    plotdata = plotDataOut()
    save = save_data()
    #prob = problem(psm.problem_susmicro())
    # N.B. please note that the seed generation for these three sources is irrational
    #      They are adjacent, non-connected numbers.
    print("seed:"+str(algo_seed))
    print("calling with : "+str(algo_seed-11))
    prob = problem(pdm.problem_dir_micro(seed=algo_seed-11))
#    for NSGA-II at least 5 individuals in the population are needed and the population size must be a multiple of 4. Detected input population size is: 50
    pop = population(prob, b=bfe(member_bfe()), size = 40, seed = algo_seed)
    # mutation values to check: 0.005, 0.01, 0.05, 0.075, 0.1
    # crossover values to check: 0.70, 0.80, 0.90, 0.95, 0.9999
    specific_algo = nsga2(gen = generation_step, m=0.01, cr=0.95, seed = algo_seed-199) # defaults: m=0.01, cr=0.95
#    specific_algo.set_seed(12)
    specific_algo.set_bfe(bfe(member_bfe()))
    algo = algorithm(specific_algo) # 250
    initial_inputs = pop.get_x()
    initial_outputs = pop.get_f()
    ndf, dl, dc, ndl = fast_non_dominated_sorting(initial_outputs)
    save.initial_ndf = copy.deepcopy(ndf)
    save.initial_inputs = copy.deepcopy(initial_inputs)
    save.initial_outputs = copy.deepcopy(initial_outputs)
    save.initial_pop = copy.deepcopy(pop)
    for generation in range(0, max_generations, generation_step):
        save.pop = copy.deepcopy(pop)
        ################# initial pop ##################
        #get a list of the non-dominated front of the first set (random points)
        pop = algo.evolve(pop)
        ndf, dl, dc, ndl = fast_non_dominated_sorting(pop.get_f())
        ndf_x = []
        for val in ndf[0]:
            ndf_x.append(pop.get_x()[val])
        save.surr_ndf_x = copy.deepcopy(ndf_x)
        print("evaluate the initial ndf in surrogate")
        inputs = pop.get_x()
        outputs = pop.get_f()
        save.pop = copy.deepcopy(pop)
        save.inputs = copy.deepcopy(inputs)
        save.outputs = copy.deepcopy(outputs)
        #save.hypervolume = hypervolume(pop)
        data_file = "./pop_"+str(generation+generation_step).zfill(4)+".pickle"
        try:
            pickle.dump(save, open(data_file, "wb+" ) )
        except Exception as e:
            print("error opening '"+data_file+"' pickle file " +str(e))
            exit()

    ndf, dl, dc, ndl = fast_non_dominated_sorting(pop.get_f())
    ndf_x = []
    for val in ndf[0]:
        ndf_x.append(pop.get_x()[val])
    save.final_surr_ndf_x = copy.deepcopy(ndf_x)
    final_inputs = pop.get_x()
    final_outputs = pop.get_f()
    save.inputs = copy.deepcopy(final_inputs)
    save.outputs = copy.deepcopy(final_outputs)
    save.pop = pop
    save.hypervolume = hypervolume(pop)
    save.final_pop = copy.deepcopy(pop)

    plotdata.final_inputs = copy.deepcopy(final_inputs)
    plotdata.final_outputs = copy.deepcopy(final_outputs)
    with open(file_base_name+"_plot_data.pickle","wb") as f:
        f.write(pickle.dumps(plotdata))
    with open(file_base_name+"_raw_data.pickle","wb") as f:
        f.write(pickle.dumps(plotdata))
    # Plot
    fig, ax = plt.subplots()
#    x_vals_f = [(row[0] + row[1]) / 2.0 for row in final_outputs]
    x_vals_f = [row[0]for row in final_outputs]#[dist(row[0], row[1]) for row in final_outputs]
    y_vals_f = [row[1]for row in final_outputs]#[row[2] for row in final_outputs]
    ax.scatter(x_vals_f, y_vals_f, c="purple", alpha=0.6, label='Final ndf surrogate model')
#    x_vals_i = [(row[0] + row[1])/2.0 for row in initial_outputs]
    x_vals_i = [row[0] for row in initial_outputs]#[dist(row[0], row[1]) for row in initial_outputs]
    y_vals_i = [row[1] for row in initial_outputs]#[row[2] for row in initial_outputs]
    ax.scatter(x_vals_i, y_vals_i, c="green", alpha=0.6, label='initial evaluation')
    ax.set_title('Initial to Surrogate population')
    ax.set_ylabel('ppf')
    ax.set_xlabel('1/radius')
    ax.legend(loc=1)
    #fig.savefig('surrogate-wims.png')
    #fig.show()
    # if you are debugging probably just show to screen
    if(show):
        print("\a")
        plt.show()
        fig.savefig(file_base_name+'-graph.svg')
    else:
        # if not debugging save the figure
        fig.savefig(file_base_name+'-graph.png')
        fig.savefig(file_base_name+'-graph.svg')

    # Plot only NDF
    #fig, ax = plt.subplots()
    initials = [[a,b] for a,b in zip(x_vals_i,y_vals_i)]
    finals = [[a,b] for a,b in zip(x_vals_f,y_vals_f)]
    ax = plot_non_dominated_fronts(initials, marker='o')
    ax = plot_non_dominated_fronts(finals, marker='x',)

    ax.set_title('Surrogate NDF to Serpent NDF')
    ax.set_ylabel('ppf')
    ax.set_xlabel('1/radius (relative)')
#    ax.legend(loc=1)
    if(show):
        print("\a")
        plt.show()
        fig.savefig(file_base_name+'-ndf_only.svg')
    else:
        # if not debugging save the figure
        fig.savefig(file_base_name+'-ndf_only.png')
        fig.savefig(file_base_name+'-ndf_only.svg')


    ndf_simplified = [] # copy.deepcopy(ndf[0])
    for idx in ndf[0]:
        x = pop.get_x()[idx]
        new_row = [aw_round(val) for val in x]
        ndf_simplified.append(new_row)
    #ndf_simplified = list(set(ndf_simplified))
    ndf_no_repeat = np.unique(ndf_simplified, axis=0)
    print(str(ndf_no_repeat))

    line = "input_test_list = ["
    for vals in ndf_no_repeat:
        line +="["
        for v in vals:
            line += str(v)+", "
        line += "],\n"
    line += "]\n"
    print(line)

    ndf, dl, dc, ndl = fast_non_dominated_sorting(pop.get_f())
    for idx in ndf[0]:
        f = pop.get_f()[idx]
        print("f: "+str(f))
    print("NDF len:" + str(len(ndf)))

if __name__ == "__main__":
    main()
