#!/usr/bin/env python3
#
#                                                 Copyright 2019 Andrew Whyte
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#  FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# python3 code, creates a fission matrix decomposition surrogate model
from scipy.io import loadmat
from numpy import linalg as la
import numpy as np
import copy
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker

import matplotlib.font_manager as font_manager
fig_font = {'fontname':'Liberation Serif'}
font = font_manager.FontProperties(family='Liberation Serif')

burn_up_step = 0

data_dir = "../data/mat_files/"
enrichments = [16,24,32]
iter =0
fn_ave = np.zeros([36,36])
fig, axs = plt.subplots(3, 5)
grid_interval=2.
#loc = plticker.MultipleLocator(base=grid_interval)
loc = plticker.FixedLocator([1.5,3.5])


K_eff_16 = 0
fs_32 = []
K_eff_32 = 0
fs_32 = []
K_eff_32 = 0
fs_32 = []

# Add the grid
for row in axs:
    for ax in row:
        plt.setp(ax.get_xticklabels(), visible=False)
        plt.setp(ax.get_yticklabels(), visible=False)
        ax.xaxis.set_major_locator(loc)
        ax.yaxis.set_major_locator(loc)
        ax.grid(which='major', axis='both', linestyle='-')

for i in enrichments:
    gen_fname = data_dir + "00" + str(i) + "_powermap_fmtx_bustep_"+str(burn_up_step)+".mat"
    x = loadmat(gen_fname, struct_as_record=True)
    fmtx_t = x['fmtx_t']
    powermap = x['powermap']
    eig_vals, eig_vecs = la.eig(fmtx_t)
    eig_vals_sorted = np.sort(eig_vals)
    eig_vecs_sorted = eig_vecs[:, eig_vals.argsort()]
  # pin powers:
    # L2 norm of the vector (thus magn =1)
    s = la.norm(eig_vecs_sorted[:,-1])
    largest_eigenvector = abs(eig_vecs_sorted[:,-1]) #/ s
    #Y = np.transpose(np.reshape(largest_eigenvector, (6,6) ))
    Y = np.reshape(largest_eigenvector, (6,6)) #*float(eig_vals_sorted[-1])
    Fs =  abs(fmtx_t).sum(axis=0) # sum the rows to get the sourciness
    print(Fs)
    fn_ave += abs(fmtx_t) /Fs
    print(largest_eigenvector)
    print(Y)
    # Set the gridding interval: here we use the major tick interval
    fig.suptitle('Fission Matrix for Uniform Cores and Generated models', **fig_font)
    axs[iter, 0].imshow(Y, cmap='viridis', interpolation='none', vmin=0.0, vmax=0.3)
    axs[iter, 0].set_title('Eigenvector of FM: '+str(i/10.0)+"", **fig_font)
    axs[iter, 1].imshow(powermap, cmap='viridis', interpolation='none', vmin=0.0, vmax=85.0)
    axs[iter, 1].set_title('Powermap:', **fig_font)
    iter+=1
    if i==16:
        print("16")
        K_eff_16 = eig_vals_sorted[-1]
        fs_16 = Fs
    elif i==24:
        print("24")
        K_eff_24 = eig_vals_sorted[-1]
        fs_24 = Fs
    elif i==32:
        print("32")
        K_eff_32 = eig_vals_sorted[-1]
        fs_32 = Fs
    else:
        print("Unknown enrichment")
        exit()

fn_ave /=len(enrichments) # make the average

lps =[[ 1.6, 1.6, 2.4, 2.4, 1.6, 1.6,
        1.6, 1.6, 2.4, 2.4, 1.6, 1.6,
        2.4, 2.4, 1.6, 1.6, 3.2, 3.2,
        2.4, 2.4, 1.6, 1.6, 3.2, 3.2,
        1.6, 1.6, 3.2, 3.2, 3.2, 3.2,
        1.6, 1.6, 3.2, 3.2, 3.2, 3.2 ],
      [ 3.2, 3.2, 3.2, 3.2, 2.4, 2.4,
        3.2, 3.2, 3.2, 3.2, 2.4, 2.4,
        3.2, 3.2, 2.4, 2.4, 1.6, 1.6,
        3.2, 3.2, 2.4, 2.4, 1.6, 1.6,
        2.4, 2.4, 1.6, 1.6, 1.6, 1.6,
        2.4, 2.4, 1.6, 1.6, 1.6, 1.6 ],
      [ 1.6, 1.6, 1.6, 1.6, 3.2, 3.2,
        1.6, 1.6, 1.6, 1.6, 3.2, 3.2,
        1.6, 1.6, 2.4, 2.4, 2.4, 2.4,
        1.6, 1.6, 2.4, 2.4, 2.4, 2.4,
        3.2, 3.2, 2.4, 2.4, 3.2, 3.2,
        3.2, 3.2, 2.4, 2.4, 3.2, 3.2 ] ]

iter = 0
for lp in lps:
    f_gen = copy.deepcopy(fn_ave)
    #f_gen = f_gen ./ fs_32;
    for r in range( f_gen.shape[0]):
        for c in range(f_gen.shape[1]):
            if lp[c] == 1.6:
                f_gen[r,c] = (f_gen[r,c]) * fs_16[c] #* K_eff_16)
            elif lp[c] == 2.4:
                f_gen[r,c] = (f_gen[r,c]) * fs_24[c] #* K_eff_24)
            elif lp[c] == 3.2:
                f_gen[r,c] = (f_gen[r,c]) * fs_32[c] #* K_eff_32)
            else:
                print('Unknown U235 value\n');
                exit()
    eig_vals, eig_vecs = la.eig(f_gen)
    eig_vals_sorted = np.sort(eig_vals)
    eig_vecs_sorted = eig_vecs[:, eig_vals.argsort()]
  # pin powers:
    # L2 norm of the vector (thus magn =1)
    s = la.norm(eig_vecs_sorted[:,-1])
    largest_eigenvector = abs(eig_vecs_sorted[:,-1]) #/ s
    Y = np.reshape(largest_eigenvector, (6,6))*float(eig_vals_sorted[-1])
    axs[iter, 3].imshow(Y, cmap='viridis', interpolation='none', vmin=0.0, vmax=0.03)
    axs[iter, 3].set_title('Generated '+ str(iter+1), **fig_font)
    iter+=1

#plt.show()
#exit()

enrichments = [1,2,3]
iter =0
for i in enrichments:
    gen_fname = data_dir + "990" + str(i) + "_powermap_fmtx_bustep_"+str(burn_up_step)+".mat"
    x = loadmat(gen_fname, struct_as_record=True)
    fmtx_t = x['fmtx_t']
    powermap = x['powermap']
    eig_vals, eig_vecs = la.eig(fmtx_t)
    eig_vals_sorted = np.sort(eig_vals)
    eig_vecs_sorted = eig_vecs[:, eig_vals.argsort()]
  # pin powers:
    # L2 norm of the vector (thus magn =1)
    s = la.norm(eig_vecs_sorted[:,-1])
    largest_eigenvector = abs(eig_vecs_sorted[:,-1]) #/ s
    Y = np.reshape(largest_eigenvector, (6,6))*float(eig_vals_sorted[-1])
#    Y = np.reshape(largest_eigenvector, (6,6) )
#    Fs =  fmtx_t.sum(axis=0)
    print(largest_eigenvector)
    print(Y)

    axs[iter, 2].imshow(Y, cmap='viridis', interpolation='none', vmin=0.0, vmax=0.03)
    axs[iter, 2].set_title('Eigenvector of FM '+str(i)+"", **fig_font)
    axs[iter, 4].imshow(powermap, cmap='viridis', interpolation='none', vmin=0.0, vmax=85.0)
    axs[iter, 4].set_title('Powermap:', **fig_font)
    iter+=1


#
plt.show()
