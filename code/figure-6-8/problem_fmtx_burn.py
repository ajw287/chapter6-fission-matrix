#!/usr/bin/env python3
#
#                                                 Copyright 2019 Andrew Whyte
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#  FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import numpy as np
from math import pi
import copy
import os.path
import sys
import os
import pickle
import socket
import time
import random as rn
#import tensorflow as tf
import math
from decompose_fission_matrix import decompose_fmtx

seed  = 'time'
# Max for PYTHONHASHSEED 4294967295
# Max for numpy 2**32
if seed == 'time':
    # shuffled time initialisation source:
    # https://github.com/schmouk/PyRandLib
    #t = int( time.time() * 1000.0 )
    #seed = str( ( ((t & 0x0f000000) >> 24) +
    #            ((t & 0x00ff0000) >>  8) +
    #            ((t & 0x0000ff00) <<  8) +
    #            ((t & 0x000000ff) << 24)   )  )
    seed = str(int( (time.time()*1000.0) % 2147483647))
#os.environ['PYTHONHASHSEED'] = '0'
np.random.seed(int(seed))
#rn.seed(np.random.randint(0, 2**32 - 1, dtype='l'))
rn.seed(np.random.randint(0, 2147483647, dtype='l'))
#
#from keras import backend as K
#tf.set_random_seed(np.random.randint(0, 2147483647, dtype='l'))
#session_conf = tf.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)
#sess = tf.Session(graph=tf.get_default_graph(), config=session_conf)
#K.set_session(sess)
##os.environ['KERAS_BACKEND'] = 'theano'
#from keras.models import Sequential
#from keras.layers import Dense, Activation
#from keras.models import load_model
#from keras.optimizers import Adam

#from parse_detectors import parse_detectors




class problem_serpent_:
    pass

class chromosome:
    pass

class problem_fmtx:
#    model_file = "cnn_model.hdf5"
#    model = load_model(model_file)
    fmtx = decompose_fmtx()

    #originally part of simple_nn.py in conv_nn file (keep in sync!)
    def get_input_image(self, nine_vars, filename='input_img.png', save=False):
        w, h = 55, 55
        control_rods = [[[ 6,  3], [ 9,  3], [12,  3],
                        [ 4,  4], [14,  4],
                        [ 3,  6], [ 6,  6], [ 9,  6], [12,  6], [15,  6],
                        [ 3,  9], [ 6,  9], [12,  9], [15,  9],
                        [ 3, 12], [ 6, 12], [ 9, 12], [12, 12], [15, 12],
                        [ 4, 14], [14, 14], [ 6, 15],
                        [ 9, 15], [12, 15]]
                       ]
        control_rods.append([ [v[0]+18,v[1]+ 0] for v in control_rods[0] ])
        control_rods.append([ [v[0]+36,v[1]+ 0] for v in control_rods[0] ])
        control_rods.append([ [v[0]+ 0,v[1]+18] for v in control_rods[0] ])
        control_rods.append([ [v[0]+18,v[1]+18] for v in control_rods[0] ])
        control_rods.append([ [v[0]+36,v[1]+18] for v in control_rods[0] ])
        control_rods.append([ [v[0]+ 0,v[1]+36] for v in control_rods[0] ])
        control_rods.append([ [v[0]+18,v[1]+36] for v in control_rods[0] ])
        control_rods.append([ [v[0]+36,v[1]+36] for v in control_rods[0] ])

        data = np.zeros((h, w, 3), dtype=np.uint8)
        for x in range(w):
            for y in range(h):
                assembly_number = int(x/18) + (int(y/18) *3)
                if x%18 == 0 or y%18==0 or assembly_number > 9:  # water gap
                    data[y, x] = [0, 0, 255]
                else:
                    if [x,y] in control_rods[assembly_number] or 0== x or 0==y or w-1 == x or h-1 == y:
                        data[y, x] = [0, 0, 255]
                    else:
                        data[y, x] = [nine_vars[assembly_number]*50, 0, 0]
        if save:
            img = Image.fromarray(data, 'RGB')
            new_p.save(filename, "PNG")
        return data

    #originally part of simple_nn.py in conv_nn file (keep in sync!)
    def get_output_image(self, d, filename='pin_power.png', save=False):
        pass

#    #originally part of simple_nn.py in conv_nn file (keep in sync!)
#    def get_cnn_output_image(self, d, filename='pin_power.png', save=False):
#        w, h = 51, 51
#        control_rods = [[[ 6,  3], [ 9,  3], [12,  3],
#                        [ 4,  4], [14,  4],
#                        [ 3,  6], [ 6,  6], [ 9,  6], [12,  6], [15,  6],
#                        [ 3,  9], [ 6,  9], [12,  9], [15,  9],
#                        [ 3, 12], [ 6, 12], [ 9, 12], [12, 12], [15, 12],
#                        [ 4, 14], [14, 14], [ 6, 15],
#                        [ 9, 15], [12, 15]]
#                       ]
#        # note the difference here as there isn't a water gap as an input
#        control_rods.append([ [v[0]+17,v[1]+ 0] for v in control_rods[0] ])
#        control_rods.append([ [v[0]+34,v[1]+ 0] for v in control_rods[0] ])
#        control_rods.append([ [v[0]+ 0,v[1]+17] for v in control_rods[0] ])
#        control_rods.append([ [v[0]+17,v[1]+17] for v in control_rods[0] ])
#        control_rods.append([ [v[0]+34,v[1]+17] for v in control_rods[0] ])
#        control_rods.append([ [v[0]+ 0,v[1]+34] for v in control_rods[0] ])
#        control_rods.append([ [v[0]+17,v[1]+34] for v in control_rods[0] ])
#        control_rods.append([ [v[0]+34,v[1]+34] for v in control_rods[0] ])
#        #full = np.block([[d[0], d[1], d[2]],
#        #                 [d[3], d[4], d[5]],
#        #                 [d[6], d[7], d[8]] ])
#        full = d
#
#        pin_pows = full
#        #ds=data_store.data_store()
#        with open( "scale.pickle", 'rb') as f:
#            ds = pickle.load(f)
#        old_max = ds.scale_data[2]
#        old_min = ds.scale_data[3]
#        offset = ds.scale_data[4]
#        old_range = old_max-old_min
#        scaled_set = ( ( (pin_pows -old_min) * 1)/ old_range) + offset
#        Y = scaled_set
#        scaled = ( ( (pin_pows -old_min) * 1)/ old_range)+0.2
#
#        max = 0.0
#        average = 0.0
#        n = 0
#        for x in range(w):
#            for y in range(h):
#                assembly_number = math.floor(x/17) + (math.floor(y/17) *3)
#                if [x,y] in control_rods[assembly_number] or 0== x or 0==y or w-1 == x or h-1 == y:
#                    #print(np.shape(full))
#                    #print(str(x)+", "+str(y))
#                    #print(full[x,y])
#                    full[y, x] = np.nan
#                else:
#                    if full[y,x] > max:
#                        max = full[y,x]
#                    n +=1
#                    average += full[y,x]
#        average /= n
#        print(" ave: " +str(average) + " max " + str(max) +"  ppf : " +str(max/average))
#        #full[full == 0.0] = np.nan
#        max = np.nanmax(full) # largest value
#        min = np.nanmin(full)
#        mean = np.nanmean(full)
#        ppf = max/mean
#        pos = np.unravel_index(full.argmax(), full.shape)
#        #full[full == np.nan] = 0.0
#        full = np.nan_to_num(full)
#        scaled[scaled == np.nan] = 0
#        #img.convert('RGB')
#        if save:
#            img = Image.fromarray(scaled*255)
#            print(full)
#            img.show()
#            new_p = img.convert("L")
#            new_p.save(filename, "PNG")
#        return full, pos, ppf

    def aw_round(self,x, base=0.2):
        return base * round(x/base)

    def __deepcopy__(self, memo):
        #print ('performing __deepcopy__(%s)' % str(type(memo)))
        #clone_model(model, input_tensors = NULL)
        return problem_fmtx()
        #return VariableWithoutRE(self.name, self.regexTarget, self.type)

    def __init__(self, saves_dir="serpent"):
        self.dim = 9
        self.nobj = 2 # average enrichment , lppf , k infinity error
        self.run_number = 0
        #self.fmtx = decompose_fmtx()
        print("completed init function")

    def get_fmtx_ppf(self, scaled_x):
        powermap = self.fmtx.get_powermap(scaled_x)
        max = np.nanmax(powermap) # largest value
        min = np.nanmin(powermap)
        mean = np.nanmean(powermap)
        ppf = max/mean
        return ppf

    def get_fmtx_cycle(self, scaled_x):
        powermap = self.fmtx.get_powermap(scaled_x)
        #vol_fuel_per_region = 35.2981752929676
        scaled_pm = powermap /np.sum(powermap)
        #cumulative_fissions=3.52355950866561E+022 # v5
        #cumulative_fissions=1.05705929800774E+023
        cumulative_fissions=3.17120355779905E+023


        cum_fm = scaled_pm * cumulative_fissions
        #print(scaled_pm)
#        kg_per_region = [0.326705875089547, 0.326701909139201, 0.326697943285142]
##[2.94035287580592, 2.94031718225281, 2.94028148956627]
#        MeV_per_fission = 200
#        s_per_d = 60*60*24
#        burnup = 10
        sigma_c_U238  = 2.73
#        small_sigma_f_U235  = 577
        sigma_a_U235  = 678
        sigma_a_Pu239 = 1015
#        small_sigma_f_Pu239 = 741
        barns_to_cm2 = 1E-024
#       eV_to_J = 1.6021892E-019
        N0_U235_per_region = {round(1.6, 1):1.33930393570703E+022, round(2.4, 1):2.00893151642837E+022, round(3.2, 1):2.67854283985102E+022}
        N0_U238_per_region = {round(1.6, 1):9.22597904109246E+023, round(2.4, 1):9.15097108140878E+023, round(3.2, 1):9.0759631217251E+023}
        N0_235 = np.zeros(np.shape(cum_fm))
        N0_238 = np.zeros(np.shape(cum_fm))

        for y, row in enumerate(powermap):
            for x, fis in enumerate(row):
                x_index = (int(y/2) *3) + int(x/2)
                N0_235[x,y] = N0_U235_per_region[round(scaled_x[x_index], 1)]
                N0_238[x,y] = N0_U238_per_region[round(scaled_x[x_index], 1)]
        Phi= cum_fm / (sigma_a_U235* barns_to_cm2* N0_235) # fluence
        #Phi /= N0_235  # Phi now calculated
        print(Phi)
        exponent_U235 = -1 * sigma_a_U235 * barns_to_cm2 * Phi
        exponent_Pu239= -1 * sigma_a_Pu239 * barns_to_cm2 * Phi
        print(np.exp(exponent_Pu239))
        print(np.mean(exponent_Pu239))
        N10_235 = N0_235 * np.exp(exponent_U235)
        N10_239 = (sigma_c_U238/sigma_a_Pu239)*N0_238 *( 1- np.exp(exponent_Pu239))
        #print("N10 235:")
        #print(N10_235)
        #print("N10 239:")
        #print(N10_239)
        #print("initial:")
        #print(np.sum(N0_235))
        #print("objective:")
        print(-1*np.sum(N10_235) + np.sum(N10_239))
        return(-1 * np.sum(N10_235) + np.sum(N10_239))
#
#### Sigma_f_u235 = [2.18928702259835E+023, 3.283890669584E+023,   4.37846774165108E+023, 5.47301823976734E+023]
#####        Sigma_c_u238 = [7.13547444680529E+022, 7.07746245943289E+022, 7.01945047206049E+022, 6.96143848468809E+022]
#####        Sigma_f_u235 = [0.218928702259835, 0.3283890669584, 0.437846774165108, 0.547301823976734]
#####        Sigma_c_u238 = [0.071354744468053, 0.070774624594329, 0.070194504720605, 0.069614384846881]
#####
#####        N235_cm2 = [3.79425827140096E+020, 5.69131831816985E+020, 7.58833230788748E+020]
#####        N238_cm2 = [2.61372690359168E+022, 2.59247709136736E+022, 2.57122727914304E+022]
#####
#####        fuel_pellet_area = (pi * 0.4126**2) * 66 # this is the area of fuel in a 1/4 assembly_number
#####        assembly area = 21.612**2
####
####        powermap = self.fmtx.get_powermap(scaled_x)
####        print(powermap)
####        tot = np.sum(powermap)
####        print(tot)
####        normmap=powermap / tot
####        print(normmap)
####        st = ""
####        tot = 0
####        for y, row in enumerate(normmap):
####            for x, fis in enumerate(row):
####                st += str(fis)+", "
####                x_index = (str( (int(y/2) *3) + int(x/2) ))
####                if (scaled_x [x_index] == 1.6):
####                    index = 0
####                elif (scaled_x [x_index] == 2.4):
####                    index = 1
####                elif (scaled_x [x_index] == 3.2):
####                    index = 2
####                else:
####                    print("error unknown enrichemnt!")
####                    exit()
####                Phi = fis * (fuel_pellet_area/assembly_area)
####
####                tot += (fuel_pellet_area * Sigma_f_u235) * np.exp(small_sigma_a_U235 * barns_to_cm2 * Phi) + (small_sigma_c_U238/small_sigma_a_Pu239) * (1-np.exp(small_sigma_f_Pu239* barns_to_cm2*Phi))
####            print(st)
####            st = ""
####        #len =
####        exit()

#        Sigma_f_u235 = [2.18928702259835E+023, 3.283890669584E+023,   4.37846774165108E+023, 5.47301823976734E+023]
#        Sigma_c_u238 = [7.13547444680529E+022, 7.07746245943289E+022, 7.01945047206049E+022, 6.96143848468809E+022]
#
#        powermap = self.fmtx.get_powermap(scaled_x)
#        q_assembly = np.array(scaled_x)
#        q_assembly =  np.repeat(q_assembly, 2)
#        q_assembly = q_assembly.reshape(3,6)
#        q_assembly =  np.repeat(q_assembly, [2, 2, 2], axis=0)
#        rows = powermap.shape[0]
#        cols = powermap.shape[1]
#
#        len = 0.0
#        for x in range(0, cols - 1):
#            for y in range(0, rows -1):
#                 index = int(q_assembly[x,y]/0.8 - 2)
#                 len -=   ((Sigma_f_u235[index]*2 )   / powermap[x,y] ) + ( (Sigma_c_u238[index]/Sigma_f_u235[index]) * powermap[x,y])
#                 #len -=  (Sigma_f_u235[index] + ( (Sigma_c_u238[index]/Sigma_f_u235[index]) * powermap[x,y]) )   / (powermap[x,y]* Sigma_f_u235[index])
#                 #len -= (1.0/powermap[x,y]) + ( (Sigma_c_u238[index]/Sigma_f_u235[index]) * powermap[x,y])
#                 #len -=  (1.0/powermap[x,y]) + (2.75 * ((100-q_assembly[x,y]) / q_assembly[x,y]) * powermap[x,y] )
        return   1.5E-24 * len
#    def start_fitness(self, x, run=0):
#        self.run_number = run
#        print("not implemented!")
#        hash = hash(tuble(x))
#        return hash

    def dist(self, a,b):
        return np.sqrt(np.sum((a-b)**2, axis=0))

    def fitness(self, x, run=0):
        self.run_number = run
        scaled_x = [(v * 0.8) + 2.4 for v in x]
        #scaled_x = [float(v)/10.0 if v%2==0 else float(v+1)/10.0 for v in x]
        ppf = self.get_fmtx_ppf(scaled_x)
        #radial_dist = self.dist(results[0]/10.0, results[1])
        #mean_enrichment = np.nanmean(scaled_x)
        cycle_length  = self.get_fmtx_cycle(scaled_x)
        return [cycle_length , ppf]

    def get_nobj(self):
        return self.nobj

    def get_bounds(self):
        return ([-1] * self.dim, [1] * self.dim)

    def get_nix(self):
        return 9

    def get_name(self):
        return "Fuel design with pyGMO 2.6+ and keras model"

    def get_extra_info(self):
        string = "Relies on keras model: "+str(model_file)+"\tDimensions: " + str(self.dim)
        return string
