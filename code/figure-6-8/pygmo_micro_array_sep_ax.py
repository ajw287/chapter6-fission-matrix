#!/usr/bin/env python3
#
#                                                 Copyright 2019 Andrew Whyte
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#  FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# time python3 pygmo_micro_array.py test_run2 ../process_array_burnup2/runs/
# time python3 pygmo_micro_array.py test_run2 /media/andrew/BackupDisk1/200618-fmtx-burnup-ndf/
#figs 6.7 & 6.8
#andrew@laptop-phd:~/Documents/PhD/code/Serpent/fission_matrix/generator-post-process/chapter_6/6-X_pygmo_burnup_fmtx_new$ time python3 pygmo_micro_array_sep_ax.py sep_ax_run2 /media/andrew/BackupDisk1/200721-fmtx-burnup-ndf3/
import sys, os
#from problem_smicro import problem_smicro
import copy
import problem_fmtx_burn as psm
import pickle
import matplotlib.pyplot as plt
import numpy as np
from pygmo import population, problem, algorithm, plot_non_dominated_fronts, moead, nsga2, hypervolume, fast_non_dominated_sorting, plot_non_dominated_fronts

from timeit import default_timer as timer

import matplotlib.font_manager as font_manager
# Dark2 is found at: https://matplotlib.org/tutorials/colors/colormaps.html done badly here! TODO:fix
dark2 =["#1b9e77", "#d95f02", "#7570b3", "#e7298a", "#66a61e", "#e6ab02", "#a6761d", "#666666"]
fig_font = {'fontname':'Liberation Serif'}
font = font_manager.FontProperties(family='Liberation Serif')

class plotDataOut:
    pass

class save_data:
    pass

### special rounding...
from decimal import *
getcontext().prec = 3       # Set a new precision to 4

def dist(a,b):
    return np.sqrt(np.sum((a-b)**2, axis=0))

def aw_round(x, base=0.2):
    return base * round(x/base)

def feq(a,b):
    if abs(a-b)<0.0001:
        return True
    else:
        return False

def up_round(x, base=0.2):
    if feq(Decimal(x) % Decimal(base), Decimal(0.2) ):
        return x
    elif feq(Decimal(x) % Decimal(base), Decimal(0) ):
        return round(x,1)
    elif feq(Decimal(x) % Decimal(base), Decimal(0.1) ):
        return round(x+0.1, 1)
    else:
        print ("Error invalid rounding value..." +str(x))
        print (str(Decimal(x) % Decimal(base)) )
        print(str(Decimal(0.1)))
        print(Decimal(Decimal(x) % Decimal(base)) == Decimal(0.1))
        exit()

def aw_round(x, base=0.2):
    return base * round(x/base)


import matplotlib.font_manager as font_manager
# Dark2 is found at: https://matplotlib.org/tutorials/colors/colormaps.html done badly here! TODO:fix
dark2 =["#1b9e77", "#d95f02", "#7570b3", "#e7298a", "#66a61e", "#e6ab02", "#a6761d", "#666666"]
fig_font = {'fontname':'Liberation Serif'}
font = font_manager.FontProperties(family='Liberation Serif')

input_test_list = [[2.3, 2.3, 2.5, 2.3, 2.5, 2.5, 2.5, 2.6, 2.3, ],
[2.3, 2.3, 2.5, 2.3, 2.5, 2.5, 2.5, 4.0, 2.3, ],
[2.3, 2.3, 2.5, 2.3, 2.9, 2.5, 4.1, 2.4, 2.3, ],
[2.3, 2.3, 4.1, 2.6, 3.4, 2.5, 4.1, 2.4, 2.3, ],
[2.3, 2.6, 2.5, 2.3, 2.5, 2.4, 2.5, 2.9, 2.3, ],
[2.3, 2.6, 2.5, 2.3, 2.5, 2.5, 2.5, 2.4, 2.3, ],
[2.3, 2.6, 2.5, 2.3, 2.5, 2.5, 2.5, 2.9, 2.3, ],
[2.3, 2.6, 2.5, 2.3, 2.9, 2.5, 2.5, 2.6, 2.3, ],
[2.3, 2.6, 2.5, 2.3, 2.9, 2.5, 2.5, 2.9, 2.3, ],
[2.3, 2.6, 2.5, 2.3, 2.9, 2.5, 2.5, 4.0, 2.3, ],
[2.3, 2.6, 2.5, 2.3, 2.9, 4.7, 4.1, 4.0, 4.6, ],
[2.3, 2.6, 2.5, 2.3, 3.4, 2.5, 2.5, 4.0, 2.3, ],
[2.3, 2.6, 2.5, 2.3, 3.4, 2.5, 3.8, 4.0, 2.3, ],
[2.3, 2.6, 2.5, 2.3, 3.4, 4.7, 4.1, 4.0, 2.3, ],
[2.3, 2.6, 2.5, 2.3, 3.4, 4.7, 4.3, 4.0, 2.3, ],
[2.3, 2.6, 2.5, 2.3, 3.4, 4.7, 4.9, 4.0, 2.3, ],
[2.3, 2.6, 2.5, 2.6, 2.9, 2.5, 2.5, 4.0, 2.3, ],
[2.3, 2.6, 2.5, 2.6, 2.9, 2.9, 2.5, 2.9, 2.3, ],
[2.3, 2.6, 2.5, 2.6, 3.4, 2.5, 2.5, 4.0, 2.3, ],
[2.3, 2.6, 2.5, 2.6, 3.4, 2.5, 2.7, 4.0, 2.6, ],
[2.3, 2.6, 2.7, 2.7, 3.4, 2.5, 2.5, 4.0, 2.3, ],
[2.3, 2.6, 3.3, 2.6, 3.4, 4.7, 4.1, 2.9, 2.9, ],
[2.3, 2.6, 4.1, 2.3, 2.9, 2.4, 4.1, 2.4, 2.3, ],
[2.3, 2.6, 4.1, 2.3, 2.9, 2.5, 4.1, 2.4, 2.3, ],
[2.3, 2.6, 4.1, 2.3, 2.9, 4.7, 4.1, 4.0, 4.6, ],
[2.3, 2.6, 4.1, 2.6, 3.4, 4.7, 4.1, 4.0, 4.6, ],
[2.3, 2.6, 4.1, 2.7, 3.4, 2.9, 4.1, 2.9, 2.3, ],
[2.3, 2.6, 4.1, 2.7, 3.4, 4.7, 4.1, 4.0, 4.6, ],
[2.3, 2.6, 4.7, 2.3, 2.9, 2.5, 4.1, 2.4, 2.3, ],
[2.3, 2.6, 4.7, 2.3, 2.9, 2.5, 4.1, 2.9, 2.3, ],
[2.3, 2.6, 4.7, 2.3, 2.9, 2.5, 4.1, 4.0, 2.3, ],
[2.3, 2.9, 2.7, 2.6, 3.4, 4.7, 4.9, 4.0, 2.3, ],
[2.3, 2.9, 3.3, 2.3, 3.4, 4.7, 4.1, 4.0, 2.3, ],
[2.3, 2.9, 3.3, 2.3, 3.4, 4.7, 4.9, 4.0, 2.3, ],
[2.3, 2.9, 3.3, 2.6, 2.9, 4.7, 4.1, 2.9, 4.6, ],
[2.3, 2.9, 3.3, 2.6, 2.9, 4.7, 4.1, 3.4, 4.6, ],
[2.3, 2.9, 3.3, 2.6, 3.4, 4.7, 4.9, 4.0, 3.9, ],
[2.3, 2.9, 4.1, 2.6, 3.4, 4.7, 3.8, 4.0, 3.9, ],
[2.3, 2.9, 4.1, 2.6, 3.4, 4.7, 4.1, 4.0, 4.6, ],
[2.3, 2.9, 4.1, 2.7, 2.9, 4.7, 4.1, 4.0, 4.6, ],
[2.3, 2.9, 4.1, 2.7, 3.4, 4.7, 4.3, 4.0, 4.6, ],
[2.3, 2.9, 4.7, 2.3, 2.9, 2.5, 4.1, 2.4, 2.3, ],
[2.3, 2.9, 4.7, 2.3, 2.9, 2.5, 4.1, 4.0, 2.3, ],
[2.3, 2.9, 4.7, 2.6, 2.9, 2.5, 4.1, 2.9, 2.3, ],
[2.3, 2.9, 4.7, 2.6, 2.9, 2.9, 4.1, 2.9, 2.3, ] ]

class plotDataOut:
    pass

class save_data:
    pass

def dist(a,b):
    return np.sqrt(np.sum((a-b)**2, axis=0))

def aw_round(x, base=0.2):
    return base * round(x/base)

#from: https://stackoverflow.com/questions/13464152/typeerror-unhashable-type-list-when-using-built-in-set-function
def uniq(lst):
    last = object()
    for item in lst:
        if item == last:
            continue
        yield item
        last = item

def num_last_chars( x):
    return(x[-4:])

def get_hot_pin_ppf( d):
    full = np.block([[d[0], d[1], d[2]],
                     [d[3], d[4], d[5]],
                     [d[6], d[7], d[8]] ])
    #print(np.shape(full))
    hot_pin = np.unravel_index(full.argmax(), full.shape)
    max = np.max(full)
    full[full == 0.0] = np.nan
    mean_no_zero = np.nanmean(full.ravel())
    ppf = max / mean_no_zero
    #print(str(mean_no_zero))
    #print(str(ppf))
    return (list(hot_pin) + [ppf])

    # takes a list of assembly powers and finds
    # the ppf and the position of the hot pin7

def removeDuplicates(listofElements):
    # Create an empty list to store unique elements
    uniqueList = []
    # Iterate over the original list and for each element
    # add it to uniqueList, if its not already there.
    for elem in listofElements:
        if elem not in uniqueList:
            uniqueList.append(elem)
    # Return the list of unique elements
    return uniqueList

def main():
    #array_sim_dir = sys.argv[1]
    generation = 0
    if len(sys.argv)>3:
        if(sys.argv[3] == 'show'):
            show = True
        else:
            show=False
    else:
        show=False
    file_base_name = sys.argv[1]
    array_sim_dir = sys.argv[2]
    plotdata = plotDataOut()
    save = save_data()
    prob = problem(psm.problem_fmtx())
    seed = 3453422
    pop = population(prob,  size = 60, seed = seed) # 60
    specific_algo = nsga2(gen = 1, seed = seed -199)
#    pop = population(prob, size = 210, seed = 3453412)
#    algo = algorithm(moead(gen = 20)) # 250
    algo = algorithm(specific_algo)
    initial_inputs = pop.get_x()
    initial_outputs = pop.get_f()
    ndf, dl, dc, ndl = fast_non_dominated_sorting(initial_outputs)
    save.initial_ndf = copy.deepcopy(ndf)
    save.initial_inputs = copy.deepcopy(initial_inputs)
    save.initial_outputs = copy.deepcopy(initial_outputs)
    plotdata.initial_inputs = copy.deepcopy(initial_inputs)
    plotdata.initial_outputs = copy.deepcopy(initial_outputs)

    start = timer()
    for generation in range(0, 50): # 50
        save.pop = copy.deepcopy(pop)
        ################# initial pop ##################
        #get a list of the non-dominated front of the first set (random points)
        ndf, dl, dc, ndl = fast_non_dominated_sorting(pop.get_f())
        ndf_x = []
        for val in ndf[0]:
            ndf_x.append(pop.get_x()[val])
        save.initial_surr_ndf_x = copy.deepcopy(ndf_x)
        print("evaluate the initial ndf in surrogate")
        inputs = pop.get_x()
        outputs = pop.get_f()
        save.inputs = copy.deepcopy(inputs)
        save.outputs = copy.deepcopy(outputs)
        data_file = "./pop_"+str(generation).zfill(4)+".pickle"
        save.pop = copy.deepcopy(pop)
        try:
            pickle.dump(save, open(data_file, "wb+" ) )
        except:
            print("error opening '"+data_file+"' pickle file")
            exit()
        pop = algo.evolve(pop)
    end = timer()


    ndf, dl, dc, ndl = fast_non_dominated_sorting(pop.get_f())
    ndf_x = []
    for val in ndf[0]:
        ndf_x.append(pop.get_x()[val])
    save.final_surr_ndf_x = copy.deepcopy(ndf_x)
    final_inputs = pop.get_x()
    final_outputs = pop.get_f()
    save.inputs = copy.deepcopy(final_inputs)
    save.outputs = copy.deepcopy(final_outputs)

    plotdata.final_inputs = copy.deepcopy(final_inputs)
    plotdata.final_outputs = copy.deepcopy(final_outputs)
    with open(file_base_name+"_plot_data.pickle","wb") as f:
        f.write(pickle.dumps(plotdata))
    # Plot
    #fig, ax = plt.subplots()
    #fig = plt.figure()
    #ax = fig.add_subplot(111)
    fig, ax2 = plt.subplots()
    ax = ax2.twiny() # counterintuitive twiny for two x axes...

############# initial pop #################
    x_vals_i = [row[0] for row in initial_outputs]
    #x_vals_i = [row[0] for row in initial_outputs]#[dist(row[0], row[1]) for row in initial_outputs]
    y_vals_i = [row[1] for row in initial_outputs] #[row[2] * 2.0 for row in initial_outputs]
    #ax.scatter(x_vals_i, y_vals_i, c="green", marker="o", alpha=0.6, label='Initial SMO popultion') #marker="1"
#
#    ndf_no_repeat2 = sorted(x_vals_f, key = np.mean)
#
#    mean_enriches =
#    ndf_no_repeat = np.unique(ndf_simplified, axis=0)
    #for x3,y3,x4,y4 in zip (final_wims_lppf, final_wims_ur, gen2_surr_lppf, gen2_surr_ur):
################ final pop  ################
#    x_vals_f = [np.mean([aw_round(val/10.0) for val in x]) for x in final_inputs]
    #x_vals_f = [np.mean([up_round((val*0.8) + 2.4) for val in x]) for x in final_inputs] # enrichment = up_round(input_test_list[line_number][pn-1])
    x_vals_f = [row[0] for row in final_outputs]
    y_vals_f = [row[1] for row in final_outputs] #[row[2] * 2.0 for row in final_outputs]
    print(len(x_vals_f))
    print(len(y_vals_f))
    ax.scatter(x_vals_f, y_vals_f, c="purple", marker="o", alpha=0.6, label='Final SMO population')#marker="2"
    ax.tick_params(axis='x')


#################  THIS IS THE BIT WHERE I LOAD THE SERPENT DATA   #############
    full_dir_list = os.listdir(array_sim_dir) #
    file_list_all_picks = [f for f in full_dir_list if f.endswith(".pickle")]
    ## only parse files that have an 'out file'
    out_file_list = [os.path.splitext(f)[0] for f in full_dir_list if f.endswith(".out")]
    file_list = [j for j in file_list_all_picks if os.path.splitext(j)[0] in out_file_list]

    # extract the data from the files.
    y_sim_vals = []
    x_vals_in = []
    in_vals_serp = []
    in_vals_test = []
    print("got here.")
    for i,filename in enumerate(sorted(file_list, key = num_last_chars) ):
        with open(array_sim_dir+filename,'rb') as pickle_file:
            print("opened file")
            var = pickle.load(pickle_file, encoding='latin1')
            results = get_hot_pin_ppf(var.detector_data)
            ppf = results[-1] # last result is ppf
            y_sim_vals.append(ppf)
            #x_vals_in.append(np.mean(ndf_simplified[i])/10.0)
            in_vals_serp.append([int((float(x)-2.4)/0.8) for x in var.enrichments])
            in_vals_test.append(copy.deepcopy(var.enrichments))
            x_vals_in.append(var.cycle_length_bu * -1.0)
            #x_vals_in.append(np.mean(input_test_list[i]))
    print("scatter: ")
    print(x_vals_in)
    print(y_sim_vals)
    #x_vals_in = [np.mean(removeDuplicates([aw_round(x) for x in inlist]])) for inlist in final_inputs]
    #x_vals_in = [np.mean(list(y)) for y in set(tuple([aw_round(v) for v in x]) for x in final_inputs)]
    color = '#000000' #'tab:black' black not in tab colors
    #ax2.set_xlabel('cycle', color=color)  # we already handled the x-label with ax1
    ax2.tick_params(axis='x', labelcolor=color)
    ax2.scatter(x_vals_in, y_sim_vals, c='orangered', marker="o", alpha=0.6, label='Final SMO NDF in Serpent')#marker="3"
######################### DSO ###########################
    #dso_pickle = "../fmtx_dso/serp-3453422_plot_data.pickle"#"../dso_pickle/s3454321_plot_data.pickle"
    dso_pickle = "../../data/6-8/s3453425_plot_data.pickle"
    # plot the ndf of the DSO
    #in_vals_dso =[]
    with open(dso_pickle,'rb') as pickle_file:
        plotdatadso = pickle.load(pickle_file, encoding='latin1')
        final_outputs_dso = sorted(plotdatadso.final_outputs, key=lambda k: [k[1], k[0]])
        #in_vals_dso = copy.deepcopy(plotdatadso.final_inputs)
        dso_x = [row[0]for row in final_outputs_dso]#[dist(row[0], row[1]) for row in final_outputs_dso]
        dso_y = [row[1]for row in final_outputs_dso]#[row[2] for row in final_outputs_dso]
    dso_x = [dso_x[0]+1] + dso_x + [dso_x[-1]]
    dso_y = [dso_y[0]] + dso_y + [dso_y[-1]+0.25]
    ax2.step(dso_x, dso_y, color="red", alpha=0.8, where='pre', label='NDF of example DSO (seed=3454325)')

#    in_vals_smo2 =[]
#    for inputs in in_vals_smo:
#        in_vals_smo2.append([(x*0.8)+2.4 for x in inputs])
#    print(in_vals_test)
    final_inputs_scaled =[]
    for inputs in final_inputs:
        final_inputs_scaled.append([(x*0.8)+2.4 for x in inputs])
#    print(in_vals_smo)
#    print(in_vals_smo2)
    print(in_vals_test)
    print(final_inputs_scaled)
    for c, inputs1 in enumerate(in_vals_test):
        for i, inputs2 in enumerate(final_inputs_scaled):
            val = [True if abs(i -j) < 0.01 else False  for i, j in zip(inputs1, inputs2) ]
            if all(val) :
                print("match")
                ax.annotate("",
                        # minmax scaling based on : https://stackoverflow.com/questions/5294955/how-to-scale-down-a-range-of-numbers-with-a-known-min-and-max-value#5295202
                        xy=( ((((-3-(-12))*(x_vals_in[c]+18.0))/(-3+18))-12)*1E23, y_sim_vals[c] ),
                        xytext=( x_vals_f[i], y_vals_f[i] ),  # scale the x axis for ax not ax2
                        arrowprops=dict(arrowstyle="->", #linestyle="dashed",
                        color="0.5",
                        shrinkA=5, shrinkB=5,
                        connectionstyle="arc3", #,rad=-0.3",
                        ),
                )
                break
    print("finished loop")
    #ax.annotate("",
    #        xy=( -4E21, 1.8 ),
    #        xytext=( -8E21, 2 ),  # scale the x axis for ax not ax2
    #        arrowprops=dict(arrowstyle="->", #linestyle="dashed",
    #        color="0.5",
    #        shrinkA=5, shrinkB=5,
    #        connectionstyle="arc3", #,rad=-0.3",
    #        ),
    #)
    #ax2.annotate("",
    #        xy=( -6,1.5 ),
    #        xytext=( -16, 3 ),  # scale the x axis for ax not ax2
    #        arrowprops=dict(arrowstyle="->", #linestyle="dashed",
    #        color="0.5",
    #        shrinkA=5, shrinkB=5,
    #        connectionstyle="arc3", #,rad=-0.3",
    #        ),
    #)
######################## SMO BIG #######################
#    smo_big_file = "./smo_big/pop_0999.pickle"
#    # extract the data from the files.
#    x_smo_big = []
#    y_smo_big = []
#    big_x = []
#    big_y = []
#    with open(smo_big_file,'rb') as pickle_file:
#        s = pickle.load(pickle_file, encoding='latin1')
#        #results = get_hot_pin_ppf(s.outputs)
#        ppf = [row[1] for row in s.outputs]#[x[-1] for x in results] # last result is ppf
#        y_smo_big.append(ppf)
#
#        x_smo_big.append(np.mean(list([up_round((x*0.8) + 2.4) for x in vals ])) for vals in s.inputs)
#        temp_list = list([[x,y] for x,y  in zip(x_smo_big[0],y_smo_big[0])])
#        #print(temp_list)
#        values = sorted(temp_list, key=lambda k: [k[1], k[0]])
#        for x,y in values:
#            big_x.append(x)
#            big_y.append(y)
#        #x_smo_big = [v[0] for v in values]
#        #y_smo_big = [v[1] for v in values]
#    #print(x_smo_big)
#    #print(y_smo_big)
#    plt.step(big_x, big_y, color=dark2[4], alpha=0.8, where='post', label='SMO (p=100, N=1000) NDF')

#    smo_big_dir = "../process_smo_pareto_ex1/s3453417-big/"
#    smo_big_list = os.listdir(smo_big_dir)
#    file_list_all_picks = [f for f in smo_big_list if f.endswith(".pickle")]
#    ## only parse files that have an 'out file'
#    out_file_list = [os.path.splitext(f)[0] for f in smo_big_list if f.endswith(".out")]
#    file_list = [j for j in file_list_all_picks if os.path.splitext(j)[0] in out_file_list]
#    # extract the data from the files.
#    x_smo_big = []
#    y_smo_big = []
#    for i,filename in enumerate(sorted(file_list, key = num_last_chars) ):
#        print(smo_big_dir+filename)
#        with open(smo_big_dir+filename,'rb') as pickle_file:
#            var = pickle.load(pickle_file, encoding='latin1')
#            results = get_hot_pin_ppf(var.detector_data)
#            ppf = results[-1] # last result is ppf
#            y_smo_big.append(ppf)
#            x_smo_big.append(np.mean(var.enrichments))
#            vars = sorted(zip(x_smo_big,y_smo_big), key=lambda k: [k[1], k[0]])
#            x_smo_big = [v[0] for v in vars]
#            y_smo_big = [v[1] for v in vars]
#    #plt.step(x_smo_big, y_smo_big, color=dark2[0], alpha=0.8, where='post', label='SMO - actual; p=100, N=1000')
#    ax.scatter(x_smo_big, y_smo_big, c="black", marker="+", alpha=0.7, label='SMO (p=100, N=1000) NDF in Serpent')
#    ax.annotate("",
#            xy=(np.min(x_smo_big), np.max(y_smo_big)),
#            xytext=(np.min(big_x), np.max(big_y)),
#            arrowprops=dict(arrowstyle="->", #linestyle="dashed",
#            color="0.5",
#            shrinkA=5, shrinkB=5,
#            connectionstyle="arc3", #,rad=-0.3",
#            ),
#    )
#    ax.annotate("",
#            xy=(np.max(x_smo_big), np.min(y_smo_big)),
#            xytext=(np.max(big_x), np.min(big_y)),
#            arrowprops=dict(arrowstyle="->", #linestyle="dashed",
#            color="0.5",
#            shrinkA=5, shrinkB=5,
#            connectionstyle="arc3", #,rad=-0.3",
#        ),
#    )
###################    Bells and whistles ##################
    color = '#0A000A'#'tab:purple'
    #ax2.set_xlabel('cycle', color=color)  # we already handled the x-label with ax1
    ax.tick_params(axis='x', labelcolor=color)

    ax.set_title('Fission Matrix SMO with NDF evaluated in Serpent', **fig_font)
    ax.set_ylabel('PPF', **fig_font)
    ax.set_xlabel('Surrogate objective (~Nf)',color=color, **fig_font)
    ax.set_xlim(-12E23, -3E23)
    ax2.set_xlim(-18, -3)
    ax2.set_xlabel('Cycle Length (MWd/kgU)', **fig_font)
    # horrible re-order code from: https://stackoverflow.com/questions/22263807/how-is-order-of-items-in-matplotlib-legend-determined
#    handles, labels = plt.gca().get_legend_handles_labels()
#    order = [2,3,4,0,1,5]
#    print(labels)
    #ax.legend([handles[idx] for idx in order],[labels[idx] for idx in order],loc=2, prop=font)
    ax2.legend(loc=1, prop=font) # loc=2-top left , loc=1 -top right
    ax.legend(loc=0, prop=font) # loc=2-top left , loc=1 -top right
    #fig.tight_layout()
#    ax.annotate("",
#            xy=(np.min(x_vals_in), np.max(y_sim_vals)),
#            xytext=(np.min(x_vals_f), np.max(y_vals_f)),
#            arrowprops=dict(arrowstyle="->", #linestyle="dashed",
#            color="0.5",
#            shrinkA=5, shrinkB=5,
#            connectionstyle="arc3", #,rad=-0.3",
#            ),
#    )
#    ax.annotate("",
#            xy=(np.max(x_vals_in), np.min(y_sim_vals)),
#            xytext=(np.max(x_vals_f), np.min(y_vals_f)),
#            arrowprops=dict(arrowstyle="->", #linestyle="dashed",
#            color="0.5",
#            shrinkA=5, shrinkB=5,
#            connectionstyle="arc3", #,rad=-0.3",
#        ),
#    )
    print(show)
    if(show):
        print("\a")
        plt.show()
        fig.savefig(file_base_name+'-graph.svg')
    else:
        # if not debugging save the figure
        fig.savefig(file_base_name+'-graph.png')
        fig.savefig(file_base_name+'-graph.svg')

    fig, ax = plt.subplots()
    ax.scatter(x_vals_i, y_vals_i, c="green", marker="o", alpha=0.6, label='Initial SMO popultion') #marker="1"
    ax.scatter(x_vals_f, y_vals_f, c="purple", marker="o", alpha=0.6, label='Final SMO population')#marker="2"
    ax.set_title('Evolution of NSGA2 on PPF and burnup using a Fission matrix SMO', **fig_font)
    ax.set_ylabel('PPF', **fig_font)
    ax.set_xlabel('Surrogate objective (~Nf)',color=color, **fig_font)
    ax.legend(loc=1, prop=font)
    if(show):
        print("\a")
        plt.show()
        fig.savefig(file_base_name+'-basic.svg')
    else:
        # if not debugging save the figure
        fig.savefig(file_base_name+'-basic.png')
        fig.savefig(file_base_name+'-basic.svg')
#
#    # Plot only NDF
#    fig, ax = plt.subplots()
#    initials = [[a,b] for a,b in zip(x_vals_i,y_vals_i)]
#    finals = [[a,b] for a,b in zip(x_vals_f,y_vals_f)]
#    plt.ylim([0,6])
#    plt.xlim([0,0.6])
#    ax = plot_non_dominated_fronts(initials, marker='o')
#    plt.ylim([0,6])
#    plt.xlim([0,0.6])
#    ax = plot_non_dominated_fronts(finals, marker='x',)
#
#    ax.set_title('Surrogate NDF to Serpent NDF')
#    ax.set_ylabel('ppf')
#    ax.set_xlabel('1/radius (relative)')
##    ax.legend(loc=1)
#    if(show):
#        print("\a")
#        plt.show()
#        fig.savefig(file_base_name+'-ndf_only.svg')
#    else:
#        # if not debugging save the figure
#        fig.savefig(file_base_name+'-ndf_only.png')
#        fig.savefig(file_base_name+'-ndf_only.svg')
#
#
    ndf_simplified = [] # copy.deepcopy(ndf[0])
    for idx in range(len(pop.get_x())):#ndf[0]:
        x = pop.get_x()[idx]
        ex = [(n*0.8) + 2.4 for n in x]
        new_row = [up_round(val) for val in ex]
        ndf_simplified.append(new_row)
    ndf_simplified = list(uniq(sorted(ndf_simplified, reverse=True)))
    #ndf_simplified = list(set(ndf_simplified))
    print(str(len(ndf_simplified)))
    ndf_no_repeat = np.unique(ndf_simplified, axis=0)
    print("data from populations:"+str(len(pop))+" "+str(len(ndf_no_repeat)))
    print(str(ndf_no_repeat))

    line = "input_test_list = ["
    # get whole final population and print it out...
    for vals in ndf_no_repeat:
        line +="["
        for v in vals:
            line += str(v/10.0)+", "
        line += "],\n"
    line += "]\n"
    print(line)
    ndf, dl, dc, ndl = fast_non_dominated_sorting(pop.get_f())
    #ax = plot_non_dominated_fronts(pop.get_f())

    for idx in ndf_simplified:
        f = ndf_simplified#pop.get_f()[idx]
        #print("f: "+str(f))
    print("NDF len:" + str(len(ndf_no_repeat)))
    print("Execution time for evolution: " + str(end-start))

    print(x_vals_in[-1])
    print(y_sim_vals[-1])

if __name__ == "__main__":
    main()
