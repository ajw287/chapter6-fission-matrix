#!/usr/bin/env python3
#
#                                                 Copyright 2019 Andrew Whyte
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#  FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import sys
#import random as rand
import numpy as np
import random
import matplotlib.pyplot as plt
import matplotlib.font_manager as fm



def print2Dlist(twoDlist):
    print ("[")
    for line in twoDlist:
        print (line)
    print("]")

def pareto_front(twoDlist):
# from:https://math.stackexchange.com/questions/101125/how-to-compute-the-pareto-frontier-intuitively-speaking
#
# We say that an alternative A dominates B if A outscores B
# regardless of the tradeoff between value and cost that is,
# if A is both better and cheaper than B.
#
#Algorithm A:
#1. Let i:=1
#2. Add Ai to the Pareto frontier.
#3. Find smallest j>i such that value(Aj)>value(Ai)
#4. If no such j exists, stop. Otherwise let i:=j and repeat from step 2.
    i = 1
    for line in twoDlist:
        line.append(i)
        i+=1
    sortedList = sorted(twoDlist,key=lambda x:(x[0],x[1]))
    y = sortedList[0][1]
    #sortedList[0].append("pareto")
    for line in sortedList:
        if line[1] > y:
            line.append("dominated")
        else:
            line.append("pareto")
            y = line[1]
    pareto = []
    dominated = []
    for line in sortedList:
        if (line[3] == "pareto"):
            pareto.append(line)
        else:
            dominated.append(line)#[:2])
    print("pareto:")
    print2Dlist(pareto)
    #print2Dlist(dominated)
    return pareto, dominated

def main():
    low = 0.0
    high= 3.0
    size= 20
    np.random.seed(22) # 6 is ok...
    #randoms = [[random.uniform(low, high),random.uniform(low, high)] for i in xrange(size)]
    randoms = [[np.random.normal(loc=5.0, scale=3.0, size=None),np.random.normal(loc=5.0, scale=3.0, size=None)] for i in range(size)]
    pareto, dominated = pareto_front (randoms)
    plt.xkcd()
    #plt.rcParams['font.size'] = 12
    #plt.rcParams['font.family'] = 'SedgwickAve-Regular.ttf'
    ax = plt.gca()
    # Change all the fonts to humor-sans.
    prop = fm.FontProperties(fname='SedgwickAve-Regular', size=16)
    x = [row[1] for row in dominated]
    y = [row[0] for row in dominated]
    plt.scatter(x,y, color='g', alpha=0.6, marker='o')
    x = [row[1] for row in pareto]
    y = [row[0] for row in pareto]
    plt.plot(x,y, color='r', alpha=.65, marker='x', linestyle='-')
    plt.ylabel('variable y to be minimised')
    plt.title("Demonstration of Pareto Fronts:")
    plt.xlabel('variable x to be minimised')
    for text in ax.texts:
        text.set_fontproperties(prop)
    plt.show()

if __name__ == "__main__":
    main()
