#!/usr/bin/env bash
cd ./code/figure-6-8
time python3 pygmo_micro_array_sep_ax.py figure-6-7b ../../data/6-9/200721-fmtx-burnup-ndf3/
cp figure-6-7b-basic.* ../../
echo "Output files generated in execution directory as SVG and PNG: 'figure-6-7b-basic'"
