
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0001' ;
WORKING_DIRECTORY         (idx, [1: 33])  = '/home/ajw287/serpent/fmtx_3453422' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Sun Apr 19 21:48:49 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Sun Apr 19 21:55:52 2020' ;

% Run parameters:

POP                       (idx, 1)        = 20000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1587329329 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.02051E+00  8.96149E-01  1.02872E+00  1.03071E+00  1.02391E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  3])  = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16890E-02 0.00066  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88311E-01 7.8E-06  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.81567E-01 7.4E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.81987E-01 7.4E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.76778E+00 0.00016  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.02083E+01 0.00029  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.02008E+01 0.00029  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.12075E+01 0.00029  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  5.15475E-01 0.00067  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 10000472 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  2.00009E+04 0.00053 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  2.00009E+04 0.00053 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.51311E+01 ;
RUNNING_TIME              (idx, 1)        =  7.04503E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  2.27667E-02  2.27667E-02 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.66668E-04  3.66668E-04 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.02190E+00  7.02190E+00  0.00000E+00 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.04488E+00  0.00000E+00 ];
CPU_USAGE                 (idx, 1)        = 4.98664 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99981E+00 9.6E-05 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.82718E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 370.78;
MEMSIZE                   (idx, 1)        = 288.50;
XS_MEMSIZE                (idx, 1)        = 127.27;
MAT_MEMSIZE               (idx, 1)        = 29.07;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 130.80;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 82.28;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 126702 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 5 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 15 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 15 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 484 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  0.00000E+00 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.99190E-05 0.00028  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.43654E-01 0.00071 ];
U235_FISS                 (idx, [1:   4]) = [  3.84793E-01 0.00042  9.32102E-01 0.00013 ];
U238_FISS                 (idx, [1:   4]) = [  2.79795E-02 0.00192  6.77735E-02 0.00184 ];
U235_CAPT                 (idx, [1:   4]) = [  7.86037E-02 0.00114  1.35646E-01 0.00109 ];
U238_CAPT                 (idx, [1:   4]) = [  2.96083E-01 0.00057  5.10942E-01 0.00039 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 10000472 1.00000E+07 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 1.46353E+04 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 10000472 1.00146E+07 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 5795864 5.80422E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 4129199 4.13498E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 75409 7.54308E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 10000472 1.00146E+07 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 2.64496E-07 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.33997E-11 0.00019 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.19091E-18 0.00019 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.01632E+00 0.00019 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.12821E-01 0.00019 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  5.79649E-01 0.00013 ];
TOT_ABSRATE               (idx, [1:   2]) = [  9.92469E-01 2.8E-05 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.98380E-01 0.00028 ];
TOT_FLUX                  (idx, [1:   2]) = [  5.13237E+01 0.00021 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  7.53064E-03 0.00369 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  4.01910E+01 0.00025 ];
INI_FMASS                 (idx, 1)        =  1.12516E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12516E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.66421E+00 0.00029 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.33980E-01 0.00023 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.97677E-01 0.00021 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.20362E+00 0.00023 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95147E-01 2.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97297E-01 1.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.02571E+00 0.00036 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.01797E+00 0.00036 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46189E+00 9.3E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02592E+02 1.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.01790E+00 0.00037  1.01083E+00 0.00036  7.13923E-03 0.00577 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.01781E+00 0.00019 ];
COL_KEFF                  (idx, [1:   2]) = [  1.01801E+00 0.00037 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.01781E+00 0.00019 ];
ABS_KINF                  (idx, [1:   2]) = [  1.02554E+00 0.00019 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.77339E+01 0.00014 ];
IMP_ALF                   (idx, [1:   2]) = [  1.77339E+01 5.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.98086E-07 0.00252 ];
IMP_EALF                  (idx, [1:   2]) = [  3.97573E-07 0.00102 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.23210E-01 0.00200 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.23476E-01 0.00082 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  7.02735E-03 0.00367  1.94766E-04 0.02212  1.12422E-03 0.00859  1.08560E-03 0.00936  3.20829E-03 0.00567  1.06940E-03 0.00958  3.45069E-04 0.01721 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.10164E-01 0.00886  1.23408E-02 0.00493  3.16368E-02 0.00015  1.10266E-01 0.00019  3.20964E-01 0.00016  1.34500E+00 0.00011  8.89379E+00 0.00229 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.02988E-03 0.00549  1.90872E-04 0.03443  1.11757E-03 0.01375  1.06979E-03 0.01432  3.22427E-03 0.00875  1.09294E-03 0.01428  3.34443E-04 0.02680 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.99728E-01 0.01353  1.24907E-02 1.9E-06  3.16258E-02 0.00023  1.10276E-01 0.00029  3.20932E-01 0.00024  1.34503E+00 0.00016  8.92213E+00 0.00165 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.87006E-05 0.00079  3.86775E-05 0.00079  4.20212E-05 0.00838 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.93906E-05 0.00070  3.93671E-05 0.00070  4.27706E-05 0.00838 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.03144E-03 0.00588  1.95947E-04 0.03591  1.12452E-03 0.01376  1.06294E-03 0.01509  3.19963E-03 0.00894  1.09819E-03 0.01410  3.50210E-04 0.02664 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.20839E-01 0.01349  1.24907E-02 2.4E-06  3.16319E-02 0.00025  1.10279E-01 0.00034  3.21035E-01 0.00025  1.34525E+00 0.00018  8.92821E+00 0.00193 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.86150E-05 0.00193  3.85909E-05 0.00193  4.17203E-05 0.01965 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.93029E-05 0.00188  3.92785E-05 0.00188  4.24625E-05 0.01965 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.04408E-03 0.01961  1.94459E-04 0.11575  1.12939E-03 0.04993  1.09342E-03 0.04977  3.20042E-03 0.02864  1.08942E-03 0.04841  3.36969E-04 0.08301 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.09812E-01 0.04329  1.24907E-02 5.7E-06  3.16281E-02 0.00071  1.10200E-01 0.00085  3.20773E-01 0.00081  1.34545E+00 0.00050  8.89012E+00 0.00403 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.01090E-03 0.01891  1.85475E-04 0.11408  1.10962E-03 0.04783  1.09510E-03 0.04893  3.19513E-03 0.02805  1.07837E-03 0.04657  3.47209E-04 0.08201 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.19976E-01 0.04288  1.24907E-02 5.7E-06  3.16307E-02 0.00069  1.10191E-01 0.00084  3.20801E-01 0.00080  1.34547E+00 0.00050  8.88734E+00 0.00401 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.83081E+02 0.01983 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.86494E-05 0.00053 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.93384E-05 0.00035 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.04852E-03 0.00333 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.82409E+02 0.00341 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  7.36357E-07 0.00036 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.92404E-06 0.00027  2.92383E-06 0.00028  2.95155E-06 0.00338 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  5.11801E-05 0.00047  5.11997E-05 0.00047  4.85307E-05 0.00516 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.95460E-01 0.00021  6.95247E-01 0.00022  7.30596E-01 0.00572 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02020E+01 0.00883 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.02008E+01 0.00029  4.03952E+01 0.00032 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.43040E+05 0.00235  5.80777E+05 0.00085  1.20567E+06 0.00067  1.30156E+06 0.00057  1.19912E+06 0.00046  1.29028E+06 0.00039  8.76192E+05 0.00050  7.75950E+05 0.00050  5.94101E+05 0.00071  4.85206E+05 0.00067  4.18608E+05 0.00063  3.77218E+05 0.00060  3.48206E+05 0.00064  3.31055E+05 0.00081  3.22689E+05 0.00050  2.78891E+05 0.00062  2.75460E+05 0.00062  2.73174E+05 0.00067  2.68472E+05 0.00066  5.24749E+05 0.00047  5.07227E+05 0.00053  3.67761E+05 0.00036  2.39237E+05 0.00067  2.76857E+05 0.00047  2.62206E+05 0.00051  2.40016E+05 0.00088  3.94691E+05 0.00058  9.01103E+04 0.00089  1.13121E+05 0.00109  1.02490E+05 0.00096  5.98103E+04 0.00146  1.03742E+05 0.00116  7.07117E+04 0.00159  6.04467E+04 0.00117  1.16046E+04 0.00230  1.15578E+04 0.00237  1.18511E+04 0.00235  1.21749E+04 0.00220  1.20915E+04 0.00250  1.18538E+04 0.00225  1.23228E+04 0.00222  1.15268E+04 0.00180  2.18136E+04 0.00182  3.48350E+04 0.00187  4.44293E+04 0.00135  1.16988E+05 0.00113  1.24472E+05 0.00095  1.39325E+05 0.00095  9.89997E+04 0.00104  7.62942E+04 0.00087  5.99457E+04 0.00125  7.22095E+04 0.00071  1.40468E+05 0.00090  1.93680E+05 0.00068  3.79739E+05 0.00071  5.94371E+05 0.00061  8.98639E+05 0.00058  5.77845E+05 0.00051  4.14723E+05 0.00073  2.98436E+05 0.00061  2.68344E+05 0.00072  2.64100E+05 0.00066  2.20876E+05 0.00087  1.49168E+05 0.00071  1.37837E+05 0.00077  1.22591E+05 0.00083  1.03746E+05 0.00085  8.15794E+04 0.00089  5.45992E+04 0.00101  1.92359E+04 0.00137 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.02575E+00 0.00041 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  3.76187E+01 0.00029  1.37058E+01 0.00034 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38324E-01 7.0E-05  1.45492E+00 0.00021 ];
INF_CAPT                  (idx, [1:   4]) = [  6.23759E-03 0.00051  2.51732E-02 0.00013 ];
INF_ABS                   (idx, [1:   4]) = [  8.00105E-03 0.00040  5.04545E-02 0.00031 ];
INF_FISS                  (idx, [1:   4]) = [  1.76346E-03 0.00031  2.52813E-02 0.00050 ];
INF_NSF                   (idx, [1:   4]) = [  4.57342E-03 0.00030  6.16030E-02 0.00050 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.59343E+00 3.7E-05  2.43670E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04276E+02 3.9E-06  2.02270E+02 3.8E-09 ];
INF_INVV                  (idx, [1:   4]) = [  6.04524E-08 0.00022  2.59157E-06 8.3E-05 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.30323E-01 7.0E-05  1.40449E+00 0.00023 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44156E-01 0.00013  3.65443E-01 0.00030 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62662E-02 0.00020  8.60749E-02 0.00073 ];
INF_SCATT3                (idx, [1:   4]) = [  7.41299E-03 0.00240  2.58811E-02 0.00156 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02965E-02 0.00137 -8.17454E-03 0.00358 ];
INF_SCATT5                (idx, [1:   4]) = [  1.13285E-04 0.13185  6.17069E-03 0.00482 ];
INF_SCATT6                (idx, [1:   4]) = [  5.04637E-03 0.00191 -1.57140E-02 0.00188 ];
INF_SCATT7                (idx, [1:   4]) = [  7.51942E-04 0.01251  3.96520E-04 0.06988 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.30361E-01 7.0E-05  1.40449E+00 0.00023 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44156E-01 0.00013  3.65443E-01 0.00030 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62661E-02 0.00020  8.60749E-02 0.00073 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.41283E-03 0.00240  2.58811E-02 0.00156 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02964E-02 0.00137 -8.17454E-03 0.00358 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.13177E-04 0.13195  6.17069E-03 0.00482 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.04640E-03 0.00190 -1.57140E-02 0.00188 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.51912E-04 0.01251  3.96520E-04 0.06988 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.14512E-01 0.00025  9.49280E-01 0.00017 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55392E+00 0.00025  3.51144E-01 0.00017 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  7.96221E-03 0.00041  5.04545E-02 0.00031 ];
INF_REMXS                 (idx, [1:   4]) = [  2.67818E-02 0.00015  5.13501E-02 0.00044 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11543E-01 6.9E-05  1.87800E-02 0.00021  9.12036E-04 0.00227  1.40357E+00 0.00023 ];
INF_S1                    (idx, [1:   8]) = [  2.38658E-01 0.00013  5.49778E-03 0.00061  3.86313E-04 0.00349  3.65057E-01 0.00030 ];
INF_S2                    (idx, [1:   8]) = [  9.78671E-02 0.00019 -1.60098E-03 0.00188  2.08644E-04 0.00540  8.58662E-02 0.00073 ];
INF_S3                    (idx, [1:   8]) = [  9.34066E-03 0.00187 -1.92767E-03 0.00118  7.42559E-05 0.01677  2.58069E-02 0.00157 ];
INF_S4                    (idx, [1:   8]) = [ -9.64191E-03 0.00146 -6.54578E-04 0.00226  2.40465E-07 1.00000 -8.17478E-03 0.00354 ];
INF_S5                    (idx, [1:   8]) = [  1.04473E-04 0.13919  8.81241E-06 0.18547 -3.10308E-05 0.02384  6.20172E-03 0.00481 ];
INF_S6                    (idx, [1:   8]) = [  5.19588E-03 0.00197 -1.49507E-04 0.01367 -3.80412E-05 0.01438 -1.56759E-02 0.00189 ];
INF_S7                    (idx, [1:   8]) = [  9.26322E-04 0.01004 -1.74380E-04 0.00750 -3.56129E-05 0.02130  4.32133E-04 0.06481 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11581E-01 6.9E-05  1.87800E-02 0.00021  9.12036E-04 0.00227  1.40357E+00 0.00023 ];
INF_SP1                   (idx, [1:   8]) = [  2.38658E-01 0.00013  5.49778E-03 0.00061  3.86313E-04 0.00349  3.65057E-01 0.00030 ];
INF_SP2                   (idx, [1:   8]) = [  9.78671E-02 0.00019 -1.60098E-03 0.00188  2.08644E-04 0.00540  8.58662E-02 0.00073 ];
INF_SP3                   (idx, [1:   8]) = [  9.34050E-03 0.00187 -1.92767E-03 0.00118  7.42559E-05 0.01677  2.58069E-02 0.00157 ];
INF_SP4                   (idx, [1:   8]) = [ -9.64182E-03 0.00146 -6.54578E-04 0.00226  2.40465E-07 1.00000 -8.17478E-03 0.00354 ];
INF_SP5                   (idx, [1:   8]) = [  1.04365E-04 0.13929  8.81241E-06 0.18547 -3.10308E-05 0.02384  6.20172E-03 0.00481 ];
INF_SP6                   (idx, [1:   8]) = [  5.19591E-03 0.00197 -1.49507E-04 0.01367 -3.80412E-05 0.01438 -1.56759E-02 0.00189 ];
INF_SP7                   (idx, [1:   8]) = [  9.26292E-04 0.01004 -1.74380E-04 0.00750 -3.56129E-05 0.02130  4.32133E-04 0.06481 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32870E-01 0.00050  1.00303E+00 0.00321 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34298E-01 0.00078  1.04984E+00 0.00368 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34334E-01 0.00059  1.05193E+00 0.00382 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.30035E-01 0.00059  9.19513E-01 0.00358 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43142E+00 0.00050  3.32407E-01 0.00320 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42271E+00 0.00078  3.17610E-01 0.00365 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42248E+00 0.00059  3.16988E-01 0.00385 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.44906E+00 0.00059  3.62622E-01 0.00356 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.02988E-03 0.00549  1.90872E-04 0.03443  1.11757E-03 0.01375  1.06979E-03 0.01432  3.22427E-03 0.00875  1.09294E-03 0.01428  3.34443E-04 0.02680 ];
LAMBDA                    (idx, [1:  14]) = [  7.99728E-01 0.01353  1.24907E-02 1.9E-06  3.16258E-02 0.00023  1.10276E-01 0.00029  3.20932E-01 0.00024  1.34503E+00 0.00016  8.92213E+00 0.00165 ];

