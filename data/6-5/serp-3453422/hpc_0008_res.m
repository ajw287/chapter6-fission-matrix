
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0008' ;
WORKING_DIRECTORY         (idx, [1: 33])  = '/home/ajw287/serpent/fmtx_3453422' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Sun Apr 19 22:37:46 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Sun Apr 19 22:44:41 2020' ;

% Run parameters:

POP                       (idx, 1)        = 20000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1587332266 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99573E-01  1.01172E+00  9.71059E-01  1.00271E+00  1.01494E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  3])  = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14569E-02 0.00072  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88543E-01 8.3E-06  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.87889E-01 7.8E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.88302E-01 7.8E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.75081E+00 0.00015  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.00172E+01 0.00030  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.00086E+01 0.00030  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.07440E+01 0.00028  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.96788E-01 0.00073  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 10000247 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  2.00005E+04 0.00055 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  2.00005E+04 0.00055 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.45135E+01 ;
RUNNING_TIME              (idx, 1)        =  6.92128E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  2.28667E-02  2.28667E-02 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.33333E-04  3.33333E-04 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.89790E+00  6.89790E+00  0.00000E+00 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  6.92097E+00  0.00000E+00 ];
CPU_USAGE                 (idx, 1)        = 4.98657 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00019E+00 8.8E-05 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81983E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 350.78;
MEMSIZE                   (idx, 1)        = 281.72;
XS_MEMSIZE                (idx, 1)        = 127.27;
MAT_MEMSIZE               (idx, 1)        = 22.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 130.80;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 69.06;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 126702 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 5 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 15 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 15 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 484 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  0.00000E+00 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.99104E-05 0.00026  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.06778E-01 0.00076 ];
U235_FISS                 (idx, [1:   4]) = [  3.92745E-01 0.00044  9.33934E-01 0.00012 ];
U238_FISS                 (idx, [1:   4]) = [  2.77352E-02 0.00188  6.59499E-02 0.00177 ];
U235_CAPT                 (idx, [1:   4]) = [  8.10167E-02 0.00107  1.41992E-01 0.00100 ];
U238_CAPT                 (idx, [1:   4]) = [  2.85382E-01 0.00060  5.00165E-01 0.00042 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 10000247 1.00000E+07 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 1.46228E+04 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 10000247 1.00146E+07 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 5707616 5.71597E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 4206880 4.21288E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 85751 8.57725E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 10000247 1.00146E+07 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 1.02445E-07 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.36461E-11 0.00019 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.21282E-18 0.00019 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.03482E+00 0.00019 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.20432E-01 0.00019 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  5.71007E-01 0.00014 ];
TOT_ABSRATE               (idx, [1:   2]) = [  9.91438E-01 3.1E-05 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.98207E-01 0.00026 ];
TOT_FLUX                  (idx, [1:   2]) = [  5.08631E+01 0.00020 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  8.56173E-03 0.00356 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.99907E+01 0.00026 ];
INI_FMASS                 (idx, 1)        =  1.12516E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12516E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.69217E+00 0.00029 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.33560E-01 0.00025 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.95953E-01 0.00020 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.21069E+00 0.00022 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94478E-01 2.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96928E-01 1.8E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.04588E+00 0.00040 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.03691E+00 0.00040 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46132E+00 9.2E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02583E+02 1.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03702E+00 0.00041  1.02960E+00 0.00041  7.30784E-03 0.00549 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.03631E+00 0.00019 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03672E+00 0.00035 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.03631E+00 0.00019 ];
ABS_KINF                  (idx, [1:   2]) = [  1.04528E+00 0.00019 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.77040E+01 0.00013 ];
IMP_ALF                   (idx, [1:   2]) = [  1.77052E+01 5.5E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.10033E-07 0.00224 ];
IMP_EALF                  (idx, [1:   2]) = [  4.09117E-07 0.00097 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.18370E-01 0.00195 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.18551E-01 0.00082 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.93049E-03 0.00385  1.98735E-04 0.02189  1.11091E-03 0.00941  1.07877E-03 0.00914  3.16828E-03 0.00550  1.02783E-03 0.00969  3.45975E-04 0.01670 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.12589E-01 0.00881  1.22160E-02 0.00671  3.16438E-02 0.00015  1.10294E-01 0.00020  3.20837E-01 0.00014  1.34535E+00 0.00012  8.87636E+00 0.00229 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.10871E-03 0.00563  2.02942E-04 0.03342  1.15548E-03 0.01381  1.08243E-03 0.01354  3.26617E-03 0.00804  1.04191E-03 0.01418  3.59773E-04 0.02654 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.16475E-01 0.01419  1.24908E-02 2.4E-06  3.16487E-02 0.00022  1.10344E-01 0.00032  3.20884E-01 0.00023  1.34517E+00 0.00018  8.89088E+00 0.00151 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.62871E-05 0.00080  3.62689E-05 0.00081  3.89121E-05 0.00818 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.76271E-05 0.00068  3.76082E-05 0.00068  4.03509E-05 0.00818 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.03242E-03 0.00569  2.00768E-04 0.03585  1.13089E-03 0.01448  1.10179E-03 0.01474  3.21726E-03 0.00815  1.03457E-03 0.01597  3.47155E-04 0.02650 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.04546E-01 0.01386  1.24908E-02 2.5E-06  3.16405E-02 0.00024  1.10365E-01 0.00036  3.20895E-01 0.00024  1.34518E+00 0.00019  8.89021E+00 0.00183 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.64461E-05 0.00181  3.64300E-05 0.00182  3.85039E-05 0.02165 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.77931E-05 0.00180  3.77765E-05 0.00181  3.99207E-05 0.02166 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.01832E-03 0.01950  1.92014E-04 0.12221  1.20242E-03 0.04807  1.06170E-03 0.04579  3.19440E-03 0.02753  1.04974E-03 0.05115  3.18042E-04 0.08663 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.77963E-01 0.04359  1.24907E-02 5.7E-06  3.16337E-02 0.00070  1.10253E-01 0.00088  3.20792E-01 0.00075  1.34520E+00 0.00050  8.91660E+00 0.00422 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.00096E-03 0.01897  1.92847E-04 0.12069  1.18513E-03 0.04783  1.07265E-03 0.04408  3.21118E-03 0.02698  1.02858E-03 0.04989  3.10579E-04 0.08339 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.70379E-01 0.04175  1.24907E-02 5.7E-06  3.16322E-02 0.00070  1.10265E-01 0.00087  3.20808E-01 0.00074  1.34525E+00 0.00049  8.91801E+00 0.00422 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.92994E+02 0.01965 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.64023E-05 0.00052 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.77469E-05 0.00034 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.98919E-03 0.00347 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.92015E+02 0.00348 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  7.28070E-07 0.00036 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.90183E-06 0.00030  2.90161E-06 0.00030  2.93370E-06 0.00333 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  5.02768E-05 0.00045  5.02980E-05 0.00045  4.74181E-05 0.00518 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.93404E-01 0.00020  6.93112E-01 0.00021  7.40899E-01 0.00586 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03957E+01 0.00869 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.00086E+01 0.00030  3.95611E+01 0.00031 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.43714E+05 0.00272  5.81035E+05 0.00103  1.20420E+06 0.00057  1.29653E+06 0.00062  1.19732E+06 0.00049  1.28611E+06 0.00043  8.73208E+05 0.00054  7.72543E+05 0.00069  5.91146E+05 0.00046  4.82731E+05 0.00058  4.16106E+05 0.00049  3.75092E+05 0.00065  3.46778E+05 0.00043  3.29268E+05 0.00052  3.20828E+05 0.00069  2.77358E+05 0.00044  2.73927E+05 0.00066  2.71577E+05 0.00071  2.66830E+05 0.00053  5.21320E+05 0.00046  5.04238E+05 0.00055  3.64804E+05 0.00051  2.37339E+05 0.00055  2.74790E+05 0.00076  2.60317E+05 0.00077  2.37969E+05 0.00079  3.91721E+05 0.00048  8.92706E+04 0.00131  1.12273E+05 0.00099  1.01784E+05 0.00091  5.92827E+04 0.00137  1.02865E+05 0.00117  7.00965E+04 0.00097  5.98101E+04 0.00158  1.15324E+04 0.00284  1.14270E+04 0.00284  1.16181E+04 0.00247  1.20338E+04 0.00256  1.19777E+04 0.00343  1.17299E+04 0.00255  1.21857E+04 0.00277  1.14347E+04 0.00284  2.15405E+04 0.00268  3.44698E+04 0.00180  4.39168E+04 0.00114  1.15853E+05 0.00120  1.22882E+05 0.00099  1.37042E+05 0.00100  9.74259E+04 0.00102  7.49588E+04 0.00122  5.86975E+04 0.00115  7.07399E+04 0.00095  1.37497E+05 0.00085  1.89815E+05 0.00068  3.71620E+05 0.00059  5.81571E+05 0.00064  8.79303E+05 0.00062  5.65256E+05 0.00048  4.06009E+05 0.00068  2.92472E+05 0.00067  2.62772E+05 0.00088  2.58951E+05 0.00080  2.16190E+05 0.00072  1.46220E+05 0.00079  1.34864E+05 0.00080  1.20041E+05 0.00096  1.01588E+05 0.00079  7.98579E+04 0.00101  5.34736E+04 0.00094  1.89253E+04 0.00132 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.04568E+00 0.00045 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  3.74425E+01 0.00031  1.34213E+01 0.00043 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38795E-01 6.3E-05  1.47659E+00 0.00027 ];
INF_CAPT                  (idx, [1:   4]) = [  6.21882E-03 0.00051  2.51970E-02 0.00015 ];
INF_ABS                   (idx, [1:   4]) = [  8.07809E-03 0.00042  5.13370E-02 0.00035 ];
INF_FISS                  (idx, [1:   4]) = [  1.85927E-03 0.00044  2.61400E-02 0.00056 ];
INF_NSF                   (idx, [1:   4]) = [  4.80694E-03 0.00045  6.36954E-02 0.00056 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.58539E+00 4.1E-05  2.43670E+00 2.7E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04161E+02 4.9E-06  2.02270E+02 5.4E-09 ];
INF_INVV                  (idx, [1:   4]) = [  6.02052E-08 0.00026  2.59131E-06 0.00010 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.30719E-01 6.3E-05  1.42527E+00 0.00029 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44941E-01 0.00014  3.72201E-01 0.00034 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65453E-02 0.00016  8.77455E-02 0.00053 ];
INF_SCATT3                (idx, [1:   4]) = [  7.37639E-03 0.00228  2.63481E-02 0.00140 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03767E-02 0.00129 -8.37531E-03 0.00328 ];
INF_SCATT5                (idx, [1:   4]) = [  8.03243E-05 0.10283  6.27463E-03 0.00458 ];
INF_SCATT6                (idx, [1:   4]) = [  5.03251E-03 0.00161 -1.60969E-02 0.00155 ];
INF_SCATT7                (idx, [1:   4]) = [  7.24863E-04 0.01137  4.00103E-04 0.07863 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.30758E-01 6.3E-05  1.42527E+00 0.00029 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44942E-01 0.00014  3.72201E-01 0.00034 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65455E-02 0.00016  8.77455E-02 0.00053 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.37636E-03 0.00228  2.63481E-02 0.00140 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03767E-02 0.00129 -8.37531E-03 0.00328 ];
INF_SCATTP5               (idx, [1:   4]) = [  8.03571E-05 0.10264  6.27463E-03 0.00458 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.03263E-03 0.00162 -1.60969E-02 0.00155 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.24888E-04 0.01139  4.00103E-04 0.07863 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13972E-01 0.00016  9.60034E-01 0.00024 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55784E+00 0.00016  3.47210E-01 0.00024 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.03911E-03 0.00042  5.13370E-02 0.00035 ];
INF_REMXS                 (idx, [1:   4]) = [  2.68837E-02 0.00016  5.22441E-02 0.00037 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11911E-01 6.1E-05  1.88084E-02 0.00026  9.26579E-04 0.00251  1.42435E+00 0.00029 ];
INF_S1                    (idx, [1:   8]) = [  2.39432E-01 0.00014  5.50829E-03 0.00059  3.92808E-04 0.00379  3.71808E-01 0.00034 ];
INF_S2                    (idx, [1:   8]) = [  9.81569E-02 0.00016 -1.61164E-03 0.00159  2.14030E-04 0.00664  8.75315E-02 0.00053 ];
INF_S3                    (idx, [1:   8]) = [  9.30816E-03 0.00173 -1.93177E-03 0.00137  7.64099E-05 0.01478  2.62717E-02 0.00141 ];
INF_S4                    (idx, [1:   8]) = [ -9.72826E-03 0.00139 -6.48427E-04 0.00358 -4.24993E-07 1.00000 -8.37488E-03 0.00326 ];
INF_S5                    (idx, [1:   8]) = [  6.94761E-05 0.11623  1.08482E-05 0.18712 -3.09996E-05 0.02909  6.30563E-03 0.00455 ];
INF_S6                    (idx, [1:   8]) = [  5.18474E-03 0.00154 -1.52231E-04 0.01056 -3.93172E-05 0.01953 -1.60576E-02 0.00154 ];
INF_S7                    (idx, [1:   8]) = [  9.01668E-04 0.00892 -1.76805E-04 0.00979 -3.51011E-05 0.02192  4.35204E-04 0.07212 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11950E-01 6.1E-05  1.88084E-02 0.00026  9.26579E-04 0.00251  1.42435E+00 0.00029 ];
INF_SP1                   (idx, [1:   8]) = [  2.39433E-01 0.00014  5.50829E-03 0.00059  3.92808E-04 0.00379  3.71808E-01 0.00034 ];
INF_SP2                   (idx, [1:   8]) = [  9.81572E-02 0.00016 -1.61164E-03 0.00159  2.14030E-04 0.00664  8.75315E-02 0.00053 ];
INF_SP3                   (idx, [1:   8]) = [  9.30813E-03 0.00173 -1.93177E-03 0.00137  7.64099E-05 0.01478  2.62717E-02 0.00141 ];
INF_SP4                   (idx, [1:   8]) = [ -9.72827E-03 0.00139 -6.48427E-04 0.00358 -4.24993E-07 1.00000 -8.37488E-03 0.00326 ];
INF_SP5                   (idx, [1:   8]) = [  6.95089E-05 0.11614  1.08482E-05 0.18712 -3.09996E-05 0.02909  6.30563E-03 0.00455 ];
INF_SP6                   (idx, [1:   8]) = [  5.18486E-03 0.00155 -1.52231E-04 0.01056 -3.93172E-05 0.01953 -1.60576E-02 0.00154 ];
INF_SP7                   (idx, [1:   8]) = [  9.01694E-04 0.00894 -1.76805E-04 0.00979 -3.51011E-05 0.02192  4.35204E-04 0.07212 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.33208E-01 0.00044  1.03378E+00 0.00370 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34885E-01 0.00056  1.08837E+00 0.00417 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34712E-01 0.00060  1.08972E+00 0.00494 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.30092E-01 0.00068  9.38768E-01 0.00348 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.42935E+00 0.00044  3.22548E-01 0.00369 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.41914E+00 0.00056  3.06397E-01 0.00420 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42019E+00 0.00060  3.06067E-01 0.00493 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.44871E+00 0.00068  3.55179E-01 0.00349 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.10871E-03 0.00563  2.02942E-04 0.03342  1.15548E-03 0.01381  1.08243E-03 0.01354  3.26617E-03 0.00804  1.04191E-03 0.01418  3.59773E-04 0.02654 ];
LAMBDA                    (idx, [1:  14]) = [  8.16475E-01 0.01419  1.24908E-02 2.4E-06  3.16487E-02 0.00022  1.10344E-01 0.00032  3.20884E-01 0.00023  1.34517E+00 0.00018  8.89088E+00 0.00151 ];

