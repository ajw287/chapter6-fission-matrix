
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0006' ;
WORKING_DIRECTORY         (idx, [1: 33])  = '/home/ajw287/serpent/fmtx_3453422' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Sun Apr 19 22:23:43 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Sun Apr 19 22:30:48 2020' ;

% Run parameters:

POP                       (idx, 1)        = 20000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1587331423 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.69829E-01  1.01029E+00  1.02701E+00  1.02065E+00  9.72215E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  3])  = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.20307E-02 0.00066  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.87969E-01 8.0E-06  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.74498E-01 6.9E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.74932E-01 6.9E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.78308E+00 0.00016  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.95499E+01 0.00028  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.95439E+01 0.00028  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.14848E+01 0.00029  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  5.27963E-01 0.00071  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 10001423 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  2.00028E+04 0.00053 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  2.00028E+04 0.00053 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.53267E+01 ;
RUNNING_TIME              (idx, 1)        =  7.08612E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  2.52500E-02  2.52500E-02 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.16664E-04  3.16664E-04 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.06055E+00  7.06055E+00  0.00000E+00 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.08598E+00  0.00000E+00 ];
CPU_USAGE                 (idx, 1)        = 4.98534 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00008E+00 8.7E-05 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80523E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 350.78;
MEMSIZE                   (idx, 1)        = 281.72;
XS_MEMSIZE                (idx, 1)        = 127.27;
MAT_MEMSIZE               (idx, 1)        = 22.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 130.80;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 69.06;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 126702 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 5 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 15 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 15 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 484 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  0.00000E+00 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.99476E-05 0.00027  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.59041E-01 0.00072 ];
U235_FISS                 (idx, [1:   4]) = [  3.85957E-01 0.00044  9.31436E-01 0.00014 ];
U238_FISS                 (idx, [1:   4]) = [  2.83636E-02 0.00205  6.84448E-02 0.00193 ];
U235_CAPT                 (idx, [1:   4]) = [  7.86455E-02 0.00110  1.35585E-01 0.00106 ];
U238_CAPT                 (idx, [1:   4]) = [  3.03941E-01 0.00059  5.23982E-01 0.00041 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 10001423 1.00000E+07 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 1.48478E+04 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 10001423 1.00148E+07 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 5798625 5.80663E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 4142658 4.14806E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 60140 6.01545E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 10001423 1.00148E+07 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 1.17347E-07 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.34423E-11 0.00018 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.19470E-18 0.00018 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.01963E+00 0.00018 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.14127E-01 0.00018 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  5.79864E-01 0.00012 ];
TOT_ABSRATE               (idx, [1:   2]) = [  9.93991E-01 2.6E-05 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.98952E-01 0.00027 ];
TOT_FLUX                  (idx, [1:   2]) = [  5.12764E+01 0.00020 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  6.00901E-03 0.00427 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.95574E+01 0.00022 ];
INI_FMASS                 (idx, 1)        =  1.12516E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12516E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.65385E+00 0.00030 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.44182E-01 0.00022 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.94683E-01 0.00020 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.20175E+00 0.00023 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.96099E-01 2.0E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97877E-01 1.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.02746E+00 0.00037 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.02128E+00 0.00037 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46212E+00 9.6E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02596E+02 1.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.02122E+00 0.00038  1.01408E+00 0.00037  7.19668E-03 0.00579 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.02115E+00 0.00018 ];
COL_KEFF                  (idx, [1:   2]) = [  1.02074E+00 0.00035 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.02115E+00 0.00018 ];
ABS_KINF                  (idx, [1:   2]) = [  1.02733E+00 0.00017 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.77444E+01 0.00014 ];
IMP_ALF                   (idx, [1:   2]) = [  1.77449E+01 5.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.93951E-07 0.00253 ];
IMP_EALF                  (idx, [1:   2]) = [  3.93222E-07 0.00101 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.24894E-01 0.00210 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.25424E-01 0.00083 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  7.08123E-03 0.00389  2.03296E-04 0.02253  1.11734E-03 0.00951  1.09894E-03 0.00935  3.24590E-03 0.00588  1.06391E-03 0.00954  3.51847E-04 0.01672 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.13793E-01 0.00882  1.22160E-02 0.00671  3.16349E-02 0.00015  1.10269E-01 0.00020  3.20897E-01 0.00016  1.34504E+00 0.00011  8.90417E+00 0.00107 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.09975E-03 0.00563  1.99040E-04 0.03358  1.10909E-03 0.01396  1.11228E-03 0.01393  3.28556E-03 0.00830  1.04771E-03 0.01512  3.46078E-04 0.02606 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.01665E-01 0.01351  1.24908E-02 2.2E-06  3.16356E-02 0.00024  1.10282E-01 0.00030  3.20918E-01 0.00024  1.34458E+00 0.00018  8.90241E+00 0.00154 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.88225E-05 0.00082  3.88038E-05 0.00082  4.13434E-05 0.00795 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.96433E-05 0.00072  3.96242E-05 0.00072  4.22175E-05 0.00794 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.03425E-03 0.00595  1.95678E-04 0.03733  1.08733E-03 0.01478  1.09449E-03 0.01444  3.24963E-03 0.00891  1.04414E-03 0.01517  3.62991E-04 0.02603 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.27914E-01 0.01376  1.24908E-02 2.5E-06  3.16469E-02 0.00027  1.10286E-01 0.00031  3.20876E-01 0.00025  1.34492E+00 0.00020  8.92731E+00 0.00190 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.83477E-05 0.00193  3.83247E-05 0.00195  4.16204E-05 0.02527 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.91584E-05 0.00188  3.91349E-05 0.00190  4.25012E-05 0.02524 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.91825E-03 0.01978  1.94847E-04 0.10514  1.01133E-03 0.04960  1.08312E-03 0.05163  3.31395E-03 0.02900  1.01646E-03 0.05033  2.98540E-04 0.08723 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.57468E-01 0.04498  1.24907E-02 5.1E-06  3.16464E-02 0.00067  1.10403E-01 0.00093  3.20728E-01 0.00078  1.34451E+00 0.00053  8.89560E+00 0.00423 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.90232E-03 0.01952  1.98447E-04 0.10585  1.02476E-03 0.04840  1.07773E-03 0.04969  3.28369E-03 0.02868  1.00942E-03 0.04940  3.08265E-04 0.08589 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.65724E-01 0.04443  1.24907E-02 5.1E-06  3.16373E-02 0.00068  1.10414E-01 0.00092  3.20759E-01 0.00078  1.34436E+00 0.00053  8.89429E+00 0.00419 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.80846E+02 0.01988 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.85921E-05 0.00051 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.94080E-05 0.00032 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.01445E-03 0.00368 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.81782E+02 0.00372 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  7.22745E-07 0.00035 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.95165E-06 0.00029  2.95151E-06 0.00029  2.96899E-06 0.00322 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  5.02613E-05 0.00045  5.02775E-05 0.00044  4.81176E-05 0.00512 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.92981E-01 0.00020  6.92723E-01 0.00020  7.35156E-01 0.00604 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02341E+01 0.00911 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.95439E+01 0.00028  4.04773E+01 0.00034 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.43860E+05 0.00221  5.81169E+05 0.00105  1.20688E+06 0.00067  1.30257E+06 0.00052  1.20298E+06 0.00049  1.29698E+06 0.00048  8.80906E+05 0.00053  7.80987E+05 0.00063  5.97918E+05 0.00065  4.88338E+05 0.00056  4.21243E+05 0.00060  3.79464E+05 0.00074  3.50592E+05 0.00055  3.33465E+05 0.00067  3.24671E+05 0.00044  2.80648E+05 0.00068  2.77258E+05 0.00070  2.74822E+05 0.00068  2.70273E+05 0.00064  5.28065E+05 0.00047  5.10511E+05 0.00048  3.69631E+05 0.00076  2.40731E+05 0.00057  2.78493E+05 0.00059  2.63960E+05 0.00046  2.41204E+05 0.00065  3.96791E+05 0.00046  9.07887E+04 0.00079  1.13806E+05 0.00104  1.03143E+05 0.00095  6.00599E+04 0.00109  1.04456E+05 0.00117  7.10635E+04 0.00118  6.08177E+04 0.00128  1.17338E+04 0.00276  1.15752E+04 0.00318  1.18756E+04 0.00264  1.22330E+04 0.00269  1.21459E+04 0.00279  1.19268E+04 0.00223  1.23740E+04 0.00216  1.15658E+04 0.00268  2.18896E+04 0.00199  3.50259E+04 0.00166  4.44676E+04 0.00111  1.17827E+05 0.00098  1.25177E+05 0.00099  1.39818E+05 0.00083  9.90817E+04 0.00097  7.61739E+04 0.00106  5.97174E+04 0.00104  7.19416E+04 0.00118  1.39305E+05 0.00096  1.91653E+05 0.00087  3.74155E+05 0.00074  5.83870E+05 0.00068  8.80845E+05 0.00059  5.65211E+05 0.00071  4.05336E+05 0.00086  2.91639E+05 0.00082  2.61727E+05 0.00069  2.57822E+05 0.00086  2.15171E+05 0.00078  1.45386E+05 0.00100  1.34167E+05 0.00109  1.19320E+05 0.00090  1.00884E+05 0.00105  7.92319E+04 0.00110  5.29445E+04 0.00123  1.86442E+04 0.00158 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.02692E+00 0.00032 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  3.78183E+01 0.00027  1.34589E+01 0.00042 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37485E-01 6.6E-05  1.42891E+00 0.00023 ];
INF_CAPT                  (idx, [1:   4]) = [  6.31134E-03 0.00045  2.53512E-02 0.00012 ];
INF_ABS                   (idx, [1:   4]) = [  8.05189E-03 0.00038  5.12315E-02 0.00032 ];
INF_FISS                  (idx, [1:   4]) = [  1.74055E-03 0.00028  2.58803E-02 0.00053 ];
INF_NSF                   (idx, [1:   4]) = [  4.51955E-03 0.00028  6.30626E-02 0.00053 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.59662E+00 5.1E-05  2.43670E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04320E+02 4.1E-06  2.02270E+02 2.7E-09 ];
INF_INVV                  (idx, [1:   4]) = [  6.05093E-08 0.00023  2.58360E-06 0.00011 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29429E-01 6.9E-05  1.37766E+00 0.00026 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42875E-01 0.00012  3.57465E-01 0.00030 ];
INF_SCATT2                (idx, [1:   4]) = [  9.57920E-02 0.00017  8.44386E-02 0.00049 ];
INF_SCATT3                (idx, [1:   4]) = [  7.41626E-03 0.00215  2.53551E-02 0.00137 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02258E-02 0.00117 -7.96417E-03 0.00362 ];
INF_SCATT5                (idx, [1:   4]) = [  1.14410E-04 0.08756  5.93373E-03 0.00376 ];
INF_SCATT6                (idx, [1:   4]) = [  5.00359E-03 0.00186 -1.52846E-02 0.00206 ];
INF_SCATT7                (idx, [1:   4]) = [  7.44096E-04 0.01545  3.98933E-04 0.06540 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29469E-01 6.9E-05  1.37766E+00 0.00026 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42876E-01 0.00012  3.57465E-01 0.00030 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.57921E-02 0.00017  8.44386E-02 0.00049 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.41644E-03 0.00216  2.53551E-02 0.00137 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02258E-02 0.00117 -7.96417E-03 0.00362 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.14330E-04 0.08771  5.93373E-03 0.00376 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.00375E-03 0.00185 -1.52846E-02 0.00206 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.44213E-04 0.01548  3.98933E-04 0.06540 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.15236E-01 0.00020  9.35724E-01 0.00021 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.54869E+00 0.00020  3.56231E-01 0.00021 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.01267E-03 0.00038  5.12315E-02 0.00032 ];
INF_REMXS                 (idx, [1:   4]) = [  2.66863E-02 0.00012  5.21942E-02 0.00052 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10799E-01 6.6E-05  1.86308E-02 0.00025  9.43088E-04 0.00307  1.37671E+00 0.00026 ];
INF_S1                    (idx, [1:   8]) = [  2.37422E-01 0.00012  5.45372E-03 0.00065  3.94112E-04 0.00501  3.57071E-01 0.00030 ];
INF_S2                    (idx, [1:   8]) = [  9.73783E-02 0.00017 -1.58626E-03 0.00121  2.13231E-04 0.00582  8.42253E-02 0.00049 ];
INF_S3                    (idx, [1:   8]) = [  9.32431E-03 0.00167 -1.90806E-03 0.00138  7.56811E-05 0.01271  2.52794E-02 0.00138 ];
INF_S4                    (idx, [1:   8]) = [ -9.58133E-03 0.00111 -6.44468E-04 0.00300 -5.37777E-07 1.00000 -7.96364E-03 0.00363 ];
INF_S5                    (idx, [1:   8]) = [  1.03304E-04 0.09131  1.11059E-05 0.19872 -3.13314E-05 0.02402  5.96506E-03 0.00373 ];
INF_S6                    (idx, [1:   8]) = [  5.15337E-03 0.00188 -1.49776E-04 0.00981 -3.92633E-05 0.02589 -1.52454E-02 0.00207 ];
INF_S7                    (idx, [1:   8]) = [  9.19165E-04 0.01276 -1.75069E-04 0.00952 -3.52062E-05 0.02247  4.34140E-04 0.05970 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10838E-01 6.6E-05  1.86308E-02 0.00025  9.43088E-04 0.00307  1.37671E+00 0.00026 ];
INF_SP1                   (idx, [1:   8]) = [  2.37422E-01 0.00012  5.45372E-03 0.00065  3.94112E-04 0.00501  3.57071E-01 0.00030 ];
INF_SP2                   (idx, [1:   8]) = [  9.73783E-02 0.00017 -1.58626E-03 0.00121  2.13231E-04 0.00582  8.42253E-02 0.00049 ];
INF_SP3                   (idx, [1:   8]) = [  9.32450E-03 0.00167 -1.90806E-03 0.00138  7.56811E-05 0.01271  2.52794E-02 0.00138 ];
INF_SP4                   (idx, [1:   8]) = [ -9.58132E-03 0.00111 -6.44468E-04 0.00300 -5.37777E-07 1.00000 -7.96364E-03 0.00363 ];
INF_SP5                   (idx, [1:   8]) = [  1.03224E-04 0.09146  1.11059E-05 0.19872 -3.13314E-05 0.02402  5.96506E-03 0.00373 ];
INF_SP6                   (idx, [1:   8]) = [  5.15352E-03 0.00188 -1.49776E-04 0.00981 -3.92633E-05 0.02589 -1.52454E-02 0.00207 ];
INF_SP7                   (idx, [1:   8]) = [  9.19282E-04 0.01279 -1.75069E-04 0.00952 -3.52062E-05 0.02247  4.34140E-04 0.05970 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32755E-01 0.00034  9.70946E-01 0.00310 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34046E-01 0.00062  1.00827E+00 0.00381 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33897E-01 0.00064  1.01269E+00 0.00368 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.30363E-01 0.00063  9.00704E-01 0.00342 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43213E+00 0.00034  3.43386E-01 0.00307 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42424E+00 0.00062  3.30712E-01 0.00374 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42514E+00 0.00064  3.29262E-01 0.00365 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.44701E+00 0.00063  3.70185E-01 0.00341 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.09975E-03 0.00563  1.99040E-04 0.03358  1.10909E-03 0.01396  1.11228E-03 0.01393  3.28556E-03 0.00830  1.04771E-03 0.01512  3.46078E-04 0.02606 ];
LAMBDA                    (idx, [1:  14]) = [  8.01665E-01 0.01351  1.24908E-02 2.2E-06  3.16356E-02 0.00024  1.10282E-01 0.00030  3.20918E-01 0.00024  1.34458E+00 0.00018  8.90241E+00 0.00154 ];

