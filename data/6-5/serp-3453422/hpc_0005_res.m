
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0005' ;
WORKING_DIRECTORY         (idx, [1: 33])  = '/home/ajw287/serpent/fmtx_3453422' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Sun Apr 19 22:16:44 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Sun Apr 19 22:23:43 2020' ;

% Run parameters:

POP                       (idx, 1)        = 20000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1587331004 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.23281E-01  1.02371E+00  1.01263E+00  1.02658E+00  1.01379E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  3])  = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 6.6E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14271E-02 0.00069  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88573E-01 8.0E-06  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.88757E-01 7.7E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.89171E-01 7.7E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.74986E+00 0.00015  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.98768E+01 0.00031  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.98681E+01 0.00031  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.06507E+01 0.00029  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.92659E-01 0.00071  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 10000803 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  2.00016E+04 0.00053 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  2.00016E+04 0.00053 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.47751E+01 ;
RUNNING_TIME              (idx, 1)        =  6.97588E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  2.52833E-02  2.52833E-02 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.33333E-04  3.33333E-04 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.95025E+00  6.95025E+00  0.00000E+00 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  6.97573E+00  0.00000E+00 ];
CPU_USAGE                 (idx, 1)        = 4.98504 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99974E+00 8.1E-05 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79171E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 370.78;
MEMSIZE                   (idx, 1)        = 288.50;
XS_MEMSIZE                (idx, 1)        = 127.27;
MAT_MEMSIZE               (idx, 1)        = 29.07;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 130.80;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 82.28;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 126702 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 5 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 15 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 15 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 484 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  0.00000E+00 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.99184E-05 0.00027  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.99766E-01 0.00078 ];
U235_FISS                 (idx, [1:   4]) = [  3.94899E-01 0.00043  9.34514E-01 0.00013 ];
U238_FISS                 (idx, [1:   4]) = [  2.76227E-02 0.00193  6.53639E-02 0.00182 ];
U235_CAPT                 (idx, [1:   4]) = [  8.18445E-02 0.00104  1.43944E-01 0.00102 ];
U238_CAPT                 (idx, [1:   4]) = [  2.83850E-01 0.00062  4.99203E-01 0.00044 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 10000803 1.00000E+07 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 1.45647E+04 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 10000803 1.00146E+07 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 5687428 5.69529E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 4226790 4.23266E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 86585 8.66109E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 10000803 1.00146E+07 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 4.33996E-07 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.37237E-11 0.00021 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.21971E-18 0.00021 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.04065E+00 0.00020 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.22825E-01 0.00021 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  5.68528E-01 0.00015 ];
TOT_ABSRATE               (idx, [1:   2]) = [  9.91353E-01 3.0E-05 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.98368E-01 0.00027 ];
TOT_FLUX                  (idx, [1:   2]) = [  5.07443E+01 0.00020 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  8.64682E-03 0.00344 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.98561E+01 0.00025 ];
INI_FMASS                 (idx, 1)        =  1.12516E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12516E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.69731E+00 0.00029 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.35172E-01 0.00024 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.95528E-01 0.00020 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.21081E+00 0.00022 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94396E-01 2.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96926E-01 1.8E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.05082E+00 0.00036 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04172E+00 0.00036 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46119E+00 9.8E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02581E+02 1.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04176E+00 0.00037  1.03441E+00 0.00036  7.30764E-03 0.00592 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.04216E+00 0.00020 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04239E+00 0.00035 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.04216E+00 0.00020 ];
ABS_KINF                  (idx, [1:   2]) = [  1.05126E+00 0.00020 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.77078E+01 0.00013 ];
IMP_ALF                   (idx, [1:   2]) = [  1.77053E+01 6.0E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.08526E-07 0.00233 ];
IMP_EALF                  (idx, [1:   2]) = [  4.09101E-07 0.00105 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.17021E-01 0.00199 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.17425E-01 0.00088 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.89090E-03 0.00390  1.94815E-04 0.02189  1.10198E-03 0.00935  1.07940E-03 0.00876  3.14761E-03 0.00593  1.02443E-03 0.00968  3.42662E-04 0.01628 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.10944E-01 0.00840  1.23409E-02 0.00493  3.16389E-02 0.00015  1.10222E-01 0.00020  3.20785E-01 0.00016  1.34516E+00 0.00012  8.86767E+00 0.00100 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.16017E-03 0.00593  2.09033E-04 0.03393  1.13868E-03 0.01471  1.12238E-03 0.01398  3.25082E-03 0.00880  1.08072E-03 0.01514  3.58541E-04 0.02543 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.13050E-01 0.01289  1.24908E-02 2.5E-06  3.16412E-02 0.00023  1.10256E-01 0.00031  3.20700E-01 0.00024  1.34507E+00 0.00017  8.86893E+00 0.00144 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.56664E-05 0.00080  3.56497E-05 0.00080  3.80472E-05 0.00798 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.71529E-05 0.00069  3.71355E-05 0.00069  3.96351E-05 0.00799 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.00965E-03 0.00609  2.03953E-04 0.03446  1.12066E-03 0.01461  1.12517E-03 0.01439  3.18236E-03 0.00931  1.03996E-03 0.01598  3.37549E-04 0.02735 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.94481E-01 0.01408  1.24908E-02 2.6E-06  3.16356E-02 0.00026  1.10274E-01 0.00036  3.20814E-01 0.00025  1.34521E+00 0.00019  8.86939E+00 0.00172 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.57946E-05 0.00190  3.57837E-05 0.00191  3.75169E-05 0.02114 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.72859E-05 0.00184  3.72746E-05 0.00186  3.90758E-05 0.02112 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.94446E-03 0.01755  1.90943E-04 0.10564  1.10657E-03 0.04541  1.11537E-03 0.04702  3.19709E-03 0.02698  9.67922E-04 0.04714  3.66565E-04 0.08149 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.03321E-01 0.04294  1.24908E-02 5.9E-06  3.16352E-02 0.00067  1.10351E-01 0.00088  3.21042E-01 0.00082  1.34554E+00 0.00049  8.87276E+00 0.00389 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.00082E-03 0.01703  1.89698E-04 0.10481  1.12501E-03 0.04454  1.11771E-03 0.04583  3.22519E-03 0.02638  9.77157E-04 0.04584  3.66062E-04 0.07847 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.03018E-01 0.04115  1.24908E-02 6.0E-06  3.16371E-02 0.00065  1.10338E-01 0.00087  3.21101E-01 0.00081  1.34556E+00 0.00048  8.87114E+00 0.00388 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.94472E+02 0.01775 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.57604E-05 0.00053 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.72510E-05 0.00038 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.04208E-03 0.00384 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.96940E+02 0.00383 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  7.23543E-07 0.00038 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.89863E-06 0.00029  2.89835E-06 0.00029  2.93646E-06 0.00345 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.98491E-05 0.00049  4.98704E-05 0.00049  4.69859E-05 0.00539 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.92934E-01 0.00020  6.92601E-01 0.00020  7.46662E-01 0.00603 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02690E+01 0.00909 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.98681E+01 0.00031  3.93537E+01 0.00034 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.44180E+05 0.00194  5.81092E+05 0.00122  1.20495E+06 0.00061  1.29827E+06 0.00055  1.19562E+06 0.00054  1.28432E+06 0.00043  8.73121E+05 0.00062  7.72406E+05 0.00056  5.91235E+05 0.00049  4.81987E+05 0.00056  4.15938E+05 0.00078  3.74922E+05 0.00076  3.46378E+05 0.00055  3.29198E+05 0.00070  3.20930E+05 0.00056  2.77698E+05 0.00056  2.73902E+05 0.00068  2.71294E+05 0.00071  2.67104E+05 0.00070  5.21345E+05 0.00049  5.04057E+05 0.00047  3.64619E+05 0.00061  2.37067E+05 0.00071  2.74458E+05 0.00068  2.60157E+05 0.00061  2.37450E+05 0.00057  3.91543E+05 0.00054  8.92209E+04 0.00124  1.12130E+05 0.00086  1.01577E+05 0.00117  5.91244E+04 0.00144  1.02646E+05 0.00091  6.98243E+04 0.00143  5.97306E+04 0.00148  1.14598E+04 0.00225  1.14009E+04 0.00247  1.17170E+04 0.00263  1.20423E+04 0.00265  1.19260E+04 0.00172  1.16936E+04 0.00232  1.21946E+04 0.00220  1.13413E+04 0.00206  2.14187E+04 0.00224  3.43329E+04 0.00150  4.37356E+04 0.00120  1.15627E+05 0.00081  1.22768E+05 0.00119  1.36996E+05 0.00081  9.71133E+04 0.00105  7.46725E+04 0.00103  5.86134E+04 0.00138  7.04888E+04 0.00131  1.36751E+05 0.00092  1.88451E+05 0.00085  3.68640E+05 0.00071  5.76445E+05 0.00052  8.71014E+05 0.00059  5.60231E+05 0.00062  4.02419E+05 0.00061  2.89368E+05 0.00069  2.60107E+05 0.00067  2.56324E+05 0.00068  2.13911E+05 0.00069  1.44791E+05 0.00063  1.33551E+05 0.00075  1.18918E+05 0.00063  1.00510E+05 0.00069  7.90759E+04 0.00094  5.29578E+04 0.00103  1.86948E+04 0.00134 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.05150E+00 0.00044 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  3.74347E+01 0.00031  1.33102E+01 0.00032 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38792E-01 6.8E-05  1.47913E+00 0.00022 ];
INF_CAPT                  (idx, [1:   4]) = [  6.21858E-03 0.00043  2.52254E-02 0.00014 ];
INF_ABS                   (idx, [1:   4]) = [  8.09262E-03 0.00037  5.17228E-02 0.00032 ];
INF_FISS                  (idx, [1:   4]) = [  1.87404E-03 0.00041  2.64974E-02 0.00050 ];
INF_NSF                   (idx, [1:   4]) = [  4.84311E-03 0.00042  6.45663E-02 0.00050 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.58431E+00 3.8E-05  2.43670E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04147E+02 3.6E-06  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  6.01268E-08 0.00024  2.58944E-06 7.5E-05 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.30704E-01 6.9E-05  1.42741E+00 0.00024 ];
INF_SCATT1                (idx, [1:   4]) = [  2.45031E-01 0.00014  3.73208E-01 0.00025 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65788E-02 0.00024  8.80017E-02 0.00067 ];
INF_SCATT3                (idx, [1:   4]) = [  7.40156E-03 0.00264  2.63781E-02 0.00145 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03703E-02 0.00124 -8.43289E-03 0.00347 ];
INF_SCATT5                (idx, [1:   4]) = [  1.13766E-04 0.09118  6.26995E-03 0.00457 ];
INF_SCATT6                (idx, [1:   4]) = [  5.06083E-03 0.00167 -1.60864E-02 0.00174 ];
INF_SCATT7                (idx, [1:   4]) = [  7.47269E-04 0.01302  4.24966E-04 0.08334 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.30742E-01 6.9E-05  1.42741E+00 0.00024 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.45031E-01 0.00014  3.73208E-01 0.00025 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65789E-02 0.00024  8.80017E-02 0.00067 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.40132E-03 0.00264  2.63781E-02 0.00145 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03703E-02 0.00124 -8.43289E-03 0.00347 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.13714E-04 0.09130  6.26995E-03 0.00457 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.06078E-03 0.00168 -1.60864E-02 0.00174 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.47369E-04 0.01302  4.24966E-04 0.08334 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13830E-01 0.00023  9.60912E-01 0.00021 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55888E+00 0.00023  3.46893E-01 0.00021 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.05378E-03 0.00037  5.17228E-02 0.00032 ];
INF_REMXS                 (idx, [1:   4]) = [  2.68907E-02 0.00017  5.26512E-02 0.00048 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11902E-01 6.9E-05  1.88021E-02 0.00022  9.33119E-04 0.00309  1.42648E+00 0.00024 ];
INF_S1                    (idx, [1:   8]) = [  2.39524E-01 0.00015  5.50607E-03 0.00070  3.97863E-04 0.00657  3.72810E-01 0.00025 ];
INF_S2                    (idx, [1:   8]) = [  9.81852E-02 0.00023 -1.60639E-03 0.00175  2.17337E-04 0.00607  8.77844E-02 0.00067 ];
INF_S3                    (idx, [1:   8]) = [  9.32735E-03 0.00205 -1.92579E-03 0.00115  7.70524E-05 0.01159  2.63010E-02 0.00147 ];
INF_S4                    (idx, [1:   8]) = [ -9.72235E-03 0.00138 -6.47986E-04 0.00225  4.06440E-08 1.00000 -8.43293E-03 0.00345 ];
INF_S5                    (idx, [1:   8]) = [  1.01554E-04 0.09654  1.22113E-05 0.14522 -3.14557E-05 0.02460  6.30141E-03 0.00454 ];
INF_S6                    (idx, [1:   8]) = [  5.21238E-03 0.00165 -1.51550E-04 0.00837 -4.11457E-05 0.01580 -1.60453E-02 0.00177 ];
INF_S7                    (idx, [1:   8]) = [  9.24121E-04 0.01049 -1.76852E-04 0.00797 -3.65064E-05 0.01954  4.61473E-04 0.07661 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11940E-01 6.9E-05  1.88021E-02 0.00022  9.33119E-04 0.00309  1.42648E+00 0.00024 ];
INF_SP1                   (idx, [1:   8]) = [  2.39525E-01 0.00015  5.50607E-03 0.00070  3.97863E-04 0.00657  3.72810E-01 0.00025 ];
INF_SP2                   (idx, [1:   8]) = [  9.81852E-02 0.00023 -1.60639E-03 0.00175  2.17337E-04 0.00607  8.77844E-02 0.00067 ];
INF_SP3                   (idx, [1:   8]) = [  9.32711E-03 0.00205 -1.92579E-03 0.00115  7.70524E-05 0.01159  2.63010E-02 0.00147 ];
INF_SP4                   (idx, [1:   8]) = [ -9.72227E-03 0.00138 -6.47986E-04 0.00225  4.06440E-08 1.00000 -8.43293E-03 0.00345 ];
INF_SP5                   (idx, [1:   8]) = [  1.01503E-04 0.09671  1.22113E-05 0.14522 -3.14557E-05 0.02460  6.30141E-03 0.00454 ];
INF_SP6                   (idx, [1:   8]) = [  5.21233E-03 0.00165 -1.51550E-04 0.00837 -4.11457E-05 0.01580 -1.60453E-02 0.00177 ];
INF_SP7                   (idx, [1:   8]) = [  9.24221E-04 0.01049 -1.76852E-04 0.00797 -3.65064E-05 0.01954  4.61473E-04 0.07661 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.33071E-01 0.00035  1.03852E+00 0.00430 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34501E-01 0.00054  1.09086E+00 0.00541 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34546E-01 0.00057  1.09714E+00 0.00552 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.30221E-01 0.00052  9.43343E-01 0.00431 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43019E+00 0.00035  3.21114E-01 0.00433 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42147E+00 0.00054  3.05784E-01 0.00539 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42120E+00 0.00057  3.04044E-01 0.00554 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.44789E+00 0.00052  3.53512E-01 0.00433 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.16017E-03 0.00593  2.09033E-04 0.03393  1.13868E-03 0.01471  1.12238E-03 0.01398  3.25082E-03 0.00880  1.08072E-03 0.01514  3.58541E-04 0.02543 ];
LAMBDA                    (idx, [1:  14]) = [  8.13050E-01 0.01289  1.24908E-02 2.5E-06  3.16412E-02 0.00023  1.10256E-01 0.00031  3.20700E-01 0.00024  1.34507E+00 0.00017  8.86893E+00 0.00144 ];

