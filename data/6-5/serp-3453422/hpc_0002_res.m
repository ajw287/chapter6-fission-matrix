
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0002' ;
WORKING_DIRECTORY         (idx, [1: 33])  = '/home/ajw287/serpent/fmtx_3453422' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Sun Apr 19 21:55:52 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Sun Apr 19 22:02:50 2020' ;

% Run parameters:

POP                       (idx, 1)        = 20000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1587329752 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.49389E-01  1.01585E+00  1.01563E+00  1.01358E+00  1.00555E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  3])  = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 9.3E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.17049E-02 0.00069  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88295E-01 8.2E-06  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.81463E-01 7.7E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.81886E-01 7.7E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.76801E+00 0.00016  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.00476E+01 0.00031  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.00402E+01 0.00031  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.11693E+01 0.00028  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  5.13799E-01 0.00071  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 10000420 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  2.00008E+04 0.00052 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  2.00008E+04 0.00052 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.47451E+01 ;
RUNNING_TIME              (idx, 1)        =  6.96987E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  2.55000E-02  2.55000E-02 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.16668E-04  3.16668E-04 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.94405E+00  6.94405E+00  0.00000E+00 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  6.96973E+00  0.00000E+00 ];
CPU_USAGE                 (idx, 1)        = 4.98505 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99951E+00 8.5E-05 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80519E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 370.78;
MEMSIZE                   (idx, 1)        = 288.50;
XS_MEMSIZE                (idx, 1)        = 127.27;
MAT_MEMSIZE               (idx, 1)        = 29.07;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 130.80;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 82.28;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 126702 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 5 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 15 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 15 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 484 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  0.00000E+00 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.99538E-05 0.00027  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.38925E-01 0.00074 ];
U235_FISS                 (idx, [1:   4]) = [  3.86977E-01 0.00042  9.32321E-01 0.00013 ];
U238_FISS                 (idx, [1:   4]) = [  2.80425E-02 0.00195  6.75578E-02 0.00185 ];
U235_CAPT                 (idx, [1:   4]) = [  7.89993E-02 0.00115  1.36673E-01 0.00108 ];
U238_CAPT                 (idx, [1:   4]) = [  2.95555E-01 0.00058  5.11325E-01 0.00043 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 10000420 1.00000E+07 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 1.46784E+04 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 10000420 1.00147E+07 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 5777222 5.78551E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 4148647 4.15458E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 74551 7.45855E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 10000420 1.00147E+07 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 2.12342E-07 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.34524E-11 0.00019 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.19560E-18 0.00019 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.02031E+00 0.00019 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.14447E-01 0.00019 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  5.78101E-01 0.00013 ];
TOT_ABSRATE               (idx, [1:   2]) = [  9.92549E-01 2.9E-05 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99075E-01 0.00027 ];
TOT_FLUX                  (idx, [1:   2]) = [  5.12539E+01 0.00020 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  7.45141E-03 0.00390 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  4.00584E+01 0.00025 ];
INI_FMASS                 (idx, 1)        =  1.12516E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12516E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.66807E+00 0.00030 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.35764E-01 0.00025 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.97129E-01 0.00021 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.20443E+00 0.00021 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95160E-01 2.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97369E-01 1.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.03047E+00 0.00037 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.02278E+00 0.00037 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46186E+00 9.9E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02592E+02 1.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.02270E+00 0.00038  1.01562E+00 0.00038  7.16640E-03 0.00571 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.02181E+00 0.00019 ];
COL_KEFF                  (idx, [1:   2]) = [  1.02130E+00 0.00036 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.02181E+00 0.00019 ];
ABS_KINF                  (idx, [1:   2]) = [  1.02949E+00 0.00018 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.77314E+01 0.00013 ];
IMP_ALF                   (idx, [1:   2]) = [  1.77314E+01 5.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.99007E-07 0.00235 ];
IMP_EALF                  (idx, [1:   2]) = [  3.98564E-07 0.00102 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.22847E-01 0.00202 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.23161E-01 0.00087 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  7.03541E-03 0.00380  2.04255E-04 0.02185  1.10073E-03 0.00957  1.12291E-03 0.00939  3.20341E-03 0.00572  1.05154E-03 0.01020  3.52556E-04 0.01752 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.15992E-01 0.00914  1.23659E-02 0.00450  3.16301E-02 0.00016  1.10272E-01 0.00019  3.20833E-01 0.00016  1.34498E+00 0.00012  8.91475E+00 0.00113 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.09130E-03 0.00550  2.06486E-04 0.03414  1.11044E-03 0.01418  1.11984E-03 0.01399  3.23426E-03 0.00800  1.06809E-03 0.01573  3.52186E-04 0.02672 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.14697E-01 0.01406  1.24908E-02 2.2E-06  3.16312E-02 0.00023  1.10270E-01 0.00030  3.20854E-01 0.00024  1.34465E+00 0.00018  8.92515E+00 0.00162 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.82029E-05 0.00077  3.81839E-05 0.00077  4.09433E-05 0.00769 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.90674E-05 0.00069  3.90480E-05 0.00069  4.18693E-05 0.00768 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.99857E-03 0.00584  2.08279E-04 0.03513  1.09787E-03 0.01506  1.14430E-03 0.01475  3.16461E-03 0.00888  1.03242E-03 0.01567  3.51085E-04 0.02792 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.13532E-01 0.01499  1.24908E-02 2.9E-06  3.16216E-02 0.00028  1.10293E-01 0.00033  3.20909E-01 0.00024  1.34466E+00 0.00019  8.92206E+00 0.00194 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.80350E-05 0.00178  3.80132E-05 0.00177  4.11121E-05 0.01858 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.88965E-05 0.00177  3.88742E-05 0.00176  4.20475E-05 0.01857 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.08804E-03 0.01993  2.12188E-04 0.10760  1.12144E-03 0.05102  1.11472E-03 0.05105  3.22178E-03 0.02871  1.02607E-03 0.05381  3.91842E-04 0.08111 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.29733E-01 0.04467  1.24907E-02 4.9E-06  3.16483E-02 0.00066  1.10374E-01 0.00092  3.20373E-01 0.00072  1.34489E+00 0.00051  8.90366E+00 0.00414 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.11393E-03 0.01936  2.24759E-04 0.10535  1.14122E-03 0.04932  1.12403E-03 0.05015  3.21456E-03 0.02810  1.02446E-03 0.05273  3.84890E-04 0.07905 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.25630E-01 0.04327  1.24907E-02 4.8E-06  3.16470E-02 0.00066  1.10333E-01 0.00089  3.20292E-01 0.00069  1.34492E+00 0.00051  8.89860E+00 0.00410 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.86705E+02 0.02005 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.81608E-05 0.00051 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.90242E-05 0.00036 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.04148E-03 0.00383 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.84530E+02 0.00383 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  7.32340E-07 0.00037 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.92394E-06 0.00029  2.92360E-06 0.00029  2.96955E-06 0.00347 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  5.08145E-05 0.00048  5.08338E-05 0.00048  4.82405E-05 0.00517 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.94896E-01 0.00021  6.94676E-01 0.00021  7.31109E-01 0.00579 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02289E+01 0.00883 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.00402E+01 0.00031  4.02201E+01 0.00031 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.43727E+05 0.00211  5.81495E+05 0.00096  1.20512E+06 0.00066  1.29986E+06 0.00054  1.20010E+06 0.00043  1.29084E+06 0.00055  8.76380E+05 0.00047  7.75645E+05 0.00052  5.93887E+05 0.00051  4.84766E+05 0.00061  4.18431E+05 0.00053  3.77258E+05 0.00044  3.48302E+05 0.00072  3.31157E+05 0.00046  3.22641E+05 0.00049  2.78699E+05 0.00066  2.75489E+05 0.00067  2.72979E+05 0.00060  2.68376E+05 0.00082  5.24390E+05 0.00055  5.07958E+05 0.00054  3.67281E+05 0.00052  2.38833E+05 0.00066  2.76985E+05 0.00070  2.62123E+05 0.00058  2.39844E+05 0.00065  3.94449E+05 0.00055  9.02626E+04 0.00109  1.13174E+05 0.00103  1.02687E+05 0.00109  5.97533E+04 0.00149  1.03731E+05 0.00096  7.05466E+04 0.00104  6.04880E+04 0.00122  1.16308E+04 0.00285  1.15418E+04 0.00258  1.17848E+04 0.00225  1.22100E+04 0.00227  1.20856E+04 0.00300  1.18475E+04 0.00242  1.22670E+04 0.00187  1.14760E+04 0.00237  2.17799E+04 0.00158  3.47379E+04 0.00159  4.42575E+04 0.00146  1.17038E+05 0.00110  1.24400E+05 0.00096  1.38870E+05 0.00090  9.86659E+04 0.00117  7.62269E+04 0.00103  5.95950E+04 0.00135  7.19954E+04 0.00104  1.39481E+05 0.00099  1.92640E+05 0.00070  3.76843E+05 0.00073  5.90242E+05 0.00071  8.91153E+05 0.00076  5.73185E+05 0.00075  4.11729E+05 0.00073  2.95888E+05 0.00079  2.65925E+05 0.00091  2.61971E+05 0.00090  2.18888E+05 0.00088  1.47903E+05 0.00106  1.36552E+05 0.00092  1.21554E+05 0.00105  1.02808E+05 0.00084  8.08722E+04 0.00100  5.40788E+04 0.00092  1.90821E+04 0.00121 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.02897E+00 0.00036 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  3.76409E+01 0.00024  1.36138E+01 0.00056 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38278E-01 6.8E-05  1.45428E+00 0.00029 ];
INF_CAPT                  (idx, [1:   4]) = [  6.24206E-03 0.00058  2.52072E-02 0.00017 ];
INF_ABS                   (idx, [1:   4]) = [  8.01696E-03 0.00048  5.07445E-02 0.00043 ];
INF_FISS                  (idx, [1:   4]) = [  1.77490E-03 0.00036  2.55373E-02 0.00070 ];
INF_NSF                   (idx, [1:   4]) = [  4.60188E-03 0.00038  6.22267E-02 0.00070 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.59275E+00 4.8E-05  2.43670E+00 2.7E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04265E+02 4.5E-06  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  6.04325E-08 0.00028  2.59016E-06 0.00011 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.30260E-01 6.9E-05  1.40350E+00 0.00032 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44114E-01 0.00016  3.65378E-01 0.00038 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62436E-02 0.00020  8.61431E-02 0.00065 ];
INF_SCATT3                (idx, [1:   4]) = [  7.39286E-03 0.00158  2.59547E-02 0.00128 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03118E-02 0.00120 -8.14631E-03 0.00346 ];
INF_SCATT5                (idx, [1:   4]) = [  1.26401E-04 0.07987  6.14430E-03 0.00437 ];
INF_SCATT6                (idx, [1:   4]) = [  5.03834E-03 0.00250 -1.56793E-02 0.00197 ];
INF_SCATT7                (idx, [1:   4]) = [  7.40535E-04 0.01623  4.28780E-04 0.06995 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.30299E-01 6.9E-05  1.40350E+00 0.00032 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44114E-01 0.00016  3.65378E-01 0.00038 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62438E-02 0.00020  8.61431E-02 0.00065 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.39287E-03 0.00158  2.59547E-02 0.00128 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03118E-02 0.00120 -8.14631E-03 0.00346 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.26286E-04 0.07988  6.14430E-03 0.00437 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.03832E-03 0.00250 -1.56793E-02 0.00197 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.40362E-04 0.01620  4.28780E-04 0.06995 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.14545E-01 0.00020  9.48692E-01 0.00025 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55368E+00 0.00020  3.51362E-01 0.00025 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  7.97800E-03 0.00050  5.07445E-02 0.00043 ];
INF_REMXS                 (idx, [1:   4]) = [  2.67865E-02 0.00014  5.17016E-02 0.00049 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11491E-01 6.7E-05  1.87686E-02 0.00023  9.22642E-04 0.00307  1.40257E+00 0.00032 ];
INF_S1                    (idx, [1:   8]) = [  2.38623E-01 0.00015  5.49108E-03 0.00056  3.92659E-04 0.00420  3.64985E-01 0.00038 ];
INF_S2                    (idx, [1:   8]) = [  9.78529E-02 0.00019 -1.60932E-03 0.00154  2.13797E-04 0.00791  8.59293E-02 0.00065 ];
INF_S3                    (idx, [1:   8]) = [  9.31818E-03 0.00118 -1.92532E-03 0.00118  7.58802E-05 0.01754  2.58789E-02 0.00131 ];
INF_S4                    (idx, [1:   8]) = [ -9.66196E-03 0.00129 -6.49876E-04 0.00318 -2.03779E-06 0.38718 -8.14427E-03 0.00344 ];
INF_S5                    (idx, [1:   8]) = [  1.15779E-04 0.08595  1.06220E-05 0.15570 -3.19836E-05 0.02543  6.17628E-03 0.00433 ];
INF_S6                    (idx, [1:   8]) = [  5.18853E-03 0.00226 -1.50188E-04 0.01486 -3.90585E-05 0.02121 -1.56402E-02 0.00198 ];
INF_S7                    (idx, [1:   8]) = [  9.16045E-04 0.01245 -1.75511E-04 0.00896 -3.51898E-05 0.02324  4.63969E-04 0.06419 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11530E-01 6.7E-05  1.87686E-02 0.00023  9.22642E-04 0.00307  1.40257E+00 0.00032 ];
INF_SP1                   (idx, [1:   8]) = [  2.38623E-01 0.00015  5.49108E-03 0.00056  3.92659E-04 0.00420  3.64985E-01 0.00038 ];
INF_SP2                   (idx, [1:   8]) = [  9.78532E-02 0.00019 -1.60932E-03 0.00154  2.13797E-04 0.00791  8.59293E-02 0.00065 ];
INF_SP3                   (idx, [1:   8]) = [  9.31819E-03 0.00119 -1.92532E-03 0.00118  7.58802E-05 0.01754  2.58789E-02 0.00131 ];
INF_SP4                   (idx, [1:   8]) = [ -9.66195E-03 0.00129 -6.49876E-04 0.00318 -2.03779E-06 0.38718 -8.14427E-03 0.00344 ];
INF_SP5                   (idx, [1:   8]) = [  1.15664E-04 0.08596  1.06220E-05 0.15570 -3.19836E-05 0.02543  6.17628E-03 0.00433 ];
INF_SP6                   (idx, [1:   8]) = [  5.18851E-03 0.00226 -1.50188E-04 0.01486 -3.90585E-05 0.02121 -1.56402E-02 0.00198 ];
INF_SP7                   (idx, [1:   8]) = [  9.15873E-04 0.01242 -1.75511E-04 0.00896 -3.51898E-05 0.02324  4.63969E-04 0.06419 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32807E-01 0.00044  1.01129E+00 0.00364 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34303E-01 0.00061  1.06154E+00 0.00392 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34119E-01 0.00063  1.05790E+00 0.00495 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.30053E-01 0.00056  9.26868E-01 0.00366 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43180E+00 0.00044  3.29716E-01 0.00363 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42267E+00 0.00061  3.14124E-01 0.00391 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42379E+00 0.00063  3.15275E-01 0.00499 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.44895E+00 0.00056  3.59749E-01 0.00364 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.09130E-03 0.00550  2.06486E-04 0.03414  1.11044E-03 0.01418  1.11984E-03 0.01399  3.23426E-03 0.00800  1.06809E-03 0.01573  3.52186E-04 0.02672 ];
LAMBDA                    (idx, [1:  14]) = [  8.14697E-01 0.01406  1.24908E-02 2.2E-06  3.16312E-02 0.00023  1.10270E-01 0.00030  3.20854E-01 0.00024  1.34465E+00 0.00018  8.92515E+00 0.00162 ];

