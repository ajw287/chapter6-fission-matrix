
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0004' ;
WORKING_DIRECTORY         (idx, [1: 33])  = '/home/ajw287/serpent/fmtx_3453422' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Sun Apr 19 22:09:52 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Sun Apr 19 22:16:44 2020' ;

% Run parameters:

POP                       (idx, 1)        = 20000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1587330592 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.03064E+00  1.02799E+00  1.00337E+00  1.02995E+00  9.08057E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  3])  = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15468E-02 0.00072  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88453E-01 8.4E-06  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.86180E-01 7.9E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.86598E-01 7.8E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.75496E+00 0.00016  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.97918E+01 0.00030  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.97837E+01 0.00030  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.07930E+01 0.00029  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.98978E-01 0.00075  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 10000667 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  2.00013E+04 0.00053 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  2.00013E+04 0.00053 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.42705E+01 ;
RUNNING_TIME              (idx, 1)        =  6.87277E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  2.30333E-02  2.30333E-02 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.49998E-04  3.49998E-04 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.84935E+00  6.84935E+00  0.00000E+00 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  6.87260E+00  0.00000E+00 ];
CPU_USAGE                 (idx, 1)        = 4.98641 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99997E+00 8.6E-05 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.82074E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 370.78;
MEMSIZE                   (idx, 1)        = 288.50;
XS_MEMSIZE                (idx, 1)        = 127.27;
MAT_MEMSIZE               (idx, 1)        = 29.07;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 130.80;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 82.28;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 126702 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 5 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 15 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 15 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 484 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  0.00000E+00 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.99561E-05 0.00027  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.09434E-01 0.00079 ];
U235_FISS                 (idx, [1:   4]) = [  3.93745E-01 0.00045  9.33771E-01 0.00014 ];
U238_FISS                 (idx, [1:   4]) = [  2.78769E-02 0.00201  6.61076E-02 0.00192 ];
U235_CAPT                 (idx, [1:   4]) = [  8.13952E-02 0.00111  1.42595E-01 0.00106 ];
U238_CAPT                 (idx, [1:   4]) = [  2.87484E-01 0.00061  5.03629E-01 0.00044 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 10000667 1.00000E+07 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 1.46970E+04 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 10000667 1.00147E+07 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 5705052 5.71321E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 4214608 4.22046E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 81007 8.10269E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 10000667 1.00147E+07 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 2.38419E-07 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.36774E-11 0.00020 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.21560E-18 0.00020 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.03720E+00 0.00020 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.21393E-01 0.00020 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  5.70512E-01 0.00014 ];
TOT_ABSRATE               (idx, [1:   2]) = [  9.91905E-01 2.9E-05 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.99121E-01 0.00027 ];
TOT_FLUX                  (idx, [1:   2]) = [  5.08528E+01 0.00020 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  8.09535E-03 0.00356 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.98029E+01 0.00025 ];
INI_FMASS                 (idx, 1)        =  1.12516E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12516E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.69004E+00 0.00030 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.36840E-01 0.00024 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.95196E-01 0.00021 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.20978E+00 0.00023 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94745E-01 2.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97138E-01 1.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.04731E+00 0.00038 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.03883E+00 0.00038 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46136E+00 9.4E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02584E+02 1.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03893E+00 0.00039  1.03144E+00 0.00038  7.38568E-03 0.00575 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.03871E+00 0.00020 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03815E+00 0.00036 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.03871E+00 0.00020 ];
ABS_KINF                  (idx, [1:   2]) = [  1.04720E+00 0.00019 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.77083E+01 0.00014 ];
IMP_ALF                   (idx, [1:   2]) = [  1.77094E+01 5.6E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.08386E-07 0.00250 ];
IMP_EALF                  (idx, [1:   2]) = [  4.07429E-07 0.00100 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.19275E-01 0.00208 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.18858E-01 0.00083 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.90198E-03 0.00390  1.87079E-04 0.02374  1.10527E-03 0.00980  1.07432E-03 0.00941  3.15352E-03 0.00560  1.03782E-03 0.00987  3.43963E-04 0.01703 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.15333E-01 0.00867  1.21910E-02 0.00702  3.16436E-02 0.00015  1.10249E-01 0.00019  3.20819E-01 0.00016  1.34515E+00 0.00011  8.89968E+00 0.00229 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.12041E-03 0.00555  1.98016E-04 0.03592  1.12767E-03 0.01515  1.12562E-03 0.01425  3.24897E-03 0.00806  1.07243E-03 0.01486  3.47704E-04 0.02621 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.06685E-01 0.01331  1.24907E-02 2.0E-06  3.16452E-02 0.00022  1.10259E-01 0.00030  3.20866E-01 0.00023  1.34481E+00 0.00017  8.93255E+00 0.00165 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.61444E-05 0.00078  3.61264E-05 0.00078  3.86350E-05 0.00766 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.75485E-05 0.00066  3.75297E-05 0.00067  4.01383E-05 0.00767 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.11920E-03 0.00583  1.95679E-04 0.03507  1.13326E-03 0.01522  1.10420E-03 0.01499  3.25052E-03 0.00861  1.06699E-03 0.01511  3.68553E-04 0.02451 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.32221E-01 0.01296  1.24908E-02 2.9E-06  3.16344E-02 0.00025  1.10292E-01 0.00033  3.20914E-01 0.00026  1.34479E+00 0.00019  8.91457E+00 0.00183 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.61967E-05 0.00182  3.61759E-05 0.00183  3.88424E-05 0.02014 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.76026E-05 0.00177  3.75810E-05 0.00178  4.03484E-05 0.02011 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.36625E-03 0.01866  2.15190E-04 0.13052  1.12763E-03 0.04914  1.14300E-03 0.04527  3.43839E-03 0.02814  1.03706E-03 0.05116  4.04975E-04 0.08164 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.52679E-01 0.04529  1.24908E-02 6.6E-06  3.16693E-02 0.00062  1.10333E-01 0.00090  3.21204E-01 0.00083  1.34463E+00 0.00053  8.90037E+00 0.00400 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.29845E-03 0.01806  2.11381E-04 0.12618  1.12324E-03 0.04855  1.14039E-03 0.04414  3.39750E-03 0.02739  1.03552E-03 0.04964  3.90426E-04 0.08109 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.45273E-01 0.04453  1.24908E-02 6.6E-06  3.16664E-02 0.00062  1.10328E-01 0.00088  3.21244E-01 0.00081  1.34455E+00 0.00052  8.89537E+00 0.00394 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.04162E+02 0.01891 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.61766E-05 0.00050 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.75819E-05 0.00030 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.13547E-03 0.00293 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.97251E+02 0.00292 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  7.23028E-07 0.00037 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.90805E-06 0.00030  2.90775E-06 0.00030  2.95004E-06 0.00347 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.98798E-05 0.00048  4.99002E-05 0.00048  4.71555E-05 0.00499 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.92774E-01 0.00021  6.92439E-01 0.00022  7.46548E-01 0.00589 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02002E+01 0.00906 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.97837E+01 0.00030  3.95362E+01 0.00030 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.43507E+05 0.00213  5.80722E+05 0.00085  1.20499E+06 0.00063  1.29743E+06 0.00062  1.19776E+06 0.00045  1.28798E+06 0.00044  8.74412E+05 0.00058  7.73093E+05 0.00037  5.91766E+05 0.00058  4.83996E+05 0.00072  4.17058E+05 0.00054  3.75557E+05 0.00068  3.47025E+05 0.00055  3.29718E+05 0.00060  3.21160E+05 0.00080  2.77886E+05 0.00073  2.74441E+05 0.00054  2.72000E+05 0.00070  2.67464E+05 0.00061  5.22441E+05 0.00039  5.05358E+05 0.00042  3.66029E+05 0.00049  2.37717E+05 0.00076  2.75341E+05 0.00059  2.60722E+05 0.00061  2.38014E+05 0.00057  3.91924E+05 0.00061  8.94345E+04 0.00092  1.12420E+05 0.00131  1.02012E+05 0.00096  5.93835E+04 0.00125  1.02863E+05 0.00104  7.01536E+04 0.00117  5.99910E+04 0.00128  1.15416E+04 0.00311  1.14629E+04 0.00276  1.17095E+04 0.00267  1.20821E+04 0.00240  1.19871E+04 0.00221  1.17601E+04 0.00259  1.20658E+04 0.00220  1.14052E+04 0.00261  2.16075E+04 0.00161  3.44335E+04 0.00138  4.39424E+04 0.00161  1.16088E+05 0.00090  1.22991E+05 0.00083  1.37506E+05 0.00095  9.73707E+04 0.00085  7.49604E+04 0.00086  5.87468E+04 0.00084  7.05658E+04 0.00125  1.37169E+05 0.00077  1.88944E+05 0.00074  3.69211E+05 0.00085  5.77142E+05 0.00070  8.71754E+05 0.00066  5.60531E+05 0.00068  4.02438E+05 0.00068  2.89238E+05 0.00080  2.60235E+05 0.00088  2.56291E+05 0.00089  2.13992E+05 0.00089  1.44663E+05 0.00092  1.33540E+05 0.00075  1.18917E+05 0.00099  1.00524E+05 0.00089  7.90884E+04 0.00080  5.29595E+04 0.00120  1.86846E+04 0.00142 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.04663E+00 0.00039 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  3.75216E+01 0.00025  1.33319E+01 0.00041 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38596E-01 6.7E-05  1.46977E+00 0.00025 ];
INF_CAPT                  (idx, [1:   4]) = [  6.23349E-03 0.00046  2.52507E-02 0.00016 ];
INF_ABS                   (idx, [1:   4]) = [  8.08678E-03 0.00037  5.16440E-02 0.00039 ];
INF_FISS                  (idx, [1:   4]) = [  1.85330E-03 0.00029  2.63933E-02 0.00061 ];
INF_NSF                   (idx, [1:   4]) = [  4.79284E-03 0.00029  6.43125E-02 0.00061 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.58612E+00 4.4E-05  2.43670E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04172E+02 4.1E-06  2.02270E+02 3.8E-09 ];
INF_INVV                  (idx, [1:   4]) = [  6.02118E-08 0.00019  2.58852E-06 8.1E-05 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.30506E-01 6.9E-05  1.41809E+00 0.00028 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44653E-01 0.00010  3.70066E-01 0.00028 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64340E-02 0.00022  8.72672E-02 0.00073 ];
INF_SCATT3                (idx, [1:   4]) = [  7.38526E-03 0.00191  2.62217E-02 0.00156 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03746E-02 0.00129 -8.36154E-03 0.00452 ];
INF_SCATT5                (idx, [1:   4]) = [  1.12989E-04 0.11742  6.19332E-03 0.00433 ];
INF_SCATT6                (idx, [1:   4]) = [  5.06160E-03 0.00199 -1.59842E-02 0.00182 ];
INF_SCATT7                (idx, [1:   4]) = [  7.36763E-04 0.01458  4.56050E-04 0.05743 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.30546E-01 7.0E-05  1.41809E+00 0.00028 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44653E-01 0.00010  3.70066E-01 0.00028 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64341E-02 0.00022  8.72672E-02 0.00073 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.38529E-03 0.00192  2.62217E-02 0.00156 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03747E-02 0.00129 -8.36154E-03 0.00452 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.13112E-04 0.11739  6.19332E-03 0.00433 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.06168E-03 0.00198 -1.59842E-02 0.00182 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.36776E-04 0.01460  4.56050E-04 0.05743 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.14143E-01 0.00021  9.56408E-01 0.00022 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55659E+00 0.00021  3.48527E-01 0.00022 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.04765E-03 0.00037  5.16440E-02 0.00039 ];
INF_REMXS                 (idx, [1:   4]) = [  2.68590E-02 0.00011  5.26117E-02 0.00052 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11737E-01 6.8E-05  1.87693E-02 0.00022  9.34133E-04 0.00219  1.41715E+00 0.00028 ];
INF_S1                    (idx, [1:   8]) = [  2.39157E-01 0.00010  5.49548E-03 0.00055  3.97686E-04 0.00349  3.69668E-01 0.00028 ];
INF_S2                    (idx, [1:   8]) = [  9.80405E-02 0.00022 -1.60644E-03 0.00166  2.16842E-04 0.00613  8.70503E-02 0.00073 ];
INF_S3                    (idx, [1:   8]) = [  9.31345E-03 0.00154 -1.92819E-03 0.00129  7.80811E-05 0.01094  2.61437E-02 0.00156 ];
INF_S4                    (idx, [1:   8]) = [ -9.72153E-03 0.00139 -6.53119E-04 0.00412  1.52679E-06 0.61108 -8.36307E-03 0.00453 ];
INF_S5                    (idx, [1:   8]) = [  1.00637E-04 0.12921  1.23523E-05 0.15342 -3.20657E-05 0.02501  6.22538E-03 0.00432 ];
INF_S6                    (idx, [1:   8]) = [  5.20988E-03 0.00186 -1.48273E-04 0.01052 -4.02198E-05 0.01583 -1.59439E-02 0.00183 ];
INF_S7                    (idx, [1:   8]) = [  9.13136E-04 0.01147 -1.76373E-04 0.00605 -3.62534E-05 0.01576  4.92303E-04 0.05299 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11776E-01 6.9E-05  1.87693E-02 0.00022  9.34133E-04 0.00219  1.41715E+00 0.00028 ];
INF_SP1                   (idx, [1:   8]) = [  2.39158E-01 0.00010  5.49548E-03 0.00055  3.97686E-04 0.00349  3.69668E-01 0.00028 ];
INF_SP2                   (idx, [1:   8]) = [  9.80405E-02 0.00022 -1.60644E-03 0.00166  2.16842E-04 0.00613  8.70503E-02 0.00073 ];
INF_SP3                   (idx, [1:   8]) = [  9.31348E-03 0.00154 -1.92819E-03 0.00129  7.80811E-05 0.01094  2.61437E-02 0.00156 ];
INF_SP4                   (idx, [1:   8]) = [ -9.72153E-03 0.00139 -6.53119E-04 0.00412  1.52679E-06 0.61108 -8.36307E-03 0.00453 ];
INF_SP5                   (idx, [1:   8]) = [  1.00760E-04 0.12918  1.23523E-05 0.15342 -3.20657E-05 0.02501  6.22538E-03 0.00432 ];
INF_SP6                   (idx, [1:   8]) = [  5.20995E-03 0.00185 -1.48273E-04 0.01052 -4.02198E-05 0.01583 -1.59439E-02 0.00183 ];
INF_SP7                   (idx, [1:   8]) = [  9.13149E-04 0.01149 -1.76373E-04 0.00605 -3.62534E-05 0.01576  4.92303E-04 0.05299 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.33086E-01 0.00038  1.03279E+00 0.00341 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34604E-01 0.00044  1.08777E+00 0.00463 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34543E-01 0.00051  1.08655E+00 0.00418 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.30171E-01 0.00074  9.39132E-01 0.00311 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43009E+00 0.00038  3.22841E-01 0.00339 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42084E+00 0.00044  3.06594E-01 0.00458 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42121E+00 0.00051  3.06911E-01 0.00417 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.44822E+00 0.00075  3.55020E-01 0.00309 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.12041E-03 0.00555  1.98016E-04 0.03592  1.12767E-03 0.01515  1.12562E-03 0.01425  3.24897E-03 0.00806  1.07243E-03 0.01486  3.47704E-04 0.02621 ];
LAMBDA                    (idx, [1:  14]) = [  8.06685E-01 0.01331  1.24907E-02 2.0E-06  3.16452E-02 0.00022  1.10259E-01 0.00030  3.20866E-01 0.00023  1.34481E+00 0.00017  8.93255E+00 0.00165 ];

