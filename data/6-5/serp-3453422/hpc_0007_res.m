
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0007' ;
WORKING_DIRECTORY         (idx, [1: 33])  = '/home/ajw287/serpent/fmtx_3453422' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Sun Apr 19 22:30:48 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Sun Apr 19 22:37:46 2020' ;

% Run parameters:

POP                       (idx, 1)        = 20000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1587331848 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  8.86700E-01  1.03074E+00  1.01647E+00  1.03440E+00  1.03169E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  3])  = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.18922E-02 0.00067  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88108E-01 8.0E-06  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.77620E-01 7.0E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.78050E-01 7.0E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.77589E+00 0.00016  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.95790E+01 0.00029  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.95725E+01 0.00029  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.12884E+01 0.00027  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  5.19257E-01 0.00069  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 10001120 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  2.00022E+04 0.00054 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  2.00022E+04 0.00054 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.46990E+01 ;
RUNNING_TIME              (idx, 1)        =  6.96043E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  2.44167E-02  2.44167E-02 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.16668E-04  3.16668E-04 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.93567E+00  6.93567E+00  0.00000E+00 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  6.96025E+00  0.00000E+00 ];
CPU_USAGE                 (idx, 1)        = 4.98518 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99947E+00 9.0E-05 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78646E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 350.78;
MEMSIZE                   (idx, 1)        = 281.72;
XS_MEMSIZE                (idx, 1)        = 127.27;
MAT_MEMSIZE               (idx, 1)        = 22.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 130.80;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 69.06;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 126702 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 5 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 15 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 15 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 484 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  0.00000E+00 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.99198E-05 0.00027  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.43201E-01 0.00076 ];
U235_FISS                 (idx, [1:   4]) = [  3.88594E-01 0.00044  9.32052E-01 0.00013 ];
U238_FISS                 (idx, [1:   4]) = [  2.82801E-02 0.00192  6.78275E-02 0.00183 ];
U235_CAPT                 (idx, [1:   4]) = [  7.96244E-02 0.00112  1.38139E-01 0.00108 ];
U238_CAPT                 (idx, [1:   4]) = [  2.98951E-01 0.00061  5.18634E-01 0.00044 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 10001120 1.00000E+07 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 1.48358E+04 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 10001120 1.00148E+07 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 5765272 5.77340E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 4170407 4.17597E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 65441 6.54617E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 10001120 1.00148E+07 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 1.71363E-07 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.35322E-11 0.00019 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.20269E-18 0.00019 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.02635E+00 0.00019 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.16905E-01 0.00019 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  5.76559E-01 0.00013 ];
TOT_ABSRATE               (idx, [1:   2]) = [  9.93464E-01 2.6E-05 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.98395E-01 0.00027 ];
TOT_FLUX                  (idx, [1:   2]) = [  5.11179E+01 0.00020 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  6.53556E-03 0.00388 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  3.95638E+01 0.00023 ];
INI_FMASS                 (idx, 1)        =  1.12516E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12516E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.66484E+00 0.00030 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.43412E-01 0.00024 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.94994E-01 0.00020 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.20309E+00 0.00022 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95733E-01 2.0E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97711E-01 1.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.03484E+00 0.00037 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.02806E+00 0.00038 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46182E+00 9.2E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02592E+02 1.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.02793E+00 0.00039  1.02087E+00 0.00038  7.19809E-03 0.00583 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.02786E+00 0.00019 ];
COL_KEFF                  (idx, [1:   2]) = [  1.02804E+00 0.00035 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.02786E+00 0.00019 ];
ABS_KINF                  (idx, [1:   2]) = [  1.03463E+00 0.00018 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.77384E+01 0.00014 ];
IMP_ALF                   (idx, [1:   2]) = [  1.77399E+01 5.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.96257E-07 0.00243 ];
IMP_EALF                  (idx, [1:   2]) = [  3.95181E-07 0.00101 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.23313E-01 0.00203 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.22898E-01 0.00081 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.96842E-03 0.00382  2.06344E-04 0.02087  1.10470E-03 0.00926  1.07495E-03 0.00952  3.21272E-03 0.00542  1.02618E-03 0.00968  3.43533E-04 0.01670 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.07879E-01 0.00873  1.23159E-02 0.00533  3.16300E-02 0.00016  1.10204E-01 0.00019  3.20965E-01 0.00016  1.34493E+00 0.00012  8.91394E+00 0.00114 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.07609E-03 0.00557  2.01907E-04 0.03537  1.11046E-03 0.01402  1.12333E-03 0.01504  3.26264E-03 0.00826  1.03207E-03 0.01471  3.45680E-04 0.02422 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.02983E-01 0.01263  1.24908E-02 2.6E-06  3.16302E-02 0.00024  1.10164E-01 0.00027  3.20962E-01 0.00024  1.34486E+00 0.00018  8.91519E+00 0.00158 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.78994E-05 0.00076  3.78800E-05 0.00077  4.06755E-05 0.00735 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.89549E-05 0.00067  3.89351E-05 0.00067  4.18059E-05 0.00732 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.99298E-03 0.00596  1.99372E-04 0.03471  1.11117E-03 0.01510  1.08658E-03 0.01533  3.22780E-03 0.00885  1.02399E-03 0.01509  3.44059E-04 0.02545 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.06890E-01 0.01383  1.24908E-02 3.4E-06  3.16296E-02 0.00026  1.10188E-01 0.00031  3.21002E-01 0.00025  1.34479E+00 0.00019  8.90504E+00 0.00181 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.75457E-05 0.00183  3.75287E-05 0.00185  3.98799E-05 0.02006 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.85906E-05 0.00177  3.85731E-05 0.00178  4.09862E-05 0.02003 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.02854E-03 0.01946  2.15366E-04 0.11805  1.09102E-03 0.04680  1.08621E-03 0.04857  3.22411E-03 0.02916  1.06625E-03 0.04978  3.45593E-04 0.08621 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.32961E-01 0.04597  1.24907E-02 5.1E-06  3.16288E-02 0.00070  1.10203E-01 0.00088  3.20991E-01 0.00082  1.34550E+00 0.00049  8.93276E+00 0.00425 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.02212E-03 0.01889  2.16405E-04 0.11586  1.07876E-03 0.04610  1.06758E-03 0.04759  3.24204E-03 0.02820  1.06916E-03 0.04880  3.48166E-04 0.08388 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.35023E-01 0.04588  1.24907E-02 5.1E-06  3.16291E-02 0.00070  1.10215E-01 0.00089  3.20965E-01 0.00080  1.34528E+00 0.00050  8.93266E+00 0.00423 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.87575E+02 0.01950 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.77162E-05 0.00049 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.87667E-05 0.00032 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.97405E-03 0.00388 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.84923E+02 0.00389 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  7.21358E-07 0.00037 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.93973E-06 0.00030  2.93958E-06 0.00030  2.96039E-06 0.00356 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  5.00201E-05 0.00046  5.00377E-05 0.00046  4.76159E-05 0.00505 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.93080E-01 0.00020  6.92805E-01 0.00021  7.37380E-01 0.00574 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03439E+01 0.00899 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.95725E+01 0.00029  4.01819E+01 0.00033 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.43613E+05 0.00184  5.80957E+05 0.00105  1.20665E+06 0.00054  1.30232E+06 0.00046  1.20144E+06 0.00061  1.29513E+06 0.00053  8.79834E+05 0.00054  7.80015E+05 0.00060  5.96255E+05 0.00052  4.87294E+05 0.00056  4.20090E+05 0.00050  3.78701E+05 0.00053  3.49840E+05 0.00044  3.32714E+05 0.00048  3.24053E+05 0.00056  2.79943E+05 0.00086  2.76625E+05 0.00073  2.74092E+05 0.00068  2.69615E+05 0.00067  5.26842E+05 0.00043  5.09488E+05 0.00042  3.69025E+05 0.00054  2.40143E+05 0.00075  2.77702E+05 0.00067  2.63466E+05 0.00071  2.40406E+05 0.00071  3.95018E+05 0.00055  9.03287E+04 0.00141  1.13387E+05 0.00096  1.02979E+05 0.00096  5.97896E+04 0.00120  1.04034E+05 0.00103  7.09255E+04 0.00122  6.06630E+04 0.00113  1.16499E+04 0.00236  1.15669E+04 0.00214  1.18456E+04 0.00225  1.21784E+04 0.00261  1.21117E+04 0.00215  1.18668E+04 0.00239  1.23441E+04 0.00290  1.15534E+04 0.00281  2.17864E+04 0.00188  3.49126E+04 0.00191  4.43820E+04 0.00122  1.17320E+05 0.00092  1.24425E+05 0.00082  1.39305E+05 0.00073  9.86721E+04 0.00096  7.59495E+04 0.00083  5.94512E+04 0.00085  7.15090E+04 0.00112  1.38703E+05 0.00090  1.90515E+05 0.00086  3.72194E+05 0.00063  5.80820E+05 0.00069  8.76133E+05 0.00062  5.62702E+05 0.00069  4.03424E+05 0.00065  2.90113E+05 0.00062  2.60662E+05 0.00070  2.56575E+05 0.00070  2.14128E+05 0.00070  1.44701E+05 0.00077  1.33657E+05 0.00101  1.18823E+05 0.00076  1.00492E+05 0.00078  7.90480E+04 0.00072  5.27771E+04 0.00112  1.86461E+04 0.00139 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.03481E+00 0.00033 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  3.77316E+01 0.00021  1.33870E+01 0.00043 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37725E-01 6.6E-05  1.43987E+00 0.00021 ];
INF_CAPT                  (idx, [1:   4]) = [  6.28966E-03 0.00046  2.53424E-02 0.00015 ];
INF_ABS                   (idx, [1:   4]) = [  8.06063E-03 0.00040  5.14947E-02 0.00036 ];
INF_FISS                  (idx, [1:   4]) = [  1.77097E-03 0.00031  2.61523E-02 0.00057 ];
INF_NSF                   (idx, [1:   4]) = [  4.59291E-03 0.00032  6.37254E-02 0.00057 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.59344E+00 4.5E-05  2.43670E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04277E+02 4.5E-06  2.02270E+02 3.8E-09 ];
INF_INVV                  (idx, [1:   4]) = [  6.04164E-08 0.00029  2.58428E-06 7.5E-05 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29668E-01 7.1E-05  1.38837E+00 0.00023 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43254E-01 0.00012  3.60771E-01 0.00036 ];
INF_SCATT2                (idx, [1:   4]) = [  9.59247E-02 0.00023  8.52051E-02 0.00058 ];
INF_SCATT3                (idx, [1:   4]) = [  7.39834E-03 0.00208  2.55764E-02 0.00140 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02596E-02 0.00145 -8.03184E-03 0.00490 ];
INF_SCATT5                (idx, [1:   4]) = [  1.01020E-04 0.12137  6.01923E-03 0.00397 ];
INF_SCATT6                (idx, [1:   4]) = [  5.01683E-03 0.00223 -1.54639E-02 0.00175 ];
INF_SCATT7                (idx, [1:   4]) = [  7.31401E-04 0.01594  4.36894E-04 0.05540 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29707E-01 7.1E-05  1.38837E+00 0.00023 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43255E-01 0.00012  3.60771E-01 0.00036 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.59252E-02 0.00023  8.52051E-02 0.00058 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.39833E-03 0.00208  2.55764E-02 0.00140 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02594E-02 0.00145 -8.03184E-03 0.00490 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.00992E-04 0.12155  6.01923E-03 0.00397 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.01689E-03 0.00224 -1.54639E-02 0.00175 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.31340E-04 0.01592  4.36894E-04 0.05540 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.14950E-01 0.00019  9.41218E-01 0.00018 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55075E+00 0.00019  3.54151E-01 0.00018 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.02137E-03 0.00039  5.14947E-02 0.00036 ];
INF_REMXS                 (idx, [1:   4]) = [  2.67200E-02 0.00016  5.24320E-02 0.00044 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11005E-01 6.9E-05  1.86629E-02 0.00023  9.38995E-04 0.00273  1.38744E+00 0.00023 ];
INF_S1                    (idx, [1:   8]) = [  2.37795E-01 0.00012  5.45846E-03 0.00058  3.94956E-04 0.00445  3.60376E-01 0.00036 ];
INF_S2                    (idx, [1:   8]) = [  9.75221E-02 0.00022 -1.59740E-03 0.00138  2.15675E-04 0.00611  8.49894E-02 0.00058 ];
INF_S3                    (idx, [1:   8]) = [  9.30924E-03 0.00165 -1.91090E-03 0.00094  7.74733E-05 0.01506  2.54989E-02 0.00139 ];
INF_S4                    (idx, [1:   8]) = [ -9.61603E-03 0.00155 -6.43541E-04 0.00359  2.49830E-07 1.00000 -8.03209E-03 0.00489 ];
INF_S5                    (idx, [1:   8]) = [  8.96625E-05 0.13791  1.13573E-05 0.17389 -3.15216E-05 0.02531  6.05075E-03 0.00396 ];
INF_S6                    (idx, [1:   8]) = [  5.16551E-03 0.00210 -1.48678E-04 0.01277 -3.98377E-05 0.01931 -1.54241E-02 0.00177 ];
INF_S7                    (idx, [1:   8]) = [  9.06163E-04 0.01267 -1.74762E-04 0.00833 -3.74494E-05 0.02117  4.74344E-04 0.05043 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11044E-01 6.9E-05  1.86629E-02 0.00023  9.38995E-04 0.00273  1.38744E+00 0.00023 ];
INF_SP1                   (idx, [1:   8]) = [  2.37797E-01 0.00012  5.45846E-03 0.00058  3.94956E-04 0.00445  3.60376E-01 0.00036 ];
INF_SP2                   (idx, [1:   8]) = [  9.75226E-02 0.00022 -1.59740E-03 0.00138  2.15675E-04 0.00611  8.49894E-02 0.00058 ];
INF_SP3                   (idx, [1:   8]) = [  9.30923E-03 0.00165 -1.91090E-03 0.00094  7.74733E-05 0.01506  2.54989E-02 0.00139 ];
INF_SP4                   (idx, [1:   8]) = [ -9.61586E-03 0.00155 -6.43541E-04 0.00359  2.49830E-07 1.00000 -8.03209E-03 0.00489 ];
INF_SP5                   (idx, [1:   8]) = [  8.96346E-05 0.13817  1.13573E-05 0.17389 -3.15216E-05 0.02531  6.05075E-03 0.00396 ];
INF_SP6                   (idx, [1:   8]) = [  5.16557E-03 0.00210 -1.48678E-04 0.01277 -3.98377E-05 0.01931 -1.54241E-02 0.00177 ];
INF_SP7                   (idx, [1:   8]) = [  9.06101E-04 0.01266 -1.74762E-04 0.00833 -3.74494E-05 0.02117  4.74344E-04 0.05043 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32723E-01 0.00038  9.82853E-01 0.00286 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34309E-01 0.00079  1.02435E+00 0.00355 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33930E-01 0.00060  1.02607E+00 0.00431 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29983E-01 0.00060  9.08183E-01 0.00336 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43233E+00 0.00038  3.39216E-01 0.00287 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42264E+00 0.00080  3.25506E-01 0.00352 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42494E+00 0.00060  3.25008E-01 0.00429 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.44939E+00 0.00060  3.67134E-01 0.00339 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.07609E-03 0.00557  2.01907E-04 0.03537  1.11046E-03 0.01402  1.12333E-03 0.01504  3.26264E-03 0.00826  1.03207E-03 0.01471  3.45680E-04 0.02422 ];
LAMBDA                    (idx, [1:  14]) = [  8.02983E-01 0.01263  1.24908E-02 2.6E-06  3.16302E-02 0.00024  1.10164E-01 0.00027  3.20962E-01 0.00024  1.34486E+00 0.00018  8.91519E+00 0.00158 ];

