
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0000' ;
WORKING_DIRECTORY         (idx, [1: 33])  = '/home/ajw287/serpent/fmtx_3453422' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Sun Apr 19 21:41:48 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Sun Apr 19 21:48:49 2020' ;

% Run parameters:

POP                       (idx, 1)        = 20000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1587328908 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.16252E-01  1.02244E+00  1.02642E+00  1.01074E+00  1.02414E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  3])  = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.20886E-02 0.00064  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.87911E-01 7.9E-06  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.72583E-01 7.1E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.73018E-01 7.1E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.78897E+00 0.00017  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.00169E+01 0.00029  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.00110E+01 0.00029  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.17483E+01 0.00029  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  5.39224E-01 0.00065  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 10000548 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  2.00011E+04 0.00054 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  2.00011E+04 0.00054 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.50063E+01 ;
RUNNING_TIME              (idx, 1)        =  7.02178E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  2.40833E-02  2.40833E-02 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.83333E-04  2.83333E-04 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.99742E+00  6.99742E+00  0.00000E+00 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.02162E+00  0.00000E+00 ];
CPU_USAGE                 (idx, 1)        = 4.98539 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99940E+00 0.00011 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79250E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 350.78;
MEMSIZE                   (idx, 1)        = 274.93;
XS_MEMSIZE                (idx, 1)        = 127.27;
MAT_MEMSIZE               (idx, 1)        = 15.50;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 130.80;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 75.84;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 126702 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 5 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 15 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 15 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 484 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  0.00000E+00 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.99294E-05 0.00029  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.83142E-01 0.00072 ];
U235_FISS                 (idx, [1:   4]) = [  3.78609E-01 0.00045  9.30169E-01 0.00013 ];
U238_FISS                 (idx, [1:   4]) = [  2.83753E-02 0.00189  6.97074E-02 0.00176 ];
U235_CAPT                 (idx, [1:   4]) = [  7.67928E-02 0.00115  1.30791E-01 0.00109 ];
U238_CAPT                 (idx, [1:   4]) = [  3.08813E-01 0.00055  5.25957E-01 0.00038 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 10000548 1.00000E+07 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 1.47765E+04 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 10000548 1.00148E+07 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 5871310 5.87975E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 4070358 4.07613E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 58880 5.88934E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 10000548 1.00148E+07 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 1.76951E-07 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.32123E-11 0.00019 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.17425E-18 0.00019 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.00231E+00 0.00019 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.07027E-01 0.00019 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  5.87092E-01 0.00013 ];
TOT_ABSRATE               (idx, [1:   2]) = [  9.94119E-01 2.5E-05 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.98588E-01 0.00029 ];
TOT_FLUX                  (idx, [1:   2]) = [  5.16652E+01 0.00021 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  5.88109E-03 0.00421 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  4.00098E+01 0.00024 ];
INI_FMASS                 (idx, 1)        =  1.12516E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12516E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.63527E+00 0.00031 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.39314E-01 0.00024 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.97475E-01 0.00021 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.19738E+00 0.00023 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.96159E-01 2.0E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97944E-01 1.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.00965E+00 0.00039 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.00370E+00 0.00039 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46251E+00 9.7E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02602E+02 1.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.00361E+00 0.00040  9.96663E-01 0.00039  7.03758E-03 0.00562 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.00379E+00 0.00019 ];
COL_KEFF                  (idx, [1:   2]) = [  1.00377E+00 0.00038 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.00379E+00 0.00019 ];
ABS_KINF                  (idx, [1:   2]) = [  1.00974E+00 0.00018 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.77639E+01 0.00014 ];
IMP_ALF                   (idx, [1:   2]) = [  1.77595E+01 5.6E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  3.86331E-07 0.00253 ];
IMP_EALF                  (idx, [1:   2]) = [  3.87520E-07 0.00099 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.27877E-01 0.00201 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.28862E-01 0.00082 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  7.22398E-03 0.00362  2.06156E-04 0.02096  1.12930E-03 0.00894  1.13616E-03 0.00911  3.29248E-03 0.00533  1.10830E-03 0.00960  3.51588E-04 0.01718 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.10035E-01 0.00897  1.23159E-02 0.00533  3.16297E-02 0.00016  1.10255E-01 0.00019  3.20992E-01 0.00015  1.34484E+00 0.00011  8.88643E+00 0.00308 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.14664E-03 0.00581  2.04118E-04 0.03419  1.12031E-03 0.01435  1.14236E-03 0.01444  3.24498E-03 0.00823  1.09437E-03 0.01475  3.40496E-04 0.02664 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.00630E-01 0.01346  1.24908E-02 2.6E-06  3.16307E-02 0.00024  1.10233E-01 0.00028  3.21037E-01 0.00024  1.34455E+00 0.00017  8.92433E+00 0.00163 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  4.11035E-05 0.00087  4.10800E-05 0.00087  4.43798E-05 0.00766 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  4.12484E-05 0.00076  4.12248E-05 0.00076  4.45361E-05 0.00764 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.01052E-03 0.00581  1.98030E-04 0.03367  1.11090E-03 0.01462  1.10035E-03 0.01500  3.19450E-03 0.00908  1.06169E-03 0.01552  3.45055E-04 0.02764 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.11961E-01 0.01448  1.24908E-02 3.2E-06  3.16372E-02 0.00026  1.10237E-01 0.00034  3.20906E-01 0.00024  1.34409E+00 0.00019  8.91658E+00 0.00192 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  4.06141E-05 0.00192  4.05865E-05 0.00194  4.46263E-05 0.02171 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  4.07578E-05 0.00189  4.07300E-05 0.00190  4.48058E-05 0.02183 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.20623E-03 0.01985  1.79847E-04 0.12381  1.10633E-03 0.05167  1.15574E-03 0.04866  3.24626E-03 0.03025  1.12235E-03 0.04908  3.95713E-04 0.07987 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.75385E-01 0.04514  1.24909E-02 7.9E-06  3.16467E-02 0.00068  1.10246E-01 0.00088  3.21293E-01 0.00089  1.34409E+00 0.00052  8.89177E+00 0.00397 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.19939E-03 0.01918  1.74638E-04 0.12098  1.10817E-03 0.04978  1.16124E-03 0.04726  3.24424E-03 0.02940  1.12268E-03 0.04809  3.88422E-04 0.07922 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.61672E-01 0.04465  1.24909E-02 7.9E-06  3.16479E-02 0.00067  1.10258E-01 0.00088  3.21287E-01 0.00088  1.34409E+00 0.00051  8.88672E+00 0.00393 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.77907E+02 0.02003 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  4.08767E-05 0.00056 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  4.10209E-05 0.00037 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.04708E-03 0.00317 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.72442E+02 0.00327 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  7.36369E-07 0.00035 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.95614E-06 0.00029  2.95590E-06 0.00029  2.98863E-06 0.00328 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  5.14744E-05 0.00046  5.14889E-05 0.00046  4.95308E-05 0.00508 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.95762E-01 0.00021  6.95596E-01 0.00021  7.23496E-01 0.00577 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.01333E+01 0.00888 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.00110E+01 0.00029  4.11632E+01 0.00035 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.42878E+05 0.00190  5.82811E+05 0.00095  1.20825E+06 0.00074  1.30300E+06 0.00051  1.20387E+06 0.00054  1.29764E+06 0.00055  8.81687E+05 0.00055  7.81366E+05 0.00040  5.98178E+05 0.00075  4.88527E+05 0.00065  4.21321E+05 0.00061  3.79987E+05 0.00071  3.50752E+05 0.00073  3.33261E+05 0.00079  3.25280E+05 0.00056  2.80914E+05 0.00074  2.77469E+05 0.00059  2.75052E+05 0.00062  2.70609E+05 0.00068  5.28909E+05 0.00056  5.12141E+05 0.00037  3.70240E+05 0.00072  2.41472E+05 0.00059  2.79410E+05 0.00055  2.64769E+05 0.00077  2.42020E+05 0.00064  3.98380E+05 0.00033  9.10617E+04 0.00097  1.14244E+05 0.00109  1.03704E+05 0.00108  6.03748E+04 0.00132  1.04927E+05 0.00121  7.14858E+04 0.00120  6.12467E+04 0.00144  1.17717E+04 0.00263  1.17052E+04 0.00259  1.19580E+04 0.00225  1.23426E+04 0.00268  1.21842E+04 0.00257  1.19346E+04 0.00210  1.24265E+04 0.00221  1.17056E+04 0.00321  2.19923E+04 0.00196  3.50083E+04 0.00198  4.48245E+04 0.00175  1.18401E+05 0.00083  1.25795E+05 0.00082  1.41050E+05 0.00067  1.00705E+05 0.00093  7.74421E+04 0.00091  6.07963E+04 0.00136  7.31573E+04 0.00102  1.42153E+05 0.00080  1.96107E+05 0.00072  3.83229E+05 0.00073  5.99139E+05 0.00066  9.04879E+05 0.00060  5.81609E+05 0.00073  4.17291E+05 0.00080  3.00219E+05 0.00071  2.69900E+05 0.00066  2.65746E+05 0.00077  2.21816E+05 0.00077  1.49846E+05 0.00091  1.38334E+05 0.00079  1.23112E+05 0.00078  1.04119E+05 0.00099  8.18131E+04 0.00079  5.47030E+04 0.00099  1.92171E+04 0.00164 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.00972E+00 0.00044 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  3.78552E+01 0.00025  1.38109E+01 0.00039 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37518E-01 7.4E-05  1.42373E+00 0.00020 ];
INF_CAPT                  (idx, [1:   4]) = [  6.29654E-03 0.00043  2.52523E-02 0.00015 ];
INF_ABS                   (idx, [1:   4]) = [  7.97586E-03 0.00034  5.01222E-02 0.00036 ];
INF_FISS                  (idx, [1:   4]) = [  1.67932E-03 0.00027  2.48700E-02 0.00059 ];
INF_NSF                   (idx, [1:   4]) = [  4.36951E-03 0.00027  6.06006E-02 0.00059 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.60195E+00 3.7E-05  2.43670E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04396E+02 2.8E-06  2.02270E+02 3.8E-09 ];
INF_INVV                  (idx, [1:   4]) = [  6.06846E-08 0.00033  2.58844E-06 9.0E-05 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29546E-01 7.8E-05  1.37360E+00 0.00022 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42866E-01 0.00013  3.55617E-01 0.00034 ];
INF_SCATT2                (idx, [1:   4]) = [  9.57782E-02 0.00020  8.39673E-02 0.00068 ];
INF_SCATT3                (idx, [1:   4]) = [  7.40595E-03 0.00185  2.52534E-02 0.00175 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02429E-02 0.00126 -7.95964E-03 0.00473 ];
INF_SCATT5                (idx, [1:   4]) = [  8.46569E-05 0.14601  5.90919E-03 0.00566 ];
INF_SCATT6                (idx, [1:   4]) = [  5.00606E-03 0.00184 -1.52519E-02 0.00197 ];
INF_SCATT7                (idx, [1:   4]) = [  7.47438E-04 0.01319  3.64888E-04 0.06890 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29585E-01 7.8E-05  1.37360E+00 0.00022 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42866E-01 0.00014  3.55617E-01 0.00034 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.57786E-02 0.00020  8.39673E-02 0.00068 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.40573E-03 0.00185  2.52534E-02 0.00175 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02429E-02 0.00126 -7.95964E-03 0.00473 ];
INF_SCATTP5               (idx, [1:   4]) = [  8.47648E-05 0.14590  5.90919E-03 0.00566 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.00603E-03 0.00184 -1.52519E-02 0.00197 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.47362E-04 0.01318  3.64888E-04 0.06890 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.15299E-01 0.00017  9.33665E-01 0.00017 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.54823E+00 0.00017  3.57016E-01 0.00017 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  7.93688E-03 0.00034  5.01222E-02 0.00036 ];
INF_REMXS                 (idx, [1:   4]) = [  2.66516E-02 0.00019  5.10512E-02 0.00049 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10867E-01 7.4E-05  1.86792E-02 0.00030  9.17505E-04 0.00352  1.37268E+00 0.00023 ];
INF_S1                    (idx, [1:   8]) = [  2.37408E-01 0.00013  5.45748E-03 0.00062  3.85672E-04 0.00404  3.55231E-01 0.00034 ];
INF_S2                    (idx, [1:   8]) = [  9.73754E-02 0.00019 -1.59722E-03 0.00178  2.09617E-04 0.00648  8.37577E-02 0.00067 ];
INF_S3                    (idx, [1:   8]) = [  9.31749E-03 0.00136 -1.91154E-03 0.00101  7.44804E-05 0.01365  2.51789E-02 0.00175 ];
INF_S4                    (idx, [1:   8]) = [ -9.59710E-03 0.00131 -6.45791E-04 0.00288 -7.25965E-07 1.00000 -7.95892E-03 0.00473 ];
INF_S5                    (idx, [1:   8]) = [  7.39727E-05 0.16675  1.06842E-05 0.14203 -3.20355E-05 0.02346  5.94122E-03 0.00564 ];
INF_S6                    (idx, [1:   8]) = [  5.15660E-03 0.00196 -1.50538E-04 0.01224 -4.03650E-05 0.02047 -1.52115E-02 0.00197 ];
INF_S7                    (idx, [1:   8]) = [  9.23489E-04 0.01102 -1.76051E-04 0.00725 -3.54058E-05 0.01878  4.00294E-04 0.06288 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10906E-01 7.4E-05  1.86792E-02 0.00030  9.17505E-04 0.00352  1.37268E+00 0.00023 ];
INF_SP1                   (idx, [1:   8]) = [  2.37409E-01 0.00013  5.45748E-03 0.00062  3.85672E-04 0.00404  3.55231E-01 0.00034 ];
INF_SP2                   (idx, [1:   8]) = [  9.73758E-02 0.00019 -1.59722E-03 0.00178  2.09617E-04 0.00648  8.37577E-02 0.00067 ];
INF_SP3                   (idx, [1:   8]) = [  9.31726E-03 0.00137 -1.91154E-03 0.00101  7.44804E-05 0.01365  2.51789E-02 0.00175 ];
INF_SP4                   (idx, [1:   8]) = [ -9.59711E-03 0.00131 -6.45791E-04 0.00288 -7.25965E-07 1.00000 -7.95892E-03 0.00473 ];
INF_SP5                   (idx, [1:   8]) = [  7.40806E-05 0.16659  1.06842E-05 0.14203 -3.20355E-05 0.02346  5.94122E-03 0.00564 ];
INF_SP6                   (idx, [1:   8]) = [  5.15657E-03 0.00196 -1.50538E-04 0.01224 -4.03650E-05 0.02047 -1.52115E-02 0.00197 ];
INF_SP7                   (idx, [1:   8]) = [  9.23413E-04 0.01101 -1.76051E-04 0.00725 -3.54058E-05 0.01878  4.00294E-04 0.06288 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32632E-01 0.00039  9.62767E-01 0.00347 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33839E-01 0.00066  1.00437E+00 0.00445 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33724E-01 0.00069  9.99622E-01 0.00396 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.30371E-01 0.00072  8.93096E-01 0.00356 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43289E+00 0.00039  3.46325E-01 0.00350 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42550E+00 0.00065  3.32042E-01 0.00451 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42620E+00 0.00069  3.33586E-01 0.00400 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.44696E+00 0.00072  3.73346E-01 0.00354 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.14664E-03 0.00581  2.04118E-04 0.03419  1.12031E-03 0.01435  1.14236E-03 0.01444  3.24498E-03 0.00823  1.09437E-03 0.01475  3.40496E-04 0.02664 ];
LAMBDA                    (idx, [1:  14]) = [  8.00630E-01 0.01346  1.24908E-02 2.6E-06  3.16307E-02 0.00024  1.10233E-01 0.00028  3.21037E-01 0.00024  1.34455E+00 0.00017  8.92433E+00 0.00163 ];

