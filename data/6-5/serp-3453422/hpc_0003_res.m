
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0003' ;
WORKING_DIRECTORY         (idx, [1: 33])  = '/home/ajw287/serpent/fmtx_3453422' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Sun Apr 19 22:02:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Sun Apr 19 22:09:52 2020' ;

% Run parameters:

POP                       (idx, 1)        = 20000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1587330170 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  8.89763E-01  1.02276E+00  1.02977E+00  1.02772E+00  1.02999E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1:  3])  = 'N/A' ;
SFY_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;
NFY_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15584E-02 0.00067  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88442E-01 7.8E-06  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.84851E-01 7.4E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.85267E-01 7.3E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.76121E+00 0.00016  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  4.01858E+01 0.00031  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  4.01778E+01 0.00031  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  1.09865E+01 0.00028  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  5.06253E-01 0.00070  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 10000738 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  2.00015E+04 0.00050 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  2.00015E+04 0.00050 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.50691E+01 ;
RUNNING_TIME              (idx, 1)        =  7.03512E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  2.54333E-02  2.54333E-02 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.16668E-04  3.16668E-04 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.00925E+00  7.00925E+00  0.00000E+00 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.03485E+00  0.00000E+00 ];
CPU_USAGE                 (idx, 1)        = 4.98487 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99958E+00 8.5E-05 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78839E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 370.78;
MEMSIZE                   (idx, 1)        = 288.50;
XS_MEMSIZE                (idx, 1)        = 127.27;
MAT_MEMSIZE               (idx, 1)        = 29.07;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 130.80;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 82.28;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  0.00000E+00 ;
NEUTRON_ERG_NE            (idx, 1)        = 126702 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 5 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 15 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 15 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 0 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 484 ;
TOT_TRANSMU_REA           (idx, 1)        = 0 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  0.00000E+00 ;
TOT_DECAY_HEAT            (idx, 1)        =  0.00000E+00 ;
TOT_SF_RATE               (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  0.00000E+00 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  0.00000E+00 ;
INGESTION_TOXICITY        (idx, 1)        =  0.00000E+00 ;
ACTINIDE_INH_TOX          (idx, 1)        =  0.00000E+00 ;
ACTINIDE_ING_TOX          (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  0.00000E+00 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  0.00000E+00 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  0.00000E+00 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.99413E-05 0.00026  0.00000E+00 0.0E+00 ];

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.27632E-01 0.00074 ];
U235_FISS                 (idx, [1:   4]) = [  3.87998E-01 0.00041  9.32887E-01 0.00013 ];
U238_FISS                 (idx, [1:   4]) = [  2.78621E-02 0.00188  6.69870E-02 0.00179 ];
U235_CAPT                 (idx, [1:   4]) = [  7.95089E-02 0.00111  1.37973E-01 0.00107 ];
U238_CAPT                 (idx, [1:   4]) = [  2.91301E-01 0.00060  5.05491E-01 0.00042 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 10000738 1.00000E+07 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 1.43103E+04 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 10000738 1.00143E+07 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 5761469 5.76945E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 4158483 4.16405E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 80786 8.08097E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 10000738 1.00143E+07 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -2.23517E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   2]) = [  1.34932E-11 0.00019 ];
TOT_POWDENS               (idx, [1:   2]) = [  1.19923E-18 0.00019 ];
TOT_GENRATE               (idx, [1:   2]) = [  1.02333E+00 0.00019 ];
TOT_FISSRATE              (idx, [1:   2]) = [  4.15711E-01 0.00019 ];
TOT_CAPTRATE              (idx, [1:   2]) = [  5.76218E-01 0.00013 ];
TOT_ABSRATE               (idx, [1:   2]) = [  9.91929E-01 2.9E-05 ];
TOT_SRCRATE               (idx, [1:   2]) = [  9.98825E-01 0.00026 ];
TOT_FLUX                  (idx, [1:   2]) = [  5.11585E+01 0.00020 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  8.07130E-03 0.00358 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  1.00000E+00 0.0E+00 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  4.01841E+01 0.00024 ];
INI_FMASS                 (idx, 1)        =  1.12516E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12516E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.67659E+00 0.00029 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.32651E-01 0.00022 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.97600E-01 0.00021 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.20599E+00 0.00023 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94783E-01 2.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97121E-01 1.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.03339E+00 0.00035 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.02504E+00 0.00035 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46164E+00 9.7E-06 ];
FISSE                     (idx, [1:   2]) = [  2.02588E+02 1.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.02510E+00 0.00037  1.01784E+00 0.00036  7.19793E-03 0.00589 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.02482E+00 0.00019 ];
COL_KEFF                  (idx, [1:   2]) = [  1.02457E+00 0.00034 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.02482E+00 0.00019 ];
ABS_KINF                  (idx, [1:   2]) = [  1.03317E+00 0.00018 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.77265E+01 0.00013 ];
IMP_ALF                   (idx, [1:   2]) = [  1.77267E+01 5.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.00983E-07 0.00237 ];
IMP_EALF                  (idx, [1:   2]) = [  4.00449E-07 0.00101 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.21369E-01 0.00203 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.21285E-01 0.00086 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  7.02726E-03 0.00394  2.02184E-04 0.02397  1.12436E-03 0.00964  1.08631E-03 0.00953  3.23022E-03 0.00549  1.04216E-03 0.00967  3.42022E-04 0.01697 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.03198E-01 0.00882  1.22909E-02 0.00571  3.16434E-02 0.00015  1.10223E-01 0.00021  3.20864E-01 0.00016  1.34508E+00 0.00010  8.90998E+00 0.00107 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.07843E-03 0.00596  1.99702E-04 0.03527  1.15058E-03 0.01425  1.10383E-03 0.01465  3.22367E-03 0.00840  1.05051E-03 0.01457  3.50139E-04 0.02619 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.08629E-01 0.01346  1.24908E-02 2.6E-06  3.16420E-02 0.00023  1.10243E-01 0.00031  3.20910E-01 0.00024  1.34482E+00 0.00016  8.89745E+00 0.00149 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.76880E-05 0.00082  3.76678E-05 0.00082  4.05406E-05 0.00768 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.86312E-05 0.00073  3.86105E-05 0.00073  4.15575E-05 0.00769 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.02889E-03 0.00598  1.94750E-04 0.03830  1.13697E-03 0.01520  1.08609E-03 0.01551  3.22682E-03 0.00828  1.02749E-03 0.01522  3.56770E-04 0.02610 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.17173E-01 0.01389  1.24908E-02 3.2E-06  3.16487E-02 0.00025  1.10187E-01 0.00035  3.20848E-01 0.00026  1.34494E+00 0.00018  8.89401E+00 0.00184 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.77208E-05 0.00195  3.77007E-05 0.00196  4.15317E-05 0.02135 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.86647E-05 0.00191  3.86440E-05 0.00192  4.25727E-05 0.02134 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.91839E-03 0.01987  2.11700E-04 0.10979  1.20766E-03 0.04656  1.06743E-03 0.04860  3.10252E-03 0.02930  9.59405E-04 0.05204  3.69679E-04 0.08124 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.28353E-01 0.04479  1.24907E-02 4.8E-06  3.16223E-02 0.00073  1.10232E-01 0.00090  3.20811E-01 0.00080  1.34523E+00 0.00052  8.92142E+00 0.00409 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.87270E-03 0.01909  2.10390E-04 0.10745  1.18706E-03 0.04439  1.06419E-03 0.04802  3.08472E-03 0.02852  9.57176E-04 0.05030  3.69160E-04 0.07853 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.36441E-01 0.04482  1.24907E-02 4.8E-06  3.16198E-02 0.00072  1.10236E-01 0.00088  3.20798E-01 0.00079  1.34528E+00 0.00052  8.92160E+00 0.00408 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.83815E+02 0.01995 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.77202E-05 0.00052 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.86641E-05 0.00034 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.06868E-03 0.00361 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.87435E+02 0.00368 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  7.33913E-07 0.00037 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.91242E-06 0.00030  2.91222E-06 0.00030  2.93840E-06 0.00334 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  5.08441E-05 0.00046  5.08603E-05 0.00046  4.85996E-05 0.00519 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.95192E-01 0.00021  6.94952E-01 0.00022  7.34755E-01 0.00604 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03950E+01 0.00924 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  4.01778E+01 0.00031  4.00466E+01 0.00033 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  1.43234E+05 0.00232  5.80975E+05 0.00118  1.20496E+06 0.00057  1.29939E+06 0.00064  1.19873E+06 0.00060  1.28608E+06 0.00040  8.75213E+05 0.00037  7.74464E+05 0.00057  5.91819E+05 0.00057  4.83752E+05 0.00072  4.17261E+05 0.00060  3.76176E+05 0.00053  3.47406E+05 0.00059  3.30009E+05 0.00070  3.21575E+05 0.00044  2.78063E+05 0.00072  2.74536E+05 0.00061  2.71961E+05 0.00065  2.67759E+05 0.00068  5.23483E+05 0.00058  5.06156E+05 0.00059  3.66133E+05 0.00062  2.38181E+05 0.00059  2.76103E+05 0.00064  2.61760E+05 0.00060  2.38658E+05 0.00060  3.93215E+05 0.00048  8.97104E+04 0.00129  1.12820E+05 0.00114  1.02006E+05 0.00071  5.95630E+04 0.00118  1.03336E+05 0.00080  7.04646E+04 0.00128  6.01175E+04 0.00138  1.15519E+04 0.00239  1.14853E+04 0.00278  1.17351E+04 0.00205  1.21751E+04 0.00267  1.20214E+04 0.00275  1.18490E+04 0.00280  1.22419E+04 0.00276  1.15203E+04 0.00306  2.16770E+04 0.00235  3.46405E+04 0.00197  4.41094E+04 0.00094  1.16359E+05 0.00110  1.23636E+05 0.00108  1.38579E+05 0.00096  9.85713E+04 0.00096  7.59716E+04 0.00116  5.94968E+04 0.00120  7.17401E+04 0.00099  1.39330E+05 0.00109  1.91855E+05 0.00079  3.76441E+05 0.00062  5.89968E+05 0.00072  8.91466E+05 0.00060  5.73731E+05 0.00054  4.11915E+05 0.00073  2.96321E+05 0.00063  2.66426E+05 0.00082  2.62429E+05 0.00068  2.19171E+05 0.00068  1.48226E+05 0.00084  1.36853E+05 0.00081  1.21821E+05 0.00095  1.02982E+05 0.00088  8.10893E+04 0.00098  5.41646E+04 0.00089  1.91643E+04 0.00129 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.03292E+00 0.00028 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  3.75469E+01 0.00026  1.36123E+01 0.00031 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38644E-01 6.8E-05  1.46637E+00 0.00019 ];
INF_CAPT                  (idx, [1:   4]) = [  6.22482E-03 0.00035  2.51620E-02 0.00014 ];
INF_ABS                   (idx, [1:   4]) = [  8.02182E-03 0.00034  5.07458E-02 0.00028 ];
INF_FISS                  (idx, [1:   4]) = [  1.79700E-03 0.00046  2.55838E-02 0.00044 ];
INF_NSF                   (idx, [1:   4]) = [  4.65485E-03 0.00048  6.23401E-02 0.00044 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.59035E+00 3.9E-05  2.43670E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04232E+02 3.7E-06  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  6.03393E-08 0.00029  2.59188E-06 9.6E-05 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.30627E-01 6.7E-05  1.41559E+00 0.00021 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44648E-01 0.00013  3.68982E-01 0.00029 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64584E-02 0.00024  8.69194E-02 0.00051 ];
INF_SCATT3                (idx, [1:   4]) = [  7.38337E-03 0.00145  2.61121E-02 0.00106 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03642E-02 0.00159 -8.28457E-03 0.00419 ];
INF_SCATT5                (idx, [1:   4]) = [  8.61486E-05 0.14769  6.19260E-03 0.00588 ];
INF_SCATT6                (idx, [1:   4]) = [  5.04441E-03 0.00250 -1.59783E-02 0.00163 ];
INF_SCATT7                (idx, [1:   4]) = [  7.42777E-04 0.01525  3.86046E-04 0.07856 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.30665E-01 6.7E-05  1.41559E+00 0.00021 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44648E-01 0.00013  3.68982E-01 0.00029 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64587E-02 0.00024  8.69194E-02 0.00051 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.38355E-03 0.00145  2.61121E-02 0.00106 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03643E-02 0.00159 -8.28457E-03 0.00419 ];
INF_SCATTP5               (idx, [1:   4]) = [  8.62609E-05 0.14728  6.19260E-03 0.00588 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.04441E-03 0.00250 -1.59783E-02 0.00163 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.42832E-04 0.01525  3.86046E-04 0.07856 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.14180E-01 0.00017  9.54993E-01 0.00016 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55632E+00 0.00017  3.49043E-01 0.00016 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  7.98375E-03 0.00034  5.07458E-02 0.00028 ];
INF_REMXS                 (idx, [1:   4]) = [  2.68349E-02 0.00017  5.16941E-02 0.00050 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11809E-01 6.6E-05  1.88174E-02 0.00022  9.20145E-04 0.00240  1.41467E+00 0.00021 ];
INF_S1                    (idx, [1:   8]) = [  2.39132E-01 0.00013  5.51569E-03 0.00062  3.86932E-04 0.00408  3.68595E-01 0.00029 ];
INF_S2                    (idx, [1:   8]) = [  9.80661E-02 0.00023 -1.60771E-03 0.00141  2.11610E-04 0.00583  8.67078E-02 0.00051 ];
INF_S3                    (idx, [1:   8]) = [  9.31864E-03 0.00114 -1.93527E-03 0.00117  7.53152E-05 0.00955  2.60368E-02 0.00106 ];
INF_S4                    (idx, [1:   8]) = [ -9.71174E-03 0.00166 -6.52508E-04 0.00259  1.89777E-06 0.55627 -8.28646E-03 0.00426 ];
INF_S5                    (idx, [1:   8]) = [  7.42618E-05 0.17413  1.18868E-05 0.16611 -3.05707E-05 0.03163  6.22317E-03 0.00587 ];
INF_S6                    (idx, [1:   8]) = [  5.19369E-03 0.00237 -1.49275E-04 0.01312 -3.84331E-05 0.02373 -1.59398E-02 0.00162 ];
INF_S7                    (idx, [1:   8]) = [  9.18723E-04 0.01235 -1.75946E-04 0.00631 -3.54907E-05 0.01878  4.21536E-04 0.07150 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11847E-01 6.5E-05  1.88174E-02 0.00022  9.20145E-04 0.00240  1.41467E+00 0.00021 ];
INF_SP1                   (idx, [1:   8]) = [  2.39132E-01 0.00013  5.51569E-03 0.00062  3.86932E-04 0.00408  3.68595E-01 0.00029 ];
INF_SP2                   (idx, [1:   8]) = [  9.80664E-02 0.00023 -1.60771E-03 0.00141  2.11610E-04 0.00583  8.67078E-02 0.00051 ];
INF_SP3                   (idx, [1:   8]) = [  9.31882E-03 0.00114 -1.93527E-03 0.00117  7.53152E-05 0.00955  2.60368E-02 0.00106 ];
INF_SP4                   (idx, [1:   8]) = [ -9.71183E-03 0.00166 -6.52508E-04 0.00259  1.89777E-06 0.55627 -8.28646E-03 0.00426 ];
INF_SP5                   (idx, [1:   8]) = [  7.43741E-05 0.17361  1.18868E-05 0.16611 -3.05707E-05 0.03163  6.22317E-03 0.00587 ];
INF_SP6                   (idx, [1:   8]) = [  5.19368E-03 0.00237 -1.49275E-04 0.01312 -3.84331E-05 0.02373 -1.59398E-02 0.00162 ];
INF_SP7                   (idx, [1:   8]) = [  9.18778E-04 0.01234 -1.75946E-04 0.00631 -3.54907E-05 0.01878  4.21536E-04 0.07150 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32896E-01 0.00045  1.02705E+00 0.00380 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34514E-01 0.00064  1.07877E+00 0.00364 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34424E-01 0.00067  1.08608E+00 0.00508 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29820E-01 0.00087  9.31971E-01 0.00403 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43126E+00 0.00045  3.24668E-01 0.00380 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42140E+00 0.00064  3.09092E-01 0.00362 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42194E+00 0.00067  3.07109E-01 0.00518 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45044E+00 0.00087  3.57803E-01 0.00399 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.07843E-03 0.00596  1.99702E-04 0.03527  1.15058E-03 0.01425  1.10383E-03 0.01465  3.22367E-03 0.00840  1.05051E-03 0.01457  3.50139E-04 0.02619 ];
LAMBDA                    (idx, [1:  14]) = [  8.08629E-01 0.01346  1.24908E-02 2.6E-06  3.16420E-02 0.00023  1.10243E-01 0.00031  3.20910E-01 0.00024  1.34482E+00 0.00016  8.89745E+00 0.00149 ];

