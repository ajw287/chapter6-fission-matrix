import sys, os
import data_store as ds

def main():
    pickle_dir = sys.argv[1]
    file_list = [x.split(".")[0] for x in os.listdir(pickle_dir) if x.endswith(".pickle")]
    #run through the files in order.
    print (str(file_list))
    for data_file in sorted(file_list):#, key=lambda a: a.split("/")[2]):
        os.chdir(pickle_dir)
        os.system("python3 ./parse_detectors.py " + data_file)
        #os.chdir("../..")

if __name__ == "__main__":
    main()
