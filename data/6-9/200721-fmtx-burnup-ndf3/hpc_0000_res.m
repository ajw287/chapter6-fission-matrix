
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0000' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:31 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Mon Jul 20 23:44:31 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284831 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99472E-01  1.00491E+00  1.00077E+00  1.00220E+00  9.92649E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12301E-02 0.00104  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88770E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.95868E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.96281E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.72635E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.86724E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.86632E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.89101E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.61442E-01 0.00108  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000607 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00012E+04 0.00079 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00012E+04 0.00079 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.83083E+01 ;
RUNNING_TIME              (idx, 1)        =  4.00095E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  4.22850E-01  4.22850E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.33333E-03  1.33333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.57648E+00  3.57648E+00  0.00000E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.00055E+00  0.00000E+00 ];
CPU_USAGE                 (idx, 1)        = 4.57599 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99883E+00 0.00021 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  8.82507E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.62280E+08 ;
TOT_DECAY_HEAT            (idx, 1)        =  6.56869E-04 ;
TOT_SF_RATE               (idx, 1)        =  7.44232E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  8.62280E+08 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  6.56869E-04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  7.89137E+03 ;
INGESTION_TOXICITY        (idx, 1)        =  4.16532E+01 ;
ACTINIDE_INH_TOX          (idx, 1)        =  7.89137E+03 ;
ACTINIDE_ING_TOX          (idx, 1)        =  4.16532E+01 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  1.07930E+08 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  8.61936E+08 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  3.50807E+08 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.13111E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 0 ;
BURNUP                     (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
BURN_DAYS                 (idx, 1)        =  0.00000E+00 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.38014E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  1.30147E+16 0.00060  9.37768E-01 0.00017 ];
U238_FISS                 (idx, [1:   4]) = [  8.62217E+14 0.00271  6.21198E-02 0.00255 ];
U235_CAPT                 (idx, [1:   4]) = [  2.74744E+15 0.00155  1.59827E-01 0.00138 ];
U238_CAPT                 (idx, [1:   4]) = [  8.41816E+15 0.00097  4.89705E-01 0.00064 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000607 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.10875E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000607 5.00711E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2741482 2.74502E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2213361 2.21631E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45764 4.57768E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000607 5.00711E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.89179E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.41156E+16 1.1E-05  3.41156E+16 1.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38674E+16 1.3E-06  1.38674E+16 1.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.71936E+16 0.00049  1.12786E+16 0.00051  5.91498E+15 0.00097 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.10610E+16 0.00027  2.51460E+16 0.00023  5.91498E+15 0.00097 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.13111E+16 0.00051  3.13111E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.55672E+18 0.00049  4.14478E+17 0.00047  1.14225E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.86693E+14 0.00482 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.13477E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.21218E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12515E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.74836E+00 0.00042 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.47505E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.87695E-01 0.00030 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.22462E+00 0.00033 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94058E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96767E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.10057E+00 0.00053 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.09049E+00 0.00053 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46013E+00 1.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02565E+02 1.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.09050E+00 0.00055  1.08264E+00 0.00053  7.84986E-03 0.00793 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.08990E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.08971E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.08990E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.09998E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.76473E+01 0.00018 ];
IMP_ALF                   (idx, [1:   2]) = [  1.76505E+01 7.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.34524E-07 0.00323 ];
IMP_EALF                  (idx, [1:   2]) = [  4.32245E-07 0.00137 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.08397E-01 0.00285 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.08292E-01 0.00111 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.57181E-03 0.00548  1.86712E-04 0.03031  1.04169E-03 0.01312  1.02284E-03 0.01358  3.03003E-03 0.00795  9.68380E-04 0.01385  3.22144E-04 0.02517 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.05738E-01 0.01324  1.09419E-02 0.01684  3.16496E-02 0.00021  1.10173E-01 0.00028  3.20623E-01 0.00021  1.34540E+00 0.00017  8.60086E+00 0.00830 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.25791E-03 0.00799  2.10452E-04 0.04715  1.15616E-03 0.02143  1.13888E-03 0.02028  3.35090E-03 0.01181  1.05285E-03 0.02121  3.48673E-04 0.03637 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.92625E-01 0.01888  1.24907E-02 2.5E-06  3.16425E-02 0.00030  1.10174E-01 0.00042  3.20686E-01 0.00033  1.34555E+00 0.00024  8.86977E+00 0.00193 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.06256E-05 0.00112  3.06096E-05 0.00112  3.27798E-05 0.01078 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.33927E-05 0.00101  3.33753E-05 0.00101  3.57380E-05 0.01074 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.18577E-03 0.00811  2.07826E-04 0.04767  1.14888E-03 0.02085  1.11826E-03 0.02070  3.30604E-03 0.01200  1.05815E-03 0.02162  3.46619E-04 0.03722 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.97375E-01 0.01980  1.24907E-02 3.0E-06  3.16561E-02 0.00031  1.10217E-01 0.00048  3.20714E-01 0.00032  1.34551E+00 0.00026  8.85569E+00 0.00230 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.07481E-05 0.00257  3.07230E-05 0.00260  3.35653E-05 0.02760 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.35279E-05 0.00256  3.35005E-05 0.00258  3.65961E-05 0.02758 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.39741E-03 0.02458  2.69635E-04 0.12542  1.10176E-03 0.06225  1.18162E-03 0.06517  3.45348E-03 0.03710  1.04740E-03 0.06825  3.43510E-04 0.13307 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.61716E-01 0.06553  1.24908E-02 7.1E-06  3.16360E-02 0.00086  1.10235E-01 0.00108  3.20398E-01 0.00093  1.34738E+00 0.00057  8.83986E+00 0.00519 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.34157E-03 0.02428  2.60501E-04 0.12060  1.11285E-03 0.05985  1.13300E-03 0.06361  3.43662E-03 0.03620  1.05307E-03 0.06710  3.45530E-04 0.12644 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.69032E-01 0.06410  1.24908E-02 7.1E-06  3.16335E-02 0.00087  1.10235E-01 0.00107  3.20391E-01 0.00092  1.34735E+00 0.00057  8.84260E+00 0.00524 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.42312E+02 0.02493 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.07002E-05 0.00075 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.34733E-05 0.00049 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.33266E-03 0.00479 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.38941E+02 0.00490 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.87034E-07 0.00057 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87485E-06 0.00043  2.87459E-06 0.00043  2.90799E-06 0.00465 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.66434E-05 0.00073  4.66669E-05 0.00073  4.34055E-05 0.00774 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.85011E-01 0.00030  6.84479E-01 0.00030  7.76760E-01 0.00843 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02186E+01 0.01289 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.86632E+01 0.00044  3.76176E+01 0.00046 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.14354E+04 0.00324  2.90593E+05 0.00178  6.02334E+05 0.00096  6.49314E+05 0.00080  5.96374E+05 0.00066  6.40748E+05 0.00049  4.36021E+05 0.00051  3.84978E+05 0.00076  2.94249E+05 0.00083  2.40714E+05 0.00085  2.07424E+05 0.00068  1.86707E+05 0.00086  1.72639E+05 0.00089  1.63829E+05 0.00069  1.59539E+05 0.00074  1.37698E+05 0.00089  1.36460E+05 0.00105  1.34955E+05 0.00103  1.32813E+05 0.00109  2.59328E+05 0.00073  2.50602E+05 0.00072  1.81177E+05 0.00076  1.17537E+05 0.00107  1.35608E+05 0.00084  1.28683E+05 0.00108  1.17043E+05 0.00092  1.93156E+05 0.00065  4.40139E+04 0.00139  5.51830E+04 0.00151  5.03470E+04 0.00160  2.92140E+04 0.00216  5.06533E+04 0.00162  3.44453E+04 0.00157  2.94111E+04 0.00151  5.60223E+03 0.00483  5.56068E+03 0.00362  5.73472E+03 0.00331  5.88886E+03 0.00303  5.84534E+03 0.00332  5.74008E+03 0.00314  5.95093E+03 0.00334  5.59856E+03 0.00434  1.05459E+04 0.00239  1.68395E+04 0.00176  2.14654E+04 0.00208  5.67537E+04 0.00155  6.00558E+04 0.00126  6.66317E+04 0.00122  4.66699E+04 0.00157  3.55806E+04 0.00182  2.77649E+04 0.00134  3.34401E+04 0.00140  6.42553E+04 0.00160  8.83385E+04 0.00133  1.71672E+05 0.00108  2.67646E+05 0.00110  4.03200E+05 0.00093  2.58541E+05 0.00113  1.85466E+05 0.00117  1.33376E+05 0.00152  1.19793E+05 0.00142  1.17981E+05 0.00155  9.83906E+04 0.00127  6.65869E+04 0.00130  6.13898E+04 0.00145  5.46889E+04 0.00113  4.62188E+04 0.00158  3.63531E+04 0.00161  2.43664E+04 0.00122  8.59970E+03 0.00162 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.09978E+00 0.00045 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.16896E+18 0.00042  3.87789E+17 0.00105 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38754E-01 0.00011  1.50191E+00 0.00041 ];
INF_CAPT                  (idx, [1:   4]) = [  6.24535E-03 0.00066  2.55134E-02 0.00028 ];
INF_ABS                   (idx, [1:   4]) = [  8.32431E-03 0.00054  5.50119E-02 0.00069 ];
INF_FISS                  (idx, [1:   4]) = [  2.07896E-03 0.00053  2.94985E-02 0.00106 ];
INF_NSF                   (idx, [1:   4]) = [  5.34376E-03 0.00051  7.18789E-02 0.00106 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.57040E+00 6.7E-05  2.43670E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03953E+02 6.0E-06  2.02270E+02 3.8E-09 ];
INF_INVV                  (idx, [1:   4]) = [  5.95691E-08 0.00049  2.57858E-06 0.00012 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.30427E-01 0.00012  1.44689E+00 0.00045 ];
INF_SCATT1                (idx, [1:   4]) = [  2.45199E-01 0.00024  3.80374E-01 0.00056 ];
INF_SCATT2                (idx, [1:   4]) = [  9.66726E-02 0.00030  8.98970E-02 0.00082 ];
INF_SCATT3                (idx, [1:   4]) = [  7.37997E-03 0.00257  2.69947E-02 0.00190 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.04049E-02 0.00151 -8.57270E-03 0.00615 ];
INF_SCATT5                (idx, [1:   4]) = [  1.05519E-04 0.17650  6.39174E-03 0.00823 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08503E-03 0.00286 -1.64296E-02 0.00280 ];
INF_SCATT7                (idx, [1:   4]) = [  7.58129E-04 0.01930  3.66158E-04 0.13936 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.30465E-01 0.00012  1.44689E+00 0.00045 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.45200E-01 0.00024  3.80374E-01 0.00056 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.66732E-02 0.00030  8.98970E-02 0.00082 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.38018E-03 0.00257  2.69947E-02 0.00190 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.04052E-02 0.00152 -8.57270E-03 0.00615 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.05383E-04 0.17630  6.39174E-03 0.00823 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08507E-03 0.00287 -1.64296E-02 0.00280 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.58252E-04 0.01924  3.66158E-04 0.13936 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13396E-01 0.00028  9.71278E-01 0.00035 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56204E+00 0.00028  3.43191E-01 0.00035 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.28623E-03 0.00053  5.50119E-02 0.00069 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69904E-02 0.00024  5.60018E-02 0.00083 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11763E-01 0.00011  1.86637E-02 0.00038  9.82862E-04 0.00309  1.44590E+00 0.00045 ];
INF_S1                    (idx, [1:   8]) = [  2.39736E-01 0.00024  5.46319E-03 0.00064  4.22943E-04 0.00471  3.79951E-01 0.00056 ];
INF_S2                    (idx, [1:   8]) = [  9.82776E-02 0.00030 -1.60500E-03 0.00238  2.28422E-04 0.00995  8.96686E-02 0.00082 ];
INF_S3                    (idx, [1:   8]) = [  9.30069E-03 0.00215 -1.92072E-03 0.00160  8.36527E-05 0.02002  2.69111E-02 0.00192 ];
INF_S4                    (idx, [1:   8]) = [ -9.75919E-03 0.00154 -6.45661E-04 0.00497 -2.55321E-06 0.54431 -8.57015E-03 0.00615 ];
INF_S5                    (idx, [1:   8]) = [  9.13278E-05 0.18609  1.41911E-05 0.23366 -3.49927E-05 0.03960  6.42673E-03 0.00810 ];
INF_S6                    (idx, [1:   8]) = [  5.23297E-03 0.00261 -1.47944E-04 0.01605 -4.28294E-05 0.03688 -1.63867E-02 0.00278 ];
INF_S7                    (idx, [1:   8]) = [  9.32072E-04 0.01533 -1.73943E-04 0.01357 -3.73250E-05 0.02816  4.03483E-04 0.12622 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11801E-01 0.00011  1.86637E-02 0.00038  9.82862E-04 0.00309  1.44590E+00 0.00045 ];
INF_SP1                   (idx, [1:   8]) = [  2.39737E-01 0.00024  5.46319E-03 0.00064  4.22943E-04 0.00471  3.79951E-01 0.00056 ];
INF_SP2                   (idx, [1:   8]) = [  9.82782E-02 0.00030 -1.60500E-03 0.00238  2.28422E-04 0.00995  8.96686E-02 0.00082 ];
INF_SP3                   (idx, [1:   8]) = [  9.30090E-03 0.00215 -1.92072E-03 0.00160  8.36527E-05 0.02002  2.69111E-02 0.00192 ];
INF_SP4                   (idx, [1:   8]) = [ -9.75949E-03 0.00154 -6.45661E-04 0.00497 -2.55321E-06 0.54431 -8.57015E-03 0.00615 ];
INF_SP5                   (idx, [1:   8]) = [  9.11915E-05 0.18584  1.41911E-05 0.23366 -3.49927E-05 0.03960  6.42673E-03 0.00810 ];
INF_SP6                   (idx, [1:   8]) = [  5.23301E-03 0.00262 -1.47944E-04 0.01605 -4.28294E-05 0.03688 -1.63867E-02 0.00278 ];
INF_SP7                   (idx, [1:   8]) = [  9.32195E-04 0.01529 -1.73943E-04 0.01357 -3.73250E-05 0.02816  4.03483E-04 0.12622 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.33293E-01 0.00058  1.07701E+00 0.00522 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34870E-01 0.00075  1.14713E+00 0.00760 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34892E-01 0.00085  1.14734E+00 0.00580 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.30187E-01 0.00093  9.60330E-01 0.00546 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.42883E+00 0.00058  3.09704E-01 0.00527 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.41925E+00 0.00075  2.90994E-01 0.00780 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.41911E+00 0.00085  2.90762E-01 0.00579 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.44813E+00 0.00093  3.47357E-01 0.00559 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.25791E-03 0.00799  2.10452E-04 0.04715  1.15616E-03 0.02143  1.13888E-03 0.02028  3.35090E-03 0.01181  1.05285E-03 0.02121  3.48673E-04 0.03637 ];
LAMBDA                    (idx, [1:  14]) = [  7.92625E-01 0.01888  1.24907E-02 2.5E-06  3.16425E-02 0.00030  1.10174E-01 0.00042  3.20686E-01 0.00033  1.34555E+00 0.00024  8.86977E+00 0.00193 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0000' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:31 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Mon Jul 20 23:50:58 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284831 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00892E+00  1.00472E+00  9.98516E-01  9.99189E-01  9.88654E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.11904E-02 0.00107  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88810E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.97567E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.97979E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.72052E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.83695E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.83603E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.71091E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.54174E-01 0.00106  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000787 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00016E+04 0.00076 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00016E+04 0.00076 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.04948E+01 ;
RUNNING_TIME              (idx, 1)        =  1.04513E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.22850E-01  4.22850E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.43000E-02  6.35000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.92270E+00  3.56457E+00  2.78165E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  8.07667E-02  2.74000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.75000E-03  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.04511E+01  1.32538E+02 ];
CPU_USAGE                 (idx, 1)        = 4.83144 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99941E+00 0.00021 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.46578E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  7.98438E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.76425E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.44219E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.29762E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  9.20228E+02 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  6.68674E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.67221E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.25431E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  1.78066E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.69968E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  3.76413E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.84345E+06 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  1.40425E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.28174E+11 ;
TE132_ACTIVITY            (idx, 1)        =  2.51513E+14 ;
I131_ACTIVITY             (idx, 1)        =  7.42667E+13 ;
I132_ACTIVITY             (idx, 1)        =  2.43721E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.72623E+07 ;
CS137_ACTIVITY            (idx, 1)        =  1.34647E+11 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  6.66975E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.65336E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.34690E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  9.27198E+16 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.24290E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 1 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E-01  1.00010E-01 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+00 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.51316E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  1.29532E+16 0.00059  9.33907E-01 0.00018 ];
U238_FISS                 (idx, [1:   4]) = [  8.86764E+14 0.00267  6.39279E-02 0.00253 ];
PU239_FISS                (idx, [1:   4]) = [  2.81941E+13 0.01464  2.03304E-03 0.01466 ];
U235_CAPT                 (idx, [1:   4]) = [  2.74262E+15 0.00154  1.49805E-01 0.00143 ];
U238_CAPT                 (idx, [1:   4]) = [  8.61485E+15 0.00098  4.70533E-01 0.00068 ];
PU239_CAPT                (idx, [1:   4]) = [  1.48735E+13 0.02062  8.12767E-04 0.02068 ];
PU240_CAPT                (idx, [1:   4]) = [  1.94314E+11 0.18375  1.06853E-05 0.18379 ];
XE135_CAPT                (idx, [1:   4]) = [  7.30102E+14 0.00297  3.98809E-02 0.00295 ];
SM149_CAPT                (idx, [1:   4]) = [  2.34324E+13 0.01617  1.28015E-03 0.01617 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000787 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.21769E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000787 5.00722E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2819195 2.82281E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2135788 2.13859E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45804 4.58104E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000787 5.00722E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -8.75443E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.41358E+16 1.2E-05  3.41358E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38659E+16 1.3E-06  1.38659E+16 1.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.83202E+16 0.00046  1.22924E+16 0.00047  6.02789E+15 0.00100 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.21861E+16 0.00026  2.61582E+16 0.00022  6.02789E+15 0.00100 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.24290E+16 0.00049  3.24290E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.60427E+18 0.00047  4.26634E+17 0.00045  1.17764E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.97139E+14 0.00472 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.24833E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.24559E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12503E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12503E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.66729E+00 0.00042 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.51644E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.88062E-01 0.00030 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23244E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94065E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96753E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.06266E+00 0.00051 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.05292E+00 0.00051 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46185E+00 1.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02587E+02 1.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.05296E+00 0.00053  1.04537E+00 0.00052  7.55244E-03 0.00812 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.05242E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.05275E+00 0.00048 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.05242E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.06215E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75861E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75793E+01 7.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.62068E-07 0.00337 ];
IMP_EALF                  (idx, [1:   2]) = [  4.64124E-07 0.00139 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.14537E-01 0.00281 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.15619E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.81419E-03 0.00525  1.90081E-04 0.03246  1.09537E-03 0.01263  1.03627E-03 0.01295  3.14251E-03 0.00795  1.01289E-03 0.01406  3.37071E-04 0.02294 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.10238E-01 0.01176  1.04922E-02 0.01954  3.16264E-02 0.00023  1.10253E-01 0.00029  3.20716E-01 0.00021  1.34548E+00 0.00016  8.71788E+00 0.00661 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.21927E-03 0.00759  1.92959E-04 0.04753  1.18849E-03 0.01851  1.10112E-03 0.01928  3.31889E-03 0.01144  1.05836E-03 0.01978  3.59452E-04 0.03296 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.09836E-01 0.01713  1.24907E-02 2.8E-06  3.16115E-02 0.00035  1.10273E-01 0.00043  3.20703E-01 0.00033  1.34607E+00 0.00022  8.90176E+00 0.00207 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.05420E-05 0.00117  3.05274E-05 0.00117  3.24692E-05 0.01198 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.21553E-05 0.00105  3.21399E-05 0.00106  3.41779E-05 0.01195 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.17489E-03 0.00819  1.95866E-04 0.04814  1.16143E-03 0.01955  1.09956E-03 0.02099  3.28445E-03 0.01191  1.07670E-03 0.02093  3.56880E-04 0.03717 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.11442E-01 0.01910  1.24907E-02 3.0E-06  3.16181E-02 0.00040  1.10218E-01 0.00047  3.20654E-01 0.00034  1.34607E+00 0.00026  8.89511E+00 0.00244 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.06542E-05 0.00269  3.06443E-05 0.00269  3.27506E-05 0.03474 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.22723E-05 0.00261  3.22620E-05 0.00262  3.44543E-05 0.03459 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.09653E-03 0.02643  2.28400E-04 0.14484  1.11271E-03 0.06300  1.11580E-03 0.06706  3.23512E-03 0.03954  1.06115E-03 0.06828  3.43352E-04 0.12007 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.11649E-01 0.06336  1.24908E-02 8.1E-06  3.15960E-02 0.00099  1.10230E-01 0.00115  3.21092E-01 0.00102  1.34668E+00 0.00060  8.90459E+00 0.00584 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.10025E-03 0.02547  2.26589E-04 0.14169  1.11141E-03 0.06147  1.13391E-03 0.06484  3.23193E-03 0.03766  1.06534E-03 0.06766  3.31069E-04 0.11978 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.94465E-01 0.06036  1.24908E-02 8.1E-06  3.15949E-02 0.00099  1.10217E-01 0.00113  3.20992E-01 0.00098  1.34666E+00 0.00060  8.90954E+00 0.00581 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.32329E+02 0.02665 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.05944E-05 0.00074 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.22103E-05 0.00051 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.13646E-03 0.00467 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.33372E+02 0.00481 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.76022E-07 0.00060 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87443E-06 0.00040  2.87412E-06 0.00041  2.91319E-06 0.00480 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.55801E-05 0.00077  4.56070E-05 0.00077  4.20075E-05 0.00815 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.85375E-01 0.00029  6.84976E-01 0.00030  7.55183E-01 0.00838 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.01613E+01 0.01213 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.83603E+01 0.00045  3.69770E+01 0.00048 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.15699E+04 0.00342  2.90102E+05 0.00173  6.02571E+05 0.00109  6.48661E+05 0.00067  5.97200E+05 0.00078  6.40592E+05 0.00066  4.35307E+05 0.00054  3.85025E+05 0.00076  2.94372E+05 0.00069  2.40612E+05 0.00072  2.07267E+05 0.00071  1.86805E+05 0.00057  1.72715E+05 0.00101  1.64226E+05 0.00084  1.60210E+05 0.00084  1.37985E+05 0.00096  1.36411E+05 0.00100  1.35205E+05 0.00106  1.32688E+05 0.00098  2.59603E+05 0.00060  2.50392E+05 0.00065  1.80968E+05 0.00088  1.17564E+05 0.00070  1.35938E+05 0.00096  1.28705E+05 0.00088  1.17064E+05 0.00102  1.92946E+05 0.00082  4.40944E+04 0.00125  5.51898E+04 0.00131  5.02986E+04 0.00120  2.92259E+04 0.00196  5.07589E+04 0.00182  3.45690E+04 0.00134  2.95125E+04 0.00210  5.64084E+03 0.00307  5.60528E+03 0.00336  5.73955E+03 0.00314  5.92202E+03 0.00309  5.85030E+03 0.00364  5.76110E+03 0.00347  5.92983E+03 0.00446  5.61396E+03 0.00339  1.05432E+04 0.00318  1.68692E+04 0.00203  2.15415E+04 0.00173  5.68535E+04 0.00143  6.00997E+04 0.00132  6.62953E+04 0.00127  4.64395E+04 0.00158  3.54746E+04 0.00164  2.75696E+04 0.00181  3.30447E+04 0.00198  6.37272E+04 0.00112  8.71107E+04 0.00102  1.68996E+05 0.00107  2.62037E+05 0.00093  3.93826E+05 0.00108  2.52142E+05 0.00101  1.80835E+05 0.00110  1.29881E+05 0.00094  1.16535E+05 0.00120  1.14959E+05 0.00108  9.59267E+04 0.00132  6.48905E+04 0.00125  5.98758E+04 0.00144  5.33792E+04 0.00125  4.51620E+04 0.00100  3.55895E+04 0.00132  2.37846E+04 0.00148  8.40238E+03 0.00183 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.06249E+00 0.00043 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.21094E+18 0.00047  3.93361E+17 0.00090 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38899E-01 0.00010  1.50762E+00 0.00041 ];
INF_CAPT                  (idx, [1:   4]) = [  6.25163E-03 0.00063  2.73305E-02 0.00030 ];
INF_ABS                   (idx, [1:   4]) = [  8.32319E-03 0.00048  5.62077E-02 0.00061 ];
INF_FISS                  (idx, [1:   4]) = [  2.07156E-03 0.00065  2.88772E-02 0.00092 ];
INF_NSF                   (idx, [1:   4]) = [  5.32609E-03 0.00063  7.03948E-02 0.00092 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.57105E+00 6.2E-05  2.43773E+00 4.9E-07 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03961E+02 7.3E-06  2.02283E+02 7.7E-08 ];
INF_INVV                  (idx, [1:   4]) = [  5.96207E-08 0.00038  2.57368E-06 0.00013 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.30583E-01 0.00011  1.45141E+00 0.00045 ];
INF_SCATT1                (idx, [1:   4]) = [  2.45299E-01 0.00017  3.82258E-01 0.00058 ];
INF_SCATT2                (idx, [1:   4]) = [  9.66882E-02 0.00032  9.04254E-02 0.00077 ];
INF_SCATT3                (idx, [1:   4]) = [  7.36616E-03 0.00326  2.71445E-02 0.00196 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.04006E-02 0.00187 -8.56008E-03 0.00659 ];
INF_SCATT5                (idx, [1:   4]) = [  9.91419E-05 0.15603  6.34665E-03 0.00987 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08070E-03 0.00285 -1.64268E-02 0.00288 ];
INF_SCATT7                (idx, [1:   4]) = [  7.42012E-04 0.02130  4.02833E-04 0.10097 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.30622E-01 0.00011  1.45141E+00 0.00045 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.45299E-01 0.00017  3.82258E-01 0.00058 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.66882E-02 0.00032  9.04254E-02 0.00077 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.36630E-03 0.00326  2.71445E-02 0.00196 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.04007E-02 0.00187 -8.56008E-03 0.00659 ];
INF_SCATTP5               (idx, [1:   4]) = [  9.92772E-05 0.15616  6.34665E-03 0.00987 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08081E-03 0.00285 -1.64268E-02 0.00288 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.41635E-04 0.02135  4.02833E-04 0.10097 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13446E-01 0.00025  9.73063E-01 0.00039 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56168E+00 0.00025  3.42562E-01 0.00039 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.28453E-03 0.00047  5.62077E-02 0.00061 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69872E-02 0.00019  5.72130E-02 0.00074 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11912E-01 0.00010  1.86713E-02 0.00029  1.00755E-03 0.00384  1.45041E+00 0.00045 ];
INF_S1                    (idx, [1:   8]) = [  2.39832E-01 0.00017  5.46657E-03 0.00099  4.31440E-04 0.00577  3.81826E-01 0.00058 ];
INF_S2                    (idx, [1:   8]) = [  9.82904E-02 0.00032 -1.60215E-03 0.00283  2.36968E-04 0.00827  9.01884E-02 0.00077 ];
INF_S3                    (idx, [1:   8]) = [  9.28190E-03 0.00261 -1.91574E-03 0.00110  8.34308E-05 0.01656  2.70610E-02 0.00196 ];
INF_S4                    (idx, [1:   8]) = [ -9.75743E-03 0.00198 -6.43189E-04 0.00441 -8.16952E-07 1.00000 -8.55927E-03 0.00655 ];
INF_S5                    (idx, [1:   8]) = [  8.81252E-05 0.17839  1.10168E-05 0.24375 -3.34812E-05 0.02897  6.38013E-03 0.00986 ];
INF_S6                    (idx, [1:   8]) = [  5.23241E-03 0.00284 -1.51715E-04 0.01314 -4.29751E-05 0.02902 -1.63838E-02 0.00288 ];
INF_S7                    (idx, [1:   8]) = [  9.20668E-04 0.01744 -1.78655E-04 0.01205 -3.65634E-05 0.02741  4.39397E-04 0.09228 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11951E-01 0.00010  1.86713E-02 0.00029  1.00755E-03 0.00384  1.45041E+00 0.00045 ];
INF_SP1                   (idx, [1:   8]) = [  2.39833E-01 0.00017  5.46657E-03 0.00099  4.31440E-04 0.00577  3.81826E-01 0.00058 ];
INF_SP2                   (idx, [1:   8]) = [  9.82904E-02 0.00032 -1.60215E-03 0.00283  2.36968E-04 0.00827  9.01884E-02 0.00077 ];
INF_SP3                   (idx, [1:   8]) = [  9.28204E-03 0.00261 -1.91574E-03 0.00110  8.34308E-05 0.01656  2.70610E-02 0.00196 ];
INF_SP4                   (idx, [1:   8]) = [ -9.75749E-03 0.00198 -6.43189E-04 0.00441 -8.16952E-07 1.00000 -8.55927E-03 0.00655 ];
INF_SP5                   (idx, [1:   8]) = [  8.82604E-05 0.17844  1.10168E-05 0.24375 -3.34812E-05 0.02897  6.38013E-03 0.00986 ];
INF_SP6                   (idx, [1:   8]) = [  5.23252E-03 0.00284 -1.51715E-04 0.01314 -4.29751E-05 0.02902 -1.63838E-02 0.00288 ];
INF_SP7                   (idx, [1:   8]) = [  9.20290E-04 0.01747 -1.78655E-04 0.01205 -3.65634E-05 0.02741  4.39397E-04 0.09228 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.33193E-01 0.00054  1.07958E+00 0.00413 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.35150E-01 0.00078  1.14529E+00 0.00554 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34782E-01 0.00067  1.14933E+00 0.00557 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29732E-01 0.00092  9.66316E-01 0.00500 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.42944E+00 0.00054  3.08889E-01 0.00417 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.41756E+00 0.00078  2.91265E-01 0.00562 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.41977E+00 0.00067  2.90242E-01 0.00563 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45099E+00 0.00092  3.45161E-01 0.00503 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.21927E-03 0.00759  1.92959E-04 0.04753  1.18849E-03 0.01851  1.10112E-03 0.01928  3.31889E-03 0.01144  1.05836E-03 0.01978  3.59452E-04 0.03296 ];
LAMBDA                    (idx, [1:  14]) = [  8.09836E-01 0.01713  1.24907E-02 2.8E-06  3.16115E-02 0.00035  1.10273E-01 0.00043  3.20703E-01 0.00033  1.34607E+00 0.00022  8.90176E+00 0.00207 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0000' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:31 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Mon Jul 20 23:57:26 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284831 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00859E+00  1.00364E+00  9.98597E-01  9.99937E-01  9.89231E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12028E-02 0.00108  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88797E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.98179E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.98592E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.71782E+00 0.00024  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.81573E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.81480E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.62056E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.51549E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000944 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00019E+04 0.00075 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00019E+04 0.00075 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  8.27760E+01 ;
RUNNING_TIME              (idx, 1)        =  1.69138E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.22850E-01  4.22850E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  4.15833E-02  7.11667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.63103E+01  3.58625E+00  2.80132E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.37800E-01  2.77500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  6.11666E-03  8.16667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.69137E+01  1.32755E+02 ];
CPU_USAGE                 (idx, 1)        = 4.89398 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00020E+00 0.00016 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.61766E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.86067E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.84673E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.45843E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.71281E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.19714E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.14784E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.72699E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.73862E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.05994E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  9.46717E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.98811E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  1.79191E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  2.36112E+07 ;
SR90_ACTIVITY             (idx, 1)        =  6.36308E+11 ;
TE132_ACTIVITY            (idx, 1)        =  5.63938E+14 ;
I131_ACTIVITY             (idx, 1)        =  2.65545E+14 ;
I132_ACTIVITY             (idx, 1)        =  5.67028E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.98209E+09 ;
CS137_ACTIVITY            (idx, 1)        =  6.73734E+11 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.80302E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.64093E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  7.18586E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.14937E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.27783E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 2 ;
BURNUP                     (idx, [1:  2])  = [  5.00000E-01  5.00044E-01 ];
BURN_DAYS                 (idx, 1)        =  1.25000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.51145E-01 0.00105 ];
U235_FISS                 (idx, [1:   4]) = [  1.26060E+16 0.00063  9.09159E-01 0.00023 ];
U238_FISS                 (idx, [1:   4]) = [  9.06214E+14 0.00286  6.53506E-02 0.00273 ];
PU239_FISS                (idx, [1:   4]) = [  3.50614E+14 0.00463  2.52859E-02 0.00457 ];
PU240_FISS                (idx, [1:   4]) = [  1.33459E+10 0.70640  9.45853E-07 0.70640 ];
PU241_FISS                (idx, [1:   4]) = [  1.51154E+11 0.20390  1.09178E-05 0.20390 ];
U235_CAPT                 (idx, [1:   4]) = [  2.67871E+15 0.00150  1.43601E-01 0.00142 ];
U238_CAPT                 (idx, [1:   4]) = [  8.65695E+15 0.00094  4.64054E-01 0.00063 ];
PU239_CAPT                (idx, [1:   4]) = [  1.98657E+14 0.00586  1.06492E-02 0.00583 ];
PU240_CAPT                (idx, [1:   4]) = [  9.09672E+12 0.02694  4.87673E-04 0.02693 ];
PU241_CAPT                (idx, [1:   4]) = [  4.60288E+10 0.37572  2.46494E-06 0.37572 ];
XE135_CAPT                (idx, [1:   4]) = [  7.36485E+14 0.00313  3.94811E-02 0.00308 ];
SM149_CAPT                (idx, [1:   4]) = [  1.28052E+14 0.00718  6.86376E-03 0.00712 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000944 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.15908E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000944 5.00716E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2841970 2.84556E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2112525 2.11514E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46449 4.64591E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000944 5.00716E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.68107E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.42581E+16 1.3E-05  3.42581E+16 1.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38566E+16 1.5E-06  1.38566E+16 1.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.86651E+16 0.00047  1.26343E+16 0.00046  6.03075E+15 0.00100 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.25217E+16 0.00027  2.64910E+16 0.00022  6.03075E+15 0.00100 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.27783E+16 0.00049  3.27783E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.61640E+18 0.00046  4.29697E+17 0.00044  1.18670E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.04615E+14 0.00481 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.28263E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.25205E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12457E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12457E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.64896E+00 0.00044 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.54481E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.87420E-01 0.00030 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23447E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93978E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96710E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.05569E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04588E+00 0.00053 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.47232E+00 1.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02722E+02 1.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04585E+00 0.00054  1.03875E+00 0.00053  7.12664E-03 0.00795 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.04517E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04527E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.04517E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.05497E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75448E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75503E+01 8.3E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.81720E-07 0.00356 ];
IMP_EALF                  (idx, [1:   2]) = [  4.77793E-07 0.00146 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.19902E-01 0.00292 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.18821E-01 0.00123 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.69191E-03 0.00544  1.84229E-04 0.03195  1.06357E-03 0.01373  1.04181E-03 0.01334  3.07133E-03 0.00808  1.01244E-03 0.01460  3.18525E-04 0.02487 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.00479E-01 0.01281  1.06920E-02 0.01836  3.16211E-02 0.00024  1.10213E-01 0.00029  3.20865E-01 0.00023  1.34500E+00 0.00017  8.68883E+00 0.00780 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.93521E-03 0.00766  1.92135E-04 0.04757  1.11029E-03 0.01976  1.08711E-03 0.02060  3.16710E-03 0.01246  1.04681E-03 0.02068  3.31753E-04 0.03624 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.02644E-01 0.01874  1.24907E-02 4.0E-06  3.16270E-02 0.00033  1.10221E-01 0.00042  3.20990E-01 0.00034  1.34491E+00 0.00024  8.95647E+00 0.00224 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.01698E-05 0.00121  3.01542E-05 0.00122  3.24258E-05 0.01232 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.15478E-05 0.00104  3.15316E-05 0.00105  3.39056E-05 0.01229 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.79629E-03 0.00811  1.88901E-04 0.04836  1.07040E-03 0.02141  1.06183E-03 0.02115  3.11592E-03 0.01272  1.02566E-03 0.02221  3.33584E-04 0.03811 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.15399E-01 0.02030  1.24907E-02 4.6E-06  3.16268E-02 0.00040  1.10129E-01 0.00048  3.20974E-01 0.00037  1.34497E+00 0.00028  8.93695E+00 0.00262 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.02431E-05 0.00278  3.02230E-05 0.00279  3.40605E-05 0.02916 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.16242E-05 0.00270  3.16032E-05 0.00272  3.56360E-05 0.02924 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.62640E-03 0.02649  1.86019E-04 0.15395  1.08508E-03 0.06621  1.06341E-03 0.06890  2.93264E-03 0.04042  1.02418E-03 0.06724  3.35080E-04 0.12009 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.74809E-01 0.06861  1.24906E-02 1.7E-06  3.16074E-02 0.00097  1.10183E-01 0.00106  3.21135E-01 0.00108  1.34421E+00 0.00067  9.03775E+00 0.00677 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.59338E-03 0.02593  1.84465E-04 0.15727  1.07796E-03 0.06467  1.05056E-03 0.06563  2.92085E-03 0.03965  1.02844E-03 0.06514  3.31100E-04 0.11772 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.69029E-01 0.06672  1.24906E-02 1.6E-06  3.16108E-02 0.00096  1.10168E-01 0.00105  3.21018E-01 0.00105  1.34418E+00 0.00066  9.03514E+00 0.00673 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.19524E+02 0.02658 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.02282E-05 0.00075 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.16094E-05 0.00050 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.86530E-03 0.00464 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.27171E+02 0.00469 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.69849E-07 0.00059 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87083E-06 0.00043  2.87055E-06 0.00043  2.90854E-06 0.00489 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.50273E-05 0.00076  4.50491E-05 0.00076  4.20028E-05 0.00834 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.84711E-01 0.00030  6.84412E-01 0.00030  7.40142E-01 0.00849 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.01871E+01 0.01206 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.81480E+01 0.00045  3.66664E+01 0.00046 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.22336E+04 0.00322  2.92006E+05 0.00125  6.02932E+05 0.00077  6.49040E+05 0.00081  5.97327E+05 0.00073  6.40180E+05 0.00088  4.34857E+05 0.00076  3.85311E+05 0.00061  2.94956E+05 0.00089  2.40617E+05 0.00086  2.07133E+05 0.00088  1.86710E+05 0.00085  1.72730E+05 0.00103  1.64175E+05 0.00094  1.59850E+05 0.00113  1.38158E+05 0.00126  1.36512E+05 0.00104  1.35002E+05 0.00090  1.32730E+05 0.00110  2.59466E+05 0.00054  2.50233E+05 0.00091  1.80770E+05 0.00093  1.17452E+05 0.00130  1.35773E+05 0.00107  1.28626E+05 0.00115  1.16864E+05 0.00111  1.92758E+05 0.00074  4.39321E+04 0.00153  5.52834E+04 0.00111  5.00311E+04 0.00134  2.91616E+04 0.00181  5.07282E+04 0.00161  3.44202E+04 0.00184  2.94409E+04 0.00119  5.60427E+03 0.00267  5.57150E+03 0.00394  5.67451E+03 0.00304  5.86418E+03 0.00341  5.87035E+03 0.00369  5.70551E+03 0.00382  5.96660E+03 0.00349  5.59707E+03 0.00418  1.05465E+04 0.00276  1.68294E+04 0.00207  2.14323E+04 0.00238  5.66058E+04 0.00159  5.98481E+04 0.00145  6.61473E+04 0.00123  4.60852E+04 0.00132  3.49156E+04 0.00144  2.70870E+04 0.00176  3.23982E+04 0.00197  6.24126E+04 0.00132  8.57069E+04 0.00118  1.66523E+05 0.00094  2.58624E+05 0.00095  3.88035E+05 0.00106  2.48604E+05 0.00099  1.78411E+05 0.00123  1.28229E+05 0.00109  1.15189E+05 0.00116  1.13570E+05 0.00110  9.48469E+04 0.00127  6.40849E+04 0.00128  5.92631E+04 0.00141  5.26956E+04 0.00119  4.45589E+04 0.00118  3.50613E+04 0.00130  2.35167E+04 0.00178  8.33592E+03 0.00165 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.05507E+00 0.00050 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.22405E+18 0.00053  3.92381E+17 0.00102 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38658E-01 0.00012  1.51061E+00 0.00036 ];
INF_CAPT                  (idx, [1:   4]) = [  6.27118E-03 0.00058  2.80078E-02 0.00032 ];
INF_ABS                   (idx, [1:   4]) = [  8.32967E-03 0.00047  5.69052E-02 0.00067 ];
INF_FISS                  (idx, [1:   4]) = [  2.05849E-03 0.00050  2.88974E-02 0.00102 ];
INF_NSF                   (idx, [1:   4]) = [  5.30137E-03 0.00049  7.07818E-02 0.00102 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.57537E+00 5.8E-05  2.44942E+00 5.3E-06 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04012E+02 6.7E-06  2.02436E+02 8.3E-07 ];
INF_INVV                  (idx, [1:   4]) = [  5.94862E-08 0.00042  2.57401E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.30327E-01 0.00012  1.45371E+00 0.00040 ];
INF_SCATT1                (idx, [1:   4]) = [  2.45188E-01 0.00022  3.83069E-01 0.00045 ];
INF_SCATT2                (idx, [1:   4]) = [  9.66410E-02 0.00030  9.05462E-02 0.00074 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35278E-03 0.00279  2.71133E-02 0.00240 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03932E-02 0.00166 -8.61140E-03 0.00603 ];
INF_SCATT5                (idx, [1:   4]) = [  9.63850E-05 0.16091  6.42642E-03 0.00925 ];
INF_SCATT6                (idx, [1:   4]) = [  5.04765E-03 0.00308 -1.64835E-02 0.00300 ];
INF_SCATT7                (idx, [1:   4]) = [  7.43071E-04 0.01951  3.82182E-04 0.11513 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.30365E-01 0.00012  1.45371E+00 0.00040 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.45189E-01 0.00022  3.83069E-01 0.00045 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.66411E-02 0.00030  9.05462E-02 0.00074 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35292E-03 0.00279  2.71133E-02 0.00240 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03932E-02 0.00166 -8.61140E-03 0.00603 ];
INF_SCATTP5               (idx, [1:   4]) = [  9.62425E-05 0.16083  6.42642E-03 0.00925 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.04761E-03 0.00308 -1.64835E-02 0.00300 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.42882E-04 0.01954  3.82182E-04 0.11513 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13220E-01 0.00027  9.75374E-01 0.00035 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56333E+00 0.00027  3.41750E-01 0.00035 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.29133E-03 0.00047  5.69052E-02 0.00067 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69831E-02 0.00028  5.79137E-02 0.00081 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11675E-01 0.00012  1.86521E-02 0.00033  1.02140E-03 0.00362  1.45269E+00 0.00040 ];
INF_S1                    (idx, [1:   8]) = [  2.39724E-01 0.00022  5.46423E-03 0.00083  4.38176E-04 0.00504  3.82631E-01 0.00045 ];
INF_S2                    (idx, [1:   8]) = [  9.82454E-02 0.00030 -1.60432E-03 0.00251  2.36742E-04 0.01086  9.03094E-02 0.00074 ];
INF_S3                    (idx, [1:   8]) = [  9.27117E-03 0.00214 -1.91839E-03 0.00166  8.28251E-05 0.02062  2.70305E-02 0.00242 ];
INF_S4                    (idx, [1:   8]) = [ -9.75117E-03 0.00181 -6.42021E-04 0.00508 -3.12619E-07 1.00000 -8.61109E-03 0.00604 ];
INF_S5                    (idx, [1:   8]) = [  8.04050E-05 0.20480  1.59801E-05 0.15025 -3.40784E-05 0.04079  6.46050E-03 0.00922 ];
INF_S6                    (idx, [1:   8]) = [  5.19709E-03 0.00280 -1.49435E-04 0.01507 -4.12148E-05 0.03074 -1.64423E-02 0.00302 ];
INF_S7                    (idx, [1:   8]) = [  9.20428E-04 0.01468 -1.77356E-04 0.01273 -3.66086E-05 0.03157  4.18791E-04 0.10480 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11713E-01 0.00012  1.86521E-02 0.00033  1.02140E-03 0.00362  1.45269E+00 0.00040 ];
INF_SP1                   (idx, [1:   8]) = [  2.39725E-01 0.00022  5.46423E-03 0.00083  4.38176E-04 0.00504  3.82631E-01 0.00045 ];
INF_SP2                   (idx, [1:   8]) = [  9.82455E-02 0.00030 -1.60432E-03 0.00251  2.36742E-04 0.01086  9.03094E-02 0.00074 ];
INF_SP3                   (idx, [1:   8]) = [  9.27131E-03 0.00215 -1.91839E-03 0.00166  8.28251E-05 0.02062  2.70305E-02 0.00242 ];
INF_SP4                   (idx, [1:   8]) = [ -9.75116E-03 0.00181 -6.42021E-04 0.00508 -3.12619E-07 1.00000 -8.61109E-03 0.00604 ];
INF_SP5                   (idx, [1:   8]) = [  8.02624E-05 0.20474  1.59801E-05 0.15025 -3.40784E-05 0.04079  6.46050E-03 0.00922 ];
INF_SP6                   (idx, [1:   8]) = [  5.19705E-03 0.00280 -1.49435E-04 0.01507 -4.12148E-05 0.03074 -1.64423E-02 0.00302 ];
INF_SP7                   (idx, [1:   8]) = [  9.20238E-04 0.01471 -1.77356E-04 0.01273 -3.66086E-05 0.03157  4.18791E-04 0.10480 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32752E-01 0.00042  1.08841E+00 0.00457 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34464E-01 0.00075  1.15805E+00 0.00615 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34137E-01 0.00080  1.16192E+00 0.00750 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29725E-01 0.00099  9.69929E-01 0.00483 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43215E+00 0.00042  3.06410E-01 0.00456 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42170E+00 0.00075  2.88102E-01 0.00614 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42369E+00 0.00080  2.87264E-01 0.00741 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45105E+00 0.00099  3.43863E-01 0.00490 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.93521E-03 0.00766  1.92135E-04 0.04757  1.11029E-03 0.01976  1.08711E-03 0.02060  3.16710E-03 0.01246  1.04681E-03 0.02068  3.31753E-04 0.03624 ];
LAMBDA                    (idx, [1:  14]) = [  8.02644E-01 0.01874  1.24907E-02 4.0E-06  3.16270E-02 0.00033  1.10221E-01 0.00042  3.20990E-01 0.00034  1.34491E+00 0.00024  8.95647E+00 0.00224 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0000' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:31 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:03:51 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284831 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00825E+00  1.00369E+00  9.98356E-01  9.99386E-01  9.90317E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.6E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.11868E-02 0.00104  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88813E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.98908E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.99321E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.71522E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.79289E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.79197E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.51960E+00 0.00042  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.47287E-01 0.00107  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000656 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00013E+04 0.00072 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00013E+04 0.00072 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.14827E+02 ;
RUNNING_TIME              (idx, 1)        =  2.33303E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.22850E-01  4.22850E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  5.90000E-02  7.75000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  2.26516E+01  3.57140E+00  2.76992E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.94933E-01  2.82833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  9.63333E-03  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  2.33302E+01  1.32610E+02 ];
CPU_USAGE                 (idx, 1)        = 4.92180 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99992E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.68530E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.06747E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.86162E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.52144E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.74170E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.21638E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.32575E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.73996E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  3.61758E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.53738E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.08912E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.19956E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  2.52846E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  2.81743E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.25908E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.05854E+14 ;
I131_ACTIVITY             (idx, 1)        =  3.60719E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.11420E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.77899E+10 ;
CS137_ACTIVITY            (idx, 1)        =  1.34860E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.00168E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.61824E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.60466E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.18850E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.29942E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 3 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E+00  1.00009E+00 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.49473E-01 0.00109 ];
U235_FISS                 (idx, [1:   4]) = [  1.21384E+16 0.00059  8.77242E-01 0.00026 ];
U238_FISS                 (idx, [1:   4]) = [  9.08700E+14 0.00268  6.56652E-02 0.00256 ];
PU239_FISS                (idx, [1:   4]) = [  7.85539E+14 0.00292  5.67675E-02 0.00283 ];
PU240_FISS                (idx, [1:   4]) = [  6.65594E+10 0.31340  4.79803E-06 0.31339 ];
PU241_FISS                (idx, [1:   4]) = [  1.63896E+12 0.06319  1.18430E-04 0.06313 ];
U235_CAPT                 (idx, [1:   4]) = [  2.58511E+15 0.00163  1.36773E-01 0.00152 ];
U238_CAPT                 (idx, [1:   4]) = [  8.66009E+15 0.00102  4.58160E-01 0.00066 ];
PU239_CAPT                (idx, [1:   4]) = [  4.33136E+14 0.00389  2.29167E-02 0.00385 ];
PU240_CAPT                (idx, [1:   4]) = [  4.09549E+13 0.01320  2.16689E-03 0.01317 ];
PU241_CAPT                (idx, [1:   4]) = [  7.12510E+11 0.09773  3.77039E-05 0.09768 ];
XE135_CAPT                (idx, [1:   4]) = [  7.40265E+14 0.00287  3.91693E-02 0.00287 ];
SM149_CAPT                (idx, [1:   4]) = [  1.50773E+14 0.00679  7.97763E-03 0.00679 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000656 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.36844E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000656 5.00737E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2860271 2.86428E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2094324 2.09701E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46061 4.60725E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000656 5.00737E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.25963E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.44180E+16 1.2E-05  3.44180E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38445E+16 1.6E-06  1.38445E+16 1.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.89113E+16 0.00049  1.29130E+16 0.00048  5.99827E+15 0.00105 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.27557E+16 0.00028  2.67575E+16 0.00023  5.99827E+15 0.00105 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.29942E+16 0.00051  3.29942E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.62157E+18 0.00046  4.31069E+17 0.00045  1.19050E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.04070E+14 0.00479 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.30598E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.25282E+18 0.00061 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12398E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12398E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.64235E+00 0.00043 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.57104E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.86347E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23321E+00 0.00033 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94062E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96704E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.05238E+00 0.00050 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04268E+00 0.00050 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.48605E+00 1.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02900E+02 1.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04289E+00 0.00052  1.03568E+00 0.00051  7.00922E-03 0.00842 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.04266E+00 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04329E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.04266E+00 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  1.05235E+00 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75228E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75219E+01 8.4E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.92189E-07 0.00328 ];
IMP_EALF                  (idx, [1:   2]) = [  4.91591E-07 0.00147 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.21192E-01 0.00286 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.21304E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.58701E-03 0.00552  2.01699E-04 0.03193  1.05803E-03 0.01325  1.01571E-03 0.01491  3.01596E-03 0.00823  9.82691E-04 0.01369  3.12912E-04 0.02609 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.93879E-01 0.01325  1.07918E-02 0.01776  3.15677E-02 0.00027  1.10237E-01 0.00031  3.20999E-01 0.00022  1.34452E+00 0.00021  8.62114E+00 0.00859 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.79641E-03 0.00818  2.03215E-04 0.04822  1.08678E-03 0.02050  1.02601E-03 0.02274  3.13798E-03 0.01206  1.00463E-03 0.02010  3.37791E-04 0.03783 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.13483E-01 0.01954  1.24906E-02 5.0E-06  3.15630E-02 0.00039  1.10296E-01 0.00048  3.21051E-01 0.00034  1.34487E+00 0.00026  8.91977E+00 0.00214 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.97614E-05 0.00118  2.97448E-05 0.00119  3.21945E-05 0.01143 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.10338E-05 0.00107  3.10165E-05 0.00107  3.35744E-05 0.01145 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.71269E-03 0.00851  1.97972E-04 0.05095  1.07907E-03 0.02201  1.04051E-03 0.02268  3.10103E-03 0.01248  9.82439E-04 0.02145  3.11675E-04 0.04015 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.78857E-01 0.02070  1.24906E-02 6.5E-06  3.15706E-02 0.00046  1.10281E-01 0.00057  3.21200E-01 0.00036  1.34452E+00 0.00029  8.94569E+00 0.00278 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.99058E-05 0.00271  2.98846E-05 0.00271  3.24133E-05 0.02950 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.11843E-05 0.00266  3.11621E-05 0.00266  3.38029E-05 0.02953 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.68142E-03 0.02768  2.09657E-04 0.16178  9.74103E-04 0.06760  1.09145E-03 0.06888  2.99107E-03 0.04051  1.12859E-03 0.07261  2.86550E-04 0.13201 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.21780E-01 0.06853  1.24907E-02 9.3E-06  3.15262E-02 0.00114  1.10398E-01 0.00135  3.20851E-01 0.00106  1.34545E+00 0.00064  8.99545E+00 0.00656 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.67332E-03 0.02641  2.06694E-04 0.15291  1.00897E-03 0.06458  1.06647E-03 0.06665  2.99781E-03 0.03869  1.11276E-03 0.07110  2.80613E-04 0.12404 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.09899E-01 0.06698  1.24907E-02 1.1E-05  3.15258E-02 0.00113  1.10388E-01 0.00133  3.20804E-01 0.00103  1.34543E+00 0.00064  8.99789E+00 0.00659 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.24385E+02 0.02778 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.98098E-05 0.00076 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.10842E-05 0.00056 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.75129E-03 0.00516 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.26522E+02 0.00517 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.62677E-07 0.00063 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.86684E-06 0.00042  2.86666E-06 0.00042  2.89127E-06 0.00509 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.44098E-05 0.00079  4.44300E-05 0.00079  4.15414E-05 0.00834 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.83697E-01 0.00031  6.83373E-01 0.00032  7.44275E-01 0.00849 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03452E+01 0.01298 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.79197E+01 0.00047  3.64148E+01 0.00046 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.29969E+04 0.00286  2.92250E+05 0.00155  6.04186E+05 0.00097  6.49707E+05 0.00066  5.96841E+05 0.00065  6.40734E+05 0.00054  4.35426E+05 0.00070  3.84665E+05 0.00072  2.94663E+05 0.00090  2.40893E+05 0.00093  2.07146E+05 0.00089  1.86983E+05 0.00105  1.72949E+05 0.00085  1.64134E+05 0.00102  1.59800E+05 0.00086  1.37905E+05 0.00068  1.36427E+05 0.00107  1.35266E+05 0.00083  1.32854E+05 0.00096  2.59386E+05 0.00075  2.50584E+05 0.00067  1.80868E+05 0.00102  1.17723E+05 0.00093  1.35742E+05 0.00084  1.28775E+05 0.00108  1.17075E+05 0.00094  1.92524E+05 0.00057  4.39786E+04 0.00114  5.51966E+04 0.00154  4.99829E+04 0.00145  2.92113E+04 0.00182  5.06627E+04 0.00133  3.44451E+04 0.00140  2.94075E+04 0.00201  5.61967E+03 0.00301  5.58724E+03 0.00301  5.63700E+03 0.00344  5.79412E+03 0.00215  5.79334E+03 0.00373  5.69210E+03 0.00412  5.92112E+03 0.00346  5.52190E+03 0.00437  1.05303E+04 0.00356  1.68094E+04 0.00260  2.13981E+04 0.00213  5.64539E+04 0.00150  5.96680E+04 0.00143  6.57708E+04 0.00136  4.55535E+04 0.00128  3.43129E+04 0.00167  2.64204E+04 0.00169  3.15399E+04 0.00134  6.09495E+04 0.00132  8.39221E+04 0.00108  1.63475E+05 0.00097  2.54415E+05 0.00099  3.82079E+05 0.00104  2.44885E+05 0.00109  1.75597E+05 0.00119  1.26319E+05 0.00108  1.13538E+05 0.00118  1.11849E+05 0.00110  9.34896E+04 0.00101  6.32243E+04 0.00130  5.84379E+04 0.00147  5.20421E+04 0.00143  4.40387E+04 0.00123  3.46407E+04 0.00125  2.31738E+04 0.00146  8.20162E+03 0.00186 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.05299E+00 0.00053 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.23269E+18 0.00047  3.88926E+17 0.00094 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38629E-01 0.00011  1.51415E+00 0.00038 ];
INF_CAPT                  (idx, [1:   4]) = [  6.30976E-03 0.00057  2.86281E-02 0.00039 ];
INF_ABS                   (idx, [1:   4]) = [  8.35712E-03 0.00044  5.77407E-02 0.00072 ];
INF_FISS                  (idx, [1:   4]) = [  2.04736E-03 0.00050  2.91127E-02 0.00104 ];
INF_NSF                   (idx, [1:   4]) = [  5.28357E-03 0.00048  7.17613E-02 0.00104 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.58067E+00 6.6E-05  2.46495E+00 1.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04076E+02 6.8E-06  2.02638E+02 1.7E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.94011E-08 0.00037  2.57487E-06 0.00012 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.30271E-01 0.00012  1.45646E+00 0.00041 ];
INF_SCATT1                (idx, [1:   4]) = [  2.45149E-01 0.00019  3.83842E-01 0.00060 ];
INF_SCATT2                (idx, [1:   4]) = [  9.66656E-02 0.00025  9.07360E-02 0.00089 ];
INF_SCATT3                (idx, [1:   4]) = [  7.41012E-03 0.00256  2.73242E-02 0.00228 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03955E-02 0.00188 -8.56028E-03 0.00600 ];
INF_SCATT5                (idx, [1:   4]) = [  8.38800E-05 0.19320  6.35201E-03 0.00760 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07999E-03 0.00307 -1.65064E-02 0.00310 ];
INF_SCATT7                (idx, [1:   4]) = [  7.65978E-04 0.01701  4.31286E-04 0.10541 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.30311E-01 0.00012  1.45646E+00 0.00041 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.45149E-01 0.00019  3.83842E-01 0.00060 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.66662E-02 0.00025  9.07360E-02 0.00089 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.41002E-03 0.00256  2.73242E-02 0.00228 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03956E-02 0.00188 -8.56028E-03 0.00600 ];
INF_SCATTP5               (idx, [1:   4]) = [  8.39437E-05 0.19284  6.35201E-03 0.00760 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08016E-03 0.00308 -1.65064E-02 0.00310 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.66075E-04 0.01699  4.31286E-04 0.10541 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13167E-01 0.00030  9.78215E-01 0.00030 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56372E+00 0.00030  3.40757E-01 0.00030 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.31768E-03 0.00044  5.77407E-02 0.00072 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69722E-02 0.00020  5.87220E-02 0.00075 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11656E-01 0.00011  1.86149E-02 0.00032  1.03203E-03 0.00338  1.45542E+00 0.00041 ];
INF_S1                    (idx, [1:   8]) = [  2.39700E-01 0.00019  5.44907E-03 0.00091  4.46452E-04 0.00649  3.83395E-01 0.00060 ];
INF_S2                    (idx, [1:   8]) = [  9.82621E-02 0.00025 -1.59644E-03 0.00195  2.41371E-04 0.01027  9.04946E-02 0.00089 ];
INF_S3                    (idx, [1:   8]) = [  9.32220E-03 0.00197 -1.91208E-03 0.00188  8.84883E-05 0.01788  2.72357E-02 0.00229 ];
INF_S4                    (idx, [1:   8]) = [ -9.75373E-03 0.00194 -6.41729E-04 0.00455  1.31957E-06 1.00000 -8.56160E-03 0.00599 ];
INF_S5                    (idx, [1:   8]) = [  7.18341E-05 0.21799  1.20459E-05 0.26864 -3.30421E-05 0.04141  6.38505E-03 0.00761 ];
INF_S6                    (idx, [1:   8]) = [  5.23143E-03 0.00278 -1.51433E-04 0.01900 -4.14978E-05 0.02864 -1.64649E-02 0.00313 ];
INF_S7                    (idx, [1:   8]) = [  9.39247E-04 0.01343 -1.73270E-04 0.01470 -3.98969E-05 0.03129  4.71183E-04 0.09693 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11696E-01 0.00011  1.86149E-02 0.00032  1.03203E-03 0.00338  1.45542E+00 0.00041 ];
INF_SP1                   (idx, [1:   8]) = [  2.39700E-01 0.00019  5.44907E-03 0.00091  4.46452E-04 0.00649  3.83395E-01 0.00060 ];
INF_SP2                   (idx, [1:   8]) = [  9.82626E-02 0.00025 -1.59644E-03 0.00195  2.41371E-04 0.01027  9.04946E-02 0.00089 ];
INF_SP3                   (idx, [1:   8]) = [  9.32210E-03 0.00197 -1.91208E-03 0.00188  8.84883E-05 0.01788  2.72357E-02 0.00229 ];
INF_SP4                   (idx, [1:   8]) = [ -9.75390E-03 0.00193 -6.41729E-04 0.00455  1.31957E-06 1.00000 -8.56160E-03 0.00599 ];
INF_SP5                   (idx, [1:   8]) = [  7.18978E-05 0.21761  1.20459E-05 0.26864 -3.30421E-05 0.04141  6.38505E-03 0.00761 ];
INF_SP6                   (idx, [1:   8]) = [  5.23159E-03 0.00278 -1.51433E-04 0.01900 -4.14978E-05 0.02864 -1.64649E-02 0.00313 ];
INF_SP7                   (idx, [1:   8]) = [  9.39344E-04 0.01340 -1.73270E-04 0.01470 -3.98969E-05 0.03129  4.71183E-04 0.09693 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32858E-01 0.00070  1.09283E+00 0.00542 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34134E-01 0.00108  1.16520E+00 0.00702 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34757E-01 0.00088  1.16809E+00 0.00769 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29755E-01 0.00088  9.71037E-01 0.00551 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43150E+00 0.00070  3.05239E-01 0.00552 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42373E+00 0.00109  2.86411E-01 0.00700 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.41993E+00 0.00088  2.85778E-01 0.00782 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45085E+00 0.00088  3.43526E-01 0.00551 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.79641E-03 0.00818  2.03215E-04 0.04822  1.08678E-03 0.02050  1.02601E-03 0.02274  3.13798E-03 0.01206  1.00463E-03 0.02010  3.37791E-04 0.03783 ];
LAMBDA                    (idx, [1:  14]) = [  8.13483E-01 0.01954  1.24906E-02 5.0E-06  3.15630E-02 0.00039  1.10296E-01 0.00048  3.21051E-01 0.00034  1.34487E+00 0.00026  8.91977E+00 0.00214 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0000' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:31 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:10:13 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284831 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00842E+00  1.00511E+00  9.98983E-01  9.98187E-01  9.89293E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.1E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12184E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88782E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.99886E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.00302E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.70805E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.75182E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.75091E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.35904E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.42521E-01 0.00112  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000510 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00010E+04 0.00074 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00010E+04 0.00074 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.46593E+02 ;
RUNNING_TIME              (idx, 1)        =  2.96899E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.22850E-01  4.22850E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  7.70000E-02  8.31666E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  2.89357E+01  3.52377E+00  2.76040E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  2.51717E-01  2.83500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.29500E-02  8.16667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  2.96897E+01  1.32172E+02 ];
CPU_USAGE                 (idx, 1)        = 4.93748 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99885E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.72295E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.22865E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.86091E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.78795E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.74855E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.22096E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.48008E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.73879E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  4.83714E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.90075E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.38195E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.24413E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  3.45519E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.17633E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.46597E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.16228E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.10087E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.23775E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.63349E+11 ;
CS137_ACTIVITY            (idx, 1)        =  2.70025E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.12028E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.57673E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.51765E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.21489E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.35125E+12 0.00048  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 4 ;
BURNUP                     (idx, [1:  2])  = [  2.00000E+00  2.00018E+00 ];
BURN_DAYS                 (idx, 1)        =  5.00000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.51785E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  1.13644E+16 0.00064  8.22658E-01 0.00032 ];
U238_FISS                 (idx, [1:   4]) = [  9.26314E+14 0.00285  6.70481E-02 0.00273 ];
PU239_FISS                (idx, [1:   4]) = [  1.50782E+15 0.00198  1.09147E-01 0.00189 ];
PU240_FISS                (idx, [1:   4]) = [  4.09146E+11 0.12663  2.95932E-05 0.12664 ];
PU241_FISS                (idx, [1:   4]) = [  1.21666E+13 0.02365  8.80670E-04 0.02367 ];
U235_CAPT                 (idx, [1:   4]) = [  2.43943E+15 0.00167  1.25479E-01 0.00162 ];
U238_CAPT                 (idx, [1:   4]) = [  8.71564E+15 0.00100  4.48269E-01 0.00067 ];
PU239_CAPT                (idx, [1:   4]) = [  8.41711E+14 0.00286  4.32929E-02 0.00278 ];
PU240_CAPT                (idx, [1:   4]) = [  1.47884E+14 0.00645  7.60617E-03 0.00641 ];
PU241_CAPT                (idx, [1:   4]) = [  4.10899E+12 0.04053  2.11384E-04 0.04054 ];
XE135_CAPT                (idx, [1:   4]) = [  7.42654E+14 0.00293  3.82000E-02 0.00290 ];
SM149_CAPT                (idx, [1:   4]) = [  1.57186E+14 0.00689  8.08407E-03 0.00684 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000510 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.33557E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000510 5.00734E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2896687 2.90071E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2058368 2.06116E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45455 4.54614E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000510 5.00734E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.30854E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.46928E+16 1.4E-05  3.46928E+16 1.4E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38234E+16 2.1E-06  1.38234E+16 2.1E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.94367E+16 0.00047  1.34741E+16 0.00045  5.96254E+15 0.00104 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.32601E+16 0.00027  2.72976E+16 0.00022  5.96254E+15 0.00104 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.35125E+16 0.00048  3.35125E+16 0.00048  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.63708E+18 0.00046  4.35466E+17 0.00044  1.20161E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.04756E+14 0.00501 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.35649E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.25871E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12282E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12282E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.62903E+00 0.00045 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.61552E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.82985E-01 0.00030 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23229E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94120E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96769E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.04406E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.03456E+00 0.00052 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.50971E+00 1.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03209E+02 2.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03436E+00 0.00054  1.02779E+00 0.00052  6.77201E-03 0.00880 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.03519E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03534E+00 0.00048 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.03519E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.04468E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74752E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74739E+01 8.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.16560E-07 0.00372 ];
IMP_EALF                  (idx, [1:   2]) = [  5.15764E-07 0.00152 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.25902E-01 0.00287 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.25756E-01 0.00119 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.40693E-03 0.00546  1.73094E-04 0.03348  1.02384E-03 0.01408  1.01404E-03 0.01363  2.93846E-03 0.00827  9.44474E-04 0.01366  3.13027E-04 0.02366 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.07714E-01 0.01290  1.04425E-02 0.01983  3.14879E-02 0.00033  1.10154E-01 0.00030  3.21288E-01 0.00025  1.34377E+00 0.00027  8.63276E+00 0.00887 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.58195E-03 0.00830  1.86464E-04 0.05153  1.04507E-03 0.02097  1.05976E-03 0.02049  3.01116E-03 0.01222  9.63423E-04 0.02132  3.16076E-04 0.03690 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.98459E-01 0.01937  1.24924E-02 0.00017  3.14855E-02 0.00046  1.10199E-01 0.00046  3.21372E-01 0.00036  1.34352E+00 0.00051  8.95204E+00 0.00239 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.91943E-05 0.00119  2.91839E-05 0.00120  3.07353E-05 0.01333 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.01932E-05 0.00108  3.01824E-05 0.00109  3.17885E-05 0.01332 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.55282E-03 0.00882  1.79760E-04 0.05296  1.02096E-03 0.02152  1.05643E-03 0.02260  3.03326E-03 0.01273  9.66213E-04 0.02306  2.96194E-04 0.04202 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.76003E-01 0.02207  1.24902E-02 1.2E-05  3.14803E-02 0.00054  1.10155E-01 0.00054  3.21394E-01 0.00038  1.34356E+00 0.00049  8.94127E+00 0.00363 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.91416E-05 0.00294  2.91308E-05 0.00296  2.99513E-05 0.03208 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.01389E-05 0.00290  3.01279E-05 0.00292  3.09714E-05 0.03201 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.64905E-03 0.02971  1.88611E-04 0.18043  1.06204E-03 0.07429  1.07384E-03 0.07121  3.13782E-03 0.04386  8.83907E-04 0.07374  3.02830E-04 0.13922 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.81833E-01 0.06851  1.24902E-02 2.7E-05  3.14564E-02 0.00133  1.10269E-01 0.00138  3.21889E-01 0.00114  1.34526E+00 0.00069  8.90518E+00 0.00605 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.56523E-03 0.02920  1.82400E-04 0.17594  1.04101E-03 0.07282  1.07174E-03 0.06931  3.09212E-03 0.04293  8.78160E-04 0.07136  2.99792E-04 0.13835 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.81177E-01 0.06695  1.24902E-02 2.7E-05  3.14505E-02 0.00133  1.10281E-01 0.00137  3.21977E-01 0.00114  1.34510E+00 0.00069  8.89888E+00 0.00596 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.29313E+02 0.03006 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.91534E-05 0.00077 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.01507E-05 0.00056 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.64487E-03 0.00530 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.28068E+02 0.00548 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.50237E-07 0.00063 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.85717E-06 0.00041  2.85691E-06 0.00041  2.89515E-06 0.00494 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.34687E-05 0.00081  4.34864E-05 0.00081  4.08638E-05 0.00878 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.80388E-01 0.00030  6.80129E-01 0.00030  7.31745E-01 0.00881 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02996E+01 0.01293 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.75091E+01 0.00047  3.59899E+01 0.00050 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.38470E+04 0.00339  2.93575E+05 0.00129  6.04474E+05 0.00076  6.49669E+05 0.00089  5.98023E+05 0.00062  6.41002E+05 0.00054  4.35473E+05 0.00076  3.85473E+05 0.00069  2.94801E+05 0.00083  2.40749E+05 0.00075  2.07304E+05 0.00095  1.87162E+05 0.00069  1.72749E+05 0.00094  1.64281E+05 0.00100  1.60071E+05 0.00094  1.38081E+05 0.00077  1.36262E+05 0.00097  1.35196E+05 0.00098  1.32862E+05 0.00086  2.59060E+05 0.00065  2.50628E+05 0.00057  1.81078E+05 0.00086  1.17650E+05 0.00083  1.35825E+05 0.00112  1.28826E+05 0.00071  1.16824E+05 0.00104  1.92085E+05 0.00071  4.40266E+04 0.00152  5.51750E+04 0.00158  5.01031E+04 0.00163  2.90350E+04 0.00195  5.05522E+04 0.00196  3.43902E+04 0.00184  2.92909E+04 0.00165  5.54983E+03 0.00359  5.48607E+03 0.00388  5.47427E+03 0.00375  5.56807E+03 0.00431  5.56307E+03 0.00417  5.58146E+03 0.00322  5.81104E+03 0.00298  5.52834E+03 0.00286  1.04275E+04 0.00250  1.66867E+04 0.00276  2.13760E+04 0.00198  5.63429E+04 0.00172  5.92582E+04 0.00132  6.49275E+04 0.00146  4.44182E+04 0.00118  3.30902E+04 0.00191  2.54662E+04 0.00173  3.04157E+04 0.00153  5.87526E+04 0.00117  8.10865E+04 0.00071  1.58779E+05 0.00079  2.47230E+05 0.00103  3.71875E+05 0.00109  2.38423E+05 0.00127  1.71011E+05 0.00106  1.22978E+05 0.00100  1.10649E+05 0.00122  1.09169E+05 0.00119  9.12094E+04 0.00102  6.16383E+04 0.00109  5.69410E+04 0.00127  5.06794E+04 0.00140  4.28543E+04 0.00129  3.37172E+04 0.00166  2.25817E+04 0.00164  8.00560E+03 0.00169 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.04484E+00 0.00050 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.25253E+18 0.00047  3.84582E+17 0.00093 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38541E-01 0.00011  1.51904E+00 0.00034 ];
INF_CAPT                  (idx, [1:   4]) = [  6.42902E-03 0.00057  2.96037E-02 0.00038 ];
INF_ABS                   (idx, [1:   4]) = [  8.44511E-03 0.00044  5.89862E-02 0.00065 ];
INF_FISS                  (idx, [1:   4]) = [  2.01609E-03 0.00054  2.93825E-02 0.00094 ];
INF_NSF                   (idx, [1:   4]) = [  5.22298E-03 0.00053  7.32100E-02 0.00095 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.59065E+00 5.8E-05  2.49162E+00 2.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04202E+02 6.3E-06  2.02987E+02 3.4E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.91976E-08 0.00037  2.57529E-06 0.00012 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.30097E-01 0.00011  1.46007E+00 0.00038 ];
INF_SCATT1                (idx, [1:   4]) = [  2.45015E-01 0.00022  3.85114E-01 0.00046 ];
INF_SCATT2                (idx, [1:   4]) = [  9.66296E-02 0.00034  9.10654E-02 0.00110 ];
INF_SCATT3                (idx, [1:   4]) = [  7.38299E-03 0.00354  2.73489E-02 0.00243 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03813E-02 0.00247 -8.54647E-03 0.00517 ];
INF_SCATT5                (idx, [1:   4]) = [  8.44834E-05 0.25724  6.41868E-03 0.00810 ];
INF_SCATT6                (idx, [1:   4]) = [  5.05936E-03 0.00322 -1.66383E-02 0.00271 ];
INF_SCATT7                (idx, [1:   4]) = [  7.24702E-04 0.02238  4.33848E-04 0.09224 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.30136E-01 0.00011  1.46007E+00 0.00038 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.45015E-01 0.00022  3.85114E-01 0.00046 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.66298E-02 0.00034  9.10654E-02 0.00110 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.38317E-03 0.00354  2.73489E-02 0.00243 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03814E-02 0.00247 -8.54647E-03 0.00517 ];
INF_SCATTP5               (idx, [1:   4]) = [  8.44092E-05 0.25691  6.41868E-03 0.00810 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.05927E-03 0.00323 -1.66383E-02 0.00271 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.24764E-04 0.02237  4.33848E-04 0.09224 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13093E-01 0.00032  9.82449E-01 0.00031 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56427E+00 0.00032  3.39289E-01 0.00031 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.40585E-03 0.00045  5.89862E-02 0.00065 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69615E-02 0.00019  6.00292E-02 0.00079 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11580E-01 0.00011  1.85173E-02 0.00040  1.05410E-03 0.00462  1.45901E+00 0.00038 ];
INF_S1                    (idx, [1:   8]) = [  2.39595E-01 0.00022  5.42049E-03 0.00086  4.51021E-04 0.00684  3.84663E-01 0.00046 ];
INF_S2                    (idx, [1:   8]) = [  9.82228E-02 0.00034 -1.59315E-03 0.00199  2.44668E-04 0.00940  9.08207E-02 0.00110 ];
INF_S3                    (idx, [1:   8]) = [  9.28564E-03 0.00276 -1.90265E-03 0.00138  8.49667E-05 0.01968  2.72639E-02 0.00242 ];
INF_S4                    (idx, [1:   8]) = [ -9.73996E-03 0.00254 -6.41353E-04 0.00508 -2.10202E-06 0.75556 -8.54436E-03 0.00518 ];
INF_S5                    (idx, [1:   8]) = [  7.27952E-05 0.28952  1.16883E-05 0.24253 -3.64524E-05 0.03338  6.45513E-03 0.00803 ];
INF_S6                    (idx, [1:   8]) = [  5.20831E-03 0.00303 -1.48952E-04 0.01758 -4.49106E-05 0.02397 -1.65933E-02 0.00271 ];
INF_S7                    (idx, [1:   8]) = [  8.98549E-04 0.01822 -1.73847E-04 0.01145 -4.22402E-05 0.03009  4.76088E-04 0.08429 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11619E-01 0.00011  1.85173E-02 0.00040  1.05410E-03 0.00462  1.45901E+00 0.00038 ];
INF_SP1                   (idx, [1:   8]) = [  2.39595E-01 0.00022  5.42049E-03 0.00086  4.51021E-04 0.00684  3.84663E-01 0.00046 ];
INF_SP2                   (idx, [1:   8]) = [  9.82229E-02 0.00034 -1.59315E-03 0.00199  2.44668E-04 0.00940  9.08207E-02 0.00110 ];
INF_SP3                   (idx, [1:   8]) = [  9.28581E-03 0.00276 -1.90265E-03 0.00138  8.49667E-05 0.01968  2.72639E-02 0.00242 ];
INF_SP4                   (idx, [1:   8]) = [ -9.74001E-03 0.00254 -6.41353E-04 0.00508 -2.10202E-06 0.75556 -8.54436E-03 0.00518 ];
INF_SP5                   (idx, [1:   8]) = [  7.27209E-05 0.28912  1.16883E-05 0.24253 -3.64524E-05 0.03338  6.45513E-03 0.00803 ];
INF_SP6                   (idx, [1:   8]) = [  5.20822E-03 0.00304 -1.48952E-04 0.01758 -4.49106E-05 0.02397 -1.65933E-02 0.00271 ];
INF_SP7                   (idx, [1:   8]) = [  8.98612E-04 0.01821 -1.73847E-04 0.01145 -4.22402E-05 0.03009  4.76088E-04 0.08429 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32542E-01 0.00063  1.10202E+00 0.00475 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34121E-01 0.00063  1.16633E+00 0.00740 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34227E-01 0.00087  1.18041E+00 0.00602 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29350E-01 0.00100  9.83467E-01 0.00475 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43345E+00 0.00063  3.02638E-01 0.00471 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42378E+00 0.00063  2.86160E-01 0.00714 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42315E+00 0.00088  2.82630E-01 0.00595 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45342E+00 0.00100  3.39123E-01 0.00482 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.58195E-03 0.00830  1.86464E-04 0.05153  1.04507E-03 0.02097  1.05976E-03 0.02049  3.01116E-03 0.01222  9.63423E-04 0.02132  3.16076E-04 0.03690 ];
LAMBDA                    (idx, [1:  14]) = [  7.98459E-01 0.01937  1.24924E-02 0.00017  3.14855E-02 0.00046  1.10199E-01 0.00046  3.21372E-01 0.00036  1.34352E+00 0.00051  8.95204E+00 0.00239 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0000' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:31 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:16:32 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284831 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00414E+00  9.90477E-01  1.00769E+00  1.00520E+00  9.92488E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12298E-02 0.00103  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88770E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.01074E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.01491E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.70070E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.72046E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.71955E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.21183E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.37834E-01 0.00105  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000977 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00020E+04 0.00081 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00020E+04 0.00081 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.78208E+02 ;
RUNNING_TIME              (idx, 1)        =  3.60202E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.22850E-01  4.22850E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  9.55833E-02  9.18334E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.51905E+01  3.52150E+00  2.73325E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  3.08050E-01  2.76000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.56000E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.60200E+01  1.30979E+02 ];
CPU_USAGE                 (idx, 1)        = 4.94746 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99880E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.74660E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.28899E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.84161E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.33268E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.76122E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.22972E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.52774E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.71861E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  5.84427E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.05810E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.76792E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.31075E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.07635E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.32702E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.62713E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.19645E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.20431E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.28602E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.59800E+11 ;
CS137_ACTIVITY            (idx, 1)        =  4.05188E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.15640E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.53621E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.79268E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.22712E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.40377E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 5 ;
BURNUP                     (idx, [1:  2])  = [  3.00000E+00  3.00027E+00 ];
BURN_DAYS                 (idx, 1)        =  7.50000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.56857E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  1.07379E+16 0.00068  7.77512E-01 0.00035 ];
U238_FISS                 (idx, [1:   4]) = [  9.37884E+14 0.00274  6.79000E-02 0.00256 ];
PU239_FISS                (idx, [1:   4]) = [  2.09453E+15 0.00174  1.51661E-01 0.00164 ];
PU240_FISS                (idx, [1:   4]) = [  8.39357E+11 0.08886  6.06578E-05 0.08875 ];
PU241_FISS                (idx, [1:   4]) = [  3.54735E+13 0.01390  2.56872E-03 0.01389 ];
U235_CAPT                 (idx, [1:   4]) = [  2.30708E+15 0.00169  1.15546E-01 0.00161 ];
U238_CAPT                 (idx, [1:   4]) = [  8.75932E+15 0.00100  4.38664E-01 0.00067 ];
PU239_CAPT                (idx, [1:   4]) = [  1.16676E+15 0.00247  5.84332E-02 0.00238 ];
PU240_CAPT                (idx, [1:   4]) = [  2.89393E+14 0.00488  1.44916E-02 0.00480 ];
PU241_CAPT                (idx, [1:   4]) = [  1.24284E+13 0.02430  6.22688E-04 0.02432 ];
XE135_CAPT                (idx, [1:   4]) = [  7.42603E+14 0.00306  3.71938E-02 0.00305 ];
SM149_CAPT                (idx, [1:   4]) = [  1.62542E+14 0.00637  8.14022E-03 0.00635 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000977 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.48725E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000977 5.00749E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2929265 2.93314E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2026208 2.02883E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45504 4.55084E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000977 5.00749E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -2.98023E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.49205E+16 1.7E-05  3.49205E+16 1.7E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38058E+16 2.7E-06  1.38058E+16 2.7E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.99638E+16 0.00047  1.40060E+16 0.00045  5.95779E+15 0.00105 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.37696E+16 0.00028  2.78118E+16 0.00023  5.95779E+15 0.00105 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.40377E+16 0.00052  3.40377E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.65444E+18 0.00049  4.39660E+17 0.00047  1.21478E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.09854E+14 0.00506 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.40794E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.26778E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12165E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12165E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.62013E+00 0.00045 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.64099E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.79295E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23167E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94101E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96778E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.03568E+00 0.00054 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.02625E+00 0.00054 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.52940E+00 1.9E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03469E+02 2.7E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.02639E+00 0.00056  1.01973E+00 0.00055  6.52924E-03 0.00855 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.02626E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.02607E+00 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.02626E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.03569E+00 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74367E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74310E+01 8.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.36633E-07 0.00352 ];
IMP_EALF                  (idx, [1:   2]) = [  5.38411E-07 0.00152 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.28697E-01 0.00290 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.29923E-01 0.00122 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.27897E-03 0.00536  1.76681E-04 0.03375  1.02997E-03 0.01413  9.61720E-04 0.01447  2.88053E-03 0.00809  9.26400E-04 0.01491  3.03660E-04 0.02646 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.02774E-01 0.01386  1.05190E-02 0.01939  3.14502E-02 0.00032  1.10129E-01 0.00032  3.21348E-01 0.00026  1.34278E+00 0.00033  8.48223E+00 0.01115 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.32606E-03 0.00822  1.76109E-04 0.05150  1.05307E-03 0.02192  9.66486E-04 0.02231  2.89275E-03 0.01228  9.51923E-04 0.02266  2.85728E-04 0.03691 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.81064E-01 0.01854  1.24929E-02 0.00021  3.14494E-02 0.00046  1.10165E-01 0.00047  3.21302E-01 0.00037  1.34288E+00 0.00038  9.01407E+00 0.00266 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.87564E-05 0.00128  2.87468E-05 0.00129  3.02338E-05 0.01330 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.95097E-05 0.00110  2.94998E-05 0.00111  3.10373E-05 0.01333 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.36413E-03 0.00869  1.77415E-04 0.05294  1.04973E-03 0.02120  9.78633E-04 0.02302  2.91381E-03 0.01343  9.55020E-04 0.02320  2.89519E-04 0.04125 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.78691E-01 0.02076  1.24947E-02 0.00033  3.14430E-02 0.00055  1.10062E-01 0.00055  3.21271E-01 0.00041  1.34148E+00 0.00091  9.01633E+00 0.00376 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.85146E-05 0.00291  2.84982E-05 0.00292  3.01854E-05 0.03238 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.92616E-05 0.00284  2.92447E-05 0.00285  3.09920E-05 0.03244 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.45497E-03 0.02928  1.90168E-04 0.16951  1.13950E-03 0.07250  9.95077E-04 0.07230  2.91349E-03 0.04410  9.33266E-04 0.08172  2.83472E-04 0.12597 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.93881E-01 0.06818  1.24905E-02 2.2E-05  3.14824E-02 0.00127  1.09974E-01 0.00120  3.20788E-01 0.00107  1.34078E+00 0.00195  8.99458E+00 0.00689 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.47026E-03 0.02845  1.88506E-04 0.16777  1.11529E-03 0.07053  9.90727E-04 0.07066  2.93799E-03 0.04282  9.39427E-04 0.08015  2.98311E-04 0.12268 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.06950E-01 0.06767  1.24905E-02 2.2E-05  3.14791E-02 0.00127  1.09973E-01 0.00120  3.20768E-01 0.00105  1.34121E+00 0.00181  8.99452E+00 0.00689 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.27621E+02 0.02934 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.87051E-05 0.00075 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.94578E-05 0.00047 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.44497E-03 0.00583 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.24608E+02 0.00591 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.40448E-07 0.00063 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.84035E-06 0.00042  2.84019E-06 0.00042  2.86579E-06 0.00547 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.27723E-05 0.00081  4.27947E-05 0.00081  3.93499E-05 0.00897 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.76711E-01 0.00032  6.76496E-01 0.00033  7.21494E-01 0.00860 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03117E+01 0.01399 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.71955E+01 0.00047  3.56372E+01 0.00047 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.43634E+04 0.00375  2.94498E+05 0.00158  6.05869E+05 0.00090  6.50205E+05 0.00076  5.97988E+05 0.00084  6.41312E+05 0.00055  4.35065E+05 0.00045  3.84956E+05 0.00066  2.94553E+05 0.00075  2.40530E+05 0.00069  2.07477E+05 0.00060  1.86972E+05 0.00076  1.72917E+05 0.00099  1.63875E+05 0.00092  1.59870E+05 0.00086  1.37868E+05 0.00114  1.36469E+05 0.00087  1.35404E+05 0.00096  1.33062E+05 0.00088  2.59561E+05 0.00073  2.50915E+05 0.00067  1.81336E+05 0.00068  1.17531E+05 0.00087  1.36299E+05 0.00102  1.28828E+05 0.00074  1.16667E+05 0.00084  1.91955E+05 0.00064  4.39533E+04 0.00122  5.51130E+04 0.00159  4.99339E+04 0.00202  2.90781E+04 0.00192  5.04157E+04 0.00181  3.42285E+04 0.00214  2.92801E+04 0.00166  5.50734E+03 0.00352  5.36722E+03 0.00373  5.23839E+03 0.00378  5.28601E+03 0.00311  5.32603E+03 0.00357  5.40484E+03 0.00411  5.75101E+03 0.00319  5.42261E+03 0.00466  1.03312E+04 0.00236  1.66769E+04 0.00215  2.11660E+04 0.00159  5.59275E+04 0.00149  5.88947E+04 0.00133  6.45347E+04 0.00132  4.35439E+04 0.00157  3.22909E+04 0.00117  2.46001E+04 0.00166  2.92461E+04 0.00134  5.69141E+04 0.00141  7.88182E+04 0.00107  1.54666E+05 0.00090  2.41512E+05 0.00103  3.63702E+05 0.00113  2.33356E+05 0.00115  1.67568E+05 0.00110  1.20569E+05 0.00103  1.08509E+05 0.00117  1.06933E+05 0.00133  8.91512E+04 0.00122  6.03477E+04 0.00124  5.57364E+04 0.00126  4.95704E+04 0.00148  4.20687E+04 0.00163  3.31255E+04 0.00154  2.21768E+04 0.00155  7.86412E+03 0.00204 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.03549E+00 0.00060 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.27229E+18 0.00058  3.82192E+17 0.00091 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38515E-01 0.00012  1.52452E+00 0.00033 ];
INF_CAPT                  (idx, [1:   4]) = [  6.55643E-03 0.00060  3.04120E-02 0.00036 ];
INF_ABS                   (idx, [1:   4]) = [  8.54631E-03 0.00052  5.99157E-02 0.00063 ];
INF_FISS                  (idx, [1:   4]) = [  1.98987E-03 0.00050  2.95037E-02 0.00092 ];
INF_NSF                   (idx, [1:   4]) = [  5.17266E-03 0.00052  7.41624E-02 0.00093 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.59949E+00 7.3E-05  2.51366E+00 2.9E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04315E+02 7.1E-06  2.03279E+02 4.7E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.89551E-08 0.00046  2.57632E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29973E-01 0.00012  1.46457E+00 0.00037 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44867E-01 0.00018  3.86646E-01 0.00047 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65369E-02 0.00027  9.14547E-02 0.00076 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35435E-03 0.00328  2.75509E-02 0.00264 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03920E-02 0.00195 -8.59074E-03 0.00596 ];
INF_SCATT5                (idx, [1:   4]) = [  1.06510E-04 0.19460  6.39867E-03 0.00727 ];
INF_SCATT6                (idx, [1:   4]) = [  5.06632E-03 0.00374 -1.66786E-02 0.00272 ];
INF_SCATT7                (idx, [1:   4]) = [  7.31148E-04 0.02182  3.65922E-04 0.11919 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.30013E-01 0.00012  1.46457E+00 0.00037 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44868E-01 0.00018  3.86646E-01 0.00047 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65373E-02 0.00027  9.14547E-02 0.00076 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35419E-03 0.00328  2.75509E-02 0.00264 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03918E-02 0.00195 -8.59074E-03 0.00596 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.06182E-04 0.19531  6.39867E-03 0.00727 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.06611E-03 0.00375 -1.66786E-02 0.00272 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.30904E-04 0.02181  3.65922E-04 0.11919 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13054E-01 0.00030  9.86627E-01 0.00030 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56455E+00 0.00030  3.37852E-01 0.00030 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.50624E-03 0.00052  5.99157E-02 0.00063 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69558E-02 0.00024  6.10139E-02 0.00076 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11559E-01 0.00012  1.84139E-02 0.00041  1.06633E-03 0.00489  1.46351E+00 0.00037 ];
INF_S1                    (idx, [1:   8]) = [  2.39494E-01 0.00018  5.37352E-03 0.00096  4.58628E-04 0.00700  3.86187E-01 0.00048 ];
INF_S2                    (idx, [1:   8]) = [  9.81373E-02 0.00026 -1.60040E-03 0.00224  2.52078E-04 0.01092  9.12026E-02 0.00076 ];
INF_S3                    (idx, [1:   8]) = [  9.24733E-03 0.00247 -1.89298E-03 0.00158  9.16054E-05 0.01829  2.74593E-02 0.00264 ];
INF_S4                    (idx, [1:   8]) = [ -9.76876E-03 0.00207 -6.23190E-04 0.00421  2.61804E-06 0.46269 -8.59335E-03 0.00591 ];
INF_S5                    (idx, [1:   8]) = [  9.00209E-05 0.23272  1.64896E-05 0.14564 -3.48544E-05 0.03224  6.43353E-03 0.00719 ];
INF_S6                    (idx, [1:   8]) = [  5.21302E-03 0.00352 -1.46702E-04 0.01530 -4.52891E-05 0.02892 -1.66333E-02 0.00271 ];
INF_S7                    (idx, [1:   8]) = [  9.03698E-04 0.01697 -1.72550E-04 0.01304 -4.26577E-05 0.02269  4.08579E-04 0.10735 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11599E-01 0.00012  1.84139E-02 0.00041  1.06633E-03 0.00489  1.46351E+00 0.00037 ];
INF_SP1                   (idx, [1:   8]) = [  2.39494E-01 0.00018  5.37352E-03 0.00096  4.58628E-04 0.00700  3.86187E-01 0.00048 ];
INF_SP2                   (idx, [1:   8]) = [  9.81377E-02 0.00026 -1.60040E-03 0.00224  2.52078E-04 0.01092  9.12026E-02 0.00076 ];
INF_SP3                   (idx, [1:   8]) = [  9.24717E-03 0.00247 -1.89298E-03 0.00158  9.16054E-05 0.01829  2.74593E-02 0.00264 ];
INF_SP4                   (idx, [1:   8]) = [ -9.76856E-03 0.00207 -6.23190E-04 0.00421  2.61804E-06 0.46269 -8.59335E-03 0.00591 ];
INF_SP5                   (idx, [1:   8]) = [  8.96928E-05 0.23370  1.64896E-05 0.14564 -3.48544E-05 0.03224  6.43353E-03 0.00719 ];
INF_SP6                   (idx, [1:   8]) = [  5.21281E-03 0.00352 -1.46702E-04 0.01530 -4.52891E-05 0.02892 -1.66333E-02 0.00271 ];
INF_SP7                   (idx, [1:   8]) = [  9.03454E-04 0.01695 -1.72550E-04 0.01304 -4.26577E-05 0.02269  4.08579E-04 0.10735 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32582E-01 0.00054  1.12217E+00 0.00615 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34347E-01 0.00087  1.19906E+00 0.00880 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34264E-01 0.00084  1.19883E+00 0.00664 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29217E-01 0.00084  9.95777E-01 0.00643 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43320E+00 0.00054  2.97323E-01 0.00637 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42242E+00 0.00087  2.78536E-01 0.00920 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42292E+00 0.00084  2.78348E-01 0.00675 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45425E+00 0.00084  3.35084E-01 0.00652 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.32606E-03 0.00822  1.76109E-04 0.05150  1.05307E-03 0.02192  9.66486E-04 0.02231  2.89275E-03 0.01228  9.51923E-04 0.02266  2.85728E-04 0.03691 ];
LAMBDA                    (idx, [1:  14]) = [  7.81064E-01 0.01854  1.24929E-02 0.00021  3.14494E-02 0.00046  1.10165E-01 0.00047  3.21302E-01 0.00037  1.34288E+00 0.00038  9.01407E+00 0.00266 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0000' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:31 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:22:50 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284831 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00308E+00  9.99496E-01  1.00549E+00  1.00218E+00  9.89753E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12300E-02 0.00102  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88770E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.02693E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.03110E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69278E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.70641E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.70549E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.08389E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.34732E-01 0.00102  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000877 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00018E+04 0.00080 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00018E+04 0.00080 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.09640E+02 ;
RUNNING_TIME              (idx, 1)        =  4.23151E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.22850E-01  4.22850E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.14617E-01  9.18333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  4.14079E+01  3.49818E+00  2.71922E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  3.65967E-01  2.91167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.74333E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.23150E+01  1.30757E+02 ];
CPU_USAGE                 (idx, 1)        = 4.95426 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99740E+00 0.00055 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76262E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.32888E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.82493E+04 ;
TOT_SF_RATE               (idx, 1)        =  9.42293E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.77290E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.23780E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.55596E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.70112E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  6.80565E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.16070E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.24788E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.37441E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.55777E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.42326E+07 ;
SR90_ACTIVITY             (idx, 1)        =  4.74951E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.22560E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.25505E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.32666E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.23060E+12 ;
CS137_ACTIVITY            (idx, 1)        =  5.40319E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.17894E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.50315E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  8.85159E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.23561E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.45733E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 6 ;
BURNUP                     (idx, [1:  2])  = [  4.00000E+00  4.00037E+00 ];
BURN_DAYS                 (idx, 1)        =  1.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.65442E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  1.01808E+16 0.00071  7.38477E-01 0.00043 ];
U238_FISS                 (idx, [1:   4]) = [  9.61726E+14 0.00273  6.97500E-02 0.00256 ];
PU239_FISS                (idx, [1:   4]) = [  2.56774E+15 0.00158  1.86256E-01 0.00149 ];
PU240_FISS                (idx, [1:   4]) = [  1.31953E+12 0.07427  9.56493E-05 0.07436 ];
PU241_FISS                (idx, [1:   4]) = [  6.99425E+13 0.01010  5.07251E-03 0.01005 ];
U235_CAPT                 (idx, [1:   4]) = [  2.19699E+15 0.00178  1.07063E-01 0.00168 ];
U238_CAPT                 (idx, [1:   4]) = [  8.81493E+15 0.00100  4.29546E-01 0.00069 ];
PU239_CAPT                (idx, [1:   4]) = [  1.42628E+15 0.00225  6.95078E-02 0.00221 ];
PU240_CAPT                (idx, [1:   4]) = [  4.37313E+14 0.00398  2.13086E-02 0.00388 ];
PU241_CAPT                (idx, [1:   4]) = [  2.59379E+13 0.01543  1.26460E-03 0.01547 ];
XE135_CAPT                (idx, [1:   4]) = [  7.46784E+14 0.00298  3.63957E-02 0.00298 ];
SM149_CAPT                (idx, [1:   4]) = [  1.69196E+14 0.00642  8.24513E-03 0.00639 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000877 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.63723E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000877 5.00764E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2963673 2.96771E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1991159 1.99387E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46045 4.60522E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000877 5.00764E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.53903E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.51148E+16 1.7E-05  3.51148E+16 1.7E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37906E+16 2.9E-06  1.37906E+16 2.9E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.05248E+16 0.00044  1.45088E+16 0.00046  6.01600E+15 0.00098 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.43154E+16 0.00026  2.82994E+16 0.00023  6.01600E+15 0.00098 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.45733E+16 0.00049  3.45733E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.67498E+18 0.00047  4.43781E+17 0.00045  1.23120E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.18472E+14 0.00486 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.46338E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.28293E+18 0.00058 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12049E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12049E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.60952E+00 0.00046 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.64599E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.75847E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23219E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94034E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96736E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.02478E+00 0.00054 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.01534E+00 0.00054 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.54629E+00 2.0E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03693E+02 2.9E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.01574E+00 0.00056  1.00909E+00 0.00054  6.25315E-03 0.00875 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.01546E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.01578E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.01546E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.02490E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73907E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73963E+01 8.6E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.61866E-07 0.00348 ];
IMP_EALF                  (idx, [1:   2]) = [  5.57371E-07 0.00150 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.35336E-01 0.00280 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.34026E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.13423E-03 0.00584  1.64760E-04 0.03552  1.02129E-03 0.01401  9.49775E-04 0.01457  2.78559E-03 0.00831  9.21450E-04 0.01416  2.91365E-04 0.02554 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.94005E-01 0.01329  1.02167E-02 0.02112  3.13799E-02 0.00036  1.10196E-01 0.00035  3.21619E-01 0.00025  1.33959E+00 0.00052  8.34819E+00 0.01258 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.20369E-03 0.00832  1.66431E-04 0.05200  1.04015E-03 0.02174  9.47042E-04 0.02202  2.81069E-03 0.01194  9.42395E-04 0.02117  2.96978E-04 0.04144 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.92562E-01 0.02095  1.24900E-02 1.1E-05  3.13736E-02 0.00053  1.10203E-01 0.00051  3.21698E-01 0.00039  1.33982E+00 0.00063  8.97220E+00 0.00299 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.85244E-05 0.00126  2.85103E-05 0.00126  3.08453E-05 0.01333 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.89685E-05 0.00110  2.89542E-05 0.00111  3.13314E-05 0.01334 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.16741E-03 0.00884  1.54498E-04 0.05674  1.04619E-03 0.02273  9.28602E-04 0.02341  2.79497E-03 0.01356  9.51496E-04 0.02368  2.91655E-04 0.04240 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.01173E-01 0.02221  1.24899E-02 1.6E-05  3.13568E-02 0.00063  1.10216E-01 0.00061  3.21738E-01 0.00043  1.33880E+00 0.00094  8.96641E+00 0.00392 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.84254E-05 0.00289  2.84094E-05 0.00292  2.99028E-05 0.03475 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.88692E-05 0.00286  2.88528E-05 0.00288  3.03866E-05 0.03486 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.98466E-03 0.03025  1.03739E-04 0.22015  1.09608E-03 0.07291  8.75326E-04 0.08089  2.72758E-03 0.04546  9.26333E-04 0.08440  2.55603E-04 0.13831 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.73236E-01 0.06896  1.24898E-02 3.5E-05  3.13770E-02 0.00142  1.09967E-01 0.00140  3.21369E-01 0.00121  1.34197E+00 0.00178  8.89493E+00 0.01232 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.06689E-03 0.02996  1.04982E-04 0.20737  1.07512E-03 0.07100  8.92663E-04 0.07925  2.79067E-03 0.04469  9.29576E-04 0.08119  2.73876E-04 0.13556 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.87980E-01 0.06728  1.24898E-02 3.5E-05  3.13784E-02 0.00142  1.09974E-01 0.00139  3.21431E-01 0.00121  1.34185E+00 0.00178  8.89392E+00 0.01230 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.12030E+02 0.03061 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.84830E-05 0.00076 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.89269E-05 0.00052 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.12179E-03 0.00504 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.15035E+02 0.00518 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.35631E-07 0.00058 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.82109E-06 0.00044  2.82089E-06 0.00044  2.85459E-06 0.00542 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.25116E-05 0.00072  4.25310E-05 0.00072  3.94943E-05 0.00932 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.73269E-01 0.00031  6.73109E-01 0.00032  7.10380E-01 0.00871 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.01677E+01 0.01350 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.70549E+01 0.00043  3.53761E+01 0.00046 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.49031E+04 0.00317  2.96271E+05 0.00114  6.06416E+05 0.00075  6.50513E+05 0.00065  5.97479E+05 0.00079  6.41191E+05 0.00069  4.35177E+05 0.00078  3.84349E+05 0.00076  2.94450E+05 0.00063  2.40262E+05 0.00062  2.07655E+05 0.00074  1.86690E+05 0.00098  1.72497E+05 0.00101  1.64009E+05 0.00080  1.59881E+05 0.00094  1.38039E+05 0.00082  1.36465E+05 0.00100  1.35227E+05 0.00086  1.32550E+05 0.00094  2.59790E+05 0.00056  2.50843E+05 0.00054  1.81224E+05 0.00081  1.17546E+05 0.00091  1.35853E+05 0.00107  1.28632E+05 0.00094  1.16763E+05 0.00112  1.91292E+05 0.00098  4.38299E+04 0.00162  5.51233E+04 0.00132  4.98975E+04 0.00157  2.90353E+04 0.00175  5.04332E+04 0.00194  3.42097E+04 0.00152  2.89667E+04 0.00217  5.40410E+03 0.00338  5.18872E+03 0.00332  5.00991E+03 0.00311  5.03085E+03 0.00317  5.07211E+03 0.00353  5.20308E+03 0.00306  5.62918E+03 0.00289  5.36771E+03 0.00434  1.01959E+04 0.00284  1.63519E+04 0.00253  2.09941E+04 0.00236  5.54043E+04 0.00139  5.83795E+04 0.00137  6.35243E+04 0.00149  4.29158E+04 0.00140  3.14289E+04 0.00129  2.38443E+04 0.00173  2.85236E+04 0.00139  5.54888E+04 0.00141  7.72098E+04 0.00131  1.52113E+05 0.00131  2.38183E+05 0.00115  3.59195E+05 0.00113  2.30825E+05 0.00140  1.65768E+05 0.00137  1.19472E+05 0.00134  1.07404E+05 0.00130  1.05863E+05 0.00116  8.84368E+04 0.00133  5.98391E+04 0.00115  5.52760E+04 0.00155  4.91720E+04 0.00158  4.16807E+04 0.00160  3.29085E+04 0.00162  2.20588E+04 0.00146  7.78802E+03 0.00214 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.02523E+00 0.00056 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.29171E+18 0.00052  3.83295E+17 0.00094 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38540E-01 0.00010  1.53226E+00 0.00035 ];
INF_CAPT                  (idx, [1:   4]) = [  6.67712E-03 0.00052  3.10486E-02 0.00042 ];
INF_ABS                   (idx, [1:   4]) = [  8.63712E-03 0.00046  6.04272E-02 0.00066 ];
INF_FISS                  (idx, [1:   4]) = [  1.96000E-03 0.00065  2.93786E-02 0.00093 ];
INF_NSF                   (idx, [1:   4]) = [  5.11207E-03 0.00067  7.43972E-02 0.00095 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.60820E+00 5.2E-05  2.53236E+00 3.0E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04425E+02 4.8E-06  2.03528E+02 4.9E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.86465E-08 0.00051  2.58018E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29903E-01 0.00011  1.47186E+00 0.00039 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44964E-01 0.00019  3.88696E-01 0.00043 ];
INF_SCATT2                (idx, [1:   4]) = [  9.66188E-02 0.00023  9.17387E-02 0.00091 ];
INF_SCATT3                (idx, [1:   4]) = [  7.40010E-03 0.00285  2.74502E-02 0.00207 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03575E-02 0.00160 -8.79250E-03 0.00620 ];
INF_SCATT5                (idx, [1:   4]) = [  1.45908E-04 0.13501  6.50706E-03 0.00713 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08817E-03 0.00326 -1.67333E-02 0.00234 ];
INF_SCATT7                (idx, [1:   4]) = [  7.49321E-04 0.01902  4.22086E-04 0.09916 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29944E-01 0.00011  1.47186E+00 0.00039 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44965E-01 0.00019  3.88696E-01 0.00043 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.66189E-02 0.00023  9.17387E-02 0.00091 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.39990E-03 0.00285  2.74502E-02 0.00207 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03576E-02 0.00160 -8.79250E-03 0.00620 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.45899E-04 0.13482  6.50706E-03 0.00713 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08796E-03 0.00326 -1.67333E-02 0.00234 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.49320E-04 0.01899  4.22086E-04 0.09916 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12761E-01 0.00032  9.92011E-01 0.00031 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56671E+00 0.00032  3.36018E-01 0.00031 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.59624E-03 0.00048  6.04272E-02 0.00066 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69658E-02 0.00013  6.14764E-02 0.00084 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11575E-01 0.00010  1.83287E-02 0.00033  1.07538E-03 0.00383  1.47079E+00 0.00039 ];
INF_S1                    (idx, [1:   8]) = [  2.39614E-01 0.00019  5.35007E-03 0.00064  4.61262E-04 0.00598  3.88234E-01 0.00043 ];
INF_S2                    (idx, [1:   8]) = [  9.82141E-02 0.00023 -1.59525E-03 0.00234  2.49126E-04 0.00933  9.14896E-02 0.00092 ];
INF_S3                    (idx, [1:   8]) = [  9.28803E-03 0.00219 -1.88793E-03 0.00206  8.87465E-05 0.01459  2.73615E-02 0.00208 ];
INF_S4                    (idx, [1:   8]) = [ -9.73355E-03 0.00165 -6.23973E-04 0.00376  2.91204E-07 1.00000 -8.79279E-03 0.00622 ];
INF_S5                    (idx, [1:   8]) = [  1.23556E-04 0.15332  2.23522E-05 0.12447 -3.61103E-05 0.03565  6.54317E-03 0.00704 ];
INF_S6                    (idx, [1:   8]) = [  5.22952E-03 0.00299 -1.41351E-04 0.01818 -4.58386E-05 0.02117 -1.66874E-02 0.00234 ];
INF_S7                    (idx, [1:   8]) = [  9.21572E-04 0.01538 -1.72251E-04 0.01003 -4.16286E-05 0.03052  4.63714E-04 0.09003 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11616E-01 0.00010  1.83287E-02 0.00033  1.07538E-03 0.00383  1.47079E+00 0.00039 ];
INF_SP1                   (idx, [1:   8]) = [  2.39615E-01 0.00019  5.35007E-03 0.00064  4.61262E-04 0.00598  3.88234E-01 0.00043 ];
INF_SP2                   (idx, [1:   8]) = [  9.82141E-02 0.00023 -1.59525E-03 0.00234  2.49126E-04 0.00933  9.14896E-02 0.00092 ];
INF_SP3                   (idx, [1:   8]) = [  9.28783E-03 0.00219 -1.88793E-03 0.00206  8.87465E-05 0.01459  2.73615E-02 0.00208 ];
INF_SP4                   (idx, [1:   8]) = [ -9.73358E-03 0.00165 -6.23973E-04 0.00376  2.91204E-07 1.00000 -8.79279E-03 0.00622 ];
INF_SP5                   (idx, [1:   8]) = [  1.23547E-04 0.15311  2.23522E-05 0.12447 -3.61103E-05 0.03565  6.54317E-03 0.00704 ];
INF_SP6                   (idx, [1:   8]) = [  5.22931E-03 0.00299 -1.41351E-04 0.01818 -4.58386E-05 0.02117 -1.66874E-02 0.00234 ];
INF_SP7                   (idx, [1:   8]) = [  9.21571E-04 0.01535 -1.72251E-04 0.01003 -4.16286E-05 0.03052  4.63714E-04 0.09003 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32209E-01 0.00049  1.12656E+00 0.00600 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33874E-01 0.00072  1.20886E+00 0.00804 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33956E-01 0.00076  1.19915E+00 0.00769 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28878E-01 0.00089  9.99275E-01 0.00621 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43550E+00 0.00049  2.96150E-01 0.00621 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42529E+00 0.00072  2.76181E-01 0.00826 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42479E+00 0.00076  2.78373E-01 0.00775 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45641E+00 0.00089  3.33896E-01 0.00646 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.20369E-03 0.00832  1.66431E-04 0.05200  1.04015E-03 0.02174  9.47042E-04 0.02202  2.81069E-03 0.01194  9.42395E-04 0.02117  2.96978E-04 0.04144 ];
LAMBDA                    (idx, [1:  14]) = [  7.92562E-01 0.02095  1.24900E-02 1.1E-05  3.13736E-02 0.00053  1.10203E-01 0.00051  3.21698E-01 0.00039  1.33982E+00 0.00063  8.97220E+00 0.00299 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0000' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:31 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:29:07 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284831 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00173E+00  9.97667E-01  1.00607E+00  1.00429E+00  9.90250E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12330E-02 0.00104  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88767E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.03356E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.03774E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68716E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.68200E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.68107E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.98602E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.31225E-01 0.00108  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000879 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00018E+04 0.00081 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00018E+04 0.00081 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.40999E+02 ;
RUNNING_TIME              (idx, 1)        =  4.85931E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.22850E-01  4.22850E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.33183E-01  9.38334E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  4.76120E+01  3.48500E+00  2.71913E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  4.20717E-01  2.84500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.91500E-02  8.49998E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.85930E+01  1.30402E+02 ];
CPU_USAGE                 (idx, 1)        = 4.95952 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99974E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.77485E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.36623E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.81168E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.14850E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.79000E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.24967E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.57620E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.68668E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  7.77553E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.24209E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.82082E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.46211E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.95471E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.49588E+07 ;
SR90_ACTIVITY             (idx, 1)        =  5.83787E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.25198E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.29443E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.36283E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.96993E+12 ;
CS137_ACTIVITY            (idx, 1)        =  6.75400E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.20428E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.47555E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.32925E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.24408E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.51430E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 7 ;
BURNUP                     (idx, [1:  2])  = [  5.00000E+00  5.00045E+00 ];
BURN_DAYS                 (idx, 1)        =  1.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.75472E-01 0.00112 ];
U235_FISS                 (idx, [1:   4]) = [  9.67902E+15 0.00074  7.02262E-01 0.00049 ];
U238_FISS                 (idx, [1:   4]) = [  9.78402E+14 0.00268  7.09782E-02 0.00251 ];
PU239_FISS                (idx, [1:   4]) = [  3.00303E+15 0.00153  2.17883E-01 0.00141 ];
PU240_FISS                (idx, [1:   4]) = [  1.65590E+12 0.06504  1.20310E-04 0.06495 ];
PU241_FISS                (idx, [1:   4]) = [  1.15034E+14 0.00750  8.34581E-03 0.00746 ];
U235_CAPT                 (idx, [1:   4]) = [  2.10857E+15 0.00173  1.00003E-01 0.00167 ];
U238_CAPT                 (idx, [1:   4]) = [  8.90918E+15 0.00108  4.22492E-01 0.00073 ];
PU239_CAPT                (idx, [1:   4]) = [  1.65634E+15 0.00205  7.85580E-02 0.00203 ];
PU240_CAPT                (idx, [1:   4]) = [  5.88507E+14 0.00345  2.79076E-02 0.00334 ];
PU241_CAPT                (idx, [1:   4]) = [  4.17426E+13 0.01289  1.97980E-03 0.01290 ];
XE135_CAPT                (idx, [1:   4]) = [  7.48866E+14 0.00321  3.55174E-02 0.00319 ];
SM149_CAPT                (idx, [1:   4]) = [  1.75822E+14 0.00626  8.33895E-03 0.00624 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000879 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.62196E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000879 5.00762E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2995876 3.00002E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1958457 1.96106E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46546 4.65461E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000879 5.00762E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -1.40630E-07 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.52924E+16 1.9E-05  3.52924E+16 1.9E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37765E+16 3.5E-06  1.37765E+16 3.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.10616E+16 0.00046  1.50258E+16 0.00047  6.03576E+15 0.00109 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.48381E+16 0.00028  2.88023E+16 0.00025  6.03576E+15 0.00109 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.51430E+16 0.00050  3.51430E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.69612E+18 0.00048  4.49337E+17 0.00047  1.24679E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.27196E+14 0.00473 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.51653E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.29541E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11932E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11932E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.59936E+00 0.00047 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.66114E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.71771E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23234E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93992E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96679E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.01429E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.00484E+00 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.56178E+00 2.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03901E+02 3.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.00487E+00 0.00057  9.98755E-01 0.00056  6.08894E-03 0.00892 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.00519E+00 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  1.00438E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.00519E+00 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  1.01464E+00 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73549E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73581E+01 8.4E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.82390E-07 0.00350 ];
IMP_EALF                  (idx, [1:   2]) = [  5.79085E-07 0.00145 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.39756E-01 0.00284 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.38961E-01 0.00117 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.10102E-03 0.00594  1.70116E-04 0.03374  1.03451E-03 0.01393  9.34893E-04 0.01447  2.76173E-03 0.00889  9.26440E-04 0.01491  2.73337E-04 0.02870 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.68673E-01 0.01411  1.02724E-02 0.02083  3.13249E-02 0.00035  1.10235E-01 0.00038  3.21699E-01 0.00028  1.33599E+00 0.00070  8.34806E+00 0.01263 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.05984E-03 0.00824  1.60791E-04 0.05193  1.03448E-03 0.02080  9.17583E-04 0.02101  2.77275E-03 0.01243  9.08876E-04 0.02177  2.65358E-04 0.04175 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.58165E-01 0.02071  1.24993E-02 0.00038  3.13212E-02 0.00053  1.10227E-01 0.00053  3.21662E-01 0.00040  1.33619E+00 0.00096  8.96654E+00 0.00409 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.84222E-05 0.00131  2.84097E-05 0.00132  3.03302E-05 0.01398 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.85559E-05 0.00118  2.85433E-05 0.00119  3.04792E-05 0.01399 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.07085E-03 0.00903  1.73874E-04 0.05252  1.02405E-03 0.02292  9.43219E-04 0.02446  2.76100E-03 0.01362  8.97685E-04 0.02355  2.71014E-04 0.04620 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.63638E-01 0.02355  1.24989E-02 0.00040  3.13449E-02 0.00062  1.10227E-01 0.00065  3.21675E-01 0.00044  1.33658E+00 0.00106  8.97599E+00 0.00505 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.84157E-05 0.00302  2.84042E-05 0.00303  3.05666E-05 0.04096 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.85491E-05 0.00296  2.85375E-05 0.00297  3.07006E-05 0.04098 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.09444E-03 0.02874  2.08458E-04 0.17169  8.57485E-04 0.07236  9.63832E-04 0.07028  2.87346E-03 0.04484  9.31645E-04 0.07549  2.59558E-04 0.15143 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.33106E-01 0.07288  1.24899E-02 2.8E-05  3.13372E-02 0.00157  1.10295E-01 0.00150  3.22130E-01 0.00128  1.34086E+00 0.00173  8.86550E+00 0.01085 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.07587E-03 0.02832  2.04606E-04 0.16026  8.60925E-04 0.06937  9.58542E-04 0.06838  2.87588E-03 0.04433  9.17325E-04 0.07413  2.58590E-04 0.14887 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.38066E-01 0.07244  1.24899E-02 2.8E-05  3.13350E-02 0.00155  1.10298E-01 0.00150  3.22147E-01 0.00126  1.34088E+00 0.00173  8.85864E+00 0.01098 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.15630E+02 0.02891 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.84102E-05 0.00083 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.85439E-05 0.00060 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.08651E-03 0.00619 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.14340E+02 0.00631 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.28216E-07 0.00067 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.80901E-06 0.00041  2.80893E-06 0.00041  2.82137E-06 0.00577 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.20762E-05 0.00084  4.20967E-05 0.00084  3.88795E-05 0.00898 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.69206E-01 0.00033  6.69091E-01 0.00034  6.99453E-01 0.00890 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05906E+01 0.01459 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.68107E+01 0.00048  3.51385E+01 0.00047 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.54378E+04 0.00356  2.97956E+05 0.00146  6.07818E+05 0.00095  6.50705E+05 0.00074  5.98103E+05 0.00066  6.40297E+05 0.00083  4.35299E+05 0.00079  3.84585E+05 0.00075  2.94261E+05 0.00093  2.40743E+05 0.00081  2.07181E+05 0.00081  1.86839E+05 0.00090  1.72461E+05 0.00099  1.63993E+05 0.00095  1.59586E+05 0.00077  1.37829E+05 0.00100  1.36605E+05 0.00104  1.35361E+05 0.00056  1.32982E+05 0.00086  2.59308E+05 0.00071  2.50857E+05 0.00077  1.81301E+05 0.00087  1.17353E+05 0.00088  1.35876E+05 0.00078  1.28765E+05 0.00086  1.16476E+05 0.00095  1.90793E+05 0.00091  4.38472E+04 0.00151  5.49410E+04 0.00111  4.98485E+04 0.00195  2.89305E+04 0.00191  5.03114E+04 0.00139  3.41304E+04 0.00170  2.88156E+04 0.00188  5.38088E+03 0.00418  5.09898E+03 0.00408  4.85495E+03 0.00352  4.75399E+03 0.00301  4.84316E+03 0.00377  5.05035E+03 0.00309  5.51974E+03 0.00320  5.27590E+03 0.00341  1.00729E+04 0.00275  1.62391E+04 0.00204  2.07924E+04 0.00236  5.50304E+04 0.00143  5.80852E+04 0.00122  6.28862E+04 0.00111  4.22969E+04 0.00126  3.08882E+04 0.00185  2.33861E+04 0.00156  2.77177E+04 0.00143  5.40135E+04 0.00158  7.54453E+04 0.00106  1.49003E+05 0.00090  2.33923E+05 0.00120  3.53148E+05 0.00114  2.27164E+05 0.00145  1.63203E+05 0.00155  1.17497E+05 0.00187  1.05690E+05 0.00174  1.04212E+05 0.00188  8.70704E+04 0.00153  5.89650E+04 0.00176  5.44813E+04 0.00187  4.84649E+04 0.00185  4.10674E+04 0.00227  3.23861E+04 0.00192  2.16934E+04 0.00186  7.70376E+03 0.00233 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.01382E+00 0.00053 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.31303E+18 0.00049  3.83127E+17 0.00126 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38464E-01 0.00014  1.53581E+00 0.00051 ];
INF_CAPT                  (idx, [1:   4]) = [  6.80198E-03 0.00072  3.16645E-02 0.00060 ];
INF_ABS                   (idx, [1:   4]) = [  8.73655E-03 0.00056  6.09981E-02 0.00093 ];
INF_FISS                  (idx, [1:   4]) = [  1.93457E-03 0.00042  2.93336E-02 0.00130 ];
INF_NSF                   (idx, [1:   4]) = [  5.06195E-03 0.00041  7.47828E-02 0.00133 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.61658E+00 6.8E-05  2.54939E+00 4.9E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04533E+02 8.0E-06  2.03758E+02 8.1E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.83747E-08 0.00033  2.58127E-06 0.00021 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29720E-01 0.00015  1.47476E+00 0.00057 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44894E-01 0.00024  3.89814E-01 0.00068 ];
INF_SCATT2                (idx, [1:   4]) = [  9.66152E-02 0.00029  9.21002E-02 0.00091 ];
INF_SCATT3                (idx, [1:   4]) = [  7.39678E-03 0.00338  2.77198E-02 0.00261 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03900E-02 0.00265 -8.69504E-03 0.00576 ];
INF_SCATT5                (idx, [1:   4]) = [  1.23153E-04 0.14528  6.49557E-03 0.00554 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08272E-03 0.00362 -1.67957E-02 0.00341 ];
INF_SCATT7                (idx, [1:   4]) = [  7.45519E-04 0.02384  4.32712E-04 0.08224 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29761E-01 0.00015  1.47476E+00 0.00057 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44895E-01 0.00024  3.89814E-01 0.00068 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.66154E-02 0.00029  9.21002E-02 0.00091 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.39669E-03 0.00338  2.77198E-02 0.00261 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03900E-02 0.00265 -8.69504E-03 0.00576 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.23081E-04 0.14557  6.49557E-03 0.00554 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08258E-03 0.00362 -1.67957E-02 0.00341 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.45535E-04 0.02389  4.32712E-04 0.08224 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12590E-01 0.00036  9.94595E-01 0.00043 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56797E+00 0.00035  3.35146E-01 0.00043 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.69575E-03 0.00057  6.09981E-02 0.00093 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69623E-02 0.00027  6.21354E-02 0.00104 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11502E-01 0.00014  1.82183E-02 0.00040  1.08796E-03 0.00478  1.47368E+00 0.00057 ];
INF_S1                    (idx, [1:   8]) = [  2.39584E-01 0.00023  5.30959E-03 0.00108  4.71585E-04 0.00587  3.89342E-01 0.00068 ];
INF_S2                    (idx, [1:   8]) = [  9.82063E-02 0.00027 -1.59112E-03 0.00261  2.55866E-04 0.00890  9.18443E-02 0.00092 ];
INF_S3                    (idx, [1:   8]) = [  9.27312E-03 0.00257 -1.87634E-03 0.00171  8.88953E-05 0.02261  2.76309E-02 0.00258 ];
INF_S4                    (idx, [1:   8]) = [ -9.77092E-03 0.00281 -6.19053E-04 0.00382 -2.08722E-06 0.67780 -8.69296E-03 0.00571 ];
INF_S5                    (idx, [1:   8]) = [  1.00382E-04 0.18329  2.27715E-05 0.08916 -3.71347E-05 0.04248  6.53270E-03 0.00546 ];
INF_S6                    (idx, [1:   8]) = [  5.22218E-03 0.00364 -1.39456E-04 0.01585 -4.64767E-05 0.02362 -1.67492E-02 0.00342 ];
INF_S7                    (idx, [1:   8]) = [  9.15474E-04 0.01957 -1.69955E-04 0.00921 -4.16955E-05 0.03017  4.74408E-04 0.07414 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11543E-01 0.00014  1.82183E-02 0.00040  1.08796E-03 0.00478  1.47368E+00 0.00057 ];
INF_SP1                   (idx, [1:   8]) = [  2.39585E-01 0.00023  5.30959E-03 0.00108  4.71585E-04 0.00587  3.89342E-01 0.00068 ];
INF_SP2                   (idx, [1:   8]) = [  9.82066E-02 0.00027 -1.59112E-03 0.00261  2.55866E-04 0.00890  9.18443E-02 0.00092 ];
INF_SP3                   (idx, [1:   8]) = [  9.27303E-03 0.00257 -1.87634E-03 0.00171  8.88953E-05 0.02261  2.76309E-02 0.00258 ];
INF_SP4                   (idx, [1:   8]) = [ -9.77095E-03 0.00281 -6.19053E-04 0.00382 -2.08722E-06 0.67780 -8.69296E-03 0.00571 ];
INF_SP5                   (idx, [1:   8]) = [  1.00310E-04 0.18375  2.27715E-05 0.08916 -3.71347E-05 0.04248  6.53270E-03 0.00546 ];
INF_SP6                   (idx, [1:   8]) = [  5.22204E-03 0.00364 -1.39456E-04 0.01585 -4.64767E-05 0.02362 -1.67492E-02 0.00342 ];
INF_SP7                   (idx, [1:   8]) = [  9.15491E-04 0.01961 -1.69955E-04 0.00921 -4.16955E-05 0.03017  4.74408E-04 0.07414 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32243E-01 0.00062  1.15909E+00 0.00607 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33905E-01 0.00095  1.24278E+00 0.00761 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33991E-01 0.00105  1.24547E+00 0.00707 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28915E-01 0.00085  1.02107E+00 0.00807 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43529E+00 0.00062  2.87841E-01 0.00617 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42511E+00 0.00095  2.68589E-01 0.00763 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42460E+00 0.00105  2.67962E-01 0.00713 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45617E+00 0.00086  3.26972E-01 0.00816 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.05984E-03 0.00824  1.60791E-04 0.05193  1.03448E-03 0.02080  9.17583E-04 0.02101  2.77275E-03 0.01243  9.08876E-04 0.02177  2.65358E-04 0.04175 ];
LAMBDA                    (idx, [1:  14]) = [  7.58165E-01 0.02071  1.24993E-02 0.00038  3.13212E-02 0.00053  1.10227E-01 0.00053  3.21662E-01 0.00040  1.33619E+00 0.00096  8.96654E+00 0.00409 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0000' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:31 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:35:23 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284831 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00332E+00  9.96974E-01  1.00625E+00  1.00414E+00  9.89321E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.0E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12424E-02 0.00103  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88758E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.03798E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04217E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68313E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.66479E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.66385E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.91899E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.28979E-01 0.00107  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000657 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00013E+04 0.00081 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00013E+04 0.00081 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.72302E+02 ;
RUNNING_TIME              (idx, 1)        =  5.48606E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.22850E-01  4.22850E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.52017E-01  8.90000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  5.38060E+01  3.48335E+00  2.71062E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  4.74900E-01  2.54500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.08833E-02  8.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  5.48604E+01  1.30032E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96353 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99891E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78423E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.39624E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.80012E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.50812E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.80602E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.26082E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.59019E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.67401E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  8.77236E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.30859E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.47960E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.54654E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.29276E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.55394E+07 ;
SR90_ACTIVITY             (idx, 1)        =  6.89534E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.27524E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.32817E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.39470E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.87569E+12 ;
CS137_ACTIVITY            (idx, 1)        =  8.10406E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.22544E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.45157E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.98225E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.25130E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.56668E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 8 ;
BURNUP                     (idx, [1:  2])  = [  6.00000E+00  6.00054E+00 ];
BURN_DAYS                 (idx, 1)        =  1.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.86049E-01 0.00102 ];
U235_FISS                 (idx, [1:   4]) = [  9.24663E+15 0.00078  6.71158E-01 0.00051 ];
U238_FISS                 (idx, [1:   4]) = [  9.87540E+14 0.00278  7.16700E-02 0.00261 ];
PU239_FISS                (idx, [1:   4]) = [  3.36441E+15 0.00145  2.44199E-01 0.00130 ];
PU240_FISS                (idx, [1:   4]) = [  2.31804E+12 0.05420  1.68252E-04 0.05421 ];
PU241_FISS                (idx, [1:   4]) = [  1.70597E+14 0.00673  1.23826E-02 0.00670 ];
U235_CAPT                 (idx, [1:   4]) = [  2.00833E+15 0.00188  9.29336E-02 0.00183 ];
U238_CAPT                 (idx, [1:   4]) = [  8.99894E+15 0.00093  4.16392E-01 0.00065 ];
PU239_CAPT                (idx, [1:   4]) = [  1.86399E+15 0.00193  8.62519E-02 0.00184 ];
PU240_CAPT                (idx, [1:   4]) = [  7.39689E+14 0.00296  3.42265E-02 0.00288 ];
PU241_CAPT                (idx, [1:   4]) = [  6.10958E+13 0.01005  2.82876E-03 0.01015 ];
XE135_CAPT                (idx, [1:   4]) = [  7.55396E+14 0.00299  3.49565E-02 0.00297 ];
SM149_CAPT                (idx, [1:   4]) = [  1.80267E+14 0.00591  8.34139E-03 0.00587 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000657 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.79130E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000657 5.00779E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3025211 3.02961E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1928743 1.93147E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46703 4.67126E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000657 5.00779E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -2.70084E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.54531E+16 2.0E-05  3.54531E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37636E+16 3.5E-06  1.37636E+16 3.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.16024E+16 0.00043  1.55332E+16 0.00044  6.06923E+15 0.00102 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.53661E+16 0.00026  2.92968E+16 0.00023  6.06923E+15 0.00102 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.56668E+16 0.00050  3.56668E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.71674E+18 0.00047  4.54717E+17 0.00046  1.26202E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.33240E+14 0.00470 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.56993E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.30862E+18 0.00059 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11816E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11816E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.58944E+00 0.00046 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.67882E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.68150E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23168E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93938E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96699E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.00435E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.94969E-01 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.57586E+00 2.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04092E+02 3.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.94946E-01 0.00057  9.89126E-01 0.00055  5.84317E-03 0.00888 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.94680E-01 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  9.94132E-01 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.94680E-01 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.00407E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73293E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73261E+01 8.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.97717E-07 0.00370 ];
IMP_EALF                  (idx, [1:   2]) = [  5.97938E-07 0.00151 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.42146E-01 0.00286 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.43232E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.05319E-03 0.00564  1.72975E-04 0.03479  1.02999E-03 0.01421  9.38077E-04 0.01426  2.68740E-03 0.00864  9.32687E-04 0.01490  2.92055E-04 0.02708 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.02158E-01 0.01368  1.01705E-02 0.02140  3.12767E-02 0.00039  1.10032E-01 0.00204  3.21728E-01 0.00026  1.33483E+00 0.00071  8.42751E+00 0.01145 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.97635E-03 0.00844  1.74285E-04 0.05291  1.00305E-03 0.02198  9.37183E-04 0.02012  2.66198E-03 0.01272  9.10903E-04 0.02307  2.88948E-04 0.04086 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.99246E-01 0.02078  1.24946E-02 0.00023  3.12714E-02 0.00055  1.10332E-01 0.00057  3.21665E-01 0.00038  1.33554E+00 0.00090  8.92589E+00 0.00444 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.83858E-05 0.00130  2.83748E-05 0.00130  3.03739E-05 0.01485 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.82373E-05 0.00114  2.82263E-05 0.00114  3.02110E-05 0.01481 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.86172E-03 0.00896  1.61899E-04 0.05873  1.01364E-03 0.02292  8.94326E-04 0.02288  2.59978E-03 0.01363  9.15630E-04 0.02490  2.76446E-04 0.04362 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.91665E-01 0.02279  1.24945E-02 0.00039  3.12709E-02 0.00066  1.10283E-01 0.00070  3.21657E-01 0.00047  1.33550E+00 0.00113  8.93990E+00 0.00562 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.83083E-05 0.00283  2.82912E-05 0.00282  2.98019E-05 0.03534 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.81608E-05 0.00278  2.81438E-05 0.00277  2.96487E-05 0.03541 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.66395E-03 0.03221  1.54439E-04 0.18052  9.98079E-04 0.07594  9.52939E-04 0.07533  2.47123E-03 0.05013  8.29820E-04 0.07394  2.57447E-04 0.13220 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.72454E-01 0.07267  1.24894E-02 4.1E-05  3.12343E-02 0.00170  1.10534E-01 0.00163  3.22112E-01 0.00136  1.33185E+00 0.00348  8.79027E+00 0.01360 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.63363E-03 0.03159  1.65598E-04 0.17249  9.84484E-04 0.07616  9.49775E-04 0.07411  2.45685E-03 0.04850  8.37853E-04 0.07317  2.39076E-04 0.12896 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.57568E-01 0.07097  1.24894E-02 4.1E-05  3.12399E-02 0.00168  1.10554E-01 0.00164  3.22093E-01 0.00135  1.33171E+00 0.00352  8.79902E+00 0.01367 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.01175E+02 0.03271 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.83232E-05 0.00079 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.81752E-05 0.00051 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.77667E-03 0.00514 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.03948E+02 0.00506 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.22916E-07 0.00062 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.79502E-06 0.00041  2.79500E-06 0.00042  2.80024E-06 0.00515 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.18026E-05 0.00077  4.18180E-05 0.00077  3.93301E-05 0.01000 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.65605E-01 0.00032  6.65504E-01 0.00032  6.93234E-01 0.00894 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08620E+01 0.01456 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.66385E+01 0.00044  3.49872E+01 0.00049 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.61286E+04 0.00378  2.98866E+05 0.00146  6.08762E+05 0.00089  6.51095E+05 0.00091  5.97624E+05 0.00073  6.40124E+05 0.00075  4.34620E+05 0.00072  3.84725E+05 0.00082  2.94263E+05 0.00061  2.40206E+05 0.00090  2.07669E+05 0.00081  1.86972E+05 0.00075  1.72588E+05 0.00071  1.63921E+05 0.00104  1.60042E+05 0.00082  1.38111E+05 0.00111  1.36556E+05 0.00083  1.35018E+05 0.00093  1.32980E+05 0.00112  2.59681E+05 0.00058  2.51279E+05 0.00076  1.81328E+05 0.00084  1.17442E+05 0.00116  1.35884E+05 0.00100  1.28984E+05 0.00092  1.16352E+05 0.00075  1.90463E+05 0.00097  4.37745E+04 0.00165  5.49055E+04 0.00137  4.97465E+04 0.00171  2.89836E+04 0.00149  5.02250E+04 0.00151  3.40332E+04 0.00185  2.87697E+04 0.00190  5.26419E+03 0.00339  4.97958E+03 0.00319  4.66818E+03 0.00337  4.54116E+03 0.00326  4.63347E+03 0.00401  4.90335E+03 0.00306  5.41622E+03 0.00311  5.19025E+03 0.00349  9.94950E+03 0.00311  1.60805E+04 0.00248  2.07324E+04 0.00164  5.49144E+04 0.00163  5.77307E+04 0.00126  6.25402E+04 0.00135  4.16683E+04 0.00120  3.03220E+04 0.00151  2.29063E+04 0.00100  2.71261E+04 0.00127  5.29356E+04 0.00130  7.42607E+04 0.00122  1.46698E+05 0.00101  2.30518E+05 0.00106  3.48917E+05 0.00112  2.24298E+05 0.00107  1.61250E+05 0.00114  1.16099E+05 0.00138  1.04579E+05 0.00141  1.03008E+05 0.00117  8.61553E+04 0.00137  5.83725E+04 0.00149  5.39986E+04 0.00117  4.80802E+04 0.00139  4.06786E+04 0.00165  3.20639E+04 0.00166  2.14844E+04 0.00228  7.64193E+03 0.00214 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.00351E+00 0.00045 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.33276E+18 0.00040  3.84012E+17 0.00105 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38461E-01 0.00011  1.53904E+00 0.00040 ];
INF_CAPT                  (idx, [1:   4]) = [  6.92915E-03 0.00071  3.22093E-02 0.00049 ];
INF_ABS                   (idx, [1:   4]) = [  8.83792E-03 0.00059  6.14314E-02 0.00075 ];
INF_FISS                  (idx, [1:   4]) = [  1.90876E-03 0.00055  2.92221E-02 0.00105 ];
INF_NSF                   (idx, [1:   4]) = [  5.00993E-03 0.00056  7.49483E-02 0.00107 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.62470E+00 5.8E-05  2.56478E+00 4.5E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04640E+02 6.7E-06  2.03968E+02 7.6E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.81714E-08 0.00037  2.58303E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29621E-01 0.00011  1.47756E+00 0.00044 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44764E-01 0.00021  3.90418E-01 0.00045 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65480E-02 0.00027  9.22863E-02 0.00081 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35834E-03 0.00269  2.77216E-02 0.00255 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03781E-02 0.00194 -8.74034E-03 0.00808 ];
INF_SCATT5                (idx, [1:   4]) = [  1.53238E-04 0.11066  6.58641E-03 0.00732 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10066E-03 0.00311 -1.67932E-02 0.00260 ];
INF_SCATT7                (idx, [1:   4]) = [  7.56438E-04 0.01556  5.03034E-04 0.09066 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29663E-01 0.00011  1.47756E+00 0.00044 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44764E-01 0.00022  3.90418E-01 0.00045 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65480E-02 0.00027  9.22863E-02 0.00081 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35826E-03 0.00268  2.77216E-02 0.00255 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03781E-02 0.00194 -8.74034E-03 0.00808 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.53239E-04 0.11048  6.58641E-03 0.00732 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10083E-03 0.00311 -1.67932E-02 0.00260 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.56346E-04 0.01557  5.03034E-04 0.09066 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12568E-01 0.00030  9.97303E-01 0.00037 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56813E+00 0.00030  3.34236E-01 0.00037 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.79621E-03 0.00060  6.14314E-02 0.00075 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69576E-02 0.00028  6.25771E-02 0.00090 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11503E-01 0.00011  1.81180E-02 0.00038  1.09835E-03 0.00442  1.47647E+00 0.00045 ];
INF_S1                    (idx, [1:   8]) = [  2.39479E-01 0.00022  5.28448E-03 0.00080  4.72633E-04 0.00620  3.89946E-01 0.00045 ];
INF_S2                    (idx, [1:   8]) = [  9.81366E-02 0.00027 -1.58862E-03 0.00271  2.58562E-04 0.00736  9.20278E-02 0.00081 ];
INF_S3                    (idx, [1:   8]) = [  9.23078E-03 0.00218 -1.87244E-03 0.00160  9.38741E-05 0.01870  2.76277E-02 0.00255 ];
INF_S4                    (idx, [1:   8]) = [ -9.76612E-03 0.00195 -6.11934E-04 0.00341  1.04427E-06 1.00000 -8.74139E-03 0.00806 ];
INF_S5                    (idx, [1:   8]) = [  1.26651E-04 0.12885  2.65872E-05 0.09418 -3.70656E-05 0.03689  6.62348E-03 0.00731 ];
INF_S6                    (idx, [1:   8]) = [  5.24162E-03 0.00291 -1.40959E-04 0.01530 -4.82280E-05 0.02512 -1.67450E-02 0.00260 ];
INF_S7                    (idx, [1:   8]) = [  9.27852E-04 0.01253 -1.71414E-04 0.01097 -4.22773E-05 0.02454  5.45311E-04 0.08308 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11545E-01 0.00011  1.81180E-02 0.00038  1.09835E-03 0.00442  1.47647E+00 0.00045 ];
INF_SP1                   (idx, [1:   8]) = [  2.39480E-01 0.00022  5.28448E-03 0.00080  4.72633E-04 0.00620  3.89946E-01 0.00045 ];
INF_SP2                   (idx, [1:   8]) = [  9.81367E-02 0.00027 -1.58862E-03 0.00271  2.58562E-04 0.00736  9.20278E-02 0.00081 ];
INF_SP3                   (idx, [1:   8]) = [  9.23071E-03 0.00217 -1.87244E-03 0.00160  9.38741E-05 0.01870  2.76277E-02 0.00255 ];
INF_SP4                   (idx, [1:   8]) = [ -9.76615E-03 0.00195 -6.11934E-04 0.00341  1.04427E-06 1.00000 -8.74139E-03 0.00806 ];
INF_SP5                   (idx, [1:   8]) = [  1.26652E-04 0.12863  2.65872E-05 0.09418 -3.70656E-05 0.03689  6.62348E-03 0.00731 ];
INF_SP6                   (idx, [1:   8]) = [  5.24179E-03 0.00291 -1.40959E-04 0.01530 -4.82280E-05 0.02512 -1.67450E-02 0.00260 ];
INF_SP7                   (idx, [1:   8]) = [  9.27759E-04 0.01254 -1.71414E-04 0.01097 -4.22773E-05 0.02454  5.45311E-04 0.08308 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32225E-01 0.00047  1.16363E+00 0.00575 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34125E-01 0.00071  1.24136E+00 0.00829 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33962E-01 0.00085  1.25832E+00 0.00758 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28680E-01 0.00099  1.02376E+00 0.00537 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43540E+00 0.00047  2.86686E-01 0.00573 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42376E+00 0.00071  2.68955E-01 0.00809 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42476E+00 0.00085  2.65279E-01 0.00780 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45768E+00 0.00099  3.25824E-01 0.00537 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.97635E-03 0.00844  1.74285E-04 0.05291  1.00305E-03 0.02198  9.37183E-04 0.02012  2.66198E-03 0.01272  9.10903E-04 0.02307  2.88948E-04 0.04086 ];
LAMBDA                    (idx, [1:  14]) = [  7.99246E-01 0.02078  1.24946E-02 0.00023  3.12714E-02 0.00055  1.10332E-01 0.00057  3.21665E-01 0.00038  1.33554E+00 0.00090  8.92589E+00 0.00444 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0000' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:31 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:41:39 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284831 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00316E+00  9.96720E-01  1.00707E+00  1.00272E+00  9.90333E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 6.6E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12075E-02 0.00117  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88792E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04762E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05179E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67992E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.65989E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.65894E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.85251E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.26523E-01 0.00120  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000776 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00016E+04 0.00081 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00016E+04 0.00081 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.03618E+02 ;
RUNNING_TIME              (idx, 1)        =  6.11300E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.22850E-01  4.22850E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.71250E-01  9.35000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.00000E+01  3.48848E+00  2.70557E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.30483E-01  2.78000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.43500E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  6.11299E+01  1.30025E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96676 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99880E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79185E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.42436E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.79008E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.08779E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.82426E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.27353E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.60008E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.66270E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  9.80896E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.36632E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.22142E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.64147E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.58755E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.60217E+07 ;
SR90_ACTIVITY             (idx, 1)        =  7.92428E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.29625E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.35803E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.42348E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.93516E+12 ;
CS137_ACTIVITY            (idx, 1)        =  9.45323E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.24731E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.43005E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.91978E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.25847E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.61440E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 9 ;
BURNUP                     (idx, [1:  2])  = [  7.00000E+00  7.00063E+00 ];
BURN_DAYS                 (idx, 1)        =  1.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.95296E-01 0.00110 ];
U235_FISS                 (idx, [1:   4]) = [  8.82972E+15 0.00080  6.42554E-01 0.00056 ];
U238_FISS                 (idx, [1:   4]) = [  9.98968E+14 0.00269  7.26896E-02 0.00256 ];
PU239_FISS                (idx, [1:   4]) = [  3.67357E+15 0.00142  2.67322E-01 0.00124 ];
PU240_FISS                (idx, [1:   4]) = [  2.66403E+12 0.05570  1.93848E-04 0.05570 ];
PU241_FISS                (idx, [1:   4]) = [  2.29807E+14 0.00563  1.67248E-02 0.00563 ];
U235_CAPT                 (idx, [1:   4]) = [  1.93189E+15 0.00195  8.73615E-02 0.00189 ];
U238_CAPT                 (idx, [1:   4]) = [  9.05137E+15 0.00106  4.09276E-01 0.00076 ];
PU239_CAPT                (idx, [1:   4]) = [  2.02725E+15 0.00192  9.16763E-02 0.00189 ];
PU240_CAPT                (idx, [1:   4]) = [  8.77479E+14 0.00298  3.96783E-02 0.00291 ];
PU241_CAPT                (idx, [1:   4]) = [  8.31306E+13 0.00903  3.75912E-03 0.00901 ];
XE135_CAPT                (idx, [1:   4]) = [  7.58211E+14 0.00305  3.42879E-02 0.00303 ];
SM149_CAPT                (idx, [1:   4]) = [  1.84236E+14 0.00628  8.33101E-03 0.00625 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000776 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.79387E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000776 5.00779E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3054861 3.05923E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1898439 1.90107E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 47476 4.74921E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000776 5.00779E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.68107E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.55961E+16 2.0E-05  3.55961E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37520E+16 3.7E-06  1.37520E+16 3.7E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.21127E+16 0.00046  1.59732E+16 0.00048  6.13951E+15 0.00113 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.58647E+16 0.00029  2.97252E+16 0.00026  6.13951E+15 0.00113 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.61440E+16 0.00050  3.61440E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.73663E+18 0.00047  4.59021E+17 0.00045  1.27760E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.43354E+14 0.00476 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.62080E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.32439E+18 0.00063 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11700E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11700E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.58014E+00 0.00048 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.67420E-01 0.00035 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.65674E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23096E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93804E-01 3.7E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96677E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.93591E-01 0.00058 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.84153E-01 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.58843E+00 2.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04265E+02 3.7E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.84171E-01 0.00060  9.78455E-01 0.00059  5.69885E-03 0.00930 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.84661E-01 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  9.84961E-01 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.84661E-01 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  9.94101E-01 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73056E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73019E+01 8.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.11865E-07 0.00354 ];
IMP_EALF                  (idx, [1:   2]) = [  6.12577E-07 0.00151 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.46413E-01 0.00288 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.46461E-01 0.00117 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.02747E-03 0.00607  1.59854E-04 0.03586  1.03175E-03 0.01357  9.26081E-04 0.01500  2.68430E-03 0.00909  9.34730E-04 0.01530  2.90747E-04 0.02638 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.04849E-01 0.01380  1.00223E-02 0.02224  3.12227E-02 0.00041  1.10066E-01 0.00204  3.21774E-01 0.00028  1.33119E+00 0.00096  8.47468E+00 0.01155 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.91080E-03 0.00857  1.56476E-04 0.05524  1.01473E-03 0.02112  8.90981E-04 0.02275  2.66083E-03 0.01347  8.85288E-04 0.02277  3.02498E-04 0.03840 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.19201E-01 0.02062  1.24958E-02 0.00029  3.12132E-02 0.00060  1.10296E-01 0.00057  3.21811E-01 0.00042  1.33147E+00 0.00129  8.95013E+00 0.00460 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.84479E-05 0.00136  2.84313E-05 0.00137  3.13225E-05 0.01522 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.79921E-05 0.00119  2.79757E-05 0.00119  3.08325E-05 0.01527 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.77788E-03 0.00939  1.48975E-04 0.05968  9.88407E-04 0.02290  8.94875E-04 0.02501  2.58428E-03 0.01459  8.68739E-04 0.02441  2.92599E-04 0.04088 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.21051E-01 0.02229  1.24949E-02 0.00040  3.12263E-02 0.00071  1.10263E-01 0.00069  3.21825E-01 0.00049  1.32996E+00 0.00171  8.94891E+00 0.00617 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.82890E-05 0.00303  2.82793E-05 0.00305  2.93578E-05 0.03189 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.78374E-05 0.00300  2.78277E-05 0.00301  2.88957E-05 0.03183 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.99088E-03 0.03219  1.42918E-04 0.21232  1.04764E-03 0.08000  9.90354E-04 0.08232  2.61163E-03 0.04663  8.70358E-04 0.07686  3.27976E-04 0.14209 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.89479E-01 0.06955  1.25173E-02 0.00216  3.12736E-02 0.00169  1.10040E-01 0.00160  3.21565E-01 0.00129  1.32702E+00 0.00406  8.98845E+00 0.01253 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.00620E-03 0.03121  1.51241E-04 0.20187  1.04988E-03 0.07680  9.99542E-04 0.07855  2.60438E-03 0.04612  8.79230E-04 0.07427  3.21926E-04 0.13747 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.89166E-01 0.06942  1.25173E-02 0.00216  3.12645E-02 0.00169  1.10044E-01 0.00159  3.21506E-01 0.00127  1.32728E+00 0.00407  8.99783E+00 0.01209 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.12649E+02 0.03222 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.83785E-05 0.00085 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.79240E-05 0.00057 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.88339E-03 0.00510 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.07430E+02 0.00523 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.20740E-07 0.00070 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.78023E-06 0.00041  2.78000E-06 0.00042  2.81806E-06 0.00550 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.17525E-05 0.00086  4.17731E-05 0.00087  3.83948E-05 0.00942 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.63068E-01 0.00033  6.63030E-01 0.00034  6.81995E-01 0.00946 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05696E+01 0.01465 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.65894E+01 0.00049  3.48547E+01 0.00049 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.65334E+04 0.00334  2.99567E+05 0.00158  6.08457E+05 0.00072  6.50211E+05 0.00085  5.97146E+05 0.00075  6.40279E+05 0.00086  4.35137E+05 0.00071  3.84439E+05 0.00059  2.94340E+05 0.00072  2.40565E+05 0.00067  2.07337E+05 0.00086  1.86915E+05 0.00067  1.72190E+05 0.00064  1.64011E+05 0.00097  1.59713E+05 0.00087  1.38101E+05 0.00082  1.36450E+05 0.00099  1.35330E+05 0.00078  1.32945E+05 0.00086  2.59398E+05 0.00086  2.50955E+05 0.00076  1.81274E+05 0.00093  1.17529E+05 0.00085  1.35810E+05 0.00073  1.28910E+05 0.00073  1.16151E+05 0.00093  1.90541E+05 0.00059  4.37687E+04 0.00168  5.49199E+04 0.00133  4.97295E+04 0.00182  2.90221E+04 0.00172  5.02938E+04 0.00130  3.39546E+04 0.00155  2.86066E+04 0.00164  5.22811E+03 0.00416  4.84560E+03 0.00279  4.48006E+03 0.00378  4.35925E+03 0.00341  4.46468E+03 0.00361  4.77363E+03 0.00346  5.25410E+03 0.00328  5.11406E+03 0.00368  9.91109E+03 0.00253  1.60345E+04 0.00260  2.04322E+04 0.00219  5.44528E+04 0.00145  5.73955E+04 0.00151  6.19493E+04 0.00156  4.13092E+04 0.00095  2.99734E+04 0.00183  2.25937E+04 0.00140  2.67803E+04 0.00177  5.23219E+04 0.00127  7.34569E+04 0.00140  1.45408E+05 0.00162  2.28971E+05 0.00149  3.46594E+05 0.00142  2.23077E+05 0.00142  1.60424E+05 0.00151  1.15593E+05 0.00153  1.04033E+05 0.00171  1.02657E+05 0.00153  8.58651E+04 0.00155  5.81291E+04 0.00156  5.38264E+04 0.00178  4.78658E+04 0.00141  4.05492E+04 0.00184  3.19772E+04 0.00169  2.14235E+04 0.00165  7.61148E+03 0.00213 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.94406E-01 0.00047 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.34996E+18 0.00053  3.86701E+17 0.00122 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38646E-01 0.00010  1.54450E+00 0.00036 ];
INF_CAPT                  (idx, [1:   4]) = [  7.02901E-03 0.00062  3.26479E-02 0.00059 ];
INF_ABS                   (idx, [1:   4]) = [  8.91093E-03 0.00045  6.16458E-02 0.00089 ];
INF_FISS                  (idx, [1:   4]) = [  1.88193E-03 0.00055  2.89979E-02 0.00124 ];
INF_NSF                   (idx, [1:   4]) = [  4.95346E-03 0.00055  7.47720E-02 0.00126 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.63213E+00 5.9E-05  2.57853E+00 4.0E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04738E+02 6.9E-06  2.04158E+02 6.9E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.79825E-08 0.00037  2.58543E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29738E-01 0.00011  1.48287E+00 0.00042 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44871E-01 0.00015  3.91819E-01 0.00053 ];
INF_SCATT2                (idx, [1:   4]) = [  9.66010E-02 0.00030  9.25490E-02 0.00102 ];
INF_SCATT3                (idx, [1:   4]) = [  7.36505E-03 0.00223  2.77922E-02 0.00269 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03707E-02 0.00197 -8.80979E-03 0.00666 ];
INF_SCATT5                (idx, [1:   4]) = [  1.34277E-04 0.10986  6.61414E-03 0.00815 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10603E-03 0.00334 -1.70010E-02 0.00314 ];
INF_SCATT7                (idx, [1:   4]) = [  7.52647E-04 0.01961  4.46150E-04 0.10537 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29780E-01 0.00011  1.48287E+00 0.00042 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44871E-01 0.00015  3.91819E-01 0.00053 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.66012E-02 0.00030  9.25490E-02 0.00102 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.36512E-03 0.00222  2.77922E-02 0.00269 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03705E-02 0.00197 -8.80979E-03 0.00666 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.34334E-04 0.10995  6.61414E-03 0.00815 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10591E-03 0.00335 -1.70010E-02 0.00314 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.52757E-04 0.01958  4.46150E-04 0.10537 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12516E-01 0.00033  1.00106E+00 0.00031 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56851E+00 0.00033  3.32980E-01 0.00031 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.86920E-03 0.00045  6.16458E-02 0.00089 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69641E-02 0.00018  6.27263E-02 0.00115 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11682E-01 0.00010  1.80561E-02 0.00039  1.09636E-03 0.00351  1.48178E+00 0.00042 ];
INF_S1                    (idx, [1:   8]) = [  2.39610E-01 0.00016  5.26107E-03 0.00083  4.74668E-04 0.00559  3.91345E-01 0.00053 ];
INF_S2                    (idx, [1:   8]) = [  9.81923E-02 0.00029 -1.59132E-03 0.00221  2.55747E-04 0.00978  9.22933E-02 0.00102 ];
INF_S3                    (idx, [1:   8]) = [  9.22986E-03 0.00176 -1.86482E-03 0.00205  9.21947E-05 0.02071  2.77000E-02 0.00273 ];
INF_S4                    (idx, [1:   8]) = [ -9.76437E-03 0.00209 -6.06334E-04 0.00456 -6.96998E-07 1.00000 -8.80909E-03 0.00661 ];
INF_S5                    (idx, [1:   8]) = [  1.02679E-04 0.14639  3.15987E-05 0.08054 -3.80544E-05 0.03704  6.65219E-03 0.00811 ];
INF_S6                    (idx, [1:   8]) = [  5.24668E-03 0.00322 -1.40647E-04 0.01541 -4.71051E-05 0.03206 -1.69539E-02 0.00316 ];
INF_S7                    (idx, [1:   8]) = [  9.30417E-04 0.01626 -1.77770E-04 0.01348 -4.21727E-05 0.03663  4.88322E-04 0.09666 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11724E-01 0.00010  1.80561E-02 0.00039  1.09636E-03 0.00351  1.48178E+00 0.00042 ];
INF_SP1                   (idx, [1:   8]) = [  2.39610E-01 0.00016  5.26107E-03 0.00083  4.74668E-04 0.00559  3.91345E-01 0.00053 ];
INF_SP2                   (idx, [1:   8]) = [  9.81926E-02 0.00029 -1.59132E-03 0.00221  2.55747E-04 0.00978  9.22933E-02 0.00102 ];
INF_SP3                   (idx, [1:   8]) = [  9.22994E-03 0.00176 -1.86482E-03 0.00205  9.21947E-05 0.02071  2.77000E-02 0.00273 ];
INF_SP4                   (idx, [1:   8]) = [ -9.76420E-03 0.00209 -6.06334E-04 0.00456 -6.96998E-07 1.00000 -8.80909E-03 0.00661 ];
INF_SP5                   (idx, [1:   8]) = [  1.02736E-04 0.14650  3.15987E-05 0.08054 -3.80544E-05 0.03704  6.65219E-03 0.00811 ];
INF_SP6                   (idx, [1:   8]) = [  5.24655E-03 0.00323 -1.40647E-04 0.01541 -4.71051E-05 0.03206 -1.69539E-02 0.00316 ];
INF_SP7                   (idx, [1:   8]) = [  9.30527E-04 0.01623 -1.77770E-04 0.01348 -4.21727E-05 0.03663  4.88322E-04 0.09666 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32047E-01 0.00056  1.16006E+00 0.00554 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34190E-01 0.00089  1.25223E+00 0.00712 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33584E-01 0.00092  1.24731E+00 0.00601 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28460E-01 0.00079  1.01529E+00 0.00685 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43650E+00 0.00056  2.87558E-01 0.00563 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42337E+00 0.00089  2.66518E-01 0.00715 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42707E+00 0.00092  2.67473E-01 0.00603 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45906E+00 0.00079  3.28682E-01 0.00684 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.91080E-03 0.00857  1.56476E-04 0.05524  1.01473E-03 0.02112  8.90981E-04 0.02275  2.66083E-03 0.01347  8.85288E-04 0.02277  3.02498E-04 0.03840 ];
LAMBDA                    (idx, [1:  14]) = [  8.19201E-01 0.02062  1.24958E-02 0.00029  3.12132E-02 0.00060  1.10296E-01 0.00057  3.21811E-01 0.00042  1.33147E+00 0.00129  8.95013E+00 0.00460 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0000' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:31 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:47:55 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284831 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00318E+00  9.99121E-01  1.00474E+00  1.00317E+00  9.89781E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.1E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12094E-02 0.00108  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88791E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05173E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05590E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67803E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.64722E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.64627E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.79883E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.24551E-01 0.00109  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001281 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00026E+04 0.00088 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00026E+04 0.00088 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.34871E+02 ;
RUNNING_TIME              (idx, 1)        =  6.73911E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.22850E-01  4.22850E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.90850E-01  9.60000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.61836E+01  3.47832E+00  2.70522E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.86117E-01  2.76333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.68500E-02  8.49998E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  6.73909E+01  1.30073E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96907 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99808E+00 0.00057 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79735E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.44892E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.78136E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.96275E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.84113E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.28532E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.60776E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.65280E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.08885E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.41675E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.03867E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.73198E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.84978E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.64356E+07 ;
SR90_ACTIVITY             (idx, 1)        =  8.92719E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.31549E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.38482E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.44961E+14 ;
CS134_ACTIVITY            (idx, 1)        =  5.15574E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.08016E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.26672E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.41073E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  4.21614E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.26489E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.66406E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 10 ;
BURNUP                     (idx, [1:  2])  = [  8.00000E+00  8.00072E+00 ];
BURN_DAYS                 (idx, 1)        =  2.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.04735E-01 0.00104 ];
U235_FISS                 (idx, [1:   4]) = [  8.46522E+15 0.00082  6.15683E-01 0.00061 ];
U238_FISS                 (idx, [1:   4]) = [  1.02014E+15 0.00269  7.41862E-02 0.00253 ];
PU239_FISS                (idx, [1:   4]) = [  3.96180E+15 0.00135  2.88134E-01 0.00117 ];
PU240_FISS                (idx, [1:   4]) = [  3.41690E+12 0.04882  2.48543E-04 0.04889 ];
PU241_FISS                (idx, [1:   4]) = [  2.91688E+14 0.00539  2.12107E-02 0.00528 ];
U235_CAPT                 (idx, [1:   4]) = [  1.84969E+15 0.00194  8.18579E-02 0.00185 ];
U238_CAPT                 (idx, [1:   4]) = [  9.13093E+15 0.00103  4.04065E-01 0.00071 ];
PU239_CAPT                (idx, [1:   4]) = [  2.18431E+15 0.00179  9.66729E-02 0.00178 ];
PU240_CAPT                (idx, [1:   4]) = [  1.00818E+15 0.00274  4.46167E-02 0.00268 ];
PU241_CAPT                (idx, [1:   4]) = [  1.07370E+14 0.00803  4.75322E-03 0.00810 ];
XE135_CAPT                (idx, [1:   4]) = [  7.53467E+14 0.00312  3.33462E-02 0.00310 ];
SM149_CAPT                (idx, [1:   4]) = [  1.90311E+14 0.00622  8.42303E-03 0.00623 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001281 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.67878E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001281 5.00768E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3079626 3.08356E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1873920 1.87637E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 47735 4.77486E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001281 5.00768E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.23986E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.57283E+16 2.1E-05  3.57283E+16 2.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37411E+16 3.9E-06  1.37411E+16 3.9E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.26038E+16 0.00047  1.64165E+16 0.00047  6.18734E+15 0.00107 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.63449E+16 0.00029  3.01576E+16 0.00026  6.18734E+15 0.00107 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.66406E+16 0.00051  3.66406E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.75572E+18 0.00049  4.63676E+17 0.00048  1.29205E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.49960E+14 0.00456 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.66949E+16 0.00030 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.33788E+18 0.00063 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11584E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11584E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.57285E+00 0.00052 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.67716E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.62485E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23167E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93831E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96598E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.85216E-01 0.00058 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.75807E-01 0.00058 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.60010E+00 2.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04427E+02 3.9E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.75559E-01 0.00060  9.70134E-01 0.00059  5.67380E-03 0.00933 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.75218E-01 0.00030 ];
COL_KEFF                  (idx, [1:   2]) = [  9.75228E-01 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.75218E-01 0.00030 ];
ABS_KINF                  (idx, [1:   2]) = [  9.84620E-01 0.00029 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72714E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72761E+01 8.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.33221E-07 0.00362 ];
IMP_EALF                  (idx, [1:   2]) = [  6.28558E-07 0.00151 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.51128E-01 0.00283 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.50093E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.97776E-03 0.00602  1.65553E-04 0.03618  1.00520E-03 0.01466  9.27590E-04 0.01508  2.69864E-03 0.00866  9.02725E-04 0.01406  2.78051E-04 0.02845 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.82977E-01 0.01493  9.72897E-03 0.02392  3.11957E-02 0.00042  1.10296E-01 0.00042  3.22013E-01 0.00029  1.32816E+00 0.00099  8.18278E+00 0.01395 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.74811E-03 0.00845  1.57020E-04 0.05489  9.68526E-04 0.02090  8.98562E-04 0.02234  2.56653E-03 0.01279  8.68357E-04 0.02180  2.89116E-04 0.04035 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.07722E-01 0.02083  1.25065E-02 0.00048  3.12010E-02 0.00059  1.10316E-01 0.00060  3.22174E-01 0.00041  1.32593E+00 0.00155  8.86850E+00 0.00608 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.84282E-05 0.00128  2.84162E-05 0.00128  3.06017E-05 0.01494 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.77286E-05 0.00114  2.77169E-05 0.00114  2.98517E-05 0.01495 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.79536E-03 0.00942  1.59104E-04 0.06117  9.69588E-04 0.02304  8.86452E-04 0.02427  2.61704E-03 0.01409  8.80308E-04 0.02357  2.82873E-04 0.04668 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.07032E-01 0.02528  1.24979E-02 0.00049  3.11862E-02 0.00074  1.10301E-01 0.00073  3.22071E-01 0.00047  1.32889E+00 0.00172  8.88429E+00 0.00714 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.81847E-05 0.00315  2.81771E-05 0.00315  2.88734E-05 0.03473 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.74897E-05 0.00306  2.74823E-05 0.00307  2.81633E-05 0.03470 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.60167E-03 0.03098  1.23120E-04 0.21973  9.20325E-04 0.07545  8.82964E-04 0.08245  2.59021E-03 0.04639  8.34860E-04 0.08112  2.50194E-04 0.15766 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.99505E-01 0.07758  1.24888E-02 5.1E-05  3.11815E-02 0.00173  1.10253E-01 0.00173  3.22124E-01 0.00132  1.32752E+00 0.00397  8.87696E+00 0.01835 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.62561E-03 0.03016  1.16763E-04 0.23271  9.18025E-04 0.07400  8.71073E-04 0.08070  2.63947E-03 0.04448  8.19990E-04 0.07936  2.60285E-04 0.15502 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.17822E-01 0.07702  1.24888E-02 5.1E-05  3.11826E-02 0.00173  1.10249E-01 0.00172  3.22053E-01 0.00130  1.32711E+00 0.00403  8.88274E+00 0.01836 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.00263E+02 0.03129 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.82989E-05 0.00085 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.76025E-05 0.00062 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.84381E-03 0.00633 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.06608E+02 0.00641 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.17639E-07 0.00066 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.76790E-06 0.00043  2.76781E-06 0.00043  2.78522E-06 0.00560 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.16111E-05 0.00082  4.16279E-05 0.00082  3.88171E-05 0.01058 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.59954E-01 0.00032  6.59967E-01 0.00033  6.69736E-01 0.00934 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04712E+01 0.01497 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.64627E+01 0.00045  3.46784E+01 0.00049 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.65572E+04 0.00355  2.99854E+05 0.00113  6.07912E+05 0.00085  6.50416E+05 0.00067  5.97802E+05 0.00064  6.39649E+05 0.00067  4.34518E+05 0.00068  3.84051E+05 0.00064  2.93686E+05 0.00081  2.40051E+05 0.00085  2.06866E+05 0.00088  1.86887E+05 0.00083  1.72524E+05 0.00072  1.63829E+05 0.00086  1.59394E+05 0.00085  1.37860E+05 0.00096  1.36348E+05 0.00089  1.35050E+05 0.00096  1.32757E+05 0.00104  2.59441E+05 0.00065  2.50765E+05 0.00079  1.80936E+05 0.00071  1.17458E+05 0.00090  1.35942E+05 0.00072  1.28769E+05 0.00101  1.16154E+05 0.00102  1.89935E+05 0.00069  4.37132E+04 0.00155  5.46626E+04 0.00092  4.97404E+04 0.00156  2.89919E+04 0.00188  5.02488E+04 0.00148  3.38509E+04 0.00134  2.83151E+04 0.00213  5.17114E+03 0.00368  4.73320E+03 0.00301  4.35524E+03 0.00380  4.20530E+03 0.00363  4.25951E+03 0.00371  4.61148E+03 0.00393  5.20724E+03 0.00298  5.08961E+03 0.00368  9.80246E+03 0.00327  1.58186E+04 0.00155  2.03464E+04 0.00164  5.39582E+04 0.00147  5.69425E+04 0.00117  6.13851E+04 0.00084  4.08161E+04 0.00137  2.94737E+04 0.00122  2.21689E+04 0.00159  2.62885E+04 0.00159  5.14931E+04 0.00132  7.24891E+04 0.00142  1.43867E+05 0.00145  2.26734E+05 0.00121  3.43808E+05 0.00126  2.21529E+05 0.00130  1.59277E+05 0.00154  1.14794E+05 0.00162  1.03398E+05 0.00161  1.01885E+05 0.00158  8.52167E+04 0.00185  5.77238E+04 0.00178  5.34246E+04 0.00177  4.75619E+04 0.00157  4.02829E+04 0.00164  3.17400E+04 0.00184  2.13068E+04 0.00213  7.57382E+03 0.00206 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.84631E-01 0.00047 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.36715E+18 0.00052  3.88605E+17 0.00114 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38707E-01 0.00012  1.54761E+00 0.00037 ];
INF_CAPT                  (idx, [1:   4]) = [  7.13352E-03 0.00077  3.30730E-02 0.00054 ];
INF_ABS                   (idx, [1:   4]) = [  8.99431E-03 0.00061  6.18920E-02 0.00081 ];
INF_FISS                  (idx, [1:   4]) = [  1.86078E-03 0.00044  2.88190E-02 0.00113 ];
INF_NSF                   (idx, [1:   4]) = [  4.91113E-03 0.00045  7.46758E-02 0.00116 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.63928E+00 7.5E-05  2.59120E+00 5.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04834E+02 8.5E-06  2.04334E+02 8.7E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.77659E-08 0.00039  2.58743E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29706E-01 0.00013  1.48574E+00 0.00043 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44883E-01 0.00022  3.92677E-01 0.00045 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65792E-02 0.00030  9.26959E-02 0.00086 ];
INF_SCATT3                (idx, [1:   4]) = [  7.33139E-03 0.00300  2.77640E-02 0.00274 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03788E-02 0.00194 -8.86695E-03 0.00733 ];
INF_SCATT5                (idx, [1:   4]) = [  1.35029E-04 0.14587  6.62307E-03 0.00624 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10058E-03 0.00228 -1.69204E-02 0.00255 ];
INF_SCATT7                (idx, [1:   4]) = [  7.44449E-04 0.02079  4.92904E-04 0.08834 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29747E-01 0.00012  1.48574E+00 0.00043 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44884E-01 0.00022  3.92677E-01 0.00045 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65793E-02 0.00030  9.26959E-02 0.00086 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.33114E-03 0.00300  2.77640E-02 0.00274 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03791E-02 0.00194 -8.86695E-03 0.00733 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.35187E-04 0.14581  6.62307E-03 0.00624 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10046E-03 0.00229 -1.69204E-02 0.00255 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.44335E-04 0.02077  4.92904E-04 0.08834 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12421E-01 0.00032  1.00364E+00 0.00037 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56921E+00 0.00032  3.32126E-01 0.00037 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.95315E-03 0.00061  6.18920E-02 0.00081 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69875E-02 0.00018  6.29629E-02 0.00104 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11719E-01 0.00012  1.79871E-02 0.00039  1.09387E-03 0.00403  1.48464E+00 0.00043 ];
INF_S1                    (idx, [1:   8]) = [  2.39649E-01 0.00022  5.23413E-03 0.00080  4.72000E-04 0.00619  3.92205E-01 0.00046 ];
INF_S2                    (idx, [1:   8]) = [  9.81655E-02 0.00028 -1.58632E-03 0.00235  2.60468E-04 0.01053  9.24355E-02 0.00087 ];
INF_S3                    (idx, [1:   8]) = [  9.18906E-03 0.00238 -1.85767E-03 0.00124  9.23887E-05 0.01998  2.76717E-02 0.00276 ];
INF_S4                    (idx, [1:   8]) = [ -9.77921E-03 0.00203 -5.99635E-04 0.00531  1.74098E-06 0.85206 -8.86869E-03 0.00734 ];
INF_S5                    (idx, [1:   8]) = [  1.08144E-04 0.17940  2.68851E-05 0.07076 -3.56883E-05 0.04344  6.65875E-03 0.00621 ];
INF_S6                    (idx, [1:   8]) = [  5.24373E-03 0.00227 -1.43153E-04 0.01712 -4.88211E-05 0.01769 -1.68716E-02 0.00255 ];
INF_S7                    (idx, [1:   8]) = [  9.17330E-04 0.01689 -1.72881E-04 0.01339 -4.37856E-05 0.02982  5.36690E-04 0.08082 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11760E-01 0.00012  1.79871E-02 0.00039  1.09387E-03 0.00403  1.48464E+00 0.00043 ];
INF_SP1                   (idx, [1:   8]) = [  2.39650E-01 0.00022  5.23413E-03 0.00080  4.72000E-04 0.00619  3.92205E-01 0.00046 ];
INF_SP2                   (idx, [1:   8]) = [  9.81656E-02 0.00028 -1.58632E-03 0.00235  2.60468E-04 0.01053  9.24355E-02 0.00087 ];
INF_SP3                   (idx, [1:   8]) = [  9.18881E-03 0.00237 -1.85767E-03 0.00124  9.23887E-05 0.01998  2.76717E-02 0.00276 ];
INF_SP4                   (idx, [1:   8]) = [ -9.77946E-03 0.00203 -5.99635E-04 0.00531  1.74098E-06 0.85206 -8.86869E-03 0.00734 ];
INF_SP5                   (idx, [1:   8]) = [  1.08302E-04 0.17925  2.68851E-05 0.07076 -3.56883E-05 0.04344  6.65875E-03 0.00621 ];
INF_SP6                   (idx, [1:   8]) = [  5.24361E-03 0.00227 -1.43153E-04 0.01712 -4.88211E-05 0.01769 -1.68716E-02 0.00255 ];
INF_SP7                   (idx, [1:   8]) = [  9.17216E-04 0.01688 -1.72881E-04 0.01339 -4.37856E-05 0.02982  5.36690E-04 0.08082 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32275E-01 0.00043  1.16269E+00 0.00720 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33924E-01 0.00090  1.25535E+00 0.00985 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33948E-01 0.00099  1.25004E+00 0.00820 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29033E-01 0.00079  1.01804E+00 0.00777 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43508E+00 0.00043  2.87048E-01 0.00719 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42499E+00 0.00090  2.66158E-01 0.01000 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42485E+00 0.00099  2.67091E-01 0.00825 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45541E+00 0.00079  3.27895E-01 0.00769 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.74811E-03 0.00845  1.57020E-04 0.05489  9.68526E-04 0.02090  8.98562E-04 0.02234  2.56653E-03 0.01279  8.68357E-04 0.02180  2.89116E-04 0.04035 ];
LAMBDA                    (idx, [1:  14]) = [  8.07722E-01 0.02083  1.25065E-02 0.00048  3.12010E-02 0.00059  1.10316E-01 0.00060  3.22174E-01 0.00041  1.32593E+00 0.00155  8.86850E+00 0.00608 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0000' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:31 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:54:10 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284831 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00221E+00  9.98688E-01  1.00631E+00  1.00257E+00  9.90220E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12195E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88781E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05217E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05635E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67631E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.63573E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.63479E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.76857E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.23399E-01 0.00115  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000930 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00019E+04 0.00086 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00019E+04 0.00086 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.66135E+02 ;
RUNNING_TIME              (idx, 1)        =  7.36505E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.22850E-01  4.22850E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.10650E-01  9.86667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.23660E+01  3.46528E+00  2.71715E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  6.42833E-01  2.77167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.87167E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.36501E+01  1.30086E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97124 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99975E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80235E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.47404E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.77434E+04 ;
TOT_SF_RATE               (idx, 1)        =  4.20608E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.85942E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.29813E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61459E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.64450E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.20165E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.46274E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.92971E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.82925E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.08679E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.67982E+07 ;
SR90_ACTIVITY             (idx, 1)        =  9.90625E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.33284E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.40856E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.47301E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.52059E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.21489E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.28761E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.39419E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.93407E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.27142E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.71212E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 11 ;
BURNUP                     (idx, [1:  2])  = [  9.00000E+00  9.00081E+00 ];
BURN_DAYS                 (idx, 1)        =  2.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.16367E-01 0.00111 ];
U235_FISS                 (idx, [1:   4]) = [  8.09824E+15 0.00090  5.90112E-01 0.00065 ];
U238_FISS                 (idx, [1:   4]) = [  1.02841E+15 0.00283  7.49283E-02 0.00265 ];
PU239_FISS                (idx, [1:   4]) = [  4.21720E+15 0.00134  3.07301E-01 0.00116 ];
PU240_FISS                (idx, [1:   4]) = [  4.12941E+12 0.04285  3.00838E-04 0.04280 ];
PU241_FISS                (idx, [1:   4]) = [  3.67550E+14 0.00449  2.67848E-02 0.00447 ];
U235_CAPT                 (idx, [1:   4]) = [  1.77483E+15 0.00215  7.68198E-02 0.00212 ];
U238_CAPT                 (idx, [1:   4]) = [  9.22697E+15 0.00104  3.99327E-01 0.00074 ];
PU239_CAPT                (idx, [1:   4]) = [  2.32827E+15 0.00183  1.00773E-01 0.00178 ];
PU240_CAPT                (idx, [1:   4]) = [  1.14270E+15 0.00261  4.94543E-02 0.00251 ];
PU241_CAPT                (idx, [1:   4]) = [  1.30386E+14 0.00758  5.64232E-03 0.00752 ];
XE135_CAPT                (idx, [1:   4]) = [  7.58970E+14 0.00318  3.28516E-02 0.00319 ];
SM149_CAPT                (idx, [1:   4]) = [  1.97758E+14 0.00591  8.55966E-03 0.00591 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000930 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.91688E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000930 5.00792E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3107716 3.11213E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1845961 1.84852E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 47253 4.72628E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000930 5.00792E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.09782E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.58578E+16 2.0E-05  3.58578E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37303E+16 3.8E-06  1.37303E+16 3.8E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.31042E+16 0.00045  1.68807E+16 0.00046  6.22353E+15 0.00109 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.68345E+16 0.00028  3.06110E+16 0.00025  6.22353E+15 0.00109 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.71212E+16 0.00049  3.71212E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.77630E+18 0.00046  4.69471E+17 0.00044  1.30682E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.50932E+14 0.00486 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.71855E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.35122E+18 0.00061 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11468E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11468E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.56079E+00 0.00052 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.68590E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.60100E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23099E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93834E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96693E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.74707E-01 0.00058 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.65495E-01 0.00058 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.61158E+00 2.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04588E+02 3.8E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.65448E-01 0.00059  9.60050E-01 0.00058  5.44453E-03 0.00947 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.65837E-01 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  9.66081E-01 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.65837E-01 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  9.75050E-01 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72538E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72514E+01 8.4E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.44805E-07 0.00388 ];
IMP_EALF                  (idx, [1:   2]) = [  6.44279E-07 0.00145 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.52654E-01 0.00290 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.53646E-01 0.00107 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.94394E-03 0.00591  1.48900E-04 0.03685  1.00895E-03 0.01481  9.53674E-04 0.01464  2.63671E-03 0.00903  9.04928E-04 0.01528  2.90771E-04 0.02831 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.03447E-01 0.01467  9.46079E-03 0.02544  3.11420E-02 0.00043  1.10409E-01 0.00044  3.22290E-01 0.00031  1.32607E+00 0.00107  8.36000E+00 0.01236 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.72207E-03 0.00961  1.35547E-04 0.05830  9.86946E-04 0.02147  9.17569E-04 0.02307  2.54195E-03 0.01415  8.56585E-04 0.02403  2.83482E-04 0.04082 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.09683E-01 0.02190  1.25164E-02 0.00065  3.11424E-02 0.00062  1.10408E-01 0.00062  3.22258E-01 0.00044  1.32753E+00 0.00143  8.91857E+00 0.00543 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.85761E-05 0.00134  2.85634E-05 0.00134  3.08672E-05 0.01518 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.75838E-05 0.00119  2.75715E-05 0.00119  2.97990E-05 0.01519 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.61797E-03 0.00962  1.33567E-04 0.06118  9.46271E-04 0.02407  9.26749E-04 0.02545  2.44493E-03 0.01473  8.90659E-04 0.02434  2.75793E-04 0.04354 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.19035E-01 0.02377  1.25191E-02 0.00098  3.11660E-02 0.00076  1.10389E-01 0.00079  3.22109E-01 0.00050  1.33013E+00 0.00165  8.85756E+00 0.00798 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.83252E-05 0.00304  2.83157E-05 0.00306  2.93194E-05 0.04212 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.73420E-05 0.00299  2.73326E-05 0.00300  2.83308E-05 0.04238 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.55932E-03 0.03400  1.12231E-04 0.22744  8.43034E-04 0.08206  9.62448E-04 0.08276  2.50834E-03 0.04921  8.07138E-04 0.08463  3.26127E-04 0.13481 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.60573E-01 0.07629  1.24888E-02 5.9E-05  3.11702E-02 0.00181  1.10220E-01 0.00170  3.22524E-01 0.00145  1.32549E+00 0.00442  9.00918E+00 0.01697 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.57919E-03 0.03360  1.11964E-04 0.21025  8.65803E-04 0.08095  9.38919E-04 0.08256  2.51819E-03 0.04769  8.15413E-04 0.08689  3.28905E-04 0.13642 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.58681E-01 0.07620  1.24887E-02 5.9E-05  3.11693E-02 0.00181  1.10237E-01 0.00170  3.22494E-01 0.00143  1.32535E+00 0.00437  9.01428E+00 0.01698 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.96137E+02 0.03389 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.84822E-05 0.00078 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.74933E-05 0.00051 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.59435E-03 0.00631 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.96488E+02 0.00638 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.13498E-07 0.00068 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.75656E-06 0.00043  2.75650E-06 0.00043  2.76543E-06 0.00582 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.14069E-05 0.00085  4.14248E-05 0.00085  3.84375E-05 0.01023 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.57543E-01 0.00032  6.57569E-01 0.00033  6.65584E-01 0.00980 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02550E+01 0.01353 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.63479E+01 0.00048  3.46039E+01 0.00053 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.66874E+04 0.00318  3.00134E+05 0.00169  6.09457E+05 0.00085  6.50741E+05 0.00066  5.97451E+05 0.00092  6.40605E+05 0.00075  4.34604E+05 0.00067  3.84106E+05 0.00094  2.94405E+05 0.00071  2.40371E+05 0.00088  2.07247E+05 0.00100  1.86591E+05 0.00079  1.72426E+05 0.00084  1.63983E+05 0.00099  1.59835E+05 0.00061  1.37909E+05 0.00089  1.36402E+05 0.00103  1.35520E+05 0.00071  1.32965E+05 0.00097  2.59734E+05 0.00075  2.51094E+05 0.00079  1.81281E+05 0.00073  1.17503E+05 0.00093  1.36127E+05 0.00069  1.29211E+05 0.00094  1.16055E+05 0.00096  1.90170E+05 0.00073  4.37906E+04 0.00152  5.46971E+04 0.00165  4.96466E+04 0.00126  2.88270E+04 0.00218  5.00296E+04 0.00140  3.39150E+04 0.00187  2.83024E+04 0.00206  5.10934E+03 0.00320  4.65774E+03 0.00340  4.22700E+03 0.00312  4.05856E+03 0.00306  4.15213E+03 0.00273  4.50544E+03 0.00321  5.10708E+03 0.00351  4.99186E+03 0.00415  9.69747E+03 0.00245  1.57073E+04 0.00322  2.02137E+04 0.00258  5.38192E+04 0.00165  5.67588E+04 0.00143  6.10401E+04 0.00153  4.04615E+04 0.00144  2.90859E+04 0.00161  2.19125E+04 0.00170  2.58910E+04 0.00142  5.08719E+04 0.00123  7.16203E+04 0.00137  1.42339E+05 0.00137  2.24529E+05 0.00132  3.40604E+05 0.00137  2.19484E+05 0.00154  1.57945E+05 0.00143  1.13870E+05 0.00172  1.02544E+05 0.00172  1.01185E+05 0.00175  8.45469E+04 0.00188  5.73454E+04 0.00185  5.29692E+04 0.00158  4.72533E+04 0.00184  3.99553E+04 0.00193  3.15551E+04 0.00213  2.11012E+04 0.00214  7.51972E+03 0.00258 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.75300E-01 0.00051 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.38612E+18 0.00056  3.90214E+17 0.00118 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38772E-01 0.00012  1.54900E+00 0.00038 ];
INF_CAPT                  (idx, [1:   4]) = [  7.23631E-03 0.00062  3.35074E-02 0.00053 ];
INF_ABS                   (idx, [1:   4]) = [  9.07165E-03 0.00052  6.21795E-02 0.00083 ];
INF_FISS                  (idx, [1:   4]) = [  1.83533E-03 0.00041  2.86721E-02 0.00119 ];
INF_NSF                   (idx, [1:   4]) = [  4.85723E-03 0.00040  7.46519E-02 0.00121 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.64651E+00 4.5E-05  2.60364E+00 3.5E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04936E+02 5.6E-06  2.04509E+02 6.0E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.75950E-08 0.00042  2.58829E-06 0.00020 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29713E-01 0.00012  1.48679E+00 0.00044 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44834E-01 0.00018  3.93098E-01 0.00062 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65681E-02 0.00029  9.27288E-02 0.00091 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34556E-03 0.00311  2.78008E-02 0.00224 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03648E-02 0.00200 -8.85098E-03 0.00665 ];
INF_SCATT5                (idx, [1:   4]) = [  1.54054E-04 0.10773  6.62161E-03 0.00530 ];
INF_SCATT6                (idx, [1:   4]) = [  5.12052E-03 0.00249 -1.69557E-02 0.00208 ];
INF_SCATT7                (idx, [1:   4]) = [  7.74859E-04 0.02223  4.25537E-04 0.11669 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29755E-01 0.00012  1.48679E+00 0.00044 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44835E-01 0.00018  3.93098E-01 0.00062 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65683E-02 0.00029  9.27288E-02 0.00091 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34530E-03 0.00311  2.78008E-02 0.00224 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03650E-02 0.00200 -8.85098E-03 0.00665 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.54071E-04 0.10809  6.62161E-03 0.00530 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.12072E-03 0.00248 -1.69557E-02 0.00208 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.74708E-04 0.02222  4.25537E-04 0.11669 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12458E-01 0.00032  1.00471E+00 0.00031 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56894E+00 0.00032  3.31770E-01 0.00031 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.02924E-03 0.00050  6.21795E-02 0.00083 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69676E-02 0.00026  6.33036E-02 0.00114 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11805E-01 0.00012  1.79085E-02 0.00037  1.09984E-03 0.00382  1.48569E+00 0.00044 ];
INF_S1                    (idx, [1:   8]) = [  2.39623E-01 0.00018  5.21101E-03 0.00094  4.73594E-04 0.00602  3.92625E-01 0.00063 ];
INF_S2                    (idx, [1:   8]) = [  9.81548E-02 0.00028 -1.58668E-03 0.00257  2.58880E-04 0.00851  9.24699E-02 0.00092 ];
INF_S3                    (idx, [1:   8]) = [  9.19454E-03 0.00236 -1.84898E-03 0.00204  9.54572E-05 0.01765  2.77053E-02 0.00223 ];
INF_S4                    (idx, [1:   8]) = [ -9.76922E-03 0.00203 -5.95557E-04 0.00492  3.00200E-06 0.59739 -8.85398E-03 0.00659 ];
INF_S5                    (idx, [1:   8]) = [  1.25974E-04 0.13340  2.80800E-05 0.09385 -3.37362E-05 0.04065  6.65535E-03 0.00520 ];
INF_S6                    (idx, [1:   8]) = [  5.25962E-03 0.00245 -1.39099E-04 0.01326 -4.58483E-05 0.01696 -1.69098E-02 0.00208 ];
INF_S7                    (idx, [1:   8]) = [  9.45133E-04 0.01765 -1.70274E-04 0.01371 -4.35769E-05 0.02606  4.69113E-04 0.10576 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11847E-01 0.00012  1.79085E-02 0.00037  1.09984E-03 0.00382  1.48569E+00 0.00044 ];
INF_SP1                   (idx, [1:   8]) = [  2.39624E-01 0.00018  5.21101E-03 0.00094  4.73594E-04 0.00602  3.92625E-01 0.00063 ];
INF_SP2                   (idx, [1:   8]) = [  9.81549E-02 0.00028 -1.58668E-03 0.00257  2.58880E-04 0.00851  9.24699E-02 0.00092 ];
INF_SP3                   (idx, [1:   8]) = [  9.19428E-03 0.00236 -1.84898E-03 0.00204  9.54572E-05 0.01765  2.77053E-02 0.00223 ];
INF_SP4                   (idx, [1:   8]) = [ -9.76942E-03 0.00203 -5.95557E-04 0.00492  3.00200E-06 0.59739 -8.85398E-03 0.00659 ];
INF_SP5                   (idx, [1:   8]) = [  1.25991E-04 0.13378  2.80800E-05 0.09385 -3.37362E-05 0.04065  6.65535E-03 0.00520 ];
INF_SP6                   (idx, [1:   8]) = [  5.25982E-03 0.00245 -1.39099E-04 0.01326 -4.58483E-05 0.01696 -1.69098E-02 0.00208 ];
INF_SP7                   (idx, [1:   8]) = [  9.44982E-04 0.01764 -1.70274E-04 0.01371 -4.35769E-05 0.02606  4.69113E-04 0.10576 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32217E-01 0.00069  1.17730E+00 0.00772 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34009E-01 0.00088  1.26994E+00 0.00838 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33656E-01 0.00125  1.26923E+00 0.00894 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29064E-01 0.00097  1.02870E+00 0.00828 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43545E+00 0.00069  2.83534E-01 0.00762 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42448E+00 0.00088  2.62910E-01 0.00816 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42665E+00 0.00125  2.63130E-01 0.00893 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45523E+00 0.00097  3.24562E-01 0.00819 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.72207E-03 0.00961  1.35547E-04 0.05830  9.86946E-04 0.02147  9.17569E-04 0.02307  2.54195E-03 0.01415  8.56585E-04 0.02403  2.83482E-04 0.04082 ];
LAMBDA                    (idx, [1:  14]) = [  8.09683E-01 0.02190  1.25164E-02 0.00065  3.11424E-02 0.00062  1.10408E-01 0.00062  3.22258E-01 0.00044  1.32753E+00 0.00143  8.91857E+00 0.00543 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0000' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:31 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:00:24 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284831 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00203E+00  9.98084E-01  1.00680E+00  1.00263E+00  9.90444E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12131E-02 0.00107  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88787E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05506E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05925E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67537E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.62526E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.62432E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.72714E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.21543E-01 0.00107  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000986 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00020E+04 0.00085 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00020E+04 0.00085 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.97292E+02 ;
RUNNING_TIME              (idx, 1)        =  7.98889E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.22850E-01  4.22850E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.30950E-01  1.01333E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.85253E+01  3.44358E+00  2.71568E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  7.01033E-01  2.89833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.05833E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.98888E+01  1.29962E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97305 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99975E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80649E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.49870E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.76754E+04 ;
TOT_SF_RATE               (idx, 1)        =  5.91592E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.87969E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.31238E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61898E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.63627E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.32002E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.50557E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  6.89678E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.93535E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.30339E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.71204E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.08618E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.34931E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.43083E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.49542E+14 ;
CS134_ACTIVITY            (idx, 1)        =  8.02824E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.34951E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.30910E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.37803E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  8.14833E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.27813E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.76448E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 12 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E+01  1.00009E+01 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.27741E-01 0.00105 ];
U235_FISS                 (idx, [1:   4]) = [  7.76176E+15 0.00092  5.66342E-01 0.00066 ];
U238_FISS                 (idx, [1:   4]) = [  1.04283E+15 0.00274  7.60753E-02 0.00251 ];
PU239_FISS                (idx, [1:   4]) = [  4.45213E+15 0.00126  3.24859E-01 0.00112 ];
PU240_FISS                (idx, [1:   4]) = [  4.60399E+12 0.04110  3.35981E-04 0.04116 ];
PU241_FISS                (idx, [1:   4]) = [  4.34860E+14 0.00432  3.17304E-02 0.00428 ];
U235_CAPT                 (idx, [1:   4]) = [  1.71136E+15 0.00211  7.23868E-02 0.00197 ];
U238_CAPT                 (idx, [1:   4]) = [  9.33156E+15 0.00102  3.94702E-01 0.00070 ];
PU239_CAPT                (idx, [1:   4]) = [  2.46354E+15 0.00174  1.04213E-01 0.00171 ];
PU240_CAPT                (idx, [1:   4]) = [  1.27048E+15 0.00257  5.37380E-02 0.00246 ];
PU241_CAPT                (idx, [1:   4]) = [  1.58487E+14 0.00670  6.70492E-03 0.00672 ];
XE135_CAPT                (idx, [1:   4]) = [  7.62538E+14 0.00306  3.22570E-02 0.00304 ];
SM149_CAPT                (idx, [1:   4]) = [  1.98439E+14 0.00620  8.39427E-03 0.00619 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000986 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.81184E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000986 5.00781E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3135603 3.14002E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1818004 1.82041E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 47379 4.73827E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000986 5.00781E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.12227E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.59789E+16 2.1E-05  3.59789E+16 2.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37202E+16 4.0E-06  1.37202E+16 4.0E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.36046E+16 0.00045  1.73296E+16 0.00047  6.27509E+15 0.00106 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.73248E+16 0.00028  3.10497E+16 0.00026  6.27509E+15 0.00106 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.76448E+16 0.00051  3.76448E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.79787E+18 0.00047  4.74712E+17 0.00046  1.32316E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.56755E+14 0.00470 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.76816E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.36628E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11352E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11352E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.55210E+00 0.00053 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.68460E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.56994E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23009E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93848E-01 3.7E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96655E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.63852E-01 0.00060 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.54719E-01 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.62233E+00 2.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04738E+02 4.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.54733E-01 0.00062  9.49415E-01 0.00061  5.30353E-03 0.00996 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.56357E-01 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  9.55870E-01 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.56357E-01 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  9.65511E-01 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72323E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72281E+01 8.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.58668E-07 0.00377 ];
IMP_EALF                  (idx, [1:   2]) = [  6.59510E-07 0.00150 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.57609E-01 0.00274 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.57470E-01 0.00110 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.92742E-03 0.00626  1.53269E-04 0.03676  1.01244E-03 0.01507  9.23898E-04 0.01532  2.66248E-03 0.00908  8.76137E-04 0.01563  2.99188E-04 0.02715 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.11431E-01 0.01495  9.57016E-03 0.02489  3.10835E-02 0.00044  1.10395E-01 0.00045  3.22246E-01 0.00030  1.31852E+00 0.00130  8.35798E+00 0.01206 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.53070E-03 0.00916  1.50292E-04 0.05716  9.24311E-04 0.02304  8.66277E-04 0.02338  2.49987E-03 0.01359  8.17090E-04 0.02416  2.72862E-04 0.03972 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.04948E-01 0.02223  1.25217E-02 0.00071  3.10882E-02 0.00063  1.10415E-01 0.00066  3.22044E-01 0.00043  1.31913E+00 0.00168  8.82777E+00 0.00649 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.87663E-05 0.00140  2.87539E-05 0.00140  3.10704E-05 0.01488 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.74589E-05 0.00125  2.74471E-05 0.00126  2.96578E-05 0.01489 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.55645E-03 0.01001  1.55530E-04 0.06071  9.38791E-04 0.02529  8.79456E-04 0.02612  2.48458E-03 0.01509  8.12169E-04 0.02554  2.85922E-04 0.04375 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.22517E-01 0.02442  1.25206E-02 0.00098  3.10778E-02 0.00080  1.10294E-01 0.00077  3.22474E-01 0.00054  1.32001E+00 0.00207  8.87450E+00 0.00760 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.85027E-05 0.00320  2.84962E-05 0.00322  2.80551E-05 0.04113 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.72079E-05 0.00316  2.72015E-05 0.00317  2.67986E-05 0.04088 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.64023E-03 0.03362  1.16079E-04 0.22140  9.34799E-04 0.08499  7.78863E-04 0.08594  2.57641E-03 0.05066  9.19572E-04 0.08273  3.14507E-04 0.14011 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  9.17721E-01 0.07557  1.25441E-02 0.00308  3.10388E-02 0.00191  1.10417E-01 0.00181  3.21705E-01 0.00137  1.31935E+00 0.00488  8.92627E+00 0.01702 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.68671E-03 0.03336  1.19249E-04 0.21086  9.41925E-04 0.08251  7.91881E-04 0.08515  2.60062E-03 0.05000  9.07416E-04 0.08092  3.25619E-04 0.13396 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  9.38932E-01 0.07442  1.25400E-02 0.00289  3.10395E-02 0.00190  1.10443E-01 0.00181  3.21580E-01 0.00134  1.31939E+00 0.00482  8.92737E+00 0.01725 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.99052E+02 0.03378 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.86370E-05 0.00086 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.73356E-05 0.00062 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.66838E-03 0.00652 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.98050E+02 0.00664 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.10310E-07 0.00069 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.74782E-06 0.00043  2.74773E-06 0.00043  2.76662E-06 0.00578 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.12955E-05 0.00083  4.13147E-05 0.00083  3.79304E-05 0.01011 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.54492E-01 0.00034  6.54596E-01 0.00035  6.50170E-01 0.00976 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04366E+01 0.01355 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.62432E+01 0.00046  3.45181E+01 0.00051 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.72941E+04 0.00338  3.01193E+05 0.00164  6.08880E+05 0.00090  6.50582E+05 0.00071  5.97771E+05 0.00071  6.39861E+05 0.00051  4.34960E+05 0.00062  3.84455E+05 0.00086  2.94479E+05 0.00085  2.40122E+05 0.00067  2.07184E+05 0.00098  1.86791E+05 0.00091  1.72380E+05 0.00098  1.63965E+05 0.00107  1.59931E+05 0.00073  1.38354E+05 0.00105  1.36161E+05 0.00078  1.35383E+05 0.00081  1.32918E+05 0.00075  2.59614E+05 0.00069  2.50842E+05 0.00070  1.81141E+05 0.00087  1.17437E+05 0.00085  1.36039E+05 0.00081  1.29185E+05 0.00098  1.16105E+05 0.00089  1.89285E+05 0.00074  4.35512E+04 0.00156  5.46732E+04 0.00131  4.96599E+04 0.00150  2.89277E+04 0.00209  4.99556E+04 0.00131  3.37228E+04 0.00180  2.80709E+04 0.00223  5.02434E+03 0.00349  4.53424E+03 0.00376  4.10540E+03 0.00323  3.93360E+03 0.00312  4.01217E+03 0.00466  4.38454E+03 0.00384  5.04188E+03 0.00324  4.92312E+03 0.00317  9.62896E+03 0.00268  1.56255E+04 0.00261  2.01027E+04 0.00221  5.35012E+04 0.00130  5.64499E+04 0.00170  6.07693E+04 0.00124  4.01735E+04 0.00133  2.89281E+04 0.00173  2.16413E+04 0.00184  2.56768E+04 0.00168  5.02252E+04 0.00121  7.09694E+04 0.00145  1.40856E+05 0.00110  2.22658E+05 0.00106  3.37910E+05 0.00114  2.17818E+05 0.00126  1.56950E+05 0.00123  1.13064E+05 0.00154  1.01767E+05 0.00128  1.00462E+05 0.00139  8.40192E+04 0.00137  5.69405E+04 0.00147  5.26752E+04 0.00152  4.68205E+04 0.00156  3.96938E+04 0.00127  3.13092E+04 0.00157  2.10611E+04 0.00179  7.45954E+03 0.00240 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.65015E-01 0.00047 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.40522E+18 0.00051  3.92691E+17 0.00110 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38824E-01 0.00013  1.55123E+00 0.00033 ];
INF_CAPT                  (idx, [1:   4]) = [  7.33083E-03 0.00073  3.38804E-02 0.00051 ];
INF_ABS                   (idx, [1:   4]) = [  9.14056E-03 0.00059  6.23484E-02 0.00077 ];
INF_FISS                  (idx, [1:   4]) = [  1.80973E-03 0.00041  2.84680E-02 0.00109 ];
INF_NSF                   (idx, [1:   4]) = [  4.80256E-03 0.00043  7.44492E-02 0.00111 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.65374E+00 6.5E-05  2.61519E+00 4.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05030E+02 6.8E-06  2.04672E+02 7.3E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.73821E-08 0.00045  2.58905E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29680E-01 0.00013  1.48884E+00 0.00038 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44894E-01 0.00023  3.93634E-01 0.00051 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65676E-02 0.00038  9.27754E-02 0.00096 ];
INF_SCATT3                (idx, [1:   4]) = [  7.31896E-03 0.00316  2.78672E-02 0.00189 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03878E-02 0.00204 -8.80111E-03 0.00849 ];
INF_SCATT5                (idx, [1:   4]) = [  1.35533E-04 0.11419  6.60401E-03 0.00868 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10294E-03 0.00300 -1.70062E-02 0.00289 ];
INF_SCATT7                (idx, [1:   4]) = [  7.21940E-04 0.02374  5.26347E-04 0.07535 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29722E-01 0.00013  1.48884E+00 0.00038 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44894E-01 0.00023  3.93634E-01 0.00051 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65676E-02 0.00038  9.27754E-02 0.00096 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.31884E-03 0.00316  2.78672E-02 0.00189 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03879E-02 0.00204 -8.80111E-03 0.00849 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.35661E-04 0.11416  6.60401E-03 0.00868 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10287E-03 0.00300 -1.70062E-02 0.00289 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.21741E-04 0.02378  5.26347E-04 0.07535 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12333E-01 0.00032  1.00634E+00 0.00026 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56987E+00 0.00032  3.31235E-01 0.00026 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.09871E-03 0.00060  6.23484E-02 0.00077 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69722E-02 0.00022  6.34808E-02 0.00088 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11851E-01 0.00013  1.78285E-02 0.00041  1.09453E-03 0.00439  1.48775E+00 0.00038 ];
INF_S1                    (idx, [1:   8]) = [  2.39708E-01 0.00023  5.18520E-03 0.00100  4.69554E-04 0.00698  3.93165E-01 0.00051 ];
INF_S2                    (idx, [1:   8]) = [  9.81509E-02 0.00036 -1.58338E-03 0.00207  2.59284E-04 0.01032  9.25162E-02 0.00095 ];
INF_S3                    (idx, [1:   8]) = [  9.16567E-03 0.00255 -1.84671E-03 0.00136  9.25672E-05 0.02147  2.77747E-02 0.00189 ];
INF_S4                    (idx, [1:   8]) = [ -9.79657E-03 0.00203 -5.91231E-04 0.00594  7.83406E-07 1.00000 -8.80189E-03 0.00852 ];
INF_S5                    (idx, [1:   8]) = [  9.98987E-05 0.15685  3.56346E-05 0.05793 -3.67914E-05 0.02920  6.64080E-03 0.00860 ];
INF_S6                    (idx, [1:   8]) = [  5.24152E-03 0.00299 -1.38584E-04 0.01356 -4.55451E-05 0.02634 -1.69606E-02 0.00293 ];
INF_S7                    (idx, [1:   8]) = [  8.97308E-04 0.01961 -1.75368E-04 0.00947 -4.35344E-05 0.02697  5.69881E-04 0.06989 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11893E-01 0.00013  1.78285E-02 0.00041  1.09453E-03 0.00439  1.48775E+00 0.00038 ];
INF_SP1                   (idx, [1:   8]) = [  2.39709E-01 0.00023  5.18520E-03 0.00100  4.69554E-04 0.00698  3.93165E-01 0.00051 ];
INF_SP2                   (idx, [1:   8]) = [  9.81510E-02 0.00036 -1.58338E-03 0.00207  2.59284E-04 0.01032  9.25162E-02 0.00095 ];
INF_SP3                   (idx, [1:   8]) = [  9.16555E-03 0.00254 -1.84671E-03 0.00136  9.25672E-05 0.02147  2.77747E-02 0.00189 ];
INF_SP4                   (idx, [1:   8]) = [ -9.79672E-03 0.00203 -5.91231E-04 0.00594  7.83406E-07 1.00000 -8.80189E-03 0.00852 ];
INF_SP5                   (idx, [1:   8]) = [  1.00026E-04 0.15678  3.56346E-05 0.05793 -3.67914E-05 0.02920  6.64080E-03 0.00860 ];
INF_SP6                   (idx, [1:   8]) = [  5.24145E-03 0.00299 -1.38584E-04 0.01356 -4.55451E-05 0.02634 -1.69606E-02 0.00293 ];
INF_SP7                   (idx, [1:   8]) = [  8.97108E-04 0.01963 -1.75368E-04 0.00947 -4.35344E-05 0.02697  5.69881E-04 0.06989 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32073E-01 0.00055  1.17950E+00 0.00577 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33976E-01 0.00079  1.28084E+00 0.00806 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33728E-01 0.00106  1.26426E+00 0.00776 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28604E-01 0.00085  1.03044E+00 0.00648 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43634E+00 0.00055  2.82831E-01 0.00576 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42467E+00 0.00079  2.60648E-01 0.00799 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42620E+00 0.00106  2.64035E-01 0.00767 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45815E+00 0.00085  3.23809E-01 0.00638 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.53070E-03 0.00916  1.50292E-04 0.05716  9.24311E-04 0.02304  8.66277E-04 0.02338  2.49987E-03 0.01359  8.17090E-04 0.02416  2.72862E-04 0.03972 ];
LAMBDA                    (idx, [1:  14]) = [  8.04948E-01 0.02223  1.25217E-02 0.00071  3.10882E-02 0.00063  1.10415E-01 0.00066  3.22044E-01 0.00043  1.31913E+00 0.00168  8.82777E+00 0.00649 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0000' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:31 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:06:37 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284831 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00372E+00  9.97825E-01  1.00590E+00  1.00227E+00  9.90284E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13765E-02 0.00113  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88623E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06309E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06729E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67582E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.61444E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.61348E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.65617E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.26760E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001361 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00027E+04 0.00085 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00027E+04 0.00085 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.28327E+02 ;
RUNNING_TIME              (idx, 1)        =  8.61025E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.22850E-01  4.22850E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.51733E-01  1.02000E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  8.46615E+01  3.44362E+00  2.69257E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  7.57117E-01  2.82500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.23500E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  8.61024E+01  1.29594E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97461 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00000E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81006E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.52146E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.74477E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.27878E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.91513E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.33749E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.60630E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.61099E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.64165E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.58855E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  9.64002E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.14431E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.77645E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.77411E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.31587E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.36652E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.46878E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.52499E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.24110E+13 ;
CS137_ACTIVITY            (idx, 1)        =  1.68577E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.32701E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.33807E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.61699E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.28823E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.87879E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 13 ;
BURNUP                     (idx, [1:  2])  = [  1.25000E+01  1.25011E+01 ];
BURN_DAYS                 (idx, 1)        =  3.12500E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.50915E-01 0.00105 ];
U235_FISS                 (idx, [1:   4]) = [  7.03205E+15 0.00098  5.13668E-01 0.00076 ];
U238_FISS                 (idx, [1:   4]) = [  1.07268E+15 0.00289  7.83460E-02 0.00274 ];
PU239_FISS                (idx, [1:   4]) = [  4.93984E+15 0.00123  3.60831E-01 0.00103 ];
PU240_FISS                (idx, [1:   4]) = [  5.82492E+12 0.03750  4.25411E-04 0.03752 ];
PU241_FISS                (idx, [1:   4]) = [  6.28335E+14 0.00328  4.59022E-02 0.00329 ];
U235_CAPT                 (idx, [1:   4]) = [  1.55005E+15 0.00226  6.25390E-02 0.00219 ];
U238_CAPT                 (idx, [1:   4]) = [  9.52261E+15 0.00105  3.84186E-01 0.00077 ];
PU239_CAPT                (idx, [1:   4]) = [  2.73373E+15 0.00171  1.10298E-01 0.00164 ];
PU240_CAPT                (idx, [1:   4]) = [  1.55561E+15 0.00217  6.27627E-02 0.00209 ];
PU241_CAPT                (idx, [1:   4]) = [  2.26811E+14 0.00610  9.15160E-03 0.00609 ];
XE135_CAPT                (idx, [1:   4]) = [  7.68654E+14 0.00315  3.10166E-02 0.00318 ];
SM149_CAPT                (idx, [1:   4]) = [  2.06737E+14 0.00622  8.34064E-03 0.00617 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001361 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.90864E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001361 5.00791E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3190707 3.19501E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1762559 1.76481E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 48095 4.80810E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001361 5.00791E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.33299E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.62455E+16 2.5E-05  3.62455E+16 2.5E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36975E+16 4.9E-06  1.36975E+16 4.9E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.47779E+16 0.00044  1.83433E+16 0.00048  6.43460E+15 0.00119 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.84753E+16 0.00028  3.20407E+16 0.00027  6.43460E+15 0.00119 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.87879E+16 0.00051  3.87879E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.84773E+18 0.00049  4.86976E+17 0.00048  1.36075E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.73022E+14 0.00470 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.88484E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.40357E+18 0.00065 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11062E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11062E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.53083E+00 0.00055 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.68343E-01 0.00035 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.51621E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23051E+00 0.00038 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93742E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96621E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.43027E-01 0.00059 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.33959E-01 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.64615E+00 3.0E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05078E+02 4.9E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.33801E-01 0.00060  9.28918E-01 0.00059  5.04082E-03 0.01005 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.34525E-01 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  9.34576E-01 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.34525E-01 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  9.43598E-01 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71798E+01 0.00023 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71791E+01 9.4E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.94559E-07 0.00403 ];
IMP_EALF                  (idx, [1:   2]) = [  6.92646E-07 0.00162 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.65975E-01 0.00294 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.65546E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.86835E-03 0.00642  1.49688E-04 0.03931  9.97862E-04 0.01401  9.45838E-04 0.01546  2.57941E-03 0.00910  9.26575E-04 0.01563  2.68980E-04 0.02872 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.70082E-01 0.01463  9.40159E-03 0.02586  3.10273E-02 0.00045  1.10481E-01 0.00048  3.22301E-01 0.00032  1.30897E+00 0.00155  8.12895E+00 0.01412 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.42054E-03 0.00921  1.40359E-04 0.06257  9.07741E-04 0.02261  8.80563E-04 0.02370  2.40101E-03 0.01321  8.47668E-04 0.02308  2.43205E-04 0.04356 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.61568E-01 0.02273  1.25369E-02 0.00087  3.10306E-02 0.00063  1.10538E-01 0.00065  3.22243E-01 0.00047  1.31267E+00 0.00188  8.67825E+00 0.00824 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.90249E-05 0.00134  2.90094E-05 0.00134  3.19515E-05 0.01651 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.70989E-05 0.00122  2.70845E-05 0.00122  2.98230E-05 0.01647 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.40706E-03 0.01007  1.36981E-04 0.06844  9.19908E-04 0.02464  8.71457E-04 0.02520  2.39366E-03 0.01514  8.41794E-04 0.02534  2.43256E-04 0.04651 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.65431E-01 0.02462  1.25352E-02 0.00125  3.10397E-02 0.00080  1.10487E-01 0.00083  3.22404E-01 0.00053  1.31079E+00 0.00252  8.69958E+00 0.01118 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.85354E-05 0.00336  2.85213E-05 0.00338  2.86942E-05 0.04122 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.66403E-05 0.00327  2.66270E-05 0.00329  2.68169E-05 0.04141 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.51911E-03 0.03477  1.72291E-04 0.20502  9.79298E-04 0.08408  8.73570E-04 0.08345  2.42929E-03 0.05282  8.56072E-04 0.09051  2.08588E-04 0.16719 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.32441E-01 0.08310  1.24895E-02 4.5E-05  3.10873E-02 0.00186  1.10421E-01 0.00197  3.21855E-01 0.00150  1.30534E+00 0.00650  8.57878E+00 0.02675 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.43262E-03 0.03396  1.68560E-04 0.20013  9.52198E-04 0.08197  8.74131E-04 0.08335  2.37281E-03 0.05227  8.57826E-04 0.08658  2.07097E-04 0.15584 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.44916E-01 0.08087  1.24895E-02 4.5E-05  3.10819E-02 0.00186  1.10424E-01 0.00196  3.21895E-01 0.00150  1.30572E+00 0.00643  8.57744E+00 0.02675 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.95077E+02 0.03479 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.88473E-05 0.00092 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.69325E-05 0.00067 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.39131E-03 0.00676 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.86973E+02 0.00684 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.06428E-07 0.00076 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.72296E-06 0.00042  2.72280E-06 0.00042  2.75552E-06 0.00553 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.12565E-05 0.00090  4.12760E-05 0.00090  3.78111E-05 0.01071 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.49143E-01 0.00036  6.49297E-01 0.00037  6.34298E-01 0.00955 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05141E+01 0.01426 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.61348E+01 0.00053  3.43124E+01 0.00054 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.84679E+04 0.00308  3.01871E+05 0.00147  6.09473E+05 0.00093  6.49864E+05 0.00083  5.97534E+05 0.00087  6.40083E+05 0.00063  4.34909E+05 0.00054  3.84607E+05 0.00068  2.94242E+05 0.00075  2.40288E+05 0.00057  2.07336E+05 0.00087  1.86962E+05 0.00076  1.72550E+05 0.00070  1.63821E+05 0.00080  1.60000E+05 0.00083  1.38203E+05 0.00079  1.36439E+05 0.00092  1.35485E+05 0.00065  1.33027E+05 0.00084  2.59740E+05 0.00066  2.50952E+05 0.00073  1.81281E+05 0.00066  1.17656E+05 0.00101  1.35779E+05 0.00074  1.29123E+05 0.00088  1.15892E+05 0.00104  1.88832E+05 0.00084  4.34877E+04 0.00146  5.45876E+04 0.00183  4.93322E+04 0.00151  2.88333E+04 0.00215  4.97363E+04 0.00140  3.35515E+04 0.00147  2.76680E+04 0.00176  4.91671E+03 0.00417  4.34727E+03 0.00337  3.88872E+03 0.00286  3.68526E+03 0.00457  3.78691E+03 0.00338  4.14921E+03 0.00332  4.82677E+03 0.00384  4.78909E+03 0.00431  9.37061E+03 0.00288  1.54036E+04 0.00193  1.98229E+04 0.00175  5.28695E+04 0.00130  5.58475E+04 0.00121  6.00253E+04 0.00103  3.94623E+04 0.00182  2.82086E+04 0.00147  2.11295E+04 0.00210  2.49848E+04 0.00215  4.90339E+04 0.00165  6.95416E+04 0.00135  1.38965E+05 0.00161  2.19858E+05 0.00159  3.34314E+05 0.00165  2.16014E+05 0.00163  1.55683E+05 0.00170  1.12232E+05 0.00201  1.01049E+05 0.00191  9.98624E+04 0.00165  8.34084E+04 0.00181  5.65259E+04 0.00198  5.22536E+04 0.00174  4.66583E+04 0.00220  3.95396E+04 0.00230  3.11432E+04 0.00214  2.09294E+04 0.00234  7.42425E+03 0.00262 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.43650E-01 0.00061 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.44746E+18 0.00061  4.00308E+17 0.00153 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39007E-01 0.00012  1.55731E+00 0.00052 ];
INF_CAPT                  (idx, [1:   4]) = [  7.53566E-03 0.00064  3.46531E-02 0.00084 ];
INF_ABS                   (idx, [1:   4]) = [  9.29241E-03 0.00052  6.25240E-02 0.00115 ];
INF_FISS                  (idx, [1:   4]) = [  1.75675E-03 0.00039  2.78708E-02 0.00154 ];
INF_NSF                   (idx, [1:   4]) = [  4.69086E-03 0.00040  7.35978E-02 0.00160 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.67019E+00 7.4E-05  2.64067E+00 7.2E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05256E+02 1.0E-05  2.05038E+02 1.3E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.69731E-08 0.00046  2.59334E-06 0.00020 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29719E-01 0.00013  1.49478E+00 0.00059 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44804E-01 0.00026  3.95257E-01 0.00063 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65863E-02 0.00035  9.30666E-02 0.00103 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34088E-03 0.00228  2.79415E-02 0.00262 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03894E-02 0.00162 -8.96411E-03 0.00442 ];
INF_SCATT5                (idx, [1:   4]) = [  1.36643E-04 0.08993  6.66908E-03 0.00666 ];
INF_SCATT6                (idx, [1:   4]) = [  5.09534E-03 0.00355 -1.71249E-02 0.00274 ];
INF_SCATT7                (idx, [1:   4]) = [  7.37850E-04 0.01946  4.88839E-04 0.10434 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29761E-01 0.00013  1.49478E+00 0.00059 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44804E-01 0.00026  3.95257E-01 0.00063 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65865E-02 0.00035  9.30666E-02 0.00103 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34100E-03 0.00229  2.79415E-02 0.00262 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03894E-02 0.00161 -8.96411E-03 0.00442 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.36548E-04 0.08980  6.66908E-03 0.00666 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.09542E-03 0.00355 -1.71249E-02 0.00274 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.37839E-04 0.01949  4.88839E-04 0.10434 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12305E-01 0.00037  1.01079E+00 0.00047 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.57007E+00 0.00037  3.29777E-01 0.00047 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.25002E-03 0.00051  6.25240E-02 0.00115 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69773E-02 0.00022  6.36387E-02 0.00133 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12029E-01 0.00013  1.76891E-02 0.00041  1.10442E-03 0.00479  1.49368E+00 0.00060 ];
INF_S1                    (idx, [1:   8]) = [  2.39669E-01 0.00026  5.13453E-03 0.00070  4.78508E-04 0.00612  3.94779E-01 0.00064 ];
INF_S2                    (idx, [1:   8]) = [  9.81623E-02 0.00034 -1.57601E-03 0.00221  2.58966E-04 0.01083  9.28076E-02 0.00104 ];
INF_S3                    (idx, [1:   8]) = [  9.15924E-03 0.00170 -1.81836E-03 0.00199  9.23727E-05 0.01957  2.78491E-02 0.00261 ];
INF_S4                    (idx, [1:   8]) = [ -9.80941E-03 0.00167 -5.79992E-04 0.00532 -6.66627E-07 1.00000 -8.96344E-03 0.00444 ];
INF_S5                    (idx, [1:   8]) = [  1.07583E-04 0.11951  2.90601E-05 0.07550 -3.71825E-05 0.04210  6.70626E-03 0.00658 ];
INF_S6                    (idx, [1:   8]) = [  5.23723E-03 0.00347 -1.41890E-04 0.01977 -4.91138E-05 0.02478 -1.70758E-02 0.00275 ];
INF_S7                    (idx, [1:   8]) = [  9.11137E-04 0.01543 -1.73287E-04 0.01291 -4.53818E-05 0.02399  5.34221E-04 0.09569 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12072E-01 0.00013  1.76891E-02 0.00041  1.10442E-03 0.00479  1.49368E+00 0.00060 ];
INF_SP1                   (idx, [1:   8]) = [  2.39669E-01 0.00026  5.13453E-03 0.00070  4.78508E-04 0.00612  3.94779E-01 0.00064 ];
INF_SP2                   (idx, [1:   8]) = [  9.81625E-02 0.00034 -1.57601E-03 0.00221  2.58966E-04 0.01083  9.28076E-02 0.00104 ];
INF_SP3                   (idx, [1:   8]) = [  9.15935E-03 0.00170 -1.81836E-03 0.00199  9.23727E-05 0.01957  2.78491E-02 0.00261 ];
INF_SP4                   (idx, [1:   8]) = [ -9.80944E-03 0.00167 -5.79992E-04 0.00532 -6.66627E-07 1.00000 -8.96344E-03 0.00444 ];
INF_SP5                   (idx, [1:   8]) = [  1.07488E-04 0.11937  2.90601E-05 0.07550 -3.71825E-05 0.04210  6.70626E-03 0.00658 ];
INF_SP6                   (idx, [1:   8]) = [  5.23731E-03 0.00347 -1.41890E-04 0.01977 -4.91138E-05 0.02478 -1.70758E-02 0.00275 ];
INF_SP7                   (idx, [1:   8]) = [  9.11126E-04 0.01545 -1.73287E-04 0.01291 -4.53818E-05 0.02399  5.34221E-04 0.09569 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32150E-01 0.00048  1.18893E+00 0.00859 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33861E-01 0.00072  1.28230E+00 0.01038 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33723E-01 0.00089  1.28300E+00 0.01055 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28940E-01 0.00073  1.03884E+00 0.00872 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43586E+00 0.00048  2.80859E-01 0.00856 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42537E+00 0.00073  2.60604E-01 0.01009 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42622E+00 0.00089  2.60510E-01 0.01066 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45600E+00 0.00073  3.21463E-01 0.00883 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.42054E-03 0.00921  1.40359E-04 0.06257  9.07741E-04 0.02261  8.80563E-04 0.02370  2.40101E-03 0.01321  8.47668E-04 0.02308  2.43205E-04 0.04356 ];
LAMBDA                    (idx, [1:  14]) = [  7.61568E-01 0.02273  1.25369E-02 0.00087  3.10306E-02 0.00063  1.10538E-01 0.00065  3.22243E-01 0.00047  1.31267E+00 0.00188  8.67825E+00 0.00824 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0000' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:31 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:12:47 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284831 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00252E+00  9.98118E-01  1.00553E+00  1.00244E+00  9.91390E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.3E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14112E-02 0.00111  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88589E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06542E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06964E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67644E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.60064E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.59968E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.61024E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.26217E-01 0.00113  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000860 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00017E+04 0.00088 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00017E+04 0.00088 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.59113E+02 ;
RUNNING_TIME              (idx, 1)        =  9.22661E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.22850E-01  4.22850E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.72867E-01  1.01333E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.07491E+01  3.40727E+00  2.68037E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  8.11650E-01  2.72833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.41333E-02  8.66663E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.22660E+01  1.29459E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97597 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99950E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81308E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.56799E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.73207E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.45063E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.96155E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.37063E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.60641E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.59497E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.00212E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.67111E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.28345E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.40488E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.18671E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.83062E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.53359E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.38794E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.50242E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.55643E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.76236E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.02112E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.36819E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.30860E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.81636E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.30186E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.99345E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 14 ;
BURNUP                     (idx, [1:  2])  = [  1.50000E+01  1.50014E+01 ];
BURN_DAYS                 (idx, 1)        =  3.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.75844E-01 0.00111 ];
U235_FISS                 (idx, [1:   4]) = [  6.38640E+15 0.00102  4.66557E-01 0.00081 ];
U238_FISS                 (idx, [1:   4]) = [  1.10509E+15 0.00287  8.07113E-02 0.00261 ];
PU239_FISS                (idx, [1:   4]) = [  5.35180E+15 0.00113  3.90963E-01 0.00089 ];
PU240_FISS                (idx, [1:   4]) = [  8.05081E+12 0.03221  5.87723E-04 0.03219 ];
PU241_FISS                (idx, [1:   4]) = [  8.23967E+14 0.00331  6.01928E-02 0.00323 ];
U235_CAPT                 (idx, [1:   4]) = [  1.41144E+15 0.00239  5.44471E-02 0.00234 ];
U238_CAPT                 (idx, [1:   4]) = [  9.74854E+15 0.00104  3.76026E-01 0.00074 ];
PU239_CAPT                (idx, [1:   4]) = [  2.94705E+15 0.00167  1.13687E-01 0.00164 ];
PU240_CAPT                (idx, [1:   4]) = [  1.82711E+15 0.00215  7.04763E-02 0.00203 ];
PU241_CAPT                (idx, [1:   4]) = [  2.96427E+14 0.00511  1.14350E-02 0.00509 ];
XE135_CAPT                (idx, [1:   4]) = [  7.73723E+14 0.00323  2.98494E-02 0.00325 ];
SM149_CAPT                (idx, [1:   4]) = [  2.15948E+14 0.00605  8.33106E-03 0.00606 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000860 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.92677E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000860 5.00793E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3241109 3.24584E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1711648 1.71398E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 48103 4.81102E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000860 5.00793E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.93601E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.64843E+16 2.5E-05  3.64843E+16 2.5E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36768E+16 5.1E-06  1.36768E+16 5.1E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.59207E+16 0.00046  1.93411E+16 0.00048  6.57955E+15 0.00117 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.95974E+16 0.00030  3.30179E+16 0.00028  6.57955E+15 0.00117 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.99345E+16 0.00051  3.99345E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.89688E+18 0.00050  4.99450E+17 0.00046  1.39743E+18 0.00056 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.84273E+14 0.00459 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.99817E+16 0.00031 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.43961E+18 0.00066 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10772E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10772E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.51164E+00 0.00057 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.68355E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.46198E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23012E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93746E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96611E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.23197E-01 0.00064 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.14313E-01 0.00064 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.66762E+00 3.0E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05388E+02 5.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.14442E-01 0.00064  9.09544E-01 0.00064  4.76932E-03 0.01022 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.14040E-01 0.00031 ];
COL_KEFF                  (idx, [1:   2]) = [  9.13724E-01 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.14040E-01 0.00031 ];
ABS_KINF                  (idx, [1:   2]) = [  9.22923E-01 0.00031 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71336E+01 0.00023 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71327E+01 9.3E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.27147E-07 0.00388 ];
IMP_EALF                  (idx, [1:   2]) = [  7.25541E-07 0.00158 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.73001E-01 0.00284 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.73688E-01 0.00112 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.92683E-03 0.00638  1.57866E-04 0.03693  1.05308E-03 0.01572  9.24307E-04 0.01480  2.58951E-03 0.00894  9.40291E-04 0.01529  2.61771E-04 0.02765 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.54256E-01 0.01395  9.70649E-03 0.02420  3.09089E-02 0.00046  1.10611E-01 0.00049  3.22852E-01 0.00032  1.30100E+00 0.00180  7.85980E+00 0.01615 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.30622E-03 0.00897  1.52930E-04 0.05808  9.45518E-04 0.02219  8.06994E-04 0.02248  2.30158E-03 0.01364  8.63414E-04 0.02339  2.35784E-04 0.04414 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.57855E-01 0.02166  1.25465E-02 0.00092  3.09068E-02 0.00067  1.10677E-01 0.00074  3.23064E-01 0.00050  1.30036E+00 0.00251  8.63751E+00 0.00899 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.94844E-05 0.00148  2.94726E-05 0.00149  3.20968E-05 0.01710 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.69553E-05 0.00129  2.69446E-05 0.00129  2.93382E-05 0.01702 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.21291E-03 0.01026  1.37201E-04 0.06845  9.27981E-04 0.02567  8.04444E-04 0.02702  2.25426E-03 0.01468  8.61910E-04 0.02636  2.27120E-04 0.05057 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.56039E-01 0.02573  1.25528E-02 0.00145  3.09028E-02 0.00084  1.10673E-01 0.00091  3.22773E-01 0.00059  1.30135E+00 0.00322  8.53054E+00 0.01325 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.92253E-05 0.00319  2.92283E-05 0.00321  2.57224E-05 0.04046 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.67197E-05 0.00314  2.67226E-05 0.00316  2.35019E-05 0.04038 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.04285E-03 0.03510  1.66766E-04 0.20105  8.66228E-04 0.08187  7.42172E-04 0.08802  2.22689E-03 0.05105  7.89440E-04 0.09215  2.51354E-04 0.15813 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.82594E-01 0.08035  1.25164E-02 0.00222  3.09474E-02 0.00203  1.10909E-01 0.00217  3.22862E-01 0.00169  1.31326E+00 0.00630  8.75376E+00 0.02821 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.04507E-03 0.03462  1.62699E-04 0.19531  8.57885E-04 0.08236  7.68194E-04 0.08686  2.22249E-03 0.05012  7.78694E-04 0.08982  2.55107E-04 0.15474 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.86124E-01 0.07844  1.25164E-02 0.00222  3.09532E-02 0.00201  1.10910E-01 0.00217  3.22823E-01 0.00168  1.31256E+00 0.00637  8.75431E+00 0.02814 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.73049E+02 0.03520 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.93653E-05 0.00089 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.68475E-05 0.00064 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.07326E-03 0.00641 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.72823E+02 0.00646 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.02798E-07 0.00072 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.70483E-06 0.00043  2.70464E-06 0.00043  2.73366E-06 0.00561 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.12208E-05 0.00089  4.12391E-05 0.00089  3.79916E-05 0.01122 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.43777E-01 0.00033  6.44020E-01 0.00033  6.17531E-01 0.01019 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07413E+01 0.01589 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.59968E+01 0.00049  3.41783E+01 0.00054 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.91010E+04 0.00297  3.02987E+05 0.00159  6.08987E+05 0.00075  6.50173E+05 0.00085  5.97207E+05 0.00067  6.38574E+05 0.00059  4.34209E+05 0.00067  3.84412E+05 0.00056  2.94112E+05 0.00097  2.40165E+05 0.00104  2.07230E+05 0.00088  1.86678E+05 0.00078  1.72409E+05 0.00091  1.63825E+05 0.00097  1.59935E+05 0.00084  1.38115E+05 0.00109  1.36316E+05 0.00090  1.35177E+05 0.00095  1.33023E+05 0.00087  2.59707E+05 0.00064  2.50933E+05 0.00063  1.81226E+05 0.00110  1.17580E+05 0.00094  1.36046E+05 0.00097  1.29092E+05 0.00079  1.15766E+05 0.00102  1.88228E+05 0.00093  4.34981E+04 0.00155  5.44168E+04 0.00144  4.92943E+04 0.00160  2.87249E+04 0.00148  4.97994E+04 0.00164  3.31726E+04 0.00209  2.75141E+04 0.00177  4.82828E+03 0.00389  4.21981E+03 0.00284  3.68926E+03 0.00286  3.54854E+03 0.00333  3.60401E+03 0.00324  3.98734E+03 0.00376  4.67803E+03 0.00341  4.71550E+03 0.00309  9.23797E+03 0.00173  1.51492E+04 0.00262  1.95443E+04 0.00218  5.23054E+04 0.00132  5.51640E+04 0.00179  5.94111E+04 0.00126  3.89435E+04 0.00168  2.78002E+04 0.00165  2.07368E+04 0.00175  2.45146E+04 0.00183  4.80837E+04 0.00126  6.84214E+04 0.00143  1.36955E+05 0.00114  2.17486E+05 0.00118  3.31049E+05 0.00119  2.14174E+05 0.00121  1.54100E+05 0.00128  1.11177E+05 0.00162  1.00323E+05 0.00174  9.89596E+04 0.00167  8.27721E+04 0.00152  5.61677E+04 0.00189  5.20053E+04 0.00134  4.63142E+04 0.00182  3.92265E+04 0.00176  3.09833E+04 0.00180  2.08208E+04 0.00214  7.38998E+03 0.00264 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.22602E-01 0.00047 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.48898E+18 0.00049  4.07925E+17 0.00112 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39195E-01 0.00011  1.56099E+00 0.00041 ];
INF_CAPT                  (idx, [1:   4]) = [  7.72636E-03 0.00057  3.53437E-02 0.00071 ];
INF_ABS                   (idx, [1:   4]) = [  9.43468E-03 0.00044  6.26408E-02 0.00090 ];
INF_FISS                  (idx, [1:   4]) = [  1.70832E-03 0.00054  2.72971E-02 0.00116 ];
INF_NSF                   (idx, [1:   4]) = [  4.58798E-03 0.00054  7.27056E-02 0.00121 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.68567E+00 8.5E-05  2.66349E+00 7.4E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05471E+02 1.0E-05  2.05370E+02 1.4E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.66906E-08 0.00033  2.59630E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29755E-01 0.00012  1.49834E+00 0.00046 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44844E-01 0.00021  3.96033E-01 0.00051 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65523E-02 0.00028  9.31880E-02 0.00104 ];
INF_SCATT3                (idx, [1:   4]) = [  7.29927E-03 0.00316  2.79761E-02 0.00208 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03986E-02 0.00227 -8.95064E-03 0.00633 ];
INF_SCATT5                (idx, [1:   4]) = [  1.56482E-04 0.15534  6.62582E-03 0.00646 ];
INF_SCATT6                (idx, [1:   4]) = [  5.12070E-03 0.00364 -1.72631E-02 0.00286 ];
INF_SCATT7                (idx, [1:   4]) = [  7.61199E-04 0.02098  5.37149E-04 0.09201 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29798E-01 0.00012  1.49834E+00 0.00046 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44845E-01 0.00021  3.96033E-01 0.00051 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65525E-02 0.00028  9.31880E-02 0.00104 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.29948E-03 0.00316  2.79761E-02 0.00208 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03986E-02 0.00226 -8.95064E-03 0.00633 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.56669E-04 0.15507  6.62582E-03 0.00646 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.12076E-03 0.00364 -1.72631E-02 0.00286 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.61347E-04 0.02096  5.37149E-04 0.09201 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12280E-01 0.00030  1.01382E+00 0.00036 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.57026E+00 0.00030  3.28791E-01 0.00036 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.39216E-03 0.00045  6.26408E-02 0.00090 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69963E-02 0.00023  6.37540E-02 0.00097 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12199E-01 0.00011  1.75561E-02 0.00047  1.10082E-03 0.00437  1.49724E+00 0.00046 ];
INF_S1                    (idx, [1:   8]) = [  2.39757E-01 0.00021  5.08712E-03 0.00063  4.67645E-04 0.00602  3.95566E-01 0.00052 ];
INF_S2                    (idx, [1:   8]) = [  9.81287E-02 0.00028 -1.57637E-03 0.00230  2.54525E-04 0.01088  9.29334E-02 0.00104 ];
INF_S3                    (idx, [1:   8]) = [  9.10891E-03 0.00256 -1.80965E-03 0.00167  9.12661E-05 0.01855  2.78848E-02 0.00207 ];
INF_S4                    (idx, [1:   8]) = [ -9.82266E-03 0.00237 -5.75932E-04 0.00432 -8.30926E-07 1.00000 -8.94981E-03 0.00624 ];
INF_S5                    (idx, [1:   8]) = [  1.21129E-04 0.19498  3.53529E-05 0.07585 -3.83861E-05 0.03776  6.66420E-03 0.00639 ];
INF_S6                    (idx, [1:   8]) = [  5.25718E-03 0.00346 -1.36477E-04 0.01453 -4.74761E-05 0.02273 -1.72156E-02 0.00288 ];
INF_S7                    (idx, [1:   8]) = [  9.33173E-04 0.01679 -1.71974E-04 0.01040 -4.35791E-05 0.02765  5.80729E-04 0.08606 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12241E-01 0.00011  1.75561E-02 0.00047  1.10082E-03 0.00437  1.49724E+00 0.00046 ];
INF_SP1                   (idx, [1:   8]) = [  2.39758E-01 0.00021  5.08712E-03 0.00063  4.67645E-04 0.00602  3.95566E-01 0.00052 ];
INF_SP2                   (idx, [1:   8]) = [  9.81289E-02 0.00028 -1.57637E-03 0.00230  2.54525E-04 0.01088  9.29334E-02 0.00104 ];
INF_SP3                   (idx, [1:   8]) = [  9.10913E-03 0.00256 -1.80965E-03 0.00167  9.12661E-05 0.01855  2.78848E-02 0.00207 ];
INF_SP4                   (idx, [1:   8]) = [ -9.82267E-03 0.00236 -5.75932E-04 0.00432 -8.30926E-07 1.00000 -8.94981E-03 0.00624 ];
INF_SP5                   (idx, [1:   8]) = [  1.21316E-04 0.19456  3.53529E-05 0.07585 -3.83861E-05 0.03776  6.66420E-03 0.00639 ];
INF_SP6                   (idx, [1:   8]) = [  5.25724E-03 0.00346 -1.36477E-04 0.01453 -4.74761E-05 0.02273 -1.72156E-02 0.00288 ];
INF_SP7                   (idx, [1:   8]) = [  9.33321E-04 0.01676 -1.71974E-04 0.01040 -4.35791E-05 0.02765  5.80729E-04 0.08606 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31947E-01 0.00059  1.20315E+00 0.00625 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33457E-01 0.00105  1.31321E+00 0.00926 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34034E-01 0.00087  1.30930E+00 0.00767 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28439E-01 0.00088  1.03403E+00 0.00587 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43712E+00 0.00059  2.77312E-01 0.00627 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42785E+00 0.00105  2.54358E-01 0.00933 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42432E+00 0.00087  2.54950E-01 0.00771 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45920E+00 0.00088  3.22627E-01 0.00581 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.30622E-03 0.00897  1.52930E-04 0.05808  9.45518E-04 0.02219  8.06994E-04 0.02248  2.30158E-03 0.01364  8.63414E-04 0.02339  2.35784E-04 0.04414 ];
LAMBDA                    (idx, [1:  14]) = [  7.57855E-01 0.02166  1.25465E-02 0.00092  3.09068E-02 0.00067  1.10677E-01 0.00074  3.23064E-01 0.00050  1.30036E+00 0.00251  8.63751E+00 0.00899 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0000' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:31 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:18:59 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284831 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00361E+00  9.96491E-01  1.00554E+00  1.00429E+00  9.90069E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 9.3E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14452E-02 0.00107  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88555E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06550E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06972E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67874E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.59236E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.59140E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.58995E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.26542E-01 0.00107  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001184 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00024E+04 0.00089 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00024E+04 0.00089 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.90085E+02 ;
RUNNING_TIME              (idx, 1)        =  9.84675E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.22850E-01  4.22850E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.94267E-01  1.06500E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.68740E+01  3.43552E+00  2.68940E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  8.66100E-01  2.72500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.67500E-02  8.16667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.84673E+01  1.29330E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97713 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00041E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81570E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.62377E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.72361E+04 ;
TOT_SF_RATE               (idx, 1)        =  4.26310E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.01260E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.40742E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61114E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.58284E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.40811E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.75288E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.65284E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.69197E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.55268E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.88369E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.74037E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.41032E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.53372E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.58785E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.35645E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.35553E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.41851E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.28348E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  4.44728E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.31685E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.09815E+12 0.00053  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 15 ;
BURNUP                     (idx, [1:  2])  = [  1.75000E+01  1.75016E+01 ];
BURN_DAYS                 (idx, 1)        =  4.37500E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.99302E-01 0.00103 ];
U235_FISS                 (idx, [1:   4]) = [  5.77453E+15 0.00115  4.22809E-01 0.00089 ];
U238_FISS                 (idx, [1:   4]) = [  1.13033E+15 0.00289  8.27518E-02 0.00270 ];
PU239_FISS                (idx, [1:   4]) = [  5.71081E+15 0.00110  4.18160E-01 0.00092 ];
PU240_FISS                (idx, [1:   4]) = [  9.60970E+12 0.02849  7.03778E-04 0.02856 ];
PU241_FISS                (idx, [1:   4]) = [  1.01660E+15 0.00284  7.44407E-02 0.00280 ];
U235_CAPT                 (idx, [1:   4]) = [  1.27293E+15 0.00259  4.71581E-02 0.00251 ];
U238_CAPT                 (idx, [1:   4]) = [  9.95200E+15 0.00100  3.68678E-01 0.00070 ];
PU239_CAPT                (idx, [1:   4]) = [  3.13480E+15 0.00153  1.16138E-01 0.00144 ];
PU240_CAPT                (idx, [1:   4]) = [  2.06546E+15 0.00211  7.65193E-02 0.00202 ];
PU241_CAPT                (idx, [1:   4]) = [  3.64988E+14 0.00482  1.35225E-02 0.00481 ];
XE135_CAPT                (idx, [1:   4]) = [  7.82699E+14 0.00311  2.89985E-02 0.00310 ];
SM149_CAPT                (idx, [1:   4]) = [  2.23885E+14 0.00574  8.29533E-03 0.00575 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001184 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.93342E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001184 5.00793E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3288733 3.29331E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1664205 1.66637E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 48246 4.82587E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001184 5.00793E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.00469E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.67015E+16 2.4E-05  3.67015E+16 2.4E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36576E+16 4.8E-06  1.36576E+16 4.8E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.70121E+16 0.00048  2.02969E+16 0.00049  6.71517E+15 0.00121 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.06697E+16 0.00032  3.39545E+16 0.00029  6.71517E+15 0.00121 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.09815E+16 0.00053  4.09815E+16 0.00053  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.94422E+18 0.00052  5.12014E+17 0.00049  1.43221E+18 0.00058 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.95553E+14 0.00454 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.10653E+16 0.00032 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.47392E+18 0.00068 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10483E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10483E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.49183E+00 0.00058 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.67864E-01 0.00035 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.42435E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.22895E+00 0.00039 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93716E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96611E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.04344E-01 0.00062 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.95615E-01 0.00062 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.68726E+00 2.9E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05676E+02 4.8E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.95592E-01 0.00063  8.91021E-01 0.00062  4.59427E-03 0.01045 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.95209E-01 0.00032 ];
COL_KEFF                  (idx, [1:   2]) = [  8.95692E-01 0.00054 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.95209E-01 0.00032 ];
ABS_KINF                  (idx, [1:   2]) = [  9.03928E-01 0.00032 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70975E+01 0.00025 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70919E+01 9.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.54401E-07 0.00421 ];
IMP_EALF                  (idx, [1:   2]) = [  7.55796E-07 0.00169 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.81172E-01 0.00301 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.80750E-01 0.00120 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.92359E-03 0.00617  1.44411E-04 0.03959  1.05839E-03 0.01474  9.41932E-04 0.01504  2.57799E-03 0.00918  9.24612E-04 0.01482  2.76251E-04 0.02982 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.56604E-01 0.01500  8.93994E-03 0.02849  3.08251E-02 0.00046  1.10719E-01 0.00054  3.23016E-01 0.00036  1.28830E+00 0.00209  7.77500E+00 0.01616 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.15199E-03 0.00874  1.23024E-04 0.06024  9.17580E-04 0.02325  8.29134E-04 0.02337  2.22366E-03 0.01337  8.17398E-04 0.02312  2.41199E-04 0.04603 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.57069E-01 0.02299  1.25530E-02 0.00100  3.08352E-02 0.00065  1.10800E-01 0.00080  3.22972E-01 0.00049  1.29109E+00 0.00272  8.46990E+00 0.00983 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.98531E-05 0.00146  2.98426E-05 0.00146  3.19158E-05 0.01619 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.67309E-05 0.00131  2.67214E-05 0.00132  2.85767E-05 0.01615 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.12490E-03 0.01048  1.33234E-04 0.06837  9.14122E-04 0.02590  7.91204E-04 0.02770  2.21840E-03 0.01513  8.26461E-04 0.02599  2.41475E-04 0.05164 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.58498E-01 0.02601  1.25695E-02 0.00166  3.08381E-02 0.00087  1.10731E-01 0.00104  3.22957E-01 0.00064  1.28865E+00 0.00371  8.48269E+00 0.01431 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.94332E-05 0.00358  2.94171E-05 0.00358  3.01571E-05 0.04053 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.63547E-05 0.00352  2.63403E-05 0.00352  2.70217E-05 0.04062 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.12880E-03 0.03635  1.61248E-04 0.23115  9.87621E-04 0.08132  6.72280E-04 0.09639  2.12252E-03 0.05505  9.64460E-04 0.09197  2.20671E-04 0.19163 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.79392E-01 0.08520  1.26198E-02 0.00493  3.07623E-02 0.00194  1.10948E-01 0.00244  3.23523E-01 0.00175  1.29496E+00 0.00738  8.93902E+00 0.02180 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.15606E-03 0.03544  1.66867E-04 0.22931  9.75951E-04 0.08106  7.23041E-04 0.09486  2.12258E-03 0.05449  9.49834E-04 0.09085  2.17785E-04 0.18392 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.80037E-01 0.08467  1.26199E-02 0.00493  3.07610E-02 0.00194  1.10942E-01 0.00242  3.23591E-01 0.00175  1.29572E+00 0.00735  8.95502E+00 0.02145 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.75420E+02 0.03675 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.96570E-05 0.00090 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.65551E-05 0.00062 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.09975E-03 0.00700 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.71935E+02 0.00688 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.99557E-07 0.00076 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.68781E-06 0.00045  2.68766E-06 0.00045  2.72193E-06 0.00582 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.11849E-05 0.00092  4.12073E-05 0.00092  3.71421E-05 0.01059 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.40060E-01 0.00032  6.40394E-01 0.00033  5.96969E-01 0.01012 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07263E+01 0.01422 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.59140E+01 0.00049  3.40592E+01 0.00057 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.92963E+04 0.00317  3.03882E+05 0.00139  6.10035E+05 0.00110  6.50962E+05 0.00074  5.97861E+05 0.00050  6.39974E+05 0.00075  4.34459E+05 0.00072  3.84377E+05 0.00056  2.94415E+05 0.00099  2.40184E+05 0.00053  2.07430E+05 0.00069  1.86741E+05 0.00097  1.72403E+05 0.00099  1.63914E+05 0.00099  1.59848E+05 0.00080  1.38170E+05 0.00099  1.36663E+05 0.00106  1.35321E+05 0.00077  1.32904E+05 0.00083  2.59800E+05 0.00053  2.51192E+05 0.00073  1.81378E+05 0.00081  1.17865E+05 0.00113  1.36001E+05 0.00084  1.29521E+05 0.00104  1.15562E+05 0.00100  1.88033E+05 0.00080  4.34859E+04 0.00120  5.40759E+04 0.00165  4.91637E+04 0.00132  2.86873E+04 0.00185  4.96531E+04 0.00151  3.31030E+04 0.00159  2.71204E+04 0.00219  4.69922E+03 0.00316  4.03907E+03 0.00281  3.55314E+03 0.00379  3.38155E+03 0.00432  3.46973E+03 0.00508  3.82693E+03 0.00329  4.55225E+03 0.00285  4.57821E+03 0.00325  9.09593E+03 0.00266  1.49787E+04 0.00241  1.93898E+04 0.00200  5.19655E+04 0.00182  5.46778E+04 0.00122  5.87797E+04 0.00132  3.85551E+04 0.00167  2.73624E+04 0.00142  2.04555E+04 0.00184  2.41558E+04 0.00173  4.75743E+04 0.00170  6.75888E+04 0.00145  1.35537E+05 0.00108  2.15464E+05 0.00133  3.28456E+05 0.00124  2.12864E+05 0.00140  1.53263E+05 0.00147  1.10637E+05 0.00159  9.96480E+04 0.00177  9.84792E+04 0.00131  8.24193E+04 0.00152  5.57917E+04 0.00166  5.17237E+04 0.00167  4.60792E+04 0.00197  3.90739E+04 0.00156  3.08398E+04 0.00169  2.06824E+04 0.00189  7.34396E+03 0.00253 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.04421E-01 0.00041 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.52882E+18 0.00044  4.15441E+17 0.00133 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39335E-01 8.7E-05  1.56315E+00 0.00043 ];
INF_CAPT                  (idx, [1:   4]) = [  7.89359E-03 0.00071  3.59762E-02 0.00073 ];
INF_ABS                   (idx, [1:   4]) = [  9.55465E-03 0.00061  6.27442E-02 0.00098 ];
INF_FISS                  (idx, [1:   4]) = [  1.66106E-03 0.00044  2.67680E-02 0.00134 ];
INF_NSF                   (idx, [1:   4]) = [  4.48506E-03 0.00042  7.18540E-02 0.00138 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.70012E+00 8.4E-05  2.68432E+00 6.4E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05679E+02 9.2E-06  2.05676E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.63828E-08 0.00047  2.59857E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29800E-01 9.2E-05  1.50039E+00 0.00049 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44750E-01 0.00016  3.96550E-01 0.00055 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65338E-02 0.00032  9.33290E-02 0.00094 ];
INF_SCATT3                (idx, [1:   4]) = [  7.27783E-03 0.00261  2.79990E-02 0.00249 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03977E-02 0.00180 -9.05447E-03 0.00656 ];
INF_SCATT5                (idx, [1:   4]) = [  1.77894E-04 0.08927  6.68653E-03 0.00743 ];
INF_SCATT6                (idx, [1:   4]) = [  5.13241E-03 0.00424 -1.72594E-02 0.00275 ];
INF_SCATT7                (idx, [1:   4]) = [  7.76610E-04 0.02190  5.02070E-04 0.10021 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29842E-01 9.2E-05  1.50039E+00 0.00049 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44751E-01 0.00017  3.96550E-01 0.00055 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65340E-02 0.00032  9.33290E-02 0.00094 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.27775E-03 0.00261  2.79990E-02 0.00249 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03980E-02 0.00180 -9.05447E-03 0.00656 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.77953E-04 0.08913  6.68653E-03 0.00743 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.13271E-03 0.00425 -1.72594E-02 0.00275 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.76523E-04 0.02192  5.02070E-04 0.10021 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12243E-01 0.00027  1.01599E+00 0.00038 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.57053E+00 0.00027  3.28088E-01 0.00038 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.51212E-03 0.00061  6.27442E-02 0.00098 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69815E-02 0.00015  6.38727E-02 0.00110 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12353E-01 8.9E-05  1.74465E-02 0.00044  1.10898E-03 0.00396  1.49928E+00 0.00049 ];
INF_S1                    (idx, [1:   8]) = [  2.39704E-01 0.00016  5.04600E-03 0.00085  4.77393E-04 0.00717  3.96073E-01 0.00055 ];
INF_S2                    (idx, [1:   8]) = [  9.81133E-02 0.00030 -1.57944E-03 0.00275  2.61685E-04 0.00883  9.30673E-02 0.00094 ];
INF_S3                    (idx, [1:   8]) = [  9.08493E-03 0.00212 -1.80710E-03 0.00190  9.41464E-05 0.02138  2.79049E-02 0.00249 ];
INF_S4                    (idx, [1:   8]) = [ -9.83222E-03 0.00195 -5.65480E-04 0.00556 -6.30853E-08 1.00000 -9.05440E-03 0.00649 ];
INF_S5                    (idx, [1:   8]) = [  1.35973E-04 0.11587  4.19208E-05 0.05031 -3.75404E-05 0.03557  6.72407E-03 0.00737 ];
INF_S6                    (idx, [1:   8]) = [  5.26522E-03 0.00408 -1.32812E-04 0.01848 -4.92552E-05 0.02915 -1.72101E-02 0.00275 ];
INF_S7                    (idx, [1:   8]) = [  9.45362E-04 0.01825 -1.68752E-04 0.01348 -4.33942E-05 0.02707  5.45464E-04 0.09242 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12396E-01 8.9E-05  1.74465E-02 0.00044  1.10898E-03 0.00396  1.49928E+00 0.00049 ];
INF_SP1                   (idx, [1:   8]) = [  2.39705E-01 0.00016  5.04600E-03 0.00085  4.77393E-04 0.00717  3.96073E-01 0.00055 ];
INF_SP2                   (idx, [1:   8]) = [  9.81134E-02 0.00030 -1.57944E-03 0.00275  2.61685E-04 0.00883  9.30673E-02 0.00094 ];
INF_SP3                   (idx, [1:   8]) = [  9.08485E-03 0.00212 -1.80710E-03 0.00190  9.41464E-05 0.02138  2.79049E-02 0.00249 ];
INF_SP4                   (idx, [1:   8]) = [ -9.83253E-03 0.00194 -5.65480E-04 0.00556 -6.30853E-08 1.00000 -9.05440E-03 0.00649 ];
INF_SP5                   (idx, [1:   8]) = [  1.36032E-04 0.11570  4.19208E-05 0.05031 -3.75404E-05 0.03557  6.72407E-03 0.00737 ];
INF_SP6                   (idx, [1:   8]) = [  5.26552E-03 0.00410 -1.32812E-04 0.01848 -4.92552E-05 0.02915 -1.72101E-02 0.00275 ];
INF_SP7                   (idx, [1:   8]) = [  9.45276E-04 0.01827 -1.68752E-04 0.01348 -4.33942E-05 0.02707  5.45464E-04 0.09242 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31779E-01 0.00060  1.20888E+00 0.00611 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33361E-01 0.00092  1.32178E+00 0.00876 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33708E-01 0.00105  1.30788E+00 0.00938 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28355E-01 0.00082  1.04274E+00 0.00551 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43816E+00 0.00060  2.75986E-01 0.00612 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42843E+00 0.00091  2.52667E-01 0.00908 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42632E+00 0.00105  2.55388E-01 0.00909 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45974E+00 0.00082  3.19902E-01 0.00547 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.15199E-03 0.00874  1.23024E-04 0.06024  9.17580E-04 0.02325  8.29134E-04 0.02337  2.22366E-03 0.01337  8.17398E-04 0.02312  2.41199E-04 0.04603 ];
LAMBDA                    (idx, [1:  14]) = [  7.57069E-01 0.02299  1.25530E-02 0.00100  3.08352E-02 0.00065  1.10800E-01 0.00080  3.22972E-01 0.00049  1.29109E+00 0.00272  8.46990E+00 0.00983 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0000' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:31 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:25:12 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284831 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00239E+00  9.97488E-01  1.00659E+00  1.00261E+00  9.90928E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 9.3E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14843E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88516E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06136E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06559E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68222E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.57583E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.57487E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.57303E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.26699E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000743 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00015E+04 0.00092 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00015E+04 0.00092 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.21128E+02 ;
RUNNING_TIME              (idx, 1)        =  1.04683E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.22850E-01  4.22850E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.14900E-01  9.90000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.03013E+02  3.43532E+00  2.70357E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  9.21283E-01  2.66500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.01667E-02  7.83332E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.04682E+02  1.29531E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97817 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00065E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81805E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.68271E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.71775E+04 ;
TOT_SF_RATE               (idx, 1)        =  6.87072E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.06431E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.44504E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61838E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.57321E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.86108E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.83374E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.07255E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.99008E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.88536E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.93473E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.93721E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.43129E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.56186E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.61682E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.02041E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.68903E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.47165E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.26174E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  6.50524E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.33199E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.20616E+12 0.00055  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 16 ;
BURNUP                     (idx, [1:  2])  = [  2.00000E+01  2.00019E+01 ];
BURN_DAYS                 (idx, 1)        =  5.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.22996E-01 0.00114 ];
U235_FISS                 (idx, [1:   4]) = [  5.22409E+15 0.00128  3.82534E-01 0.00108 ];
U238_FISS                 (idx, [1:   4]) = [  1.16591E+15 0.00264  8.53668E-02 0.00248 ];
PU239_FISS                (idx, [1:   4]) = [  6.02642E+15 0.00110  4.41292E-01 0.00090 ];
PU240_FISS                (idx, [1:   4]) = [  1.12612E+13 0.02541  8.24399E-04 0.02542 ];
PU241_FISS                (idx, [1:   4]) = [  1.20979E+15 0.00263  8.85900E-02 0.00256 ];
U235_CAPT                 (idx, [1:   4]) = [  1.15781E+15 0.00270  4.12540E-02 0.00264 ];
U238_CAPT                 (idx, [1:   4]) = [  1.01703E+16 0.00111  3.62354E-01 0.00079 ];
PU239_CAPT                (idx, [1:   4]) = [  3.30454E+15 0.00157  1.17753E-01 0.00156 ];
PU240_CAPT                (idx, [1:   4]) = [  2.30979E+15 0.00202  8.22947E-02 0.00187 ];
PU241_CAPT                (idx, [1:   4]) = [  4.32788E+14 0.00440  1.54209E-02 0.00437 ];
XE135_CAPT                (idx, [1:   4]) = [  7.86269E+14 0.00339  2.80147E-02 0.00332 ];
SM149_CAPT                (idx, [1:   4]) = [  2.32046E+14 0.00616  8.26918E-03 0.00616 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000743 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.04445E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000743 5.00804E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3331269 3.33629E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1621214 1.62349E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 48260 4.82638E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000743 5.00804E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.91155E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.69012E+16 2.6E-05  3.69012E+16 2.6E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36400E+16 5.7E-06  1.36400E+16 5.7E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.80586E+16 0.00047  2.12314E+16 0.00049  6.82723E+15 0.00121 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.16986E+16 0.00031  3.48713E+16 0.00030  6.82723E+15 0.00121 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.20616E+16 0.00055  4.20616E+16 0.00055  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.99070E+18 0.00052  5.24416E+17 0.00051  1.46629E+18 0.00058 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.06062E+14 0.00464 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.21046E+16 0.00032 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.50586E+18 0.00068 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10194E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10194E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.47205E+00 0.00056 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.68699E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.37684E-01 0.00037 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.22944E+00 0.00038 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93682E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96644E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.87067E-01 0.00065 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.78505E-01 0.00065 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.70537E+00 3.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05942E+02 5.7E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.78437E-01 0.00067  8.74041E-01 0.00065  4.46353E-03 0.01128 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.77883E-01 0.00032 ];
COL_KEFF                  (idx, [1:   2]) = [  8.77442E-01 0.00054 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.77883E-01 0.00032 ];
ABS_KINF                  (idx, [1:   2]) = [  8.86444E-01 0.00031 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70500E+01 0.00024 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70526E+01 9.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.90829E-07 0.00406 ];
IMP_EALF                  (idx, [1:   2]) = [  7.86078E-07 0.00167 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.89138E-01 0.00281 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.88644E-01 0.00118 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.94164E-03 0.00608  1.51827E-04 0.03761  1.08053E-03 0.01485  9.08067E-04 0.01632  2.55788E-03 0.00903  9.47236E-04 0.01564  2.96107E-04 0.02819 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.91142E-01 0.01496  9.44934E-03 0.02573  3.07597E-02 0.00047  1.10890E-01 0.00058  3.22583E-01 0.00036  1.27832E+00 0.00213  7.83638E+00 0.01562 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.02058E-03 0.00932  1.25069E-04 0.05778  9.14402E-04 0.02165  7.65817E-04 0.02502  2.15250E-03 0.01414  8.11201E-04 0.02455  2.51583E-04 0.04106 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.98548E-01 0.02202  1.25704E-02 0.00109  3.07717E-02 0.00066  1.10959E-01 0.00083  3.22625E-01 0.00054  1.28097E+00 0.00285  8.58585E+00 0.00974 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.03815E-05 0.00155  3.03737E-05 0.00155  3.15924E-05 0.01800 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.66817E-05 0.00136  2.66749E-05 0.00137  2.77406E-05 0.01793 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.07532E-03 0.01133  1.29763E-04 0.06566  9.20472E-04 0.02553  7.92655E-04 0.02812  2.12917E-03 0.01686  8.41344E-04 0.02690  2.61909E-04 0.04641 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.10606E-01 0.02591  1.25699E-02 0.00165  3.07721E-02 0.00083  1.10975E-01 0.00105  3.22514E-01 0.00072  1.27942E+00 0.00369  8.49285E+00 0.01438 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.99519E-05 0.00357  2.99524E-05 0.00359  2.66757E-05 0.04532 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.63043E-05 0.00350  2.63047E-05 0.00351  2.34538E-05 0.04544 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.47396E-03 0.03951  7.86277E-05 0.26387  8.32721E-04 0.08564  7.31716E-04 0.10112  1.75632E-03 0.05997  7.92305E-04 0.09587  2.82274E-04 0.17175 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.12419E-01 0.08649  1.24878E-02 8.6E-05  3.07227E-02 0.00199  1.11190E-01 0.00250  3.22939E-01 0.00189  1.25589E+00 0.01083  8.52317E+00 0.03421 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.45944E-03 0.03863  7.53289E-05 0.26336  8.25248E-04 0.08376  7.26012E-04 0.09832  1.75878E-03 0.05818  7.90150E-04 0.09316  2.83921E-04 0.17124 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.13344E-01 0.08522  1.24878E-02 8.6E-05  3.07204E-02 0.00199  1.11246E-01 0.00250  3.22895E-01 0.00186  1.25805E+00 0.01065  8.51917E+00 0.03424 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.50793E+02 0.03991 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.01970E-05 0.00095 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.65203E-05 0.00068 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.95215E-03 0.00695 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.64133E+02 0.00714 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.95195E-07 0.00077 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.67528E-06 0.00044  2.67519E-06 0.00044  2.69188E-06 0.00601 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.10780E-05 0.00091  4.10950E-05 0.00091  3.78473E-05 0.01118 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.35309E-01 0.00037  6.35715E-01 0.00038  5.78606E-01 0.00985 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.09003E+01 0.01407 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.57487E+01 0.00051  3.39757E+01 0.00056 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.00203E+04 0.00408  3.04410E+05 0.00184  6.10917E+05 0.00088  6.50223E+05 0.00068  5.97787E+05 0.00081  6.40243E+05 0.00064  4.34976E+05 0.00068  3.84462E+05 0.00071  2.94307E+05 0.00080  2.40468E+05 0.00107  2.07561E+05 0.00082  1.86702E+05 0.00084  1.72384E+05 0.00078  1.64001E+05 0.00097  1.59945E+05 0.00090  1.38174E+05 0.00086  1.36425E+05 0.00087  1.35278E+05 0.00111  1.32873E+05 0.00074  2.59858E+05 0.00075  2.51328E+05 0.00067  1.81314E+05 0.00057  1.17786E+05 0.00077  1.36010E+05 0.00078  1.29596E+05 0.00087  1.15514E+05 0.00096  1.87691E+05 0.00072  4.33129E+04 0.00179  5.38372E+04 0.00129  4.89346E+04 0.00128  2.86404E+04 0.00180  4.93368E+04 0.00129  3.27837E+04 0.00147  2.68437E+04 0.00158  4.58336E+03 0.00378  3.93548E+03 0.00236  3.43376E+03 0.00372  3.30574E+03 0.00394  3.35923E+03 0.00267  3.69848E+03 0.00391  4.43583E+03 0.00299  4.51385E+03 0.00389  8.93782E+03 0.00284  1.48510E+04 0.00250  1.92136E+04 0.00244  5.15138E+04 0.00132  5.42989E+04 0.00174  5.81721E+04 0.00120  3.80993E+04 0.00170  2.70197E+04 0.00204  2.01570E+04 0.00191  2.37952E+04 0.00181  4.68268E+04 0.00172  6.67246E+04 0.00178  1.33941E+05 0.00149  2.13033E+05 0.00174  3.24775E+05 0.00163  2.10394E+05 0.00188  1.51750E+05 0.00191  1.09558E+05 0.00198  9.87971E+04 0.00179  9.75726E+04 0.00176  8.17039E+04 0.00185  5.53855E+04 0.00204  5.13480E+04 0.00235  4.56927E+04 0.00237  3.87206E+04 0.00201  3.06068E+04 0.00219  2.04693E+04 0.00208  7.28378E+03 0.00197 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.85995E-01 0.00046 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.56884E+18 0.00045  4.21903E+17 0.00156 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39368E-01 0.00010  1.56362E+00 0.00049 ];
INF_CAPT                  (idx, [1:   4]) = [  8.04454E-03 0.00087  3.65964E-02 0.00093 ];
INF_ABS                   (idx, [1:   4]) = [  9.66240E-03 0.00074  6.29163E-02 0.00118 ];
INF_FISS                  (idx, [1:   4]) = [  1.61786E-03 0.00054  2.63199E-02 0.00154 ];
INF_NSF                   (idx, [1:   4]) = [  4.39120E-03 0.00055  7.11518E-02 0.00161 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.71420E+00 9.0E-05  2.70334E+00 8.7E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05876E+02 1.2E-05  2.05957E+02 1.6E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.60841E-08 0.00033  2.60001E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29705E-01 0.00011  1.50065E+00 0.00056 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44724E-01 0.00022  3.96682E-01 0.00060 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65537E-02 0.00035  9.34135E-02 0.00080 ];
INF_SCATT3                (idx, [1:   4]) = [  7.31502E-03 0.00336  2.80072E-02 0.00247 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03854E-02 0.00194 -9.03194E-03 0.00688 ];
INF_SCATT5                (idx, [1:   4]) = [  1.66288E-04 0.12077  6.68285E-03 0.00677 ];
INF_SCATT6                (idx, [1:   4]) = [  5.16609E-03 0.00266 -1.72493E-02 0.00257 ];
INF_SCATT7                (idx, [1:   4]) = [  7.82959E-04 0.01706  5.19184E-04 0.08673 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29748E-01 0.00011  1.50065E+00 0.00056 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44724E-01 0.00022  3.96682E-01 0.00060 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65536E-02 0.00035  9.34135E-02 0.00080 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.31491E-03 0.00335  2.80072E-02 0.00247 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03854E-02 0.00194 -9.03194E-03 0.00688 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.66411E-04 0.12083  6.68285E-03 0.00677 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.16607E-03 0.00266 -1.72493E-02 0.00257 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.82986E-04 0.01705  5.19184E-04 0.08673 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12174E-01 0.00031  1.01669E+00 0.00044 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.57104E+00 0.00031  3.27864E-01 0.00044 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.61926E-03 0.00075  6.29163E-02 0.00118 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69798E-02 0.00017  6.40614E-02 0.00125 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12389E-01 0.00010  1.73165E-02 0.00055  1.09853E-03 0.00440  1.49955E+00 0.00056 ];
INF_S1                    (idx, [1:   8]) = [  2.39714E-01 0.00022  5.00978E-03 0.00100  4.73824E-04 0.00722  3.96208E-01 0.00060 ];
INF_S2                    (idx, [1:   8]) = [  9.81226E-02 0.00034 -1.56889E-03 0.00248  2.56057E-04 0.00914  9.31574E-02 0.00080 ];
INF_S3                    (idx, [1:   8]) = [  9.10735E-03 0.00265 -1.79233E-03 0.00148  9.18388E-05 0.01736  2.79154E-02 0.00247 ];
INF_S4                    (idx, [1:   8]) = [ -9.82067E-03 0.00215 -5.64682E-04 0.00598 -1.47370E-06 1.00000 -9.03047E-03 0.00688 ];
INF_S5                    (idx, [1:   8]) = [  1.30839E-04 0.14857  3.54491E-05 0.06979 -3.97506E-05 0.03865  6.72260E-03 0.00671 ];
INF_S6                    (idx, [1:   8]) = [  5.30075E-03 0.00255 -1.34656E-04 0.01785 -5.11885E-05 0.02818 -1.71981E-02 0.00258 ];
INF_S7                    (idx, [1:   8]) = [  9.49648E-04 0.01419 -1.66689E-04 0.01467 -4.58477E-05 0.03283  5.65031E-04 0.08051 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12432E-01 0.00010  1.73165E-02 0.00055  1.09853E-03 0.00440  1.49955E+00 0.00056 ];
INF_SP1                   (idx, [1:   8]) = [  2.39715E-01 0.00022  5.00978E-03 0.00100  4.73824E-04 0.00722  3.96208E-01 0.00060 ];
INF_SP2                   (idx, [1:   8]) = [  9.81225E-02 0.00034 -1.56889E-03 0.00248  2.56057E-04 0.00914  9.31574E-02 0.00080 ];
INF_SP3                   (idx, [1:   8]) = [  9.10724E-03 0.00265 -1.79233E-03 0.00148  9.18388E-05 0.01736  2.79154E-02 0.00247 ];
INF_SP4                   (idx, [1:   8]) = [ -9.82074E-03 0.00215 -5.64682E-04 0.00598 -1.47370E-06 1.00000 -9.03047E-03 0.00688 ];
INF_SP5                   (idx, [1:   8]) = [  1.30962E-04 0.14865  3.54491E-05 0.06979 -3.97506E-05 0.03865  6.72260E-03 0.00671 ];
INF_SP6                   (idx, [1:   8]) = [  5.30073E-03 0.00255 -1.34656E-04 0.01785 -5.11885E-05 0.02818 -1.71981E-02 0.00258 ];
INF_SP7                   (idx, [1:   8]) = [  9.49675E-04 0.01418 -1.66689E-04 0.01467 -4.58477E-05 0.03283  5.65031E-04 0.08051 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31986E-01 0.00053  1.22099E+00 0.00552 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33556E-01 0.00090  1.32618E+00 0.00809 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33701E-01 0.00071  1.32951E+00 0.00584 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28777E-01 0.00104  1.05298E+00 0.00712 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43688E+00 0.00053  2.73202E-01 0.00551 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42724E+00 0.00090  2.51729E-01 0.00782 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42634E+00 0.00071  2.50928E-01 0.00595 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45706E+00 0.00104  3.16949E-01 0.00715 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.02058E-03 0.00932  1.25069E-04 0.05778  9.14402E-04 0.02165  7.65817E-04 0.02502  2.15250E-03 0.01414  8.11201E-04 0.02455  2.51583E-04 0.04106 ];
LAMBDA                    (idx, [1:  14]) = [  7.98548E-01 0.02202  1.25704E-02 0.00109  3.07717E-02 0.00066  1.10959E-01 0.00083  3.22625E-01 0.00054  1.28097E+00 0.00285  8.58585E+00 0.00974 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0000' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:31 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:31:24 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284831 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00266E+00  9.97953E-01  1.00600E+00  1.00333E+00  9.90047E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15757E-02 0.00113  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88424E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05968E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06395E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68598E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.56266E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.56171E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.55051E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.28773E-01 0.00112  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001178 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00024E+04 0.00104 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00024E+04 0.00104 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.52095E+02 ;
RUNNING_TIME              (idx, 1)        =  1.10883E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.22850E-01  4.22850E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.36633E-01  1.05000E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.09135E+02  3.42477E+00  2.69685E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  9.77750E-01  2.76000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.36833E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.10883E+02  1.29508E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97907 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99915E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.82012E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.71790E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68858E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.50254E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.13846E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.50088E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.57941E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.53845E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  3.92699E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.95652E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.08176E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.49446E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  8.45229E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.00708E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.30428E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.41481E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.57202E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.61219E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.52879E+13 ;
CS137_ACTIVITY            (idx, 1)        =  3.35310E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.50042E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.21494E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.17592E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.34846E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.40631E+12 0.00053  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 17 ;
BURNUP                     (idx, [1:  2])  = [  2.50000E+01  2.50024E+01 ];
BURN_DAYS                 (idx, 1)        =  6.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.67210E-01 0.00107 ];
U233_FISS                 (idx, [1:   4]) = [  8.78549E+09 1.00000  6.40410E-07 1.00000 ];
U235_FISS                 (idx, [1:   4]) = [  4.26840E+15 0.00144  3.13876E-01 0.00131 ];
U238_FISS                 (idx, [1:   4]) = [  1.21275E+15 0.00266  8.91673E-02 0.00248 ];
PU239_FISS                (idx, [1:   4]) = [  6.49793E+15 0.00119  4.77802E-01 0.00093 ];
PU240_FISS                (idx, [1:   4]) = [  1.47269E+13 0.02503  1.08249E-03 0.02502 ];
PU241_FISS                (idx, [1:   4]) = [  1.57886E+15 0.00251  1.16095E-01 0.00238 ];
U235_CAPT                 (idx, [1:   4]) = [  9.49959E+14 0.00300  3.15507E-02 0.00301 ];
U238_CAPT                 (idx, [1:   4]) = [  1.05878E+16 0.00103  3.51605E-01 0.00074 ];
PU239_CAPT                (idx, [1:   4]) = [  3.56323E+15 0.00153  1.18338E-01 0.00146 ];
PU240_CAPT                (idx, [1:   4]) = [  2.70294E+15 0.00184  8.97610E-02 0.00171 ];
PU241_CAPT                (idx, [1:   4]) = [  5.69907E+14 0.00397  1.89275E-02 0.00396 ];
XE135_CAPT                (idx, [1:   4]) = [  7.99087E+14 0.00313  2.65381E-02 0.00309 ];
SM149_CAPT                (idx, [1:   4]) = [  2.44290E+14 0.00599  8.11181E-03 0.00591 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001178 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.11604E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001178 5.00812E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3412044 3.41692E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1541226 1.54328E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 47908 4.79184E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001178 5.00812E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.09782E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.72517E+16 2.4E-05  3.72517E+16 2.4E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36086E+16 5.5E-06  1.36086E+16 5.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.01144E+16 0.00045  2.30186E+16 0.00047  7.09586E+15 0.00122 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.37230E+16 0.00031  3.66271E+16 0.00030  7.09586E+15 0.00122 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.40631E+16 0.00053  4.40631E+16 0.00053  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.08081E+18 0.00051  5.48629E+17 0.00051  1.53218E+18 0.00057 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.22334E+14 0.00484 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.41453E+16 0.00032 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.57173E+18 0.00068 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.09616E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.09616E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.43430E+00 0.00066 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.67799E-01 0.00036 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.30115E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.22950E+00 0.00040 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93733E-01 3.7E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96662E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.53098E-01 0.00071 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.44923E-01 0.00071 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.73737E+00 3.0E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06418E+02 5.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.45023E-01 0.00073  8.40863E-01 0.00071  4.06024E-03 0.01149 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.45259E-01 0.00032 ];
COL_KEFF                  (idx, [1:   2]) = [  8.45535E-01 0.00053 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.45259E-01 0.00032 ];
ABS_KINF                  (idx, [1:   2]) = [  8.53435E-01 0.00031 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.69804E+01 0.00025 ];
IMP_ALF                   (idx, [1:   2]) = [  1.69820E+01 0.00011 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  8.48046E-07 0.00417 ];
IMP_EALF                  (idx, [1:   2]) = [  8.43720E-07 0.00180 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.03139E-01 0.00280 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.02372E-01 0.00117 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.89575E-03 0.00637  1.42024E-04 0.04052  1.07694E-03 0.01492  9.62088E-04 0.01521  2.48692E-03 0.00974  9.56034E-04 0.01661  2.71756E-04 0.02968 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.45939E-01 0.01561  8.84166E-03 0.02919  3.06144E-02 0.00046  1.11200E-01 0.00057  3.22995E-01 0.00037  1.25392E+00 0.00315  7.46074E+00 0.01821 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.85539E-03 0.00952  1.17593E-04 0.06394  8.86440E-04 0.02289  7.96433E-04 0.02422  2.05949E-03 0.01546  7.66827E-04 0.02494  2.28606E-04 0.04669 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.52326E-01 0.02579  1.25926E-02 0.00128  3.06063E-02 0.00069  1.11255E-01 0.00084  3.22829E-01 0.00056  1.25518E+00 0.00350  8.18777E+00 0.01303 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.15136E-05 0.00155  3.15007E-05 0.00155  3.41250E-05 0.01813 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.66232E-05 0.00139  2.66122E-05 0.00140  2.88363E-05 0.01814 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.82215E-03 0.01153  1.11888E-04 0.07518  9.07309E-04 0.02659  7.87070E-04 0.02901  2.03149E-03 0.01783  7.70254E-04 0.02952  2.14143E-04 0.05537 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.23270E-01 0.02892  1.25852E-02 0.00187  3.06086E-02 0.00085  1.11122E-01 0.00112  3.22872E-01 0.00074  1.25655E+00 0.00470  8.23738E+00 0.01856 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.11524E-05 0.00373  3.11393E-05 0.00376  2.88982E-05 0.04867 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.63154E-05 0.00362  2.63043E-05 0.00365  2.43847E-05 0.04844 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.66528E-03 0.04168  1.21633E-04 0.24675  8.74649E-04 0.09382  7.48269E-04 0.10021  2.01011E-03 0.06189  6.63301E-04 0.11157  2.47319E-04 0.17719 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.97927E-01 0.09720  1.25767E-02 0.00489  3.06613E-02 0.00213  1.11809E-01 0.00281  3.22963E-01 0.00194  1.26743E+00 0.01113  8.30244E+00 0.04235 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.66565E-03 0.04081  1.15889E-04 0.25484  8.84150E-04 0.09253  7.68087E-04 0.09895  1.97767E-03 0.06049  6.68183E-04 0.10788  2.51676E-04 0.17503 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.01213E-01 0.09520  1.25767E-02 0.00489  3.06584E-02 0.00213  1.11810E-01 0.00279  3.22862E-01 0.00192  1.26689E+00 0.01114  8.30539E+00 0.04237 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.51458E+02 0.04192 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.13177E-05 0.00109 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.64569E-05 0.00078 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.72429E-03 0.00865 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.50977E+02 0.00877 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.91222E-07 0.00077 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.64621E-06 0.00045  2.64611E-06 0.00045  2.66521E-06 0.00580 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.11923E-05 0.00090  4.12084E-05 0.00089  3.80714E-05 0.01196 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.27856E-01 0.00036  6.28340E-01 0.00036  5.57375E-01 0.01042 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08022E+01 0.01477 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.56171E+01 0.00050  3.38554E+01 0.00055 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.04474E+04 0.00299  3.05618E+05 0.00114  6.11217E+05 0.00100  6.50118E+05 0.00083  5.97923E+05 0.00073  6.41141E+05 0.00069  4.35646E+05 0.00065  3.84677E+05 0.00075  2.94476E+05 0.00077  2.40854E+05 0.00072  2.07374E+05 0.00087  1.86960E+05 0.00079  1.72515E+05 0.00105  1.64312E+05 0.00091  1.60246E+05 0.00073  1.38121E+05 0.00092  1.36626E+05 0.00107  1.35612E+05 0.00073  1.33075E+05 0.00088  2.60147E+05 0.00083  2.51314E+05 0.00072  1.81576E+05 0.00098  1.17917E+05 0.00095  1.35953E+05 0.00096  1.29398E+05 0.00104  1.15440E+05 0.00105  1.86455E+05 0.00068  4.32608E+04 0.00160  5.33793E+04 0.00131  4.86011E+04 0.00108  2.85624E+04 0.00176  4.91397E+04 0.00195  3.24263E+04 0.00149  2.61659E+04 0.00153  4.40675E+03 0.00284  3.75694E+03 0.00335  3.26874E+03 0.00482  3.15250E+03 0.00428  3.19999E+03 0.00395  3.52941E+03 0.00286  4.22423E+03 0.00428  4.36385E+03 0.00443  8.73852E+03 0.00237  1.44869E+04 0.00275  1.88679E+04 0.00275  5.06060E+04 0.00141  5.33634E+04 0.00163  5.72859E+04 0.00166  3.75071E+04 0.00159  2.65559E+04 0.00178  1.97675E+04 0.00217  2.33171E+04 0.00225  4.60192E+04 0.00178  6.55866E+04 0.00164  1.31953E+05 0.00146  2.10713E+05 0.00161  3.21884E+05 0.00161  2.08556E+05 0.00188  1.50501E+05 0.00184  1.08661E+05 0.00186  9.80005E+04 0.00205  9.68362E+04 0.00176  8.10433E+04 0.00179  5.49986E+04 0.00182  5.09788E+04 0.00164  4.54291E+04 0.00194  3.85117E+04 0.00197  3.04400E+04 0.00241  2.04120E+04 0.00268  7.24937E+03 0.00315 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.53717E-01 0.00054 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.64340E+18 0.00062  4.37449E+17 0.00143 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39638E-01 0.00010  1.56568E+00 0.00047 ];
INF_CAPT                  (idx, [1:   4]) = [  8.32540E-03 0.00080  3.75688E-02 0.00088 ];
INF_ABS                   (idx, [1:   4]) = [  9.86799E-03 0.00066  6.28879E-02 0.00108 ];
INF_FISS                  (idx, [1:   4]) = [  1.54258E-03 0.00048  2.53191E-02 0.00139 ];
INF_NSF                   (idx, [1:   4]) = [  4.22594E-03 0.00048  6.92956E-02 0.00146 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.73952E+00 8.4E-05  2.73688E+00 8.9E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06236E+02 1.2E-05  2.06459E+02 1.7E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.55469E-08 0.00039  2.60376E-06 0.00021 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29772E-01 0.00010  1.50282E+00 0.00054 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44564E-01 0.00022  3.96958E-01 0.00063 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65240E-02 0.00027  9.33344E-02 0.00107 ];
INF_SCATT3                (idx, [1:   4]) = [  7.28313E-03 0.00268  2.78371E-02 0.00203 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03963E-02 0.00169 -9.11791E-03 0.00747 ];
INF_SCATT5                (idx, [1:   4]) = [  1.50760E-04 0.15335  6.59869E-03 0.00983 ];
INF_SCATT6                (idx, [1:   4]) = [  5.11506E-03 0.00373 -1.73589E-02 0.00311 ];
INF_SCATT7                (idx, [1:   4]) = [  7.72633E-04 0.02247  5.62043E-04 0.07894 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29816E-01 0.00010  1.50282E+00 0.00054 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44565E-01 0.00022  3.96958E-01 0.00063 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65237E-02 0.00027  9.33344E-02 0.00107 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.28282E-03 0.00269  2.78371E-02 0.00203 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03965E-02 0.00169 -9.11791E-03 0.00747 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.50819E-04 0.15347  6.59869E-03 0.00983 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.11530E-03 0.00372 -1.73589E-02 0.00311 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.72571E-04 0.02247  5.62043E-04 0.07894 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12304E-01 0.00026  1.01920E+00 0.00038 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.57008E+00 0.00026  3.27054E-01 0.00038 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.82446E-03 0.00067  6.28879E-02 0.00108 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69791E-02 0.00025  6.39587E-02 0.00132 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12659E-01 1.0E-04  1.71132E-02 0.00049  1.09458E-03 0.00435  1.50173E+00 0.00054 ];
INF_S1                    (idx, [1:   8]) = [  2.39631E-01 0.00022  4.93280E-03 0.00090  4.73805E-04 0.00649  3.96484E-01 0.00063 ];
INF_S2                    (idx, [1:   8]) = [  9.80899E-02 0.00027 -1.56594E-03 0.00166  2.57769E-04 0.01033  9.30767E-02 0.00107 ];
INF_S3                    (idx, [1:   8]) = [  9.05392E-03 0.00209 -1.77079E-03 0.00189  9.14694E-05 0.02189  2.77456E-02 0.00205 ];
INF_S4                    (idx, [1:   8]) = [ -9.84838E-03 0.00180 -5.47961E-04 0.00475 -1.84473E-06 0.79801 -9.11607E-03 0.00747 ];
INF_S5                    (idx, [1:   8]) = [  1.08581E-04 0.20974  4.21781E-05 0.06816 -3.95646E-05 0.03980  6.63826E-03 0.00978 ];
INF_S6                    (idx, [1:   8]) = [  5.24950E-03 0.00360 -1.34440E-04 0.01751 -4.76499E-05 0.03042 -1.73113E-02 0.00314 ];
INF_S7                    (idx, [1:   8]) = [  9.40738E-04 0.01830 -1.68104E-04 0.01271 -4.32746E-05 0.03426  6.05318E-04 0.07328 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12703E-01 9.9E-05  1.71132E-02 0.00049  1.09458E-03 0.00435  1.50173E+00 0.00054 ];
INF_SP1                   (idx, [1:   8]) = [  2.39632E-01 0.00022  4.93280E-03 0.00090  4.73805E-04 0.00649  3.96484E-01 0.00063 ];
INF_SP2                   (idx, [1:   8]) = [  9.80897E-02 0.00027 -1.56594E-03 0.00166  2.57769E-04 0.01033  9.30767E-02 0.00107 ];
INF_SP3                   (idx, [1:   8]) = [  9.05362E-03 0.00210 -1.77079E-03 0.00189  9.14694E-05 0.02189  2.77456E-02 0.00205 ];
INF_SP4                   (idx, [1:   8]) = [ -9.84853E-03 0.00179 -5.47961E-04 0.00475 -1.84473E-06 0.79801 -9.11607E-03 0.00747 ];
INF_SP5                   (idx, [1:   8]) = [  1.08641E-04 0.20984  4.21781E-05 0.06816 -3.95646E-05 0.03980  6.63826E-03 0.00978 ];
INF_SP6                   (idx, [1:   8]) = [  5.24974E-03 0.00360 -1.34440E-04 0.01751 -4.76499E-05 0.03042 -1.73113E-02 0.00314 ];
INF_SP7                   (idx, [1:   8]) = [  9.40676E-04 0.01829 -1.68104E-04 0.01271 -4.32746E-05 0.03426  6.05318E-04 0.07328 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31957E-01 0.00052  1.21391E+00 0.00641 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33646E-01 0.00082  1.32471E+00 0.00684 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33857E-01 0.00089  1.32145E+00 0.00885 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28460E-01 0.00098  1.04289E+00 0.00669 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43705E+00 0.00052  2.74873E-01 0.00661 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42668E+00 0.00082  2.51918E-01 0.00704 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42540E+00 0.00089  2.52730E-01 0.00899 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45908E+00 0.00099  3.19972E-01 0.00676 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.85539E-03 0.00952  1.17593E-04 0.06394  8.86440E-04 0.02289  7.96433E-04 0.02422  2.05949E-03 0.01546  7.66827E-04 0.02494  2.28606E-04 0.04669 ];
LAMBDA                    (idx, [1:  14]) = [  7.52326E-01 0.02579  1.25926E-02 0.00128  3.06063E-02 0.00069  1.11255E-01 0.00084  3.22829E-01 0.00056  1.25518E+00 0.00350  8.18777E+00 0.01303 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0000' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:31 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:37:36 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284831 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00322E+00  9.98378E-01  1.00652E+00  1.00199E+00  9.89892E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.6E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16105E-02 0.00105  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88389E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05155E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05582E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68818E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.54164E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.54071E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.54451E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.28280E-01 0.00111  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001350 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00027E+04 0.00093 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00027E+04 0.00093 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.83059E+02 ;
RUNNING_TIME              (idx, 1)        =  1.17083E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.22850E-01  4.22850E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.58617E-01  1.06667E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.15255E+02  3.42382E+00  2.69655E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.03465E+00  2.78333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.54500E-02  9.16668E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.17083E+02  1.29479E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97988 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00024E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.82192E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.85012E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68938E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.80991E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.24029E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.57734E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.60979E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.53160E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  5.22232E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.11884E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.32634E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.01537E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  8.95978E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.10348E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.63982E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.45242E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.61511E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.66131E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.23241E+13 ;
CS137_ACTIVITY            (idx, 1)        =  4.01306E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.61791E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.18736E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.82702E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.37857E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.58897E+12 0.00053  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 18 ;
BURNUP                     (idx, [1:  2])  = [  3.00000E+01  3.00029E+01 ];
BURN_DAYS                 (idx, 1)        =  7.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  8.07987E-01 0.00105 ];
U235_FISS                 (idx, [1:   4]) = [  3.45723E+15 0.00159  2.54576E-01 0.00145 ];
U238_FISS                 (idx, [1:   4]) = [  1.25417E+15 0.00270  9.23417E-02 0.00254 ];
PU239_FISS                (idx, [1:   4]) = [  6.90631E+15 0.00106  5.08552E-01 0.00084 ];
PU240_FISS                (idx, [1:   4]) = [  1.71240E+13 0.02240  1.26086E-03 0.02240 ];
PU241_FISS                (idx, [1:   4]) = [  1.91042E+15 0.00214  1.40674E-01 0.00203 ];
U235_CAPT                 (idx, [1:   4]) = [  7.67075E+14 0.00345  2.40023E-02 0.00335 ];
U238_CAPT                 (idx, [1:   4]) = [  1.09887E+16 0.00108  3.43851E-01 0.00079 ];
PU239_CAPT                (idx, [1:   4]) = [  3.77625E+15 0.00140  1.18179E-01 0.00140 ];
PU240_CAPT                (idx, [1:   4]) = [  3.05225E+15 0.00180  9.55089E-02 0.00164 ];
PU241_CAPT                (idx, [1:   4]) = [  6.80695E+14 0.00350  2.13029E-02 0.00351 ];
XE135_CAPT                (idx, [1:   4]) = [  7.99837E+14 0.00348  2.50339E-02 0.00354 ];
SM149_CAPT                (idx, [1:   4]) = [  2.54991E+14 0.00599  7.98040E-03 0.00600 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001350 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.35472E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001350 5.00835E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3476847 3.48187E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1477808 1.47979E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46695 4.67013E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001350 5.00835E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -1.01514E-07 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.75526E+16 2.2E-05  3.75526E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35817E+16 4.4E-06  1.35817E+16 4.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.19784E+16 0.00046  2.46862E+16 0.00046  7.29215E+15 0.00118 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.55601E+16 0.00032  3.82679E+16 0.00030  7.29215E+15 0.00118 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.58897E+16 0.00053  4.58897E+16 0.00053  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.16086E+18 0.00052  5.70926E+17 0.00047  1.58994E+18 0.00059 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.28703E+14 0.00487 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.59888E+16 0.00033 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.62726E+18 0.00067 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.09039E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.09039E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.40212E+00 0.00061 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.69047E-01 0.00035 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.23590E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.22856E+00 0.00040 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93897E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96743E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.26033E-01 0.00066 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.18319E-01 0.00067 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.76495E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06827E+02 4.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.18395E-01 0.00067  8.14445E-01 0.00067  3.87362E-03 0.01204 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.17961E-01 0.00033 ];
COL_KEFF                  (idx, [1:   2]) = [  8.18439E-01 0.00053 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.17961E-01 0.00033 ];
ABS_KINF                  (idx, [1:   2]) = [  8.25668E-01 0.00032 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.69263E+01 0.00026 ];
IMP_ALF                   (idx, [1:   2]) = [  1.69198E+01 0.00010 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  8.95642E-07 0.00442 ];
IMP_EALF                  (idx, [1:   2]) = [  8.97820E-07 0.00174 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.14133E-01 0.00281 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.15012E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.02027E-03 0.00634  1.44408E-04 0.04101  1.10948E-03 0.01464  9.22277E-04 0.01604  2.54783E-03 0.01047  1.01587E-03 0.01548  2.80403E-04 0.02919 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.33125E-01 0.01550  8.89549E-03 0.02906  3.05111E-02 0.00041  1.11064E-01 0.00210  3.23301E-01 0.00040  1.23825E+00 0.00256  7.11682E+00 0.01925 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.73292E-03 0.00998  1.11870E-04 0.06465  8.86439E-04 0.02400  7.36440E-04 0.02586  1.96245E-03 0.01629  8.11832E-04 0.02293  2.23893E-04 0.04786 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.37341E-01 0.02313  1.26373E-02 0.00147  3.05191E-02 0.00062  1.11340E-01 0.00091  3.23494E-01 0.00060  1.23784E+00 0.00385  7.90935E+00 0.01436 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.24449E-05 0.00149  3.24320E-05 0.00149  3.53018E-05 0.02088 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.65481E-05 0.00140  2.65375E-05 0.00140  2.88833E-05 0.02082 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.73541E-03 0.01197  1.13028E-04 0.08094  8.84557E-04 0.02730  7.42934E-04 0.03020  1.95755E-03 0.01872  8.01580E-04 0.02807  2.35756E-04 0.05566 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.58517E-01 0.03105  1.26505E-02 0.00252  3.05087E-02 0.00086  1.11411E-01 0.00127  3.23324E-01 0.00078  1.24117E+00 0.00493  7.78525E+00 0.02293 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.18002E-05 0.00379  3.17752E-05 0.00380  2.97118E-05 0.05151 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.60220E-05 0.00379  2.60017E-05 0.00380  2.42943E-05 0.05151 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.52955E-03 0.04330  9.05603E-05 0.25830  9.82458E-04 0.09625  8.13455E-04 0.09931  1.70026E-03 0.06387  7.14513E-04 0.10471  2.28303E-04 0.23355 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  6.62476E-01 0.09228  1.27100E-02 0.00802  3.04292E-02 0.00189  1.12200E-01 0.00297  3.24033E-01 0.00216  1.23465E+00 0.01314  7.30401E+00 0.07032 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.53841E-03 0.04219  9.44926E-05 0.25054  9.94215E-04 0.09558  8.02038E-04 0.09848  1.72512E-03 0.06225  7.01595E-04 0.10295  2.20947E-04 0.22151 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  6.63861E-01 0.09089  1.27100E-02 0.00802  3.04275E-02 0.00189  1.12219E-01 0.00296  3.24069E-01 0.00215  1.23500E+00 0.01302  7.30702E+00 0.07033 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.43810E+02 0.04363 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.22030E-05 0.00098 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.63485E-05 0.00068 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.65111E-03 0.00830 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.44506E+02 0.00838 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.85482E-07 0.00071 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.62840E-06 0.00045  2.62820E-06 0.00045  2.66707E-06 0.00612 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.10822E-05 0.00087  4.11000E-05 0.00087  3.75491E-05 0.01188 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.21483E-01 0.00035  6.22092E-01 0.00036  5.32761E-01 0.01077 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06453E+01 0.01455 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.54071E+01 0.00047  3.37749E+01 0.00058 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.13123E+04 0.00258  3.06459E+05 0.00188  6.10896E+05 0.00087  6.49879E+05 0.00062  5.98342E+05 0.00060  6.40505E+05 0.00056  4.34869E+05 0.00075  3.84830E+05 0.00063  2.94653E+05 0.00054  2.41089E+05 0.00079  2.07806E+05 0.00080  1.87326E+05 0.00058  1.72974E+05 0.00091  1.64633E+05 0.00092  1.60376E+05 0.00080  1.38449E+05 0.00097  1.36912E+05 0.00090  1.35778E+05 0.00097  1.33254E+05 0.00092  2.60392E+05 0.00062  2.51991E+05 0.00080  1.81775E+05 0.00088  1.17893E+05 0.00102  1.35828E+05 0.00094  1.29627E+05 0.00081  1.15020E+05 0.00091  1.86056E+05 0.00070  4.29019E+04 0.00163  5.28964E+04 0.00099  4.82698E+04 0.00163  2.84385E+04 0.00188  4.89062E+04 0.00196  3.19686E+04 0.00226  2.57247E+04 0.00223  4.29085E+03 0.00305  3.62317E+03 0.00437  3.15606E+03 0.00405  3.06034E+03 0.00315  3.11400E+03 0.00509  3.37733E+03 0.00329  4.09248E+03 0.00456  4.25606E+03 0.00314  8.52004E+03 0.00312  1.42650E+04 0.00283  1.86107E+04 0.00249  4.99951E+04 0.00166  5.27672E+04 0.00118  5.66334E+04 0.00139  3.69404E+04 0.00158  2.61523E+04 0.00175  1.94233E+04 0.00176  2.30047E+04 0.00144  4.52195E+04 0.00109  6.44200E+04 0.00161  1.30145E+05 0.00159  2.07636E+05 0.00144  3.17712E+05 0.00143  2.06067E+05 0.00153  1.48614E+05 0.00175  1.07365E+05 0.00159  9.69156E+04 0.00154  9.56563E+04 0.00161  8.00546E+04 0.00168  5.42650E+04 0.00194  5.02496E+04 0.00186  4.48133E+04 0.00181  3.79899E+04 0.00181  3.00452E+04 0.00209  2.01469E+04 0.00190  7.17065E+03 0.00211 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.26155E-01 0.00053 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.71132E+18 0.00049  4.49584E+17 0.00151 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39958E-01 0.00010  1.56420E+00 0.00042 ];
INF_CAPT                  (idx, [1:   4]) = [  8.57030E-03 0.00055  3.85111E-02 0.00081 ];
INF_ABS                   (idx, [1:   4]) = [  1.00497E-02 0.00049  6.30947E-02 0.00108 ];
INF_FISS                  (idx, [1:   4]) = [  1.47938E-03 0.00050  2.45837E-02 0.00153 ];
INF_NSF                   (idx, [1:   4]) = [  4.08608E-03 0.00050  6.79890E-02 0.00155 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.76202E+00 6.4E-05  2.76562E+00 3.8E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06548E+02 7.7E-06  2.06890E+02 7.4E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.51349E-08 0.00040  2.60436E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29914E-01 0.00010  1.50114E+00 0.00049 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44438E-01 0.00019  3.96599E-01 0.00059 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64688E-02 0.00024  9.34506E-02 0.00081 ];
INF_SCATT3                (idx, [1:   4]) = [  7.26527E-03 0.00294  2.80552E-02 0.00253 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03786E-02 0.00217 -8.95819E-03 0.00662 ];
INF_SCATT5                (idx, [1:   4]) = [  1.56894E-04 0.09447  6.72424E-03 0.00759 ];
INF_SCATT6                (idx, [1:   4]) = [  5.14131E-03 0.00289 -1.72934E-02 0.00333 ];
INF_SCATT7                (idx, [1:   4]) = [  7.73830E-04 0.02194  4.64688E-04 0.08401 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29959E-01 0.00010  1.50114E+00 0.00049 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44439E-01 0.00019  3.96599E-01 0.00059 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64688E-02 0.00024  9.34506E-02 0.00081 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.26523E-03 0.00293  2.80552E-02 0.00253 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03785E-02 0.00217 -8.95819E-03 0.00662 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.56716E-04 0.09466  6.72424E-03 0.00759 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.14113E-03 0.00288 -1.72934E-02 0.00333 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.73727E-04 0.02195  4.64688E-04 0.08401 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12507E-01 0.00034  1.01907E+00 0.00033 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56858E+00 0.00034  3.27098E-01 0.00033 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.00049E-02 0.00047  6.30947E-02 0.00108 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69860E-02 0.00024  6.41604E-02 0.00118 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12972E-01 0.00010  1.69419E-02 0.00040  1.10174E-03 0.00297  1.50004E+00 0.00049 ];
INF_S1                    (idx, [1:   8]) = [  2.39551E-01 0.00019  4.88748E-03 0.00096  4.73397E-04 0.00457  3.96126E-01 0.00059 ];
INF_S2                    (idx, [1:   8]) = [  9.80217E-02 0.00024 -1.55284E-03 0.00295  2.57453E-04 0.00830  9.31932E-02 0.00082 ];
INF_S3                    (idx, [1:   8]) = [  9.02044E-03 0.00236 -1.75517E-03 0.00201  9.24260E-05 0.01965  2.79628E-02 0.00254 ];
INF_S4                    (idx, [1:   8]) = [ -9.83836E-03 0.00230 -5.40238E-04 0.00371  1.24003E-06 1.00000 -8.95943E-03 0.00663 ];
INF_S5                    (idx, [1:   8]) = [  1.10032E-04 0.13691  4.68620E-05 0.04805 -3.99465E-05 0.03798  6.76418E-03 0.00756 ];
INF_S6                    (idx, [1:   8]) = [  5.26996E-03 0.00292 -1.28648E-04 0.01441 -4.70082E-05 0.03686 -1.72464E-02 0.00333 ];
INF_S7                    (idx, [1:   8]) = [  9.41445E-04 0.01863 -1.67615E-04 0.01335 -4.23268E-05 0.03493  5.07014E-04 0.07719 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.13017E-01 0.00010  1.69419E-02 0.00040  1.10174E-03 0.00297  1.50004E+00 0.00049 ];
INF_SP1                   (idx, [1:   8]) = [  2.39552E-01 0.00019  4.88748E-03 0.00096  4.73397E-04 0.00457  3.96126E-01 0.00059 ];
INF_SP2                   (idx, [1:   8]) = [  9.80216E-02 0.00024 -1.55284E-03 0.00295  2.57453E-04 0.00830  9.31932E-02 0.00082 ];
INF_SP3                   (idx, [1:   8]) = [  9.02039E-03 0.00236 -1.75517E-03 0.00201  9.24260E-05 0.01965  2.79628E-02 0.00254 ];
INF_SP4                   (idx, [1:   8]) = [ -9.83822E-03 0.00230 -5.40238E-04 0.00371  1.24003E-06 1.00000 -8.95943E-03 0.00663 ];
INF_SP5                   (idx, [1:   8]) = [  1.09854E-04 0.13720  4.68620E-05 0.04805 -3.99465E-05 0.03798  6.76418E-03 0.00756 ];
INF_SP6                   (idx, [1:   8]) = [  5.26977E-03 0.00291 -1.28648E-04 0.01441 -4.70082E-05 0.03686 -1.72464E-02 0.00333 ];
INF_SP7                   (idx, [1:   8]) = [  9.41342E-04 0.01864 -1.67615E-04 0.01335 -4.23268E-05 0.03493  5.07014E-04 0.07719 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31874E-01 0.00063  1.20270E+00 0.00751 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33759E-01 0.00092  1.31178E+00 0.01015 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33565E-01 0.00090  1.30605E+00 0.00784 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28389E-01 0.00112  1.03600E+00 0.00807 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43758E+00 0.00063  2.77520E-01 0.00733 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42600E+00 0.00092  2.54726E-01 0.01000 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42718E+00 0.00090  2.55594E-01 0.00772 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45954E+00 0.00112  3.22241E-01 0.00785 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.73292E-03 0.00998  1.11870E-04 0.06465  8.86439E-04 0.02400  7.36440E-04 0.02586  1.96245E-03 0.01629  8.11832E-04 0.02293  2.23893E-04 0.04786 ];
LAMBDA                    (idx, [1:  14]) = [  7.37341E-01 0.02313  1.26373E-02 0.00147  3.05191E-02 0.00062  1.11340E-01 0.00091  3.23494E-01 0.00060  1.23784E+00 0.00385  7.90935E+00 0.01436 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0000' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:31 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:43:44 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284831 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00250E+00  9.96628E-01  1.00640E+00  1.00385E+00  9.90621E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.17010E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88299E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04609E-01 0.00013  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05040E-01 0.00013  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69163E+00 0.00024  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.51933E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.51841E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.51995E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.29073E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001013 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00020E+04 0.00099 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00020E+04 0.00099 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  6.13668E+02 ;
RUNNING_TIME              (idx, 1)        =  1.23212E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.22850E-01  4.22850E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.80467E-01  1.06667E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.21305E+02  3.36300E+00  2.68695E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.09108E+00  2.75500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.73167E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.23212E+02  1.29399E+02 ];
CPU_USAGE                 (idx, 1)        = 4.98059 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00120E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.82350E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.97857E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.69378E+04 ;
TOT_SF_RATE               (idx, 1)        =  4.69727E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.33281E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.64832E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.64572E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.52891E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  6.75624E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.28087E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.81554E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.08200E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  9.40707E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.19887E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.94847E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.48824E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.65382E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.70695E+14 ;
CS134_ACTIVITY            (idx, 1)        =  8.09087E+13 ;
CS137_ACTIVITY            (idx, 1)        =  4.66885E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.72988E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.16543E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.55838E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.40643E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.77017E+12 0.00054  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 19 ;
BURNUP                     (idx, [1:  2])  = [  3.50000E+01  3.50034E+01 ];
BURN_DAYS                 (idx, 1)        =  8.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  8.45724E-01 0.00113 ];
U235_FISS                 (idx, [1:   4]) = [  2.78628E+15 0.00185  2.05339E-01 0.00172 ];
U238_FISS                 (idx, [1:   4]) = [  1.30817E+15 0.00267  9.63951E-02 0.00248 ];
PU239_FISS                (idx, [1:   4]) = [  7.23035E+15 0.00108  5.32847E-01 0.00081 ];
PU240_FISS                (idx, [1:   4]) = [  1.96910E+13 0.02233  1.45088E-03 0.02229 ];
PU241_FISS                (idx, [1:   4]) = [  2.17770E+15 0.00209  1.60481E-01 0.00192 ];
U235_CAPT                 (idx, [1:   4]) = [  6.18860E+14 0.00388  1.83227E-02 0.00383 ];
U238_CAPT                 (idx, [1:   4]) = [  1.13760E+16 0.00108  3.36792E-01 0.00076 ];
PU239_CAPT                (idx, [1:   4]) = [  3.95247E+15 0.00143  1.17030E-01 0.00141 ];
PU240_CAPT                (idx, [1:   4]) = [  3.35041E+15 0.00165  9.91987E-02 0.00157 ];
PU241_CAPT                (idx, [1:   4]) = [  7.85433E+14 0.00346  2.32560E-02 0.00345 ];
XE135_CAPT                (idx, [1:   4]) = [  8.11823E+14 0.00352  2.40375E-02 0.00352 ];
SM149_CAPT                (idx, [1:   4]) = [  2.66823E+14 0.00587  7.90089E-03 0.00588 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001013 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.37572E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001013 5.00838E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3534971 3.54032E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1420400 1.42242E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45642 4.56418E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001013 5.00838E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.74975E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.78049E+16 2.3E-05  3.78049E+16 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35592E+16 5.0E-06  1.35592E+16 5.0E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.37676E+16 0.00044  2.62938E+16 0.00045  7.47381E+15 0.00123 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.73268E+16 0.00032  3.98530E+16 0.00029  7.47381E+15 0.00123 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.77017E+16 0.00054  4.77017E+16 0.00054  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.23920E+18 0.00052  5.93238E+17 0.00048  1.64596E+18 0.00059 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.35467E+14 0.00511 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.77623E+16 0.00032 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.68088E+18 0.00069 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.08463E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.08463E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.37079E+00 0.00070 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.69620E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.16865E-01 0.00037 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23006E+00 0.00041 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94067E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96786E-01 2.8E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.00422E-01 0.00074 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  7.93116E-01 0.00074 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.78813E+00 2.8E-05 ];
FISSE                     (idx, [1:   2]) = [  2.07169E+02 5.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  7.93109E-01 0.00075  7.89360E-01 0.00075  3.75653E-03 0.01207 ];
IMP_KEFF                  (idx, [1:   2]) = [  7.92885E-01 0.00033 ];
COL_KEFF                  (idx, [1:   2]) = [  7.92644E-01 0.00055 ];
ABS_KEFF                  (idx, [1:   2]) = [  7.92885E-01 0.00033 ];
ABS_KINF                  (idx, [1:   2]) = [  8.00191E-01 0.00032 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.68550E+01 0.00027 ];
IMP_ALF                   (idx, [1:   2]) = [  1.68530E+01 0.00011 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  9.62051E-07 0.00454 ];
IMP_EALF                  (idx, [1:   2]) = [  9.59840E-07 0.00179 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.27900E-01 0.00283 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.27769E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.10882E-03 0.00610  1.37925E-04 0.04575  1.16380E-03 0.01460  9.42640E-04 0.01556  2.54867E-03 0.00978  1.02472E-03 0.01545  2.91056E-04 0.03133 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.29119E-01 0.01607  8.15587E-03 0.03317  3.04033E-02 0.00039  1.11729E-01 0.00065  3.23552E-01 0.00040  1.23132E+00 0.00268  6.94503E+00 0.02027 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.65946E-03 0.00987  9.85192E-05 0.06962  8.73333E-04 0.02334  7.48763E-04 0.02589  1.94966E-03 0.01579  7.74258E-04 0.02473  2.14927E-04 0.04780 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.09163E-01 0.02393  1.26296E-02 0.00150  3.03929E-02 0.00057  1.11693E-01 0.00093  3.23921E-01 0.00064  1.23317E+00 0.00385  7.59685E+00 0.01659 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.32741E-05 0.00166  3.32614E-05 0.00165  3.60008E-05 0.02109 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.63824E-05 0.00146  2.63722E-05 0.00146  2.85469E-05 0.02102 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.74374E-03 0.01231  9.94401E-05 0.08588  9.28229E-04 0.02787  7.04577E-04 0.03163  1.98811E-03 0.01965  7.91039E-04 0.02833  2.32338E-04 0.05424 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.34241E-01 0.02923  1.26262E-02 0.00258  3.03842E-02 0.00077  1.11649E-01 0.00132  3.23165E-01 0.00081  1.23492E+00 0.00530  7.53260E+00 0.02449 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.24494E-05 0.00388  3.24441E-05 0.00389  2.84857E-05 0.04963 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.57291E-05 0.00382  2.57250E-05 0.00383  2.25826E-05 0.04966 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.89133E-03 0.04375  6.75704E-05 0.29917  9.80889E-04 0.09093  7.06435E-04 0.10654  2.07793E-03 0.06521  8.49708E-04 0.09909  2.08806E-04 0.19211 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.15397E-01 0.09477  1.26148E-02 0.00712  3.03045E-02 0.00164  1.11862E-01 0.00310  3.22473E-01 0.00222  1.22324E+00 0.01293  7.59134E+00 0.06385 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.88771E-03 0.04267  7.76189E-05 0.29452  9.64264E-04 0.08992  6.99739E-04 0.10488  2.08788E-03 0.06316  8.57725E-04 0.09493  2.00485E-04 0.18397 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.07365E-01 0.09421  1.26148E-02 0.00712  3.03058E-02 0.00164  1.11893E-01 0.00309  3.22524E-01 0.00221  1.22271E+00 0.01292  7.58954E+00 0.06384 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.52232E+02 0.04448 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.28887E-05 0.00099 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.60769E-05 0.00064 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.84536E-03 0.00798 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.47467E+02 0.00819 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.79362E-07 0.00075 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.61094E-06 0.00040  2.61078E-06 0.00040  2.64548E-06 0.00640 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.09342E-05 0.00090  4.09505E-05 0.00090  3.75764E-05 0.01195 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.14909E-01 0.00036  6.15623E-01 0.00037  5.07385E-01 0.00972 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08940E+01 0.01487 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.51841E+01 0.00050  3.35909E+01 0.00063 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.21978E+04 0.00357  3.07519E+05 0.00177  6.11163E+05 0.00102  6.49582E+05 0.00050  5.98072E+05 0.00061  6.41231E+05 0.00066  4.35405E+05 0.00067  3.85534E+05 0.00056  2.95373E+05 0.00059  2.41102E+05 0.00058  2.08135E+05 0.00074  1.87095E+05 0.00075  1.73304E+05 0.00082  1.64739E+05 0.00089  1.60274E+05 0.00078  1.38591E+05 0.00105  1.36640E+05 0.00090  1.35865E+05 0.00065  1.33478E+05 0.00091  2.60295E+05 0.00037  2.52161E+05 0.00072  1.81691E+05 0.00077  1.17912E+05 0.00120  1.35924E+05 0.00084  1.29771E+05 0.00093  1.14847E+05 0.00089  1.85107E+05 0.00066  4.28639E+04 0.00176  5.22690E+04 0.00143  4.78078E+04 0.00118  2.81545E+04 0.00244  4.84989E+04 0.00189  3.14912E+04 0.00188  2.52991E+04 0.00165  4.17385E+03 0.00402  3.51928E+03 0.00314  3.07724E+03 0.00411  2.98431E+03 0.00404  3.01560E+03 0.00463  3.28004E+03 0.00436  3.95878E+03 0.00331  4.13196E+03 0.00285  8.32156E+03 0.00310  1.40066E+04 0.00261  1.83296E+04 0.00197  4.94951E+04 0.00125  5.19370E+04 0.00190  5.58720E+04 0.00164  3.65776E+04 0.00160  2.56777E+04 0.00167  1.91297E+04 0.00185  2.25526E+04 0.00172  4.43922E+04 0.00165  6.33778E+04 0.00158  1.27824E+05 0.00168  2.04412E+05 0.00162  3.12819E+05 0.00170  2.03066E+05 0.00165  1.46692E+05 0.00172  1.05832E+05 0.00186  9.55672E+04 0.00175  9.44080E+04 0.00194  7.90835E+04 0.00181  5.36148E+04 0.00179  4.95723E+04 0.00194  4.42400E+04 0.00201  3.74935E+04 0.00175  2.96802E+04 0.00190  1.99370E+04 0.00151  7.08930E+03 0.00217 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  7.99946E-01 0.00057 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.77872E+18 0.00057  4.60526E+17 0.00173 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.40116E-01 0.00012  1.56383E+00 0.00049 ];
INF_CAPT                  (idx, [1:   4]) = [  8.80566E-03 0.00069  3.93192E-02 0.00107 ];
INF_ABS                   (idx, [1:   4]) = [  1.02366E-02 0.00059  6.32411E-02 0.00135 ];
INF_FISS                  (idx, [1:   4]) = [  1.43096E-03 0.00050  2.39218E-02 0.00181 ];
INF_NSF                   (idx, [1:   4]) = [  3.97973E-03 0.00052  6.67361E-02 0.00186 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.78115E+00 7.5E-05  2.78975E+00 7.2E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06815E+02 1.0E-05  2.07251E+02 1.4E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.46968E-08 0.00038  2.60598E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29890E-01 0.00012  1.50051E+00 0.00056 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44281E-01 0.00015  3.96134E-01 0.00062 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64154E-02 0.00025  9.31353E-02 0.00101 ];
INF_SCATT3                (idx, [1:   4]) = [  7.24809E-03 0.00270  2.79146E-02 0.00275 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03797E-02 0.00174 -9.03707E-03 0.00766 ];
INF_SCATT5                (idx, [1:   4]) = [  1.43801E-04 0.10610  6.63259E-03 0.00668 ];
INF_SCATT6                (idx, [1:   4]) = [  5.13017E-03 0.00292 -1.72041E-02 0.00316 ];
INF_SCATT7                (idx, [1:   4]) = [  7.76879E-04 0.01960  5.92058E-04 0.07112 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29935E-01 0.00012  1.50051E+00 0.00056 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44282E-01 0.00015  3.96134E-01 0.00062 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64157E-02 0.00025  9.31353E-02 0.00101 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.24809E-03 0.00270  2.79146E-02 0.00275 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03796E-02 0.00174 -9.03707E-03 0.00766 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.43571E-04 0.10599  6.63259E-03 0.00668 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.13024E-03 0.00293 -1.72041E-02 0.00316 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.76929E-04 0.01959  5.92058E-04 0.07112 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12500E-01 0.00041  1.01976E+00 0.00041 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56863E+00 0.00041  3.26877E-01 0.00041 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.01917E-02 0.00059  6.32411E-02 0.00135 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69867E-02 0.00021  6.44078E-02 0.00127 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.13129E-01 0.00012  1.67606E-02 0.00057  1.09256E-03 0.00367  1.49942E+00 0.00056 ];
INF_S1                    (idx, [1:   8]) = [  2.39453E-01 0.00013  4.82840E-03 0.00123  4.77181E-04 0.00595  3.95657E-01 0.00062 ];
INF_S2                    (idx, [1:   8]) = [  9.79626E-02 0.00024 -1.54723E-03 0.00186  2.57031E-04 0.00852  9.28783E-02 0.00101 ];
INF_S3                    (idx, [1:   8]) = [  8.98701E-03 0.00223 -1.73892E-03 0.00235  9.21506E-05 0.02198  2.78225E-02 0.00278 ];
INF_S4                    (idx, [1:   8]) = [ -9.85106E-03 0.00190 -5.28663E-04 0.00621  1.42371E-06 1.00000 -9.03850E-03 0.00771 ];
INF_S5                    (idx, [1:   8]) = [  9.60319E-05 0.16352  4.77695E-05 0.04237 -3.75091E-05 0.04449  6.67010E-03 0.00665 ];
INF_S6                    (idx, [1:   8]) = [  5.25922E-03 0.00294 -1.29046E-04 0.01963 -4.83309E-05 0.03452 -1.71558E-02 0.00315 ];
INF_S7                    (idx, [1:   8]) = [  9.41067E-04 0.01576 -1.64188E-04 0.01272 -4.40135E-05 0.03247  6.36071E-04 0.06634 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.13174E-01 0.00012  1.67606E-02 0.00057  1.09256E-03 0.00367  1.49942E+00 0.00056 ];
INF_SP1                   (idx, [1:   8]) = [  2.39453E-01 0.00013  4.82840E-03 0.00123  4.77181E-04 0.00595  3.95657E-01 0.00062 ];
INF_SP2                   (idx, [1:   8]) = [  9.79629E-02 0.00024 -1.54723E-03 0.00186  2.57031E-04 0.00852  9.28783E-02 0.00101 ];
INF_SP3                   (idx, [1:   8]) = [  8.98701E-03 0.00223 -1.73892E-03 0.00235  9.21506E-05 0.02198  2.78225E-02 0.00278 ];
INF_SP4                   (idx, [1:   8]) = [ -9.85090E-03 0.00190 -5.28663E-04 0.00621  1.42371E-06 1.00000 -9.03850E-03 0.00771 ];
INF_SP5                   (idx, [1:   8]) = [  9.58011E-05 0.16349  4.77695E-05 0.04237 -3.75091E-05 0.04449  6.67010E-03 0.00665 ];
INF_SP6                   (idx, [1:   8]) = [  5.25929E-03 0.00294 -1.29046E-04 0.01963 -4.83309E-05 0.03452 -1.71558E-02 0.00315 ];
INF_SP7                   (idx, [1:   8]) = [  9.41117E-04 0.01574 -1.64188E-04 0.01272 -4.40135E-05 0.03247  6.36071E-04 0.06634 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31690E-01 0.00073  1.23009E+00 0.00765 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33316E-01 0.00095  1.34646E+00 0.00878 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33325E-01 0.00126  1.33882E+00 0.01192 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28506E-01 0.00106  1.05562E+00 0.00803 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43872E+00 0.00072  2.71361E-01 0.00760 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42871E+00 0.00094  2.48015E-01 0.00868 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42868E+00 0.00127  2.49806E-01 0.01167 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45879E+00 0.00106  3.16260E-01 0.00807 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.65946E-03 0.00987  9.85192E-05 0.06962  8.73333E-04 0.02334  7.48763E-04 0.02589  1.94966E-03 0.01579  7.74258E-04 0.02473  2.14927E-04 0.04780 ];
LAMBDA                    (idx, [1:  14]) = [  7.09163E-01 0.02393  1.26296E-02 0.00150  3.03929E-02 0.00057  1.11693E-01 0.00093  3.23921E-01 0.00064  1.23317E+00 0.00385  7.59685E+00 0.01659 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0000' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:31 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:49:50 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284831 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00177E+00  9.97790E-01  1.00271E+00  1.00515E+00  9.92591E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.1E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.17624E-02 0.00112  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88238E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.03553E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.03986E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69417E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.49305E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.49216E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.51317E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.28921E-01 0.00111  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001396 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00028E+04 0.00101 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00028E+04 0.00101 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  6.44139E+02 ;
RUNNING_TIME              (idx, 1)        =  1.29313E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.22850E-01  4.22850E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  4.03100E-01  1.18500E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.27327E+02  3.39287E+00  2.62885E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.14728E+00  2.93667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.91833E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.29313E+02  1.29313E+02 ];
CPU_USAGE                 (idx, 1)        = 4.98124 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00047E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.82466E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  1.01164E+17 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.70401E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.21701E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.42326E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.71831E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.69312E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.53213E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  8.52863E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.44872E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  7.54780E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.15237E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  9.80828E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.29635E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.23447E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.52904E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.69313E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.75641E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.00693E+14 ;
CS137_ACTIVITY            (idx, 1)        =  5.32051E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.84940E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.14960E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.32417E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.43457E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.92208E+12 0.00053  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 20 ;
BURNUP                     (idx, [1:  2])  = [  4.00000E+01  4.00040E+01 ];
BURN_DAYS                 (idx, 1)        =  1.00000E+03 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  8.79541E-01 0.00114 ];
U235_FISS                 (idx, [1:   4]) = [  2.20392E+15 0.00210  1.62985E-01 0.00199 ];
U238_FISS                 (idx, [1:   4]) = [  1.34455E+15 0.00268  9.94179E-02 0.00247 ];
PU239_FISS                (idx, [1:   4]) = [  7.47401E+15 0.00108  5.52715E-01 0.00081 ];
PU240_FISS                (idx, [1:   4]) = [  2.17490E+13 0.02136  1.60770E-03 0.02130 ];
PU241_FISS                (idx, [1:   4]) = [  2.41476E+15 0.00198  1.78571E-01 0.00182 ];
U235_CAPT                 (idx, [1:   4]) = [  4.91440E+14 0.00451  1.39074E-02 0.00449 ];
U238_CAPT                 (idx, [1:   4]) = [  1.16926E+16 0.00113  3.30852E-01 0.00078 ];
PU239_CAPT                (idx, [1:   4]) = [  4.09551E+15 0.00144  1.15904E-01 0.00143 ];
PU240_CAPT                (idx, [1:   4]) = [  3.60416E+15 0.00183  1.01984E-01 0.00166 ];
PU241_CAPT                (idx, [1:   4]) = [  8.66516E+14 0.00321  2.45227E-02 0.00320 ];
XE135_CAPT                (idx, [1:   4]) = [  8.24722E+14 0.00348  2.33397E-02 0.00348 ];
SM149_CAPT                (idx, [1:   4]) = [  2.69563E+14 0.00595  7.62834E-03 0.00593 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001396 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.39614E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001396 5.00840E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3584628 3.58976E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1371895 1.37374E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44873 4.48903E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001396 5.00840E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.37722E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.80208E+16 2.3E-05  3.80208E+16 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35402E+16 4.7E-06  1.35402E+16 4.7E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.53060E+16 0.00044  2.77252E+16 0.00047  7.58084E+15 0.00125 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.88463E+16 0.00032  4.12654E+16 0.00031  7.58084E+15 0.00125 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.92208E+16 0.00053  4.92208E+16 0.00053  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.30307E+18 0.00050  6.12312E+17 0.00050  1.69076E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.41967E+14 0.00514 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.92883E+16 0.00032 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.72140E+18 0.00065 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.07887E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.07887E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.34267E+00 0.00066 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.71637E-01 0.00036 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.10277E-01 0.00038 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23128E+00 0.00040 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94145E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96858E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  7.78447E-01 0.00072 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  7.71458E-01 0.00073 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.80799E+00 2.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.07459E+02 4.7E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  7.71327E-01 0.00072  7.67906E-01 0.00073  3.55240E-03 0.01233 ];
IMP_KEFF                  (idx, [1:   2]) = [  7.72737E-01 0.00033 ];
COL_KEFF                  (idx, [1:   2]) = [  7.72561E-01 0.00053 ];
ABS_KEFF                  (idx, [1:   2]) = [  7.72737E-01 0.00033 ];
ABS_KINF                  (idx, [1:   2]) = [  7.79739E-01 0.00032 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.67992E+01 0.00027 ];
IMP_ALF                   (idx, [1:   2]) = [  1.68009E+01 0.00011 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  1.01731E-06 0.00451 ];
IMP_EALF                  (idx, [1:   2]) = [  1.01125E-06 0.00187 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.37455E-01 0.00274 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.37671E-01 0.00119 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.14129E-03 0.00676  1.37175E-04 0.04318  1.17969E-03 0.01442  9.36141E-04 0.01653  2.53133E-03 0.00981  1.07104E-03 0.01586  2.85924E-04 0.03122 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.22845E-01 0.01604  8.50579E-03 0.03146  3.03302E-02 0.00039  1.11749E-01 0.00067  3.23816E-01 0.00044  1.21501E+00 0.00345  6.62636E+00 0.02258 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.57548E-03 0.01032  1.10448E-04 0.06627  8.85321E-04 0.02376  6.91890E-04 0.02647  1.89039E-03 0.01608  7.80640E-04 0.02416  2.16784E-04 0.04819 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.31756E-01 0.02558  1.27098E-02 0.00177  3.03126E-02 0.00055  1.11647E-01 0.00095  3.23627E-01 0.00069  1.20654E+00 0.00431  7.67255E+00 0.01678 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.39723E-05 0.00151  3.39545E-05 0.00151  3.78410E-05 0.02044 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.61971E-05 0.00134  2.61833E-05 0.00134  2.91842E-05 0.02044 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.60049E-03 0.01240  9.69719E-05 0.08165  8.85193E-04 0.03064  6.96659E-04 0.03282  1.91237E-03 0.01951  7.98779E-04 0.02849  2.10521E-04 0.05873 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.28028E-01 0.03229  1.27284E-02 0.00313  3.03075E-02 0.00075  1.11778E-01 0.00139  3.23101E-01 0.00090  1.21026E+00 0.00597  7.57250E+00 0.02617 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.32198E-05 0.00408  3.32009E-05 0.00409  3.09532E-05 0.05500 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.56143E-05 0.00398  2.55997E-05 0.00398  2.38939E-05 0.05502 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.47300E-03 0.04406  6.90844E-05 0.35759  8.09493E-04 0.11264  6.67039E-04 0.12074  1.82175E-03 0.06804  8.05919E-04 0.10178  2.99719E-04 0.17608 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.97322E-01 0.09345  1.27271E-02 0.00992  3.02786E-02 0.00172  1.12414E-01 0.00346  3.23030E-01 0.00240  1.19184E+00 0.01474  7.24561E+00 0.06091 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.48733E-03 0.04336  6.39845E-05 0.34604  8.33975E-04 0.10800  6.46132E-04 0.11826  1.84601E-03 0.06727  8.01742E-04 0.09929  2.95487E-04 0.17044 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.86015E-01 0.09292  1.27271E-02 0.00992  3.02807E-02 0.00173  1.12394E-01 0.00346  3.23072E-01 0.00240  1.19145E+00 0.01468  7.21022E+00 0.06127 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.35871E+02 0.04413 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.35831E-05 0.00109 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.58967E-05 0.00080 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.56288E-03 0.00770 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.35995E+02 0.00784 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.72359E-07 0.00085 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.59939E-06 0.00045  2.59929E-06 0.00045  2.62488E-06 0.00651 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.07165E-05 0.00099  4.07355E-05 0.00099  3.66317E-05 0.01168 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.08425E-01 0.00038  6.09228E-01 0.00039  4.89127E-01 0.01053 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.09716E+01 0.01530 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.49216E+01 0.00053  3.34317E+01 0.00061 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.25605E+04 0.00314  3.07638E+05 0.00123  6.10946E+05 0.00102  6.49534E+05 0.00060  5.98003E+05 0.00082  6.42238E+05 0.00079  4.36230E+05 0.00058  3.85936E+05 0.00050  2.95281E+05 0.00081  2.41287E+05 0.00090  2.08262E+05 0.00080  1.87811E+05 0.00066  1.73430E+05 0.00085  1.64844E+05 0.00101  1.60568E+05 0.00080  1.38851E+05 0.00115  1.37150E+05 0.00085  1.35909E+05 0.00103  1.33619E+05 0.00099  2.60397E+05 0.00055  2.52291E+05 0.00084  1.81810E+05 0.00075  1.17934E+05 0.00094  1.35456E+05 0.00099  1.29619E+05 0.00082  1.14843E+05 0.00097  1.84423E+05 0.00097  4.27734E+04 0.00116  5.18830E+04 0.00125  4.76232E+04 0.00143  2.81342E+04 0.00188  4.83461E+04 0.00166  3.12448E+04 0.00179  2.49424E+04 0.00175  4.08268E+03 0.00420  3.39817E+03 0.00515  2.99952E+03 0.00337  2.92495E+03 0.00336  2.95159E+03 0.00374  3.18332E+03 0.00442  3.86202E+03 0.00377  4.04355E+03 0.00374  8.18741E+03 0.00251  1.38359E+04 0.00239  1.81606E+04 0.00248  4.88116E+04 0.00149  5.13339E+04 0.00121  5.53993E+04 0.00129  3.60438E+04 0.00134  2.53785E+04 0.00189  1.88346E+04 0.00134  2.22158E+04 0.00237  4.36152E+04 0.00180  6.22238E+04 0.00147  1.25620E+05 0.00185  2.00918E+05 0.00163  3.07540E+05 0.00190  1.99630E+05 0.00192  1.44307E+05 0.00209  1.04140E+05 0.00203  9.40518E+04 0.00222  9.29428E+04 0.00220  7.78504E+04 0.00227  5.28516E+04 0.00244  4.89339E+04 0.00216  4.36338E+04 0.00224  3.68429E+04 0.00229  2.91907E+04 0.00221  1.96188E+04 0.00220  6.95143E+03 0.00269 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  7.79560E-01 0.00043 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.83551E+18 0.00041  4.67612E+17 0.00165 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.40240E-01 0.00011  1.56077E+00 0.00048 ];
INF_CAPT                  (idx, [1:   4]) = [  9.00920E-03 0.00058  4.01449E-02 0.00099 ];
INF_ABS                   (idx, [1:   4]) = [  1.03994E-02 0.00049  6.36494E-02 0.00121 ];
INF_FISS                  (idx, [1:   4]) = [  1.39021E-03 0.00057  2.35045E-02 0.00161 ];
INF_NSF                   (idx, [1:   4]) = [  3.88922E-03 0.00059  6.60574E-02 0.00167 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.79757E+00 9.1E-05  2.81041E+00 7.4E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.07040E+02 1.0E-05  2.07557E+02 1.5E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.43602E-08 0.00038  2.60581E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29837E-01 0.00012  1.49710E+00 0.00056 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43961E-01 0.00020  3.94914E-01 0.00062 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63033E-02 0.00037  9.28981E-02 0.00108 ];
INF_SCATT3                (idx, [1:   4]) = [  7.23963E-03 0.00326  2.78657E-02 0.00229 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03568E-02 0.00184 -8.93506E-03 0.00578 ];
INF_SCATT5                (idx, [1:   4]) = [  1.59478E-04 0.09796  6.72080E-03 0.00636 ];
INF_SCATT6                (idx, [1:   4]) = [  5.14413E-03 0.00268 -1.72338E-02 0.00304 ];
INF_SCATT7                (idx, [1:   4]) = [  7.72832E-04 0.02095  5.35177E-04 0.08640 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29883E-01 0.00012  1.49710E+00 0.00056 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43962E-01 0.00020  3.94914E-01 0.00062 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63037E-02 0.00037  9.28981E-02 0.00108 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.23959E-03 0.00327  2.78657E-02 0.00229 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03570E-02 0.00184 -8.93506E-03 0.00578 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.59698E-04 0.09797  6.72080E-03 0.00636 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.14416E-03 0.00268 -1.72338E-02 0.00304 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.73006E-04 0.02097  5.35177E-04 0.08640 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12738E-01 0.00038  1.01895E+00 0.00042 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56688E+00 0.00038  3.27136E-01 0.00042 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.03544E-02 0.00050  6.36494E-02 0.00121 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69859E-02 0.00022  6.47710E-02 0.00146 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.13254E-01 0.00011  1.65839E-02 0.00047  1.10677E-03 0.00477  1.49600E+00 0.00056 ];
INF_S1                    (idx, [1:   8]) = [  2.39190E-01 0.00019  4.77137E-03 0.00092  4.75823E-04 0.00932  3.94438E-01 0.00063 ];
INF_S2                    (idx, [1:   8]) = [  9.78354E-02 0.00037 -1.53203E-03 0.00203  2.61887E-04 0.01044  9.26362E-02 0.00109 ];
INF_S3                    (idx, [1:   8]) = [  8.96101E-03 0.00267 -1.72138E-03 0.00147  9.44018E-05 0.02287  2.77713E-02 0.00229 ];
INF_S4                    (idx, [1:   8]) = [ -9.83880E-03 0.00193 -5.17995E-04 0.00419  1.30973E-06 1.00000 -8.93637E-03 0.00581 ];
INF_S5                    (idx, [1:   8]) = [  1.07053E-04 0.13833  5.24252E-05 0.04398 -3.73645E-05 0.03547  6.75816E-03 0.00628 ];
INF_S6                    (idx, [1:   8]) = [  5.27414E-03 0.00266 -1.30011E-04 0.01690 -4.84380E-05 0.03446 -1.71854E-02 0.00303 ];
INF_S7                    (idx, [1:   8]) = [  9.36966E-04 0.01693 -1.64134E-04 0.01284 -4.35501E-05 0.03548  5.78727E-04 0.07949 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.13299E-01 0.00011  1.65839E-02 0.00047  1.10677E-03 0.00477  1.49600E+00 0.00056 ];
INF_SP1                   (idx, [1:   8]) = [  2.39191E-01 0.00019  4.77137E-03 0.00092  4.75823E-04 0.00932  3.94438E-01 0.00063 ];
INF_SP2                   (idx, [1:   8]) = [  9.78357E-02 0.00037 -1.53203E-03 0.00203  2.61887E-04 0.01044  9.26362E-02 0.00109 ];
INF_SP3                   (idx, [1:   8]) = [  8.96097E-03 0.00268 -1.72138E-03 0.00147  9.44018E-05 0.02287  2.77713E-02 0.00229 ];
INF_SP4                   (idx, [1:   8]) = [ -9.83905E-03 0.00192 -5.17995E-04 0.00419  1.30973E-06 1.00000 -8.93637E-03 0.00581 ];
INF_SP5                   (idx, [1:   8]) = [  1.07273E-04 0.13829  5.24252E-05 0.04398 -3.73645E-05 0.03547  6.75816E-03 0.00628 ];
INF_SP6                   (idx, [1:   8]) = [  5.27417E-03 0.00266 -1.30011E-04 0.01690 -4.84380E-05 0.03446 -1.71854E-02 0.00303 ];
INF_SP7                   (idx, [1:   8]) = [  9.37140E-04 0.01694 -1.64134E-04 0.01284 -4.35501E-05 0.03548  5.78727E-04 0.07949 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31992E-01 0.00059  1.22012E+00 0.00758 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33528E-01 0.00097  1.31871E+00 0.01050 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33564E-01 0.00084  1.33199E+00 0.00789 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28950E-01 0.00076  1.05413E+00 0.00765 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43685E+00 0.00059  2.73569E-01 0.00746 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42741E+00 0.00097  2.53424E-01 0.01024 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42718E+00 0.00084  2.50625E-01 0.00787 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45594E+00 0.00076  3.16657E-01 0.00759 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.57548E-03 0.01032  1.10448E-04 0.06627  8.85321E-04 0.02376  6.91890E-04 0.02647  1.89039E-03 0.01608  7.80640E-04 0.02416  2.16784E-04 0.04819 ];
LAMBDA                    (idx, [1:  14]) = [  7.31756E-01 0.02558  1.27098E-02 0.00177  3.03126E-02 0.00055  1.11647E-01 0.00095  3.23627E-01 0.00069  1.20654E+00 0.00431  7.67255E+00 0.01678 ];

