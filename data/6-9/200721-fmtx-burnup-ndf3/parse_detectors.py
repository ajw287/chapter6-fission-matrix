# Parses for detector data, k-eff and k-eff over burn up...
import copy
import numpy as np
import sys, os
import re
import data_store as ds
import pickle
import time

num_detector = 9

def main():
    detector_data  = [copy.deepcopy(np.empty([17,17])) for x in range(num_detector)]
    detector_error = [copy.deepcopy(np.empty([17,17])) for x in range(num_detector)]
    bu_steps_to_bu = [0.0, 0.1, 0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12.5, 15, 17.5, 20, 25, 30, 35, 40]
    k_eff = 0.0
    k_effs = []
    cycle_length_steps = 0.0
    cycle_length = 0.0
    cycle_length_bu = 0.0
    if len(sys.argv) == 2:
        detector_filename = sys.argv[1]+"_det0.m"
        main_out_filename = sys.argv[1]+"_res.m"
    else:
        detector_filename = "super_cell_test_det0.m"
        main_out_filename = "super_cell_test_res.m"

    try:
        file = open(detector_filename, 'r')
    except in_file_error:
        usage_error("detector file error: (check path and that it exists!)")
        quit()
    else:
        filedata = file.readlines()
        for i in range(1, num_detector+1):
            temp_data = copy.deepcopy(filedata)
            parsing = False
            for line in temp_data:
                if parsing == False:
                    if "DETa{:03d} = [".format(int(i)) in line:
                        #print("found " + "DETa{:03d} = [".format(int(i)))
                        parsing = True
                else:
                    if "];" in line:
                        parsing = False
                    else:
                        data  = [float(x) for x in re.findall(r"[+-]? *(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][+-]?\d+)?", line)]
                        #print("y:"+str(int(data[-4])-1) + " x:"+str(int(data[-3])-1) +" i:"+str(i-1))
                        (detector_data[i-1])[int(data[-4])-1][int(data[-3])-1]  = data[-2]
                        (detector_error[i-1])[int(data[-4])-1][int(data[-3])-1] = data[-1]
    try:
        file = open(main_out_filename, 'r')
    except in_file_error:
        usage_error("detector file error: (check path and that it exists!)")
        quit()
    else:
        filedata = file.readlines()
        temp_data = copy.deepcopy(filedata)
        for line in temp_data:
            if "ABS_KEFF" in line:
                #print("Success! I found a keff value")
                data = [float(x) for x in re.findall(r"[+-]? *(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][+-]?\d+)?", line)]
                #print("k_eff:"+str(float(data[-2])) + " error:"+str(float(data[-1])))
                k_effs.append(float(data[-2]))
        if(k_effs[-1] >=1.0):  # need to be robust to systems where the k_eff never goes below 1
            cycle_length_steps = len(k_effs)
            if(cycle_length_steps < 6):
                print("----------------- suspiciously short cycle lenght!!!! -----------------")
                time.sleep(1)
            cycle_length_bu = bu_steps_to_bu[cycle_length_steps]
            cycle_length = cycle_length_steps + ( (1-k_effs[cycle_length_steps-1]) / ( (1- k_effs[cycle_length_steps-1]) - (1-k_effs[cycle_length_steps])))
            print(str(cycle_length_steps) + "  " +str(cycle_length) + " " +str(k_effs[cycle_length_steps-1]) + " " + str(k_effs[cycle_length_steps]))
            cycle_length_bu = bu_steps_to_bu [cycle_length_steps] + ( (bu_steps_to_bu [cycle_length_steps+1] - bu_steps_to_bu [cycle_length_steps]) * (cycle_length - bu_steps_to_bu [cycle_length_steps]) )
        else:
            if(k_effs[0] < 1.0):
                cycle_length_steps = 0.0
                cycle_length = 0.0
            else:
                cycle_length_steps = next(i for i,v in enumerate(k_effs) if v < 1.0)
                print("cycle length: " + str(cycle_length_steps) + " length keffs:"+str(len(k_effs)))
                cycle_length = cycle_length_steps + ( (1-k_effs[cycle_length_steps-1]) / ( (1- k_effs[cycle_length_steps-1]) - (1-k_effs[cycle_length_steps])))
                print(str(cycle_length_steps) + "  " +str(cycle_length) + " " +str(k_effs[cycle_length_steps-1]) + " " + str(k_effs[cycle_length_steps]))
                cycle_length_bu = bu_steps_to_bu [cycle_length_steps] + ( (bu_steps_to_bu [cycle_length_steps+1] - bu_steps_to_bu [cycle_length_steps]) * (cycle_length - bu_steps_to_bu [cycle_length_steps]) )

        if(len(k_effs) > 0):
            k_eff = k_effs[0]
        #if k_eff == 0.0:
        #    print("error")
        #    time.sleep(1)
        #    exit()
    try:
#        print (sys.argv[1]+".pickle")
        pickle_file = open(sys.argv[1]+".pickle", "rb")
#        print("opened file")
        var = pickle.load(pickle_file)
#        print(var.enrichments)
    except:
        print("error opening '' pickle file")
        exit()
    else:
        var.detector_data = detector_data
        var.k_eff = k_eff
        var.k_effs = k_effs
        var.cycle_length_steps = cycle_length_steps
        var.cycle_length = cycle_length
        var.cycle_length_bu = cycle_length_bu
        pickle_file.close()
        try:
            f = open(sys.argv[1]+".pickle", "wb+")  # you should be creating state.pickle here...
        except:
            print ("Error opening output file")
            exit()
        else:
            f.write(pickle.dumps(var))
            f.close()


if __name__ == "__main__":
    main()
