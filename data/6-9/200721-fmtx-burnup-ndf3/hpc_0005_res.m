
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0005' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:52:32 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:56:26 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299952 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00832E+00  9.86726E-01  1.00250E+00  9.94275E-01  1.00818E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13839E-02 0.00106  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88616E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.96034E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.96461E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.71647E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.65094E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.65015E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.32766E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.38568E-01 0.00113  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000513 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00010E+04 0.00069 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00010E+04 0.00069 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.76517E+01 ;
RUNNING_TIME              (idx, 1)        =  3.90072E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  4.61967E-01  4.61967E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.33333E-03  1.33333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.43692E+00  3.43692E+00  0.00000E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.89972E+00  0.00000E+00 ];
CPU_USAGE                 (idx, 1)        = 4.52523 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00034E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  8.68661E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.64974E+08 ;
TOT_DECAY_HEAT            (idx, 1)        =  6.58887E-04 ;
TOT_SF_RATE               (idx, 1)        =  7.41545E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  8.64974E+08 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  6.58887E-04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  7.91454E+03 ;
INGESTION_TOXICITY        (idx, 1)        =  4.17808E+01 ;
ACTINIDE_INH_TOX          (idx, 1)        =  7.91454E+03 ;
ACTINIDE_ING_TOX          (idx, 1)        =  4.17808E+01 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  1.11735E+08 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  8.64586E+08 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  3.55881E+08 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.96447E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 0 ;
BURNUP                     (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
BURN_DAYS                 (idx, 1)        =  0.00000E+00 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.88443E-01 0.00105 ];
U235_FISS                 (idx, [1:   4]) = [  1.30520E+16 0.00057  9.40753E-01 0.00016 ];
U238_FISS                 (idx, [1:   4]) = [  8.20657E+14 0.00271  5.91472E-02 0.00261 ];
U235_CAPT                 (idx, [1:   4]) = [  2.80576E+15 0.00146  1.80096E-01 0.00133 ];
U238_CAPT                 (idx, [1:   4]) = [  7.69088E+15 0.00099  4.93635E-01 0.00064 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000513 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.21393E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000513 5.00721E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2624085 2.62771E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2337079 2.34015E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 39349 3.93546E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000513 5.00721E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.93601E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.41059E+16 1.0E-05  3.41059E+16 1.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38683E+16 1.2E-06  1.38683E+16 1.2E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.55779E+16 0.00046  1.06084E+16 0.00051  4.96950E+15 0.00097 ];
TOT_ABSRATE               (idx, [1:   6]) = [  2.94462E+16 0.00024  2.44767E+16 0.00022  4.96950E+15 0.00097 ];
TOT_SRCRATE               (idx, [1:   6]) = [  2.96447E+16 0.00050  2.96447E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.43350E+18 0.00046  3.87485E+17 0.00046  1.04601E+18 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.33343E+14 0.00494 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  2.96795E+16 0.00025 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.08349E+18 0.00056 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12514E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.79621E+00 0.00038 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.73702E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.74646E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23746E+00 0.00032 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94909E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97206E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.16015E+00 0.00047 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.15102E+00 0.00047 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.45928E+00 1.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02551E+02 1.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.15121E+00 0.00049  1.14292E+00 0.00047  8.09904E-03 0.00776 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.15084E+00 0.00024 ];
COL_KEFF                  (idx, [1:   2]) = [  1.15063E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.15084E+00 0.00024 ];
ABS_KINF                  (idx, [1:   2]) = [  1.15997E+00 0.00024 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75948E+01 0.00018 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75916E+01 7.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.57864E-07 0.00311 ];
IMP_EALF                  (idx, [1:   2]) = [  4.58443E-07 0.00136 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.00906E-01 0.00284 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.00903E-01 0.00110 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.17853E-03 0.00555  1.92668E-04 0.03045  9.68313E-04 0.01360  9.42400E-04 0.01305  2.84584E-03 0.00800  9.22958E-04 0.01367  3.06343E-04 0.02283 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.12401E-01 0.01231  1.11418E-02 0.01558  3.16586E-02 0.00022  1.10166E-01 0.00027  3.20440E-01 0.00020  1.34566E+00 0.00016  8.66180E+00 0.00719 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.08516E-03 0.00754  2.16108E-04 0.04554  1.11852E-03 0.01901  1.11802E-03 0.02029  3.22539E-03 0.01163  1.06739E-03 0.02020  3.39739E-04 0.03357 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.97813E-01 0.01768  1.24908E-02 2.8E-06  3.16557E-02 0.00031  1.10139E-01 0.00038  3.20337E-01 0.00032  1.34573E+00 0.00023  8.87255E+00 0.00192 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.55933E-05 0.00108  2.55792E-05 0.00108  2.75848E-05 0.01092 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.94595E-05 0.00095  2.94433E-05 0.00095  3.17452E-05 0.01086 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.02243E-03 0.00783  2.23807E-04 0.04631  1.11918E-03 0.01858  1.08587E-03 0.01945  3.18714E-03 0.01165  1.06546E-03 0.02017  3.40971E-04 0.03570 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.03136E-01 0.01859  1.24908E-02 3.2E-06  3.16613E-02 0.00034  1.10205E-01 0.00045  3.20408E-01 0.00032  1.34587E+00 0.00023  8.90075E+00 0.00242 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.53784E-05 0.00235  2.53659E-05 0.00235  2.68028E-05 0.02733 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.92118E-05 0.00228  2.91973E-05 0.00228  3.08572E-05 0.02740 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.79349E-03 0.02427  2.36441E-04 0.13795  1.02282E-03 0.06459  1.02322E-03 0.06045  3.08865E-03 0.03598  1.03958E-03 0.06538  3.82771E-04 0.10720 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.52051E-01 0.05586  1.24908E-02 7.5E-06  3.16616E-02 0.00078  1.10192E-01 0.00106  3.20626E-01 0.00096  1.34603E+00 0.00059  8.88953E+00 0.00500 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.75764E-03 0.02341  2.30488E-04 0.13854  1.04867E-03 0.06206  1.02952E-03 0.05788  3.06103E-03 0.03551  1.01233E-03 0.06328  3.75600E-04 0.10038 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.53214E-01 0.05500  1.24908E-02 7.5E-06  3.16615E-02 0.00077  1.10175E-01 0.00103  3.20687E-01 0.00096  1.34611E+00 0.00058  8.88810E+00 0.00499 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.68371E+02 0.02431 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.54563E-05 0.00068 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.93019E-05 0.00045 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.96208E-03 0.00433 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.73543E+02 0.00437 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.24591E-07 0.00062 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87698E-06 0.00044  2.87684E-06 0.00044  2.89756E-06 0.00490 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.16654E-05 0.00078  4.16856E-05 0.00079  3.89491E-05 0.00810 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.72550E-01 0.00033  6.71797E-01 0.00032  8.06963E-01 0.00859 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03506E+01 0.01326 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.65015E+01 0.00045  3.57593E+01 0.00043 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.16540E+04 0.00309  2.91383E+05 0.00128  6.03172E+05 0.00103  6.49662E+05 0.00085  5.99412E+05 0.00065  6.43084E+05 0.00070  4.36158E+05 0.00073  3.86861E+05 0.00094  2.95646E+05 0.00070  2.41485E+05 0.00078  2.08371E+05 0.00097  1.87647E+05 0.00072  1.73552E+05 0.00085  1.64527E+05 0.00115  1.60693E+05 0.00073  1.38817E+05 0.00101  1.36805E+05 0.00091  1.35710E+05 0.00086  1.33284E+05 0.00097  2.60186E+05 0.00059  2.50744E+05 0.00059  1.81021E+05 0.00089  1.17036E+05 0.00109  1.35237E+05 0.00094  1.27566E+05 0.00105  1.16060E+05 0.00110  1.90871E+05 0.00070  4.37183E+04 0.00154  5.46757E+04 0.00141  4.97153E+04 0.00165  2.88832E+04 0.00205  5.01971E+04 0.00158  3.41404E+04 0.00174  2.90796E+04 0.00208  5.52555E+03 0.00405  5.50671E+03 0.00393  5.61659E+03 0.00452  5.81867E+03 0.00394  5.69562E+03 0.00403  5.66153E+03 0.00401  5.87807E+03 0.00487  5.51340E+03 0.00496  1.04499E+04 0.00294  1.66206E+04 0.00216  2.11508E+04 0.00192  5.58261E+04 0.00117  5.89278E+04 0.00125  6.46422E+04 0.00116  4.44974E+04 0.00151  3.36274E+04 0.00130  2.59770E+04 0.00155  3.08814E+04 0.00110  5.89077E+04 0.00155  8.00817E+04 0.00103  1.54283E+05 0.00112  2.37602E+05 0.00097  3.54199E+05 0.00094  2.26093E+05 0.00094  1.61355E+05 0.00091  1.15775E+05 0.00099  1.03958E+05 0.00104  1.02046E+05 0.00107  8.51431E+04 0.00119  5.74506E+04 0.00117  5.30241E+04 0.00113  4.71417E+04 0.00134  3.98167E+04 0.00118  3.13464E+04 0.00135  2.09408E+04 0.00159  7.38844E+03 0.00178 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.15976E+00 0.00040 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.10817E+18 0.00040  3.25367E+17 0.00082 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37680E-01 0.00011  1.49889E+00 0.00035 ];
INF_CAPT                  (idx, [1:   4]) = [  6.37052E-03 0.00063  2.61833E-02 0.00023 ];
INF_ABS                   (idx, [1:   4]) = [  8.68284E-03 0.00049  6.09371E-02 0.00058 ];
INF_FISS                  (idx, [1:   4]) = [  2.31232E-03 0.00042  3.47537E-02 0.00085 ];
INF_NSF                   (idx, [1:   4]) = [  5.91702E-03 0.00040  8.46844E-02 0.00085 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.55891E+00 4.0E-05  2.43670E+00 2.7E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03792E+02 5.4E-06  2.02270E+02 2.7E-09 ];
INF_INVV                  (idx, [1:   4]) = [  5.89217E-08 0.00050  2.55133E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29006E-01 0.00012  1.43791E+00 0.00039 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43825E-01 0.00018  3.80058E-01 0.00042 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61419E-02 0.00030  9.01502E-02 0.00098 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32684E-03 0.00358  2.70838E-02 0.00242 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03221E-02 0.00218 -8.28883E-03 0.00615 ];
INF_SCATT5                (idx, [1:   4]) = [  1.24979E-04 0.13040  6.34326E-03 0.00714 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07058E-03 0.00280 -1.61452E-02 0.00286 ];
INF_SCATT7                (idx, [1:   4]) = [  7.35316E-04 0.02247  3.96643E-04 0.12908 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29045E-01 0.00012  1.43791E+00 0.00039 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43826E-01 0.00018  3.80058E-01 0.00042 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61420E-02 0.00030  9.01502E-02 0.00098 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32727E-03 0.00358  2.70838E-02 0.00242 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03221E-02 0.00218 -8.28883E-03 0.00615 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.25079E-04 0.13023  6.34326E-03 0.00714 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07070E-03 0.00279 -1.61452E-02 0.00286 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.35276E-04 0.02247  3.96643E-04 0.12908 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13662E-01 0.00028  9.66938E-01 0.00034 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56010E+00 0.00028  3.44732E-01 0.00034 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.64425E-03 0.00050  6.09371E-02 0.00058 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69738E-02 0.00022  6.20755E-02 0.00065 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10706E-01 0.00011  1.83001E-02 0.00035  1.09054E-03 0.00455  1.43681E+00 0.00039 ];
INF_S1                    (idx, [1:   8]) = [  2.38466E-01 0.00018  5.35943E-03 0.00084  4.65676E-04 0.00717  3.79592E-01 0.00042 ];
INF_S2                    (idx, [1:   8]) = [  9.77116E-02 0.00030 -1.56972E-03 0.00300  2.53273E-04 0.00879  8.98970E-02 0.00098 ];
INF_S3                    (idx, [1:   8]) = [  9.20650E-03 0.00285 -1.87966E-03 0.00168  8.85760E-05 0.02272  2.69952E-02 0.00242 ];
INF_S4                    (idx, [1:   8]) = [ -9.69289E-03 0.00227 -6.29170E-04 0.00450  2.31754E-06 0.58950 -8.29115E-03 0.00618 ];
INF_S5                    (idx, [1:   8]) = [  1.12238E-04 0.14833  1.27411E-05 0.18678 -3.43440E-05 0.04209  6.37761E-03 0.00699 ];
INF_S6                    (idx, [1:   8]) = [  5.21773E-03 0.00278 -1.47145E-04 0.01416 -4.68767E-05 0.02477 -1.60983E-02 0.00289 ];
INF_S7                    (idx, [1:   8]) = [  9.07925E-04 0.01708 -1.72609E-04 0.01432 -4.29570E-05 0.02484  4.39600E-04 0.11618 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10745E-01 0.00011  1.83001E-02 0.00035  1.09054E-03 0.00455  1.43681E+00 0.00039 ];
INF_SP1                   (idx, [1:   8]) = [  2.38466E-01 0.00018  5.35943E-03 0.00084  4.65676E-04 0.00717  3.79592E-01 0.00042 ];
INF_SP2                   (idx, [1:   8]) = [  9.77118E-02 0.00030 -1.56972E-03 0.00300  2.53273E-04 0.00879  8.98970E-02 0.00098 ];
INF_SP3                   (idx, [1:   8]) = [  9.20692E-03 0.00285 -1.87966E-03 0.00168  8.85760E-05 0.02272  2.69952E-02 0.00242 ];
INF_SP4                   (idx, [1:   8]) = [ -9.69294E-03 0.00227 -6.29170E-04 0.00450  2.31754E-06 0.58950 -8.29115E-03 0.00618 ];
INF_SP5                   (idx, [1:   8]) = [  1.12338E-04 0.14810  1.27411E-05 0.18678 -3.43440E-05 0.04209  6.37761E-03 0.00699 ];
INF_SP6                   (idx, [1:   8]) = [  5.21784E-03 0.00278 -1.47145E-04 0.01416 -4.68767E-05 0.02477 -1.60983E-02 0.00289 ];
INF_SP7                   (idx, [1:   8]) = [  9.07885E-04 0.01707 -1.72609E-04 0.01432 -4.29570E-05 0.02484  4.39600E-04 0.11618 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32413E-01 0.00062  1.07793E+00 0.00601 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34194E-01 0.00096  1.14549E+00 0.00922 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33845E-01 0.00082  1.15002E+00 0.00822 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29272E-01 0.00090  9.62338E-01 0.00502 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43424E+00 0.00062  3.09501E-01 0.00595 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42336E+00 0.00096  2.91602E-01 0.00939 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42547E+00 0.00082  2.90314E-01 0.00809 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45391E+00 0.00090  3.46588E-01 0.00501 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.08516E-03 0.00754  2.16108E-04 0.04554  1.11852E-03 0.01901  1.11802E-03 0.02029  3.22539E-03 0.01163  1.06739E-03 0.02020  3.39739E-04 0.03357 ];
LAMBDA                    (idx, [1:  14]) = [  7.97813E-01 0.01768  1.24908E-02 2.8E-06  3.16557E-02 0.00031  1.10139E-01 0.00038  3.20337E-01 0.00032  1.34573E+00 0.00023  8.87255E+00 0.00192 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0005' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:52:32 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:02:21 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299952 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00088E+00  9.91378E-01  1.00156E+00  1.00384E+00  1.00234E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 9.3E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13392E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88661E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.97789E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.98215E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.70885E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.62261E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.62183E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.15534E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.31553E-01 0.00112  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000371 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00007E+04 0.00075 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00007E+04 0.00075 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.71949E+01 ;
RUNNING_TIME              (idx, 1)        =  9.81462E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  4.61967E-01  4.61967E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.41167E-02  6.36667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.28625E+00  3.28988E+00  2.55945E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.13000E-02  2.68833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.21666E-03  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.81447E+00  1.25009E+02 ];
CPU_USAGE                 (idx, 1)        = 4.80864 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99956E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.38962E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  7.87232E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.75484E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.41529E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.18637E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  8.41239E+02 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  6.68593E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.67070E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.21439E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  1.74876E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.29865E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  3.44203E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.84530E+06 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  1.40456E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.28369E+11 ;
TE132_ACTIVITY            (idx, 1)        =  2.51427E+14 ;
I131_ACTIVITY             (idx, 1)        =  7.42376E+13 ;
I132_ACTIVITY             (idx, 1)        =  2.43593E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.58890E+07 ;
CS137_ACTIVITY            (idx, 1)        =  1.34682E+11 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  6.55885E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.64935E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.30771E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  9.02550E+16 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.07033E+12 0.00047  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 1 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E-01  1.00008E-01 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+00 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.00527E-01 0.00112 ];
U235_FISS                 (idx, [1:   4]) = [  1.30060E+16 0.00059  9.37091E-01 0.00017 ];
U238_FISS                 (idx, [1:   4]) = [  8.48459E+14 0.00273  6.11260E-02 0.00260 ];
PU239_FISS                (idx, [1:   4]) = [  2.27456E+13 0.01663  1.63885E-03 0.01665 ];
U235_CAPT                 (idx, [1:   4]) = [  2.80462E+15 0.00146  1.68703E-01 0.00139 ];
U238_CAPT                 (idx, [1:   4]) = [  7.87438E+15 0.00102  4.73614E-01 0.00068 ];
PU239_CAPT                (idx, [1:   4]) = [  1.22742E+13 0.02182  7.38535E-04 0.02185 ];
PU240_CAPT                (idx, [1:   4]) = [  1.95978E+11 0.17123  1.17967E-05 0.17123 ];
XE135_CAPT                (idx, [1:   4]) = [  7.12372E+14 0.00277  4.28504E-02 0.00273 ];
SM149_CAPT                (idx, [1:   4]) = [  1.99690E+13 0.01792  1.20081E-03 0.01789 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000371 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.17772E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000371 5.00718E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2703808 2.70744E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2257128 2.26030E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 39435 3.94413E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000371 5.00718E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -8.38190E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.41227E+16 1.1E-05  3.41227E+16 1.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38670E+16 1.3E-06  1.38670E+16 1.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.66406E+16 0.00047  1.15757E+16 0.00046  5.06487E+15 0.00101 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.05076E+16 0.00026  2.54427E+16 0.00021  5.06487E+15 0.00101 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.07033E+16 0.00047  3.07033E+16 0.00047  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.47696E+18 0.00043  3.98752E+17 0.00043  1.07821E+18 0.00048 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.42227E+14 0.00526 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.07498E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.11349E+18 0.00057 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12503E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12503E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.71204E+00 0.00042 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.78416E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.74444E-01 0.00030 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24753E+00 0.00032 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94894E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97203E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.12124E+00 0.00050 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.11240E+00 0.00051 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46071E+00 1.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02570E+02 1.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.11259E+00 0.00052  1.10469E+00 0.00051  7.70813E-03 0.00771 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.11133E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.11149E+00 0.00047 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.11133E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.12016E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75213E+01 0.00018 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75208E+01 7.6E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.92786E-07 0.00311 ];
IMP_EALF                  (idx, [1:   2]) = [  4.92087E-07 0.00133 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.07569E-01 0.00277 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.07426E-01 0.00114 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.40603E-03 0.00568  1.83917E-04 0.03148  9.99004E-04 0.01386  9.89092E-04 0.01346  2.96016E-03 0.00779  9.59311E-04 0.01381  3.14553E-04 0.02338 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.07617E-01 0.01200  1.08170E-02 0.01761  3.16525E-02 0.00023  1.10147E-01 0.00027  3.20511E-01 0.00022  1.34560E+00 0.00016  8.60261E+00 0.00803 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.06226E-03 0.00793  2.10879E-04 0.04463  1.09421E-03 0.02037  1.08200E-03 0.01909  3.25699E-03 0.01175  1.04019E-03 0.02063  3.77989E-04 0.03398 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.42323E-01 0.01847  1.24908E-02 2.9E-06  3.16525E-02 0.00031  1.10121E-01 0.00036  3.20558E-01 0.00032  1.34570E+00 0.00023  8.86875E+00 0.00194 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.54388E-05 0.00112  2.54311E-05 0.00113  2.65641E-05 0.01121 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.82995E-05 0.00101  2.82907E-05 0.00101  2.95598E-05 0.01126 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.91828E-03 0.00765  1.98981E-04 0.04627  1.08731E-03 0.01953  1.08332E-03 0.01971  3.17954E-03 0.01126  1.01461E-03 0.02008  3.54508E-04 0.03518 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.22307E-01 0.01891  1.24908E-02 3.5E-06  3.16518E-02 0.00033  1.10108E-01 0.00042  3.20496E-01 0.00036  1.34546E+00 0.00026  8.89023E+00 0.00246 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.51803E-05 0.00267  2.51659E-05 0.00269  2.72141E-05 0.02916 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.80118E-05 0.00262  2.79958E-05 0.00264  3.02743E-05 0.02914 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.59903E-03 0.02571  2.15046E-04 0.15225  1.13161E-03 0.06257  9.98351E-04 0.06449  3.00632E-03 0.03961  9.42655E-04 0.06543  3.05041E-04 0.11303 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.08583E-01 0.06232  1.24908E-02 8.4E-06  3.16326E-02 0.00085  1.10389E-01 0.00119  3.20425E-01 0.00099  1.34441E+00 0.00066  8.95537E+00 0.00581 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.56953E-03 0.02550  2.10551E-04 0.14707  1.11213E-03 0.06120  1.00082E-03 0.06341  2.98800E-03 0.03899  9.45625E-04 0.06332  3.12394E-04 0.11222 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.05336E-01 0.06052  1.24908E-02 8.4E-06  3.16293E-02 0.00084  1.10389E-01 0.00118  3.20391E-01 0.00097  1.34442E+00 0.00065  8.94905E+00 0.00575 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.62846E+02 0.02579 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.52801E-05 0.00070 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.81228E-05 0.00048 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.86547E-03 0.00466 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.71649E+02 0.00473 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.14560E-07 0.00062 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87344E-06 0.00044  2.87324E-06 0.00045  2.89969E-06 0.00463 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.07243E-05 0.00078  4.07506E-05 0.00079  3.72248E-05 0.00828 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.72334E-01 0.00030  6.71715E-01 0.00031  7.82124E-01 0.00909 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  9.98298E+00 0.01290 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.62183E+01 0.00044  3.51667E+01 0.00043 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.12704E+04 0.00299  2.90576E+05 0.00153  6.02008E+05 0.00090  6.49743E+05 0.00072  5.98275E+05 0.00086  6.42632E+05 0.00082  4.36824E+05 0.00060  3.86807E+05 0.00061  2.95752E+05 0.00075  2.41645E+05 0.00078  2.08107E+05 0.00090  1.87832E+05 0.00088  1.73080E+05 0.00089  1.64825E+05 0.00099  1.60425E+05 0.00106  1.38748E+05 0.00075  1.36819E+05 0.00103  1.35705E+05 0.00103  1.33385E+05 0.00104  2.60193E+05 0.00051  2.50560E+05 0.00066  1.80991E+05 0.00069  1.16927E+05 0.00089  1.34958E+05 0.00058  1.27718E+05 0.00104  1.16035E+05 0.00103  1.90797E+05 0.00076  4.35638E+04 0.00146  5.48384E+04 0.00141  4.97040E+04 0.00164  2.88197E+04 0.00252  5.00868E+04 0.00177  3.40337E+04 0.00202  2.90311E+04 0.00175  5.55412E+03 0.00403  5.48691E+03 0.00353  5.63517E+03 0.00255  5.80637E+03 0.00514  5.75488E+03 0.00414  5.67969E+03 0.00345  5.88712E+03 0.00345  5.47512E+03 0.00256  1.03731E+04 0.00266  1.65805E+04 0.00252  2.11262E+04 0.00167  5.58145E+04 0.00125  5.88808E+04 0.00130  6.45272E+04 0.00127  4.41610E+04 0.00146  3.32820E+04 0.00143  2.57765E+04 0.00179  3.05331E+04 0.00166  5.81778E+04 0.00116  7.87873E+04 0.00097  1.51667E+05 0.00098  2.32749E+05 0.00118  3.45953E+05 0.00101  2.19986E+05 0.00111  1.57340E+05 0.00115  1.12845E+05 0.00108  1.01241E+05 0.00104  9.95746E+04 0.00136  8.28821E+04 0.00102  5.59719E+04 0.00134  5.16803E+04 0.00140  4.60092E+04 0.00131  3.88875E+04 0.00136  3.06151E+04 0.00150  2.04602E+04 0.00153  7.20881E+03 0.00220 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.12033E+00 0.00040 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.14706E+18 0.00037  3.29937E+17 0.00094 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37863E-01 0.00011  1.50500E+00 0.00032 ];
INF_CAPT                  (idx, [1:   4]) = [  6.37519E-03 0.00065  2.82741E-02 0.00032 ];
INF_ABS                   (idx, [1:   4]) = [  8.68137E-03 0.00052  6.22909E-02 0.00067 ];
INF_FISS                  (idx, [1:   4]) = [  2.30618E-03 0.00052  3.40168E-02 0.00096 ];
INF_NSF                   (idx, [1:   4]) = [  5.90178E-03 0.00053  8.29166E-02 0.00096 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.55912E+00 5.2E-05  2.43752E+00 3.3E-07 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03796E+02 5.0E-06  2.02281E+02 5.3E-08 ];
INF_INVV                  (idx, [1:   4]) = [  5.89114E-08 0.00038  2.54643E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29180E-01 0.00012  1.44273E+00 0.00035 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43919E-01 0.00021  3.82027E-01 0.00040 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61964E-02 0.00032  9.08331E-02 0.00086 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35717E-03 0.00268  2.73004E-02 0.00224 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03162E-02 0.00165 -8.43219E-03 0.00647 ];
INF_SCATT5                (idx, [1:   4]) = [  1.47098E-04 0.12078  6.18867E-03 0.00702 ];
INF_SCATT6                (idx, [1:   4]) = [  5.06399E-03 0.00369 -1.62806E-02 0.00299 ];
INF_SCATT7                (idx, [1:   4]) = [  7.38307E-04 0.02672  3.61086E-04 0.09115 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29218E-01 0.00012  1.44273E+00 0.00035 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43920E-01 0.00021  3.82027E-01 0.00040 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61969E-02 0.00032  9.08331E-02 0.00086 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35717E-03 0.00268  2.73004E-02 0.00224 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03164E-02 0.00166 -8.43219E-03 0.00647 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.47150E-04 0.12090  6.18867E-03 0.00702 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.06398E-03 0.00369 -1.62806E-02 0.00299 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.38525E-04 0.02678  3.61086E-04 0.09115 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13828E-01 0.00032  9.68962E-01 0.00029 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55889E+00 0.00032  3.44011E-01 0.00029 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.64294E-03 0.00052  6.22909E-02 0.00067 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69882E-02 0.00016  6.33787E-02 0.00073 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10875E-01 0.00011  1.83043E-02 0.00034  1.11048E-03 0.00371  1.44162E+00 0.00035 ];
INF_S1                    (idx, [1:   8]) = [  2.38568E-01 0.00022  5.35103E-03 0.00083  4.70833E-04 0.00612  3.81556E-01 0.00039 ];
INF_S2                    (idx, [1:   8]) = [  9.77749E-02 0.00030 -1.57848E-03 0.00214  2.56918E-04 0.01023  9.05762E-02 0.00086 ];
INF_S3                    (idx, [1:   8]) = [  9.23735E-03 0.00207 -1.88018E-03 0.00179  9.01915E-05 0.02733  2.72102E-02 0.00222 ];
INF_S4                    (idx, [1:   8]) = [ -9.68785E-03 0.00173 -6.28370E-04 0.00478  6.78155E-07 1.00000 -8.43287E-03 0.00649 ];
INF_S5                    (idx, [1:   8]) = [  1.29274E-04 0.13522  1.78242E-05 0.14292 -3.80551E-05 0.03251  6.22672E-03 0.00694 ];
INF_S6                    (idx, [1:   8]) = [  5.20895E-03 0.00352 -1.44959E-04 0.01636 -4.75240E-05 0.02417 -1.62331E-02 0.00299 ];
INF_S7                    (idx, [1:   8]) = [  9.13241E-04 0.02171 -1.74934E-04 0.01122 -4.38048E-05 0.02837  4.04891E-04 0.08011 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10914E-01 0.00011  1.83043E-02 0.00034  1.11048E-03 0.00371  1.44162E+00 0.00035 ];
INF_SP1                   (idx, [1:   8]) = [  2.38569E-01 0.00022  5.35103E-03 0.00083  4.70833E-04 0.00612  3.81556E-01 0.00039 ];
INF_SP2                   (idx, [1:   8]) = [  9.77754E-02 0.00030 -1.57848E-03 0.00214  2.56918E-04 0.01023  9.05762E-02 0.00086 ];
INF_SP3                   (idx, [1:   8]) = [  9.23735E-03 0.00206 -1.88018E-03 0.00179  9.01915E-05 0.02733  2.72102E-02 0.00222 ];
INF_SP4                   (idx, [1:   8]) = [ -9.68803E-03 0.00173 -6.28370E-04 0.00478  6.78155E-07 1.00000 -8.43287E-03 0.00649 ];
INF_SP5                   (idx, [1:   8]) = [  1.29326E-04 0.13537  1.78242E-05 0.14292 -3.80551E-05 0.03251  6.22672E-03 0.00694 ];
INF_SP6                   (idx, [1:   8]) = [  5.20894E-03 0.00352 -1.44959E-04 0.01636 -4.75240E-05 0.02417 -1.62331E-02 0.00299 ];
INF_SP7                   (idx, [1:   8]) = [  9.13459E-04 0.02176 -1.74934E-04 0.01122 -4.38048E-05 0.02837  4.04891E-04 0.08011 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32646E-01 0.00063  1.07375E+00 0.00590 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34029E-01 0.00085  1.14725E+00 0.00692 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34330E-01 0.00085  1.13846E+00 0.00727 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29647E-01 0.00109  9.58560E-01 0.00607 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43280E+00 0.00063  3.10692E-01 0.00576 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42435E+00 0.00085  2.90880E-01 0.00685 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42252E+00 0.00085  2.93147E-01 0.00693 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45154E+00 0.00109  3.48051E-01 0.00606 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.06226E-03 0.00793  2.10879E-04 0.04463  1.09421E-03 0.02037  1.08200E-03 0.01909  3.25699E-03 0.01175  1.04019E-03 0.02063  3.77989E-04 0.03398 ];
LAMBDA                    (idx, [1:  14]) = [  8.42323E-01 0.01847  1.24908E-02 2.9E-06  3.16525E-02 0.00031  1.10121E-01 0.00036  3.20558E-01 0.00032  1.34570E+00 0.00023  8.86875E+00 0.00194 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0005' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:52:32 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:08:16 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299952 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00016E+00  9.91608E-01  1.00081E+00  1.00424E+00  1.00318E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13621E-02 0.00104  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88638E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.98198E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.98626E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.70631E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.60801E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.60722E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.09515E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.30202E-01 0.00109  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000999 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00020E+04 0.00076 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00020E+04 0.00076 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  7.67851E+01 ;
RUNNING_TIME              (idx, 1)        =  1.57382E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.61967E-01  4.61967E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.99000E-02  7.81667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.51397E+01  3.27572E+00  2.57777E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.05033E-01  2.68667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.91667E-03  8.50002E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.57380E+01  1.22604E+02 ];
CPU_USAGE                 (idx, 1)        = 4.87891 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99941E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.56383E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.70849E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.83390E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.42812E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.56689E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.09494E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.14158E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.72438E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.65825E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  2.99957E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  8.66224E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.39441E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  1.79203E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  2.36013E+07 ;
SR90_ACTIVITY             (idx, 1)        =  6.38230E+11 ;
TE132_ACTIVITY            (idx, 1)        =  5.63218E+14 ;
I131_ACTIVITY             (idx, 1)        =  2.65304E+14 ;
I132_ACTIVITY             (idx, 1)        =  5.66054E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.66128E+09 ;
CS137_ACTIVITY            (idx, 1)        =  6.73889E+11 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.63198E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.63878E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  6.63957E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.11148E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.10212E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 2 ;
BURNUP                     (idx, [1:  2])  = [  5.00000E-01  5.00038E-01 ];
BURN_DAYS                 (idx, 1)        =  1.25000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.02513E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  1.27085E+16 0.00057  9.17202E-01 0.00020 ];
U238_FISS                 (idx, [1:   4]) = [  8.62249E+14 0.00283  6.22180E-02 0.00263 ];
PU239_FISS                (idx, [1:   4]) = [  2.82701E+14 0.00455  2.04033E-02 0.00453 ];
PU240_FISS                (idx, [1:   4]) = [  1.22795E+10 0.70645  8.88301E-07 0.70644 ];
PU241_FISS                (idx, [1:   4]) = [  1.11054E+11 0.23170  8.04643E-06 0.23170 ];
U235_CAPT                 (idx, [1:   4]) = [  2.75753E+15 0.00149  1.62556E-01 0.00140 ];
U238_CAPT                 (idx, [1:   4]) = [  7.93090E+15 0.00101  4.67485E-01 0.00065 ];
PU239_CAPT                (idx, [1:   4]) = [  1.56477E+14 0.00637  9.22443E-03 0.00637 ];
PU240_CAPT                (idx, [1:   4]) = [  6.86680E+12 0.03089  4.04545E-04 0.03083 ];
PU241_CAPT                (idx, [1:   4]) = [  3.07429E+10 0.44557  1.82949E-06 0.44554 ];
XE135_CAPT                (idx, [1:   4]) = [  7.14258E+14 0.00280  4.21079E-02 0.00280 ];
SM149_CAPT                (idx, [1:   4]) = [  1.21458E+14 0.00720  7.16156E-03 0.00724 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000999 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.35314E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000999 5.00735E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2730750 2.73430E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2230593 2.23339E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 39656 3.96669E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000999 5.00735E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.12227E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.42206E+16 1.2E-05  3.42206E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38596E+16 1.4E-06  1.38596E+16 1.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.69711E+16 0.00047  1.18971E+16 0.00048  5.07398E+15 0.00104 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.08307E+16 0.00026  2.57567E+16 0.00022  5.07398E+15 0.00104 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.10212E+16 0.00049  3.10212E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.48907E+18 0.00046  4.02044E+17 0.00044  1.08703E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.46138E+14 0.00514 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.10768E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.12049E+18 0.00059 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12456E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12456E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.69357E+00 0.00040 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.80061E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.74290E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24812E+00 0.00033 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94834E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97218E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.11176E+00 0.00050 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.10294E+00 0.00050 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46909E+00 1.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02678E+02 1.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.10311E+00 0.00052  1.09521E+00 0.00051  7.72988E-03 0.00791 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.10282E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.10327E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.10282E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.11163E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74935E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74963E+01 8.1E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.06815E-07 0.00328 ];
IMP_EALF                  (idx, [1:   2]) = [  5.04323E-07 0.00142 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.11024E-01 0.00300 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.10393E-01 0.00119 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.40951E-03 0.00554  1.78821E-04 0.03112  1.03566E-03 0.01392  9.95104E-04 0.01334  2.94624E-03 0.00816  9.37500E-04 0.01416  3.16178E-04 0.02395 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.03687E-01 0.01241  1.07169E-02 0.01821  3.16351E-02 0.00023  1.10230E-01 0.00030  3.20749E-01 0.00022  1.34532E+00 0.00017  8.54561E+00 0.00905 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.04249E-03 0.00820  1.93972E-04 0.04454  1.16886E-03 0.01986  1.11168E-03 0.02014  3.21559E-03 0.01226  1.01474E-03 0.02041  3.37633E-04 0.03490 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.90776E-01 0.01842  1.24906E-02 4.5E-06  3.16314E-02 0.00033  1.10252E-01 0.00043  3.20718E-01 0.00031  1.34588E+00 0.00022  8.89106E+00 0.00206 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.52662E-05 0.00123  2.52559E-05 0.00123  2.67579E-05 0.01184 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.78667E-05 0.00105  2.78554E-05 0.00105  2.95093E-05 0.01180 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.00523E-03 0.00795  1.94584E-04 0.04814  1.13800E-03 0.02115  1.07954E-03 0.01998  3.22129E-03 0.01221  1.03473E-03 0.02057  3.37087E-04 0.03609 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.97638E-01 0.01876  1.24907E-02 4.2E-06  3.16263E-02 0.00037  1.10223E-01 0.00049  3.20827E-01 0.00035  1.34539E+00 0.00025  8.86411E+00 0.00234 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.50589E-05 0.00249  2.50427E-05 0.00250  2.72628E-05 0.02818 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.76399E-05 0.00246  2.76220E-05 0.00247  3.00666E-05 0.02820 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.09172E-03 0.02611  2.08845E-04 0.14635  1.10346E-03 0.06795  1.13848E-03 0.06097  3.15178E-03 0.04037  1.04803E-03 0.06361  4.41116E-04 0.10659 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  9.53225E-01 0.06331  1.24907E-02 6.4E-06  3.16154E-02 0.00092  1.10259E-01 0.00109  3.20935E-01 0.00105  1.34541E+00 0.00062  8.82599E+00 0.00453 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.07716E-03 0.02530  2.09928E-04 0.14508  1.08356E-03 0.06696  1.13970E-03 0.05956  3.13319E-03 0.03874  1.06358E-03 0.06238  4.47195E-04 0.10632 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  9.49626E-01 0.06226  1.24907E-02 6.4E-06  3.16131E-02 0.00092  1.10252E-01 0.00107  3.20907E-01 0.00103  1.34542E+00 0.00061  8.82545E+00 0.00451 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.83652E+02 0.02627 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.51490E-05 0.00072 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.77384E-05 0.00048 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.08043E-03 0.00504 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.81609E+02 0.00509 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.09919E-07 0.00065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87377E-06 0.00044  2.87356E-06 0.00044  2.90225E-06 0.00499 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.03112E-05 0.00081  4.03373E-05 0.00081  3.67072E-05 0.00865 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.72135E-01 0.00031  6.71563E-01 0.00031  7.74211E-01 0.00911 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04098E+01 0.01262 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.60722E+01 0.00045  3.49297E+01 0.00044 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.21564E+04 0.00334  2.90534E+05 0.00153  6.03333E+05 0.00101  6.50531E+05 0.00088  5.99071E+05 0.00072  6.42986E+05 0.00067  4.36575E+05 0.00066  3.86335E+05 0.00076  2.95702E+05 0.00075  2.41199E+05 0.00074  2.08071E+05 0.00068  1.87612E+05 0.00088  1.73252E+05 0.00075  1.64781E+05 0.00099  1.60339E+05 0.00086  1.38469E+05 0.00104  1.36953E+05 0.00100  1.35611E+05 0.00082  1.33312E+05 0.00083  2.59922E+05 0.00073  2.50353E+05 0.00073  1.80818E+05 0.00090  1.17019E+05 0.00084  1.35250E+05 0.00076  1.27724E+05 0.00084  1.15997E+05 0.00087  1.90873E+05 0.00072  4.36368E+04 0.00163  5.45874E+04 0.00188  4.95687E+04 0.00149  2.88033E+04 0.00182  5.01487E+04 0.00152  3.41257E+04 0.00139  2.89672E+04 0.00198  5.52805E+03 0.00454  5.49159E+03 0.00395  5.61800E+03 0.00399  5.79726E+03 0.00310  5.73703E+03 0.00346  5.66399E+03 0.00264  5.85996E+03 0.00378  5.52116E+03 0.00335  1.04116E+04 0.00307  1.66421E+04 0.00284  2.11197E+04 0.00227  5.57522E+04 0.00165  5.87687E+04 0.00106  6.42820E+04 0.00124  4.40214E+04 0.00139  3.29969E+04 0.00153  2.53912E+04 0.00172  3.00939E+04 0.00123  5.73308E+04 0.00131  7.78603E+04 0.00114  1.49757E+05 0.00091  2.30189E+05 0.00087  3.41761E+05 0.00113  2.17746E+05 0.00106  1.55562E+05 0.00098  1.11620E+05 0.00134  1.00213E+05 0.00107  9.85986E+04 0.00135  8.21221E+04 0.00122  5.54992E+04 0.00134  5.12137E+04 0.00129  4.55473E+04 0.00168  3.84555E+04 0.00148  3.02960E+04 0.00146  2.02785E+04 0.00149  7.14675E+03 0.00172 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.11209E+00 0.00046 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.15923E+18 0.00049  3.29873E+17 0.00105 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37714E-01 0.00012  1.50720E+00 0.00032 ];
INF_CAPT                  (idx, [1:   4]) = [  6.39565E-03 0.00059  2.89745E-02 0.00038 ];
INF_ABS                   (idx, [1:   4]) = [  8.68572E-03 0.00050  6.29476E-02 0.00076 ];
INF_FISS                  (idx, [1:   4]) = [  2.29006E-03 0.00051  3.39731E-02 0.00109 ];
INF_NSF                   (idx, [1:   4]) = [  5.86925E-03 0.00051  8.31275E-02 0.00109 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.56292E+00 5.8E-05  2.44686E+00 2.9E-06 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03841E+02 6.0E-06  2.02403E+02 4.6E-07 ];
INF_INVV                  (idx, [1:   4]) = [  5.88808E-08 0.00037  2.54647E-06 0.00012 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29027E-01 0.00013  1.44429E+00 0.00037 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43831E-01 0.00017  3.82641E-01 0.00042 ];
INF_SCATT2                (idx, [1:   4]) = [  9.60988E-02 0.00025  9.10995E-02 0.00073 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32315E-03 0.00276  2.74523E-02 0.00224 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03031E-02 0.00209 -8.28224E-03 0.00589 ];
INF_SCATT5                (idx, [1:   4]) = [  1.48094E-04 0.13709  6.32678E-03 0.00758 ];
INF_SCATT6                (idx, [1:   4]) = [  5.06503E-03 0.00278 -1.61590E-02 0.00294 ];
INF_SCATT7                (idx, [1:   4]) = [  7.25513E-04 0.01894  3.35715E-04 0.14980 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29067E-01 0.00013  1.44429E+00 0.00037 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43832E-01 0.00017  3.82641E-01 0.00042 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.60993E-02 0.00025  9.10995E-02 0.00073 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32302E-03 0.00276  2.74523E-02 0.00224 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03031E-02 0.00209 -8.28224E-03 0.00589 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.48095E-04 0.13700  6.32678E-03 0.00758 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.06529E-03 0.00277 -1.61590E-02 0.00294 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.25534E-04 0.01898  3.35715E-04 0.14980 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13715E-01 0.00027  9.70826E-01 0.00027 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55971E+00 0.00027  3.43351E-01 0.00027 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.64636E-03 0.00051  6.29476E-02 0.00076 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69799E-02 0.00024  6.40274E-02 0.00081 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10734E-01 0.00012  1.82936E-02 0.00037  1.11919E-03 0.00490  1.44317E+00 0.00037 ];
INF_S1                    (idx, [1:   8]) = [  2.38478E-01 0.00018  5.35287E-03 0.00096  4.78646E-04 0.00746  3.82162E-01 0.00042 ];
INF_S2                    (idx, [1:   8]) = [  9.76793E-02 0.00024 -1.58055E-03 0.00313  2.58789E-04 0.00930  9.08408E-02 0.00073 ];
INF_S3                    (idx, [1:   8]) = [  9.20653E-03 0.00214 -1.88339E-03 0.00162  9.12556E-05 0.01473  2.73610E-02 0.00224 ];
INF_S4                    (idx, [1:   8]) = [ -9.67806E-03 0.00212 -6.25082E-04 0.00537 -3.09410E-07 1.00000 -8.28193E-03 0.00591 ];
INF_S5                    (idx, [1:   8]) = [  1.26561E-04 0.16056  2.15327E-05 0.11150 -3.90794E-05 0.03650  6.36586E-03 0.00755 ];
INF_S6                    (idx, [1:   8]) = [  5.20890E-03 0.00255 -1.43874E-04 0.01592 -4.94467E-05 0.01983 -1.61096E-02 0.00296 ];
INF_S7                    (idx, [1:   8]) = [  9.00184E-04 0.01502 -1.74670E-04 0.01424 -4.40061E-05 0.03105  3.79721E-04 0.13282 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10773E-01 0.00012  1.82936E-02 0.00037  1.11919E-03 0.00490  1.44317E+00 0.00037 ];
INF_SP1                   (idx, [1:   8]) = [  2.38479E-01 0.00018  5.35287E-03 0.00096  4.78646E-04 0.00746  3.82162E-01 0.00042 ];
INF_SP2                   (idx, [1:   8]) = [  9.76798E-02 0.00024 -1.58055E-03 0.00313  2.58789E-04 0.00930  9.08408E-02 0.00073 ];
INF_SP3                   (idx, [1:   8]) = [  9.20641E-03 0.00214 -1.88339E-03 0.00162  9.12556E-05 0.01473  2.73610E-02 0.00224 ];
INF_SP4                   (idx, [1:   8]) = [ -9.67797E-03 0.00212 -6.25082E-04 0.00537 -3.09410E-07 1.00000 -8.28193E-03 0.00591 ];
INF_SP5                   (idx, [1:   8]) = [  1.26562E-04 0.16040  2.15327E-05 0.11150 -3.90794E-05 0.03650  6.36586E-03 0.00755 ];
INF_SP6                   (idx, [1:   8]) = [  5.20916E-03 0.00255 -1.43874E-04 0.01592 -4.94467E-05 0.01983 -1.61096E-02 0.00296 ];
INF_SP7                   (idx, [1:   8]) = [  9.00205E-04 0.01504 -1.74670E-04 0.01424 -4.40061E-05 0.03105  3.79721E-04 0.13282 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32499E-01 0.00051  1.06906E+00 0.00585 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34107E-01 0.00110  1.13971E+00 0.00671 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33980E-01 0.00104  1.13208E+00 0.00753 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29480E-01 0.00072  9.57482E-01 0.00697 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43371E+00 0.00051  3.12055E-01 0.00578 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42389E+00 0.00110  2.92789E-01 0.00673 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42466E+00 0.00103  2.94842E-01 0.00751 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45258E+00 0.00072  3.48534E-01 0.00685 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.04249E-03 0.00820  1.93972E-04 0.04454  1.16886E-03 0.01986  1.11168E-03 0.02014  3.21559E-03 0.01226  1.01474E-03 0.02041  3.37633E-04 0.03490 ];
LAMBDA                    (idx, [1:  14]) = [  7.90776E-01 0.01842  1.24906E-02 4.5E-06  3.16314E-02 0.00033  1.10252E-01 0.00043  3.20718E-01 0.00031  1.34588E+00 0.00022  8.89106E+00 0.00206 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0005' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:52:32 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:14:15 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299952 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.66188E-01  1.00049E+00  1.00948E+00  1.01267E+00  1.01117E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.6E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13695E-02 0.00106  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88631E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.98743E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.99172E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.70404E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.58940E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.58861E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.01761E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.27654E-01 0.00107  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000816 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00016E+04 0.00074 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00016E+04 0.00074 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.06619E+02 ;
RUNNING_TIME              (idx, 1)        =  2.17109E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.61967E-01  4.61967E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  4.55333E-02  7.58334E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  2.10432E+01  3.32033E+00  2.58317E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.58017E-01  2.62667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  6.63334E-03  8.50002E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  2.17107E+01  1.22463E+02 ];
CPU_USAGE                 (idx, 1)        = 4.91084 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99912E+00 0.00021 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64142E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.91726E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.85078E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.47885E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.59695E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.11505E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.32029E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.73925E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  3.52610E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.47533E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  9.97179E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.60299E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  2.52892E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  2.81503E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.26546E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.04225E+14 ;
I131_ACTIVITY             (idx, 1)        =  3.59855E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.09307E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.58307E+10 ;
CS137_ACTIVITY            (idx, 1)        =  1.34860E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.83095E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.62045E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.47330E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.15048E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.13207E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 3 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E+00  1.00008E+00 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.03087E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  1.23587E+16 0.00059  8.91547E-01 0.00024 ];
U238_FISS                 (idx, [1:   4]) = [  8.69207E+14 0.00281  6.26945E-02 0.00265 ];
PU239_FISS                (idx, [1:   4]) = [  6.30728E+14 0.00322  4.54976E-02 0.00314 ];
PU240_FISS                (idx, [1:   4]) = [  4.38949E+10 0.37572  3.16476E-06 0.37576 ];
PU241_FISS                (idx, [1:   4]) = [  1.10294E+12 0.07591  7.95838E-05 0.07588 ];
U235_CAPT                 (idx, [1:   4]) = [  2.68426E+15 0.00143  1.55555E-01 0.00137 ];
U238_CAPT                 (idx, [1:   4]) = [  7.97678E+15 0.00103  4.62209E-01 0.00066 ];
PU239_CAPT                (idx, [1:   4]) = [  3.53347E+14 0.00421  2.04753E-02 0.00416 ];
PU240_CAPT                (idx, [1:   4]) = [  3.09423E+13 0.01492  1.79288E-03 0.01488 ];
PU241_CAPT                (idx, [1:   4]) = [  4.25934E+11 0.12528  2.46542E-05 0.12548 ];
XE135_CAPT                (idx, [1:   4]) = [  7.16448E+14 0.00289  4.15197E-02 0.00288 ];
SM149_CAPT                (idx, [1:   4]) = [  1.48612E+14 0.00672  8.61283E-03 0.00674 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000816 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.44259E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000816 5.00744E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2751254 2.75489E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2210061 2.21304E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 39501 3.95051E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000816 5.00744E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.84288E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.43511E+16 1.2E-05  3.43511E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38496E+16 1.5E-06  1.38496E+16 1.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.72444E+16 0.00047  1.21765E+16 0.00046  5.06789E+15 0.00102 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.10940E+16 0.00026  2.60261E+16 0.00022  5.06789E+15 0.00102 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.13207E+16 0.00051  3.13207E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.49934E+18 0.00047  4.04905E+17 0.00047  1.09444E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.47469E+14 0.00522 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.13415E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.12549E+18 0.00059 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12398E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12398E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.68369E+00 0.00040 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.82049E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.72855E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24909E+00 0.00033 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94894E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97191E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.10659E+00 0.00048 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.09785E+00 0.00049 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.48029E+00 1.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02824E+02 1.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.09782E+00 0.00051  1.09039E+00 0.00049  7.45790E-03 0.00850 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.09767E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.09690E+00 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.09767E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.10641E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74668E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74679E+01 8.4E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.20563E-07 0.00328 ];
IMP_EALF                  (idx, [1:   2]) = [  5.18888E-07 0.00147 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.13689E-01 0.00282 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.12757E-01 0.00117 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.32222E-03 0.00561  1.76629E-04 0.03293  9.82779E-04 0.01332  9.74788E-04 0.01375  2.94359E-03 0.00797  9.43830E-04 0.01463  3.00602E-04 0.02424 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.95500E-01 0.01250  1.02922E-02 0.02069  3.15949E-02 0.00026  1.10102E-01 0.00029  3.20783E-01 0.00022  1.34550E+00 0.00016  8.48294E+00 0.00996 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.81331E-03 0.00799  1.90080E-04 0.04671  1.04230E-03 0.02018  1.05475E-03 0.01929  3.16056E-03 0.01162  1.02891E-03 0.02100  3.36709E-04 0.03476 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.14667E-01 0.01843  1.24906E-02 5.3E-06  3.15850E-02 0.00037  1.10120E-01 0.00042  3.20685E-01 0.00031  1.34530E+00 0.00023  8.89336E+00 0.00201 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.51079E-05 0.00113  2.50975E-05 0.00113  2.66169E-05 0.01286 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.75604E-05 0.00102  2.75490E-05 0.00102  2.92186E-05 0.01287 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.79676E-03 0.00856  1.93698E-04 0.04885  1.06832E-03 0.02103  1.02386E-03 0.01971  3.16204E-03 0.01201  1.02542E-03 0.02109  3.23425E-04 0.03766 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.95879E-01 0.01928  1.24906E-02 5.2E-06  3.15919E-02 0.00041  1.10095E-01 0.00046  3.20838E-01 0.00036  1.34556E+00 0.00027  8.92178E+00 0.00267 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.48085E-05 0.00262  2.48005E-05 0.00262  2.52935E-05 0.02675 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.72325E-05 0.00259  2.72236E-05 0.00259  2.77733E-05 0.02676 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.06076E-03 0.02562  1.72689E-04 0.16369  1.12766E-03 0.06801  9.92300E-04 0.06914  3.31876E-03 0.03602  1.15776E-03 0.06778  2.91591E-04 0.12049 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.83755E-01 0.06038  1.24905E-02 1.7E-05  3.15604E-02 0.00106  1.10069E-01 0.00103  3.20736E-01 0.00099  1.34539E+00 0.00063  8.97976E+00 0.00639 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.02529E-03 0.02510  1.74598E-04 0.15041  1.10265E-03 0.06546  1.00795E-03 0.06748  3.28777E-03 0.03466  1.17105E-03 0.06597  2.81269E-04 0.11680 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.83325E-01 0.05900  1.24905E-02 1.7E-05  3.15531E-02 0.00106  1.10068E-01 0.00102  3.20704E-01 0.00097  1.34538E+00 0.00062  8.97719E+00 0.00634 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.85387E+02 0.02564 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.49366E-05 0.00068 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.73723E-05 0.00046 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.92020E-03 0.00445 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.77559E+02 0.00447 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.03687E-07 0.00064 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87292E-06 0.00043  2.87270E-06 0.00043  2.90031E-06 0.00467 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.98323E-05 0.00080  3.98576E-05 0.00080  3.62972E-05 0.00891 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.70771E-01 0.00032  6.70265E-01 0.00032  7.61854E-01 0.00857 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02070E+01 0.01380 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.58861E+01 0.00043  3.47455E+01 0.00044 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.23863E+04 0.00293  2.91538E+05 0.00164  6.03619E+05 0.00081  6.49856E+05 0.00076  5.98860E+05 0.00067  6.42855E+05 0.00089  4.37104E+05 0.00065  3.87028E+05 0.00069  2.96036E+05 0.00069  2.41671E+05 0.00076  2.08384E+05 0.00078  1.87740E+05 0.00093  1.73469E+05 0.00084  1.64948E+05 0.00083  1.60649E+05 0.00066  1.38620E+05 0.00100  1.37070E+05 0.00086  1.35699E+05 0.00091  1.33160E+05 0.00088  2.60191E+05 0.00078  2.50745E+05 0.00059  1.80739E+05 0.00091  1.17137E+05 0.00097  1.35114E+05 0.00074  1.27655E+05 0.00084  1.16092E+05 0.00086  1.90472E+05 0.00084  4.36215E+04 0.00156  5.47423E+04 0.00155  4.95781E+04 0.00165  2.86665E+04 0.00180  5.00734E+04 0.00159  3.40987E+04 0.00188  2.89564E+04 0.00224  5.54883E+03 0.00367  5.48324E+03 0.00332  5.56777E+03 0.00348  5.72817E+03 0.00374  5.71754E+03 0.00367  5.61645E+03 0.00450  5.86032E+03 0.00382  5.48087E+03 0.00388  1.03778E+04 0.00266  1.66363E+04 0.00206  2.11420E+04 0.00254  5.56172E+04 0.00137  5.85642E+04 0.00186  6.39017E+04 0.00113  4.35418E+04 0.00130  3.23536E+04 0.00172  2.48222E+04 0.00155  2.93760E+04 0.00134  5.62437E+04 0.00153  7.66332E+04 0.00111  1.47573E+05 0.00106  2.26713E+05 0.00084  3.36876E+05 0.00090  2.14558E+05 0.00085  1.53366E+05 0.00125  1.10070E+05 0.00109  9.88352E+04 0.00127  9.72970E+04 0.00100  8.11017E+04 0.00109  5.48554E+04 0.00118  5.05041E+04 0.00101  4.48784E+04 0.00143  3.79865E+04 0.00121  2.98813E+04 0.00163  1.99842E+04 0.00147  7.07197E+03 0.00202 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.10563E+00 0.00047 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.17098E+18 0.00044  3.28395E+17 0.00076 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37689E-01 0.00010  1.51004E+00 0.00023 ];
INF_CAPT                  (idx, [1:   4]) = [  6.43539E-03 0.00053  2.95665E-02 0.00029 ];
INF_ABS                   (idx, [1:   4]) = [  8.70969E-03 0.00042  6.36364E-02 0.00053 ];
INF_FISS                  (idx, [1:   4]) = [  2.27430E-03 0.00038  3.40698E-02 0.00075 ];
INF_NSF                   (idx, [1:   4]) = [  5.83885E-03 0.00038  8.37971E-02 0.00075 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.56732E+00 3.5E-05  2.45957E+00 5.8E-06 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03897E+02 4.3E-06  2.02568E+02 9.2E-07 ];
INF_INVV                  (idx, [1:   4]) = [  5.87949E-08 0.00039  2.54675E-06 0.00013 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28968E-01 1.0E-04  1.44637E+00 0.00026 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43792E-01 0.00019  3.83084E-01 0.00035 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61607E-02 0.00023  9.11258E-02 0.00081 ];
INF_SCATT3                (idx, [1:   4]) = [  7.36457E-03 0.00262  2.73876E-02 0.00170 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02916E-02 0.00193 -8.36537E-03 0.00600 ];
INF_SCATT5                (idx, [1:   4]) = [  1.40211E-04 0.14350  6.30366E-03 0.01198 ];
INF_SCATT6                (idx, [1:   4]) = [  5.06021E-03 0.00317 -1.63127E-02 0.00343 ];
INF_SCATT7                (idx, [1:   4]) = [  7.50710E-04 0.02032  3.28549E-04 0.11849 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29008E-01 1.0E-04  1.44637E+00 0.00026 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43793E-01 0.00019  3.83084E-01 0.00035 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61610E-02 0.00023  9.11258E-02 0.00081 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.36475E-03 0.00262  2.73876E-02 0.00170 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02917E-02 0.00193 -8.36537E-03 0.00600 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.39941E-04 0.14397  6.30366E-03 0.01198 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.06030E-03 0.00317 -1.63127E-02 0.00343 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.50666E-04 0.02033  3.28549E-04 0.11849 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13677E-01 0.00027  9.73226E-01 0.00024 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55999E+00 0.00027  3.42504E-01 0.00024 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.66988E-03 0.00041  6.36364E-02 0.00053 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69696E-02 0.00029  6.48043E-02 0.00065 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10719E-01 9.5E-05  1.82486E-02 0.00037  1.13715E-03 0.00370  1.44524E+00 0.00026 ];
INF_S1                    (idx, [1:   8]) = [  2.38462E-01 0.00019  5.32998E-03 0.00090  4.84341E-04 0.00651  3.82600E-01 0.00035 ];
INF_S2                    (idx, [1:   8]) = [  9.77347E-02 0.00020 -1.57405E-03 0.00213  2.64584E-04 0.00771  9.08612E-02 0.00081 ];
INF_S3                    (idx, [1:   8]) = [  9.23965E-03 0.00202 -1.87507E-03 0.00148  9.26549E-05 0.01910  2.72949E-02 0.00171 ];
INF_S4                    (idx, [1:   8]) = [ -9.66278E-03 0.00203 -6.28809E-04 0.00393 -1.44618E-06 0.97479 -8.36392E-03 0.00600 ];
INF_S5                    (idx, [1:   8]) = [  1.26836E-04 0.15681  1.33745E-05 0.14291 -3.90961E-05 0.03077  6.34275E-03 0.01194 ];
INF_S6                    (idx, [1:   8]) = [  5.20528E-03 0.00294 -1.45071E-04 0.01548 -4.81211E-05 0.02898 -1.62646E-02 0.00344 ];
INF_S7                    (idx, [1:   8]) = [  9.24226E-04 0.01643 -1.73516E-04 0.01067 -4.46973E-05 0.02691  3.73246E-04 0.10495 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10759E-01 9.5E-05  1.82486E-02 0.00037  1.13715E-03 0.00370  1.44524E+00 0.00026 ];
INF_SP1                   (idx, [1:   8]) = [  2.38463E-01 0.00019  5.32998E-03 0.00090  4.84341E-04 0.00651  3.82600E-01 0.00035 ];
INF_SP2                   (idx, [1:   8]) = [  9.77350E-02 0.00020 -1.57405E-03 0.00213  2.64584E-04 0.00771  9.08612E-02 0.00081 ];
INF_SP3                   (idx, [1:   8]) = [  9.23983E-03 0.00202 -1.87507E-03 0.00148  9.26549E-05 0.01910  2.72949E-02 0.00171 ];
INF_SP4                   (idx, [1:   8]) = [ -9.66287E-03 0.00203 -6.28809E-04 0.00393 -1.44618E-06 0.97479 -8.36392E-03 0.00600 ];
INF_SP5                   (idx, [1:   8]) = [  1.26567E-04 0.15741  1.33745E-05 0.14291 -3.90961E-05 0.03077  6.34275E-03 0.01194 ];
INF_SP6                   (idx, [1:   8]) = [  5.20537E-03 0.00293 -1.45071E-04 0.01548 -4.81211E-05 0.02898 -1.62646E-02 0.00344 ];
INF_SP7                   (idx, [1:   8]) = [  9.24182E-04 0.01644 -1.73516E-04 0.01067 -4.46973E-05 0.02691  3.73246E-04 0.10495 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32349E-01 0.00075  1.09671E+00 0.00560 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33756E-01 0.00107  1.17487E+00 0.00758 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33813E-01 0.00120  1.16175E+00 0.00803 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29540E-01 0.00110  9.78230E-01 0.00581 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43465E+00 0.00075  3.04166E-01 0.00557 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42602E+00 0.00107  2.84108E-01 0.00754 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42569E+00 0.00120  2.87361E-01 0.00792 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45222E+00 0.00110  3.41028E-01 0.00583 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.81331E-03 0.00799  1.90080E-04 0.04671  1.04230E-03 0.02018  1.05475E-03 0.01929  3.16056E-03 0.01162  1.02891E-03 0.02100  3.36709E-04 0.03476 ];
LAMBDA                    (idx, [1:  14]) = [  8.14667E-01 0.01843  1.24906E-02 5.3E-06  3.15850E-02 0.00037  1.10120E-01 0.00042  3.20685E-01 0.00031  1.34530E+00 0.00023  8.89336E+00 0.00201 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0005' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:52:32 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:20:12 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299952 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.98736E-01  9.94239E-01  1.00121E+00  1.00358E+00  1.00223E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13611E-02 0.00104  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88639E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.00183E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.00612E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69770E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.56913E+01 0.00042  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.56832E+01 0.00042  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.88626E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.23510E-01 0.00108  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000612 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00012E+04 0.00076 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00012E+04 0.00076 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.36390E+02 ;
RUNNING_TIME              (idx, 1)        =  2.76714E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.61967E-01  4.61967E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  6.34333E-02  8.73334E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  2.69296E+01  3.30577E+00  2.58062E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  2.13617E-01  2.79167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  9.30001E-03  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  2.76712E+01  1.23259E+02 ];
CPU_USAGE                 (idx, 1)        = 4.92891 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99931E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.68673E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.08160E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.85224E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.69460E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.60861E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.12300E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.47297E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.73992E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  4.71715E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.83691E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.26091E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.66660E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  3.45623E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.17025E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.48757E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.12825E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.07696E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.19575E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.52564E+11 ;
CS137_ACTIVITY            (idx, 1)        =  2.69914E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.95247E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.58486E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.21770E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.17747E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.18198E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 4 ;
BURNUP                     (idx, [1:  2])  = [  2.00000E+00  2.00018E+00 ];
BURN_DAYS                 (idx, 1)        =  5.00000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.05241E-01 0.00111 ];
U235_FISS                 (idx, [1:   4]) = [  1.17149E+16 0.00062  8.45973E-01 0.00029 ];
U238_FISS                 (idx, [1:   4]) = [  8.84788E+14 0.00270  6.38848E-02 0.00253 ];
PU239_FISS                (idx, [1:   4]) = [  1.23668E+15 0.00215  8.93065E-02 0.00208 ];
PU240_FISS                (idx, [1:   4]) = [  3.49663E+11 0.13534  2.52500E-05 0.13531 ];
PU241_FISS                (idx, [1:   4]) = [  8.19002E+12 0.02863  5.90963E-04 0.02857 ];
U235_CAPT                 (idx, [1:   4]) = [  2.55867E+15 0.00150  1.44060E-01 0.00135 ];
U238_CAPT                 (idx, [1:   4]) = [  8.02219E+15 0.00105  4.51655E-01 0.00072 ];
PU239_CAPT                (idx, [1:   4]) = [  6.94630E+14 0.00299  3.91142E-02 0.00300 ];
PU240_CAPT                (idx, [1:   4]) = [  1.16656E+14 0.00752  6.56850E-03 0.00751 ];
PU241_CAPT                (idx, [1:   4]) = [  3.16188E+12 0.04403  1.77864E-04 0.04396 ];
XE135_CAPT                (idx, [1:   4]) = [  7.17783E+14 0.00294  4.04156E-02 0.00291 ];
SM149_CAPT                (idx, [1:   4]) = [  1.59672E+14 0.00634  8.99059E-03 0.00633 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000612 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.23431E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000612 5.00723E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2787095 2.79087E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2173241 2.17608E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 40276 4.02798E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000612 5.00723E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.86733E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.45793E+16 1.2E-05  3.45793E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38322E+16 1.8E-06  1.38322E+16 1.8E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.77566E+16 0.00043  1.26648E+16 0.00045  5.09175E+15 0.00099 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.15888E+16 0.00024  2.64970E+16 0.00021  5.09175E+15 0.00099 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.18198E+16 0.00050  3.18198E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.51747E+18 0.00046  4.08960E+17 0.00044  1.10851E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.56361E+14 0.00497 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.18451E+16 0.00025 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.13692E+18 0.00057 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12281E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12281E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.67126E+00 0.00043 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.84200E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.70586E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24806E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94781E-01 3.0E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97148E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.09682E+00 0.00051 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.08798E+00 0.00051 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.49992E+00 1.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03080E+02 1.8E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.08803E+00 0.00054  1.08081E+00 0.00052  7.17434E-03 0.00778 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.08750E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.08686E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.08750E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.09633E+00 0.00024 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74250E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74278E+01 7.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.42834E-07 0.00338 ];
IMP_EALF                  (idx, [1:   2]) = [  5.40036E-07 0.00138 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.16937E-01 0.00272 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.17015E-01 0.00110 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.21553E-03 0.00573  1.67979E-04 0.03438  9.89626E-04 0.01385  9.95717E-04 0.01382  2.83737E-03 0.00784  9.25021E-04 0.01396  2.99815E-04 0.02385 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.01947E-01 0.01239  1.03670E-02 0.02026  3.15303E-02 0.00029  1.10095E-01 0.00029  3.20929E-01 0.00023  1.34452E+00 0.00025  8.65838E+00 0.00808 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.62400E-03 0.00794  1.75256E-04 0.04674  1.07976E-03 0.02061  1.06834E-03 0.02037  2.99832E-03 0.01105  9.75219E-04 0.02052  3.27101E-04 0.03519 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.06891E-01 0.01809  1.24903E-02 7.3E-06  3.15218E-02 0.00043  1.10110E-01 0.00040  3.20949E-01 0.00033  1.34453E+00 0.00035  8.93119E+00 0.00222 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.47178E-05 0.00114  2.47046E-05 0.00115  2.68210E-05 0.01168 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.68899E-05 0.00101  2.68756E-05 0.00102  2.91817E-05 0.01169 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.60443E-03 0.00793  1.77009E-04 0.05369  1.07882E-03 0.02112  1.03137E-03 0.02212  2.98370E-03 0.01167  1.01009E-03 0.02158  3.23433E-04 0.03781 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.15256E-01 0.02065  1.24904E-02 8.9E-06  3.14964E-02 0.00054  1.10078E-01 0.00047  3.20941E-01 0.00037  1.34454E+00 0.00040  8.93513E+00 0.00279 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.44477E-05 0.00278  2.44266E-05 0.00280  2.68351E-05 0.02904 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.65955E-05 0.00272  2.65726E-05 0.00273  2.92003E-05 0.02905 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.51313E-03 0.02839  1.70714E-04 0.16546  1.08124E-03 0.07157  9.83138E-04 0.06852  2.94016E-03 0.03925  1.02755E-03 0.06622  3.10325E-04 0.11216 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.64114E-01 0.06521  1.24902E-02 2.3E-05  3.15719E-02 0.00108  1.10112E-01 0.00116  3.20622E-01 0.00104  1.34632E+00 0.00081  8.92015E+00 0.00564 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.53541E-03 0.02745  1.65234E-04 0.16028  1.07320E-03 0.06866  9.88097E-04 0.06673  2.95084E-03 0.03874  1.03763E-03 0.06523  3.20406E-04 0.10870 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.78631E-01 0.06491  1.24902E-02 2.2E-05  3.15713E-02 0.00108  1.10108E-01 0.00115  3.20592E-01 0.00102  1.34660E+00 0.00069  8.91922E+00 0.00559 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.67424E+02 0.02853 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.45661E-05 0.00072 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.67247E-05 0.00046 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.58770E-03 0.00551 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.68275E+02 0.00562 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.96616E-07 0.00061 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.85947E-06 0.00042  2.85943E-06 0.00042  2.86525E-06 0.00487 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.93164E-05 0.00077  3.93384E-05 0.00077  3.61613E-05 0.00928 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.68443E-01 0.00032  6.67988E-01 0.00032  7.53563E-01 0.00871 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.01950E+01 0.01336 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.56832E+01 0.00042  3.44155E+01 0.00045 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.29438E+04 0.00294  2.93630E+05 0.00172  6.05358E+05 0.00097  6.50618E+05 0.00096  5.98891E+05 0.00075  6.42935E+05 0.00072  4.37056E+05 0.00084  3.86934E+05 0.00080  2.95628E+05 0.00063  2.41244E+05 0.00081  2.08225E+05 0.00068  1.87827E+05 0.00076  1.73237E+05 0.00104  1.64918E+05 0.00096  1.60387E+05 0.00094  1.38502E+05 0.00075  1.36786E+05 0.00076  1.35863E+05 0.00126  1.33192E+05 0.00101  2.59875E+05 0.00047  2.51025E+05 0.00071  1.81070E+05 0.00081  1.16968E+05 0.00096  1.35053E+05 0.00123  1.27566E+05 0.00103  1.15836E+05 0.00104  1.90182E+05 0.00073  4.36010E+04 0.00188  5.45558E+04 0.00129  4.95591E+04 0.00145  2.87363E+04 0.00217  4.98931E+04 0.00128  3.39201E+04 0.00186  2.89065E+04 0.00158  5.43529E+03 0.00325  5.35649E+03 0.00429  5.43685E+03 0.00326  5.51598E+03 0.00306  5.51176E+03 0.00354  5.53052E+03 0.00391  5.74674E+03 0.00377  5.45531E+03 0.00331  1.02845E+04 0.00317  1.64710E+04 0.00174  2.10685E+04 0.00192  5.54733E+04 0.00145  5.81730E+04 0.00170  6.32499E+04 0.00154  4.26983E+04 0.00147  3.14562E+04 0.00128  2.40448E+04 0.00164  2.83449E+04 0.00149  5.43830E+04 0.00114  7.44746E+04 0.00120  1.43995E+05 0.00098  2.22238E+05 0.00083  3.31135E+05 0.00090  2.11178E+05 0.00102  1.51043E+05 0.00118  1.08558E+05 0.00131  9.73265E+04 0.00105  9.57859E+04 0.00127  7.99611E+04 0.00123  5.41440E+04 0.00131  4.98162E+04 0.00094  4.43003E+04 0.00148  3.74186E+04 0.00154  2.94473E+04 0.00162  1.97255E+04 0.00183  7.00291E+03 0.00170 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.09568E+00 0.00048 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.18978E+18 0.00048  3.27728E+17 0.00088 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37629E-01 0.00013  1.51739E+00 0.00034 ];
INF_CAPT                  (idx, [1:   4]) = [  6.53230E-03 0.00062  3.04690E-02 0.00036 ];
INF_ABS                   (idx, [1:   4]) = [  8.77274E-03 0.00047  6.45473E-02 0.00066 ];
INF_FISS                  (idx, [1:   4]) = [  2.24044E-03 0.00042  3.40783E-02 0.00093 ];
INF_NSF                   (idx, [1:   4]) = [  5.77157E-03 0.00042  8.45733E-02 0.00093 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.57609E+00 4.5E-05  2.48173E+00 1.5E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04006E+02 4.7E-06  2.02858E+02 2.5E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.85803E-08 0.00027  2.54999E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28851E-01 0.00014  1.45281E+00 0.00038 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43786E-01 0.00020  3.85301E-01 0.00046 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61655E-02 0.00028  9.17235E-02 0.00082 ];
INF_SCATT3                (idx, [1:   4]) = [  7.38403E-03 0.00275  2.76243E-02 0.00280 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03072E-02 0.00163 -8.40792E-03 0.00701 ];
INF_SCATT5                (idx, [1:   4]) = [  1.25995E-04 0.16203  6.23589E-03 0.00759 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08529E-03 0.00308 -1.64369E-02 0.00283 ];
INF_SCATT7                (idx, [1:   4]) = [  7.66585E-04 0.02235  3.11868E-04 0.16018 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28890E-01 0.00014  1.45281E+00 0.00038 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43786E-01 0.00020  3.85301E-01 0.00046 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61657E-02 0.00027  9.17235E-02 0.00082 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.38404E-03 0.00275  2.76243E-02 0.00280 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03073E-02 0.00163 -8.40792E-03 0.00701 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.25969E-04 0.16174  6.23589E-03 0.00759 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08533E-03 0.00308 -1.64369E-02 0.00283 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.66499E-04 0.02241  3.11868E-04 0.16018 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13457E-01 0.00021  9.78687E-01 0.00029 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56160E+00 0.00021  3.40593E-01 0.00029 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.73404E-03 0.00048  6.45473E-02 0.00066 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69605E-02 0.00025  6.57357E-02 0.00076 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10668E-01 0.00013  1.81828E-02 0.00038  1.15087E-03 0.00410  1.45166E+00 0.00038 ];
INF_S1                    (idx, [1:   8]) = [  2.38473E-01 0.00021  5.31261E-03 0.00083  4.92243E-04 0.00517  3.84809E-01 0.00046 ];
INF_S2                    (idx, [1:   8]) = [  9.77386E-02 0.00027 -1.57313E-03 0.00212  2.66328E-04 0.00643  9.14572E-02 0.00083 ];
INF_S3                    (idx, [1:   8]) = [  9.26069E-03 0.00213 -1.87666E-03 0.00231  9.54874E-05 0.01908  2.75288E-02 0.00280 ];
INF_S4                    (idx, [1:   8]) = [ -9.68210E-03 0.00168 -6.25068E-04 0.00529  7.34993E-07 1.00000 -8.40865E-03 0.00700 ];
INF_S5                    (idx, [1:   8]) = [  1.05754E-04 0.19155  2.02411E-05 0.12136 -3.93923E-05 0.03792  6.27528E-03 0.00751 ];
INF_S6                    (idx, [1:   8]) = [  5.22854E-03 0.00303 -1.43247E-04 0.01481 -5.04171E-05 0.02663 -1.63865E-02 0.00283 ];
INF_S7                    (idx, [1:   8]) = [  9.38958E-04 0.01789 -1.72373E-04 0.01110 -4.54312E-05 0.02292  3.57299E-04 0.13949 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10707E-01 0.00013  1.81828E-02 0.00038  1.15087E-03 0.00410  1.45166E+00 0.00038 ];
INF_SP1                   (idx, [1:   8]) = [  2.38474E-01 0.00021  5.31261E-03 0.00083  4.92243E-04 0.00517  3.84809E-01 0.00046 ];
INF_SP2                   (idx, [1:   8]) = [  9.77388E-02 0.00027 -1.57313E-03 0.00212  2.66328E-04 0.00643  9.14572E-02 0.00083 ];
INF_SP3                   (idx, [1:   8]) = [  9.26069E-03 0.00212 -1.87666E-03 0.00231  9.54874E-05 0.01908  2.75288E-02 0.00280 ];
INF_SP4                   (idx, [1:   8]) = [ -9.68228E-03 0.00168 -6.25068E-04 0.00529  7.34993E-07 1.00000 -8.40865E-03 0.00700 ];
INF_SP5                   (idx, [1:   8]) = [  1.05728E-04 0.19122  2.02411E-05 0.12136 -3.93923E-05 0.03792  6.27528E-03 0.00751 ];
INF_SP6                   (idx, [1:   8]) = [  5.22857E-03 0.00303 -1.43247E-04 0.01481 -5.04171E-05 0.02663 -1.63865E-02 0.00283 ];
INF_SP7                   (idx, [1:   8]) = [  9.38871E-04 0.01793 -1.72373E-04 0.01110 -4.54312E-05 0.02292  3.57299E-04 0.13949 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32094E-01 0.00053  1.11218E+00 0.00609 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33209E-01 0.00079  1.19264E+00 0.00754 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33805E-01 0.00079  1.18144E+00 0.00863 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29324E-01 0.00067  9.88854E-01 0.00645 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43621E+00 0.00053  2.99980E-01 0.00610 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42936E+00 0.00079  2.79867E-01 0.00742 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42571E+00 0.00078  2.82646E-01 0.00863 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45356E+00 0.00067  3.37427E-01 0.00645 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.62400E-03 0.00794  1.75256E-04 0.04674  1.07976E-03 0.02061  1.06834E-03 0.02037  2.99832E-03 0.01105  9.75219E-04 0.02052  3.27101E-04 0.03519 ];
LAMBDA                    (idx, [1:  14]) = [  8.06891E-01 0.01809  1.24903E-02 7.3E-06  3.15218E-02 0.00043  1.10110E-01 0.00040  3.20949E-01 0.00033  1.34453E+00 0.00035  8.93119E+00 0.00222 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0005' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:52:32 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:26:01 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299952 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.82962E-01  9.98103E-01  1.00465E+00  1.00793E+00  1.00636E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.2E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13709E-02 0.00108  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88629E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.01190E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.01619E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69121E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.55117E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.55036E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.78573E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.20822E-01 0.00115  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000714 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00014E+04 0.00079 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00014E+04 0.00079 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.65423E+02 ;
RUNNING_TIME              (idx, 1)        =  3.34841E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.61967E-01  4.61967E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  8.03333E-02  8.41667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.26709E+01  3.20538E+00  2.53595E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  2.67717E-01  2.60167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.16667E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.34839E+01  1.22143E+02 ];
CPU_USAGE                 (idx, 1)        = 4.94036 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99948E+00 0.00021 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.71447E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.15291E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.83849E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.12564E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.62268E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.13271E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.53021E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.72520E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  5.68301E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.99571E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.60230E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.73795E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.08071E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.32192E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.67093E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.15890E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.17207E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.23843E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.32563E+11 ;
CS137_ACTIVITY            (idx, 1)        =  4.04939E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.99721E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.55099E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.25146E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.19066E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.23074E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 5 ;
BURNUP                     (idx, [1:  2])  = [  3.00000E+00  3.00026E+00 ];
BURN_DAYS                 (idx, 1)        =  7.50000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.12393E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  1.11521E+16 0.00065  8.06613E-01 0.00033 ];
U238_FISS                 (idx, [1:   4]) = [  8.98801E+14 0.00281  6.49981E-02 0.00262 ];
PU239_FISS                (idx, [1:   4]) = [  1.74530E+15 0.00182  1.26232E-01 0.00171 ];
PU240_FISS                (idx, [1:   4]) = [  5.82598E+11 0.10170  4.21358E-05 0.10182 ];
PU241_FISS                (idx, [1:   4]) = [  2.49971E+13 0.01585  1.80798E-03 0.01585 ];
U235_CAPT                 (idx, [1:   4]) = [  2.44418E+15 0.00159  1.33803E-01 0.00155 ];
U238_CAPT                 (idx, [1:   4]) = [  8.09114E+15 0.00100  4.42888E-01 0.00067 ];
PU239_CAPT                (idx, [1:   4]) = [  9.73524E+14 0.00262  5.32930E-02 0.00258 ];
PU240_CAPT                (idx, [1:   4]) = [  2.32242E+14 0.00512  1.27112E-02 0.00503 ];
PU241_CAPT                (idx, [1:   4]) = [  9.12598E+12 0.02628  4.99597E-04 0.02627 ];
XE135_CAPT                (idx, [1:   4]) = [  7.18155E+14 0.00288  3.93124E-02 0.00283 ];
SM149_CAPT                (idx, [1:   4]) = [  1.62315E+14 0.00618  8.88390E-03 0.00611 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000714 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.52984E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000714 5.00753E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2823320 2.82726E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2136992 2.13986E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 40402 4.04085E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000714 5.00753E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -1.76951E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.47757E+16 1.5E-05  3.47757E+16 1.5E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38171E+16 2.2E-06  1.38171E+16 2.2E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.82733E+16 0.00048  1.31534E+16 0.00045  5.11989E+15 0.00108 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.20904E+16 0.00027  2.69705E+16 0.00022  5.11989E+15 0.00108 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.23074E+16 0.00049  3.23074E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.53576E+18 0.00048  4.13492E+17 0.00044  1.12227E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.61119E+14 0.00501 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.23515E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.14862E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12164E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12164E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.66012E+00 0.00046 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.85573E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.67469E-01 0.00030 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24759E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94778E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97125E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.08594E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07716E+00 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.51686E+00 1.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03302E+02 2.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07740E+00 0.00057  1.07021E+00 0.00056  6.95241E-03 0.00891 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07660E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07653E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07660E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.08537E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73895E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73922E+01 8.3E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.62547E-07 0.00346 ];
IMP_EALF                  (idx, [1:   2]) = [  5.59674E-07 0.00144 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.21151E-01 0.00268 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.21156E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.09477E-03 0.00595  1.74172E-04 0.03360  9.93315E-04 0.01354  9.66527E-04 0.01495  2.75993E-03 0.00802  9.09057E-04 0.01492  2.91767E-04 0.02528 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.96871E-01 0.01307  1.05418E-02 0.01925  3.14725E-02 0.00032  1.10100E-01 0.00032  3.21172E-01 0.00024  1.34397E+00 0.00028  8.47460E+00 0.01088 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.46924E-03 0.00813  1.89696E-04 0.04688  1.05521E-03 0.02042  1.03775E-03 0.02163  2.92567E-03 0.01177  9.50761E-04 0.02120  3.10153E-04 0.03711 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.96449E-01 0.01992  1.24903E-02 8.5E-06  3.14776E-02 0.00045  1.10080E-01 0.00047  3.21040E-01 0.00034  1.34361E+00 0.00045  8.93787E+00 0.00220 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.45528E-05 0.00120  2.45443E-05 0.00120  2.58901E-05 0.01270 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.64485E-05 0.00102  2.64393E-05 0.00102  2.78948E-05 0.01272 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.44912E-03 0.00897  1.85127E-04 0.05198  1.06378E-03 0.02142  1.01492E-03 0.02246  2.89398E-03 0.01295  9.81645E-04 0.02324  3.09666E-04 0.03949 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.06649E-01 0.02132  1.24905E-02 9.2E-06  3.14889E-02 0.00052  1.10059E-01 0.00051  3.21185E-01 0.00037  1.34399E+00 0.00055  8.96116E+00 0.00291 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.42362E-05 0.00273  2.42278E-05 0.00274  2.52969E-05 0.03078 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.61087E-05 0.00269  2.60997E-05 0.00270  2.72500E-05 0.03082 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.24968E-03 0.02790  1.09882E-04 0.16531  1.07563E-03 0.06628  1.02564E-03 0.06836  2.71729E-03 0.04328  9.83554E-04 0.06767  3.37685E-04 0.11848 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.81981E-01 0.06861  1.24909E-02 1.3E-05  3.15151E-02 0.00118  1.09929E-01 0.00113  3.22136E-01 0.00122  1.34483E+00 0.00079  9.03143E+00 0.00698 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.24309E-03 0.02710  1.15766E-04 0.17187  1.08700E-03 0.06324  9.88313E-04 0.06657  2.70876E-03 0.04113  9.94490E-04 0.06758  3.48756E-04 0.11602 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.80331E-01 0.06677  1.24909E-02 1.3E-05  3.15049E-02 0.00119  1.09941E-01 0.00114  3.22142E-01 0.00120  1.34461E+00 0.00083  9.03155E+00 0.00702 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.58721E+02 0.02801 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.43812E-05 0.00081 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.62643E-05 0.00058 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.38547E-03 0.00536 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.62023E+02 0.00547 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.90689E-07 0.00064 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.84540E-06 0.00041  2.84516E-06 0.00041  2.87737E-06 0.00486 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.89449E-05 0.00079  3.89669E-05 0.00079  3.56916E-05 0.00907 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.65375E-01 0.00029  6.64954E-01 0.00030  7.46687E-01 0.00885 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03395E+01 0.01329 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.55036E+01 0.00045  3.42061E+01 0.00042 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.40000E+04 0.00314  2.95054E+05 0.00147  6.06041E+05 0.00106  6.50693E+05 0.00069  5.98383E+05 0.00069  6.42777E+05 0.00056  4.36546E+05 0.00064  3.86326E+05 0.00082  2.95735E+05 0.00068  2.41380E+05 0.00068  2.08126E+05 0.00084  1.87612E+05 0.00094  1.73289E+05 0.00089  1.64551E+05 0.00089  1.60582E+05 0.00096  1.38519E+05 0.00104  1.36958E+05 0.00104  1.35854E+05 0.00087  1.33138E+05 0.00076  2.60102E+05 0.00059  2.50947E+05 0.00079  1.80834E+05 0.00078  1.17286E+05 0.00110  1.35308E+05 0.00072  1.28061E+05 0.00105  1.15669E+05 0.00107  1.89895E+05 0.00083  4.34573E+04 0.00106  5.45253E+04 0.00139  4.95357E+04 0.00140  2.87318E+04 0.00188  4.99051E+04 0.00119  3.40042E+04 0.00157  2.88818E+04 0.00206  5.43065E+03 0.00442  5.26751E+03 0.00288  5.23916E+03 0.00351  5.30191E+03 0.00344  5.32840E+03 0.00358  5.36640E+03 0.00425  5.67851E+03 0.00380  5.35957E+03 0.00322  1.02303E+04 0.00253  1.63301E+04 0.00190  2.08543E+04 0.00232  5.50372E+04 0.00158  5.79715E+04 0.00148  6.26495E+04 0.00150  4.20974E+04 0.00124  3.05854E+04 0.00148  2.32537E+04 0.00147  2.74963E+04 0.00124  5.29767E+04 0.00115  7.27900E+04 0.00132  1.41377E+05 0.00117  2.18616E+05 0.00131  3.26440E+05 0.00124  2.08196E+05 0.00128  1.48977E+05 0.00121  1.06962E+05 0.00123  9.60850E+04 0.00122  9.46972E+04 0.00139  7.88689E+04 0.00151  5.33242E+04 0.00116  4.92955E+04 0.00138  4.38528E+04 0.00163  3.70426E+04 0.00166  2.91958E+04 0.00164  1.95341E+04 0.00171  6.90349E+03 0.00261 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.08530E+00 0.00045 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.20800E+18 0.00047  3.27789E+17 0.00109 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37635E-01 0.00013  1.52285E+00 0.00033 ];
INF_CAPT                  (idx, [1:   4]) = [  6.65105E-03 0.00050  3.12387E-02 0.00046 ];
INF_ABS                   (idx, [1:   4]) = [  8.85938E-03 0.00040  6.52585E-02 0.00078 ];
INF_FISS                  (idx, [1:   4]) = [  2.20833E-03 0.00045  3.40198E-02 0.00109 ];
INF_NSF                   (idx, [1:   4]) = [  5.70712E-03 0.00047  8.50738E-02 0.00110 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.58436E+00 6.4E-05  2.50071E+00 1.8E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04108E+02 6.2E-06  2.03109E+02 2.9E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.83836E-08 0.00043  2.55250E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28774E-01 0.00014  1.45759E+00 0.00038 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43713E-01 0.00022  3.86547E-01 0.00049 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61511E-02 0.00028  9.18648E-02 0.00079 ];
INF_SCATT3                (idx, [1:   4]) = [  7.36921E-03 0.00321  2.76877E-02 0.00243 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03000E-02 0.00241 -8.49569E-03 0.00561 ];
INF_SCATT5                (idx, [1:   4]) = [  1.46084E-04 0.13419  6.38848E-03 0.00651 ];
INF_SCATT6                (idx, [1:   4]) = [  5.05647E-03 0.00445 -1.64256E-02 0.00320 ];
INF_SCATT7                (idx, [1:   4]) = [  7.55247E-04 0.02050  3.39530E-04 0.14244 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28814E-01 0.00014  1.45759E+00 0.00038 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43714E-01 0.00022  3.86547E-01 0.00049 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61513E-02 0.00028  9.18648E-02 0.00079 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.36954E-03 0.00321  2.76877E-02 0.00243 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02998E-02 0.00241 -8.49569E-03 0.00561 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.46110E-04 0.13435  6.38848E-03 0.00651 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.05646E-03 0.00445 -1.64256E-02 0.00320 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.55209E-04 0.02047  3.39530E-04 0.14244 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13256E-01 0.00027  9.83059E-01 0.00028 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56307E+00 0.00027  3.39078E-01 0.00028 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.81910E-03 0.00040  6.52585E-02 0.00078 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69596E-02 0.00020  6.64149E-02 0.00096 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10676E-01 0.00013  1.80978E-02 0.00038  1.15835E-03 0.00470  1.45643E+00 0.00039 ];
INF_S1                    (idx, [1:   8]) = [  2.38432E-01 0.00022  5.28033E-03 0.00098  4.95226E-04 0.00618  3.86052E-01 0.00049 ];
INF_S2                    (idx, [1:   8]) = [  9.77188E-02 0.00026 -1.56772E-03 0.00181  2.67708E-04 0.01066  9.15971E-02 0.00081 ];
INF_S3                    (idx, [1:   8]) = [  9.23109E-03 0.00246 -1.86188E-03 0.00165  9.33343E-05 0.01913  2.75944E-02 0.00245 ];
INF_S4                    (idx, [1:   8]) = [ -9.68384E-03 0.00256 -6.16201E-04 0.00396 -1.88785E-06 0.85383 -8.49380E-03 0.00561 ];
INF_S5                    (idx, [1:   8]) = [  1.28452E-04 0.15617  1.76321E-05 0.14997 -3.92427E-05 0.03269  6.42772E-03 0.00642 ];
INF_S6                    (idx, [1:   8]) = [  5.20167E-03 0.00422 -1.45199E-04 0.01395 -4.91474E-05 0.02459 -1.63765E-02 0.00320 ];
INF_S7                    (idx, [1:   8]) = [  9.25619E-04 0.01680 -1.70372E-04 0.01087 -4.54741E-05 0.02601  3.85004E-04 0.12502 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10716E-01 0.00013  1.80978E-02 0.00038  1.15835E-03 0.00470  1.45643E+00 0.00039 ];
INF_SP1                   (idx, [1:   8]) = [  2.38434E-01 0.00022  5.28033E-03 0.00098  4.95226E-04 0.00618  3.86052E-01 0.00049 ];
INF_SP2                   (idx, [1:   8]) = [  9.77190E-02 0.00026 -1.56772E-03 0.00181  2.67708E-04 0.01066  9.15971E-02 0.00081 ];
INF_SP3                   (idx, [1:   8]) = [  9.23143E-03 0.00246 -1.86188E-03 0.00165  9.33343E-05 0.01913  2.75944E-02 0.00245 ];
INF_SP4                   (idx, [1:   8]) = [ -9.68361E-03 0.00256 -6.16201E-04 0.00396 -1.88785E-06 0.85383 -8.49380E-03 0.00561 ];
INF_SP5                   (idx, [1:   8]) = [  1.28478E-04 0.15635  1.76321E-05 0.14997 -3.92427E-05 0.03269  6.42772E-03 0.00642 ];
INF_SP6                   (idx, [1:   8]) = [  5.20166E-03 0.00422 -1.45199E-04 0.01395 -4.91474E-05 0.02459 -1.63765E-02 0.00320 ];
INF_SP7                   (idx, [1:   8]) = [  9.25581E-04 0.01678 -1.70372E-04 0.01087 -4.54741E-05 0.02601  3.85004E-04 0.12502 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32103E-01 0.00069  1.10909E+00 0.00665 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33674E-01 0.00100  1.18082E+00 0.00714 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33790E-01 0.00100  1.18852E+00 0.00882 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28919E-01 0.00096  9.84462E-01 0.00687 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43616E+00 0.00069  3.00868E-01 0.00671 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42652E+00 0.00100  2.82631E-01 0.00703 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42582E+00 0.00101  2.80985E-01 0.00880 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45615E+00 0.00096  3.38990E-01 0.00708 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.46924E-03 0.00813  1.89696E-04 0.04688  1.05521E-03 0.02042  1.03775E-03 0.02163  2.92567E-03 0.01177  9.50761E-04 0.02120  3.10153E-04 0.03711 ];
LAMBDA                    (idx, [1:  14]) = [  7.96449E-01 0.01992  1.24903E-02 8.5E-06  3.14776E-02 0.00045  1.10080E-01 0.00047  3.21040E-01 0.00034  1.34361E+00 0.00045  8.93787E+00 0.00220 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0005' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:52:32 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:31:46 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299952 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00688E+00  9.96920E-01  9.98648E-01  1.00600E+00  9.91556E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13517E-02 0.00107  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88648E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.02425E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.02853E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68444E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.53960E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.53877E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.68918E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.17911E-01 0.00109  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000896 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00018E+04 0.00076 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00018E+04 0.00076 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.94161E+02 ;
RUNNING_TIME              (idx, 1)        =  3.92372E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.61967E-01  4.61967E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  9.87667E-02  9.18334E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.83520E+01  3.18877E+00  2.49230E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  3.21100E-01  2.67333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.41667E-02  8.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.92371E+01  1.20010E+02 ];
CPU_USAGE                 (idx, 1)        = 4.94840 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00116E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.73447E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.20361E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.82581E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.96957E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.63850E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.14366E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.56509E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.71142E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  6.59431E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.10125E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.02901E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.81794E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.56530E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.41946E+07 ;
SR90_ACTIVITY             (idx, 1)        =  4.82035E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.18629E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.21779E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.27606E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.18212E+12 ;
CS137_ACTIVITY            (idx, 1)        =  5.39915E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.02951E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.52240E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  7.89703E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.20068E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.28079E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 6 ;
BURNUP                     (idx, [1:  2])  = [  4.00000E+00  4.00035E+00 ];
BURN_DAYS                 (idx, 1)        =  1.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.19904E-01 0.00105 ];
U235_FISS                 (idx, [1:   4]) = [  1.06639E+16 0.00067  7.71840E-01 0.00039 ];
U238_FISS                 (idx, [1:   4]) = [  9.14693E+14 0.00288  6.61934E-02 0.00271 ];
PU239_FISS                (idx, [1:   4]) = [  2.18113E+15 0.00167  1.57862E-01 0.00154 ];
PU240_FISS                (idx, [1:   4]) = [  1.02473E+12 0.08390  7.40480E-05 0.08398 ];
PU241_FISS                (idx, [1:   4]) = [  5.10890E+13 0.01146  3.69750E-03 0.01143 ];
U235_CAPT                 (idx, [1:   4]) = [  2.33757E+15 0.00164  1.24553E-01 0.00154 ];
U238_CAPT                 (idx, [1:   4]) = [  8.15011E+15 0.00100  4.34237E-01 0.00068 ];
PU239_CAPT                (idx, [1:   4]) = [  1.21385E+15 0.00222  6.46758E-02 0.00212 ];
PU240_CAPT                (idx, [1:   4]) = [  3.56930E+14 0.00447  1.90148E-02 0.00435 ];
PU241_CAPT                (idx, [1:   4]) = [  1.76705E+13 0.01864  9.41730E-04 0.01866 ];
XE135_CAPT                (idx, [1:   4]) = [  7.22371E+14 0.00299  3.84950E-02 0.00302 ];
SM149_CAPT                (idx, [1:   4]) = [  1.67903E+14 0.00652  8.94672E-03 0.00652 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000896 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.54977E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000896 5.00755E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2856445 2.86031E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2102969 2.10575E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 41482 4.14860E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000896 5.00755E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -1.86265E-09 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.49477E+16 1.6E-05  3.49477E+16 1.6E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38036E+16 2.6E-06  1.38036E+16 2.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.87900E+16 0.00048  1.36214E+16 0.00047  5.16853E+15 0.00113 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.25936E+16 0.00028  2.74251E+16 0.00023  5.16853E+15 0.00113 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.28079E+16 0.00051  3.28079E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.55554E+18 0.00048  4.17990E+17 0.00046  1.13755E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.72232E+14 0.00511 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.28658E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.16258E+18 0.00063 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12048E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12048E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.65136E+00 0.00044 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.85988E-01 0.00030 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.64638E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24655E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94612E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97075E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.07529E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06637E+00 0.00052 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.53177E+00 1.9E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03499E+02 2.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06638E+00 0.00053  1.05961E+00 0.00052  6.76246E-03 0.00885 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06500E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06536E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06500E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.07391E+00 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73581E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73618E+01 8.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.80566E-07 0.00356 ];
IMP_EALF                  (idx, [1:   2]) = [  5.76938E-07 0.00153 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.25741E-01 0.00285 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.24722E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.05082E-03 0.00584  1.74069E-04 0.03213  1.00194E-03 0.01434  9.50981E-04 0.01425  2.74001E-03 0.00828  8.92431E-04 0.01494  2.91390E-04 0.02538 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.97068E-01 0.01303  1.07665E-02 0.01791  3.14322E-02 0.00033  1.10168E-01 0.00033  3.21342E-01 0.00025  1.34176E+00 0.00041  8.61154E+00 0.00925 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.43149E-03 0.00856  1.83451E-04 0.04959  1.06089E-03 0.02068  1.00424E-03 0.02130  2.91706E-03 0.01164  9.46208E-04 0.02260  3.19639E-04 0.03587 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.06526E-01 0.01878  1.24901E-02 8.6E-06  3.14338E-02 0.00050  1.10130E-01 0.00045  3.21347E-01 0.00036  1.34240E+00 0.00051  8.90759E+00 0.00348 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.45550E-05 0.00121  2.45429E-05 0.00121  2.62437E-05 0.01272 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.61812E-05 0.00107  2.61683E-05 0.00108  2.79821E-05 0.01271 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.34234E-03 0.00892  1.88661E-04 0.05120  1.02661E-03 0.02241  9.93077E-04 0.02259  2.89166E-03 0.01278  9.29007E-04 0.02501  3.13328E-04 0.03883 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.13269E-01 0.02143  1.24901E-02 1.1E-05  3.14233E-02 0.00060  1.10183E-01 0.00061  3.21529E-01 0.00039  1.34221E+00 0.00061  8.93122E+00 0.00389 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.42601E-05 0.00276  2.42463E-05 0.00275  2.59783E-05 0.03106 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.58679E-05 0.00274  2.58531E-05 0.00273  2.77280E-05 0.03112 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.23112E-03 0.02758  2.01129E-04 0.15394  8.75202E-04 0.07523  9.38484E-04 0.07138  2.87223E-03 0.03980  9.23115E-04 0.07621  4.20965E-04 0.11664 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  9.31206E-01 0.06777  1.24898E-02 2.9E-05  3.14355E-02 0.00135  1.10214E-01 0.00138  3.21811E-01 0.00121  1.34079E+00 0.00187  8.92365E+00 0.00600 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.24424E-03 0.02660  2.04548E-04 0.15211  9.04050E-04 0.07259  9.28307E-04 0.06980  2.88746E-03 0.03817  9.06280E-04 0.07491  4.13601E-04 0.11551 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  9.28589E-01 0.06647  1.24898E-02 2.9E-05  3.14342E-02 0.00133  1.10226E-01 0.00137  3.21713E-01 0.00119  1.34100E+00 0.00183  8.92391E+00 0.00600 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.58049E+02 0.02805 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.43999E-05 0.00070 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.60162E-05 0.00049 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.34055E-03 0.00454 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.59863E+02 0.00450 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.86579E-07 0.00072 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.82955E-06 0.00042  2.82943E-06 0.00042  2.84972E-06 0.00490 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.87372E-05 0.00089  3.87550E-05 0.00090  3.60515E-05 0.00988 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.62468E-01 0.00033  6.62087E-01 0.00034  7.38885E-01 0.00942 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06133E+01 0.01321 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.53877E+01 0.00048  3.40500E+01 0.00046 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.46045E+04 0.00374  2.95592E+05 0.00158  6.06139E+05 0.00099  6.51716E+05 0.00085  5.98597E+05 0.00059  6.43104E+05 0.00072  4.36769E+05 0.00078  3.85762E+05 0.00084  2.95308E+05 0.00089  2.41248E+05 0.00082  2.08253E+05 0.00074  1.87452E+05 0.00077  1.73206E+05 0.00091  1.64674E+05 0.00084  1.60341E+05 0.00070  1.38494E+05 0.00091  1.36701E+05 0.00108  1.35738E+05 0.00078  1.32934E+05 0.00086  2.59861E+05 0.00048  2.51137E+05 0.00063  1.81133E+05 0.00061  1.17278E+05 0.00099  1.35197E+05 0.00101  1.27937E+05 0.00074  1.15628E+05 0.00089  1.89275E+05 0.00098  4.34354E+04 0.00171  5.44875E+04 0.00155  4.94557E+04 0.00124  2.86794E+04 0.00173  4.99707E+04 0.00131  3.37791E+04 0.00156  2.87748E+04 0.00168  5.41774E+03 0.00306  5.18741E+03 0.00345  5.04494E+03 0.00317  5.04443E+03 0.00331  5.09983E+03 0.00364  5.22971E+03 0.00359  5.59116E+03 0.00366  5.31014E+03 0.00284  1.01236E+04 0.00293  1.62182E+04 0.00273  2.07171E+04 0.00169  5.48927E+04 0.00119  5.75973E+04 0.00128  6.21176E+04 0.00100  4.14004E+04 0.00177  3.00645E+04 0.00129  2.28210E+04 0.00210  2.68758E+04 0.00111  5.16667E+04 0.00121  7.14744E+04 0.00114  1.39257E+05 0.00106  2.15814E+05 0.00108  3.22411E+05 0.00108  2.06235E+05 0.00103  1.47536E+05 0.00146  1.06124E+05 0.00124  9.54988E+04 0.00150  9.38362E+04 0.00122  7.83265E+04 0.00128  5.29290E+04 0.00137  4.88420E+04 0.00128  4.35415E+04 0.00135  3.67496E+04 0.00144  2.90063E+04 0.00141  1.93957E+04 0.00170  6.87341E+03 0.00218 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.07427E+00 0.00056 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.22645E+18 0.00058  3.29128E+17 0.00120 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37679E-01 0.00013  1.52881E+00 0.00035 ];
INF_CAPT                  (idx, [1:   4]) = [  6.76524E-03 0.00052  3.18834E-02 0.00054 ];
INF_ABS                   (idx, [1:   4]) = [  8.94049E-03 0.00042  6.57239E-02 0.00091 ];
INF_FISS                  (idx, [1:   4]) = [  2.17525E-03 0.00053  3.38405E-02 0.00128 ];
INF_NSF                   (idx, [1:   4]) = [  5.63823E-03 0.00054  8.51883E-02 0.00129 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.59198E+00 5.4E-05  2.51735E+00 2.8E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04207E+02 5.7E-06  2.03330E+02 4.6E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.81806E-08 0.00039  2.55571E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28742E-01 0.00013  1.46311E+00 0.00040 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43780E-01 0.00018  3.88102E-01 0.00053 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61509E-02 0.00031  9.21247E-02 0.00084 ];
INF_SCATT3                (idx, [1:   4]) = [  7.38416E-03 0.00297  2.76978E-02 0.00220 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02826E-02 0.00162 -8.50417E-03 0.00607 ];
INF_SCATT5                (idx, [1:   4]) = [  1.35689E-04 0.11266  6.45126E-03 0.00697 ];
INF_SCATT6                (idx, [1:   4]) = [  5.05340E-03 0.00344 -1.65968E-02 0.00325 ];
INF_SCATT7                (idx, [1:   4]) = [  7.12510E-04 0.02598  3.97711E-04 0.11927 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28782E-01 0.00013  1.46311E+00 0.00040 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43781E-01 0.00018  3.88102E-01 0.00053 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61511E-02 0.00030  9.21247E-02 0.00084 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.38430E-03 0.00297  2.76978E-02 0.00220 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02827E-02 0.00162 -8.50417E-03 0.00607 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.35981E-04 0.11236  6.45126E-03 0.00697 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.05333E-03 0.00345 -1.65968E-02 0.00325 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.12646E-04 0.02598  3.97711E-04 0.11927 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13179E-01 0.00029  9.87525E-01 0.00030 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56363E+00 0.00029  3.37545E-01 0.00030 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.90010E-03 0.00044  6.57239E-02 0.00091 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69615E-02 0.00021  6.68767E-02 0.00099 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10717E-01 0.00013  1.80244E-02 0.00044  1.17307E-03 0.00400  1.46194E+00 0.00040 ];
INF_S1                    (idx, [1:   8]) = [  2.38522E-01 0.00018  5.25853E-03 0.00112  4.99977E-04 0.00740  3.87602E-01 0.00053 ];
INF_S2                    (idx, [1:   8]) = [  9.77215E-02 0.00028 -1.57067E-03 0.00258  2.73333E-04 0.00883  9.18514E-02 0.00085 ];
INF_S3                    (idx, [1:   8]) = [  9.23925E-03 0.00225 -1.85509E-03 0.00224  9.75708E-05 0.02022  2.76002E-02 0.00223 ];
INF_S4                    (idx, [1:   8]) = [ -9.66965E-03 0.00176 -6.12981E-04 0.00506  1.32819E-06 1.00000 -8.50550E-03 0.00612 ];
INF_S5                    (idx, [1:   8]) = [  1.15492E-04 0.13396  2.01970E-05 0.11635 -4.11593E-05 0.03318  6.49242E-03 0.00700 ];
INF_S6                    (idx, [1:   8]) = [  5.19697E-03 0.00337 -1.43564E-04 0.01537 -5.08699E-05 0.02518 -1.65459E-02 0.00326 ];
INF_S7                    (idx, [1:   8]) = [  8.84031E-04 0.02017 -1.71521E-04 0.01396 -4.70529E-05 0.03463  4.44764E-04 0.10512 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10758E-01 0.00013  1.80244E-02 0.00044  1.17307E-03 0.00400  1.46194E+00 0.00040 ];
INF_SP1                   (idx, [1:   8]) = [  2.38523E-01 0.00018  5.25853E-03 0.00112  4.99977E-04 0.00740  3.87602E-01 0.00053 ];
INF_SP2                   (idx, [1:   8]) = [  9.77218E-02 0.00028 -1.57067E-03 0.00258  2.73333E-04 0.00883  9.18514E-02 0.00085 ];
INF_SP3                   (idx, [1:   8]) = [  9.23939E-03 0.00225 -1.85509E-03 0.00224  9.75708E-05 0.02022  2.76002E-02 0.00223 ];
INF_SP4                   (idx, [1:   8]) = [ -9.66976E-03 0.00176 -6.12981E-04 0.00506  1.32819E-06 1.00000 -8.50550E-03 0.00612 ];
INF_SP5                   (idx, [1:   8]) = [  1.15784E-04 0.13359  2.01970E-05 0.11635 -4.11593E-05 0.03318  6.49242E-03 0.00700 ];
INF_SP6                   (idx, [1:   8]) = [  5.19689E-03 0.00338 -1.43564E-04 0.01537 -5.08699E-05 0.02518 -1.65459E-02 0.00326 ];
INF_SP7                   (idx, [1:   8]) = [  8.84168E-04 0.02017 -1.71521E-04 0.01396 -4.70529E-05 0.03463  4.44764E-04 0.10512 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32112E-01 0.00070  1.12933E+00 0.00677 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33783E-01 0.00091  1.21932E+00 0.00879 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33623E-01 0.00097  1.21010E+00 0.00783 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29001E-01 0.00100  9.91130E-01 0.00682 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43611E+00 0.00070  2.95488E-01 0.00682 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42585E+00 0.00091  2.73905E-01 0.00916 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42684E+00 0.00097  2.75867E-01 0.00788 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45563E+00 0.00100  3.36692E-01 0.00682 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.43149E-03 0.00856  1.83451E-04 0.04959  1.06089E-03 0.02068  1.00424E-03 0.02130  2.91706E-03 0.01164  9.46208E-04 0.02260  3.19639E-04 0.03587 ];
LAMBDA                    (idx, [1:  14]) = [  8.06526E-01 0.01878  1.24901E-02 8.6E-06  3.14338E-02 0.00050  1.10130E-01 0.00045  3.21347E-01 0.00036  1.34240E+00 0.00051  8.90759E+00 0.00348 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0005' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:52:32 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:37:24 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299952 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.94966E-01  1.00833E+00  9.96515E-01  9.99705E-01  1.00049E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13464E-02 0.00108  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88654E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.03484E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.03911E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67874E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.52957E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.52874E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.60675E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.15568E-01 0.00112  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000423 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00008E+04 0.00082 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00008E+04 0.00082 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.22309E+02 ;
RUNNING_TIME              (idx, 1)        =  4.48727E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.61967E-01  4.61967E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.17167E-01  9.16667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  4.39148E+01  3.09347E+00  2.46930E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  3.74867E-01  2.61833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.65833E-02  8.49998E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.48722E+01  1.19346E+02 ];
CPU_USAGE                 (idx, 1)        = 4.95420 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00012E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.74829E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.24391E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.81462E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.05531E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.65522E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.15528E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.58866E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.69907E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  7.50263E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.18322E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.53803E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.90266E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.96460E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.49296E+07 ;
SR90_ACTIVITY             (idx, 1)        =  5.93934E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.21084E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.25350E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.30954E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.89445E+12 ;
CS137_ACTIVITY            (idx, 1)        =  6.74825E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.05693E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.49757E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.16285E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.20924E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.33491E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 7 ;
BURNUP                     (idx, [1:  2])  = [  5.00000E+00  5.00044E+00 ];
BURN_DAYS                 (idx, 1)        =  1.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.30101E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  1.02172E+16 0.00073  7.41399E-01 0.00041 ];
U238_FISS                 (idx, [1:   4]) = [  9.27896E+14 0.00275  6.73233E-02 0.00259 ];
PU239_FISS                (idx, [1:   4]) = [  2.54650E+15 0.00158  1.84787E-01 0.00148 ];
PU240_FISS                (idx, [1:   4]) = [  1.40422E+12 0.06718  1.01925E-04 0.06713 ];
PU241_FISS                (idx, [1:   4]) = [  8.28911E+13 0.00934  6.01567E-03 0.00935 ];
U235_CAPT                 (idx, [1:   4]) = [  2.26604E+15 0.00164  1.17178E-01 0.00157 ];
U238_CAPT                 (idx, [1:   4]) = [  8.24621E+15 0.00101  4.26376E-01 0.00064 ];
PU239_CAPT                (idx, [1:   4]) = [  1.42328E+15 0.00231  7.35907E-02 0.00216 ];
PU240_CAPT                (idx, [1:   4]) = [  4.82850E+14 0.00381  2.49654E-02 0.00372 ];
PU241_CAPT                (idx, [1:   4]) = [  3.05831E+13 0.01513  1.58166E-03 0.01514 ];
XE135_CAPT                (idx, [1:   4]) = [  7.25025E+14 0.00303  3.74921E-02 0.00300 ];
SM149_CAPT                (idx, [1:   4]) = [  1.73847E+14 0.00620  8.99039E-03 0.00621 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000423 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.51086E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000423 5.00751E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2895347 2.89954E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2063395 2.06629E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 41681 4.16867E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000423 5.00751E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.70552E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.51027E+16 1.7E-05  3.51027E+16 1.7E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37914E+16 2.8E-06  1.37914E+16 2.8E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.93142E+16 0.00047  1.40846E+16 0.00046  5.22965E+15 0.00110 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.31056E+16 0.00027  2.78760E+16 0.00023  5.22965E+15 0.00110 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.33491E+16 0.00051  3.33491E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.57699E+18 0.00048  4.22934E+17 0.00044  1.15406E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.78085E+14 0.00549 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.33837E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.17844E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11932E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11932E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.63757E+00 0.00049 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.85559E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.61526E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24650E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94573E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97074E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.06069E+00 0.00056 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.05185E+00 0.00057 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.54526E+00 1.9E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03680E+02 2.8E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.05163E+00 0.00058  1.04533E+00 0.00057  6.51700E-03 0.00946 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.05313E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.05272E+00 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.05313E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.06199E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73291E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73319E+01 8.5E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.97567E-07 0.00350 ];
IMP_EALF                  (idx, [1:   2]) = [  5.94424E-07 0.00147 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.29430E-01 0.00282 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.28550E-01 0.00122 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.96926E-03 0.00577  1.63253E-04 0.03281  9.87208E-04 0.01393  9.38677E-04 0.01381  2.71565E-03 0.00840  8.83199E-04 0.01543  2.81272E-04 0.02710 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.90945E-01 0.01421  1.04713E-02 0.01968  3.13807E-02 0.00034  1.10200E-01 0.00036  3.21273E-01 0.00025  1.34039E+00 0.00053  8.38274E+00 0.01240 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.27989E-03 0.00824  1.77503E-04 0.04553  1.04641E-03 0.02000  9.91368E-04 0.02098  2.82882E-03 0.01243  9.32062E-04 0.02229  3.03721E-04 0.03818 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.02174E-01 0.02083  1.24941E-02 0.00019  3.13772E-02 0.00048  1.10221E-01 0.00051  3.21163E-01 0.00035  1.33898E+00 0.00084  8.98196E+00 0.00337 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.46156E-05 0.00123  2.46072E-05 0.00124  2.59915E-05 0.01198 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.58829E-05 0.00113  2.58740E-05 0.00114  2.73366E-05 0.01200 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.19835E-03 0.00950  1.82389E-04 0.05032  1.05319E-03 0.02162  9.40701E-04 0.02349  2.80354E-03 0.01306  9.14304E-04 0.02399  3.04224E-04 0.04095 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.08834E-01 0.02191  1.24914E-02 0.00011  3.14062E-02 0.00057  1.10227E-01 0.00062  3.21226E-01 0.00043  1.33834E+00 0.00100  8.94941E+00 0.00411 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.41687E-05 0.00280  2.41634E-05 0.00281  2.49924E-05 0.03584 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.54115E-05 0.00272  2.54060E-05 0.00273  2.62507E-05 0.03567 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.81911E-03 0.02987  1.84590E-04 0.18152  9.61190E-04 0.07343  9.81747E-04 0.07535  2.46578E-03 0.04363  9.16535E-04 0.07695  3.09270E-04 0.13105 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.40696E-01 0.06905  1.24893E-02 3.9E-05  3.14769E-02 0.00136  1.09965E-01 0.00131  3.21398E-01 0.00120  1.34389E+00 0.00075  8.84679E+00 0.01002 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.84285E-03 0.02941  1.87197E-04 0.16660  9.66135E-04 0.07239  9.75834E-04 0.07509  2.51695E-03 0.04330  9.04674E-04 0.07543  2.92054E-04 0.12808 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.17720E-01 0.06675  1.24893E-02 3.9E-05  3.14749E-02 0.00136  1.09984E-01 0.00131  3.21444E-01 0.00119  1.34393E+00 0.00075  8.85331E+00 0.01004 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.42445E+02 0.03021 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.43861E-05 0.00082 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.56407E-05 0.00056 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.08330E-03 0.00641 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.49456E+02 0.00635 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.83452E-07 0.00069 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.81389E-06 0.00044  2.81383E-06 0.00044  2.82595E-06 0.00537 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.85957E-05 0.00085  3.86175E-05 0.00085  3.52934E-05 0.00933 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.59359E-01 0.00031  6.59021E-01 0.00032  7.28079E-01 0.00906 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04358E+01 0.01324 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.52874E+01 0.00045  3.38753E+01 0.00046 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.47881E+04 0.00350  2.95988E+05 0.00112  6.06749E+05 0.00082  6.51183E+05 0.00088  5.97701E+05 0.00087  6.41911E+05 0.00054  4.35853E+05 0.00068  3.86383E+05 0.00074  2.95023E+05 0.00064  2.40984E+05 0.00080  2.08040E+05 0.00075  1.87346E+05 0.00100  1.73145E+05 0.00091  1.64410E+05 0.00083  1.60256E+05 0.00088  1.38542E+05 0.00090  1.36832E+05 0.00091  1.35530E+05 0.00087  1.33178E+05 0.00087  2.60077E+05 0.00053  2.50831E+05 0.00076  1.81001E+05 0.00061  1.17140E+05 0.00092  1.35250E+05 0.00093  1.27850E+05 0.00088  1.15480E+05 0.00094  1.88800E+05 0.00067  4.33173E+04 0.00153  5.43783E+04 0.00140  4.95536E+04 0.00150  2.87387E+04 0.00183  4.98236E+04 0.00164  3.36872E+04 0.00142  2.83800E+04 0.00180  5.32116E+03 0.00425  5.04455E+03 0.00307  4.86190E+03 0.00375  4.79155E+03 0.00354  4.84996E+03 0.00392  5.06545E+03 0.00383  5.50141E+03 0.00367  5.24805E+03 0.00404  1.00207E+04 0.00224  1.60958E+04 0.00237  2.05796E+04 0.00170  5.43893E+04 0.00146  5.72514E+04 0.00156  6.16494E+04 0.00141  4.08245E+04 0.00148  2.95138E+04 0.00105  2.22433E+04 0.00220  2.62736E+04 0.00111  5.06958E+04 0.00149  7.03905E+04 0.00143  1.37411E+05 0.00113  2.13532E+05 0.00096  3.20020E+05 0.00125  2.04623E+05 0.00128  1.46413E+05 0.00116  1.05294E+05 0.00137  9.45559E+04 0.00123  9.31771E+04 0.00114  7.78684E+04 0.00151  5.26767E+04 0.00143  4.85760E+04 0.00155  4.33189E+04 0.00138  3.65145E+04 0.00190  2.88311E+04 0.00177  1.92903E+04 0.00177  6.83210E+03 0.00248 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.06157E+00 0.00050 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.24557E+18 0.00051  3.31451E+17 0.00096 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37830E-01 0.00011  1.53431E+00 0.00035 ];
INF_CAPT                  (idx, [1:   4]) = [  6.87761E-03 0.00065  3.24287E-02 0.00044 ];
INF_ABS                   (idx, [1:   4]) = [  9.01964E-03 0.00053  6.59941E-02 0.00070 ];
INF_FISS                  (idx, [1:   4]) = [  2.14203E-03 0.00044  3.35654E-02 0.00097 ];
INF_NSF                   (idx, [1:   4]) = [  5.56800E-03 0.00045  8.49969E-02 0.00098 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.59940E+00 4.6E-05  2.53228E+00 3.2E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04305E+02 4.9E-06  2.03530E+02 5.4E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.79337E-08 0.00044  2.55842E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28803E-01 0.00011  1.46830E+00 0.00040 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43824E-01 0.00019  3.89368E-01 0.00045 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61504E-02 0.00026  9.23497E-02 0.00094 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34482E-03 0.00287  2.78346E-02 0.00235 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02980E-02 0.00182 -8.60276E-03 0.00658 ];
INF_SCATT5                (idx, [1:   4]) = [  1.73590E-04 0.09694  6.50316E-03 0.00775 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10605E-03 0.00306 -1.67236E-02 0.00283 ];
INF_SCATT7                (idx, [1:   4]) = [  7.66279E-04 0.02263  2.61108E-04 0.15467 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28843E-01 0.00011  1.46830E+00 0.00040 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43825E-01 0.00019  3.89368E-01 0.00045 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61506E-02 0.00026  9.23497E-02 0.00094 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34488E-03 0.00287  2.78346E-02 0.00235 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02980E-02 0.00182 -8.60276E-03 0.00658 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.73756E-04 0.09685  6.50316E-03 0.00775 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10591E-03 0.00306 -1.67236E-02 0.00283 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.66311E-04 0.02264  2.61108E-04 0.15467 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13086E-01 0.00021  9.91527E-01 0.00031 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56432E+00 0.00021  3.36183E-01 0.00031 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.97942E-03 0.00050  6.59941E-02 0.00070 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69803E-02 0.00024  6.71758E-02 0.00089 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10850E-01 0.00011  1.79533E-02 0.00036  1.16998E-03 0.00431  1.46713E+00 0.00040 ];
INF_S1                    (idx, [1:   8]) = [  2.38596E-01 0.00018  5.22769E-03 0.00108  5.00487E-04 0.00735  3.88868E-01 0.00045 ];
INF_S2                    (idx, [1:   8]) = [  9.77265E-02 0.00025 -1.57606E-03 0.00176  2.72020E-04 0.00638  9.20777E-02 0.00095 ];
INF_S3                    (idx, [1:   8]) = [  9.19558E-03 0.00221 -1.85076E-03 0.00199  9.78917E-05 0.01817  2.77367E-02 0.00236 ];
INF_S4                    (idx, [1:   8]) = [ -9.69295E-03 0.00198 -6.05001E-04 0.00441 -6.62035E-08 1.00000 -8.60269E-03 0.00659 ];
INF_S5                    (idx, [1:   8]) = [  1.45637E-04 0.12109  2.79529E-05 0.07787 -3.88406E-05 0.04230  6.54200E-03 0.00760 ];
INF_S6                    (idx, [1:   8]) = [  5.24667E-03 0.00290 -1.40615E-04 0.01517 -4.83038E-05 0.02804 -1.66753E-02 0.00280 ];
INF_S7                    (idx, [1:   8]) = [  9.39094E-04 0.01807 -1.72815E-04 0.01575 -4.42709E-05 0.02562  3.05379E-04 0.13100 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10890E-01 0.00011  1.79533E-02 0.00036  1.16998E-03 0.00431  1.46713E+00 0.00040 ];
INF_SP1                   (idx, [1:   8]) = [  2.38597E-01 0.00018  5.22769E-03 0.00108  5.00487E-04 0.00735  3.88868E-01 0.00045 ];
INF_SP2                   (idx, [1:   8]) = [  9.77266E-02 0.00025 -1.57606E-03 0.00176  2.72020E-04 0.00638  9.20777E-02 0.00095 ];
INF_SP3                   (idx, [1:   8]) = [  9.19564E-03 0.00220 -1.85076E-03 0.00199  9.78917E-05 0.01817  2.77367E-02 0.00236 ];
INF_SP4                   (idx, [1:   8]) = [ -9.69302E-03 0.00198 -6.05001E-04 0.00441 -6.62035E-08 1.00000 -8.60269E-03 0.00659 ];
INF_SP5                   (idx, [1:   8]) = [  1.45803E-04 0.12092  2.79529E-05 0.07787 -3.88406E-05 0.04230  6.54200E-03 0.00760 ];
INF_SP6                   (idx, [1:   8]) = [  5.24653E-03 0.00290 -1.40615E-04 0.01517 -4.83038E-05 0.02804 -1.66753E-02 0.00280 ];
INF_SP7                   (idx, [1:   8]) = [  9.39127E-04 0.01809 -1.72815E-04 0.01575 -4.42709E-05 0.02562  3.05379E-04 0.13100 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32175E-01 0.00061  1.13566E+00 0.00737 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33735E-01 0.00101  1.22115E+00 0.00955 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33777E-01 0.00086  1.22142E+00 0.00913 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29082E-01 0.00075  9.97267E-01 0.00736 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43571E+00 0.00061  2.93886E-01 0.00713 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42615E+00 0.00101  2.73545E-01 0.00925 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42588E+00 0.00086  2.73437E-01 0.00889 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45510E+00 0.00075  3.34675E-01 0.00726 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.27989E-03 0.00824  1.77503E-04 0.04553  1.04641E-03 0.02000  9.91368E-04 0.02098  2.82882E-03 0.01243  9.32062E-04 0.02229  3.03721E-04 0.03818 ];
LAMBDA                    (idx, [1:  14]) = [  8.02174E-01 0.02083  1.24941E-02 0.00019  3.13772E-02 0.00048  1.10221E-01 0.00051  3.21163E-01 0.00035  1.33898E+00 0.00084  8.98196E+00 0.00337 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0005' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:52:32 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:43:06 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299952 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.86497E-01  1.01443E+00  9.99650E-01  1.01099E+00  9.88435E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 9.3E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13314E-02 0.00104  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88669E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04188E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04615E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67573E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.52143E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.52059E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.54852E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.13448E-01 0.00109  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000985 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00020E+04 0.00078 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00020E+04 0.00078 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.50781E+02 ;
RUNNING_TIME              (idx, 1)        =  5.05734E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.61967E-01  4.61967E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.36267E-01  9.33333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  4.95418E+01  3.13598E+00  2.49105E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  4.28700E-01  2.73667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.90833E-02  9.16668E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  5.05732E+01  1.18468E+02 ];
CPU_USAGE                 (idx, 1)        = 4.95875 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99935E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75927E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.27872E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.80496E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.33084E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.67315E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.16775E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.60555E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.68816E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  8.43177E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.25139E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.12717E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.99380E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.30460E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.55201E+07 ;
SR90_ACTIVITY             (idx, 1)        =  7.03048E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.23304E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.28465E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.33971E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.76425E+12 ;
CS137_ACTIVITY            (idx, 1)        =  8.09654E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.08258E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.47590E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.70130E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.21713E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.38466E+12 0.00048  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 8 ;
BURNUP                     (idx, [1:  2])  = [  6.00000E+00  6.00053E+00 ];
BURN_DAYS                 (idx, 1)        =  1.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.39604E-01 0.00105 ];
U235_FISS                 (idx, [1:   4]) = [  9.82572E+15 0.00073  7.13154E-01 0.00046 ];
U238_FISS                 (idx, [1:   4]) = [  9.37637E+14 0.00287  6.80412E-02 0.00268 ];
PU239_FISS                (idx, [1:   4]) = [  2.88362E+15 0.00155  2.09288E-01 0.00140 ];
PU240_FISS                (idx, [1:   4]) = [  1.69329E+12 0.06293  1.22819E-04 0.06290 ];
PU241_FISS                (idx, [1:   4]) = [  1.23646E+14 0.00738  8.97367E-03 0.00734 ];
U235_CAPT                 (idx, [1:   4]) = [  2.17949E+15 0.00175  1.09881E-01 0.00170 ];
U238_CAPT                 (idx, [1:   4]) = [  8.32832E+15 0.00095  4.19845E-01 0.00066 ];
PU239_CAPT                (idx, [1:   4]) = [  1.60888E+15 0.00209  8.11112E-02 0.00203 ];
PU240_CAPT                (idx, [1:   4]) = [  6.12732E+14 0.00333  3.08888E-02 0.00325 ];
PU241_CAPT                (idx, [1:   4]) = [  4.56465E+13 0.01197  2.30131E-03 0.01197 ];
XE135_CAPT                (idx, [1:   4]) = [  7.26205E+14 0.00310  3.66131E-02 0.00309 ];
SM149_CAPT                (idx, [1:   4]) = [  1.77889E+14 0.00610  8.96918E-03 0.00611 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000985 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.65745E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000985 5.00766E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2926381 2.93030E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2032687 2.03543E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 41917 4.19260E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000985 5.00766E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -1.76951E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.52444E+16 1.8E-05  3.52444E+16 1.8E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37801E+16 3.1E-06  1.37801E+16 3.1E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.98151E+16 0.00043  1.45306E+16 0.00043  5.28450E+15 0.00110 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.35952E+16 0.00025  2.83107E+16 0.00022  5.28450E+15 0.00110 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.38466E+16 0.00048  3.38466E+16 0.00048  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.59822E+18 0.00046  4.27985E+17 0.00044  1.17023E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83843E+14 0.00505 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.38791E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.19326E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11815E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11815E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.62820E+00 0.00048 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.86237E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.58606E-01 0.00030 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24547E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94514E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97084E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.05001E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04120E+00 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.55762E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03847E+02 3.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04126E+00 0.00055  1.03492E+00 0.00055  6.28022E-03 0.00837 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.04195E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04142E+00 0.00048 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.04195E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.05076E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73102E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73058E+01 8.3E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.08979E-07 0.00350 ];
IMP_EALF                  (idx, [1:   2]) = [  6.10184E-07 0.00144 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.32175E-01 0.00276 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.32568E-01 0.00113 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.96103E-03 0.00570  1.74442E-04 0.03289  9.85515E-04 0.01478  9.62725E-04 0.01431  2.67384E-03 0.00870  8.91190E-04 0.01532  2.73319E-04 0.02534 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.84100E-01 0.01380  1.04184E-02 0.01997  3.13433E-02 0.00037  1.10203E-01 0.00035  3.21464E-01 0.00025  1.33700E+00 0.00075  8.54491E+00 0.01022 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.12795E-03 0.00827  1.79183E-04 0.04785  1.02486E-03 0.02110  9.64366E-04 0.02142  2.73999E-03 0.01235  9.39066E-04 0.02267  2.80479E-04 0.04085 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.85791E-01 0.02104  1.24925E-02 0.00021  3.13490E-02 0.00051  1.10264E-01 0.00053  3.21493E-01 0.00037  1.33755E+00 0.00086  8.88729E+00 0.00447 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.46518E-05 0.00124  2.46412E-05 0.00124  2.62862E-05 0.01329 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.56645E-05 0.00106  2.56534E-05 0.00107  2.73659E-05 0.01328 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.02090E-03 0.00855  1.93930E-04 0.05162  9.94010E-04 0.02295  9.43106E-04 0.02304  2.68249E-03 0.01275  9.36654E-04 0.02370  2.70709E-04 0.04118 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.77341E-01 0.02137  1.24899E-02 1.4E-05  3.13693E-02 0.00062  1.10253E-01 0.00060  3.21615E-01 0.00045  1.33766E+00 0.00111  8.86230E+00 0.00613 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.42678E-05 0.00287  2.42582E-05 0.00289  2.55409E-05 0.03229 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.52654E-05 0.00282  2.52554E-05 0.00284  2.65827E-05 0.03225 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.96875E-03 0.02945  1.32257E-04 0.19892  9.43003E-04 0.07610  9.39109E-04 0.07762  2.64094E-03 0.04599  9.91891E-04 0.07105  3.21546E-04 0.14014 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  9.38867E-01 0.07198  1.24902E-02 3.6E-05  3.13374E-02 0.00154  1.10201E-01 0.00144  3.21996E-01 0.00127  1.33430E+00 0.00315  8.92277E+00 0.01248 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.97862E-03 0.02898  1.33753E-04 0.20156  9.55796E-04 0.07710  9.57693E-04 0.07492  2.61980E-03 0.04504  9.79591E-04 0.06985  3.31986E-04 0.13529 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  9.41790E-01 0.07047  1.24902E-02 3.6E-05  3.13428E-02 0.00152  1.10204E-01 0.00144  3.21974E-01 0.00126  1.33430E+00 0.00315  8.92166E+00 0.01246 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.47360E+02 0.02976 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.44392E-05 0.00081 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.54434E-05 0.00054 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.94594E-03 0.00492 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.43419E+02 0.00507 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.80145E-07 0.00068 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.80057E-06 0.00042  2.80043E-06 0.00043  2.82278E-06 0.00525 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.84854E-05 0.00084  3.85033E-05 0.00084  3.56079E-05 0.00951 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.56407E-01 0.00030  6.56142E-01 0.00030  7.11885E-01 0.00880 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06011E+01 0.01303 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.52059E+01 0.00045  3.37604E+01 0.00046 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.57066E+04 0.00330  2.98010E+05 0.00116  6.07963E+05 0.00081  6.51724E+05 0.00075  5.98273E+05 0.00049  6.42566E+05 0.00060  4.35820E+05 0.00070  3.86395E+05 0.00063  2.95242E+05 0.00072  2.41133E+05 0.00076  2.08134E+05 0.00065  1.87453E+05 0.00073  1.72987E+05 0.00078  1.64557E+05 0.00086  1.60247E+05 0.00084  1.38366E+05 0.00091  1.36617E+05 0.00098  1.35633E+05 0.00091  1.33279E+05 0.00078  2.59815E+05 0.00061  2.50632E+05 0.00053  1.81032E+05 0.00064  1.16916E+05 0.00085  1.35186E+05 0.00073  1.27976E+05 0.00094  1.15361E+05 0.00108  1.88712E+05 0.00084  4.34269E+04 0.00165  5.44317E+04 0.00133  4.93071E+04 0.00178  2.87383E+04 0.00128  4.98668E+04 0.00147  3.37065E+04 0.00163  2.84027E+04 0.00209  5.25087E+03 0.00340  4.99417E+03 0.00310  4.70267E+03 0.00284  4.60405E+03 0.00352  4.67864E+03 0.00322  4.89366E+03 0.00295  5.40150E+03 0.00385  5.19479E+03 0.00337  9.90637E+03 0.00326  1.59208E+04 0.00277  2.04518E+04 0.00175  5.40594E+04 0.00179  5.69115E+04 0.00134  6.11631E+04 0.00145  4.03361E+04 0.00129  2.90571E+04 0.00141  2.18530E+04 0.00150  2.57926E+04 0.00156  4.98218E+04 0.00122  6.92295E+04 0.00091  1.35698E+05 0.00099  2.11454E+05 0.00096  3.16869E+05 0.00122  2.02783E+05 0.00153  1.45430E+05 0.00162  1.04624E+05 0.00191  9.41387E+04 0.00148  9.27358E+04 0.00134  7.74592E+04 0.00166  5.23977E+04 0.00180  4.84334E+04 0.00155  4.30263E+04 0.00164  3.64066E+04 0.00159  2.87132E+04 0.00187  1.92314E+04 0.00195  6.81212E+03 0.00240 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.05022E+00 0.00037 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.26479E+18 0.00040  3.33453E+17 0.00112 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37724E-01 7.7E-05  1.53896E+00 0.00040 ];
INF_CAPT                  (idx, [1:   4]) = [  6.98344E-03 0.00051  3.29387E-02 0.00052 ];
INF_ABS                   (idx, [1:   4]) = [  9.09287E-03 0.00042  6.62689E-02 0.00082 ];
INF_FISS                  (idx, [1:   4]) = [  2.10943E-03 0.00044  3.33302E-02 0.00112 ];
INF_NSF                   (idx, [1:   4]) = [  5.49903E-03 0.00044  8.48519E-02 0.00115 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.60688E+00 5.1E-05  2.54580E+00 4.0E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04400E+02 5.4E-06  2.03714E+02 6.8E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.77233E-08 0.00038  2.56181E-06 0.00020 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28624E-01 8.1E-05  1.47266E+00 0.00046 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43837E-01 0.00015  3.90829E-01 0.00052 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61940E-02 0.00024  9.26484E-02 0.00100 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32580E-03 0.00269  2.78744E-02 0.00239 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03133E-02 0.00170 -8.53314E-03 0.00681 ];
INF_SCATT5                (idx, [1:   4]) = [  1.33756E-04 0.10950  6.51152E-03 0.00887 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08480E-03 0.00287 -1.67494E-02 0.00331 ];
INF_SCATT7                (idx, [1:   4]) = [  7.52364E-04 0.02024  2.36726E-04 0.16223 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28665E-01 8.1E-05  1.47266E+00 0.00046 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43837E-01 0.00015  3.90829E-01 0.00052 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61942E-02 0.00024  9.26484E-02 0.00100 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32629E-03 0.00269  2.78744E-02 0.00239 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03132E-02 0.00170 -8.53314E-03 0.00681 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.33647E-04 0.10965  6.51152E-03 0.00887 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08474E-03 0.00288 -1.67494E-02 0.00331 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.52431E-04 0.02020  2.36726E-04 0.16223 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12915E-01 0.00022  9.94741E-01 0.00038 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56557E+00 0.00022  3.35097E-01 0.00038 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.05189E-03 0.00042  6.62689E-02 0.00082 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69652E-02 0.00017  6.74693E-02 0.00101 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10759E-01 7.7E-05  1.78649E-02 0.00036  1.17742E-03 0.00394  1.47149E+00 0.00046 ];
INF_S1                    (idx, [1:   8]) = [  2.38630E-01 0.00015  5.20669E-03 0.00083  5.03927E-04 0.00596  3.90325E-01 0.00053 ];
INF_S2                    (idx, [1:   8]) = [  9.77577E-02 0.00024 -1.56379E-03 0.00236  2.73935E-04 0.01095  9.23745E-02 0.00100 ];
INF_S3                    (idx, [1:   8]) = [  9.16550E-03 0.00213 -1.83970E-03 0.00132  1.00948E-04 0.01661  2.77734E-02 0.00240 ];
INF_S4                    (idx, [1:   8]) = [ -9.70922E-03 0.00172 -6.04058E-04 0.00481 -4.44827E-07 1.00000 -8.53270E-03 0.00683 ];
INF_S5                    (idx, [1:   8]) = [  1.09288E-04 0.12535  2.44680E-05 0.12205 -3.81950E-05 0.04686  6.54971E-03 0.00888 ];
INF_S6                    (idx, [1:   8]) = [  5.22225E-03 0.00277 -1.37451E-04 0.01773 -4.85177E-05 0.02607 -1.67009E-02 0.00334 ];
INF_S7                    (idx, [1:   8]) = [  9.20154E-04 0.01587 -1.67791E-04 0.01319 -4.62270E-05 0.02319  2.82953E-04 0.13608 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10800E-01 7.8E-05  1.78649E-02 0.00036  1.17742E-03 0.00394  1.47149E+00 0.00046 ];
INF_SP1                   (idx, [1:   8]) = [  2.38630E-01 0.00015  5.20669E-03 0.00083  5.03927E-04 0.00596  3.90325E-01 0.00053 ];
INF_SP2                   (idx, [1:   8]) = [  9.77580E-02 0.00024 -1.56379E-03 0.00236  2.73935E-04 0.01095  9.23745E-02 0.00100 ];
INF_SP3                   (idx, [1:   8]) = [  9.16599E-03 0.00213 -1.83970E-03 0.00132  1.00948E-04 0.01661  2.77734E-02 0.00240 ];
INF_SP4                   (idx, [1:   8]) = [ -9.70913E-03 0.00172 -6.04058E-04 0.00481 -4.44827E-07 1.00000 -8.53270E-03 0.00683 ];
INF_SP5                   (idx, [1:   8]) = [  1.09178E-04 0.12552  2.44680E-05 0.12205 -3.81950E-05 0.04686  6.54971E-03 0.00888 ];
INF_SP6                   (idx, [1:   8]) = [  5.22219E-03 0.00278 -1.37451E-04 0.01773 -4.85177E-05 0.02607 -1.67009E-02 0.00334 ];
INF_SP7                   (idx, [1:   8]) = [  9.20221E-04 0.01583 -1.67791E-04 0.01319 -4.62270E-05 0.02319  2.82953E-04 0.13608 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32257E-01 0.00055  1.14906E+00 0.00483 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33990E-01 0.00104  1.23966E+00 0.00629 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33911E-01 0.00066  1.23268E+00 0.00786 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28950E-01 0.00089  1.00853E+00 0.00611 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43520E+00 0.00055  2.90255E-01 0.00481 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42460E+00 0.00104  2.69152E-01 0.00641 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42506E+00 0.00066  2.70814E-01 0.00782 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45595E+00 0.00089  3.30800E-01 0.00593 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.12795E-03 0.00827  1.79183E-04 0.04785  1.02486E-03 0.02110  9.64366E-04 0.02142  2.73999E-03 0.01235  9.39066E-04 0.02267  2.80479E-04 0.04085 ];
LAMBDA                    (idx, [1:  14]) = [  7.85791E-01 0.02104  1.24925E-02 0.00021  3.13490E-02 0.00051  1.10264E-01 0.00053  3.21493E-01 0.00037  1.33755E+00 0.00086  8.88729E+00 0.00447 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0005' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:52:32 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:48:44 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299952 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.96356E-01  1.00405E+00  9.94606E-01  1.00730E+00  9.97685E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.3E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13136E-02 0.00106  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88686E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04609E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05036E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67282E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.51121E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.51037E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.50091E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.10934E-01 0.00112  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000745 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00015E+04 0.00080 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00015E+04 0.00080 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.78846E+02 ;
RUNNING_TIME              (idx, 1)        =  5.61926E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.61967E-01  4.61967E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.55800E-01  9.33333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  5.50857E+01  3.12170E+00  2.42223E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  4.84133E-01  2.65500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.08000E-02  8.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  5.61924E+01  1.18122E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96233 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99955E+00 0.00023 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76800E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.30945E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.79652E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.77452E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.69142E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.18047E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61800E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.67845E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  9.39436E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.30998E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.79336E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.08770E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.60099E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.60121E+07 ;
SR90_ACTIVITY             (idx, 1)        =  8.09624E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.25289E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.31215E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.36658E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.79215E+12 ;
CS137_ACTIVITY            (idx, 1)        =  9.44389E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.10656E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.45655E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.46571E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.22446E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.43411E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 9 ;
BURNUP                     (idx, [1:  2])  = [  7.00000E+00  7.00061E+00 ];
BURN_DAYS                 (idx, 1)        =  1.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.48818E-01 0.00111 ];
U235_FISS                 (idx, [1:   4]) = [  9.45845E+15 0.00078  6.86337E-01 0.00051 ];
U238_FISS                 (idx, [1:   4]) = [  9.54951E+14 0.00280  6.92852E-02 0.00265 ];
PU239_FISS                (idx, [1:   4]) = [  3.18668E+15 0.00151  2.31230E-01 0.00134 ];
PU240_FISS                (idx, [1:   4]) = [  2.37551E+12 0.05382  1.72041E-04 0.05380 ];
PU241_FISS                (idx, [1:   4]) = [  1.72013E+14 0.00614  1.24814E-02 0.00610 ];
U235_CAPT                 (idx, [1:   4]) = [  2.10106E+15 0.00172  1.03387E-01 0.00163 ];
U238_CAPT                 (idx, [1:   4]) = [  8.40257E+15 0.00106  4.13431E-01 0.00071 ];
PU239_CAPT                (idx, [1:   4]) = [  1.77592E+15 0.00192  8.73924E-02 0.00190 ];
PU240_CAPT                (idx, [1:   4]) = [  7.40588E+14 0.00314  3.64407E-02 0.00307 ];
PU241_CAPT                (idx, [1:   4]) = [  6.14034E+13 0.01072  3.02257E-03 0.01078 ];
XE135_CAPT                (idx, [1:   4]) = [  7.33058E+14 0.00317  3.60746E-02 0.00318 ];
SM149_CAPT                (idx, [1:   4]) = [  1.82696E+14 0.00569  8.99013E-03 0.00567 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000745 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.64057E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000745 5.00764E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2954873 2.95897E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2003801 2.00660E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 42071 4.20726E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000745 5.00764E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.72529E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.53789E+16 1.8E-05  3.53789E+16 1.8E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37693E+16 3.2E-06  1.37693E+16 3.2E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.03040E+16 0.00043  1.49706E+16 0.00044  5.33333E+15 0.00107 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.40732E+16 0.00025  2.87399E+16 0.00023  5.33333E+15 0.00107 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.43411E+16 0.00050  3.43411E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.61774E+18 0.00046  4.32835E+17 0.00047  1.18490E+18 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.88996E+14 0.00478 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.43622E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.20717E+18 0.00058 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11699E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11699E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.61950E+00 0.00049 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.86295E-01 0.00029 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.55640E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24566E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94558E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97012E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.03993E+00 0.00054 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.03118E+00 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.56941E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04007E+02 3.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03119E+00 0.00056  1.02507E+00 0.00055  6.10887E-03 0.00916 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.03121E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03035E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.03121E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.03997E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72790E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72825E+01 8.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.28510E-07 0.00366 ];
IMP_EALF                  (idx, [1:   2]) = [  6.24554E-07 0.00154 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.36556E-01 0.00286 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.35843E-01 0.00118 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.85447E-03 0.00559  1.51553E-04 0.03604  9.84299E-04 0.01287  9.27587E-04 0.01438  2.64711E-03 0.00832  8.68023E-04 0.01465  2.75894E-04 0.02785 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.86090E-01 0.01433  9.72272E-03 0.02391  3.12990E-02 0.00040  1.10156E-01 0.00037  3.21736E-01 0.00027  1.33612E+00 0.00068  8.27870E+00 0.01306 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.99964E-03 0.00833  1.55939E-04 0.05533  1.00190E-03 0.01986  9.65597E-04 0.02310  2.69491E-03 0.01230  9.07099E-04 0.02380  2.74197E-04 0.03947 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.77047E-01 0.01982  1.24952E-02 0.00029  3.12969E-02 0.00055  1.10108E-01 0.00049  3.21741E-01 0.00039  1.33758E+00 0.00081  8.92746E+00 0.00408 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.46886E-05 0.00126  2.46805E-05 0.00126  2.61549E-05 0.01459 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.54542E-05 0.00109  2.54458E-05 0.00109  2.69603E-05 0.01451 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.93695E-03 0.00936  1.52138E-04 0.06079  9.91057E-04 0.02213  9.35200E-04 0.02321  2.70617E-03 0.01432  8.67075E-04 0.02565  2.85308E-04 0.04343 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.93024E-01 0.02323  1.24924E-02 0.00020  3.13030E-02 0.00066  1.10155E-01 0.00064  3.21780E-01 0.00045  1.33764E+00 0.00095  8.96279E+00 0.00512 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.41740E-05 0.00285  2.41586E-05 0.00285  2.54657E-05 0.03473 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.49226E-05 0.00274  2.49068E-05 0.00275  2.62422E-05 0.03462 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.02481E-03 0.03099  1.90931E-04 0.17842  1.03563E-03 0.07421  9.05679E-04 0.07662  2.77085E-03 0.04328  8.51067E-04 0.08575  2.70653E-04 0.13937 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.57499E-01 0.07334  1.24902E-02 3.2E-05  3.12137E-02 0.00159  1.10436E-01 0.00164  3.22174E-01 0.00133  1.33130E+00 0.00357  8.81759E+00 0.01317 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.97316E-03 0.03043  1.85021E-04 0.18181  1.02428E-03 0.07166  8.88166E-04 0.07473  2.75815E-03 0.04203  8.38902E-04 0.08315  2.78643E-04 0.13544 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.51769E-01 0.07050  1.24902E-02 3.2E-05  3.12223E-02 0.00158  1.10430E-01 0.00163  3.22186E-01 0.00131  1.33142E+00 0.00354  8.83270E+00 0.01324 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.52591E+02 0.03194 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.44376E-05 0.00078 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.51958E-05 0.00050 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.89672E-03 0.00592 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.41373E+02 0.00598 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.77381E-07 0.00070 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.78773E-06 0.00044  2.78752E-06 0.00044  2.82106E-06 0.00547 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.83557E-05 0.00086  3.83782E-05 0.00086  3.47884E-05 0.00989 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.53555E-01 0.00033  6.53327E-01 0.00033  7.03165E-01 0.00870 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05071E+01 0.01339 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.51037E+01 0.00046  3.36300E+01 0.00050 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.56254E+04 0.00281  2.98270E+05 0.00126  6.07131E+05 0.00096  6.50521E+05 0.00082  5.98871E+05 0.00061  6.41953E+05 0.00070  4.36007E+05 0.00079  3.85410E+05 0.00060  2.95081E+05 0.00069  2.41355E+05 0.00088  2.07763E+05 0.00084  1.87381E+05 0.00074  1.73061E+05 0.00081  1.64450E+05 0.00083  1.60356E+05 0.00082  1.38274E+05 0.00080  1.36582E+05 0.00100  1.35415E+05 0.00111  1.33050E+05 0.00105  2.60045E+05 0.00067  2.50706E+05 0.00066  1.80762E+05 0.00056  1.17101E+05 0.00103  1.35412E+05 0.00080  1.28064E+05 0.00083  1.15227E+05 0.00087  1.88215E+05 0.00072  4.32818E+04 0.00181  5.43425E+04 0.00150  4.92544E+04 0.00167  2.86880E+04 0.00189  4.98124E+04 0.00161  3.35290E+04 0.00181  2.82438E+04 0.00183  5.19746E+03 0.00414  4.84761E+03 0.00348  4.53854E+03 0.00355  4.38961E+03 0.00279  4.49349E+03 0.00321  4.76281E+03 0.00296  5.28212E+03 0.00269  5.10527E+03 0.00435  9.86037E+03 0.00264  1.58100E+04 0.00241  2.02808E+04 0.00227  5.37749E+04 0.00125  5.63412E+04 0.00113  6.06196E+04 0.00142  3.99624E+04 0.00137  2.86190E+04 0.00181  2.14784E+04 0.00260  2.53433E+04 0.00163  4.90464E+04 0.00151  6.84454E+04 0.00131  1.34303E+05 0.00094  2.09390E+05 0.00095  3.14454E+05 0.00106  2.01382E+05 0.00111  1.44477E+05 0.00132  1.04049E+05 0.00132  9.35528E+04 0.00138  9.21389E+04 0.00095  7.69771E+04 0.00136  5.21016E+04 0.00137  4.81171E+04 0.00122  4.27787E+04 0.00120  3.61726E+04 0.00142  2.85824E+04 0.00144  1.91268E+04 0.00149  6.76545E+03 0.00236 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.03909E+00 0.00045 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.28222E+18 0.00038  3.35551E+17 0.00099 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37960E-01 0.00012  1.54199E+00 0.00031 ];
INF_CAPT                  (idx, [1:   4]) = [  7.08857E-03 0.00076  3.34258E-02 0.00050 ];
INF_ABS                   (idx, [1:   4]) = [  9.16736E-03 0.00061  6.65229E-02 0.00076 ];
INF_FISS                  (idx, [1:   4]) = [  2.07879E-03 0.00047  3.30970E-02 0.00102 ];
INF_NSF                   (idx, [1:   4]) = [  5.43354E-03 0.00047  8.46871E-02 0.00104 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.61379E+00 5.9E-05  2.55875E+00 3.8E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04493E+02 7.2E-06  2.03891E+02 6.4E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.75260E-08 0.00044  2.56401E-06 0.00013 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28773E-01 0.00012  1.47546E+00 0.00035 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43866E-01 0.00022  3.91509E-01 0.00048 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61778E-02 0.00032  9.27328E-02 0.00094 ];
INF_SCATT3                (idx, [1:   4]) = [  7.30896E-03 0.00279  2.77917E-02 0.00271 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03024E-02 0.00197 -8.78112E-03 0.00517 ];
INF_SCATT5                (idx, [1:   4]) = [  1.79584E-04 0.12191  6.43338E-03 0.00734 ];
INF_SCATT6                (idx, [1:   4]) = [  5.11639E-03 0.00289 -1.67504E-02 0.00272 ];
INF_SCATT7                (idx, [1:   4]) = [  7.44544E-04 0.02365  4.44789E-04 0.09769 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28814E-01 0.00012  1.47546E+00 0.00035 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43867E-01 0.00022  3.91509E-01 0.00048 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61778E-02 0.00032  9.27328E-02 0.00094 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.30898E-03 0.00279  2.77917E-02 0.00271 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03023E-02 0.00197 -8.78112E-03 0.00517 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.79561E-04 0.12189  6.43338E-03 0.00734 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.11661E-03 0.00289 -1.67504E-02 0.00272 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.44685E-04 0.02367  4.44789E-04 0.09769 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12942E-01 0.00025  9.97168E-01 0.00027 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56537E+00 0.00025  3.34280E-01 0.00027 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.12644E-03 0.00063  6.65229E-02 0.00076 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69853E-02 0.00022  6.77121E-02 0.00078 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10974E-01 0.00012  1.77992E-02 0.00032  1.17589E-03 0.00463  1.47428E+00 0.00035 ];
INF_S1                    (idx, [1:   8]) = [  2.38694E-01 0.00022  5.17199E-03 0.00071  5.08521E-04 0.00812  3.91000E-01 0.00048 ];
INF_S2                    (idx, [1:   8]) = [  9.77486E-02 0.00032 -1.57084E-03 0.00222  2.77036E-04 0.00953  9.24557E-02 0.00094 ];
INF_S3                    (idx, [1:   8]) = [  9.14514E-03 0.00219 -1.83618E-03 0.00219  9.83203E-05 0.01837  2.76933E-02 0.00271 ];
INF_S4                    (idx, [1:   8]) = [ -9.70676E-03 0.00197 -5.95651E-04 0.00479 -1.59796E-06 0.94853 -8.77952E-03 0.00516 ];
INF_S5                    (idx, [1:   8]) = [  1.48839E-04 0.13856  3.07453E-05 0.10044 -4.07397E-05 0.03122  6.47412E-03 0.00734 ];
INF_S6                    (idx, [1:   8]) = [  5.25180E-03 0.00266 -1.35406E-04 0.02025 -4.94246E-05 0.02521 -1.67010E-02 0.00274 ];
INF_S7                    (idx, [1:   8]) = [  9.16117E-04 0.01837 -1.71574E-04 0.01510 -4.48312E-05 0.02476  4.89620E-04 0.08796 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11015E-01 0.00012  1.77992E-02 0.00032  1.17589E-03 0.00463  1.47428E+00 0.00035 ];
INF_SP1                   (idx, [1:   8]) = [  2.38695E-01 0.00022  5.17199E-03 0.00071  5.08521E-04 0.00812  3.91000E-01 0.00048 ];
INF_SP2                   (idx, [1:   8]) = [  9.77487E-02 0.00032 -1.57084E-03 0.00222  2.77036E-04 0.00953  9.24557E-02 0.00094 ];
INF_SP3                   (idx, [1:   8]) = [  9.14516E-03 0.00219 -1.83618E-03 0.00219  9.83203E-05 0.01837  2.76933E-02 0.00271 ];
INF_SP4                   (idx, [1:   8]) = [ -9.70666E-03 0.00197 -5.95651E-04 0.00479 -1.59796E-06 0.94853 -8.77952E-03 0.00516 ];
INF_SP5                   (idx, [1:   8]) = [  1.48816E-04 0.13855  3.07453E-05 0.10044 -4.07397E-05 0.03122  6.47412E-03 0.00734 ];
INF_SP6                   (idx, [1:   8]) = [  5.25202E-03 0.00266 -1.35406E-04 0.02025 -4.94246E-05 0.02521 -1.67010E-02 0.00274 ];
INF_SP7                   (idx, [1:   8]) = [  9.16258E-04 0.01838 -1.71574E-04 0.01510 -4.48312E-05 0.02476  4.89620E-04 0.08796 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32173E-01 0.00055  1.15015E+00 0.00608 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33938E-01 0.00083  1.25081E+00 0.00740 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33397E-01 0.00092  1.22525E+00 0.00693 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29249E-01 0.00080  1.00853E+00 0.00798 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43572E+00 0.00055  2.90077E-01 0.00615 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42490E+00 0.00082  2.66848E-01 0.00749 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42821E+00 0.00092  2.72371E-01 0.00701 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45405E+00 0.00080  3.31013E-01 0.00788 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.99964E-03 0.00833  1.55939E-04 0.05533  1.00190E-03 0.01986  9.65597E-04 0.02310  2.69491E-03 0.01230  9.07099E-04 0.02380  2.74197E-04 0.03947 ];
LAMBDA                    (idx, [1:  14]) = [  7.77047E-01 0.01982  1.24952E-02 0.00029  3.12969E-02 0.00055  1.10108E-01 0.00049  3.21741E-01 0.00039  1.33758E+00 0.00081  8.92746E+00 0.00408 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0005' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:52:32 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:54:23 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299952 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99273E-01  1.00395E+00  9.93721E-01  1.00716E+00  9.95900E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13239E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88676E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05125E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05555E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.66943E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.51002E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.50917E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.46986E+00 0.00036  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.10474E-01 0.00111  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000863 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00017E+04 0.00083 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00017E+04 0.00083 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.07108E+02 ;
RUNNING_TIME              (idx, 1)        =  6.18504E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.61967E-01  4.61967E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.74517E-01  9.46667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.06729E+01  3.13360E+00  2.45350E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.35833E-01  2.66500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.32167E-02  8.83333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  6.18502E+01  1.18325E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96533 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99901E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.77531E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.33648E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.78873E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.44686E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.70961E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.19318E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62684E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.66939E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.03980E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.36143E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.53412E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.18231E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.86391E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.64320E+07 ;
SR90_ACTIVITY             (idx, 1)        =  9.13771E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.27174E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.33767E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.39206E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.96675E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.07903E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.12857E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.43866E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.52142E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.23129E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.47937E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 10 ;
BURNUP                     (idx, [1:  2])  = [  8.00000E+00  8.00071E+00 ];
BURN_DAYS                 (idx, 1)        =  2.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.57375E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  9.11249E+15 0.00075  6.61877E-01 0.00053 ];
U238_FISS                 (idx, [1:   4]) = [  9.68313E+14 0.00293  7.03139E-02 0.00269 ];
PU239_FISS                (idx, [1:   4]) = [  3.45463E+15 0.00132  2.50921E-01 0.00119 ];
PU240_FISS                (idx, [1:   4]) = [  2.94227E+12 0.04776  2.13868E-04 0.04774 ];
PU241_FISS                (idx, [1:   4]) = [  2.22034E+14 0.00572  1.61277E-02 0.00570 ];
U235_CAPT                 (idx, [1:   4]) = [  2.02017E+15 0.00192  9.72028E-02 0.00182 ];
U238_CAPT                 (idx, [1:   4]) = [  8.46474E+15 0.00105  4.07271E-01 0.00074 ];
PU239_CAPT                (idx, [1:   4]) = [  1.92122E+15 0.00181  9.24476E-02 0.00178 ];
PU240_CAPT                (idx, [1:   4]) = [  8.52817E+14 0.00301  4.10298E-02 0.00287 ];
PU241_CAPT                (idx, [1:   4]) = [  8.07866E+13 0.00871  3.88771E-03 0.00872 ];
XE135_CAPT                (idx, [1:   4]) = [  7.35773E+14 0.00303  3.54038E-02 0.00299 ];
SM149_CAPT                (idx, [1:   4]) = [  1.86946E+14 0.00598  8.99453E-03 0.00592 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000863 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.79406E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000863 5.00779E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2982419 2.98661E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1975867 1.97860E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 42577 4.25823E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000863 5.00779E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.44589E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.55018E+16 1.9E-05  3.55018E+16 1.9E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37593E+16 3.5E-06  1.37593E+16 3.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.07952E+16 0.00043  1.53926E+16 0.00044  5.40264E+15 0.00113 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.45545E+16 0.00026  2.91518E+16 0.00023  5.40264E+15 0.00113 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.47937E+16 0.00050  3.47937E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.63800E+18 0.00045  4.37588E+17 0.00044  1.20042E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.96355E+14 0.00498 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.48508E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.22268E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11583E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11583E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.61105E+00 0.00049 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.85627E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.53752E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24464E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94475E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96992E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.02979E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.02102E+00 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.58021E+00 2.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04156E+02 3.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.02103E+00 0.00057  1.01507E+00 0.00055  5.94909E-03 0.00923 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.02029E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.02048E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.02029E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.02906E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72585E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72613E+01 8.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.41676E-07 0.00377 ];
IMP_EALF                  (idx, [1:   2]) = [  6.37962E-07 0.00150 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.39530E-01 0.00287 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.39406E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.85602E-03 0.00612  1.64882E-04 0.03592  9.66336E-04 0.01455  9.11533E-04 0.01453  2.66233E-03 0.00865  8.71555E-04 0.01532  2.79384E-04 0.02607 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.90884E-01 0.01372  1.00609E-02 0.02211  3.11851E-02 0.00204  1.10278E-01 0.00042  3.21707E-01 0.00026  1.33184E+00 0.00087  8.40336E+00 0.01186 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.92584E-03 0.00874  1.65022E-04 0.05054  1.00129E-03 0.02155  9.09578E-04 0.02205  2.68668E-03 0.01291  8.69200E-04 0.02300  2.94061E-04 0.04146 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.96885E-01 0.02154  1.25073E-02 0.00050  3.12478E-02 0.00058  1.10272E-01 0.00057  3.21544E-01 0.00037  1.33013E+00 0.00135  8.92496E+00 0.00484 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.48245E-05 0.00131  2.48127E-05 0.00131  2.68048E-05 0.01360 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.53421E-05 0.00116  2.53299E-05 0.00116  2.73696E-05 0.01363 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.83764E-03 0.00930  1.63395E-04 0.05562  9.57511E-04 0.02354  9.25258E-04 0.02450  2.64623E-03 0.01380  8.60508E-04 0.02401  2.84731E-04 0.04292 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.89911E-01 0.02189  1.25102E-02 0.00076  3.12391E-02 0.00071  1.10350E-01 0.00074  3.21690E-01 0.00044  1.33050E+00 0.00184  8.93062E+00 0.00560 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.43186E-05 0.00291  2.43021E-05 0.00291  2.60601E-05 0.04154 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.48255E-05 0.00284  2.48088E-05 0.00285  2.65770E-05 0.04132 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.57583E-03 0.03239  1.14970E-04 0.19983  9.21386E-04 0.07340  8.88340E-04 0.07900  2.54606E-03 0.04644  7.88026E-04 0.08866  3.17055E-04 0.12924 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.40848E-01 0.07556  1.25160E-02 0.00216  3.12789E-02 0.00160  1.10026E-01 0.00140  3.22462E-01 0.00133  1.33362E+00 0.00336  8.95930E+00 0.01496 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.57902E-03 0.03145  1.18345E-04 0.19814  9.48161E-04 0.07197  8.92284E-04 0.07643  2.53937E-03 0.04485  7.74328E-04 0.08558  3.06541E-04 0.12751 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.19239E-01 0.07463  1.25160E-02 0.00216  3.12825E-02 0.00159  1.10012E-01 0.00139  3.22459E-01 0.00131  1.33376E+00 0.00327  8.95970E+00 0.01497 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.30761E+02 0.03291 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.45999E-05 0.00080 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.51129E-05 0.00054 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.78438E-03 0.00608 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.35177E+02 0.00607 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.76274E-07 0.00074 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.77304E-06 0.00041  2.77297E-06 0.00041  2.78557E-06 0.00543 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.83797E-05 0.00087  3.83976E-05 0.00087  3.54340E-05 0.01011 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.51601E-01 0.00035  6.51429E-01 0.00036  6.94068E-01 0.00945 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05870E+01 0.01333 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.50917E+01 0.00049  3.35745E+01 0.00049 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.61731E+04 0.00326  2.98792E+05 0.00153  6.08762E+05 0.00100  6.50887E+05 0.00101  5.98710E+05 0.00081  6.41273E+05 0.00053  4.35784E+05 0.00057  3.86139E+05 0.00060  2.95220E+05 0.00076  2.41144E+05 0.00093  2.07653E+05 0.00094  1.87410E+05 0.00098  1.72876E+05 0.00080  1.64577E+05 0.00069  1.60682E+05 0.00086  1.38467E+05 0.00089  1.36864E+05 0.00100  1.35703E+05 0.00085  1.33055E+05 0.00084  2.59885E+05 0.00053  2.50675E+05 0.00054  1.80789E+05 0.00078  1.16996E+05 0.00073  1.35350E+05 0.00094  1.28125E+05 0.00124  1.14946E+05 0.00068  1.88029E+05 0.00049  4.31280E+04 0.00151  5.42248E+04 0.00136  4.93385E+04 0.00126  2.86925E+04 0.00197  4.95825E+04 0.00169  3.35459E+04 0.00182  2.81408E+04 0.00167  5.13011E+03 0.00350  4.75039E+03 0.00305  4.36735E+03 0.00333  4.23043E+03 0.00335  4.33262E+03 0.00274  4.67642E+03 0.00342  5.16552E+03 0.00246  5.00721E+03 0.00339  9.70591E+03 0.00299  1.57057E+04 0.00244  2.01808E+04 0.00223  5.35678E+04 0.00126  5.62601E+04 0.00152  6.02244E+04 0.00138  3.96380E+04 0.00153  2.83979E+04 0.00173  2.12024E+04 0.00191  2.50054E+04 0.00155  4.86702E+04 0.00183  6.78169E+04 0.00125  1.33340E+05 0.00114  2.08453E+05 0.00142  3.13448E+05 0.00129  2.00876E+05 0.00143  1.44146E+05 0.00137  1.03915E+05 0.00143  9.33210E+04 0.00130  9.20533E+04 0.00159  7.69122E+04 0.00147  5.19858E+04 0.00177  4.80832E+04 0.00152  4.28164E+04 0.00189  3.63071E+04 0.00189  2.85867E+04 0.00206  1.91018E+04 0.00154  6.76597E+03 0.00213 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.02924E+00 0.00072 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.29927E+18 0.00067  3.38770E+17 0.00108 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38023E-01 0.00011  1.54580E+00 0.00037 ];
INF_CAPT                  (idx, [1:   4]) = [  7.18929E-03 0.00060  3.38148E-02 0.00056 ];
INF_ABS                   (idx, [1:   4]) = [  9.23728E-03 0.00049  6.65809E-02 0.00082 ];
INF_FISS                  (idx, [1:   4]) = [  2.04799E-03 0.00048  3.27661E-02 0.00109 ];
INF_NSF                   (idx, [1:   4]) = [  5.36756E-03 0.00049  8.42238E-02 0.00113 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.62089E+00 5.9E-05  2.57045E+00 5.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04586E+02 5.9E-06  2.04052E+02 8.7E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.73211E-08 0.00032  2.56673E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28788E-01 0.00012  1.47923E+00 0.00042 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43932E-01 0.00022  3.92469E-01 0.00053 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62080E-02 0.00035  9.30590E-02 0.00105 ];
INF_SCATT3                (idx, [1:   4]) = [  7.31034E-03 0.00277  2.78747E-02 0.00236 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03210E-02 0.00193 -8.68205E-03 0.00702 ];
INF_SCATT5                (idx, [1:   4]) = [  1.44423E-04 0.14095  6.48246E-03 0.00714 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07786E-03 0.00388 -1.69101E-02 0.00300 ];
INF_SCATT7                (idx, [1:   4]) = [  7.41578E-04 0.02039  4.16673E-04 0.10797 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28830E-01 0.00012  1.47923E+00 0.00042 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43933E-01 0.00022  3.92469E-01 0.00053 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62083E-02 0.00035  9.30590E-02 0.00105 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.31043E-03 0.00278  2.78747E-02 0.00236 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03209E-02 0.00194 -8.68205E-03 0.00702 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.44517E-04 0.14085  6.48246E-03 0.00714 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07789E-03 0.00390 -1.69101E-02 0.00300 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.41558E-04 0.02036  4.16673E-04 0.10797 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12787E-01 0.00029  1.00017E+00 0.00034 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56652E+00 0.00029  3.33277E-01 0.00034 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.19554E-03 0.00050  6.65809E-02 0.00082 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69795E-02 0.00021  6.77479E-02 0.00090 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11044E-01 0.00011  1.77444E-02 0.00046  1.17476E-03 0.00429  1.47805E+00 0.00042 ];
INF_S1                    (idx, [1:   8]) = [  2.38768E-01 0.00021  5.16390E-03 0.00094  5.01768E-04 0.00567  3.91967E-01 0.00053 ];
INF_S2                    (idx, [1:   8]) = [  9.77754E-02 0.00034 -1.56740E-03 0.00233  2.73606E-04 0.00893  9.27854E-02 0.00105 ];
INF_S3                    (idx, [1:   8]) = [  9.14390E-03 0.00223 -1.83356E-03 0.00140  9.92203E-05 0.01914  2.77755E-02 0.00235 ];
INF_S4                    (idx, [1:   8]) = [ -9.72704E-03 0.00193 -5.93937E-04 0.00519  1.32072E-06 1.00000 -8.68337E-03 0.00700 ];
INF_S5                    (idx, [1:   8]) = [  1.18285E-04 0.16895  2.61386E-05 0.08466 -3.91131E-05 0.04301  6.52157E-03 0.00722 ];
INF_S6                    (idx, [1:   8]) = [  5.21767E-03 0.00386 -1.39815E-04 0.01495 -5.02036E-05 0.03336 -1.68599E-02 0.00302 ];
INF_S7                    (idx, [1:   8]) = [  9.10400E-04 0.01646 -1.68822E-04 0.01240 -4.53525E-05 0.02859  4.62025E-04 0.09778 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11086E-01 0.00011  1.77444E-02 0.00046  1.17476E-03 0.00429  1.47805E+00 0.00042 ];
INF_SP1                   (idx, [1:   8]) = [  2.38769E-01 0.00021  5.16390E-03 0.00094  5.01768E-04 0.00567  3.91967E-01 0.00053 ];
INF_SP2                   (idx, [1:   8]) = [  9.77757E-02 0.00034 -1.56740E-03 0.00233  2.73606E-04 0.00893  9.27854E-02 0.00105 ];
INF_SP3                   (idx, [1:   8]) = [  9.14398E-03 0.00223 -1.83356E-03 0.00140  9.92203E-05 0.01914  2.77755E-02 0.00235 ];
INF_SP4                   (idx, [1:   8]) = [ -9.72693E-03 0.00193 -5.93937E-04 0.00519  1.32072E-06 1.00000 -8.68337E-03 0.00700 ];
INF_SP5                   (idx, [1:   8]) = [  1.18379E-04 0.16876  2.61386E-05 0.08466 -3.91131E-05 0.04301  6.52157E-03 0.00722 ];
INF_SP6                   (idx, [1:   8]) = [  5.21771E-03 0.00387 -1.39815E-04 0.01495 -5.02036E-05 0.03336 -1.68599E-02 0.00302 ];
INF_SP7                   (idx, [1:   8]) = [  9.10380E-04 0.01645 -1.68822E-04 0.01240 -4.53525E-05 0.02859  4.62025E-04 0.09778 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31916E-01 0.00058  1.15662E+00 0.00706 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33556E-01 0.00102  1.24760E+00 0.00748 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33411E-01 0.00069  1.24539E+00 0.01195 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28849E-01 0.00087  1.01273E+00 0.00680 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43732E+00 0.00058  2.88550E-01 0.00724 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42724E+00 0.00102  2.67538E-01 0.00747 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42811E+00 0.00069  2.68599E-01 0.01230 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45659E+00 0.00087  3.29513E-01 0.00690 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.92584E-03 0.00874  1.65022E-04 0.05054  1.00129E-03 0.02155  9.09578E-04 0.02205  2.68668E-03 0.01291  8.69200E-04 0.02300  2.94061E-04 0.04146 ];
LAMBDA                    (idx, [1:  14]) = [  7.96885E-01 0.02154  1.25073E-02 0.00050  3.12478E-02 0.00058  1.10272E-01 0.00057  3.21544E-01 0.00037  1.33013E+00 0.00135  8.92496E+00 0.00484 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0005' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:52:32 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:00:01 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299952 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00136E+00  1.00402E+00  9.97129E-01  9.95325E-01  1.00217E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13193E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88681E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05081E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05509E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.66910E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.49827E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.49743E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.44398E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.09326E-01 0.00115  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000993 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00020E+04 0.00087 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00020E+04 0.00087 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.35255E+02 ;
RUNNING_TIME              (idx, 1)        =  6.74858E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.61967E-01  4.61967E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.93533E-01  9.63333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.62358E+01  3.13588E+00  2.42703E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.88733E-01  2.66667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.49333E-02  8.66663E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  6.74856E+01  1.18185E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96778 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99864E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78133E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.36046E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.78175E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.41094E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.72703E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.20536E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.63341E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.66119E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.14466E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.40725E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.34568E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.27476E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.10089E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.67978E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.01566E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.28855E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.36056E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.41480E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.28303E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.21355E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.14843E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.42257E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  4.92900E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.23754E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.52917E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 11 ;
BURNUP                     (idx, [1:  2])  = [  9.00000E+00  9.00080E+00 ];
BURN_DAYS                 (idx, 1)        =  2.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.68290E-01 0.00109 ];
U233_FISS                 (idx, [1:   4]) = [  7.13910E+09 1.00000  5.19751E-07 1.00000 ];
U235_FISS                 (idx, [1:   4]) = [  8.78493E+15 0.00083  6.38215E-01 0.00059 ];
U238_FISS                 (idx, [1:   4]) = [  9.76558E+14 0.00266  7.09390E-02 0.00252 ];
PU239_FISS                (idx, [1:   4]) = [  3.70898E+15 0.00139  2.69457E-01 0.00128 ];
PU240_FISS                (idx, [1:   4]) = [  3.22408E+12 0.04559  2.34068E-04 0.04560 ];
PU241_FISS                (idx, [1:   4]) = [  2.82940E+14 0.00525  2.05530E-02 0.00517 ];
U235_CAPT                 (idx, [1:   4]) = [  1.95713E+15 0.00183  9.19636E-02 0.00178 ];
U238_CAPT                 (idx, [1:   4]) = [  8.57608E+15 0.00105  4.02940E-01 0.00071 ];
PU239_CAPT                (idx, [1:   4]) = [  2.06195E+15 0.00186  9.68897E-02 0.00182 ];
PU240_CAPT                (idx, [1:   4]) = [  9.72447E+14 0.00284  4.56903E-02 0.00275 ];
PU241_CAPT                (idx, [1:   4]) = [  1.00037E+14 0.00824  4.70046E-03 0.00822 ];
XE135_CAPT                (idx, [1:   4]) = [  7.34624E+14 0.00323  3.45188E-02 0.00319 ];
SM149_CAPT                (idx, [1:   4]) = [  1.91399E+14 0.00642  8.99326E-03 0.00637 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000993 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.83591E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000993 5.00784E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3011033 3.01524E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1947649 1.95027E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 42311 4.23247E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000993 5.00784E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.93601E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.56206E+16 2.1E-05  3.56206E+16 2.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37495E+16 3.9E-06  1.37495E+16 3.9E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.12696E+16 0.00045  1.58308E+16 0.00046  5.43877E+15 0.00114 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.50191E+16 0.00027  2.95803E+16 0.00025  5.43877E+15 0.00114 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.52917E+16 0.00049  3.52917E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.65863E+18 0.00047  4.43222E+17 0.00048  1.21541E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.98774E+14 0.00513 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.53178E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.23605E+18 0.00061 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11467E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11467E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.60131E+00 0.00049 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.85916E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.50903E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24421E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94470E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97048E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.01914E+00 0.00058 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.01051E+00 0.00058 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.59068E+00 2.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04301E+02 3.9E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.01035E+00 0.00060  1.00473E+00 0.00058  5.78300E-03 0.00962 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.01019E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.00944E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.01019E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.01882E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72428E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72380E+01 9.0E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.51397E-07 0.00341 ];
IMP_EALF                  (idx, [1:   2]) = [  6.53028E-07 0.00155 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.41718E-01 0.00277 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.42818E-01 0.00114 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.79818E-03 0.00616  1.53540E-04 0.03406  1.00239E-03 0.01414  9.14175E-04 0.01465  2.58356E-03 0.00926  8.88853E-04 0.01531  2.55663E-04 0.02843 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.59986E-01 0.01415  1.00011E-02 0.02239  3.12432E-02 0.00040  1.10299E-01 0.00040  3.21744E-01 0.00028  1.33004E+00 0.00089  8.20827E+00 0.01340 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.73190E-03 0.00864  1.60741E-04 0.05322  9.85013E-04 0.02074  9.11562E-04 0.02257  2.55575E-03 0.01329  8.81170E-04 0.02329  2.37671E-04 0.04223 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.42283E-01 0.02065  1.24998E-02 0.00036  3.12429E-02 0.00057  1.10315E-01 0.00055  3.21901E-01 0.00041  1.33075E+00 0.00120  8.85812E+00 0.00501 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.49860E-05 0.00134  2.49768E-05 0.00134  2.64672E-05 0.01487 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.52400E-05 0.00118  2.52307E-05 0.00119  2.67256E-05 0.01480 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.72034E-03 0.00966  1.53365E-04 0.05828  9.89442E-04 0.02347  9.20141E-04 0.02454  2.53439E-03 0.01428  8.76755E-04 0.02526  2.46241E-04 0.04513 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.50177E-01 0.02299  1.24960E-02 0.00029  3.12258E-02 0.00070  1.10206E-01 0.00063  3.21892E-01 0.00048  1.32921E+00 0.00199  8.82910E+00 0.00752 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.44767E-05 0.00316  2.44631E-05 0.00317  2.46180E-05 0.03773 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.47248E-05 0.00307  2.47109E-05 0.00309  2.48863E-05 0.03778 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.59674E-03 0.03148  1.48861E-04 0.19429  1.07233E-03 0.06891  9.56780E-04 0.08166  2.46016E-03 0.04962  7.96547E-04 0.08326  1.62067E-04 0.16750 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.04669E-01 0.08815  1.24904E-02 3.5E-05  3.12015E-02 0.00160  1.10287E-01 0.00170  3.22185E-01 0.00139  1.33012E+00 0.00380  8.91984E+00 0.01629 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.54541E-03 0.03063  1.41213E-04 0.19312  1.05537E-03 0.06750  9.74179E-04 0.08014  2.42437E-03 0.04816  7.90997E-04 0.08212  1.59283E-04 0.17196 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  6.92114E-01 0.08794  1.24904E-02 3.5E-05  3.12015E-02 0.00160  1.10309E-01 0.00171  3.22206E-01 0.00137  1.33053E+00 0.00371  8.91878E+00 0.01628 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.29734E+02 0.03173 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.47292E-05 0.00086 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.49807E-05 0.00061 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.62832E-03 0.00599 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.27620E+02 0.00597 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.72478E-07 0.00075 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.76628E-06 0.00043  2.76603E-06 0.00043  2.80553E-06 0.00558 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.82135E-05 0.00088  3.82316E-05 0.00088  3.51672E-05 0.01077 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.48783E-01 0.00033  6.48665E-01 0.00034  6.81911E-01 0.00970 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06252E+01 0.01264 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.49743E+01 0.00047  3.34933E+01 0.00046 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.63524E+04 0.00328  2.98481E+05 0.00152  6.09134E+05 0.00098  6.50918E+05 0.00061  5.99388E+05 0.00084  6.42770E+05 0.00071  4.36322E+05 0.00070  3.85628E+05 0.00092  2.95137E+05 0.00073  2.41468E+05 0.00082  2.08022E+05 0.00073  1.87370E+05 0.00049  1.73060E+05 0.00080  1.64322E+05 0.00102  1.60404E+05 0.00073  1.38382E+05 0.00084  1.36732E+05 0.00088  1.35561E+05 0.00121  1.33046E+05 0.00092  2.60067E+05 0.00060  2.51098E+05 0.00065  1.80865E+05 0.00089  1.17065E+05 0.00072  1.35323E+05 0.00064  1.28238E+05 0.00115  1.14881E+05 0.00093  1.87841E+05 0.00089  4.32881E+04 0.00155  5.42954E+04 0.00132  4.92298E+04 0.00158  2.86592E+04 0.00167  4.96009E+04 0.00148  3.34888E+04 0.00185  2.79359E+04 0.00166  5.06697E+03 0.00399  4.67717E+03 0.00405  4.25287E+03 0.00452  4.11760E+03 0.00449  4.18258E+03 0.00322  4.50873E+03 0.00391  5.08931E+03 0.00384  4.99531E+03 0.00372  9.62219E+03 0.00295  1.56097E+04 0.00282  2.00932E+04 0.00198  5.34148E+04 0.00158  5.60890E+04 0.00147  6.00536E+04 0.00098  3.92549E+04 0.00139  2.80398E+04 0.00181  2.09229E+04 0.00175  2.46354E+04 0.00161  4.78241E+04 0.00142  6.68973E+04 0.00148  1.31938E+05 0.00121  2.06438E+05 0.00149  3.10415E+05 0.00145  1.99446E+05 0.00154  1.43127E+05 0.00155  1.02857E+05 0.00176  9.25551E+04 0.00198  9.12758E+04 0.00179  7.62443E+04 0.00168  5.16126E+04 0.00172  4.76396E+04 0.00193  4.24875E+04 0.00166  3.58502E+04 0.00190  2.83618E+04 0.00185  1.90061E+04 0.00244  6.73530E+03 0.00320 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.01806E+00 0.00058 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.31814E+18 0.00056  3.40513E+17 0.00145 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38104E-01 0.00011  1.54699E+00 0.00049 ];
INF_CAPT                  (idx, [1:   4]) = [  7.29106E-03 0.00071  3.42426E-02 0.00079 ];
INF_ABS                   (idx, [1:   4]) = [  9.31072E-03 0.00057  6.68094E-02 0.00113 ];
INF_FISS                  (idx, [1:   4]) = [  2.01966E-03 0.00041  3.25668E-02 0.00150 ];
INF_NSF                   (idx, [1:   4]) = [  5.30663E-03 0.00039  8.40825E-02 0.00154 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.62749E+00 8.9E-05  2.58185E+00 6.3E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04676E+02 8.4E-06  2.04211E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.71660E-08 0.00037  2.56740E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28795E-01 0.00011  1.48010E+00 0.00055 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43948E-01 0.00023  3.92751E-01 0.00062 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62309E-02 0.00039  9.30635E-02 0.00090 ];
INF_SCATT3                (idx, [1:   4]) = [  7.36225E-03 0.00330  2.78915E-02 0.00200 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02978E-02 0.00208 -8.78706E-03 0.00556 ];
INF_SCATT5                (idx, [1:   4]) = [  1.80113E-04 0.07585  6.58455E-03 0.00838 ];
INF_SCATT6                (idx, [1:   4]) = [  5.11410E-03 0.00286 -1.68255E-02 0.00349 ];
INF_SCATT7                (idx, [1:   4]) = [  7.45769E-04 0.01812  3.74433E-04 0.09008 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28837E-01 0.00011  1.48010E+00 0.00055 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43948E-01 0.00023  3.92751E-01 0.00062 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62313E-02 0.00039  9.30635E-02 0.00090 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.36182E-03 0.00330  2.78915E-02 0.00200 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02981E-02 0.00209 -8.78706E-03 0.00556 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.80226E-04 0.07583  6.58455E-03 0.00838 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.11407E-03 0.00287 -1.68255E-02 0.00349 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.45771E-04 0.01812  3.74433E-04 0.09008 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12857E-01 0.00034  1.00134E+00 0.00040 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56600E+00 0.00034  3.32887E-01 0.00040 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.26876E-03 0.00058  6.68094E-02 0.00113 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69751E-02 0.00025  6.80829E-02 0.00119 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11129E-01 0.00011  1.76657E-02 0.00045  1.19012E-03 0.00469  1.47891E+00 0.00055 ];
INF_S1                    (idx, [1:   8]) = [  2.38810E-01 0.00023  5.13791E-03 0.00064  5.06746E-04 0.00816  3.92244E-01 0.00063 ];
INF_S2                    (idx, [1:   8]) = [  9.78000E-02 0.00038 -1.56910E-03 0.00210  2.77279E-04 0.00772  9.27862E-02 0.00090 ];
INF_S3                    (idx, [1:   8]) = [  9.18895E-03 0.00254 -1.82670E-03 0.00145  9.73456E-05 0.01644  2.77942E-02 0.00202 ];
INF_S4                    (idx, [1:   8]) = [ -9.71024E-03 0.00221 -5.87579E-04 0.00398  8.47273E-07 1.00000 -8.78791E-03 0.00560 ];
INF_S5                    (idx, [1:   8]) = [  1.53657E-04 0.09223  2.64557E-05 0.09048 -3.81929E-05 0.03467  6.62274E-03 0.00842 ];
INF_S6                    (idx, [1:   8]) = [  5.25580E-03 0.00278 -1.41698E-04 0.01686 -4.96895E-05 0.02581 -1.67758E-02 0.00349 ];
INF_S7                    (idx, [1:   8]) = [  9.16944E-04 0.01457 -1.71174E-04 0.01140 -4.66430E-05 0.02875  4.21076E-04 0.07985 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11171E-01 0.00011  1.76657E-02 0.00045  1.19012E-03 0.00469  1.47891E+00 0.00055 ];
INF_SP1                   (idx, [1:   8]) = [  2.38810E-01 0.00023  5.13791E-03 0.00064  5.06746E-04 0.00816  3.92244E-01 0.00063 ];
INF_SP2                   (idx, [1:   8]) = [  9.78004E-02 0.00038 -1.56910E-03 0.00210  2.77279E-04 0.00772  9.27862E-02 0.00090 ];
INF_SP3                   (idx, [1:   8]) = [  9.18852E-03 0.00254 -1.82670E-03 0.00145  9.73456E-05 0.01644  2.77942E-02 0.00202 ];
INF_SP4                   (idx, [1:   8]) = [ -9.71052E-03 0.00222 -5.87579E-04 0.00398  8.47273E-07 1.00000 -8.78791E-03 0.00560 ];
INF_SP5                   (idx, [1:   8]) = [  1.53770E-04 0.09217  2.64557E-05 0.09048 -3.81929E-05 0.03467  6.62274E-03 0.00842 ];
INF_SP6                   (idx, [1:   8]) = [  5.25577E-03 0.00280 -1.41698E-04 0.01686 -4.96895E-05 0.02581 -1.67758E-02 0.00349 ];
INF_SP7                   (idx, [1:   8]) = [  9.16945E-04 0.01457 -1.71174E-04 0.01140 -4.66430E-05 0.02875  4.21076E-04 0.07985 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31983E-01 0.00052  1.18057E+00 0.00844 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33776E-01 0.00092  1.27591E+00 0.00998 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33481E-01 0.00097  1.27347E+00 0.01010 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28771E-01 0.00087  1.02966E+00 0.00811 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43689E+00 0.00052  2.82814E-01 0.00810 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42590E+00 0.00092  2.61851E-01 0.00957 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42770E+00 0.00096  2.62369E-01 0.00972 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45708E+00 0.00087  3.24222E-01 0.00778 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.73190E-03 0.00864  1.60741E-04 0.05322  9.85013E-04 0.02074  9.11562E-04 0.02257  2.55575E-03 0.01329  8.81170E-04 0.02329  2.37671E-04 0.04223 ];
LAMBDA                    (idx, [1:  14]) = [  7.42283E-01 0.02065  1.24998E-02 0.00036  3.12429E-02 0.00057  1.10315E-01 0.00055  3.21901E-01 0.00041  1.33075E+00 0.00120  8.85812E+00 0.00501 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0005' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:52:32 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:05:37 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299952 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99240E-01  1.00551E+00  9.97645E-01  9.97680E-01  9.99922E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 6.6E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13180E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88682E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05672E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06100E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.66869E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.49950E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.49865E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.41512E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.08792E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001032 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00021E+04 0.00087 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00021E+04 0.00087 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.63184E+02 ;
RUNNING_TIME              (idx, 1)        =  7.30778E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.61967E-01  4.61967E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.13050E-01  9.66667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.17524E+01  3.10525E+00  2.41142E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  6.43950E-01  2.72000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.73500E-02  8.50002E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.30776E+01  1.18046E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96983 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00004E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78640E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.38488E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.77527E+04 ;
TOT_SF_RATE               (idx, 1)        =  4.73859E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.74695E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.21935E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.63790E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.65331E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.25476E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.45014E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  6.23048E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.37853E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.31713E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.71229E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.11535E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.30438E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.38187E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.43630E+14 ;
CS134_ACTIVITY            (idx, 1)        =  7.74096E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.34796E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.16994E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.40718E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  6.75166E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.24415E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.57253E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 12 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E+01  1.00009E+01 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.78705E-01 0.00108 ];
U233_FISS                 (idx, [1:   4]) = [  1.47663E+10 0.70640  1.08191E-06 0.70643 ];
U235_FISS                 (idx, [1:   4]) = [  8.46577E+15 0.00080  6.15904E-01 0.00052 ];
U238_FISS                 (idx, [1:   4]) = [  9.95927E+14 0.00269  7.24459E-02 0.00251 ];
PU239_FISS                (idx, [1:   4]) = [  3.92906E+15 0.00124  2.85841E-01 0.00105 ];
PU240_FISS                (idx, [1:   4]) = [  3.97943E+12 0.04123  2.89360E-04 0.04112 ];
PU241_FISS                (idx, [1:   4]) = [  3.42038E+14 0.00446  2.48834E-02 0.00441 ];
U235_CAPT                 (idx, [1:   4]) = [  1.88955E+15 0.00208  8.69557E-02 0.00196 ];
U238_CAPT                 (idx, [1:   4]) = [  8.65552E+15 0.00101  3.98317E-01 0.00070 ];
PU239_CAPT                (idx, [1:   4]) = [  2.17566E+15 0.00187  1.00127E-01 0.00178 ];
PU240_CAPT                (idx, [1:   4]) = [  1.08564E+15 0.00268  4.99606E-02 0.00260 ];
PU241_CAPT                (idx, [1:   4]) = [  1.22526E+14 0.00739  5.63853E-03 0.00735 ];
XE135_CAPT                (idx, [1:   4]) = [  7.38958E+14 0.00309  3.40098E-02 0.00307 ];
SM149_CAPT                (idx, [1:   4]) = [  1.98932E+14 0.00603  9.15488E-03 0.00599 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001032 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.00946E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001032 5.00801E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3036867 3.04119E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1921218 1.92386E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 42947 4.29591E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001032 5.00801E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 8.38190E-09 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.57309E+16 1.9E-05  3.57309E+16 1.9E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37404E+16 3.6E-06  1.37404E+16 3.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.17322E+16 0.00044  1.62140E+16 0.00044  5.51824E+15 0.00110 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.54726E+16 0.00027  2.99544E+16 0.00024  5.51824E+15 0.00110 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.57253E+16 0.00049  3.57253E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.67796E+18 0.00046  4.47578E+17 0.00047  1.23038E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.06968E+14 0.00489 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.57796E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.25174E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11351E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11351E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.59249E+00 0.00052 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.85612E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.48690E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24377E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94397E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96994E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.00934E+00 0.00060 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.00066E+00 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.60043E+00 2.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04436E+02 3.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.00082E+00 0.00061  9.94985E-01 0.00061  5.67935E-03 0.00975 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.00026E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.00028E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.00026E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.00892E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72168E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72189E+01 9.3E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.68713E-07 0.00354 ];
IMP_EALF                  (idx, [1:   2]) = [  6.65629E-07 0.00160 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.47186E-01 0.00270 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.46544E-01 0.00114 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.85267E-03 0.00640  1.54788E-04 0.03605  9.88834E-04 0.01462  9.19609E-04 0.01422  2.63430E-03 0.00905  8.78925E-04 0.01557  2.76215E-04 0.02799 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.86614E-01 0.01447  9.90686E-03 0.02295  3.11936E-02 0.00043  1.10057E-01 0.00205  3.21920E-01 0.00029  1.32593E+00 0.00106  8.28450E+00 0.01337 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.73005E-03 0.00848  1.55732E-04 0.05371  9.79650E-04 0.02128  9.04160E-04 0.02085  2.56561E-03 0.01325  8.48134E-04 0.02222  2.76764E-04 0.04057 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.86081E-01 0.02055  1.25051E-02 0.00045  3.11831E-02 0.00061  1.10357E-01 0.00059  3.21982E-01 0.00043  1.32586E+00 0.00149  8.90918E+00 0.00532 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.51588E-05 0.00141  2.51462E-05 0.00142  2.76246E-05 0.01488 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.51740E-05 0.00122  2.51613E-05 0.00122  2.76452E-05 0.01489 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.67099E-03 0.00990  1.45509E-04 0.06006  9.50759E-04 0.02415  9.01492E-04 0.02430  2.54654E-03 0.01496  8.49710E-04 0.02552  2.76977E-04 0.04259 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.08302E-01 0.02366  1.25031E-02 0.00063  3.11961E-02 0.00073  1.10307E-01 0.00073  3.21972E-01 0.00049  1.32620E+00 0.00196  8.99261E+00 0.00548 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.46813E-05 0.00306  2.46651E-05 0.00306  2.69281E-05 0.03706 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.46977E-05 0.00302  2.46815E-05 0.00302  2.69473E-05 0.03699 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.39372E-03 0.03013  2.12368E-04 0.17998  8.37591E-04 0.07616  9.22185E-04 0.07653  2.26679E-03 0.04870  8.99794E-04 0.07931  2.54993E-04 0.14046 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.40927E-01 0.07768  1.24898E-02 4.1E-05  3.11196E-02 0.00179  1.10245E-01 0.00163  3.21401E-01 0.00134  1.32735E+00 0.00400  8.89353E+00 0.01815 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.38949E-03 0.02963  2.15023E-04 0.17500  8.68084E-04 0.07442  9.25511E-04 0.07510  2.21850E-03 0.04733  9.12569E-04 0.07869  2.49812E-04 0.13931 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.28216E-01 0.07626  1.24898E-02 4.1E-05  3.11276E-02 0.00178  1.10234E-01 0.00164  3.21464E-01 0.00133  1.32796E+00 0.00387  8.89574E+00 0.01815 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.19747E+02 0.03038 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.49384E-05 0.00083 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.49542E-05 0.00056 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.66852E-03 0.00595 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.27371E+02 0.00600 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.72809E-07 0.00072 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.75212E-06 0.00045  2.75180E-06 0.00045  2.80331E-06 0.00566 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.83549E-05 0.00087  3.83752E-05 0.00088  3.50359E-05 0.01099 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.46541E-01 0.00034  6.46448E-01 0.00034  6.77319E-01 0.01016 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05257E+01 0.01403 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.49865E+01 0.00046  3.34440E+01 0.00048 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.72782E+04 0.00271  2.99453E+05 0.00114  6.08747E+05 0.00083  6.51721E+05 0.00077  5.98684E+05 0.00052  6.41589E+05 0.00076  4.35752E+05 0.00072  3.85806E+05 0.00073  2.95306E+05 0.00066  2.41210E+05 0.00075  2.07909E+05 0.00081  1.87259E+05 0.00082  1.72694E+05 0.00080  1.64292E+05 0.00076  1.60142E+05 0.00092  1.38363E+05 0.00092  1.36467E+05 0.00087  1.35310E+05 0.00089  1.33026E+05 0.00076  2.59834E+05 0.00071  2.51039E+05 0.00054  1.81136E+05 0.00070  1.16997E+05 0.00105  1.35405E+05 0.00104  1.28174E+05 0.00102  1.14998E+05 0.00078  1.87342E+05 0.00081  4.32409E+04 0.00135  5.41342E+04 0.00140  4.90972E+04 0.00162  2.85912E+04 0.00218  4.95679E+04 0.00135  3.34232E+04 0.00189  2.78614E+04 0.00203  4.99818E+03 0.00341  4.56433E+03 0.00362  4.13886E+03 0.00445  3.99227E+03 0.00429  4.07983E+03 0.00370  4.40903E+03 0.00333  4.98664E+03 0.00289  4.94346E+03 0.00291  9.60583E+03 0.00279  1.55040E+04 0.00252  1.99426E+04 0.00252  5.30178E+04 0.00144  5.57883E+04 0.00125  5.95970E+04 0.00125  3.90413E+04 0.00130  2.77575E+04 0.00154  2.07006E+04 0.00190  2.44410E+04 0.00163  4.75266E+04 0.00129  6.65658E+04 0.00128  1.31361E+05 0.00119  2.06038E+05 0.00131  3.10519E+05 0.00135  1.99383E+05 0.00133  1.43084E+05 0.00151  1.03091E+05 0.00147  9.28100E+04 0.00132  9.14403E+04 0.00130  7.63703E+04 0.00146  5.17596E+04 0.00153  4.78266E+04 0.00160  4.25493E+04 0.00193  3.60018E+04 0.00191  2.84131E+04 0.00197  1.90218E+04 0.00171  6.75642E+03 0.00185 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.00895E+00 0.00044 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.33363E+18 0.00041  3.44364E+17 0.00123 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38090E-01 9.0E-05  1.55111E+00 0.00038 ];
INF_CAPT                  (idx, [1:   4]) = [  7.37082E-03 0.00049  3.45666E-02 0.00060 ];
INF_ABS                   (idx, [1:   4]) = [  9.36327E-03 0.00043  6.67569E-02 0.00091 ];
INF_FISS                  (idx, [1:   4]) = [  1.99245E-03 0.00053  3.21903E-02 0.00125 ];
INF_NSF                   (idx, [1:   4]) = [  5.24898E-03 0.00052  8.34462E-02 0.00127 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.63443E+00 6.8E-05  2.59228E+00 4.0E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04767E+02 8.3E-06  2.04357E+02 7.0E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.69902E-08 0.00039  2.57056E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28717E-01 9.6E-05  1.48439E+00 0.00043 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43898E-01 0.00017  3.94030E-01 0.00055 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62058E-02 0.00020  9.32066E-02 0.00080 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32241E-03 0.00244  2.80302E-02 0.00253 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03142E-02 0.00171 -8.80427E-03 0.00624 ];
INF_SCATT5                (idx, [1:   4]) = [  1.55731E-04 0.09510  6.54767E-03 0.00755 ];
INF_SCATT6                (idx, [1:   4]) = [  5.11180E-03 0.00242 -1.68899E-02 0.00277 ];
INF_SCATT7                (idx, [1:   4]) = [  7.38900E-04 0.01864  4.28524E-04 0.11651 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28760E-01 9.6E-05  1.48439E+00 0.00043 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43899E-01 0.00017  3.94030E-01 0.00055 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62057E-02 0.00020  9.32066E-02 0.00080 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32216E-03 0.00243  2.80302E-02 0.00253 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03142E-02 0.00170 -8.80427E-03 0.00624 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.55659E-04 0.09535  6.54767E-03 0.00755 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.11179E-03 0.00243 -1.68899E-02 0.00277 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.38856E-04 0.01863  4.28524E-04 0.11651 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12669E-01 0.00023  1.00403E+00 0.00031 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56739E+00 0.00023  3.31996E-01 0.00031 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.32036E-03 0.00046  6.67569E-02 0.00091 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69868E-02 0.00019  6.79053E-02 0.00102 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11103E-01 9.1E-05  1.76138E-02 0.00038  1.18489E-03 0.00367  1.48321E+00 0.00043 ];
INF_S1                    (idx, [1:   8]) = [  2.38774E-01 0.00017  5.12476E-03 0.00080  5.14988E-04 0.00607  3.93515E-01 0.00056 ];
INF_S2                    (idx, [1:   8]) = [  9.77642E-02 0.00020 -1.55844E-03 0.00271  2.81469E-04 0.00771  9.29251E-02 0.00080 ];
INF_S3                    (idx, [1:   8]) = [  9.13912E-03 0.00187 -1.81671E-03 0.00135  1.02485E-04 0.01958  2.79277E-02 0.00252 ];
INF_S4                    (idx, [1:   8]) = [ -9.72982E-03 0.00190 -5.84402E-04 0.00480  3.20476E-07 1.00000 -8.80459E-03 0.00623 ];
INF_S5                    (idx, [1:   8]) = [  1.24747E-04 0.11564  3.09842E-05 0.06280 -4.20970E-05 0.03723  6.58977E-03 0.00754 ];
INF_S6                    (idx, [1:   8]) = [  5.25208E-03 0.00233 -1.40280E-04 0.01743 -5.16455E-05 0.02825 -1.68383E-02 0.00281 ];
INF_S7                    (idx, [1:   8]) = [  9.12789E-04 0.01487 -1.73889E-04 0.00883 -4.81361E-05 0.03125  4.76660E-04 0.10536 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11146E-01 9.1E-05  1.76138E-02 0.00038  1.18489E-03 0.00367  1.48321E+00 0.00043 ];
INF_SP1                   (idx, [1:   8]) = [  2.38774E-01 0.00017  5.12476E-03 0.00080  5.14988E-04 0.00607  3.93515E-01 0.00056 ];
INF_SP2                   (idx, [1:   8]) = [  9.77642E-02 0.00020 -1.55844E-03 0.00271  2.81469E-04 0.00771  9.29251E-02 0.00080 ];
INF_SP3                   (idx, [1:   8]) = [  9.13886E-03 0.00187 -1.81671E-03 0.00135  1.02485E-04 0.01958  2.79277E-02 0.00252 ];
INF_SP4                   (idx, [1:   8]) = [ -9.72984E-03 0.00189 -5.84402E-04 0.00480  3.20476E-07 1.00000 -8.80459E-03 0.00623 ];
INF_SP5                   (idx, [1:   8]) = [  1.24675E-04 0.11593  3.09842E-05 0.06280 -4.20970E-05 0.03723  6.58977E-03 0.00754 ];
INF_SP6                   (idx, [1:   8]) = [  5.25207E-03 0.00234 -1.40280E-04 0.01743 -5.16455E-05 0.02825 -1.68383E-02 0.00281 ];
INF_SP7                   (idx, [1:   8]) = [  9.12745E-04 0.01486 -1.73889E-04 0.00883 -4.81361E-05 0.03125  4.76660E-04 0.10536 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31943E-01 0.00063  1.16192E+00 0.00662 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33623E-01 0.00097  1.25538E+00 0.00993 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33657E-01 0.00091  1.25107E+00 0.00838 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28630E-01 0.00091  1.01525E+00 0.00507 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43715E+00 0.00063  2.87177E-01 0.00651 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42683E+00 0.00097  2.66127E-01 0.00953 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42662E+00 0.00091  2.66875E-01 0.00814 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45799E+00 0.00091  3.28530E-01 0.00510 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.73005E-03 0.00848  1.55732E-04 0.05371  9.79650E-04 0.02128  9.04160E-04 0.02085  2.56561E-03 0.01325  8.48134E-04 0.02222  2.76764E-04 0.04057 ];
LAMBDA                    (idx, [1:  14]) = [  7.86081E-01 0.02055  1.25051E-02 0.00045  3.11831E-02 0.00061  1.10357E-01 0.00059  3.21982E-01 0.00043  1.32586E+00 0.00149  8.90918E+00 0.00532 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0005' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:52:32 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:11:10 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299952 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99704E-01  1.00480E+00  9.97527E-01  9.97672E-01  1.00030E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14450E-02 0.00113  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88555E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06240E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06669E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.66927E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.49195E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.49107E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.36619E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.13178E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000451 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00009E+04 0.00083 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00009E+04 0.00083 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.90914E+02 ;
RUNNING_TIME              (idx, 1)        =  7.86293E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.61967E-01  4.61967E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.32500E-01  9.85000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.72336E+01  3.09193E+00  2.38927E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  6.94533E-01  2.58333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.89833E-02  8.49998E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.86292E+01  1.17590E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97161 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00126E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79071E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.39637E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.74950E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.01224E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.78033E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.24294E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61601E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.62517E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.55396E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.52902E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  8.75422E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.57618E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.78540E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.77140E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.35595E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.31305E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.41380E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.45683E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.19605E+13 ;
CS137_ACTIVITY            (idx, 1)        =  1.68362E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.17870E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.36689E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.34302E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.25276E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.68817E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 13 ;
BURNUP                     (idx, [1:  2])  = [  1.25000E+01  1.25011E+01 ];
BURN_DAYS                 (idx, 1)        =  3.12500E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.02223E-01 0.00105 ];
U235_FISS                 (idx, [1:   4]) = [  7.76900E+15 0.00091  5.66108E-01 0.00069 ];
U238_FISS                 (idx, [1:   4]) = [  1.02423E+15 0.00278  7.46259E-02 0.00265 ];
PU239_FISS                (idx, [1:   4]) = [  4.41369E+15 0.00120  3.21623E-01 0.00109 ];
PU240_FISS                (idx, [1:   4]) = [  5.38885E+12 0.03698  3.92478E-04 0.03692 ];
PU241_FISS                (idx, [1:   4]) = [  5.00492E+14 0.00388  3.64700E-02 0.00384 ];
U235_CAPT                 (idx, [1:   4]) = [  1.74839E+15 0.00205  7.63855E-02 0.00202 ];
U238_CAPT                 (idx, [1:   4]) = [  8.86312E+15 0.00106  3.87181E-01 0.00075 ];
PU239_CAPT                (idx, [1:   4]) = [  2.44815E+15 0.00177  1.06959E-01 0.00175 ];
PU240_CAPT                (idx, [1:   4]) = [  1.35691E+15 0.00243  5.92811E-02 0.00238 ];
PU241_CAPT                (idx, [1:   4]) = [  1.80532E+14 0.00662  7.88594E-03 0.00656 ];
XE135_CAPT                (idx, [1:   4]) = [  7.43470E+14 0.00324  3.24841E-02 0.00327 ];
SM149_CAPT                (idx, [1:   4]) = [  2.04619E+14 0.00590  8.94086E-03 0.00593 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000451 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.95122E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000451 5.00795E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3098406 3.10319E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1857857 1.86056E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44188 4.41987E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000451 5.00795E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.25963E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.59843E+16 2.3E-05  3.59843E+16 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37190E+16 4.4E-06  1.37190E+16 4.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.28843E+16 0.00043  1.72109E+16 0.00043  5.67344E+15 0.00124 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.66033E+16 0.00027  3.09298E+16 0.00024  5.67344E+15 0.00124 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.68817E+16 0.00049  3.68817E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.72914E+18 0.00048  4.60527E+17 0.00046  1.26861E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.26073E+14 0.00484 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.69293E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.28942E+18 0.00065 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11060E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11060E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.57055E+00 0.00049 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.84225E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.43521E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24239E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94236E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96906E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.84650E-01 0.00054 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.75947E-01 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.62296E+00 2.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04756E+02 4.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.75749E-01 0.00057  9.70564E-01 0.00055  5.38307E-03 0.00962 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.75980E-01 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  9.75783E-01 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.75980E-01 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  9.84685E-01 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71774E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71734E+01 9.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.95740E-07 0.00372 ];
IMP_EALF                  (idx, [1:   2]) = [  6.96625E-07 0.00166 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.53271E-01 0.00301 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.54084E-01 0.00124 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.79142E-03 0.00587  1.58234E-04 0.03628  9.83952E-04 0.01459  9.04580E-04 0.01489  2.56289E-03 0.00843  8.97075E-04 0.01510  2.84690E-04 0.02586 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.00315E-01 0.01365  9.65780E-03 0.02433  3.10940E-02 0.00046  1.10408E-01 0.00044  3.22224E-01 0.00031  1.31881E+00 0.00123  8.25650E+00 0.01221 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.55367E-03 0.00885  1.57623E-04 0.05843  9.18883E-04 0.02249  8.70755E-04 0.02311  2.45164E-03 0.01291  8.78638E-04 0.02260  2.76127E-04 0.04029 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.12144E-01 0.02184  1.25131E-02 0.00061  3.11020E-02 0.00065  1.10331E-01 0.00060  3.22360E-01 0.00045  1.31794E+00 0.00179  8.76658E+00 0.00681 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.56404E-05 0.00133  2.56315E-05 0.00134  2.73294E-05 0.01414 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.50147E-05 0.00121  2.50059E-05 0.00121  2.66697E-05 0.01418 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.53469E-03 0.00977  1.52071E-04 0.05849  9.14658E-04 0.02479  8.42838E-04 0.02629  2.44612E-03 0.01531  8.96424E-04 0.02481  2.82576E-04 0.04515 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.23918E-01 0.02425  1.25124E-02 0.00082  3.11276E-02 0.00082  1.10439E-01 0.00080  3.22563E-01 0.00056  1.31823E+00 0.00203  8.83089E+00 0.00807 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.52406E-05 0.00312  2.52339E-05 0.00314  2.50603E-05 0.03659 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.46242E-05 0.00306  2.46178E-05 0.00308  2.44425E-05 0.03655 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.59343E-03 0.03282  1.82716E-04 0.17026  8.32999E-04 0.08256  9.06320E-04 0.07980  2.49063E-03 0.04818  9.08450E-04 0.08177  2.72320E-04 0.13194 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.89441E-01 0.06993  1.24879E-02 5.0E-05  3.11474E-02 0.00184  1.10199E-01 0.00174  3.22146E-01 0.00148  1.31948E+00 0.00471  8.50600E+00 0.02390 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.61395E-03 0.03221  1.82266E-04 0.16964  8.20080E-04 0.08015  9.28129E-04 0.07785  2.50129E-03 0.04683  9.20311E-04 0.07930  2.61868E-04 0.13113 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.79809E-01 0.06807  1.24880E-02 5.0E-05  3.11468E-02 0.00182  1.10204E-01 0.00174  3.22184E-01 0.00147  1.31912E+00 0.00479  8.50575E+00 0.02390 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.24248E+02 0.03332 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.54271E-05 0.00080 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.48064E-05 0.00055 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.42728E-03 0.00607 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.13508E+02 0.00614 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.69652E-07 0.00079 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.73062E-06 0.00045  2.73043E-06 0.00046  2.76346E-06 0.00576 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.83731E-05 0.00096  3.83922E-05 0.00096  3.50856E-05 0.01129 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.41355E-01 0.00034  6.41359E-01 0.00035  6.52344E-01 0.00934 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04593E+01 0.01410 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.49107E+01 0.00051  3.33622E+01 0.00052 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.74906E+04 0.00320  3.01039E+05 0.00127  6.10597E+05 0.00084  6.51299E+05 0.00070  5.99158E+05 0.00063  6.41395E+05 0.00064  4.36020E+05 0.00063  3.85804E+05 0.00050  2.95090E+05 0.00090  2.41039E+05 0.00088  2.07879E+05 0.00087  1.87285E+05 0.00076  1.73081E+05 0.00069  1.64331E+05 0.00071  1.60088E+05 0.00093  1.38406E+05 0.00068  1.36956E+05 0.00094  1.35513E+05 0.00091  1.32968E+05 0.00110  2.59810E+05 0.00055  2.50893E+05 0.00064  1.80992E+05 0.00068  1.17188E+05 0.00085  1.35282E+05 0.00087  1.28443E+05 0.00090  1.15065E+05 0.00110  1.87151E+05 0.00066  4.31660E+04 0.00175  5.41070E+04 0.00133  4.90962E+04 0.00176  2.85565E+04 0.00196  4.94940E+04 0.00153  3.32511E+04 0.00190  2.75605E+04 0.00177  4.91066E+03 0.00284  4.40566E+03 0.00280  3.92172E+03 0.00442  3.75870E+03 0.00348  3.82666E+03 0.00422  4.19157E+03 0.00380  4.82323E+03 0.00411  4.80063E+03 0.00354  9.34803E+03 0.00280  1.53216E+04 0.00267  1.95666E+04 0.00228  5.23673E+04 0.00151  5.50809E+04 0.00124  5.88876E+04 0.00109  3.83413E+04 0.00135  2.71664E+04 0.00161  2.02022E+04 0.00166  2.38294E+04 0.00136  4.65243E+04 0.00140  6.54091E+04 0.00166  1.29662E+05 0.00138  2.03898E+05 0.00152  3.07972E+05 0.00138  1.97977E+05 0.00137  1.42020E+05 0.00146  1.02368E+05 0.00169  9.22646E+04 0.00174  9.11061E+04 0.00168  7.60385E+04 0.00198  5.15406E+04 0.00166  4.75897E+04 0.00177  4.23416E+04 0.00174  3.58388E+04 0.00179  2.83161E+04 0.00191  1.89912E+04 0.00188  6.75615E+03 0.00221 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.84486E-01 0.00047 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.37693E+18 0.00048  3.52243E+17 0.00134 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38288E-01 9.6E-05  1.55647E+00 0.00043 ];
INF_CAPT                  (idx, [1:   4]) = [  7.58027E-03 0.00054  3.53395E-02 0.00080 ];
INF_ABS                   (idx, [1:   4]) = [  9.50752E-03 0.00043  6.67591E-02 0.00108 ];
INF_FISS                  (idx, [1:   4]) = [  1.92725E-03 0.00033  3.14196E-02 0.00141 ];
INF_NSF                   (idx, [1:   4]) = [  5.10680E-03 0.00031  8.22103E-02 0.00146 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.64978E+00 7.5E-05  2.61653E+00 6.6E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04982E+02 8.8E-06  2.04701E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.66296E-08 0.00038  2.57524E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28782E-01 0.00010  1.48968E+00 0.00050 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43922E-01 0.00024  3.95422E-01 0.00060 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62151E-02 0.00035  9.35446E-02 0.00075 ];
INF_SCATT3                (idx, [1:   4]) = [  7.33224E-03 0.00308  2.80509E-02 0.00233 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03207E-02 0.00189 -8.82471E-03 0.00622 ];
INF_SCATT5                (idx, [1:   4]) = [  1.67877E-04 0.09140  6.63134E-03 0.00836 ];
INF_SCATT6                (idx, [1:   4]) = [  5.12404E-03 0.00324 -1.70133E-02 0.00284 ];
INF_SCATT7                (idx, [1:   4]) = [  7.87974E-04 0.01827  4.15165E-04 0.08153 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28824E-01 0.00010  1.48968E+00 0.00050 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43923E-01 0.00024  3.95422E-01 0.00060 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62153E-02 0.00035  9.35446E-02 0.00075 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.33236E-03 0.00307  2.80509E-02 0.00233 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03206E-02 0.00189 -8.82471E-03 0.00622 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.67700E-04 0.09164  6.63134E-03 0.00836 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.12371E-03 0.00323 -1.70133E-02 0.00284 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.87955E-04 0.01825  4.15165E-04 0.08153 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12565E-01 0.00031  1.00829E+00 0.00036 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56815E+00 0.00031  3.30592E-01 0.00036 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.46493E-03 0.00044  6.67591E-02 0.00108 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69731E-02 0.00016  6.79570E-02 0.00118 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11315E-01 9.8E-05  1.74664E-02 0.00030  1.17028E-03 0.00318  1.48851E+00 0.00050 ];
INF_S1                    (idx, [1:   8]) = [  2.38855E-01 0.00024  5.06732E-03 0.00093  5.04296E-04 0.00764  3.94918E-01 0.00060 ];
INF_S2                    (idx, [1:   8]) = [  9.77789E-02 0.00033 -1.56378E-03 0.00255  2.76579E-04 0.00954  9.32680E-02 0.00076 ];
INF_S3                    (idx, [1:   8]) = [  9.13531E-03 0.00242 -1.80307E-03 0.00171  9.80311E-05 0.01746  2.79528E-02 0.00234 ];
INF_S4                    (idx, [1:   8]) = [ -9.74727E-03 0.00197 -5.73473E-04 0.00426  1.30637E-07 1.00000 -8.82484E-03 0.00614 ];
INF_S5                    (idx, [1:   8]) = [  1.37691E-04 0.11177  3.01860E-05 0.10900 -3.82724E-05 0.03564  6.66962E-03 0.00823 ];
INF_S6                    (idx, [1:   8]) = [  5.26284E-03 0.00305 -1.38795E-04 0.01536 -5.15681E-05 0.02513 -1.69617E-02 0.00285 ];
INF_S7                    (idx, [1:   8]) = [  9.54972E-04 0.01464 -1.66998E-04 0.01184 -4.68769E-05 0.02195  4.62042E-04 0.07358 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11358E-01 9.8E-05  1.74664E-02 0.00030  1.17028E-03 0.00318  1.48851E+00 0.00050 ];
INF_SP1                   (idx, [1:   8]) = [  2.38856E-01 0.00024  5.06732E-03 0.00093  5.04296E-04 0.00764  3.94918E-01 0.00060 ];
INF_SP2                   (idx, [1:   8]) = [  9.77791E-02 0.00033 -1.56378E-03 0.00255  2.76579E-04 0.00954  9.32680E-02 0.00076 ];
INF_SP3                   (idx, [1:   8]) = [  9.13543E-03 0.00242 -1.80307E-03 0.00171  9.80311E-05 0.01746  2.79528E-02 0.00234 ];
INF_SP4                   (idx, [1:   8]) = [ -9.74710E-03 0.00197 -5.73473E-04 0.00426  1.30637E-07 1.00000 -8.82484E-03 0.00614 ];
INF_SP5                   (idx, [1:   8]) = [  1.37514E-04 0.11206  3.01860E-05 0.10900 -3.82724E-05 0.03564  6.66962E-03 0.00823 ];
INF_SP6                   (idx, [1:   8]) = [  5.26251E-03 0.00304 -1.38795E-04 0.01536 -5.15681E-05 0.02513 -1.69617E-02 0.00285 ];
INF_SP7                   (idx, [1:   8]) = [  9.54953E-04 0.01462 -1.66998E-04 0.01184 -4.68769E-05 0.02195  4.62042E-04 0.07358 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31948E-01 0.00065  1.18894E+00 0.00673 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33405E-01 0.00088  1.29259E+00 0.00794 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33699E-01 0.00085  1.27895E+00 0.00849 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28814E-01 0.00122  1.03440E+00 0.00726 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43712E+00 0.00065  2.80658E-01 0.00657 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42816E+00 0.00088  2.58271E-01 0.00794 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42636E+00 0.00085  2.61058E-01 0.00807 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45684E+00 0.00122  3.22646E-01 0.00712 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.55367E-03 0.00885  1.57623E-04 0.05843  9.18883E-04 0.02249  8.70755E-04 0.02311  2.45164E-03 0.01291  8.78638E-04 0.02260  2.76127E-04 0.04029 ];
LAMBDA                    (idx, [1:  14]) = [  8.12144E-01 0.02184  1.25131E-02 0.00061  3.11020E-02 0.00065  1.10331E-01 0.00060  3.22360E-01 0.00045  1.31794E+00 0.00179  8.76658E+00 0.00681 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0005' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:52:32 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:16:50 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299952 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.01520E+00  1.02177E+00  1.01468E+00  1.01386E+00  9.34491E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.1E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14684E-02 0.00115  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88532E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06626E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.07054E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.66983E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.48796E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.48708E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.33596E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.13310E-01 0.00115  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000657 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00013E+04 0.00088 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00013E+04 0.00088 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.19207E+02 ;
RUNNING_TIME              (idx, 1)        =  8.42939E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.61967E-01  4.61967E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.51617E-01  9.55000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  8.28280E+01  3.20032E+00  2.39407E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  7.45083E-01  2.58500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.21167E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  8.42938E+01  1.17635E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97315 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99895E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79459E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.44781E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.73740E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.94261E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.83125E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.27915E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61653E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.60945E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.89188E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.61373E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.17273E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.85267E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.19149E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.82847E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.58491E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.33707E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.44908E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.49114E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.69671E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.01835E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.22645E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.33792E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.36309E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.26767E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.79702E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 14 ;
BURNUP                     (idx, [1:  2])  = [  1.50000E+01  1.50014E+01 ];
BURN_DAYS                 (idx, 1)        =  3.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.26137E-01 0.00109 ];
U235_FISS                 (idx, [1:   4]) = [  7.12890E+15 0.00097  5.19817E-01 0.00072 ];
U238_FISS                 (idx, [1:   4]) = [  1.05466E+15 0.00273  7.68964E-02 0.00259 ];
PU239_FISS                (idx, [1:   4]) = [  4.84294E+15 0.00119  3.53136E-01 0.00102 ];
PU240_FISS                (idx, [1:   4]) = [  6.70349E+12 0.03337  4.88770E-04 0.03335 ];
PU241_FISS                (idx, [1:   4]) = [  6.68413E+14 0.00350  4.87382E-02 0.00343 ];
U235_CAPT                 (idx, [1:   4]) = [  1.59143E+15 0.00222  6.63683E-02 0.00215 ];
U238_CAPT                 (idx, [1:   4]) = [  9.08025E+15 0.00102  3.78659E-01 0.00075 ];
PU239_CAPT                (idx, [1:   4]) = [  2.67894E+15 0.00162  1.11724E-01 0.00157 ];
PU240_CAPT                (idx, [1:   4]) = [  1.60184E+15 0.00229  6.67969E-02 0.00215 ];
PU241_CAPT                (idx, [1:   4]) = [  2.41122E+14 0.00556  1.00568E-02 0.00557 ];
XE135_CAPT                (idx, [1:   4]) = [  7.56312E+14 0.00306  3.15413E-02 0.00302 ];
SM149_CAPT                (idx, [1:   4]) = [  2.13467E+14 0.00552  8.90234E-03 0.00549 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000657 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.00640E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000657 5.00801E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3152942 3.15765E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1803381 1.80601E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44334 4.43476E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000657 5.00801E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.93601E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.62153E+16 2.2E-05  3.62153E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36992E+16 4.4E-06  1.36992E+16 4.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.39757E+16 0.00046  1.81434E+16 0.00048  5.83238E+15 0.00126 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.76749E+16 0.00030  3.18426E+16 0.00027  5.83238E+15 0.00126 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.79702E+16 0.00050  3.79702E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.77734E+18 0.00049  4.72319E+17 0.00048  1.30502E+18 0.00056 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.36799E+14 0.00504 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.80117E+16 0.00030 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.32600E+18 0.00068 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10770E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10770E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.54982E+00 0.00055 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.82941E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.39502E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24168E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94193E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96920E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.63448E-01 0.00062 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.54904E-01 0.00062 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.64361E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05051E+02 4.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.54799E-01 0.00063  9.49748E-01 0.00063  5.15575E-03 0.01026 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.54302E-01 0.00030 ];
COL_KEFF                  (idx, [1:   2]) = [  9.53901E-01 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.54302E-01 0.00030 ];
ABS_KINF                  (idx, [1:   2]) = [  9.62845E-01 0.00030 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71315E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71339E+01 9.1E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.28497E-07 0.00374 ];
IMP_EALF                  (idx, [1:   2]) = [  7.24692E-07 0.00156 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.61833E-01 0.00281 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.61656E-01 0.00113 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.79480E-03 0.00644  1.61734E-04 0.03717  1.01468E-03 0.01498  9.08239E-04 0.01539  2.54586E-03 0.00877  8.97797E-04 0.01558  2.66490E-04 0.02767 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.72160E-01 0.01436  9.92078E-03 0.02295  3.10248E-02 0.00044  1.10438E-01 0.00049  3.22223E-01 0.00032  1.30488E+00 0.00178  8.21156E+00 0.01365 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.42869E-03 0.00926  1.50330E-04 0.05530  9.38626E-04 0.02328  8.72747E-04 0.02224  2.37285E-03 0.01298  8.40202E-04 0.02284  2.53935E-04 0.04291 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.81236E-01 0.02262  1.25272E-02 0.00076  3.10253E-02 0.00063  1.10417E-01 0.00066  3.22216E-01 0.00048  1.30181E+00 0.00265  8.87582E+00 0.00692 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.60926E-05 0.00139  2.60810E-05 0.00139  2.83021E-05 0.01637 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.49085E-05 0.00125  2.48974E-05 0.00126  2.70126E-05 0.01631 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.39342E-03 0.01043  1.50657E-04 0.05999  9.31361E-04 0.02557  8.56533E-04 0.02560  2.34001E-03 0.01554  8.70008E-04 0.02602  2.44841E-04 0.04648 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.77589E-01 0.02463  1.25248E-02 0.00103  3.10304E-02 0.00080  1.10297E-01 0.00084  3.22142E-01 0.00055  1.30968E+00 0.00289  8.88216E+00 0.00996 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.56168E-05 0.00316  2.56013E-05 0.00318  2.67025E-05 0.03980 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.44546E-05 0.00311  2.44398E-05 0.00314  2.54838E-05 0.03972 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.26957E-03 0.03365  1.35934E-04 0.21454  8.97466E-04 0.07908  8.13354E-04 0.08554  2.26269E-03 0.05157  8.95648E-04 0.09532  2.64480E-04 0.15681 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.23523E-01 0.08018  1.24887E-02 5.5E-05  3.10007E-02 0.00191  1.10336E-01 0.00196  3.22921E-01 0.00152  1.30559E+00 0.00655  8.93365E+00 0.01976 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.25681E-03 0.03259  1.42203E-04 0.21438  8.88432E-04 0.07723  8.10567E-04 0.08283  2.23520E-03 0.05001  9.12288E-04 0.09101  2.68123E-04 0.15139 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.44430E-01 0.08024  1.24887E-02 5.5E-05  3.10027E-02 0.00191  1.10332E-01 0.00196  3.22890E-01 0.00150  1.30588E+00 0.00652  8.93951E+00 0.01978 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.06584E+02 0.03397 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.58009E-05 0.00090 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.46296E-05 0.00061 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.30798E-03 0.00559 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.05840E+02 0.00571 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.68452E-07 0.00078 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.71055E-06 0.00043  2.71045E-06 0.00043  2.73184E-06 0.00575 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.84730E-05 0.00096  3.84870E-05 0.00096  3.59259E-05 0.01129 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.37335E-01 0.00036  6.37446E-01 0.00036  6.33004E-01 0.01065 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07408E+01 0.01439 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.48708E+01 0.00051  3.32622E+01 0.00051 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.80585E+04 0.00301  3.01365E+05 0.00165  6.10611E+05 0.00085  6.51163E+05 0.00071  5.97991E+05 0.00072  6.41807E+05 0.00074  4.35894E+05 0.00065  3.84745E+05 0.00065  2.94865E+05 0.00083  2.40774E+05 0.00073  2.07478E+05 0.00094  1.87129E+05 0.00093  1.73067E+05 0.00090  1.64500E+05 0.00090  1.60192E+05 0.00107  1.38281E+05 0.00092  1.36890E+05 0.00090  1.35642E+05 0.00110  1.32997E+05 0.00088  2.59725E+05 0.00067  2.50956E+05 0.00073  1.81100E+05 0.00088  1.17347E+05 0.00109  1.35176E+05 0.00114  1.28638E+05 0.00091  1.14991E+05 0.00102  1.86581E+05 0.00078  4.31736E+04 0.00203  5.39146E+04 0.00162  4.89152E+04 0.00150  2.85405E+04 0.00171  4.92397E+04 0.00156  3.31314E+04 0.00175  2.71690E+04 0.00178  4.81618E+03 0.00343  4.21122E+03 0.00353  3.73397E+03 0.00322  3.56292E+03 0.00365  3.62106E+03 0.00319  4.00519E+03 0.00365  4.69117E+03 0.00396  4.69781E+03 0.00295  9.19281E+03 0.00285  1.50875E+04 0.00246  1.94652E+04 0.00231  5.20396E+04 0.00146  5.45824E+04 0.00120  5.84541E+04 0.00134  3.78888E+04 0.00128  2.68061E+04 0.00188  1.98997E+04 0.00193  2.34928E+04 0.00216  4.57854E+04 0.00104  6.45631E+04 0.00171  1.28545E+05 0.00158  2.02646E+05 0.00150  3.06578E+05 0.00160  1.97375E+05 0.00177  1.41882E+05 0.00176  1.02169E+05 0.00183  9.21141E+04 0.00163  9.08989E+04 0.00174  7.60166E+04 0.00179  5.13881E+04 0.00177  4.74779E+04 0.00210  4.23821E+04 0.00204  3.58098E+04 0.00229  2.82954E+04 0.00191  1.89627E+04 0.00260  6.74048E+03 0.00221 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.62437E-01 0.00048 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.41651E+18 0.00044  3.60862E+17 0.00160 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38613E-01 0.00011  1.56031E+00 0.00053 ];
INF_CAPT                  (idx, [1:   4]) = [  7.75408E-03 0.00097  3.60067E-02 0.00087 ];
INF_ABS                   (idx, [1:   4]) = [  9.62035E-03 0.00078  6.66497E-02 0.00121 ];
INF_FISS                  (idx, [1:   4]) = [  1.86627E-03 0.00058  3.06431E-02 0.00161 ];
INF_NSF                   (idx, [1:   4]) = [  4.97365E-03 0.00058  8.08516E-02 0.00165 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.66502E+00 6.8E-05  2.63849E+00 6.4E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05192E+02 9.6E-06  2.05017E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.63531E-08 0.00033  2.57879E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28992E-01 0.00011  1.49362E+00 0.00061 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44078E-01 0.00019  3.96241E-01 0.00071 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62921E-02 0.00031  9.36758E-02 0.00097 ];
INF_SCATT3                (idx, [1:   4]) = [  7.30266E-03 0.00265  2.82040E-02 0.00233 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03230E-02 0.00200 -8.75005E-03 0.00899 ];
INF_SCATT5                (idx, [1:   4]) = [  1.81756E-04 0.10812  6.67786E-03 0.00798 ];
INF_SCATT6                (idx, [1:   4]) = [  5.12141E-03 0.00306 -1.70708E-02 0.00287 ];
INF_SCATT7                (idx, [1:   4]) = [  7.81409E-04 0.01993  4.44752E-04 0.13017 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29035E-01 0.00011  1.49362E+00 0.00061 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44079E-01 0.00019  3.96241E-01 0.00071 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62921E-02 0.00031  9.36758E-02 0.00097 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.30307E-03 0.00264  2.82040E-02 0.00233 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03229E-02 0.00200 -8.75005E-03 0.00899 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.81737E-04 0.10825  6.67786E-03 0.00798 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.12157E-03 0.00306 -1.70708E-02 0.00287 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.81370E-04 0.01997  4.44752E-04 0.13017 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12502E-01 0.00030  1.01163E+00 0.00047 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56861E+00 0.00030  3.29504E-01 0.00047 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.57743E-03 0.00077  6.66497E-02 0.00121 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69936E-02 0.00021  6.78766E-02 0.00130 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11619E-01 0.00011  1.73731E-02 0.00037  1.18124E-03 0.00416  1.49243E+00 0.00061 ];
INF_S1                    (idx, [1:   8]) = [  2.39040E-01 0.00019  5.03763E-03 0.00108  5.11617E-04 0.00563  3.95729E-01 0.00071 ];
INF_S2                    (idx, [1:   8]) = [  9.78555E-02 0.00031 -1.56333E-03 0.00261  2.79062E-04 0.01019  9.33968E-02 0.00097 ];
INF_S3                    (idx, [1:   8]) = [  9.10125E-03 0.00219 -1.79859E-03 0.00225  1.04092E-04 0.02098  2.80999E-02 0.00235 ];
INF_S4                    (idx, [1:   8]) = [ -9.75880E-03 0.00214 -5.64243E-04 0.00384  9.96333E-07 1.00000 -8.75105E-03 0.00899 ];
INF_S5                    (idx, [1:   8]) = [  1.44014E-04 0.12990  3.77423E-05 0.07388 -3.74712E-05 0.04625  6.71534E-03 0.00790 ];
INF_S6                    (idx, [1:   8]) = [  5.25663E-03 0.00291 -1.35212E-04 0.01578 -4.98667E-05 0.02663 -1.70209E-02 0.00288 ];
INF_S7                    (idx, [1:   8]) = [  9.50579E-04 0.01666 -1.69170E-04 0.01361 -4.50767E-05 0.02397  4.89829E-04 0.11803 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11662E-01 0.00011  1.73731E-02 0.00037  1.18124E-03 0.00416  1.49243E+00 0.00061 ];
INF_SP1                   (idx, [1:   8]) = [  2.39041E-01 0.00019  5.03763E-03 0.00108  5.11617E-04 0.00563  3.95729E-01 0.00071 ];
INF_SP2                   (idx, [1:   8]) = [  9.78554E-02 0.00031 -1.56333E-03 0.00261  2.79062E-04 0.01019  9.33968E-02 0.00097 ];
INF_SP3                   (idx, [1:   8]) = [  9.10166E-03 0.00219 -1.79859E-03 0.00225  1.04092E-04 0.02098  2.80999E-02 0.00235 ];
INF_SP4                   (idx, [1:   8]) = [ -9.75868E-03 0.00214 -5.64243E-04 0.00384  9.96333E-07 1.00000 -8.75105E-03 0.00899 ];
INF_SP5                   (idx, [1:   8]) = [  1.43995E-04 0.13004  3.77423E-05 0.07388 -3.74712E-05 0.04625  6.71534E-03 0.00790 ];
INF_SP6                   (idx, [1:   8]) = [  5.25678E-03 0.00291 -1.35212E-04 0.01578 -4.98667E-05 0.02663 -1.70209E-02 0.00288 ];
INF_SP7                   (idx, [1:   8]) = [  9.50540E-04 0.01669 -1.69170E-04 0.01361 -4.50767E-05 0.02397  4.89829E-04 0.11803 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31852E-01 0.00062  1.20601E+00 0.00590 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33395E-01 0.00081  1.31383E+00 0.00759 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33453E-01 0.00084  1.30182E+00 0.00644 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28776E-01 0.00092  1.04469E+00 0.00757 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43771E+00 0.00062  2.76623E-01 0.00585 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42822E+00 0.00081  2.54056E-01 0.00745 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42786E+00 0.00084  2.56308E-01 0.00648 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45706E+00 0.00092  3.19504E-01 0.00741 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.42869E-03 0.00926  1.50330E-04 0.05530  9.38626E-04 0.02328  8.72747E-04 0.02224  2.37285E-03 0.01298  8.40202E-04 0.02284  2.53935E-04 0.04291 ];
LAMBDA                    (idx, [1:  14]) = [  7.81236E-01 0.02262  1.25272E-02 0.00076  3.10253E-02 0.00063  1.10417E-01 0.00066  3.22216E-01 0.00048  1.30181E+00 0.00265  8.87582E+00 0.00692 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0005' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:52:32 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:22:32 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299952 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00370E+00  1.00837E+00  1.00319E+00  1.00993E+00  9.74803E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15229E-02 0.00111  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88477E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06591E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.07020E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67270E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.48193E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.48104E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.32337E+00 0.00042  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.14929E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001053 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00021E+04 0.00088 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00021E+04 0.00088 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.47709E+02 ;
RUNNING_TIME              (idx, 1)        =  9.00008E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.61967E-01  4.61967E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.71933E-01  1.02833E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  8.84623E+01  3.25567E+00  2.37858E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  7.96850E-01  2.61833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.52167E-02  7.33332E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.00003E+01  1.18259E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97449 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99884E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79785E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.49748E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.72793E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.39036E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.87866E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.31327E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61879E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.59657E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.27155E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.69323E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.51631E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.12224E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.55234E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.88101E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.80327E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.35944E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.48075E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.52261E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.27047E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.35218E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.27183E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.31259E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.77484E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.28156E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.90341E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 15 ;
BURNUP                     (idx, [1:  2])  = [  1.75000E+01  1.75016E+01 ];
BURN_DAYS                 (idx, 1)        =  4.37500E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.49667E-01 0.00109 ];
U235_FISS                 (idx, [1:   4]) = [  6.53916E+15 0.00103  4.77584E-01 0.00083 ];
U238_FISS                 (idx, [1:   4]) = [  1.08078E+15 0.00267  7.89248E-02 0.00251 ];
PU239_FISS                (idx, [1:   4]) = [  5.20097E+15 0.00122  3.79842E-01 0.00102 ];
PU240_FISS                (idx, [1:   4]) = [  8.24399E+12 0.03118  6.02120E-04 0.03120 ];
PU241_FISS                (idx, [1:   4]) = [  8.46897E+14 0.00310  6.18500E-02 0.00300 ];
U233_CAPT                 (idx, [1:   4]) = [  7.95088E+09 1.00000  3.14268E-07 1.00000 ];
U235_CAPT                 (idx, [1:   4]) = [  1.46701E+15 0.00233  5.85545E-02 0.00225 ];
U238_CAPT                 (idx, [1:   4]) = [  9.30706E+15 0.00111  3.71465E-01 0.00080 ];
PU239_CAPT                (idx, [1:   4]) = [  2.86802E+15 0.00164  1.14482E-01 0.00160 ];
PU240_CAPT                (idx, [1:   4]) = [  1.82559E+15 0.00220  7.28650E-02 0.00209 ];
PU241_CAPT                (idx, [1:   4]) = [  3.04877E+14 0.00521  1.21689E-02 0.00517 ];
XE135_CAPT                (idx, [1:   4]) = [  7.59746E+14 0.00311  3.03281E-02 0.00311 ];
SM149_CAPT                (idx, [1:   4]) = [  2.19605E+14 0.00590  8.76631E-03 0.00591 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001053 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.94970E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001053 5.00795E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3204707 3.20921E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1751611 1.75400E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44735 4.47452E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001053 5.00795E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -8.94070E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.64293E+16 2.3E-05  3.64293E+16 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36806E+16 4.6E-06  1.36806E+16 4.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.50475E+16 0.00044  1.90714E+16 0.00048  5.97606E+15 0.00126 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.87281E+16 0.00029  3.27520E+16 0.00028  5.97606E+15 0.00126 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.90341E+16 0.00052  3.90341E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.82467E+18 0.00050  4.84663E+17 0.00050  1.34000E+18 0.00056 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.49349E+14 0.00524 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.90774E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.36072E+18 0.00067 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10481E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10481E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.52888E+00 0.00055 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.82470E-01 0.00035 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.35215E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24043E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94170E-01 3.7E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96862E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.42549E-01 0.00062 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.34115E-01 0.00063 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.66284E+00 2.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05330E+02 4.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.34067E-01 0.00064  9.29189E-01 0.00063  4.92590E-03 0.01004 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.33764E-01 0.00030 ];
COL_KEFF                  (idx, [1:   2]) = [  9.33392E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.33764E-01 0.00030 ];
ABS_KINF                  (idx, [1:   2]) = [  9.42198E-01 0.00029 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70969E+01 0.00023 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70985E+01 9.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.54425E-07 0.00390 ];
IMP_EALF                  (idx, [1:   2]) = [  7.50822E-07 0.00170 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.68822E-01 0.00283 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.68757E-01 0.00114 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.81654E-03 0.00641  1.63285E-04 0.03595  1.02486E-03 0.01504  9.07018E-04 0.01607  2.54080E-03 0.00963  9.11321E-04 0.01635  2.69252E-04 0.02846 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.70845E-01 0.01518  9.86022E-03 0.02337  3.09193E-02 0.00043  1.10552E-01 0.00051  3.22454E-01 0.00033  1.30112E+00 0.00161  7.82155E+00 0.01623 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.28175E-03 0.00913  1.54653E-04 0.05418  9.35057E-04 0.02225  8.24631E-04 0.02523  2.29598E-03 0.01347  8.27732E-04 0.02400  2.43693E-04 0.04344 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.70175E-01 0.02260  1.25423E-02 0.00084  3.09085E-02 0.00064  1.10648E-01 0.00070  3.22331E-01 0.00050  1.30210E+00 0.00233  8.62003E+00 0.00875 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.66685E-05 0.00145  2.66593E-05 0.00145  2.84150E-05 0.01439 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.49045E-05 0.00126  2.48958E-05 0.00127  2.65314E-05 0.01435 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.28333E-03 0.01018  1.51018E-04 0.06229  9.26490E-04 0.02589  8.43757E-04 0.02695  2.28697E-03 0.01557  8.40005E-04 0.02637  2.35093E-04 0.04656 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.62569E-01 0.02437  1.25231E-02 0.00088  3.09212E-02 0.00083  1.10654E-01 0.00092  3.22246E-01 0.00058  1.30260E+00 0.00296  8.76490E+00 0.01088 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.59319E-05 0.00321  2.59197E-05 0.00321  2.63884E-05 0.04053 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.42172E-05 0.00315  2.42058E-05 0.00315  2.46651E-05 0.04058 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.05845E-03 0.03535  1.59127E-04 0.23614  8.83741E-04 0.08727  9.02252E-04 0.09420  2.17136E-03 0.05397  6.94016E-04 0.09526  2.47950E-04 0.16147 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.15642E-01 0.08524  1.25388E-02 0.00281  3.08281E-02 0.00197  1.10879E-01 0.00228  3.22119E-01 0.00158  1.30744E+00 0.00687  9.09430E+00 0.01839 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.06430E-03 0.03479  1.56568E-04 0.22831  8.83522E-04 0.08317  9.01280E-04 0.09091  2.19469E-03 0.05256  6.76316E-04 0.09590  2.51925E-04 0.16070 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.02983E-01 0.08360  1.25388E-02 0.00281  3.08201E-02 0.00195  1.10869E-01 0.00227  3.22210E-01 0.00158  1.30798E+00 0.00678  9.09640E+00 0.01839 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.97160E+02 0.03565 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.63646E-05 0.00087 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.46213E-05 0.00059 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.14858E-03 0.00676 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.95343E+02 0.00677 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.66775E-07 0.00081 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.69479E-06 0.00042  2.69467E-06 0.00042  2.71782E-06 0.00547 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.85631E-05 0.00096  3.85844E-05 0.00097  3.48038E-05 0.01064 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.33108E-01 0.00036  6.33280E-01 0.00037  6.16098E-01 0.00987 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07831E+01 0.01491 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.48104E+01 0.00051  3.32110E+01 0.00054 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.88198E+04 0.00266  3.02719E+05 0.00124  6.10432E+05 0.00084  6.50320E+05 0.00082  5.98430E+05 0.00074  6.41249E+05 0.00067  4.35662E+05 0.00076  3.85832E+05 0.00070  2.95085E+05 0.00069  2.40472E+05 0.00067  2.07394E+05 0.00085  1.87218E+05 0.00105  1.72938E+05 0.00071  1.64056E+05 0.00108  1.60405E+05 0.00095  1.38468E+05 0.00097  1.36586E+05 0.00089  1.35436E+05 0.00094  1.33085E+05 0.00083  2.60023E+05 0.00061  2.51141E+05 0.00076  1.80997E+05 0.00075  1.17338E+05 0.00120  1.35212E+05 0.00065  1.28613E+05 0.00078  1.14723E+05 0.00098  1.86177E+05 0.00059  4.31446E+04 0.00142  5.38247E+04 0.00170  4.87327E+04 0.00136  2.86134E+04 0.00207  4.92387E+04 0.00148  3.29097E+04 0.00179  2.69234E+04 0.00188  4.66670E+03 0.00262  4.07936E+03 0.00349  3.56972E+03 0.00398  3.43382E+03 0.00363  3.48876E+03 0.00344  3.83501E+03 0.00440  4.56797E+03 0.00365  4.60540E+03 0.00355  9.03927E+03 0.00232  1.49459E+04 0.00258  1.92954E+04 0.00204  5.15132E+04 0.00176  5.41036E+04 0.00115  5.76832E+04 0.00126  3.76002E+04 0.00153  2.64984E+04 0.00164  1.96746E+04 0.00218  2.31359E+04 0.00141  4.52565E+04 0.00180  6.39271E+04 0.00168  1.27358E+05 0.00177  2.01113E+05 0.00182  3.04704E+05 0.00186  1.96517E+05 0.00206  1.41553E+05 0.00179  1.01963E+05 0.00187  9.18658E+04 0.00205  9.05820E+04 0.00220  7.57634E+04 0.00205  5.12354E+04 0.00240  4.74041E+04 0.00237  4.22669E+04 0.00218  3.58078E+04 0.00221  2.82722E+04 0.00227  1.89823E+04 0.00215  6.75287E+03 0.00267 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.41820E-01 0.00059 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.45583E+18 0.00065  3.68878E+17 0.00188 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38767E-01 0.00013  1.56254E+00 0.00056 ];
INF_CAPT                  (idx, [1:   4]) = [  7.92180E-03 0.00082  3.66424E-02 0.00110 ];
INF_ABS                   (idx, [1:   4]) = [  9.73009E-03 0.00071  6.66000E-02 0.00147 ];
INF_FISS                  (idx, [1:   4]) = [  1.80830E-03 0.00049  2.99576E-02 0.00193 ];
INF_NSF                   (idx, [1:   4]) = [  4.84523E-03 0.00049  7.96540E-02 0.00198 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.67945E+00 5.9E-05  2.65888E+00 6.4E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05395E+02 8.4E-06  2.05314E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.60771E-08 0.00040  2.58248E-06 0.00020 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29031E-01 0.00013  1.49592E+00 0.00064 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44041E-01 0.00021  3.96709E-01 0.00072 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62619E-02 0.00035  9.37290E-02 0.00110 ];
INF_SCATT3                (idx, [1:   4]) = [  7.27688E-03 0.00344  2.82189E-02 0.00218 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03395E-02 0.00216 -8.83211E-03 0.00815 ];
INF_SCATT5                (idx, [1:   4]) = [  1.70364E-04 0.10373  6.62880E-03 0.00802 ];
INF_SCATT6                (idx, [1:   4]) = [  5.11459E-03 0.00303 -1.71176E-02 0.00376 ];
INF_SCATT7                (idx, [1:   4]) = [  7.57834E-04 0.01626  4.63329E-04 0.11727 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29073E-01 0.00013  1.49592E+00 0.00064 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44041E-01 0.00021  3.96709E-01 0.00072 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62619E-02 0.00035  9.37290E-02 0.00110 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.27707E-03 0.00344  2.82189E-02 0.00218 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03398E-02 0.00216 -8.83211E-03 0.00815 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.70447E-04 0.10378  6.62880E-03 0.00802 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.11456E-03 0.00303 -1.71176E-02 0.00376 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.57924E-04 0.01624  4.63329E-04 0.11727 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12434E-01 0.00026  1.01391E+00 0.00052 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56912E+00 0.00026  3.28764E-01 0.00052 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.68746E-03 0.00072  6.66000E-02 0.00147 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69930E-02 0.00024  6.77815E-02 0.00146 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11774E-01 0.00012  1.72571E-02 0.00052  1.16156E-03 0.00523  1.49476E+00 0.00065 ];
INF_S1                    (idx, [1:   8]) = [  2.39044E-01 0.00020  4.99634E-03 0.00089  5.02460E-04 0.00785  3.96206E-01 0.00072 ];
INF_S2                    (idx, [1:   8]) = [  9.78156E-02 0.00033 -1.55372E-03 0.00210  2.71636E-04 0.01064  9.34574E-02 0.00110 ];
INF_S3                    (idx, [1:   8]) = [  9.05740E-03 0.00266 -1.78052E-03 0.00181  9.82855E-05 0.02218  2.81207E-02 0.00216 ];
INF_S4                    (idx, [1:   8]) = [ -9.77828E-03 0.00218 -5.61265E-04 0.00447 -2.08083E-06 0.91480 -8.83003E-03 0.00816 ];
INF_S5                    (idx, [1:   8]) = [  1.34159E-04 0.13087  3.62056E-05 0.07711 -4.04931E-05 0.03889  6.66929E-03 0.00799 ];
INF_S6                    (idx, [1:   8]) = [  5.25325E-03 0.00314 -1.38657E-04 0.02110 -5.08837E-05 0.02905 -1.70668E-02 0.00375 ];
INF_S7                    (idx, [1:   8]) = [  9.26954E-04 0.01415 -1.69120E-04 0.01584 -4.57162E-05 0.02797  5.09045E-04 0.10598 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11816E-01 0.00012  1.72571E-02 0.00052  1.16156E-03 0.00523  1.49476E+00 0.00065 ];
INF_SP1                   (idx, [1:   8]) = [  2.39045E-01 0.00020  4.99634E-03 0.00089  5.02460E-04 0.00785  3.96206E-01 0.00072 ];
INF_SP2                   (idx, [1:   8]) = [  9.78156E-02 0.00033 -1.55372E-03 0.00210  2.71636E-04 0.01064  9.34574E-02 0.00110 ];
INF_SP3                   (idx, [1:   8]) = [  9.05758E-03 0.00266 -1.78052E-03 0.00181  9.82855E-05 0.02218  2.81207E-02 0.00216 ];
INF_SP4                   (idx, [1:   8]) = [ -9.77853E-03 0.00218 -5.61265E-04 0.00447 -2.08083E-06 0.91480 -8.83003E-03 0.00816 ];
INF_SP5                   (idx, [1:   8]) = [  1.34241E-04 0.13092  3.62056E-05 0.07711 -4.04931E-05 0.03889  6.66929E-03 0.00799 ];
INF_SP6                   (idx, [1:   8]) = [  5.25321E-03 0.00314 -1.38657E-04 0.02110 -5.08837E-05 0.02905 -1.70668E-02 0.00375 ];
INF_SP7                   (idx, [1:   8]) = [  9.27045E-04 0.01414 -1.69120E-04 0.01584 -4.57162E-05 0.02797  5.09045E-04 0.10598 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31874E-01 0.00057  1.20740E+00 0.00781 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33598E-01 0.00085  1.31325E+00 0.00794 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33694E-01 0.00089  1.30509E+00 0.01207 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28415E-01 0.00068  1.04654E+00 0.00732 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43757E+00 0.00057  2.76480E-01 0.00782 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42698E+00 0.00085  2.54209E-01 0.00797 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42639E+00 0.00089  2.56314E-01 0.01220 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45935E+00 0.00067  3.18916E-01 0.00725 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.28175E-03 0.00913  1.54653E-04 0.05418  9.35057E-04 0.02225  8.24631E-04 0.02523  2.29598E-03 0.01347  8.27732E-04 0.02400  2.43693E-04 0.04344 ];
LAMBDA                    (idx, [1:  14]) = [  7.70175E-01 0.02260  1.25423E-02 0.00084  3.09085E-02 0.00064  1.10648E-01 0.00070  3.22331E-01 0.00050  1.30210E+00 0.00233  8.62003E+00 0.00875 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0005' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:52:32 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:28:12 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299952 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00020E+00  1.00293E+00  9.95691E-01  1.00416E+00  9.97015E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.3E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15748E-02 0.00108  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88425E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06449E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06880E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67539E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.47826E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.47737E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.32219E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.16513E-01 0.00112  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000808 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00016E+04 0.00089 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00016E+04 0.00089 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.75970E+02 ;
RUNNING_TIME              (idx, 1)        =  9.56593E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.61967E-01  4.61967E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.91983E-01  9.71667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.40492E+01  3.11265E+00  2.47428E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  8.47750E-01  2.53167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.84500E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.56592E+01  1.18859E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97568 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99975E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80090E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.55478E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.72115E+04 ;
TOT_SF_RATE               (idx, 1)        =  5.49743E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.93117E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.35132E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62358E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.58598E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.69693E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.77340E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.90902E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.42116E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.87915E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.93128E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.01171E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.38111E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.50991E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.55261E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.91204E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.68509E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.32503E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.29008E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.59812E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.29678E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.01133E+12 0.00054  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 16 ;
BURNUP                     (idx, [1:  2])  = [  2.00000E+01  2.00018E+01 ];
BURN_DAYS                 (idx, 1)        =  5.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.74544E-01 0.00113 ];
U235_FISS                 (idx, [1:   4]) = [  5.98226E+15 0.00102  4.37852E-01 0.00088 ];
U238_FISS                 (idx, [1:   4]) = [  1.10567E+15 0.00263  8.09104E-02 0.00243 ];
PU239_FISS                (idx, [1:   4]) = [  5.52176E+15 0.00117  4.04121E-01 0.00093 ];
PU240_FISS                (idx, [1:   4]) = [  9.72660E+12 0.02962  7.11492E-04 0.02961 ];
PU241_FISS                (idx, [1:   4]) = [  1.02570E+15 0.00282  7.50684E-02 0.00273 ];
U235_CAPT                 (idx, [1:   4]) = [  1.33895E+15 0.00250  5.11940E-02 0.00247 ];
U238_CAPT                 (idx, [1:   4]) = [  9.55028E+15 0.00112  3.65104E-01 0.00079 ];
PU239_CAPT                (idx, [1:   4]) = [  3.05191E+15 0.00151  1.16690E-01 0.00150 ];
PU240_CAPT                (idx, [1:   4]) = [  2.04784E+15 0.00214  7.82886E-02 0.00200 ];
PU241_CAPT                (idx, [1:   4]) = [  3.70020E+14 0.00467  1.41488E-02 0.00469 ];
XE135_CAPT                (idx, [1:   4]) = [  7.64502E+14 0.00335  2.92318E-02 0.00335 ];
SM149_CAPT                (idx, [1:   4]) = [  2.27605E+14 0.00551  8.70252E-03 0.00551 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000808 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.14810E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000808 5.00815E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3255377 3.26028E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1700758 1.70319E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44673 4.46800E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000808 5.00815E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -1.76951E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.66310E+16 2.3E-05  3.66310E+16 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36630E+16 4.7E-06  1.36630E+16 4.7E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.61411E+16 0.00046  2.00148E+16 0.00049  6.12629E+15 0.00119 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.98040E+16 0.00030  3.36778E+16 0.00029  6.12629E+15 0.00119 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.01133E+16 0.00054  4.01133E+16 0.00054  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.87386E+18 0.00050  4.97806E+17 0.00050  1.37606E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.58512E+14 0.00507 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.01626E+16 0.00031 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.39692E+18 0.00064 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10191E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10191E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.50710E+00 0.00055 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.81262E-01 0.00035 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.31440E-01 0.00037 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23956E+00 0.00038 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94165E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96881E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.21511E-01 0.00061 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.13278E-01 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.68104E+00 2.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05595E+02 4.7E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.13317E-01 0.00063  9.08559E-01 0.00061  4.71896E-03 0.01030 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.13592E-01 0.00031 ];
COL_KEFF                  (idx, [1:   2]) = [  9.13320E-01 0.00054 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.13592E-01 0.00031 ];
ABS_KINF                  (idx, [1:   2]) = [  9.21832E-01 0.00030 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70616E+01 0.00023 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70620E+01 9.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.81590E-07 0.00397 ];
IMP_EALF                  (idx, [1:   2]) = [  7.78790E-07 0.00169 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.76542E-01 0.00267 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.76693E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.84788E-03 0.00633  1.52855E-04 0.03825  1.03251E-03 0.01460  9.05734E-04 0.01612  2.56394E-03 0.00949  9.23208E-04 0.01582  2.69639E-04 0.02902 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.64653E-01 0.01526  9.29057E-03 0.02655  3.08540E-02 0.00046  1.10854E-01 0.00050  3.22593E-01 0.00031  1.28676E+00 0.00194  7.81686E+00 0.01662 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.20552E-03 0.00918  1.38217E-04 0.05865  9.27931E-04 0.02147  8.01157E-04 0.02407  2.24856E-03 0.01383  8.42438E-04 0.02342  2.47212E-04 0.04572 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.73314E-01 0.02384  1.25579E-02 0.00107  3.08736E-02 0.00067  1.10855E-01 0.00071  3.22422E-01 0.00047  1.28304E+00 0.00289  8.59416E+00 0.00966 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.72519E-05 0.00141  2.72380E-05 0.00143  2.96925E-05 0.01730 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.48851E-05 0.00129  2.48724E-05 0.00130  2.71210E-05 0.01734 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.17027E-03 0.01034  1.45007E-04 0.06436  9.10320E-04 0.02573  8.15491E-04 0.02753  2.19467E-03 0.01643  8.41553E-04 0.02725  2.63232E-04 0.04688 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.94134E-01 0.02575  1.25336E-02 0.00122  3.08882E-02 0.00086  1.10817E-01 0.00093  3.22555E-01 0.00056  1.28528E+00 0.00381  8.42223E+00 0.01355 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.65714E-05 0.00361  2.65595E-05 0.00362  2.55261E-05 0.04014 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.42636E-05 0.00356  2.42528E-05 0.00358  2.32989E-05 0.04010 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.41019E-03 0.03572  1.84608E-04 0.19422  1.00830E-03 0.08317  8.93191E-04 0.09339  2.14707E-03 0.05350  9.00855E-04 0.10118  2.76164E-04 0.15101 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.28896E-01 0.08346  1.25448E-02 0.00309  3.07967E-02 0.00194  1.11439E-01 0.00241  3.22789E-01 0.00166  1.29455E+00 0.00791  8.32943E+00 0.03414 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.42204E-03 0.03516  1.88889E-04 0.18402  1.00998E-03 0.08155  8.84262E-04 0.08992  2.18179E-03 0.05223  8.81888E-04 0.10030  2.75223E-04 0.14802 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.21532E-01 0.08166  1.25447E-02 0.00309  3.07918E-02 0.00192  1.11425E-01 0.00240  3.22691E-01 0.00165  1.29458E+00 0.00789  8.33108E+00 0.03415 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.05063E+02 0.03601 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.69221E-05 0.00092 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.45839E-05 0.00071 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.30227E-03 0.00688 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.97022E+02 0.00694 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.65418E-07 0.00079 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.68178E-06 0.00043  2.68166E-06 0.00043  2.70170E-06 0.00585 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.86877E-05 0.00095  3.87076E-05 0.00095  3.51307E-05 0.01087 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.29381E-01 0.00037  6.29623E-01 0.00038  6.00754E-01 0.01002 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08873E+01 0.01462 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.47737E+01 0.00049  3.31811E+01 0.00052 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.92176E+04 0.00363  3.03128E+05 0.00152  6.09934E+05 0.00099  6.51654E+05 0.00076  5.98422E+05 0.00054  6.42645E+05 0.00078  4.35681E+05 0.00061  3.85188E+05 0.00072  2.95191E+05 0.00078  2.40782E+05 0.00063  2.07494E+05 0.00074  1.87716E+05 0.00101  1.72951E+05 0.00077  1.64502E+05 0.00089  1.60201E+05 0.00092  1.38514E+05 0.00072  1.37072E+05 0.00092  1.35565E+05 0.00096  1.33218E+05 0.00091  2.59942E+05 0.00056  2.51336E+05 0.00049  1.81098E+05 0.00086  1.17301E+05 0.00100  1.35341E+05 0.00118  1.28794E+05 0.00079  1.14674E+05 0.00094  1.85793E+05 0.00077  4.30032E+04 0.00179  5.36584E+04 0.00155  4.86705E+04 0.00141  2.83342E+04 0.00188  4.90376E+04 0.00141  3.27441E+04 0.00152  2.66227E+04 0.00171  4.56382E+03 0.00268  3.96385E+03 0.00320  3.45040E+03 0.00413  3.29538E+03 0.00282  3.34925E+03 0.00406  3.72802E+03 0.00366  4.43631E+03 0.00358  4.52303E+03 0.00394  8.92414E+03 0.00401  1.47016E+04 0.00256  1.91362E+04 0.00225  5.11120E+04 0.00157  5.38335E+04 0.00112  5.73752E+04 0.00129  3.72128E+04 0.00145  2.61490E+04 0.00157  1.93862E+04 0.00230  2.28693E+04 0.00158  4.47019E+04 0.00134  6.33667E+04 0.00152  1.26400E+05 0.00100  2.00035E+05 0.00140  3.03608E+05 0.00147  1.95973E+05 0.00178  1.41050E+05 0.00159  1.01740E+05 0.00164  9.17124E+04 0.00167  9.05319E+04 0.00167  7.57523E+04 0.00174  5.13449E+04 0.00175  4.74101E+04 0.00181  4.23101E+04 0.00190  3.57980E+04 0.00211  2.82899E+04 0.00171  1.89135E+04 0.00180  6.71508E+03 0.00216 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.21555E-01 0.00065 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.49635E+18 0.00068  3.77559E+17 0.00124 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38913E-01 0.00014  1.56415E+00 0.00037 ];
INF_CAPT                  (idx, [1:   4]) = [  8.08093E-03 0.00073  3.72148E-02 0.00075 ];
INF_ABS                   (idx, [1:   4]) = [  9.83503E-03 0.00058  6.64566E-02 0.00099 ];
INF_FISS                  (idx, [1:   4]) = [  1.75410E-03 0.00047  2.92418E-02 0.00130 ];
INF_NSF                   (idx, [1:   4]) = [  4.72535E-03 0.00043  7.83091E-02 0.00134 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.69389E+00 6.3E-05  2.67798E+00 6.3E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05595E+02 9.6E-06  2.05594E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.57749E-08 0.00052  2.58538E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29077E-01 0.00015  1.49766E+00 0.00043 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44041E-01 0.00021  3.97018E-01 0.00051 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62739E-02 0.00025  9.36327E-02 0.00101 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32493E-03 0.00319  2.80357E-02 0.00216 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03263E-02 0.00167 -9.01314E-03 0.00846 ];
INF_SCATT5                (idx, [1:   4]) = [  1.59154E-04 0.11934  6.65711E-03 0.00803 ];
INF_SCATT6                (idx, [1:   4]) = [  5.09935E-03 0.00315 -1.72335E-02 0.00331 ];
INF_SCATT7                (idx, [1:   4]) = [  7.32738E-04 0.02408  4.45338E-04 0.09295 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29121E-01 0.00015  1.49766E+00 0.00043 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44043E-01 0.00021  3.97018E-01 0.00051 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62739E-02 0.00025  9.36327E-02 0.00101 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32449E-03 0.00319  2.80357E-02 0.00216 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03264E-02 0.00167 -9.01314E-03 0.00846 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.59296E-04 0.11905  6.65711E-03 0.00803 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.09936E-03 0.00316 -1.72335E-02 0.00331 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.32723E-04 0.02398  4.45338E-04 0.09295 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12435E-01 0.00030  1.01567E+00 0.00033 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56911E+00 0.00030  3.28190E-01 0.00033 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.79134E-03 0.00059  6.64566E-02 0.00099 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69897E-02 0.00022  6.76580E-02 0.00118 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11923E-01 0.00014  1.71542E-02 0.00052  1.16909E-03 0.00462  1.49649E+00 0.00043 ];
INF_S1                    (idx, [1:   8]) = [  2.39073E-01 0.00021  4.96843E-03 0.00113  5.00552E-04 0.00705  3.96518E-01 0.00052 ];
INF_S2                    (idx, [1:   8]) = [  9.78249E-02 0.00025 -1.55091E-03 0.00192  2.73519E-04 0.00790  9.33592E-02 0.00101 ];
INF_S3                    (idx, [1:   8]) = [  9.10250E-03 0.00254 -1.77758E-03 0.00160  1.01744E-04 0.01976  2.79339E-02 0.00214 ];
INF_S4                    (idx, [1:   8]) = [ -9.77108E-03 0.00167 -5.55254E-04 0.00458  3.09963E-06 0.49475 -9.01624E-03 0.00845 ];
INF_S5                    (idx, [1:   8]) = [  1.18562E-04 0.14934  4.05918E-05 0.05841 -3.55858E-05 0.04812  6.69270E-03 0.00794 ];
INF_S6                    (idx, [1:   8]) = [  5.23543E-03 0.00295 -1.36077E-04 0.02367 -4.85839E-05 0.02556 -1.71849E-02 0.00332 ];
INF_S7                    (idx, [1:   8]) = [  9.01357E-04 0.01878 -1.68618E-04 0.01086 -4.31564E-05 0.02844  4.88494E-04 0.08438 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11967E-01 0.00014  1.71542E-02 0.00052  1.16909E-03 0.00462  1.49649E+00 0.00043 ];
INF_SP1                   (idx, [1:   8]) = [  2.39074E-01 0.00021  4.96843E-03 0.00113  5.00552E-04 0.00705  3.96518E-01 0.00052 ];
INF_SP2                   (idx, [1:   8]) = [  9.78248E-02 0.00025 -1.55091E-03 0.00192  2.73519E-04 0.00790  9.33592E-02 0.00101 ];
INF_SP3                   (idx, [1:   8]) = [  9.10207E-03 0.00255 -1.77758E-03 0.00160  1.01744E-04 0.01976  2.79339E-02 0.00214 ];
INF_SP4                   (idx, [1:   8]) = [ -9.77113E-03 0.00167 -5.55254E-04 0.00458  3.09963E-06 0.49475 -9.01624E-03 0.00845 ];
INF_SP5                   (idx, [1:   8]) = [  1.18704E-04 0.14889  4.05918E-05 0.05841 -3.55858E-05 0.04812  6.69270E-03 0.00794 ];
INF_SP6                   (idx, [1:   8]) = [  5.23543E-03 0.00296 -1.36077E-04 0.02367 -4.85839E-05 0.02556 -1.71849E-02 0.00332 ];
INF_SP7                   (idx, [1:   8]) = [  9.01341E-04 0.01870 -1.68618E-04 0.01086 -4.31564E-05 0.02844  4.88494E-04 0.08438 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31937E-01 0.00065  1.21589E+00 0.00905 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33339E-01 0.00084  1.31696E+00 0.01120 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33431E-01 0.00088  1.32752E+00 0.01100 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29100E-01 0.00103  1.04927E+00 0.00954 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43719E+00 0.00065  2.74682E-01 0.00898 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42856E+00 0.00084  2.53853E-01 0.01094 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42800E+00 0.00088  2.51836E-01 0.01117 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45501E+00 0.00103  3.18357E-01 0.00930 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.20552E-03 0.00918  1.38217E-04 0.05865  9.27931E-04 0.02147  8.01157E-04 0.02407  2.24856E-03 0.01383  8.42438E-04 0.02342  2.47212E-04 0.04572 ];
LAMBDA                    (idx, [1:  14]) = [  7.73314E-01 0.02384  1.25579E-02 0.00107  3.08736E-02 0.00067  1.10855E-01 0.00071  3.22422E-01 0.00047  1.28304E+00 0.00289  8.59416E+00 0.00966 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0005' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:52:32 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:33:57 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299952 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99631E-01  1.00206E+00  9.98418E-01  1.00482E+00  9.95070E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.1E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16021E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88398E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06487E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06917E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67902E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.48158E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.48067E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.32799E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.18518E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000952 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00019E+04 0.00092 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00019E+04 0.00092 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.04691E+02 ;
RUNNING_TIME              (idx, 1)        =  1.01412E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.61967E-01  4.61967E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.13067E-01  1.11500E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.97269E+01  3.20500E+00  2.47270E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  8.98883E-01  2.59167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.09500E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.01412E+02  1.18390E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97666 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00025E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80343E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.57519E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68401E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.22108E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.01253E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.41193E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.56263E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.54278E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  3.69818E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.88927E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.85551E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.94798E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  8.42663E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.99447E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.40098E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.35302E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.51360E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.53717E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.37562E+13 ;
CS137_ACTIVITY            (idx, 1)        =  3.34772E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.34634E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.23746E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.03971E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.31314E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.21086E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 17 ;
BURNUP                     (idx, [1:  2])  = [  2.50000E+01  2.50024E+01 ];
BURN_DAYS                 (idx, 1)        =  6.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.16321E-01 0.00112 ];
U235_FISS                 (idx, [1:   4]) = [  5.00670E+15 0.00121  3.67354E-01 0.00108 ];
U238_FISS                 (idx, [1:   4]) = [  1.15438E+15 0.00285  8.46848E-02 0.00266 ];
PU239_FISS                (idx, [1:   4]) = [  6.05165E+15 0.00111  4.44012E-01 0.00090 ];
PU240_FISS                (idx, [1:   4]) = [  1.34097E+13 0.02426  9.84369E-04 0.02429 ];
PU241_FISS                (idx, [1:   4]) = [  1.37839E+15 0.00234  1.01139E-01 0.00230 ];
U235_CAPT                 (idx, [1:   4]) = [  1.12500E+15 0.00277  3.99393E-02 0.00268 ];
U238_CAPT                 (idx, [1:   4]) = [  9.94782E+15 0.00109  3.53156E-01 0.00078 ];
PU239_CAPT                (idx, [1:   4]) = [  3.33154E+15 0.00157  1.18286E-01 0.00154 ];
PU240_CAPT                (idx, [1:   4]) = [  2.43358E+15 0.00203  8.63932E-02 0.00188 ];
PU241_CAPT                (idx, [1:   4]) = [  4.97045E+14 0.00409  1.76481E-02 0.00409 ];
XE135_CAPT                (idx, [1:   4]) = [  7.76630E+14 0.00323  2.75747E-02 0.00322 ];
SM149_CAPT                (idx, [1:   4]) = [  2.40857E+14 0.00616  8.55223E-03 0.00617 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000952 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.27925E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000952 5.00828E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3339557 3.34454E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1616139 1.61848E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45256 4.52638E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000952 5.00828E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.19095E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.69870E+16 2.2E-05  3.69870E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36314E+16 4.4E-06  1.36314E+16 4.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.82055E+16 0.00048  2.17571E+16 0.00048  6.44838E+15 0.00126 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.18368E+16 0.00032  3.53885E+16 0.00030  6.44838E+15 0.00126 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.21086E+16 0.00051  4.21086E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.96631E+18 0.00051  5.21367E+17 0.00050  1.44494E+18 0.00057 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.81271E+14 0.00493 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.22181E+16 0.00033 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.46785E+18 0.00068 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.09614E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.09614E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.47013E+00 0.00060 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.79207E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.25571E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23688E+00 0.00039 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94092E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96836E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.86298E-01 0.00065 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.78275E-01 0.00066 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.71337E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06071E+02 4.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.78218E-01 0.00066  8.73883E-01 0.00065  4.39202E-03 0.01154 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.77577E-01 0.00033 ];
COL_KEFF                  (idx, [1:   2]) = [  8.78486E-01 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.77577E-01 0.00033 ];
ABS_KINF                  (idx, [1:   2]) = [  8.85586E-01 0.00033 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70070E+01 0.00025 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70010E+01 0.00010 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  8.25939E-07 0.00425 ];
IMP_EALF                  (idx, [1:   2]) = [  8.27763E-07 0.00175 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.88666E-01 0.00288 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.90006E-01 0.00118 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.93011E-03 0.00659  1.44907E-04 0.03897  1.06949E-03 0.01564  9.19852E-04 0.01632  2.51091E-03 0.01008  1.00837E-03 0.01477  2.76577E-04 0.02877 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.66746E-01 0.01431  9.05641E-03 0.02794  3.07423E-02 0.00046  1.11046E-01 0.00056  3.22772E-01 0.00035  1.27275E+00 0.00200  7.64947E+00 0.01717 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.06367E-03 0.00972  1.22173E-04 0.06552  9.24633E-04 0.02354  8.04107E-04 0.02426  2.12525E-03 0.01422  8.45350E-04 0.02318  2.42154E-04 0.04590 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.73897E-01 0.02299  1.25885E-02 0.00125  3.07495E-02 0.00066  1.10944E-01 0.00077  3.22855E-01 0.00053  1.27180E+00 0.00299  8.51203E+00 0.01037 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.85675E-05 0.00151  2.85561E-05 0.00152  3.10749E-05 0.01605 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.50830E-05 0.00135  2.50729E-05 0.00136  2.72891E-05 0.01608 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.98714E-03 0.01150  1.25107E-04 0.07183  8.86721E-04 0.02669  7.83843E-04 0.02900  2.14377E-03 0.01783  8.35510E-04 0.02692  2.12197E-04 0.05549 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.36196E-01 0.02919  1.25649E-02 0.00164  3.07584E-02 0.00087  1.10938E-01 0.00103  3.22792E-01 0.00070  1.27993E+00 0.00386  8.36257E+00 0.01700 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.80011E-05 0.00349  2.79779E-05 0.00351  2.95926E-05 0.04274 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.45861E-05 0.00343  2.45657E-05 0.00345  2.59735E-05 0.04269 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.77215E-03 0.03874  1.46126E-04 0.25999  9.20989E-04 0.08839  7.67078E-04 0.10086  2.01433E-03 0.06104  7.31644E-04 0.09333  1.91982E-04 0.20238 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  6.93863E-01 0.09015  1.25596E-02 0.00397  3.07723E-02 0.00207  1.11115E-01 0.00259  3.23382E-01 0.00190  1.26937E+00 0.01023  8.34034E+00 0.04904 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.76265E-03 0.03789  1.48268E-04 0.23859  9.25632E-04 0.08632  7.63132E-04 0.09707  2.02727E-03 0.06066  7.22218E-04 0.09148  1.76123E-04 0.18890 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  6.81838E-01 0.08862  1.25596E-02 0.00397  3.07738E-02 0.00206  1.11093E-01 0.00258  3.23374E-01 0.00189  1.26925E+00 0.01024  8.35877E+00 0.04865 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.71689E+02 0.03894 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.82903E-05 0.00101 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.48393E-05 0.00072 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.91203E-03 0.00707 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.73746E+02 0.00720 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.66152E-07 0.00079 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.65493E-06 0.00042  2.65467E-06 0.00042  2.70110E-06 0.00577 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.91186E-05 0.00096  3.91383E-05 0.00096  3.54222E-05 0.01133 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.23513E-01 0.00036  6.23883E-01 0.00036  5.73637E-01 0.01016 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06164E+01 0.01482 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.48067E+01 0.00051  3.32195E+01 0.00056 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.04222E+04 0.00411  3.04395E+05 0.00171  6.10944E+05 0.00089  6.50159E+05 0.00071  5.98891E+05 0.00064  6.42338E+05 0.00058  4.34898E+05 0.00069  3.85556E+05 0.00075  2.94645E+05 0.00072  2.40870E+05 0.00103  2.07873E+05 0.00067  1.87533E+05 0.00086  1.72896E+05 0.00089  1.64446E+05 0.00098  1.60308E+05 0.00084  1.38441E+05 0.00079  1.36829E+05 0.00098  1.35782E+05 0.00104  1.33147E+05 0.00087  2.60344E+05 0.00076  2.51460E+05 0.00076  1.81161E+05 0.00076  1.17495E+05 0.00102  1.35610E+05 0.00092  1.28909E+05 0.00126  1.14597E+05 0.00114  1.85578E+05 0.00078  4.29656E+04 0.00140  5.31922E+04 0.00141  4.83338E+04 0.00166  2.83880E+04 0.00180  4.89449E+04 0.00117  3.22414E+04 0.00182  2.63068E+04 0.00154  4.41052E+03 0.00362  3.76734E+03 0.00420  3.29653E+03 0.00379  3.16902E+03 0.00367  3.20309E+03 0.00405  3.53511E+03 0.00278  4.23377E+03 0.00293  4.36661E+03 0.00357  8.65785E+03 0.00325  1.44542E+04 0.00206  1.88426E+04 0.00231  5.03545E+04 0.00112  5.30952E+04 0.00133  5.67217E+04 0.00122  3.66773E+04 0.00141  2.57696E+04 0.00175  1.91232E+04 0.00182  2.25534E+04 0.00172  4.40960E+04 0.00142  6.28034E+04 0.00134  1.25669E+05 0.00143  1.99642E+05 0.00161  3.03958E+05 0.00171  1.96429E+05 0.00162  1.41510E+05 0.00159  1.02029E+05 0.00187  9.20090E+04 0.00174  9.08794E+04 0.00163  7.60653E+04 0.00164  5.15859E+04 0.00193  4.77244E+04 0.00203  4.25284E+04 0.00187  3.59676E+04 0.00220  2.84740E+04 0.00208  1.90823E+04 0.00193  6.76695E+03 0.00222 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.86512E-01 0.00047 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.57027E+18 0.00047  3.96064E+17 0.00139 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39350E-01 0.00011  1.56775E+00 0.00041 ];
INF_CAPT                  (idx, [1:   4]) = [  8.35078E-03 0.00072  3.81100E-02 0.00084 ];
INF_ABS                   (idx, [1:   4]) = [  1.00110E-02 0.00059  6.59505E-02 0.00107 ];
INF_FISS                  (idx, [1:   4]) = [  1.66021E-03 0.00054  2.78405E-02 0.00141 ];
INF_NSF                   (idx, [1:   4]) = [  4.51545E-03 0.00055  7.54995E-02 0.00145 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.71981E+00 5.9E-05  2.71185E+00 5.6E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05961E+02 8.7E-06  2.06097E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.53701E-08 0.00029  2.59140E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29356E-01 0.00011  1.50184E+00 0.00047 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44122E-01 0.00014  3.97901E-01 0.00048 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63314E-02 0.00024  9.36576E-02 0.00102 ];
INF_SCATT3                (idx, [1:   4]) = [  7.24725E-03 0.00300  2.80870E-02 0.00238 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03601E-02 0.00242 -8.94228E-03 0.00609 ];
INF_SCATT5                (idx, [1:   4]) = [  1.37034E-04 0.14160  6.71867E-03 0.01010 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10776E-03 0.00337 -1.70966E-02 0.00301 ];
INF_SCATT7                (idx, [1:   4]) = [  7.40334E-04 0.02455  5.30978E-04 0.09738 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29400E-01 0.00011  1.50184E+00 0.00047 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44123E-01 0.00014  3.97901E-01 0.00048 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63313E-02 0.00024  9.36576E-02 0.00102 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.24757E-03 0.00300  2.80870E-02 0.00238 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03601E-02 0.00242 -8.94228E-03 0.00609 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.37206E-04 0.14136  6.71867E-03 0.01010 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10777E-03 0.00337 -1.70966E-02 0.00301 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.40300E-04 0.02459  5.30978E-04 0.09738 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12485E-01 0.00034  1.01884E+00 0.00038 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56874E+00 0.00034  3.27171E-01 0.00038 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.96658E-03 0.00059  6.59505E-02 0.00107 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69911E-02 0.00024  6.70553E-02 0.00127 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12359E-01 0.00011  1.69968E-02 0.00047  1.14553E-03 0.00527  1.50069E+00 0.00048 ];
INF_S1                    (idx, [1:   8]) = [  2.39207E-01 0.00014  4.91493E-03 0.00086  4.92883E-04 0.00779  3.97408E-01 0.00049 ];
INF_S2                    (idx, [1:   8]) = [  9.78824E-02 0.00023 -1.55098E-03 0.00240  2.65740E-04 0.01079  9.33919E-02 0.00102 ];
INF_S3                    (idx, [1:   8]) = [  9.00880E-03 0.00229 -1.76156E-03 0.00193  9.49841E-05 0.02100  2.79920E-02 0.00241 ];
INF_S4                    (idx, [1:   8]) = [ -9.81571E-03 0.00248 -5.44371E-04 0.00707 -3.14936E-07 1.00000 -8.94197E-03 0.00611 ];
INF_S5                    (idx, [1:   8]) = [  9.19478E-05 0.20852  4.50860E-05 0.06135 -3.85288E-05 0.04486  6.75720E-03 0.01008 ];
INF_S6                    (idx, [1:   8]) = [  5.23757E-03 0.00328 -1.29809E-04 0.01832 -5.02973E-05 0.02788 -1.70463E-02 0.00304 ];
INF_S7                    (idx, [1:   8]) = [  9.07950E-04 0.01985 -1.67616E-04 0.01439 -4.46207E-05 0.02911  5.75599E-04 0.09023 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12404E-01 0.00011  1.69968E-02 0.00047  1.14553E-03 0.00527  1.50069E+00 0.00048 ];
INF_SP1                   (idx, [1:   8]) = [  2.39208E-01 0.00014  4.91493E-03 0.00086  4.92883E-04 0.00779  3.97408E-01 0.00049 ];
INF_SP2                   (idx, [1:   8]) = [  9.78823E-02 0.00023 -1.55098E-03 0.00240  2.65740E-04 0.01079  9.33919E-02 0.00102 ];
INF_SP3                   (idx, [1:   8]) = [  9.00913E-03 0.00229 -1.76156E-03 0.00193  9.49841E-05 0.02100  2.79920E-02 0.00241 ];
INF_SP4                   (idx, [1:   8]) = [ -9.81576E-03 0.00249 -5.44371E-04 0.00707 -3.14936E-07 1.00000 -8.94197E-03 0.00611 ];
INF_SP5                   (idx, [1:   8]) = [  9.21199E-05 0.20805  4.50860E-05 0.06135 -3.85288E-05 0.04486  6.75720E-03 0.01008 ];
INF_SP6                   (idx, [1:   8]) = [  5.23758E-03 0.00329 -1.29809E-04 0.01832 -5.02973E-05 0.02788 -1.70463E-02 0.00304 ];
INF_SP7                   (idx, [1:   8]) = [  9.07916E-04 0.01987 -1.67616E-04 0.01439 -4.46207E-05 0.02911  5.75599E-04 0.09023 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31900E-01 0.00067  1.19636E+00 0.00749 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33329E-01 0.00084  1.30064E+00 0.00904 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33896E-01 0.00094  1.29902E+00 0.00879 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28556E-01 0.00083  1.03327E+00 0.00814 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43742E+00 0.00067  2.78999E-01 0.00748 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42862E+00 0.00084  2.56793E-01 0.00915 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42517E+00 0.00094  2.57085E-01 0.00888 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45846E+00 0.00084  3.23118E-01 0.00819 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.06367E-03 0.00972  1.22173E-04 0.06552  9.24633E-04 0.02354  8.04107E-04 0.02426  2.12525E-03 0.01422  8.45350E-04 0.02318  2.42154E-04 0.04590 ];
LAMBDA                    (idx, [1:  14]) = [  7.73897E-01 0.02299  1.25885E-02 0.00125  3.07495E-02 0.00066  1.10944E-01 0.00077  3.22855E-01 0.00053  1.27180E+00 0.00299  8.51203E+00 0.01037 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0005' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:52:32 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:39:38 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299952 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.83496E-01  1.00542E+00  1.00222E+00  1.00610E+00  1.00276E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16385E-02 0.00106  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88361E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06332E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06762E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68158E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.47781E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.47690E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.32718E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.19847E-01 0.00108  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001218 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00024E+04 0.00098 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00024E+04 0.00098 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.33051E+02 ;
RUNNING_TIME              (idx, 1)        =  1.07092E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.61967E-01  4.61967E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.34650E-01  1.06333E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.05330E+02  3.12160E+00  2.48145E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  9.53667E-01  2.66333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.34167E-02  8.50002E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.07092E+02  1.18613E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97752 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99734E+00 0.00030 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80551E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.69512E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68061E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.30663E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.11283E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.48715E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.58226E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.53186E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  4.91217E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.04537E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.02014E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.59230E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  8.92034E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.08614E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.75804E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.38677E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.55550E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.58285E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.03899E+13 ;
CS137_ACTIVITY            (idx, 1)        =  4.00617E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.45580E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.20693E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.65084E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.34208E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.41796E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 18 ;
BURNUP                     (idx, [1:  2])  = [  3.00000E+01  3.00029E+01 ];
BURN_DAYS                 (idx, 1)        =  7.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.60641E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  4.15709E+15 0.00143  3.05613E-01 0.00122 ];
U238_FISS                 (idx, [1:   4]) = [  1.21330E+15 0.00272  8.91836E-02 0.00250 ];
PU239_FISS                (idx, [1:   4]) = [  6.48125E+15 0.00109  4.76491E-01 0.00087 ];
PU240_FISS                (idx, [1:   4]) = [  1.54407E+13 0.02516  1.13526E-03 0.02516 ];
PU241_FISS                (idx, [1:   4]) = [  1.70256E+15 0.00245  1.25168E-01 0.00235 ];
U235_CAPT                 (idx, [1:   4]) = [  9.32503E+14 0.00309  3.08325E-02 0.00306 ];
U238_CAPT                 (idx, [1:   4]) = [  1.04014E+16 0.00106  3.43888E-01 0.00078 ];
PU239_CAPT                (idx, [1:   4]) = [  3.58057E+15 0.00154  1.18389E-01 0.00147 ];
PU240_CAPT                (idx, [1:   4]) = [  2.79044E+15 0.00184  9.22578E-02 0.00171 ];
PU241_CAPT                (idx, [1:   4]) = [  6.09733E+14 0.00368  2.01609E-02 0.00366 ];
XE135_CAPT                (idx, [1:   4]) = [  7.89955E+14 0.00313  2.61218E-02 0.00315 ];
SM149_CAPT                (idx, [1:   4]) = [  2.52488E+14 0.00568  8.34822E-03 0.00566 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001218 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.17660E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001218 5.00818E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3418120 3.42299E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1537429 1.53951E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45669 4.56730E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001218 5.00818E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.12227E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.72988E+16 2.2E-05  3.72988E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36036E+16 4.7E-06  1.36036E+16 4.7E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.02213E+16 0.00046  2.34678E+16 0.00047  6.75340E+15 0.00128 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.38248E+16 0.00032  3.70714E+16 0.00030  6.75340E+15 0.00128 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.41796E+16 0.00052  4.41796E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.06020E+18 0.00052  5.45886E+17 0.00050  1.51431E+18 0.00058 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.03560E+14 0.00482 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.42284E+16 0.00032 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.53835E+18 0.00069 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.09037E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.09037E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.43202E+00 0.00064 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.76758E-01 0.00036 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.19598E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23627E+00 0.00038 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94022E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96824E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.51964E-01 0.00068 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.44182E-01 0.00068 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.74184E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06492E+02 4.7E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.44026E-01 0.00069  8.40058E-01 0.00069  4.12380E-03 0.01176 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.44752E-01 0.00033 ];
COL_KEFF                  (idx, [1:   2]) = [  8.44366E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.44752E-01 0.00033 ];
ABS_KINF                  (idx, [1:   2]) = [  8.52542E-01 0.00032 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.69366E+01 0.00025 ];
IMP_ALF                   (idx, [1:   2]) = [  1.69369E+01 0.00010 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  8.86297E-07 0.00429 ];
IMP_EALF                  (idx, [1:   2]) = [  8.82597E-07 0.00174 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.03747E-01 0.00273 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.03496E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.98542E-03 0.00672  1.51485E-04 0.03935  1.11283E-03 0.01415  9.33985E-04 0.01602  2.54449E-03 0.01023  9.52974E-04 0.01605  2.89653E-04 0.02770 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.58873E-01 0.01498  8.88278E-03 0.02905  3.06048E-02 0.00042  1.11296E-01 0.00061  3.22950E-01 0.00040  1.25383E+00 0.00238  7.55294E+00 0.01699 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.88944E-03 0.01010  1.14014E-04 0.06294  9.01224E-04 0.02341  7.44286E-04 0.02433  2.10428E-03 0.01462  7.95935E-04 0.02548  2.29695E-04 0.04181 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.52352E-01 0.02291  1.26149E-02 0.00137  3.05929E-02 0.00061  1.11376E-01 0.00089  3.22911E-01 0.00059  1.25030E+00 0.00348  8.12682E+00 0.01283 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.98121E-05 0.00163  2.97959E-05 0.00162  3.32274E-05 0.02072 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.51556E-05 0.00144  2.51419E-05 0.00144  2.80199E-05 0.02052 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.88784E-03 0.01189  1.16460E-04 0.07291  9.10149E-04 0.02527  7.62302E-04 0.02989  2.09766E-03 0.01731  7.67675E-04 0.03082  2.33595E-04 0.05287 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.38935E-01 0.02867  1.26215E-02 0.00223  3.06250E-02 0.00087  1.11506E-01 0.00121  3.22812E-01 0.00078  1.25173E+00 0.00499  8.10610E+00 0.01905 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.91991E-05 0.00374  2.91771E-05 0.00373  2.98706E-05 0.05311 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.46397E-05 0.00370  2.46213E-05 0.00369  2.51887E-05 0.05300 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.72609E-03 0.04069  1.11712E-04 0.26581  9.20612E-04 0.08704  7.80768E-04 0.09781  1.93766E-03 0.06262  7.69281E-04 0.10224  2.06054E-04 0.17280 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.12742E-01 0.09004  1.26326E-02 0.00632  3.04958E-02 0.00195  1.11221E-01 0.00258  3.22525E-01 0.00199  1.25995E+00 0.01116  7.79040E+00 0.05158 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.76573E-03 0.04001  1.16257E-04 0.27174  9.28536E-04 0.08517  7.76643E-04 0.09560  1.97807E-03 0.06081  7.70990E-04 0.10408  1.95235E-04 0.16694 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.11877E-01 0.09094  1.26326E-02 0.00632  3.04939E-02 0.00195  1.11225E-01 0.00258  3.22615E-01 0.00199  1.25981E+00 0.01110  7.80942E+00 0.05132 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.62466E+02 0.04074 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.94729E-05 0.00103 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.48696E-05 0.00072 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.91281E-03 0.00787 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.66720E+02 0.00786 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.65487E-07 0.00082 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.63147E-06 0.00043  2.63136E-06 0.00043  2.65157E-06 0.00620 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.94152E-05 0.00097  3.94370E-05 0.00097  3.52509E-05 0.01167 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.17584E-01 0.00036  6.18092E-01 0.00036  5.47435E-01 0.01166 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.11190E+01 0.01435 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.47690E+01 0.00053  3.31433E+01 0.00056 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.09134E+04 0.00279  3.05093E+05 0.00125  6.10929E+05 0.00071  6.50304E+05 0.00096  5.98100E+05 0.00075  6.40924E+05 0.00071  4.35614E+05 0.00063  3.85341E+05 0.00072  2.95146E+05 0.00074  2.41100E+05 0.00091  2.08257E+05 0.00081  1.87217E+05 0.00093  1.73086E+05 0.00093  1.64557E+05 0.00085  1.60414E+05 0.00083  1.38564E+05 0.00082  1.36859E+05 0.00077  1.35616E+05 0.00092  1.33332E+05 0.00100  2.59712E+05 0.00083  2.51590E+05 0.00073  1.81247E+05 0.00089  1.17719E+05 0.00106  1.35216E+05 0.00073  1.28899E+05 0.00086  1.14318E+05 0.00094  1.84413E+05 0.00085  4.28244E+04 0.00200  5.27528E+04 0.00137  4.81711E+04 0.00201  2.81948E+04 0.00182  4.86336E+04 0.00115  3.19889E+04 0.00132  2.57144E+04 0.00166  4.29567E+03 0.00404  3.61098E+03 0.00324  3.17383E+03 0.00332  3.04683E+03 0.00366  3.09616E+03 0.00389  3.39026E+03 0.00382  4.11804E+03 0.00403  4.26045E+03 0.00345  8.53519E+03 0.00273  1.41760E+04 0.00220  1.85237E+04 0.00206  4.96209E+04 0.00136  5.22860E+04 0.00131  5.59878E+04 0.00085  3.62954E+04 0.00133  2.55661E+04 0.00180  1.88702E+04 0.00161  2.22803E+04 0.00195  4.36892E+04 0.00162  6.21141E+04 0.00159  1.24736E+05 0.00183  1.98388E+05 0.00182  3.02805E+05 0.00201  1.95982E+05 0.00192  1.41445E+05 0.00211  1.02044E+05 0.00202  9.20469E+04 0.00230  9.08791E+04 0.00218  7.61305E+04 0.00221  5.16670E+04 0.00222  4.77595E+04 0.00213  4.26170E+04 0.00220  3.60404E+04 0.00209  2.84957E+04 0.00191  1.91057E+04 0.00288  6.76731E+03 0.00310 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.52150E-01 0.00061 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.64627E+18 0.00062  4.13962E+17 0.00176 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39763E-01 0.00014  1.56965E+00 0.00051 ];
INF_CAPT                  (idx, [1:   4]) = [  8.58308E-03 0.00083  3.88762E-02 0.00101 ];
INF_ABS                   (idx, [1:   4]) = [  1.01629E-02 0.00070  6.54617E-02 0.00133 ];
INF_FISS                  (idx, [1:   4]) = [  1.57978E-03 0.00044  2.65855E-02 0.00181 ];
INF_NSF                   (idx, [1:   4]) = [  4.33350E-03 0.00044  7.28852E-02 0.00185 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.74311E+00 6.6E-05  2.74153E+00 5.7E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06291E+02 7.8E-06  2.06540E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.49455E-08 0.00047  2.59601E-06 0.00021 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29603E-01 0.00015  1.50412E+00 0.00059 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44139E-01 0.00027  3.98072E-01 0.00064 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63941E-02 0.00040  9.37060E-02 0.00099 ];
INF_SCATT3                (idx, [1:   4]) = [  7.28695E-03 0.00342  2.80713E-02 0.00211 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03687E-02 0.00157 -9.04992E-03 0.00616 ];
INF_SCATT5                (idx, [1:   4]) = [  1.62665E-04 0.10863  6.68899E-03 0.00839 ];
INF_SCATT6                (idx, [1:   4]) = [  5.13070E-03 0.00322 -1.73461E-02 0.00254 ];
INF_SCATT7                (idx, [1:   4]) = [  7.86333E-04 0.02107  4.74924E-04 0.08207 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29647E-01 0.00015  1.50412E+00 0.00059 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44140E-01 0.00027  3.98072E-01 0.00064 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63945E-02 0.00040  9.37060E-02 0.00099 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.28745E-03 0.00341  2.80713E-02 0.00211 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03686E-02 0.00158 -9.04992E-03 0.00616 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.62691E-04 0.10854  6.68899E-03 0.00839 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.13071E-03 0.00323 -1.73461E-02 0.00254 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.86299E-04 0.02102  4.74924E-04 0.08207 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12594E-01 0.00031  1.02124E+00 0.00045 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56793E+00 0.00031  3.26401E-01 0.00045 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.01190E-02 0.00071  6.54617E-02 0.00133 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70036E-02 0.00020  6.66546E-02 0.00150 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12759E-01 0.00015  1.68444E-02 0.00044  1.12899E-03 0.00299  1.50299E+00 0.00059 ];
INF_S1                    (idx, [1:   8]) = [  2.39276E-01 0.00027  4.86320E-03 0.00090  4.85770E-04 0.00814  3.97586E-01 0.00064 ];
INF_S2                    (idx, [1:   8]) = [  9.79343E-02 0.00041 -1.54022E-03 0.00308  2.67882E-04 0.01196  9.34381E-02 0.00098 ];
INF_S3                    (idx, [1:   8]) = [  9.03250E-03 0.00266 -1.74555E-03 0.00236  9.76224E-05 0.02345  2.79737E-02 0.00211 ];
INF_S4                    (idx, [1:   8]) = [ -9.83325E-03 0.00156 -5.35419E-04 0.00534  7.66554E-07 1.00000 -9.05068E-03 0.00613 ];
INF_S5                    (idx, [1:   8]) = [  1.19608E-04 0.14636  4.30565E-05 0.05719 -3.80226E-05 0.03942  6.72701E-03 0.00829 ];
INF_S6                    (idx, [1:   8]) = [  5.26507E-03 0.00314 -1.34368E-04 0.01941 -4.81232E-05 0.03102 -1.72979E-02 0.00253 ];
INF_S7                    (idx, [1:   8]) = [  9.53935E-04 0.01719 -1.67601E-04 0.01083 -4.53030E-05 0.03493  5.20227E-04 0.07531 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12803E-01 0.00015  1.68444E-02 0.00044  1.12899E-03 0.00299  1.50299E+00 0.00059 ];
INF_SP1                   (idx, [1:   8]) = [  2.39277E-01 0.00027  4.86320E-03 0.00090  4.85770E-04 0.00814  3.97586E-01 0.00064 ];
INF_SP2                   (idx, [1:   8]) = [  9.79347E-02 0.00041 -1.54022E-03 0.00308  2.67882E-04 0.01196  9.34381E-02 0.00098 ];
INF_SP3                   (idx, [1:   8]) = [  9.03300E-03 0.00266 -1.74555E-03 0.00236  9.76224E-05 0.02345  2.79737E-02 0.00211 ];
INF_SP4                   (idx, [1:   8]) = [ -9.83317E-03 0.00156 -5.35419E-04 0.00534  7.66554E-07 1.00000 -9.05068E-03 0.00613 ];
INF_SP5                   (idx, [1:   8]) = [  1.19635E-04 0.14615  4.30565E-05 0.05719 -3.80226E-05 0.03942  6.72701E-03 0.00829 ];
INF_SP6                   (idx, [1:   8]) = [  5.26508E-03 0.00314 -1.34368E-04 0.01941 -4.81232E-05 0.03102 -1.72979E-02 0.00253 ];
INF_SP7                   (idx, [1:   8]) = [  9.53900E-04 0.01715 -1.67601E-04 0.01083 -4.53030E-05 0.03493  5.20227E-04 0.07531 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32264E-01 0.00055  1.23687E+00 0.00733 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33885E-01 0.00100  1.35743E+00 0.00915 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34242E-01 0.00075  1.34176E+00 0.00854 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28752E-01 0.00066  1.06112E+00 0.00795 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43516E+00 0.00055  2.69837E-01 0.00717 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42523E+00 0.00101  2.46044E-01 0.00894 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42305E+00 0.00075  2.48867E-01 0.00857 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45720E+00 0.00066  3.14599E-01 0.00778 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.88944E-03 0.01010  1.14014E-04 0.06294  9.01224E-04 0.02341  7.44286E-04 0.02433  2.10428E-03 0.01462  7.95935E-04 0.02548  2.29695E-04 0.04181 ];
LAMBDA                    (idx, [1:  14]) = [  7.52352E-01 0.02291  1.26149E-02 0.00137  3.05929E-02 0.00061  1.11376E-01 0.00089  3.22911E-01 0.00059  1.25030E+00 0.00348  8.12682E+00 0.01283 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0005' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:52:32 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:45:17 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299952 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.97524E-01  1.00329E+00  9.99084E-01  1.00168E+00  9.98423E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.3E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.17229E-02 0.00114  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88277E-01 1.4E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05118E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05552E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68628E+00 0.00024  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.46181E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.46092E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.35337E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.21590E-01 0.00116  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001348 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00027E+04 0.00099 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00027E+04 0.00099 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.61280E+02 ;
RUNNING_TIME              (idx, 1)        =  1.12744E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.61967E-01  4.61967E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.56400E-01  1.08333E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.10907E+02  3.08903E+00  2.48827E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.00605E+00  2.58667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.51500E-02  8.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.12744E+02  1.18427E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97837 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00041E+00 0.00021 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80766E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.82972E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68377E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.90306E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.21526E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.56491E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61443E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.52723E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  6.35547E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.20847E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.41967E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.02870E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  9.35792E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.17977E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.08637E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.42582E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.59810E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.63281E+14 ;
CS134_ACTIVITY            (idx, 1)        =  7.87403E+13 ;
CS137_ACTIVITY            (idx, 1)        =  4.66051E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.57777E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.18246E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.35653E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.37237E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.60629E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 19 ;
BURNUP                     (idx, [1:  2])  = [  3.50000E+01  3.50034E+01 ];
BURN_DAYS                 (idx, 1)        =  8.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  8.01643E-01 0.00104 ];
U235_FISS                 (idx, [1:   4]) = [  3.38888E+15 0.00169  2.49229E-01 0.00155 ];
U238_FISS                 (idx, [1:   4]) = [  1.26186E+15 0.00269  9.27891E-02 0.00250 ];
PU239_FISS                (idx, [1:   4]) = [  6.89113E+15 0.00110  5.06789E-01 0.00084 ];
PU240_FISS                (idx, [1:   4]) = [  1.75646E+13 0.02200  1.29122E-03 0.02196 ];
PU241_FISS                (idx, [1:   4]) = [  1.99429E+15 0.00223  1.46666E-01 0.00212 ];
U235_CAPT                 (idx, [1:   4]) = [  7.63596E+14 0.00345  2.37641E-02 0.00343 ];
U238_CAPT                 (idx, [1:   4]) = [  1.08220E+16 0.00101  3.36770E-01 0.00076 ];
PU239_CAPT                (idx, [1:   4]) = [  3.78048E+15 0.00150  1.17654E-01 0.00145 ];
PU240_CAPT                (idx, [1:   4]) = [  3.12347E+15 0.00168  9.71992E-02 0.00155 ];
PU241_CAPT                (idx, [1:   4]) = [  7.16665E+14 0.00376  2.23025E-02 0.00372 ];
XE135_CAPT                (idx, [1:   4]) = [  7.99002E+14 0.00335  2.48661E-02 0.00333 ];
SM149_CAPT                (idx, [1:   4]) = [  2.59309E+14 0.00568  8.06980E-03 0.00565 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001348 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.48207E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001348 5.00848E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3482930 3.48806E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1474050 1.47606E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44368 4.43638E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001348 5.00848E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -8.19564E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.75841E+16 2.2E-05  3.75841E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35783E+16 4.6E-06  1.35783E+16 4.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.21269E+16 0.00044  2.51626E+16 0.00046  6.96431E+15 0.00130 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.57052E+16 0.00031  3.87409E+16 0.00030  6.96431E+15 0.00130 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.60629E+16 0.00051  4.60629E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.14429E+18 0.00051  5.69961E+17 0.00048  1.57433E+18 0.00058 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.08801E+14 0.00539 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.61140E+16 0.00032 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.59667E+18 0.00068 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.08460E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.08460E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.39915E+00 0.00069 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.77690E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.13150E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23583E+00 0.00039 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94231E-01 3.8E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96878E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.24423E-01 0.00069 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.17109E-01 0.00070 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.76795E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06876E+02 4.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.17095E-01 0.00071  8.13294E-01 0.00070  3.81470E-03 0.01187 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.16434E-01 0.00032 ];
COL_KEFF                  (idx, [1:   2]) = [  8.16036E-01 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.16434E-01 0.00032 ];
ABS_KINF                  (idx, [1:   2]) = [  8.23746E-01 0.00031 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.68784E+01 0.00026 ];
IMP_ALF                   (idx, [1:   2]) = [  1.68769E+01 0.00010 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  9.39457E-07 0.00433 ];
IMP_EALF                  (idx, [1:   2]) = [  9.37112E-07 0.00172 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.16384E-01 0.00288 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.16646E-01 0.00111 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.03140E-03 0.00673  1.53020E-04 0.03761  1.12603E-03 0.01539  9.28786E-04 0.01589  2.53282E-03 0.01057  9.95669E-04 0.01520  2.95066E-04 0.03146 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.48506E-01 0.01616  9.37434E-03 0.02643  3.04985E-02 0.00042  1.11504E-01 0.00062  3.23180E-01 0.00039  1.23392E+00 0.00290  7.25984E+00 0.01856 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.70203E-03 0.01015  1.11549E-04 0.06370  8.77880E-04 0.02322  7.22116E-04 0.02500  2.00953E-03 0.01574  7.56837E-04 0.02470  2.24115E-04 0.04840 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.31321E-01 0.02453  1.26346E-02 0.00143  3.05012E-02 0.00061  1.11556E-01 0.00094  3.23271E-01 0.00061  1.23704E+00 0.00400  7.97435E+00 0.01402 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.09332E-05 0.00154  3.09229E-05 0.00155  3.26876E-05 0.01942 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.52697E-05 0.00141  2.52614E-05 0.00142  2.66878E-05 0.01932 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.66015E-03 0.01199  1.08112E-04 0.08021  8.81264E-04 0.02828  6.86073E-04 0.03163  1.98680E-03 0.01758  7.74108E-04 0.03092  2.23790E-04 0.05651 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.38056E-01 0.03055  1.25934E-02 0.00219  3.05459E-02 0.00090  1.11724E-01 0.00130  3.22909E-01 0.00078  1.23093E+00 0.00567  7.86730E+00 0.02212 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.02528E-05 0.00402  3.02401E-05 0.00403  2.77107E-05 0.05354 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.47108E-05 0.00389  2.47006E-05 0.00390  2.26138E-05 0.05339 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.20650E-03 0.04251  8.39776E-05 0.32339  6.44309E-04 0.09210  8.09618E-04 0.11043  1.78484E-03 0.06724  6.53891E-04 0.10276  2.29860E-04 0.21668 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.55928E-01 0.10076  1.26459E-02 0.00853  3.05268E-02 0.00215  1.11652E-01 0.00297  3.22151E-01 0.00213  1.23309E+00 0.01362  7.01247E+00 0.06951 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.18480E-03 0.04155  7.90210E-05 0.31892  6.43598E-04 0.09032  8.06868E-04 0.10842  1.77405E-03 0.06554  6.65203E-04 0.10008  2.16063E-04 0.20241 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.51297E-01 0.10041  1.26459E-02 0.00853  3.05252E-02 0.00214  1.11635E-01 0.00296  3.22062E-01 0.00212  1.23189E+00 0.01366  6.96039E+00 0.06994 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.40614E+02 0.04317 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.05976E-05 0.00101 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.49952E-05 0.00075 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.47952E-03 0.00851 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.46553E+02 0.00872 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.61525E-07 0.00082 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.61424E-06 0.00046  2.61403E-06 0.00046  2.65294E-06 0.00608 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.94598E-05 0.00097  3.94813E-05 0.00098  3.51508E-05 0.01192 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.11293E-01 0.00036  6.11939E-01 0.00036  5.17402E-01 0.01135 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.11169E+01 0.01513 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.46092E+01 0.00051  3.30947E+01 0.00057 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.17853E+04 0.00295  3.06399E+05 0.00137  6.09919E+05 0.00089  6.49406E+05 0.00083  5.99178E+05 0.00063  6.41741E+05 0.00080  4.36062E+05 0.00056  3.85582E+05 0.00071  2.95261E+05 0.00077  2.41553E+05 0.00083  2.08267E+05 0.00110  1.87473E+05 0.00082  1.73038E+05 0.00096  1.64717E+05 0.00071  1.60632E+05 0.00067  1.38761E+05 0.00090  1.36854E+05 0.00091  1.35923E+05 0.00086  1.33366E+05 0.00120  2.60342E+05 0.00058  2.51695E+05 0.00090  1.81579E+05 0.00070  1.17774E+05 0.00093  1.35433E+05 0.00067  1.29121E+05 0.00109  1.14433E+05 0.00089  1.83866E+05 0.00067  4.26454E+04 0.00176  5.23911E+04 0.00146  4.78342E+04 0.00127  2.80718E+04 0.00198  4.83499E+04 0.00143  3.14583E+04 0.00176  2.53419E+04 0.00212  4.18942E+03 0.00313  3.52668E+03 0.00351  3.08103E+03 0.00428  2.97930E+03 0.00337  3.02500E+03 0.00488  3.28388E+03 0.00474  3.96274E+03 0.00376  4.11209E+03 0.00317  8.37140E+03 0.00230  1.39355E+04 0.00200  1.83547E+04 0.00234  4.92266E+04 0.00141  5.17664E+04 0.00153  5.54043E+04 0.00117  3.59438E+04 0.00163  2.51776E+04 0.00187  1.85859E+04 0.00211  2.19701E+04 0.00171  4.30912E+04 0.00177  6.14071E+04 0.00174  1.23205E+05 0.00167  1.96296E+05 0.00166  3.00001E+05 0.00185  1.94442E+05 0.00191  1.40147E+05 0.00202  1.01264E+05 0.00196  9.12940E+04 0.00209  9.01018E+04 0.00206  7.53765E+04 0.00198  5.12014E+04 0.00232  4.73920E+04 0.00207  4.22450E+04 0.00236  3.58008E+04 0.00209  2.82963E+04 0.00214  1.89647E+04 0.00264  6.74445E+03 0.00287 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.23341E-01 0.00058 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.71683E+18 0.00055  4.27492E+17 0.00178 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39946E-01 0.00013  1.56653E+00 0.00056 ];
INF_CAPT                  (idx, [1:   4]) = [  8.81798E-03 0.00062  3.97434E-02 0.00103 ];
INF_ABS                   (idx, [1:   4]) = [  1.03297E-02 0.00053  6.54406E-02 0.00133 ];
INF_FISS                  (idx, [1:   4]) = [  1.51175E-03 0.00046  2.56972E-02 0.00179 ];
INF_NSF                   (idx, [1:   4]) = [  4.17992E-03 0.00048  7.11471E-02 0.00184 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.76495E+00 7.7E-05  2.76866E+00 6.4E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06590E+02 9.1E-06  2.06944E+02 1.3E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.45870E-08 0.00046  2.59757E-06 0.00021 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29614E-01 0.00014  1.50104E+00 0.00064 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43907E-01 0.00019  3.97033E-01 0.00077 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62501E-02 0.00023  9.35969E-02 0.00088 ];
INF_SCATT3                (idx, [1:   4]) = [  7.22018E-03 0.00258  2.81839E-02 0.00253 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03718E-02 0.00182 -9.01588E-03 0.00723 ];
INF_SCATT5                (idx, [1:   4]) = [  1.65358E-04 0.12442  6.71797E-03 0.00778 ];
INF_SCATT6                (idx, [1:   4]) = [  5.14079E-03 0.00311 -1.72967E-02 0.00299 ];
INF_SCATT7                (idx, [1:   4]) = [  7.68657E-04 0.02099  4.65975E-04 0.10157 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29659E-01 0.00014  1.50104E+00 0.00064 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43908E-01 0.00019  3.97033E-01 0.00077 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62508E-02 0.00023  9.35969E-02 0.00088 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.22013E-03 0.00259  2.81839E-02 0.00253 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03718E-02 0.00182 -9.01588E-03 0.00723 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.65365E-04 0.12445  6.71797E-03 0.00778 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.14058E-03 0.00311 -1.72967E-02 0.00299 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.68556E-04 0.02099  4.65975E-04 0.10157 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12805E-01 0.00038  1.02025E+00 0.00048 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56638E+00 0.00037  3.26719E-01 0.00048 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.02842E-02 0.00053  6.54406E-02 0.00133 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70029E-02 0.00022  6.66178E-02 0.00150 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12944E-01 0.00013  1.66702E-02 0.00045  1.13243E-03 0.00473  1.49991E+00 0.00065 ];
INF_S1                    (idx, [1:   8]) = [  2.39103E-01 0.00019  4.80329E-03 0.00133  4.91594E-04 0.00845  3.96542E-01 0.00078 ];
INF_S2                    (idx, [1:   8]) = [  9.77856E-02 0.00023 -1.53541E-03 0.00335  2.65358E-04 0.01165  9.33315E-02 0.00088 ];
INF_S3                    (idx, [1:   8]) = [  8.95175E-03 0.00201 -1.73157E-03 0.00194  9.44803E-05 0.02617  2.80894E-02 0.00253 ];
INF_S4                    (idx, [1:   8]) = [ -9.84780E-03 0.00185 -5.23971E-04 0.00483  7.21572E-07 1.00000 -9.01660E-03 0.00721 ];
INF_S5                    (idx, [1:   8]) = [  1.18621E-04 0.17856  4.67367E-05 0.04514 -3.63926E-05 0.04082  6.75436E-03 0.00776 ];
INF_S6                    (idx, [1:   8]) = [  5.27001E-03 0.00305 -1.29213E-04 0.01619 -4.89764E-05 0.03100 -1.72478E-02 0.00302 ];
INF_S7                    (idx, [1:   8]) = [  9.34218E-04 0.01650 -1.65561E-04 0.01557 -4.74044E-05 0.03024  5.13380E-04 0.09247 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12989E-01 0.00013  1.66702E-02 0.00045  1.13243E-03 0.00473  1.49991E+00 0.00065 ];
INF_SP1                   (idx, [1:   8]) = [  2.39104E-01 0.00019  4.80329E-03 0.00133  4.91594E-04 0.00845  3.96542E-01 0.00078 ];
INF_SP2                   (idx, [1:   8]) = [  9.77862E-02 0.00023 -1.53541E-03 0.00335  2.65358E-04 0.01165  9.33315E-02 0.00088 ];
INF_SP3                   (idx, [1:   8]) = [  8.95170E-03 0.00202 -1.73157E-03 0.00194  9.44803E-05 0.02617  2.80894E-02 0.00253 ];
INF_SP4                   (idx, [1:   8]) = [ -9.84779E-03 0.00185 -5.23971E-04 0.00483  7.21572E-07 1.00000 -9.01660E-03 0.00721 ];
INF_SP5                   (idx, [1:   8]) = [  1.18628E-04 0.17859  4.67367E-05 0.04514 -3.63926E-05 0.04082  6.75436E-03 0.00776 ];
INF_SP6                   (idx, [1:   8]) = [  5.26979E-03 0.00305 -1.29213E-04 0.01619 -4.89764E-05 0.03100 -1.72478E-02 0.00302 ];
INF_SP7                   (idx, [1:   8]) = [  9.34117E-04 0.01649 -1.65561E-04 0.01557 -4.74044E-05 0.03024  5.13380E-04 0.09247 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32054E-01 0.00042  1.22837E+00 0.00786 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33383E-01 0.00066  1.34634E+00 0.01020 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33926E-01 0.00090  1.33286E+00 0.00984 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28926E-01 0.00078  1.05492E+00 0.00766 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43645E+00 0.00042  2.71762E-01 0.00780 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42828E+00 0.00066  2.48205E-01 0.01023 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42498E+00 0.00090  2.50667E-01 0.00979 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45609E+00 0.00078  3.16416E-01 0.00750 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.70203E-03 0.01015  1.11549E-04 0.06370  8.77880E-04 0.02322  7.22116E-04 0.02500  2.00953E-03 0.01574  7.56837E-04 0.02470  2.24115E-04 0.04840 ];
LAMBDA                    (idx, [1:  14]) = [  7.31321E-01 0.02453  1.26346E-02 0.00143  3.05012E-02 0.00061  1.11556E-01 0.00094  3.23271E-01 0.00061  1.23704E+00 0.00400  7.97435E+00 0.01402 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0005' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:52:32 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:50:37 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299952 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99296E-01  1.00228E+00  1.00009E+00  1.00244E+00  9.95898E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 9.3E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.17437E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88256E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04860E-01 0.00013  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05294E-01 0.00013  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68932E+00 0.00024  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.45922E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.45833E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.36085E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.22704E-01 0.00113  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001337 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00027E+04 0.00101 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00027E+04 0.00101 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.87988E+02 ;
RUNNING_TIME              (idx, 1)        =  1.18091E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.61967E-01  4.61967E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.77817E-01  1.04167E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.16183E+02  2.95643E+00  2.31920E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.05597E+00  2.49667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.74833E-02  7.16666E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.18091E+02  1.18091E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97911 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00032E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80956E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.96577E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.69052E+04 ;
TOT_SF_RATE               (idx, 1)        =  6.08574E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.31253E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.63973E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.65320E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.52651E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  8.03570E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.37414E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  7.06071E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.10054E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  9.74991E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.27360E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.38940E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.46531E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.63854E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.68204E+14 ;
CS134_ACTIVITY            (idx, 1)        =  9.83009E+13 ;
CS137_ACTIVITY            (idx, 1)        =  5.31070E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.69924E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.16245E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.11711E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.40164E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.77883E+12 0.00053  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 20 ;
BURNUP                     (idx, [1:  2])  = [  4.00000E+01  4.00039E+01 ];
BURN_DAYS                 (idx, 1)        =  1.00000E+03 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  8.39523E-01 0.00108 ];
U233_FISS                 (idx, [1:   4]) = [  9.60211E+09 1.00000  7.17360E-07 1.00000 ];
U235_FISS                 (idx, [1:   4]) = [  2.74645E+15 0.00203  2.02542E-01 0.00186 ];
U238_FISS                 (idx, [1:   4]) = [  1.30318E+15 0.00291  9.60912E-02 0.00269 ];
PU239_FISS                (idx, [1:   4]) = [  7.18429E+15 0.00101  5.29864E-01 0.00082 ];
PU240_FISS                (idx, [1:   4]) = [  2.02640E+13 0.02147  1.49452E-03 0.02148 ];
PU241_FISS                (idx, [1:   4]) = [  2.24701E+15 0.00201  1.65719E-01 0.00189 ];
U235_CAPT                 (idx, [1:   4]) = [  6.13851E+14 0.00404  1.81170E-02 0.00399 ];
U238_CAPT                 (idx, [1:   4]) = [  1.11938E+16 0.00102  3.30357E-01 0.00072 ];
PU239_CAPT                (idx, [1:   4]) = [  3.93570E+15 0.00144  1.16166E-01 0.00142 ];
PU240_CAPT                (idx, [1:   4]) = [  3.39317E+15 0.00175  1.00139E-01 0.00157 ];
PU241_CAPT                (idx, [1:   4]) = [  8.07547E+14 0.00339  2.38349E-02 0.00337 ];
XE135_CAPT                (idx, [1:   4]) = [  8.08091E+14 0.00351  2.38524E-02 0.00352 ];
SM149_CAPT                (idx, [1:   4]) = [  2.70265E+14 0.00607  7.97785E-03 0.00609 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001337 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.46091E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001337 5.00846E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3539983 3.54507E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1416759 1.41878E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44595 4.46031E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001337 5.00846E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.65661E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.78211E+16 2.3E-05  3.78211E+16 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35574E+16 5.1E-06  1.35574E+16 5.1E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.38891E+16 0.00046  2.66792E+16 0.00048  7.20990E+15 0.00133 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.74464E+16 0.00033  4.02365E+16 0.00032  7.20990E+15 0.00133 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.77883E+16 0.00053  4.77883E+16 0.00053  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.22327E+18 0.00051  5.90901E+17 0.00051  1.63237E+18 0.00058 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.26363E+14 0.00523 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.78728E+16 0.00034 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.65519E+18 0.00070 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.07885E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.07885E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.36850E+00 0.00065 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.76572E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.08288E-01 0.00039 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23572E+00 0.00043 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94186E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96875E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  7.98751E-01 0.00071 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  7.91627E-01 0.00072 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.78971E+00 2.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.07196E+02 5.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  7.91367E-01 0.00073  7.87948E-01 0.00072  3.67881E-03 0.01265 ];
IMP_KEFF                  (idx, [1:   2]) = [  7.91389E-01 0.00034 ];
COL_KEFF                  (idx, [1:   2]) = [  7.91544E-01 0.00054 ];
ABS_KEFF                  (idx, [1:   2]) = [  7.91389E-01 0.00034 ];
ABS_KINF                  (idx, [1:   2]) = [  7.98510E-01 0.00033 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.68251E+01 0.00028 ];
IMP_ALF                   (idx, [1:   2]) = [  1.68245E+01 0.00010 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  9.91809E-07 0.00474 ];
IMP_EALF                  (idx, [1:   2]) = [  9.87554E-07 0.00174 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.27947E-01 0.00285 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.27650E-01 0.00110 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.08913E-03 0.00702  1.22902E-04 0.04728  1.11917E-03 0.01501  9.38710E-04 0.01564  2.53424E-03 0.01063  1.07248E-03 0.01568  3.01620E-04 0.02755 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.58193E-01 0.01515  7.75409E-03 0.03552  3.03812E-02 0.00040  1.11697E-01 0.00068  3.23678E-01 0.00040  1.22330E+00 0.00286  7.19129E+00 0.01811 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.65299E-03 0.01022  8.94732E-05 0.07819  8.74113E-04 0.02311  7.20508E-04 0.02682  1.90324E-03 0.01628  8.37663E-04 0.02431  2.27997E-04 0.04380 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.50420E-01 0.02341  1.26287E-02 0.00156  3.03659E-02 0.00056  1.11732E-01 0.00098  3.23607E-01 0.00063  1.22408E+00 0.00406  7.62445E+00 0.01622 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.20433E-05 0.00165  3.20266E-05 0.00166  3.56633E-05 0.02231 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.53508E-05 0.00146  2.53376E-05 0.00146  2.82118E-05 0.02231 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.65027E-03 0.01279  9.88894E-05 0.08858  8.18881E-04 0.02818  7.15402E-04 0.03049  1.93878E-03 0.01982  8.37282E-04 0.02992  2.41035E-04 0.05342 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.63617E-01 0.02885  1.26533E-02 0.00279  3.03802E-02 0.00081  1.11799E-01 0.00136  3.23920E-01 0.00089  1.21673E+00 0.00589  7.56727E+00 0.02388 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.12775E-05 0.00393  3.12631E-05 0.00397  2.65330E-05 0.05269 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.47427E-05 0.00380  2.47312E-05 0.00383  2.10019E-05 0.05266 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.26381E-03 0.04588  7.60308E-05 0.33265  7.39891E-04 0.10309  6.73863E-04 0.12168  1.85890E-03 0.06536  7.07861E-04 0.11873  2.07265E-04 0.19408 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.49001E-01 0.09975  1.25600E-02 0.00591  3.03746E-02 0.00194  1.12474E-01 0.00331  3.24289E-01 0.00233  1.23251E+00 0.01392  7.90967E+00 0.05993 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.18195E-03 0.04514  8.02381E-05 0.31589  7.38483E-04 0.10031  6.62778E-04 0.11945  1.80203E-03 0.06330  6.90379E-04 0.11853  2.08037E-04 0.19161 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.39736E-01 0.09733  1.25600E-02 0.00591  3.03754E-02 0.00194  1.12448E-01 0.00331  3.24300E-01 0.00232  1.23334E+00 0.01384  7.93341E+00 0.05949 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.37904E+02 0.04640 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.16666E-05 0.00102 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.50533E-05 0.00071 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.47524E-03 0.00847 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.41449E+02 0.00860 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.60853E-07 0.00087 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.59847E-06 0.00046  2.59833E-06 0.00046  2.62460E-06 0.00642 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.97131E-05 0.00101  3.97307E-05 0.00101  3.61335E-05 0.01237 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.06477E-01 0.00039  6.07170E-01 0.00039  5.06206E-01 0.01106 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.01765E+01 0.01487 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.45833E+01 0.00054  3.30988E+01 0.00063 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.20084E+04 0.00248  3.07024E+05 0.00156  6.11493E+05 0.00082  6.50007E+05 0.00068  5.98543E+05 0.00062  6.42394E+05 0.00062  4.36045E+05 0.00070  3.85464E+05 0.00070  2.95737E+05 0.00072  2.41630E+05 0.00079  2.08229E+05 0.00074  1.87513E+05 0.00078  1.73431E+05 0.00108  1.64648E+05 0.00073  1.60655E+05 0.00074  1.38780E+05 0.00120  1.37064E+05 0.00118  1.36045E+05 0.00084  1.33542E+05 0.00083  2.60102E+05 0.00075  2.51976E+05 0.00091  1.81652E+05 0.00110  1.17460E+05 0.00116  1.34972E+05 0.00092  1.29116E+05 0.00087  1.14095E+05 0.00096  1.83447E+05 0.00078  4.25228E+04 0.00156  5.16815E+04 0.00168  4.73254E+04 0.00138  2.79207E+04 0.00192  4.81472E+04 0.00136  3.11491E+04 0.00193  2.48693E+04 0.00170  4.08749E+03 0.00318  3.43894E+03 0.00382  3.01790E+03 0.00481  2.89537E+03 0.00339  2.94314E+03 0.00336  3.19934E+03 0.00428  3.84947E+03 0.00377  4.06759E+03 0.00371  8.22681E+03 0.00234  1.38042E+04 0.00289  1.81087E+04 0.00233  4.87608E+04 0.00146  5.11917E+04 0.00155  5.49816E+04 0.00130  3.55632E+04 0.00157  2.49921E+04 0.00191  1.85212E+04 0.00215  2.17871E+04 0.00155  4.27032E+04 0.00155  6.09555E+04 0.00173  1.22514E+05 0.00191  1.95932E+05 0.00195  2.99818E+05 0.00203  1.94199E+05 0.00202  1.39933E+05 0.00207  1.01201E+05 0.00237  9.12047E+04 0.00225  9.01887E+04 0.00225  7.55086E+04 0.00220  5.10846E+04 0.00247  4.74545E+04 0.00218  4.22795E+04 0.00228  3.57613E+04 0.00214  2.83279E+04 0.00264  1.89629E+04 0.00278  6.72256E+03 0.00316 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  7.98668E-01 0.00051 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.78085E+18 0.00052  4.42469E+17 0.00193 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.40160E-01 0.00011  1.56682E+00 0.00056 ];
INF_CAPT                  (idx, [1:   4]) = [  9.00506E-03 0.00066  4.03532E-02 0.00121 ];
INF_ABS                   (idx, [1:   4]) = [  1.04601E-02 0.00057  6.51436E-02 0.00147 ];
INF_FISS                  (idx, [1:   4]) = [  1.45503E-03 0.00052  2.47904E-02 0.00190 ];
INF_NSF                   (idx, [1:   4]) = [  4.04924E-03 0.00054  6.91980E-02 0.00197 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.78292E+00 7.9E-05  2.79132E+00 8.5E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06841E+02 1.1E-05  2.07280E+02 1.7E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.42181E-08 0.00037  2.60013E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29697E-01 0.00011  1.50171E+00 0.00064 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43878E-01 0.00025  3.97116E-01 0.00081 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62473E-02 0.00034  9.36315E-02 0.00129 ];
INF_SCATT3                (idx, [1:   4]) = [  7.24917E-03 0.00314  2.81926E-02 0.00201 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03352E-02 0.00259 -8.93676E-03 0.00701 ];
INF_SCATT5                (idx, [1:   4]) = [  1.99984E-04 0.10334  6.75918E-03 0.00851 ];
INF_SCATT6                (idx, [1:   4]) = [  5.15149E-03 0.00302 -1.72332E-02 0.00328 ];
INF_SCATT7                (idx, [1:   4]) = [  7.79779E-04 0.01649  4.57673E-04 0.09482 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29742E-01 0.00011  1.50171E+00 0.00064 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43878E-01 0.00025  3.97116E-01 0.00081 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62477E-02 0.00034  9.36315E-02 0.00129 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.24908E-03 0.00313  2.81926E-02 0.00201 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03352E-02 0.00259 -8.93676E-03 0.00701 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.99878E-04 0.10346  6.75918E-03 0.00851 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.15153E-03 0.00302 -1.72332E-02 0.00328 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.79839E-04 0.01649  4.57673E-04 0.09482 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12709E-01 0.00024  1.02117E+00 0.00045 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56709E+00 0.00024  3.26426E-01 0.00045 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.04147E-02 0.00056  6.51436E-02 0.00147 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70052E-02 0.00029  6.62474E-02 0.00150 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.13155E-01 0.00010  1.65421E-02 0.00068  1.13186E-03 0.00495  1.50058E+00 0.00064 ];
INF_S1                    (idx, [1:   8]) = [  2.39117E-01 0.00025  4.76070E-03 0.00104  4.85827E-04 0.00905  3.96630E-01 0.00082 ];
INF_S2                    (idx, [1:   8]) = [  9.77806E-02 0.00036 -1.53332E-03 0.00302  2.63654E-04 0.01042  9.33679E-02 0.00131 ];
INF_S3                    (idx, [1:   8]) = [  8.96644E-03 0.00272 -1.71727E-03 0.00184  9.58556E-05 0.02100  2.80967E-02 0.00201 ];
INF_S4                    (idx, [1:   8]) = [ -9.81707E-03 0.00272 -5.18125E-04 0.00546  1.34081E-07 1.00000 -8.93690E-03 0.00701 ];
INF_S5                    (idx, [1:   8]) = [  1.47809E-04 0.13121  5.21746E-05 0.05192 -3.57348E-05 0.05138  6.79491E-03 0.00856 ];
INF_S6                    (idx, [1:   8]) = [  5.27918E-03 0.00298 -1.27691E-04 0.01846 -4.79291E-05 0.03866 -1.71853E-02 0.00330 ];
INF_S7                    (idx, [1:   8]) = [  9.48028E-04 0.01381 -1.68249E-04 0.01167 -4.24260E-05 0.03368  5.00099E-04 0.08707 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.13200E-01 0.00010  1.65421E-02 0.00068  1.13186E-03 0.00495  1.50058E+00 0.00064 ];
INF_SP1                   (idx, [1:   8]) = [  2.39118E-01 0.00025  4.76070E-03 0.00104  4.85827E-04 0.00905  3.96630E-01 0.00082 ];
INF_SP2                   (idx, [1:   8]) = [  9.77810E-02 0.00036 -1.53332E-03 0.00302  2.63654E-04 0.01042  9.33679E-02 0.00131 ];
INF_SP3                   (idx, [1:   8]) = [  8.96635E-03 0.00272 -1.71727E-03 0.00184  9.58556E-05 0.02100  2.80967E-02 0.00201 ];
INF_SP4                   (idx, [1:   8]) = [ -9.81711E-03 0.00271 -5.18125E-04 0.00546  1.34081E-07 1.00000 -8.93690E-03 0.00701 ];
INF_SP5                   (idx, [1:   8]) = [  1.47704E-04 0.13136  5.21746E-05 0.05192 -3.57348E-05 0.05138  6.79491E-03 0.00856 ];
INF_SP6                   (idx, [1:   8]) = [  5.27922E-03 0.00298 -1.27691E-04 0.01846 -4.79291E-05 0.03866 -1.71853E-02 0.00330 ];
INF_SP7                   (idx, [1:   8]) = [  9.48088E-04 0.01381 -1.68249E-04 0.01167 -4.24260E-05 0.03368  5.00099E-04 0.08707 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32008E-01 0.00059  1.20993E+00 0.00727 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33553E-01 0.00116  1.31419E+00 0.00980 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33739E-01 0.00095  1.32708E+00 0.00925 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28810E-01 0.00084  1.03771E+00 0.00711 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43674E+00 0.00059  2.75832E-01 0.00696 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42727E+00 0.00116  2.54219E-01 0.00968 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42612E+00 0.00094  2.51679E-01 0.00898 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45684E+00 0.00085  3.21597E-01 0.00687 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.65299E-03 0.01022  8.94732E-05 0.07819  8.74113E-04 0.02311  7.20508E-04 0.02682  1.90324E-03 0.01628  8.37663E-04 0.02431  2.27997E-04 0.04380 ];
LAMBDA                    (idx, [1:  14]) = [  7.50420E-01 0.02341  1.26287E-02 0.00156  3.03659E-02 0.00056  1.11732E-01 0.00098  3.23607E-01 0.00063  1.22408E+00 0.00406  7.62445E+00 0.01622 ];

