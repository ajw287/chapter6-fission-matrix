
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0007' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:45:38 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:49:32 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292338 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.92150E-01  1.00454E+00  1.00106E+00  1.00218E+00  1.00007E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14852E-02 0.00103  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88515E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.93710E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.94143E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.72399E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.61165E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.61091E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.35965E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.39161E-01 0.00103  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000564 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00011E+04 0.00066 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00011E+04 0.00066 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.75950E+01 ;
RUNNING_TIME              (idx, 1)        =  3.90510E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  4.82450E-01  4.82450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.26667E-03  1.26667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.42120E+00  3.42120E+00  0.00000E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.90480E+00  0.00000E+00 ];
CPU_USAGE                 (idx, 1)        = 4.50565 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00015E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  8.64784E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.64974E+08 ;
TOT_DECAY_HEAT            (idx, 1)        =  6.58887E-04 ;
TOT_SF_RATE               (idx, 1)        =  7.41545E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  8.64974E+08 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  6.58887E-04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  7.91453E+03 ;
INGESTION_TOXICITY        (idx, 1)        =  4.17808E+01 ;
ACTINIDE_INH_TOX          (idx, 1)        =  7.91453E+03 ;
ACTINIDE_ING_TOX          (idx, 1)        =  4.17808E+01 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  1.11735E+08 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  8.64586E+08 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  3.55880E+08 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.94013E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 0 ;
BURNUP                     (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
BURN_DAYS                 (idx, 1)        =  0.00000E+00 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.88534E-01 0.00099 ];
U235_FISS                 (idx, [1:   4]) = [  1.30375E+16 0.00056  9.41071E-01 0.00015 ];
U238_FISS                 (idx, [1:   4]) = [  8.15095E+14 0.00260  5.88293E-02 0.00247 ];
U235_CAPT                 (idx, [1:   4]) = [  2.79211E+15 0.00142  1.81628E-01 0.00127 ];
U238_CAPT                 (idx, [1:   4]) = [  7.67887E+15 0.00097  4.99500E-01 0.00064 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000564 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.27975E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000564 5.00728E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2610797 2.61428E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2352859 2.35609E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 36908 3.69132E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000564 5.00728E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.44589E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 3.7E-09  4.50057E+05 3.7E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.41044E+16 1.0E-05  3.41044E+16 1.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38683E+16 1.2E-06  1.38683E+16 1.2E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.53891E+16 0.00050  1.05905E+16 0.00050  4.79864E+15 0.00104 ];
TOT_ABSRATE               (idx, [1:   6]) = [  2.92574E+16 0.00026  2.44588E+16 0.00022  4.79864E+15 0.00104 ];
TOT_SRCRATE               (idx, [1:   6]) = [  2.94013E+16 0.00049  2.94013E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.41793E+18 0.00047  3.86565E+17 0.00046  1.03137E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.17075E+14 0.00532 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  2.94745E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.06309E+18 0.00059 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12514E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.79769E+00 0.00036 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.80847E-01 0.00030 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.73664E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23456E+00 0.00032 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95214E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97391E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.16738E+00 0.00045 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.15876E+00 0.00045 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.45915E+00 1.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02550E+02 1.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.15891E+00 0.00046  1.15062E+00 0.00045  8.14417E-03 0.00795 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.15880E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.16010E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.15880E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.16741E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.76171E+01 0.00017 ];
IMP_ALF                   (idx, [1:   2]) = [  1.76128E+01 7.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.47718E-07 0.00303 ];
IMP_EALF                  (idx, [1:   2]) = [  4.48857E-07 0.00136 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.99210E-01 0.00268 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.99785E-01 0.00108 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.10940E-03 0.00545  1.72121E-04 0.03198  9.73284E-04 0.01339  9.62648E-04 0.01372  2.78331E-03 0.00750  9.07978E-04 0.01340  3.10052E-04 0.02429 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.18266E-01 0.01273  1.09419E-02 0.01684  3.16548E-02 0.00023  1.10150E-01 0.00028  3.20443E-01 0.00021  1.34596E+00 0.00016  8.54254E+00 0.00931 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.05805E-03 0.00824  1.88796E-04 0.04762  1.13302E-03 0.01956  1.10073E-03 0.01999  3.21438E-03 0.01136  1.06811E-03 0.02112  3.53018E-04 0.03565 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.15537E-01 0.01876  1.24908E-02 2.7E-06  3.16453E-02 0.00031  1.10160E-01 0.00041  3.20495E-01 0.00031  1.34617E+00 0.00022  8.89981E+00 0.00206 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.55707E-05 0.00105  2.55594E-05 0.00105  2.72832E-05 0.01116 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.96307E-05 0.00091  2.96175E-05 0.00092  3.16095E-05 0.01112 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.03151E-03 0.00800  1.95113E-04 0.04516  1.11667E-03 0.01891  1.09393E-03 0.02055  3.21997E-03 0.01108  1.05955E-03 0.02025  3.46286E-04 0.03513 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.07684E-01 0.01850  1.24907E-02 3.4E-06  3.16451E-02 0.00035  1.10115E-01 0.00041  3.20461E-01 0.00031  1.34599E+00 0.00024  8.89067E+00 0.00240 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.55519E-05 0.00243  2.55475E-05 0.00244  2.65050E-05 0.02655 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.96099E-05 0.00241  2.96047E-05 0.00241  3.07347E-05 0.02664 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.23625E-03 0.02418  2.21933E-04 0.14028  1.19130E-03 0.05919  1.07757E-03 0.06132  3.26057E-03 0.03479  1.15360E-03 0.06253  3.31274E-04 0.11645 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.98119E-01 0.05930  1.24907E-02 6.1E-06  3.16326E-02 0.00080  1.10084E-01 0.00096  3.20464E-01 0.00092  1.34588E+00 0.00059  8.89974E+00 0.00518 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.26763E-03 0.02387  2.21858E-04 0.14447  1.19676E-03 0.05715  1.06121E-03 0.05993  3.29494E-03 0.03453  1.15986E-03 0.06039  3.32994E-04 0.11172 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.04774E-01 0.05763  1.24907E-02 6.0E-06  3.16275E-02 0.00081  1.10076E-01 0.00095  3.20471E-01 0.00090  1.34588E+00 0.00058  8.90122E+00 0.00518 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.83958E+02 0.02421 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.55380E-05 0.00061 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.95932E-05 0.00041 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.10071E-03 0.00472 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.78067E+02 0.00470 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.13812E-07 0.00063 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.88599E-06 0.00041  2.88562E-06 0.00042  2.93463E-06 0.00505 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.08074E-05 0.00077  4.08251E-05 0.00077  3.83496E-05 0.00837 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.71732E-01 0.00032  6.70956E-01 0.00032  8.10708E-01 0.00869 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03782E+01 0.01344 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.61091E+01 0.00045  3.57674E+01 0.00042 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.16399E+04 0.00337  2.91447E+05 0.00157  6.03066E+05 0.00077  6.50811E+05 0.00079  5.99926E+05 0.00088  6.44314E+05 0.00052  4.37650E+05 0.00075  3.87617E+05 0.00061  2.96425E+05 0.00079  2.42400E+05 0.00078  2.08810E+05 0.00094  1.88490E+05 0.00099  1.73439E+05 0.00077  1.65363E+05 0.00084  1.60844E+05 0.00071  1.38974E+05 0.00060  1.37539E+05 0.00076  1.36086E+05 0.00094  1.33693E+05 0.00088  2.60733E+05 0.00065  2.51766E+05 0.00066  1.81303E+05 0.00080  1.17693E+05 0.00083  1.35644E+05 0.00070  1.28135E+05 0.00101  1.16361E+05 0.00102  1.90955E+05 0.00075  4.37772E+04 0.00179  5.47888E+04 0.00143  4.97094E+04 0.00160  2.89138E+04 0.00185  5.03817E+04 0.00136  3.40900E+04 0.00152  2.90854E+04 0.00183  5.54028E+03 0.00344  5.50998E+03 0.00393  5.65263E+03 0.00403  5.81789E+03 0.00375  5.77425E+03 0.00393  5.68867E+03 0.00312  5.89321E+03 0.00369  5.50769E+03 0.00346  1.03985E+04 0.00313  1.66405E+04 0.00209  2.12694E+04 0.00192  5.60537E+04 0.00154  5.91700E+04 0.00133  6.48532E+04 0.00148  4.45669E+04 0.00179  3.34938E+04 0.00187  2.57726E+04 0.00158  3.06667E+04 0.00131  5.82900E+04 0.00129  7.90731E+04 0.00123  1.51822E+05 0.00104  2.33251E+05 0.00114  3.46969E+05 0.00135  2.21158E+05 0.00118  1.57768E+05 0.00111  1.13075E+05 0.00151  1.01320E+05 0.00154  9.96004E+04 0.00151  8.29657E+04 0.00132  5.59631E+04 0.00170  5.16132E+04 0.00160  4.58993E+04 0.00142  3.87272E+04 0.00154  3.04020E+04 0.00141  2.03505E+04 0.00160  7.19826E+03 0.00224 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.16873E+00 0.00042 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.10132E+18 0.00047  3.16647E+17 0.00111 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37394E-01 8.3E-05  1.48832E+00 0.00039 ];
INF_CAPT                  (idx, [1:   4]) = [  6.39633E-03 0.00063  2.63552E-02 0.00031 ];
INF_ABS                   (idx, [1:   4]) = [  8.69635E-03 0.00049  6.21592E-02 0.00076 ];
INF_FISS                  (idx, [1:   4]) = [  2.30002E-03 0.00053  3.58040E-02 0.00109 ];
INF_NSF                   (idx, [1:   4]) = [  5.88723E-03 0.00053  8.72435E-02 0.00109 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.55964E+00 4.1E-05  2.43670E+00 2.7E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03804E+02 4.9E-06  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  5.89279E-08 0.00041  2.54384E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28707E-01 8.7E-05  1.42623E+00 0.00044 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43394E-01 0.00017  3.77008E-01 0.00055 ];
INF_SCATT2                (idx, [1:   4]) = [  9.59931E-02 0.00032  8.96083E-02 0.00089 ];
INF_SCATT3                (idx, [1:   4]) = [  7.36461E-03 0.00271  2.70598E-02 0.00229 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02854E-02 0.00172 -8.10758E-03 0.00760 ];
INF_SCATT5                (idx, [1:   4]) = [  1.58003E-04 0.10975  6.25878E-03 0.00855 ];
INF_SCATT6                (idx, [1:   4]) = [  5.05760E-03 0.00301 -1.59565E-02 0.00266 ];
INF_SCATT7                (idx, [1:   4]) = [  7.46314E-04 0.01639  3.28209E-04 0.11630 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28746E-01 8.7E-05  1.42623E+00 0.00044 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43395E-01 0.00017  3.77008E-01 0.00055 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.59930E-02 0.00032  8.96083E-02 0.00089 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.36438E-03 0.00271  2.70598E-02 0.00229 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02854E-02 0.00172 -8.10758E-03 0.00760 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.57858E-04 0.10981  6.25878E-03 0.00855 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.05781E-03 0.00301 -1.59565E-02 0.00266 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.46404E-04 0.01639  3.28209E-04 0.11630 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13974E-01 0.00031  9.60893E-01 0.00032 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55783E+00 0.00031  3.46900E-01 0.00032 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.65749E-03 0.00050  6.21592E-02 0.00076 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69303E-02 0.00018  6.32128E-02 0.00092 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10464E-01 8.2E-05  1.82437E-02 0.00037  1.12236E-03 0.00366  1.42511E+00 0.00044 ];
INF_S1                    (idx, [1:   8]) = [  2.38059E-01 0.00016  5.33558E-03 0.00106  4.74282E-04 0.00599  3.76534E-01 0.00055 ];
INF_S2                    (idx, [1:   8]) = [  9.75606E-02 0.00030 -1.56745E-03 0.00295  2.56954E-04 0.00885  8.93513E-02 0.00090 ];
INF_S3                    (idx, [1:   8]) = [  9.24053E-03 0.00213 -1.87592E-03 0.00155  9.18963E-05 0.01749  2.69679E-02 0.00230 ];
INF_S4                    (idx, [1:   8]) = [ -9.65650E-03 0.00182 -6.28938E-04 0.00414 -1.98888E-07 1.00000 -8.10739E-03 0.00765 ];
INF_S5                    (idx, [1:   8]) = [  1.41299E-04 0.12332  1.67032E-05 0.14012 -3.84962E-05 0.03922  6.29728E-03 0.00852 ];
INF_S6                    (idx, [1:   8]) = [  5.20184E-03 0.00296 -1.44244E-04 0.01683 -4.87175E-05 0.03013 -1.59077E-02 0.00268 ];
INF_S7                    (idx, [1:   8]) = [  9.18993E-04 0.01290 -1.72679E-04 0.01130 -4.46579E-05 0.02302  3.72867E-04 0.10247 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10503E-01 8.2E-05  1.82437E-02 0.00037  1.12236E-03 0.00366  1.42511E+00 0.00044 ];
INF_SP1                   (idx, [1:   8]) = [  2.38059E-01 0.00016  5.33558E-03 0.00106  4.74282E-04 0.00599  3.76534E-01 0.00055 ];
INF_SP2                   (idx, [1:   8]) = [  9.75605E-02 0.00030 -1.56745E-03 0.00295  2.56954E-04 0.00885  8.93513E-02 0.00090 ];
INF_SP3                   (idx, [1:   8]) = [  9.24029E-03 0.00213 -1.87592E-03 0.00155  9.18963E-05 0.01749  2.69679E-02 0.00230 ];
INF_SP4                   (idx, [1:   8]) = [ -9.65644E-03 0.00181 -6.28938E-04 0.00414 -1.98888E-07 1.00000 -8.10739E-03 0.00765 ];
INF_SP5                   (idx, [1:   8]) = [  1.41155E-04 0.12337  1.67032E-05 0.14012 -3.84962E-05 0.03922  6.29728E-03 0.00852 ];
INF_SP6                   (idx, [1:   8]) = [  5.20205E-03 0.00296 -1.44244E-04 0.01683 -4.87175E-05 0.03013 -1.59077E-02 0.00268 ];
INF_SP7                   (idx, [1:   8]) = [  9.19083E-04 0.01290 -1.72679E-04 0.01130 -4.46579E-05 0.02302  3.72867E-04 0.10247 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32323E-01 0.00073  1.03996E+00 0.00597 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33613E-01 0.00100  1.10206E+00 0.00648 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33856E-01 0.00090  1.09728E+00 0.00762 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29556E-01 0.00088  9.39026E-01 0.00724 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43480E+00 0.00073  3.20799E-01 0.00599 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42689E+00 0.00100  3.02765E-01 0.00643 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42541E+00 0.00090  3.04206E-01 0.00764 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45210E+00 0.00088  3.55427E-01 0.00727 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.05805E-03 0.00824  1.88796E-04 0.04762  1.13302E-03 0.01956  1.10073E-03 0.01999  3.21438E-03 0.01136  1.06811E-03 0.02112  3.53018E-04 0.03565 ];
LAMBDA                    (idx, [1:  14]) = [  8.15537E-01 0.01876  1.24908E-02 2.7E-06  3.16453E-02 0.00031  1.10160E-01 0.00041  3.20495E-01 0.00031  1.34617E+00 0.00022  8.89981E+00 0.00206 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0007' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:45:38 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:55:39 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292338 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.91697E-01  1.00399E+00  1.00150E+00  1.00190E+00  1.00090E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14304E-02 0.00108  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88570E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.95811E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.96243E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.71513E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.58632E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.58558E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.17496E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.31642E-01 0.00112  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000383 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00008E+04 0.00071 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00008E+04 0.00071 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.81403E+01 ;
RUNNING_TIME              (idx, 1)        =  1.00196E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.82450E-01  4.82450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.42833E-02  6.30000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.46630E+00  3.40875E+00  2.63635E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.59500E-02  2.75500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.53334E-03  8.50002E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.00195E+01  1.26399E+02 ];
CPU_USAGE                 (idx, 1)        = 4.80460 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99939E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.38248E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  7.87044E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.75449E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.41531E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.18521E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  8.40463E+02 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  6.68521E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.67042E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.21394E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  1.74838E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.29425E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  3.43853E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.84514E+06 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  1.40452E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.28380E+11 ;
TE132_ACTIVITY            (idx, 1)        =  2.51418E+14 ;
I131_ACTIVITY             (idx, 1)        =  7.42327E+13 ;
I132_ACTIVITY             (idx, 1)        =  2.43592E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.68660E+07 ;
CS137_ACTIVITY            (idx, 1)        =  1.34684E+11 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  6.55707E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.64895E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.30740E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  9.02219E+16 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.05348E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 1 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E-01  1.00009E-01 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+00 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.00882E-01 0.00112 ];
U235_FISS                 (idx, [1:   4]) = [  1.29973E+16 0.00060  9.36928E-01 0.00018 ];
U238_FISS                 (idx, [1:   4]) = [  8.49778E+14 0.00276  6.12546E-02 0.00267 ];
PU239_FISS                (idx, [1:   4]) = [  2.32898E+13 0.01522  1.67935E-03 0.01526 ];
U235_CAPT                 (idx, [1:   4]) = [  2.80489E+15 0.00143  1.70206E-01 0.00136 ];
U238_CAPT                 (idx, [1:   4]) = [  7.87763E+15 0.00104  4.77978E-01 0.00067 ];
PU239_CAPT                (idx, [1:   4]) = [  1.32405E+13 0.02095  8.04523E-04 0.02107 ];
PU240_CAPT                (idx, [1:   4]) = [  1.71339E+11 0.19070  1.03357E-05 0.19053 ];
XE135_CAPT                (idx, [1:   4]) = [  7.08289E+14 0.00300  4.29776E-02 0.00293 ];
SM149_CAPT                (idx, [1:   4]) = [  2.08797E+13 0.01684  1.26700E-03 0.01681 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000383 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.47164E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000383 5.00747E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2694702 2.69859E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2268431 2.27162E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 37250 3.72629E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000383 5.00747E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.02914E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 3.7E-09  4.50057E+05 3.7E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.41229E+16 1.2E-05  3.41229E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38670E+16 1.4E-06  1.38670E+16 1.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.64803E+16 0.00045  1.15655E+16 0.00048  4.91480E+15 0.00099 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.03473E+16 0.00024  2.54325E+16 0.00022  4.91480E+15 0.00099 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.05348E+16 0.00049  3.05348E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.46487E+18 0.00046  3.98420E+17 0.00044  1.06645E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.27594E+14 0.00512 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.05749E+16 0.00025 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.09635E+18 0.00057 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12503E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12503E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.71292E+00 0.00039 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.84084E-01 0.00029 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.73915E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24455E+00 0.00032 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95181E-01 3.0E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97354E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.12640E+00 0.00048 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.11801E+00 0.00049 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46073E+00 1.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02570E+02 1.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.11771E+00 0.00050  1.11014E+00 0.00049  7.86882E-03 0.00771 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.11769E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.11764E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.11769E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.12609E+00 0.00024 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75368E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75360E+01 8.2E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.85344E-07 0.00326 ];
IMP_EALF                  (idx, [1:   2]) = [  4.84713E-07 0.00145 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.08038E-01 0.00290 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.07344E-01 0.00122 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.37853E-03 0.00519  1.82528E-04 0.03243  1.00393E-03 0.01346  9.97117E-04 0.01308  2.92298E-03 0.00801  9.57447E-04 0.01396  3.14527E-04 0.02345 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.09891E-01 0.01210  1.07670E-02 0.01791  3.16563E-02 0.00021  1.10163E-01 0.00028  3.20614E-01 0.00021  1.34529E+00 0.00017  8.63844E+00 0.00804 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.10959E-03 0.00824  1.99766E-04 0.04637  1.10173E-03 0.01914  1.10280E-03 0.01988  3.28202E-03 0.01124  1.07940E-03 0.02077  3.43862E-04 0.03474 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.02543E-01 0.01764  1.24908E-02 2.8E-06  3.16549E-02 0.00030  1.10180E-01 0.00039  3.20614E-01 0.00032  1.34540E+00 0.00023  8.89657E+00 0.00201 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.53914E-05 0.00110  2.53756E-05 0.00110  2.76583E-05 0.01100 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.83763E-05 0.00096  2.83586E-05 0.00096  3.09111E-05 0.01100 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.03086E-03 0.00780  2.12246E-04 0.04883  1.09492E-03 0.02061  1.09682E-03 0.01935  3.22510E-03 0.01205  1.06228E-03 0.02100  3.39508E-04 0.03712 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.01042E-01 0.01931  1.24907E-02 3.2E-06  3.16604E-02 0.00033  1.10170E-01 0.00044  3.20498E-01 0.00033  1.34514E+00 0.00026  8.90375E+00 0.00247 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.52642E-05 0.00246  2.52487E-05 0.00246  2.81824E-05 0.02897 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.82343E-05 0.00240  2.82170E-05 0.00241  3.14973E-05 0.02892 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.29604E-03 0.02599  2.50774E-04 0.16188  1.26938E-03 0.06116  1.01370E-03 0.06266  3.27461E-03 0.03812  1.13095E-03 0.06377  3.56620E-04 0.11636 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.69302E-01 0.05498  1.24908E-02 7.6E-06  3.16506E-02 0.00079  1.10034E-01 0.00098  3.20510E-01 0.00094  1.34446E+00 0.00064  8.97750E+00 0.00611 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.32200E-03 0.02480  2.53090E-04 0.15392  1.27544E-03 0.05916  1.02100E-03 0.06157  3.32749E-03 0.03669  1.10459E-03 0.06189  3.40391E-04 0.11301 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.56287E-01 0.05414  1.24908E-02 7.5E-06  3.16532E-02 0.00077  1.10071E-01 0.00099  3.20530E-01 0.00092  1.34432E+00 0.00064  8.98114E+00 0.00610 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.90401E+02 0.02637 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.53251E-05 0.00073 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.83024E-05 0.00051 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.14524E-03 0.00450 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.82193E+02 0.00453 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.04404E-07 0.00061 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.88254E-06 0.00042  2.88239E-06 0.00042  2.90508E-06 0.00502 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.99084E-05 0.00077  3.99294E-05 0.00077  3.70207E-05 0.00811 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.71943E-01 0.00031  6.71308E-01 0.00031  7.81032E-01 0.00810 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03144E+01 0.01209 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.58558E+01 0.00043  3.51272E+01 0.00040 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.13927E+04 0.00286  2.90528E+05 0.00195  6.02992E+05 0.00098  6.50150E+05 0.00068  5.98578E+05 0.00069  6.44683E+05 0.00094  4.37220E+05 0.00074  3.87234E+05 0.00078  2.96444E+05 0.00105  2.42146E+05 0.00080  2.09033E+05 0.00085  1.88228E+05 0.00083  1.73678E+05 0.00072  1.65075E+05 0.00090  1.60865E+05 0.00075  1.38848E+05 0.00100  1.36877E+05 0.00093  1.35953E+05 0.00095  1.33519E+05 0.00075  2.60490E+05 0.00056  2.51172E+05 0.00074  1.81254E+05 0.00075  1.17109E+05 0.00084  1.35559E+05 0.00090  1.27925E+05 0.00111  1.16256E+05 0.00076  1.91050E+05 0.00070  4.36985E+04 0.00191  5.48612E+04 0.00151  4.97501E+04 0.00140  2.88703E+04 0.00237  5.02617E+04 0.00110  3.42054E+04 0.00190  2.91478E+04 0.00189  5.58478E+03 0.00326  5.54902E+03 0.00399  5.67420E+03 0.00453  5.80178E+03 0.00343  5.77649E+03 0.00317  5.66814E+03 0.00455  5.90691E+03 0.00395  5.49649E+03 0.00339  1.04737E+04 0.00325  1.66836E+04 0.00281  2.12035E+04 0.00244  5.58474E+04 0.00152  5.89868E+04 0.00151  6.46851E+04 0.00102  4.43184E+04 0.00136  3.30719E+04 0.00157  2.56956E+04 0.00197  3.03068E+04 0.00166  5.76360E+04 0.00169  7.80680E+04 0.00131  1.49645E+05 0.00096  2.28701E+05 0.00118  3.38934E+05 0.00121  2.15199E+05 0.00117  1.53610E+05 0.00140  1.10122E+05 0.00124  9.86759E+04 0.00137  9.71279E+04 0.00148  8.10576E+04 0.00140  5.46014E+04 0.00138  5.04232E+04 0.00150  4.48245E+04 0.00128  3.78911E+04 0.00187  2.97614E+04 0.00168  1.98700E+04 0.00211  7.04479E+03 0.00215 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.12603E+00 0.00046 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.14272E+18 0.00045  3.22182E+17 0.00114 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37584E-01 0.00010  1.49627E+00 0.00043 ];
INF_CAPT                  (idx, [1:   4]) = [  6.39574E-03 0.00068  2.84705E-02 0.00040 ];
INF_ABS                   (idx, [1:   4]) = [  8.68950E-03 0.00053  6.33819E-02 0.00082 ];
INF_FISS                  (idx, [1:   4]) = [  2.29376E-03 0.00036  3.49114E-02 0.00116 ];
INF_NSF                   (idx, [1:   4]) = [  5.87254E-03 0.00037  8.50978E-02 0.00117 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.56023E+00 6.2E-05  2.43754E+00 5.2E-07 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03811E+02 5.0E-06  2.02281E+02 8.2E-08 ];
INF_INVV                  (idx, [1:   4]) = [  5.89555E-08 0.00037  2.53911E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28896E-01 0.00010  1.43287E+00 0.00048 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43578E-01 0.00017  3.79328E-01 0.00056 ];
INF_SCATT2                (idx, [1:   4]) = [  9.60668E-02 0.00028  9.03892E-02 0.00085 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34343E-03 0.00245  2.71740E-02 0.00329 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02931E-02 0.00183 -8.29417E-03 0.00607 ];
INF_SCATT5                (idx, [1:   4]) = [  1.37235E-04 0.13869  6.20913E-03 0.00697 ];
INF_SCATT6                (idx, [1:   4]) = [  5.06657E-03 0.00340 -1.61426E-02 0.00324 ];
INF_SCATT7                (idx, [1:   4]) = [  7.42686E-04 0.02334  2.75889E-04 0.14775 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28935E-01 0.00010  1.43287E+00 0.00048 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43579E-01 0.00017  3.79328E-01 0.00056 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.60666E-02 0.00028  9.03892E-02 0.00085 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34318E-03 0.00245  2.71740E-02 0.00329 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02931E-02 0.00183 -8.29417E-03 0.00607 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.37403E-04 0.13861  6.20913E-03 0.00697 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.06660E-03 0.00341 -1.61426E-02 0.00324 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.42573E-04 0.02334  2.75889E-04 0.14775 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.14023E-01 0.00036  9.64000E-01 0.00038 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55747E+00 0.00036  3.45783E-01 0.00038 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.64957E-03 0.00055  6.33819E-02 0.00082 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69533E-02 0.00019  6.45342E-02 0.00095 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10631E-01 0.00010  1.82649E-02 0.00032  1.13841E-03 0.00385  1.43173E+00 0.00048 ];
INF_S1                    (idx, [1:   8]) = [  2.38230E-01 0.00017  5.34881E-03 0.00121  4.85571E-04 0.00750  3.78842E-01 0.00056 ];
INF_S2                    (idx, [1:   8]) = [  9.76365E-02 0.00029 -1.56977E-03 0.00329  2.61575E-04 0.00940  9.01276E-02 0.00086 ];
INF_S3                    (idx, [1:   8]) = [  9.22496E-03 0.00204 -1.88153E-03 0.00207  9.37874E-05 0.01907  2.70803E-02 0.00330 ];
INF_S4                    (idx, [1:   8]) = [ -9.65990E-03 0.00181 -6.33186E-04 0.00513 -7.20197E-07 1.00000 -8.29345E-03 0.00611 ];
INF_S5                    (idx, [1:   8]) = [  1.21415E-04 0.15246  1.58198E-05 0.16867 -3.83276E-05 0.03924  6.24746E-03 0.00703 ];
INF_S6                    (idx, [1:   8]) = [  5.20727E-03 0.00321 -1.40701E-04 0.02021 -4.97895E-05 0.02341 -1.60928E-02 0.00326 ];
INF_S7                    (idx, [1:   8]) = [  9.11804E-04 0.01862 -1.69118E-04 0.01483 -4.44140E-05 0.02502  3.20303E-04 0.12564 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10671E-01 0.00010  1.82649E-02 0.00032  1.13841E-03 0.00385  1.43173E+00 0.00048 ];
INF_SP1                   (idx, [1:   8]) = [  2.38230E-01 0.00017  5.34881E-03 0.00121  4.85571E-04 0.00750  3.78842E-01 0.00056 ];
INF_SP2                   (idx, [1:   8]) = [  9.76363E-02 0.00029 -1.56977E-03 0.00329  2.61575E-04 0.00940  9.01276E-02 0.00086 ];
INF_SP3                   (idx, [1:   8]) = [  9.22471E-03 0.00204 -1.88153E-03 0.00207  9.37874E-05 0.01907  2.70803E-02 0.00330 ];
INF_SP4                   (idx, [1:   8]) = [ -9.65993E-03 0.00181 -6.33186E-04 0.00513 -7.20197E-07 1.00000 -8.29345E-03 0.00611 ];
INF_SP5                   (idx, [1:   8]) = [  1.21583E-04 0.15225  1.58198E-05 0.16867 -3.83276E-05 0.03924  6.24746E-03 0.00703 ];
INF_SP6                   (idx, [1:   8]) = [  5.20730E-03 0.00322 -1.40701E-04 0.02021 -4.97895E-05 0.02341 -1.60928E-02 0.00326 ];
INF_SP7                   (idx, [1:   8]) = [  9.11691E-04 0.01863 -1.69118E-04 0.01483 -4.44140E-05 0.02502  3.20303E-04 0.12564 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32340E-01 0.00062  1.06427E+00 0.00568 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33691E-01 0.00075  1.12137E+00 0.00802 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34045E-01 0.00085  1.13854E+00 0.00674 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29350E-01 0.00122  9.54453E-01 0.00599 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43469E+00 0.00062  3.13448E-01 0.00570 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42640E+00 0.00074  2.97709E-01 0.00792 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42425E+00 0.00085  2.93092E-01 0.00672 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45343E+00 0.00122  3.49543E-01 0.00602 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.10959E-03 0.00824  1.99766E-04 0.04637  1.10173E-03 0.01914  1.10280E-03 0.01988  3.28202E-03 0.01124  1.07940E-03 0.02077  3.43862E-04 0.03474 ];
LAMBDA                    (idx, [1:  14]) = [  8.02543E-01 0.01764  1.24908E-02 2.8E-06  3.16549E-02 0.00030  1.10180E-01 0.00039  3.20614E-01 0.00032  1.34540E+00 0.00023  8.89657E+00 0.00201 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0007' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:45:38 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:01:48 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292338 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.91919E-01  1.00426E+00  1.00030E+00  1.00323E+00  1.00028E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14388E-02 0.00106  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88561E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.96310E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.96743E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.71317E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.57118E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.57043E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.10806E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.29661E-01 0.00108  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000712 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00014E+04 0.00073 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00014E+04 0.00073 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  7.88702E+01 ;
RUNNING_TIME              (idx, 1)        =  1.61714E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.82450E-01  4.82450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.02000E-02  7.68333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.55441E+01  3.41222E+00  2.66558E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.13467E-01  2.80500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  5.18334E-03  9.16664E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.61713E+01  1.26796E+02 ];
CPU_USAGE                 (idx, 1)        = 4.87712 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99964E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.56235E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.70747E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.83352E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.42859E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.56655E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.09479E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.14090E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.72402E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.65807E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  2.99934E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  8.66100E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.39270E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  1.79197E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  2.36007E+07 ;
SR90_ACTIVITY             (idx, 1)        =  6.38141E+11 ;
TE132_ACTIVITY            (idx, 1)        =  5.63247E+14 ;
I131_ACTIVITY             (idx, 1)        =  2.65305E+14 ;
I132_ACTIVITY             (idx, 1)        =  5.66129E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.87601E+09 ;
CS137_ACTIVITY            (idx, 1)        =  6.73903E+11 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.63105E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.63816E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  6.64018E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.11135E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.08684E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 2 ;
BURNUP                     (idx, [1:  2])  = [  5.00000E-01  5.00045E-01 ];
BURN_DAYS                 (idx, 1)        =  1.25000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.01561E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  1.27142E+16 0.00058  9.17182E-01 0.00019 ];
U238_FISS                 (idx, [1:   4]) = [  8.54815E+14 0.00269  6.16571E-02 0.00254 ];
PU239_FISS                (idx, [1:   4]) = [  2.90994E+14 0.00461  2.09925E-02 0.00459 ];
PU240_FISS                (idx, [1:   4]) = [  1.86013E+10 0.57620  1.34878E-06 0.57623 ];
PU241_FISS                (idx, [1:   4]) = [  1.23305E+11 0.23068  8.91064E-06 0.23117 ];
U235_CAPT                 (idx, [1:   4]) = [  2.75858E+15 0.00152  1.64014E-01 0.00141 ];
U238_CAPT                 (idx, [1:   4]) = [  7.92486E+15 0.00100  4.71148E-01 0.00065 ];
PU239_CAPT                (idx, [1:   4]) = [  1.61618E+14 0.00625  9.60743E-03 0.00616 ];
PU240_CAPT                (idx, [1:   4]) = [  7.89261E+12 0.02771  4.69172E-04 0.02771 ];
PU241_CAPT                (idx, [1:   4]) = [  7.40732E+10 0.28550  4.41167E-06 0.28550 ];
XE135_CAPT                (idx, [1:   4]) = [  7.19774E+14 0.00284  4.27972E-02 0.00282 ];
SM149_CAPT                (idx, [1:   4]) = [  1.22043E+14 0.00698  7.25699E-03 0.00700 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000712 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.31448E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000712 5.00731E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2720846 2.72441E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2242451 2.24549E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 37415 3.74154E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000712 5.00731E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -2.51457E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 3.7E-09  4.50057E+05 3.7E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.42228E+16 1.2E-05  3.42228E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38594E+16 1.4E-06  1.38594E+16 1.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.68163E+16 0.00048  1.18941E+16 0.00049  4.92216E+15 0.00105 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.06757E+16 0.00027  2.57535E+16 0.00023  4.92216E+15 0.00105 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.08684E+16 0.00049  3.08684E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.47794E+18 0.00046  4.01926E+17 0.00044  1.07601E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.31008E+14 0.00507 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.09067E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.10363E+18 0.00059 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12456E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12456E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.69360E+00 0.00041 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.86264E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.73461E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24595E+00 0.00033 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95126E-01 3.1E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97378E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.11729E+00 0.00051 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.10893E+00 0.00051 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46929E+00 1.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02681E+02 1.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.10887E+00 0.00053  1.10133E+00 0.00051  7.59857E-03 0.00767 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.10896E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.10880E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.10896E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.11732E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75123E+01 0.00018 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75089E+01 8.2E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.97243E-07 0.00309 ];
IMP_EALF                  (idx, [1:   2]) = [  4.97992E-07 0.00144 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.08961E-01 0.00270 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.09815E-01 0.00119 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.30652E-03 0.00540  1.81084E-04 0.03072  1.00738E-03 0.01332  9.73906E-04 0.01297  2.91141E-03 0.00778  9.14704E-04 0.01375  3.18038E-04 0.02253 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.14657E-01 0.01186  1.09419E-02 0.01684  3.16254E-02 0.00025  1.10128E-01 0.00027  3.20571E-01 0.00020  1.34530E+00 0.00016  8.70354E+00 0.00693 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.88373E-03 0.00789  2.00080E-04 0.04356  1.12511E-03 0.01921  1.04239E-03 0.01932  3.17212E-03 0.01134  1.00673E-03 0.02127  3.37314E-04 0.03298 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.05002E-01 0.01753  1.24907E-02 4.4E-06  3.16251E-02 0.00034  1.10113E-01 0.00038  3.20643E-01 0.00030  1.34509E+00 0.00024  8.90360E+00 0.00213 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.51784E-05 0.00108  2.51684E-05 0.00109  2.66790E-05 0.01121 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.79163E-05 0.00098  2.79052E-05 0.00099  2.95781E-05 0.01120 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.85194E-03 0.00764  1.92265E-04 0.04966  1.08158E-03 0.01994  1.05061E-03 0.01838  3.19399E-03 0.01177  9.97440E-04 0.02184  3.36053E-04 0.03457 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.05559E-01 0.01817  1.24907E-02 4.1E-06  3.16303E-02 0.00039  1.10077E-01 0.00043  3.20645E-01 0.00032  1.34477E+00 0.00029  8.91786E+00 0.00263 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.49289E-05 0.00246  2.49271E-05 0.00248  2.53918E-05 0.02802 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.76382E-05 0.00238  2.76362E-05 0.00239  2.81503E-05 0.02800 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.76543E-03 0.02468  1.51888E-04 0.16426  1.00266E-03 0.06343  9.92624E-04 0.06315  3.20391E-03 0.03720  1.01355E-03 0.06944  4.00795E-04 0.11857 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.95674E-01 0.06406  1.24906E-02 1.4E-05  3.16431E-02 0.00089  1.10035E-01 0.00101  3.20717E-01 0.00097  1.34333E+00 0.00067  8.94415E+00 0.00597 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.83994E-03 0.02409  1.47570E-04 0.16328  1.00349E-03 0.06249  1.02153E-03 0.06191  3.24609E-03 0.03673  1.03789E-03 0.06640  3.83377E-04 0.11776 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.85891E-01 0.06380  1.24906E-02 1.4E-05  3.16390E-02 0.00089  1.10051E-01 0.00101  3.20681E-01 0.00095  1.34321E+00 0.00067  8.94440E+00 0.00596 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.72169E+02 0.02490 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.50689E-05 0.00072 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.77944E-05 0.00048 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.71596E-03 0.00459 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.67987E+02 0.00467 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.98997E-07 0.00064 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.88407E-06 0.00042  2.88381E-06 0.00043  2.92010E-06 0.00469 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.94569E-05 0.00079  3.94792E-05 0.00079  3.64058E-05 0.00890 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.71461E-01 0.00032  6.70909E-01 0.00032  7.69382E-01 0.00816 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03645E+01 0.01259 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.57043E+01 0.00043  3.49028E+01 0.00041 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.22444E+04 0.00391  2.90934E+05 0.00159  6.03491E+05 0.00092  6.51126E+05 0.00079  5.99362E+05 0.00074  6.44689E+05 0.00081  4.37657E+05 0.00071  3.87393E+05 0.00052  2.96406E+05 0.00094  2.42236E+05 0.00068  2.08801E+05 0.00073  1.88166E+05 0.00067  1.73959E+05 0.00089  1.65246E+05 0.00097  1.61031E+05 0.00094  1.39038E+05 0.00093  1.37107E+05 0.00095  1.35768E+05 0.00083  1.33820E+05 0.00100  2.60502E+05 0.00075  2.51342E+05 0.00055  1.81327E+05 0.00095  1.17551E+05 0.00116  1.35539E+05 0.00080  1.27955E+05 0.00094  1.16287E+05 0.00099  1.90964E+05 0.00077  4.37320E+04 0.00146  5.47510E+04 0.00161  4.98103E+04 0.00164  2.88247E+04 0.00206  5.03201E+04 0.00153  3.41018E+04 0.00164  2.90725E+04 0.00198  5.55031E+03 0.00253  5.52768E+03 0.00297  5.62952E+03 0.00393  5.82120E+03 0.00359  5.73588E+03 0.00269  5.68512E+03 0.00321  5.88114E+03 0.00261  5.49423E+03 0.00288  1.04750E+04 0.00240  1.66203E+04 0.00265  2.12393E+04 0.00171  5.60332E+04 0.00149  5.89268E+04 0.00120  6.44680E+04 0.00148  4.39874E+04 0.00148  3.28263E+04 0.00165  2.52647E+04 0.00169  2.98353E+04 0.00168  5.69225E+04 0.00126  7.69047E+04 0.00108  1.47426E+05 0.00103  2.25891E+05 0.00083  3.34598E+05 0.00067  2.12744E+05 0.00085  1.51807E+05 0.00098  1.08863E+05 0.00104  9.76366E+04 0.00092  9.60487E+04 0.00094  7.99527E+04 0.00116  5.40235E+04 0.00151  4.97898E+04 0.00109  4.42732E+04 0.00152  3.74650E+04 0.00133  2.93693E+04 0.00108  1.96984E+04 0.00174  6.95554E+03 0.00207 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.11716E+00 0.00050 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.15606E+18 0.00054  3.21907E+17 0.00076 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37433E-01 9.6E-05  1.49838E+00 0.00029 ];
INF_CAPT                  (idx, [1:   4]) = [  6.41640E-03 0.00063  2.91983E-02 0.00030 ];
INF_ABS                   (idx, [1:   4]) = [  8.69603E-03 0.00049  6.40709E-02 0.00058 ];
INF_FISS                  (idx, [1:   4]) = [  2.27964E-03 0.00038  3.48726E-02 0.00083 ];
INF_NSF                   (idx, [1:   4]) = [  5.84408E-03 0.00039  8.53383E-02 0.00083 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.56360E+00 4.5E-05  2.44715E+00 4.1E-06 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03852E+02 4.4E-06  2.02406E+02 6.6E-07 ];
INF_INVV                  (idx, [1:   4]) = [  5.89119E-08 0.00049  2.53870E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28740E-01 9.7E-05  1.43429E+00 0.00032 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43394E-01 0.00016  3.80051E-01 0.00038 ];
INF_SCATT2                (idx, [1:   4]) = [  9.60121E-02 0.00026  9.05709E-02 0.00082 ];
INF_SCATT3                (idx, [1:   4]) = [  7.37561E-03 0.00310  2.72189E-02 0.00234 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02201E-02 0.00189 -8.19998E-03 0.00716 ];
INF_SCATT5                (idx, [1:   4]) = [  1.89807E-04 0.09766  6.25581E-03 0.00798 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08193E-03 0.00316 -1.60825E-02 0.00294 ];
INF_SCATT7                (idx, [1:   4]) = [  7.60706E-04 0.01724  3.20899E-04 0.11161 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28780E-01 9.7E-05  1.43429E+00 0.00032 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43395E-01 0.00016  3.80051E-01 0.00038 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.60120E-02 0.00026  9.05709E-02 0.00082 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.37557E-03 0.00310  2.72189E-02 0.00234 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02202E-02 0.00189 -8.19998E-03 0.00716 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.89657E-04 0.09814  6.25581E-03 0.00798 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08190E-03 0.00317 -1.60825E-02 0.00294 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.60630E-04 0.01725  3.20899E-04 0.11161 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13950E-01 0.00028  9.65548E-01 0.00025 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55800E+00 0.00028  3.45228E-01 0.00025 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.65697E-03 0.00051  6.40709E-02 0.00058 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69281E-02 0.00020  6.52380E-02 0.00058 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10505E-01 9.5E-05  1.82352E-02 0.00037  1.14109E-03 0.00459  1.43314E+00 0.00032 ];
INF_S1                    (idx, [1:   8]) = [  2.38062E-01 0.00016  5.33168E-03 0.00086  4.89824E-04 0.00881  3.79561E-01 0.00039 ];
INF_S2                    (idx, [1:   8]) = [  9.75852E-02 0.00025 -1.57312E-03 0.00207  2.65488E-04 0.01282  9.03054E-02 0.00082 ];
INF_S3                    (idx, [1:   8]) = [  9.25394E-03 0.00231 -1.87833E-03 0.00182  9.48417E-05 0.02296  2.71240E-02 0.00236 ];
INF_S4                    (idx, [1:   8]) = [ -9.59428E-03 0.00195 -6.25859E-04 0.00453 -2.44136E-07 1.00000 -8.19973E-03 0.00711 ];
INF_S5                    (idx, [1:   8]) = [  1.72027E-04 0.10417  1.77804E-05 0.17172 -3.70096E-05 0.04268  6.29282E-03 0.00798 ];
INF_S6                    (idx, [1:   8]) = [  5.22354E-03 0.00311 -1.41607E-04 0.01226 -4.53638E-05 0.02935 -1.60371E-02 0.00296 ];
INF_S7                    (idx, [1:   8]) = [  9.34776E-04 0.01340 -1.74070E-04 0.01167 -4.27958E-05 0.03336  3.63695E-04 0.09823 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10544E-01 9.5E-05  1.82352E-02 0.00037  1.14109E-03 0.00459  1.43314E+00 0.00032 ];
INF_SP1                   (idx, [1:   8]) = [  2.38063E-01 0.00016  5.33168E-03 0.00086  4.89824E-04 0.00881  3.79561E-01 0.00039 ];
INF_SP2                   (idx, [1:   8]) = [  9.75852E-02 0.00025 -1.57312E-03 0.00207  2.65488E-04 0.01282  9.03054E-02 0.00082 ];
INF_SP3                   (idx, [1:   8]) = [  9.25391E-03 0.00232 -1.87833E-03 0.00182  9.48417E-05 0.02296  2.71240E-02 0.00236 ];
INF_SP4                   (idx, [1:   8]) = [ -9.59437E-03 0.00195 -6.25859E-04 0.00453 -2.44136E-07 1.00000 -8.19973E-03 0.00711 ];
INF_SP5                   (idx, [1:   8]) = [  1.71876E-04 0.10469  1.77804E-05 0.17172 -3.70096E-05 0.04268  6.29282E-03 0.00798 ];
INF_SP6                   (idx, [1:   8]) = [  5.22351E-03 0.00311 -1.41607E-04 0.01226 -4.53638E-05 0.02935 -1.60371E-02 0.00296 ];
INF_SP7                   (idx, [1:   8]) = [  9.34700E-04 0.01341 -1.74070E-04 0.01167 -4.27958E-05 0.03336  3.63695E-04 0.09823 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32408E-01 0.00063  1.06543E+00 0.00454 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33844E-01 0.00071  1.12592E+00 0.00557 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33804E-01 0.00097  1.12928E+00 0.00624 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29635E-01 0.00102  9.60371E-01 0.00543 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43427E+00 0.00063  3.13021E-01 0.00460 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42547E+00 0.00071  2.96275E-01 0.00556 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42573E+00 0.00097  2.95454E-01 0.00636 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45161E+00 0.00103  3.47333E-01 0.00541 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.88373E-03 0.00789  2.00080E-04 0.04356  1.12511E-03 0.01921  1.04239E-03 0.01932  3.17212E-03 0.01134  1.00673E-03 0.02127  3.37314E-04 0.03298 ];
LAMBDA                    (idx, [1:  14]) = [  8.05002E-01 0.01753  1.24907E-02 4.4E-06  3.16251E-02 0.00034  1.10113E-01 0.00038  3.20643E-01 0.00030  1.34509E+00 0.00024  8.90360E+00 0.00213 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0007' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:45:38 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:07:56 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292338 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.92875E-01  1.00423E+00  9.99894E-01  1.00192E+00  1.00108E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14691E-02 0.00100  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88531E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.96936E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.97370E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.70900E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.55292E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.55217E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.02645E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.28132E-01 0.00103  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000346 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00007E+04 0.00071 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00007E+04 0.00071 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.09503E+02 ;
RUNNING_TIME              (idx, 1)        =  2.23040E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.82450E-01  4.82450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  4.60000E-02  7.43333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  2.16046E+01  3.40202E+00  2.65847E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.69250E-01  2.77500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  7.88333E-03  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  2.23038E+01  1.26702E+02 ];
CPU_USAGE                 (idx, 1)        = 4.90956 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99920E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64258E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.91527E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.85006E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.48068E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.59601E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.11447E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.31924E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.73859E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  3.52621E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.47491E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  9.97416E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.59901E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  2.52879E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  2.81501E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.26500E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.04320E+14 ;
I131_ACTIVITY             (idx, 1)        =  3.59904E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.09474E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.71607E+10 ;
CS137_ACTIVITY            (idx, 1)        =  1.34865E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.82910E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.61939E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.47379E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.15018E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.11356E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 3 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E+00  1.00009E+00 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.01551E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  1.23454E+16 0.00059  8.90344E-01 0.00024 ];
U238_FISS                 (idx, [1:   4]) = [  8.69900E+14 0.00276  6.27255E-02 0.00258 ];
PU239_FISS                (idx, [1:   4]) = [  6.46790E+14 0.00301  4.66450E-02 0.00294 ];
PU240_FISS                (idx, [1:   4]) = [  7.41374E+10 0.28553  5.34221E-06 0.28551 ];
PU241_FISS                (idx, [1:   4]) = [  1.38962E+12 0.06745  1.00141E-04 0.06747 ];
U235_CAPT                 (idx, [1:   4]) = [  2.68047E+15 0.00153  1.56899E-01 0.00144 ];
U238_CAPT                 (idx, [1:   4]) = [  7.95563E+15 0.00102  4.65633E-01 0.00065 ];
PU239_CAPT                (idx, [1:   4]) = [  3.64027E+14 0.00435  2.13059E-02 0.00429 ];
PU240_CAPT                (idx, [1:   4]) = [  3.34895E+13 0.01368  1.96017E-03 0.01365 ];
PU241_CAPT                (idx, [1:   4]) = [  4.29405E+11 0.11567  2.51663E-05 0.11567 ];
XE135_CAPT                (idx, [1:   4]) = [  7.15950E+14 0.00298  4.19112E-02 0.00299 ];
SM149_CAPT                (idx, [1:   4]) = [  1.47905E+14 0.00654  8.65834E-03 0.00655 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000346 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.50412E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000346 5.00750E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2739659 2.74360E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2223597 2.22680E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 37090 3.71062E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000346 5.00750E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -1.11759E-07 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 3.7E-09  4.50057E+05 3.7E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.43563E+16 1.2E-05  3.43563E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38492E+16 1.5E-06  1.38492E+16 1.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.70867E+16 0.00046  1.21723E+16 0.00047  4.91435E+15 0.00103 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.09359E+16 0.00025  2.60216E+16 0.00022  4.91435E+15 0.00103 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.11356E+16 0.00050  3.11356E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.48608E+18 0.00046  4.04051E+17 0.00046  1.08203E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.31093E+14 0.00524 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.11670E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.10750E+18 0.00057 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12398E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12398E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.68577E+00 0.00043 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.88177E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.72478E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24580E+00 0.00033 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95195E-01 3.1E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97371E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.11306E+00 0.00049 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.10480E+00 0.00049 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.48074E+00 1.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02830E+02 1.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.10467E+00 0.00051  1.09725E+00 0.00049  7.55652E-03 0.00818 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.10400E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.10358E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.10400E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.11226E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74836E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74840E+01 8.1E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.11895E-07 0.00329 ];
IMP_EALF                  (idx, [1:   2]) = [  5.10538E-07 0.00141 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.12162E-01 0.00279 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.12465E-01 0.00120 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.32482E-03 0.00557  1.76742E-04 0.03139  1.01831E-03 0.01335  9.80618E-04 0.01339  2.89906E-03 0.00798  9.42781E-04 0.01367  3.07315E-04 0.02480 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.01745E-01 0.01304  1.09917E-02 0.01653  3.16001E-02 0.00026  1.10210E-01 0.00030  3.20652E-01 0.00022  1.34525E+00 0.00016  8.50857E+00 0.00978 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.91661E-03 0.00786  2.08140E-04 0.04510  1.10347E-03 0.02005  1.06704E-03 0.01979  3.15918E-03 0.01169  1.04322E-03 0.02018  3.35562E-04 0.03591 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.02294E-01 0.01884  1.24906E-02 4.8E-06  3.15971E-02 0.00036  1.10193E-01 0.00042  3.20732E-01 0.00033  1.34504E+00 0.00023  8.89155E+00 0.00256 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.49036E-05 0.00110  2.48958E-05 0.00109  2.60171E-05 0.01099 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.75068E-05 0.00098  2.74982E-05 0.00098  2.87341E-05 0.01097 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.83448E-03 0.00822  1.96548E-04 0.04764  1.11455E-03 0.02046  1.02381E-03 0.02045  3.12880E-03 0.01244  1.03491E-03 0.02033  3.35860E-04 0.03800 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.06615E-01 0.02037  1.24907E-02 4.7E-06  3.15993E-02 0.00040  1.10278E-01 0.00052  3.20622E-01 0.00035  1.34545E+00 0.00026  8.89735E+00 0.00287 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.47428E-05 0.00252  2.47422E-05 0.00251  2.46213E-05 0.02763 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.73286E-05 0.00245  2.73280E-05 0.00245  2.71946E-05 0.02764 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.48185E-03 0.02618  1.61183E-04 0.16158  1.01622E-03 0.06440  9.03345E-04 0.06719  3.10790E-03 0.03917  9.29045E-04 0.06692  3.64160E-04 0.10722 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.24127E-01 0.05882  1.24906E-02 1.4E-05  3.16161E-02 0.00098  1.10183E-01 0.00122  3.21102E-01 0.00104  1.34487E+00 0.00067  8.94253E+00 0.00571 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.48326E-03 0.02539  1.66900E-04 0.15353  1.01151E-03 0.06326  9.15355E-04 0.06512  3.08930E-03 0.03873  9.27471E-04 0.06680  3.72720E-04 0.10878 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.33804E-01 0.05869  1.24906E-02 1.4E-05  3.16184E-02 0.00097  1.10210E-01 0.00122  3.21211E-01 0.00105  1.34486E+00 0.00066  8.95052E+00 0.00573 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.63063E+02 0.02632 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.47876E-05 0.00070 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.73785E-05 0.00047 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.67263E-03 0.00501 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.69258E+02 0.00506 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.93624E-07 0.00066 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87926E-06 0.00045  2.87895E-06 0.00044  2.91757E-06 0.00485 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.90003E-05 0.00080  3.90211E-05 0.00080  3.60821E-05 0.00820 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.70535E-01 0.00032  6.69971E-01 0.00032  7.71060E-01 0.00847 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03497E+01 0.01303 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.55217E+01 0.00043  3.46801E+01 0.00041 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.25276E+04 0.00340  2.92350E+05 0.00156  6.04633E+05 0.00115  6.50712E+05 0.00086  5.98975E+05 0.00061  6.44353E+05 0.00092  4.37863E+05 0.00070  3.87144E+05 0.00084  2.96617E+05 0.00090  2.41883E+05 0.00077  2.08643E+05 0.00093  1.87941E+05 0.00094  1.73588E+05 0.00089  1.65411E+05 0.00088  1.60843E+05 0.00114  1.38772E+05 0.00103  1.37035E+05 0.00098  1.36016E+05 0.00096  1.33603E+05 0.00097  2.60185E+05 0.00060  2.51167E+05 0.00066  1.81307E+05 0.00079  1.17312E+05 0.00110  1.35226E+05 0.00080  1.27919E+05 0.00092  1.16117E+05 0.00129  1.90682E+05 0.00093  4.36831E+04 0.00158  5.47159E+04 0.00153  4.96304E+04 0.00171  2.88075E+04 0.00220  5.03214E+04 0.00169  3.42329E+04 0.00119  2.90730E+04 0.00150  5.54207E+03 0.00328  5.47331E+03 0.00305  5.60172E+03 0.00371  5.77291E+03 0.00436  5.68355E+03 0.00385  5.63403E+03 0.00420  5.87669E+03 0.00299  5.49634E+03 0.00325  1.04411E+04 0.00218  1.66208E+04 0.00206  2.12241E+04 0.00160  5.58771E+04 0.00168  5.88352E+04 0.00175  6.40189E+04 0.00111  4.33709E+04 0.00162  3.22089E+04 0.00158  2.46967E+04 0.00219  2.91598E+04 0.00161  5.55249E+04 0.00141  7.54361E+04 0.00137  1.45157E+05 0.00127  2.22500E+05 0.00122  3.29971E+05 0.00111  2.09825E+05 0.00149  1.49952E+05 0.00163  1.07493E+05 0.00156  9.64964E+04 0.00147  9.48537E+04 0.00133  7.91414E+04 0.00149  5.33996E+04 0.00124  4.92860E+04 0.00109  4.38195E+04 0.00154  3.70296E+04 0.00150  2.91006E+04 0.00214  1.94426E+04 0.00230  6.84969E+03 0.00286 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.11183E+00 0.00059 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.16580E+18 0.00057  3.20318E+17 0.00120 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37376E-01 0.00014  1.50180E+00 0.00041 ];
INF_CAPT                  (idx, [1:   4]) = [  6.45965E-03 0.00069  2.98359E-02 0.00045 ];
INF_ABS                   (idx, [1:   4]) = [  8.72300E-03 0.00057  6.48406E-02 0.00087 ];
INF_FISS                  (idx, [1:   4]) = [  2.26335E-03 0.00055  3.50047E-02 0.00123 ];
INF_NSF                   (idx, [1:   4]) = [  5.81362E-03 0.00055  8.61138E-02 0.00123 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.56859E+00 6.3E-05  2.46006E+00 1.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03912E+02 7.0E-06  2.02575E+02 1.7E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.88430E-08 0.00046  2.54006E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28654E-01 0.00014  1.43691E+00 0.00047 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43385E-01 0.00017  3.80777E-01 0.00055 ];
INF_SCATT2                (idx, [1:   4]) = [  9.59776E-02 0.00028  9.06258E-02 0.00082 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34557E-03 0.00276  2.73106E-02 0.00198 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02586E-02 0.00235 -8.26332E-03 0.00643 ];
INF_SCATT5                (idx, [1:   4]) = [  1.65938E-04 0.11685  6.24278E-03 0.00831 ];
INF_SCATT6                (idx, [1:   4]) = [  5.05612E-03 0.00359 -1.61033E-02 0.00300 ];
INF_SCATT7                (idx, [1:   4]) = [  7.51251E-04 0.01968  2.82094E-04 0.17631 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28694E-01 0.00014  1.43691E+00 0.00047 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43386E-01 0.00017  3.80777E-01 0.00055 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.59777E-02 0.00028  9.06258E-02 0.00082 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34541E-03 0.00276  2.73106E-02 0.00198 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02587E-02 0.00236 -8.26332E-03 0.00643 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.66063E-04 0.11622  6.24278E-03 0.00831 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.05616E-03 0.00359 -1.61033E-02 0.00300 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.51309E-04 0.01966  2.82094E-04 0.17631 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13775E-01 0.00037  9.68472E-01 0.00032 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55928E+00 0.00037  3.44186E-01 0.00032 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.68292E-03 0.00057  6.48406E-02 0.00087 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69387E-02 0.00026  6.60456E-02 0.00103 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10437E-01 0.00014  1.82163E-02 0.00042  1.16268E-03 0.00402  1.43575E+00 0.00047 ];
INF_S1                    (idx, [1:   8]) = [  2.38063E-01 0.00017  5.32211E-03 0.00081  4.93505E-04 0.00668  3.80284E-01 0.00055 ];
INF_S2                    (idx, [1:   8]) = [  9.75472E-02 0.00027 -1.56957E-03 0.00172  2.68764E-04 0.00998  9.03570E-02 0.00082 ];
INF_S3                    (idx, [1:   8]) = [  9.21748E-03 0.00206 -1.87191E-03 0.00167  9.57917E-05 0.02068  2.72149E-02 0.00199 ];
INF_S4                    (idx, [1:   8]) = [ -9.63331E-03 0.00251 -6.25297E-04 0.00277  2.27342E-06 0.80699 -8.26559E-03 0.00643 ];
INF_S5                    (idx, [1:   8]) = [  1.52935E-04 0.13367  1.30034E-05 0.20364 -3.77519E-05 0.04184  6.28053E-03 0.00830 ];
INF_S6                    (idx, [1:   8]) = [  5.20421E-03 0.00338 -1.48085E-04 0.02093 -4.93004E-05 0.03103 -1.60540E-02 0.00299 ];
INF_S7                    (idx, [1:   8]) = [  9.23365E-04 0.01645 -1.72114E-04 0.01426 -4.38658E-05 0.03388  3.25960E-04 0.15085 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10477E-01 0.00014  1.82163E-02 0.00042  1.16268E-03 0.00402  1.43575E+00 0.00047 ];
INF_SP1                   (idx, [1:   8]) = [  2.38064E-01 0.00017  5.32211E-03 0.00081  4.93505E-04 0.00668  3.80284E-01 0.00055 ];
INF_SP2                   (idx, [1:   8]) = [  9.75473E-02 0.00027 -1.56957E-03 0.00172  2.68764E-04 0.00998  9.03570E-02 0.00082 ];
INF_SP3                   (idx, [1:   8]) = [  9.21732E-03 0.00206 -1.87191E-03 0.00167  9.57917E-05 0.02068  2.72149E-02 0.00199 ];
INF_SP4                   (idx, [1:   8]) = [ -9.63339E-03 0.00251 -6.25297E-04 0.00277  2.27342E-06 0.80699 -8.26559E-03 0.00643 ];
INF_SP5                   (idx, [1:   8]) = [  1.53060E-04 0.13296  1.30034E-05 0.20364 -3.77519E-05 0.04184  6.28053E-03 0.00830 ];
INF_SP6                   (idx, [1:   8]) = [  5.20424E-03 0.00338 -1.48085E-04 0.02093 -4.93004E-05 0.03103 -1.60540E-02 0.00299 ];
INF_SP7                   (idx, [1:   8]) = [  9.23423E-04 0.01643 -1.72114E-04 0.01426 -4.38658E-05 0.03388  3.25960E-04 0.15085 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32026E-01 0.00071  1.07892E+00 0.00566 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33539E-01 0.00101  1.14842E+00 0.00877 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33650E-01 0.00096  1.14610E+00 0.00610 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28960E-01 0.00117  9.65207E-01 0.00618 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43664E+00 0.00071  3.09188E-01 0.00561 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42735E+00 0.00101  2.90788E-01 0.00876 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42667E+00 0.00096  2.91109E-01 0.00627 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45591E+00 0.00118  3.45665E-01 0.00617 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.91661E-03 0.00786  2.08140E-04 0.04510  1.10347E-03 0.02005  1.06704E-03 0.01979  3.15918E-03 0.01169  1.04322E-03 0.02018  3.35562E-04 0.03591 ];
LAMBDA                    (idx, [1:  14]) = [  8.02294E-01 0.01884  1.24906E-02 4.8E-06  3.15971E-02 0.00036  1.10193E-01 0.00042  3.20732E-01 0.00033  1.34504E+00 0.00023  8.89155E+00 0.00256 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0007' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:45:38 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:14:02 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292338 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.92057E-01  1.00348E+00  1.00018E+00  1.00209E+00  1.00218E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14200E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88580E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.98704E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.99136E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.70086E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.53875E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.53799E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.89230E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.22810E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000572 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00011E+04 0.00074 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00011E+04 0.00074 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.39958E+02 ;
RUNNING_TIME              (idx, 1)        =  2.84010E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.82450E-01  4.82450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  6.29833E-02  8.03334E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  2.76278E+01  3.37982E+00  2.64337E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  2.25667E-01  2.75833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  9.75000E-03  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  2.84008E+01  1.26267E+02 ];
CPU_USAGE                 (idx, 1)        = 4.92794 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00010E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.68796E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.07861E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.85086E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.70284E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.60789E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.12256E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.47070E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.73858E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  4.72050E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.83654E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.26461E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.66361E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  3.45588E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.17018E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.48596E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.12936E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.07817E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.19775E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.59602E+11 ;
CS137_ACTIVITY            (idx, 1)        =  2.69934E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.94993E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.58298E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.22447E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.17714E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.16381E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 4 ;
BURNUP                     (idx, [1:  2])  = [  2.00000E+00  2.00018E+00 ];
BURN_DAYS                 (idx, 1)        =  5.00000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.05263E-01 0.00103 ];
U235_FISS                 (idx, [1:   4]) = [  1.16866E+16 0.00065  8.44696E-01 0.00029 ];
U238_FISS                 (idx, [1:   4]) = [  8.80851E+14 0.00291  6.36617E-02 0.00280 ];
PU239_FISS                (idx, [1:   4]) = [  1.25467E+15 0.00229  9.06886E-02 0.00224 ];
PU240_FISS                (idx, [1:   4]) = [  2.65851E+11 0.14786  1.91972E-05 0.14786 ];
PU241_FISS                (idx, [1:   4]) = [  9.53266E+12 0.02462  6.89163E-04 0.02463 ];
U235_CAPT                 (idx, [1:   4]) = [  2.54424E+15 0.00158  1.44478E-01 0.00148 ];
U238_CAPT                 (idx, [1:   4]) = [  8.00703E+15 0.00099  4.54668E-01 0.00069 ];
PU239_CAPT                (idx, [1:   4]) = [  7.01578E+14 0.00303  3.98409E-02 0.00299 ];
PU240_CAPT                (idx, [1:   4]) = [  1.24297E+14 0.00694  7.05855E-03 0.00694 ];
PU241_CAPT                (idx, [1:   4]) = [  3.46811E+12 0.04179  1.96893E-04 0.04180 ];
XE135_CAPT                (idx, [1:   4]) = [  7.19435E+14 0.00277  4.08564E-02 0.00275 ];
SM149_CAPT                (idx, [1:   4]) = [  1.57441E+14 0.00636  8.94059E-03 0.00633 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000572 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.56479E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000572 5.00756E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2779139 2.78308E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2183539 2.18658E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 37894 3.79041E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000572 5.00756E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.93601E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 3.7E-09  4.50057E+05 3.7E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.45849E+16 1.3E-05  3.45849E+16 1.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38317E+16 1.8E-06  1.38317E+16 1.8E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.76193E+16 0.00047  1.26619E+16 0.00044  4.95733E+15 0.00110 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.14510E+16 0.00027  2.64936E+16 0.00021  4.95733E+15 0.00110 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.16381E+16 0.00050  3.16381E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.50528E+18 0.00049  4.07961E+17 0.00043  1.09732E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.39879E+14 0.00538 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.16908E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.12092E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12281E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12281E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.67262E+00 0.00043 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.89233E-01 0.00030 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.69774E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24628E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95118E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97288E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.10184E+00 0.00049 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.09349E+00 0.00049 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.50040E+00 1.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03087E+02 1.8E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.09347E+00 0.00052  1.08634E+00 0.00050  7.14924E-03 0.00857 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.09298E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.09328E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.09298E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.10133E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74407E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74434E+01 8.0E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.34498E-07 0.00351 ];
IMP_EALF                  (idx, [1:   2]) = [  5.31716E-07 0.00139 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.16263E-01 0.00293 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.15982E-01 0.00121 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.13893E-03 0.00576  1.87841E-04 0.03183  9.90165E-04 0.01418  9.52452E-04 0.01378  2.78743E-03 0.00830  9.17250E-04 0.01461  3.03784E-04 0.02693 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.11278E-01 0.01399  1.11164E-02 0.01574  3.15146E-02 0.00030  1.09941E-01 0.00203  3.21058E-01 0.00023  1.34408E+00 0.00026  8.54879E+00 0.01025 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.62376E-03 0.00831  2.17180E-04 0.04713  1.07851E-03 0.01992  1.01986E-03 0.01925  2.97257E-03 0.01219  1.01255E-03 0.02106  3.23086E-04 0.03803 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.04714E-01 0.01943  1.24904E-02 7.2E-06  3.14987E-02 0.00044  1.10105E-01 0.00042  3.21237E-01 0.00034  1.34425E+00 0.00027  8.97444E+00 0.00228 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.46399E-05 0.00117  2.46269E-05 0.00118  2.65377E-05 0.01233 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.69390E-05 0.00103  2.69249E-05 0.00104  2.90104E-05 0.01230 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.52917E-03 0.00873  1.94934E-04 0.04822  1.05008E-03 0.02011  1.00988E-03 0.01985  2.95140E-03 0.01308  9.96853E-04 0.02196  3.26033E-04 0.03782 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.16949E-01 0.02076  1.24904E-02 9.7E-06  3.15112E-02 0.00049  1.10094E-01 0.00052  3.21277E-01 0.00038  1.34401E+00 0.00038  8.94957E+00 0.00266 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.43547E-05 0.00268  2.43445E-05 0.00269  2.55796E-05 0.02889 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.66263E-05 0.00260  2.66151E-05 0.00260  2.79655E-05 0.02884 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.69158E-03 0.02468  2.42111E-04 0.14823  9.71831E-04 0.06498  1.06133E-03 0.06397  3.02336E-03 0.04000  1.07285E-03 0.06722  3.20095E-04 0.11446 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.23730E-01 0.06243  1.24905E-02 2.1E-05  3.15316E-02 0.00115  1.10146E-01 0.00120  3.21141E-01 0.00109  1.34485E+00 0.00062  8.94293E+00 0.00583 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.74761E-03 0.02439  2.55564E-04 0.14550  1.00740E-03 0.06460  1.05604E-03 0.06326  3.03805E-03 0.03930  1.08077E-03 0.06527  3.09793E-04 0.10983 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.10203E-01 0.06030  1.24905E-02 2.1E-05  3.15319E-02 0.00115  1.10141E-01 0.00119  3.21187E-01 0.00108  1.34488E+00 0.00062  8.94353E+00 0.00581 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.76721E+02 0.02504 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.44903E-05 0.00072 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.67758E-05 0.00051 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.68673E-03 0.00531 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.73099E+02 0.00534 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.87920E-07 0.00063 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.86624E-06 0.00045  2.86606E-06 0.00045  2.89151E-06 0.00515 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.86201E-05 0.00079  3.86403E-05 0.00079  3.57399E-05 0.00898 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.67830E-01 0.00032  6.67341E-01 0.00032  7.59708E-01 0.00916 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04928E+01 0.01365 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.53799E+01 0.00044  3.44082E+01 0.00046 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.28878E+04 0.00332  2.93937E+05 0.00171  6.04965E+05 0.00092  6.51471E+05 0.00076  5.99408E+05 0.00066  6.44203E+05 0.00059  4.37246E+05 0.00075  3.87151E+05 0.00058  2.96197E+05 0.00060  2.41961E+05 0.00076  2.08414E+05 0.00084  1.88114E+05 0.00073  1.73516E+05 0.00070  1.64874E+05 0.00060  1.60958E+05 0.00095  1.38969E+05 0.00085  1.37087E+05 0.00087  1.36050E+05 0.00121  1.33696E+05 0.00118  2.60652E+05 0.00072  2.51201E+05 0.00077  1.81311E+05 0.00044  1.17412E+05 0.00110  1.35404E+05 0.00083  1.27981E+05 0.00086  1.16084E+05 0.00107  1.90662E+05 0.00057  4.35679E+04 0.00189  5.47131E+04 0.00161  4.96850E+04 0.00175  2.87997E+04 0.00211  5.00375E+04 0.00157  3.40252E+04 0.00172  2.90151E+04 0.00171  5.47434E+03 0.00285  5.43493E+03 0.00315  5.44815E+03 0.00300  5.54251E+03 0.00377  5.55090E+03 0.00324  5.50940E+03 0.00371  5.76945E+03 0.00331  5.46429E+03 0.00353  1.02979E+04 0.00205  1.64735E+04 0.00292  2.11384E+04 0.00287  5.55734E+04 0.00162  5.83356E+04 0.00197  6.33345E+04 0.00121  4.26167E+04 0.00161  3.12712E+04 0.00164  2.38244E+04 0.00165  2.80494E+04 0.00154  5.37600E+04 0.00126  7.36071E+04 0.00124  1.42291E+05 0.00100  2.18940E+05 0.00090  3.25343E+05 0.00107  2.07152E+05 0.00133  1.47966E+05 0.00151  1.06148E+05 0.00121  9.52138E+04 0.00150  9.38117E+04 0.00126  7.82971E+04 0.00148  5.28318E+04 0.00185  4.87981E+04 0.00153  4.33976E+04 0.00181  3.66033E+04 0.00155  2.87955E+04 0.00163  1.92781E+04 0.00167  6.81293E+03 0.00249 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.10163E+00 0.00050 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.18474E+18 0.00044  3.20570E+17 0.00114 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37411E-01 0.00010  1.51056E+00 0.00040 ];
INF_CAPT                  (idx, [1:   4]) = [  6.55610E-03 0.00059  3.07353E-02 0.00046 ];
INF_ABS                   (idx, [1:   4]) = [  8.78771E-03 0.00046  6.56414E-02 0.00085 ];
INF_FISS                  (idx, [1:   4]) = [  2.23161E-03 0.00036  3.49061E-02 0.00119 ];
INF_NSF                   (idx, [1:   4]) = [  5.75070E-03 0.00037  8.66481E-02 0.00120 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.57693E+00 4.7E-05  2.48232E+00 1.6E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04019E+02 5.1E-06  2.02866E+02 2.5E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.86374E-08 0.00041  2.54411E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28618E-01 0.00010  1.44496E+00 0.00045 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43461E-01 0.00020  3.83069E-01 0.00061 ];
INF_SCATT2                (idx, [1:   4]) = [  9.60072E-02 0.00031  9.11334E-02 0.00116 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34971E-03 0.00222  2.73589E-02 0.00186 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02964E-02 0.00168 -8.34895E-03 0.00603 ];
INF_SCATT5                (idx, [1:   4]) = [  1.31510E-04 0.13127  6.36261E-03 0.00886 ];
INF_SCATT6                (idx, [1:   4]) = [  5.05568E-03 0.00331 -1.62596E-02 0.00291 ];
INF_SCATT7                (idx, [1:   4]) = [  7.42098E-04 0.02325  3.30757E-04 0.16642 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28659E-01 0.00010  1.44496E+00 0.00045 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43463E-01 0.00020  3.83069E-01 0.00061 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.60072E-02 0.00031  9.11334E-02 0.00116 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34957E-03 0.00220  2.73589E-02 0.00186 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02964E-02 0.00168 -8.34895E-03 0.00603 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.31411E-04 0.13153  6.36261E-03 0.00886 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.05588E-03 0.00330 -1.62596E-02 0.00291 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.42152E-04 0.02330  3.30757E-04 0.16642 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13656E-01 0.00026  9.74959E-01 0.00034 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56014E+00 0.00026  3.41896E-01 0.00034 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.74731E-03 0.00047  6.56414E-02 0.00085 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69309E-02 0.00019  6.67708E-02 0.00094 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10480E-01 0.00010  1.81387E-02 0.00033  1.16850E-03 0.00572  1.44379E+00 0.00045 ];
INF_S1                    (idx, [1:   8]) = [  2.38162E-01 0.00020  5.29937E-03 0.00114  4.94753E-04 0.01036  3.82575E-01 0.00061 ];
INF_S2                    (idx, [1:   8]) = [  9.75744E-02 0.00028 -1.56728E-03 0.00284  2.71370E-04 0.01293  9.08620E-02 0.00116 ];
INF_S3                    (idx, [1:   8]) = [  9.21577E-03 0.00175 -1.86606E-03 0.00175  9.91471E-05 0.02522  2.72597E-02 0.00188 ];
INF_S4                    (idx, [1:   8]) = [ -9.67270E-03 0.00179 -6.23669E-04 0.00426  4.42680E-06 0.47762 -8.35337E-03 0.00603 ];
INF_S5                    (idx, [1:   8]) = [  1.17114E-04 0.13492  1.43962E-05 0.22940 -3.79436E-05 0.03689  6.40055E-03 0.00882 ];
INF_S6                    (idx, [1:   8]) = [  5.20250E-03 0.00306 -1.46817E-04 0.01337 -4.95453E-05 0.02754 -1.62101E-02 0.00293 ];
INF_S7                    (idx, [1:   8]) = [  9.13795E-04 0.01867 -1.71697E-04 0.01182 -4.81428E-05 0.03050  3.78900E-04 0.14572 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10520E-01 0.00010  1.81387E-02 0.00033  1.16850E-03 0.00572  1.44379E+00 0.00045 ];
INF_SP1                   (idx, [1:   8]) = [  2.38163E-01 0.00019  5.29937E-03 0.00114  4.94753E-04 0.01036  3.82575E-01 0.00061 ];
INF_SP2                   (idx, [1:   8]) = [  9.75745E-02 0.00028 -1.56728E-03 0.00284  2.71370E-04 0.01293  9.08620E-02 0.00116 ];
INF_SP3                   (idx, [1:   8]) = [  9.21562E-03 0.00174 -1.86606E-03 0.00175  9.91471E-05 0.02522  2.72597E-02 0.00188 ];
INF_SP4                   (idx, [1:   8]) = [ -9.67269E-03 0.00179 -6.23669E-04 0.00426  4.42680E-06 0.47762 -8.35337E-03 0.00603 ];
INF_SP5                   (idx, [1:   8]) = [  1.17015E-04 0.13525  1.43962E-05 0.22940 -3.79436E-05 0.03689  6.40055E-03 0.00882 ];
INF_SP6                   (idx, [1:   8]) = [  5.20269E-03 0.00305 -1.46817E-04 0.01337 -4.95453E-05 0.02754 -1.62101E-02 0.00293 ];
INF_SP7                   (idx, [1:   8]) = [  9.13849E-04 0.01871 -1.71697E-04 0.01182 -4.81428E-05 0.03050  3.78900E-04 0.14572 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32213E-01 0.00050  1.07815E+00 0.00442 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33546E-01 0.00089  1.14925E+00 0.00613 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33888E-01 0.00105  1.15020E+00 0.00641 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29270E-01 0.00054  9.59627E-01 0.00483 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43547E+00 0.00050  3.09315E-01 0.00442 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42730E+00 0.00090  2.90303E-01 0.00604 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42522E+00 0.00105  2.90090E-01 0.00640 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45390E+00 0.00054  3.47553E-01 0.00486 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.62376E-03 0.00831  2.17180E-04 0.04713  1.07851E-03 0.01992  1.01986E-03 0.01925  2.97257E-03 0.01219  1.01255E-03 0.02106  3.23086E-04 0.03803 ];
LAMBDA                    (idx, [1:  14]) = [  8.04714E-01 0.01943  1.24904E-02 7.2E-06  3.14987E-02 0.00044  1.10105E-01 0.00042  3.21237E-01 0.00034  1.34425E+00 0.00027  8.97444E+00 0.00228 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0007' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:45:38 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:20:07 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292338 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.92366E-01  1.00452E+00  1.00056E+00  1.00141E+00  1.00115E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.1E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14083E-02 0.00115  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88592E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.99965E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.00396E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69435E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.52288E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.52211E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.78294E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.19592E-01 0.00117  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000570 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00011E+04 0.00075 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00011E+04 0.00075 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.70332E+02 ;
RUNNING_TIME              (idx, 1)        =  3.44820E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.82450E-01  4.82450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  8.10500E-02  8.96667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.36348E+01  3.37312E+00  2.63395E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  2.80983E-01  2.79667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.29833E-02  7.83332E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.44819E+01  1.25796E+02 ];
CPU_USAGE                 (idx, 1)        = 4.93974 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99902E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.71709E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.14760E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.83686E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.15419E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.61998E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.13088E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.52759E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.72375E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  5.69034E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.99439E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.61017E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.72697E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.08017E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.32170E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.66805E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.15950E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.17326E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.23979E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.49265E+11 ;
CS137_ACTIVITY            (idx, 1)        =  4.04973E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.99206E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.54912E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.28407E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.18979E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.21641E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 5 ;
BURNUP                     (idx, [1:  2])  = [  3.00000E+00  3.00026E+00 ];
BURN_DAYS                 (idx, 1)        =  7.50000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.12221E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  1.11230E+16 0.00066  8.05401E-01 0.00034 ];
U238_FISS                 (idx, [1:   4]) = [  8.95480E+14 0.00275  6.48325E-02 0.00260 ];
PU239_FISS                (idx, [1:   4]) = [  1.76063E+15 0.00190  1.27484E-01 0.00181 ];
PU240_FISS                (idx, [1:   4]) = [  5.84851E+11 0.10233  4.23840E-05 0.10250 ];
PU241_FISS                (idx, [1:   4]) = [  2.68079E+13 0.01593  1.94104E-03 0.01591 ];
U235_CAPT                 (idx, [1:   4]) = [  2.43815E+15 0.00163  1.34323E-01 0.00158 ];
U238_CAPT                 (idx, [1:   4]) = [  8.07708E+15 0.00100  4.44941E-01 0.00066 ];
PU239_CAPT                (idx, [1:   4]) = [  9.86445E+14 0.00273  5.43395E-02 0.00262 ];
PU240_CAPT                (idx, [1:   4]) = [  2.41299E+14 0.00538  1.32921E-02 0.00533 ];
PU241_CAPT                (idx, [1:   4]) = [  9.99175E+12 0.02411  5.50327E-04 0.02403 ];
XE135_CAPT                (idx, [1:   4]) = [  7.24127E+14 0.00290  3.98910E-02 0.00282 ];
SM149_CAPT                (idx, [1:   4]) = [  1.60443E+14 0.00669  8.83991E-03 0.00671 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000570 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.57713E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000570 5.00758E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2817775 2.82186E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2144077 2.14700E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 38718 3.87234E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000570 5.00758E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.23986E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 3.7E-09  4.50057E+05 3.7E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.47822E+16 1.4E-05  3.47822E+16 1.4E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38165E+16 2.1E-06  1.38165E+16 2.1E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.81628E+16 0.00046  1.31646E+16 0.00045  4.99821E+15 0.00110 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.19793E+16 0.00026  2.69811E+16 0.00022  4.99821E+15 0.00110 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.21641E+16 0.00050  3.21641E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.52540E+18 0.00047  4.12877E+17 0.00045  1.11253E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.49140E+14 0.00521 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.22284E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.13445E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12164E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12164E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.66050E+00 0.00045 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.90371E-01 0.00030 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.66580E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24538E+00 0.00033 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94989E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97253E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.08944E+00 0.00053 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.08100E+00 0.00054 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.51744E+00 1.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03310E+02 2.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.08100E+00 0.00055  1.07403E+00 0.00054  6.96766E-03 0.00860 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.08090E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.08153E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.08090E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.08933E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74043E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74049E+01 8.1E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.54110E-07 0.00325 ];
IMP_EALF                  (idx, [1:   2]) = [  5.52605E-07 0.00141 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.20630E-01 0.00266 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.20642E-01 0.00112 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.10610E-03 0.00595  1.76040E-04 0.03349  9.81822E-04 0.01323  9.66905E-04 0.01396  2.76984E-03 0.00838  9.12583E-04 0.01410  2.98911E-04 0.02443 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.07249E-01 0.01313  1.06417E-02 0.01866  3.14701E-02 0.00033  1.10094E-01 0.00032  3.21056E-01 0.00024  1.34249E+00 0.00036  8.65069E+00 0.00840 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.56752E-03 0.00838  1.82123E-04 0.05116  1.07380E-03 0.01986  1.05056E-03 0.02100  2.97934E-03 0.01168  9.55092E-04 0.02030  3.26609E-04 0.03887 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.09549E-01 0.02060  1.24902E-02 8.2E-06  3.14669E-02 0.00045  1.10050E-01 0.00041  3.20914E-01 0.00035  1.34208E+00 0.00047  8.95178E+00 0.00241 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.44936E-05 0.00114  2.44805E-05 0.00115  2.64781E-05 0.01218 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.64736E-05 0.00099  2.64595E-05 0.00100  2.86169E-05 0.01216 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.43971E-03 0.00883  1.76003E-04 0.05574  1.02662E-03 0.02246  1.01918E-03 0.02211  2.92161E-03 0.01263  9.77506E-04 0.02184  3.18784E-04 0.03973 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.18178E-01 0.02152  1.24903E-02 9.3E-06  3.14679E-02 0.00055  1.09974E-01 0.00046  3.21000E-01 0.00036  1.34301E+00 0.00044  8.97802E+00 0.00294 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.42570E-05 0.00277  2.42505E-05 0.00279  2.46623E-05 0.02709 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.62168E-05 0.00268  2.62097E-05 0.00270  2.66679E-05 0.02712 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.31745E-03 0.02703  1.44987E-04 0.18584  9.54251E-04 0.06908  1.09919E-03 0.06858  2.82542E-03 0.04312  9.65695E-04 0.06785  3.27904E-04 0.11116 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.74382E-01 0.06366  1.24901E-02 2.2E-05  3.14723E-02 0.00131  1.09972E-01 0.00120  3.20998E-01 0.00110  1.34398E+00 0.00074  9.03497E+00 0.00661 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.28247E-03 0.02623  1.47719E-04 0.17846  9.73461E-04 0.06749  1.09070E-03 0.06655  2.79449E-03 0.04152  9.55260E-04 0.06490  3.20839E-04 0.11102 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.60618E-01 0.06414  1.24901E-02 2.2E-05  3.14775E-02 0.00129  1.09948E-01 0.00118  3.21032E-01 0.00109  1.34386E+00 0.00075  9.03262E+00 0.00656 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.61397E+02 0.02721 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.43672E-05 0.00076 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.63368E-05 0.00048 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.40679E-03 0.00545 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.62987E+02 0.00547 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.82524E-07 0.00068 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.84868E-06 0.00041  2.84853E-06 0.00041  2.87476E-06 0.00545 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.83121E-05 0.00086  3.83316E-05 0.00086  3.54879E-05 0.00924 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.64583E-01 0.00032  6.64138E-01 0.00032  7.50138E-01 0.00942 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04594E+01 0.01359 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.52211E+01 0.00046  3.41894E+01 0.00043 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.36842E+04 0.00284  2.95373E+05 0.00121  6.06470E+05 0.00095  6.51644E+05 0.00055  5.98867E+05 0.00070  6.43833E+05 0.00059  4.37151E+05 0.00072  3.87118E+05 0.00067  2.96395E+05 0.00084  2.42112E+05 0.00062  2.08490E+05 0.00085  1.87970E+05 0.00103  1.73648E+05 0.00088  1.64870E+05 0.00087  1.60645E+05 0.00085  1.38728E+05 0.00089  1.37166E+05 0.00096  1.35843E+05 0.00099  1.33459E+05 0.00119  2.60439E+05 0.00073  2.51155E+05 0.00068  1.81192E+05 0.00084  1.17446E+05 0.00089  1.35491E+05 0.00091  1.27980E+05 0.00078  1.15743E+05 0.00099  1.89930E+05 0.00090  4.35414E+04 0.00151  5.47379E+04 0.00125  4.97106E+04 0.00170  2.87162E+04 0.00179  5.00184E+04 0.00144  3.39619E+04 0.00190  2.88016E+04 0.00178  5.42847E+03 0.00341  5.26576E+03 0.00452  5.21860E+03 0.00411  5.25533E+03 0.00284  5.28459E+03 0.00400  5.37171E+03 0.00310  5.67535E+03 0.00370  5.39107E+03 0.00345  1.02124E+04 0.00264  1.63217E+04 0.00239  2.09034E+04 0.00207  5.51779E+04 0.00157  5.80200E+04 0.00135  6.27679E+04 0.00126  4.19421E+04 0.00167  3.04827E+04 0.00149  2.30853E+04 0.00141  2.72293E+04 0.00158  5.23384E+04 0.00142  7.18979E+04 0.00139  1.39592E+05 0.00135  2.15322E+05 0.00098  3.20488E+05 0.00117  2.04447E+05 0.00123  1.46299E+05 0.00132  1.04976E+05 0.00128  9.42298E+04 0.00143  9.28189E+04 0.00151  7.73788E+04 0.00133  5.22965E+04 0.00150  4.82712E+04 0.00164  4.28902E+04 0.00147  3.62909E+04 0.00178  2.85910E+04 0.00174  1.90893E+04 0.00186  6.75081E+03 0.00208 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.08997E+00 0.00038 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.20420E+18 0.00036  3.21237E+17 0.00098 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37370E-01 0.00011  1.51719E+00 0.00036 ];
INF_CAPT                  (idx, [1:   4]) = [  6.67889E-03 0.00061  3.15064E-02 0.00042 ];
INF_ABS                   (idx, [1:   4]) = [  8.87778E-03 0.00048  6.62799E-02 0.00070 ];
INF_FISS                  (idx, [1:   4]) = [  2.19889E-03 0.00040  3.47735E-02 0.00095 ];
INF_NSF                   (idx, [1:   4]) = [  5.68524E-03 0.00039  8.69790E-02 0.00096 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.58550E+00 4.4E-05  2.50130E+00 2.8E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04124E+02 4.2E-06  2.03117E+02 4.6E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.83608E-08 0.00046  2.54754E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28493E-01 0.00011  1.45096E+00 0.00041 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43345E-01 0.00020  3.84673E-01 0.00048 ];
INF_SCATT2                (idx, [1:   4]) = [  9.59618E-02 0.00024  9.15694E-02 0.00077 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35099E-03 0.00255  2.75765E-02 0.00203 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02500E-02 0.00187 -8.34650E-03 0.00628 ];
INF_SCATT5                (idx, [1:   4]) = [  1.63576E-04 0.11037  6.32741E-03 0.00858 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07030E-03 0.00373 -1.64407E-02 0.00280 ];
INF_SCATT7                (idx, [1:   4]) = [  7.28783E-04 0.02058  3.06201E-04 0.12954 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28533E-01 0.00011  1.45096E+00 0.00041 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43346E-01 0.00020  3.84673E-01 0.00048 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.59622E-02 0.00024  9.15694E-02 0.00077 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35078E-03 0.00255  2.75765E-02 0.00203 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02501E-02 0.00187 -8.34650E-03 0.00628 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.63549E-04 0.11043  6.32741E-03 0.00858 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07042E-03 0.00373 -1.64407E-02 0.00280 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.28720E-04 0.02059  3.06201E-04 0.12954 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13401E-01 0.00027  9.79902E-01 0.00032 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56201E+00 0.00027  3.40171E-01 0.00032 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.83730E-03 0.00047  6.62799E-02 0.00070 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69315E-02 0.00017  6.74065E-02 0.00084 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10438E-01 0.00011  1.80544E-02 0.00042  1.18043E-03 0.00358  1.44978E+00 0.00041 ];
INF_S1                    (idx, [1:   8]) = [  2.38075E-01 0.00019  5.26968E-03 0.00108  4.99703E-04 0.00611  3.84173E-01 0.00048 ];
INF_S2                    (idx, [1:   8]) = [  9.75326E-02 0.00025 -1.57082E-03 0.00234  2.71185E-04 0.00873  9.12982E-02 0.00076 ];
INF_S3                    (idx, [1:   8]) = [  9.20843E-03 0.00202 -1.85745E-03 0.00156  9.72400E-05 0.02071  2.74793E-02 0.00206 ];
INF_S4                    (idx, [1:   8]) = [ -9.63707E-03 0.00203 -6.12968E-04 0.00500 -1.81562E-06 1.00000 -8.34469E-03 0.00630 ];
INF_S5                    (idx, [1:   8]) = [  1.43106E-04 0.12126  2.04698E-05 0.13432 -4.06560E-05 0.04554  6.36806E-03 0.00849 ];
INF_S6                    (idx, [1:   8]) = [  5.21787E-03 0.00365 -1.47571E-04 0.01833 -4.96137E-05 0.02875 -1.63911E-02 0.00280 ];
INF_S7                    (idx, [1:   8]) = [  9.04255E-04 0.01670 -1.75471E-04 0.01132 -4.41366E-05 0.02538  3.50337E-04 0.11272 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10479E-01 0.00011  1.80544E-02 0.00042  1.18043E-03 0.00358  1.44978E+00 0.00041 ];
INF_SP1                   (idx, [1:   8]) = [  2.38076E-01 0.00019  5.26968E-03 0.00108  4.99703E-04 0.00611  3.84173E-01 0.00048 ];
INF_SP2                   (idx, [1:   8]) = [  9.75331E-02 0.00025 -1.57082E-03 0.00234  2.71185E-04 0.00873  9.12982E-02 0.00076 ];
INF_SP3                   (idx, [1:   8]) = [  9.20823E-03 0.00202 -1.85745E-03 0.00156  9.72400E-05 0.02071  2.74793E-02 0.00206 ];
INF_SP4                   (idx, [1:   8]) = [ -9.63709E-03 0.00203 -6.12968E-04 0.00500 -1.81562E-06 1.00000 -8.34469E-03 0.00630 ];
INF_SP5                   (idx, [1:   8]) = [  1.43079E-04 0.12132  2.04698E-05 0.13432 -4.06560E-05 0.04554  6.36806E-03 0.00849 ];
INF_SP6                   (idx, [1:   8]) = [  5.21799E-03 0.00365 -1.47571E-04 0.01833 -4.96137E-05 0.02875 -1.63911E-02 0.00280 ];
INF_SP7                   (idx, [1:   8]) = [  9.04192E-04 0.01671 -1.75471E-04 0.01132 -4.41366E-05 0.02538  3.50337E-04 0.11272 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31982E-01 0.00065  1.09389E+00 0.00542 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33474E-01 0.00106  1.16027E+00 0.00827 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33282E-01 0.00087  1.17380E+00 0.00641 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29245E-01 0.00080  9.73098E-01 0.00554 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43691E+00 0.00065  3.04936E-01 0.00534 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42775E+00 0.00106  2.87754E-01 0.00813 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42891E+00 0.00087  2.84257E-01 0.00637 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45407E+00 0.00080  3.42797E-01 0.00545 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.56752E-03 0.00838  1.82123E-04 0.05116  1.07380E-03 0.01986  1.05056E-03 0.02100  2.97934E-03 0.01168  9.55092E-04 0.02030  3.26609E-04 0.03887 ];
LAMBDA                    (idx, [1:  14]) = [  8.09549E-01 0.02060  1.24902E-02 8.2E-06  3.14669E-02 0.00045  1.10050E-01 0.00041  3.20914E-01 0.00035  1.34208E+00 0.00047  8.95178E+00 0.00241 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0007' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:45:38 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:26:11 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292338 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.92928E-01  1.00425E+00  1.00129E+00  1.00115E+00  1.00037E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13895E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88610E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.01484E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.01915E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68637E+00 0.00020  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.51590E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.51511E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.68235E+00 0.00036  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.16882E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000861 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00017E+04 0.00079 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00017E+04 0.00079 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.00685E+02 ;
RUNNING_TIME              (idx, 1)        =  4.05586E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.82450E-01  4.82450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  9.92667E-02  8.78333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.96373E+01  3.36568E+00  2.63675E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  3.36350E-01  2.65667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.48333E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.05585E+01  1.25686E+02 ];
CPU_USAGE                 (idx, 1)        = 4.94802 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00036E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.73748E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.19973E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.82431E+04 ;
TOT_SF_RATE               (idx, 1)        =  9.05300E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.63725E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.14284E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.56245E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.71000E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  6.60624E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.10043E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.04162E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.81304E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.56462E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.41912E+07 ;
SR90_ACTIVITY             (idx, 1)        =  4.81610E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.18655E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.21877E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.27699E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.21111E+12 ;
CS137_ACTIVITY            (idx, 1)        =  5.39956E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.02597E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.52066E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  8.00298E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.20017E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.27132E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 6 ;
BURNUP                     (idx, [1:  2])  = [  4.00000E+00  4.00035E+00 ];
BURN_DAYS                 (idx, 1)        =  1.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.21054E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  1.06394E+16 0.00070  7.71441E-01 0.00037 ];
U238_FISS                 (idx, [1:   4]) = [  9.12672E+14 0.00265  6.61673E-02 0.00248 ];
PU239_FISS                (idx, [1:   4]) = [  2.18062E+15 0.00170  1.58108E-01 0.00156 ];
PU240_FISS                (idx, [1:   4]) = [  1.06103E+12 0.07817  7.69743E-05 0.07828 ];
PU241_FISS                (idx, [1:   4]) = [  5.33115E+13 0.01161  3.86484E-03 0.01156 ];
U235_CAPT                 (idx, [1:   4]) = [  2.33292E+15 0.00157  1.24687E-01 0.00148 ];
U238_CAPT                 (idx, [1:   4]) = [  8.15096E+15 0.00099  4.35614E-01 0.00068 ];
PU239_CAPT                (idx, [1:   4]) = [  1.22607E+15 0.00235  6.55294E-02 0.00229 ];
PU240_CAPT                (idx, [1:   4]) = [  3.69148E+14 0.00427  1.97300E-02 0.00424 ];
PU241_CAPT                (idx, [1:   4]) = [  1.99388E+13 0.01885  1.06599E-03 0.01888 ];
XE135_CAPT                (idx, [1:   4]) = [  7.24923E+14 0.00300  3.87439E-02 0.00295 ];
SM149_CAPT                (idx, [1:   4]) = [  1.68095E+14 0.00612  8.98605E-03 0.00617 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000861 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.64537E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000861 5.00765E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2855870 2.85984E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2105243 2.10805E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 39748 3.97486E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000861 5.00765E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.86733E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 3.7E-09  4.50057E+05 3.7E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.49532E+16 1.5E-05  3.49532E+16 1.5E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38032E+16 2.4E-06  1.38032E+16 2.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.87043E+16 0.00046  1.36343E+16 0.00045  5.06997E+15 0.00111 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.25074E+16 0.00027  2.74375E+16 0.00022  5.06997E+15 0.00111 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.27132E+16 0.00049  3.27132E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.54774E+18 0.00047  4.17651E+17 0.00044  1.13009E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.60082E+14 0.00492 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.27675E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.15151E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12048E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12048E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.64906E+00 0.00046 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.89437E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.63770E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24544E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94838E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97198E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.07615E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06759E+00 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.53226E+00 1.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03506E+02 2.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06768E+00 0.00057  1.06088E+00 0.00056  6.71392E-03 0.00880 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06837E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06860E+00 0.00048 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06837E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.07693E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73686E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73722E+01 8.5E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.74333E-07 0.00334 ];
IMP_EALF                  (idx, [1:   2]) = [  5.70961E-07 0.00149 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.25021E-01 0.00270 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.24645E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.02579E-03 0.00559  1.69494E-04 0.03448  9.84178E-04 0.01386  9.24655E-04 0.01483  2.75573E-03 0.00814  9.11165E-04 0.01396  2.80561E-04 0.02785 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.92241E-01 0.01464  1.00676E-02 0.02196  3.14451E-02 0.00034  1.10237E-01 0.00035  3.21142E-01 0.00024  1.34294E+00 0.00030  8.38291E+00 0.01210 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.32561E-03 0.00849  1.82695E-04 0.05356  1.02442E-03 0.02182  9.65947E-04 0.02154  2.89542E-03 0.01205  9.60321E-04 0.02117  2.96807E-04 0.04009 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.92052E-01 0.02049  1.24915E-02 0.00013  3.14486E-02 0.00047  1.10259E-01 0.00050  3.21207E-01 0.00036  1.34263E+00 0.00060  8.94763E+00 0.00294 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.45086E-05 0.00122  2.44995E-05 0.00123  2.58425E-05 0.01205 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.61627E-05 0.00104  2.61530E-05 0.00105  2.75932E-05 0.01210 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.28607E-03 0.00894  1.85503E-04 0.05089  1.03121E-03 0.02195  9.56444E-04 0.02379  2.85170E-03 0.01268  9.59111E-04 0.02259  3.02103E-04 0.04282 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.99308E-01 0.02185  1.24920E-02 0.00016  3.14553E-02 0.00055  1.10294E-01 0.00062  3.21310E-01 0.00039  1.34198E+00 0.00062  8.95117E+00 0.00361 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.42146E-05 0.00276  2.42000E-05 0.00277  2.60448E-05 0.03702 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.58496E-05 0.00271  2.58340E-05 0.00272  2.78056E-05 0.03684 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.10178E-03 0.02990  1.46566E-04 0.18635  1.02251E-03 0.07466  9.13015E-04 0.07140  2.76487E-03 0.04397  9.38826E-04 0.07736  3.15991E-04 0.13466 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.39524E-01 0.07178  1.25097E-02 0.00156  3.13636E-02 0.00141  1.10457E-01 0.00157  3.21769E-01 0.00124  1.34204E+00 0.00165  8.91912E+00 0.00637 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.05755E-03 0.02932  1.48506E-04 0.17028  1.01661E-03 0.07142  9.14856E-04 0.06976  2.76411E-03 0.04349  9.12144E-04 0.07568  3.01322E-04 0.13127 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.30729E-01 0.06986  1.25097E-02 0.00156  3.13634E-02 0.00140  1.10476E-01 0.00158  3.21746E-01 0.00122  1.34204E+00 0.00165  8.91858E+00 0.00643 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.52190E+02 0.02986 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.43548E-05 0.00085 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.59985E-05 0.00058 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.19305E-03 0.00544 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.54404E+02 0.00555 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.79679E-07 0.00067 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.83223E-06 0.00042  2.83206E-06 0.00042  2.85449E-06 0.00512 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.81881E-05 0.00085  3.82091E-05 0.00085  3.49045E-05 0.00950 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.61714E-01 0.00031  6.61354E-01 0.00031  7.32316E-01 0.00861 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03450E+01 0.01297 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.51511E+01 0.00044  3.40120E+01 0.00045 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.47163E+04 0.00216  2.95609E+05 0.00137  6.06386E+05 0.00066  6.51471E+05 0.00051  5.99646E+05 0.00055  6.42998E+05 0.00070  4.36786E+05 0.00067  3.86774E+05 0.00055  2.95470E+05 0.00090  2.41644E+05 0.00072  2.08384E+05 0.00094  1.87678E+05 0.00083  1.73387E+05 0.00074  1.64815E+05 0.00103  1.60751E+05 0.00080  1.38860E+05 0.00085  1.37215E+05 0.00101  1.35904E+05 0.00085  1.33643E+05 0.00105  2.60162E+05 0.00054  2.50879E+05 0.00076  1.81340E+05 0.00083  1.17580E+05 0.00094  1.35312E+05 0.00091  1.28133E+05 0.00081  1.15950E+05 0.00085  1.89174E+05 0.00072  4.35033E+04 0.00146  5.45817E+04 0.00141  4.94775E+04 0.00145  2.88706E+04 0.00235  4.98840E+04 0.00134  3.40098E+04 0.00207  2.86454E+04 0.00216  5.35226E+03 0.00308  5.19089E+03 0.00334  5.04883E+03 0.00403  5.02165E+03 0.00371  5.05396E+03 0.00310  5.19246E+03 0.00332  5.54535E+03 0.00462  5.32149E+03 0.00333  1.01144E+04 0.00263  1.62029E+04 0.00229  2.07902E+04 0.00228  5.47584E+04 0.00125  5.75741E+04 0.00131  6.19961E+04 0.00174  4.12927E+04 0.00164  3.00416E+04 0.00171  2.26035E+04 0.00182  2.66120E+04 0.00146  5.13193E+04 0.00139  7.08973E+04 0.00114  1.37555E+05 0.00114  2.12879E+05 0.00139  3.17785E+05 0.00142  2.02961E+05 0.00152  1.45305E+05 0.00123  1.04372E+05 0.00165  9.37605E+04 0.00163  9.22202E+04 0.00145  7.70198E+04 0.00170  5.20937E+04 0.00179  4.81223E+04 0.00155  4.27374E+04 0.00186  3.61088E+04 0.00184  2.84073E+04 0.00186  1.90344E+04 0.00190  6.72754E+03 0.00311 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.07716E+00 0.00046 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.22400E+18 0.00045  3.23766E+17 0.00125 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37548E-01 8.2E-05  1.52443E+00 0.00045 ];
INF_CAPT                  (idx, [1:   4]) = [  6.78786E-03 0.00069  3.21120E-02 0.00052 ];
INF_ABS                   (idx, [1:   4]) = [  8.95327E-03 0.00053  6.65650E-02 0.00092 ];
INF_FISS                  (idx, [1:   4]) = [  2.16541E-03 0.00046  3.44530E-02 0.00130 ];
INF_NSF                   (idx, [1:   4]) = [  5.61555E-03 0.00046  8.67440E-02 0.00131 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.59330E+00 4.4E-05  2.51775E+00 2.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04224E+02 4.7E-06  2.03336E+02 3.5E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.81455E-08 0.00033  2.55144E-06 0.00020 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28593E-01 8.3E-05  1.45789E+00 0.00051 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43475E-01 0.00018  3.86626E-01 0.00061 ];
INF_SCATT2                (idx, [1:   4]) = [  9.60581E-02 0.00023  9.17996E-02 0.00114 ];
INF_SCATT3                (idx, [1:   4]) = [  7.36461E-03 0.00308  2.76154E-02 0.00320 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02695E-02 0.00178 -8.43532E-03 0.00547 ];
INF_SCATT5                (idx, [1:   4]) = [  1.63400E-04 0.09933  6.48133E-03 0.00712 ];
INF_SCATT6                (idx, [1:   4]) = [  5.06277E-03 0.00329 -1.64258E-02 0.00290 ];
INF_SCATT7                (idx, [1:   4]) = [  7.13200E-04 0.02005  3.49235E-04 0.11055 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28634E-01 8.3E-05  1.45789E+00 0.00051 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43476E-01 0.00018  3.86626E-01 0.00061 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.60582E-02 0.00023  9.17996E-02 0.00114 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.36471E-03 0.00308  2.76154E-02 0.00320 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02697E-02 0.00178 -8.43532E-03 0.00547 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.63525E-04 0.09916  6.48133E-03 0.00712 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.06273E-03 0.00329 -1.64258E-02 0.00290 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.13208E-04 0.02001  3.49235E-04 0.11055 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13390E-01 0.00020  9.85023E-01 0.00041 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56208E+00 0.00020  3.38403E-01 0.00041 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.91240E-03 0.00054  6.65650E-02 0.00092 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69430E-02 0.00020  6.77217E-02 0.00117 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10605E-01 8.1E-05  1.79873E-02 0.00032  1.18584E-03 0.00401  1.45671E+00 0.00051 ];
INF_S1                    (idx, [1:   8]) = [  2.38237E-01 0.00018  5.23824E-03 0.00085  5.09408E-04 0.00708  3.86117E-01 0.00062 ];
INF_S2                    (idx, [1:   8]) = [  9.76255E-02 0.00023 -1.56738E-03 0.00254  2.76628E-04 0.01112  9.15230E-02 0.00115 ];
INF_S3                    (idx, [1:   8]) = [  9.21690E-03 0.00249 -1.85229E-03 0.00175  1.02620E-04 0.02080  2.75128E-02 0.00322 ];
INF_S4                    (idx, [1:   8]) = [ -9.65988E-03 0.00187 -6.09600E-04 0.00516  2.15887E-06 0.69693 -8.43748E-03 0.00545 ];
INF_S5                    (idx, [1:   8]) = [  1.37511E-04 0.11669  2.58895E-05 0.10154 -3.81514E-05 0.04842  6.51948E-03 0.00712 ];
INF_S6                    (idx, [1:   8]) = [  5.20237E-03 0.00329 -1.39602E-04 0.01562 -5.05259E-05 0.02619 -1.63753E-02 0.00293 ];
INF_S7                    (idx, [1:   8]) = [  8.85543E-04 0.01639 -1.72344E-04 0.01634 -4.47845E-05 0.03255  3.94020E-04 0.09856 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10646E-01 8.1E-05  1.79873E-02 0.00032  1.18584E-03 0.00401  1.45671E+00 0.00051 ];
INF_SP1                   (idx, [1:   8]) = [  2.38238E-01 0.00018  5.23824E-03 0.00085  5.09408E-04 0.00708  3.86117E-01 0.00062 ];
INF_SP2                   (idx, [1:   8]) = [  9.76256E-02 0.00023 -1.56738E-03 0.00254  2.76628E-04 0.01112  9.15230E-02 0.00115 ];
INF_SP3                   (idx, [1:   8]) = [  9.21699E-03 0.00249 -1.85229E-03 0.00175  1.02620E-04 0.02080  2.75128E-02 0.00322 ];
INF_SP4                   (idx, [1:   8]) = [ -9.66010E-03 0.00186 -6.09600E-04 0.00516  2.15887E-06 0.69693 -8.43748E-03 0.00545 ];
INF_SP5                   (idx, [1:   8]) = [  1.37635E-04 0.11642  2.58895E-05 0.10154 -3.81514E-05 0.04842  6.51948E-03 0.00712 ];
INF_SP6                   (idx, [1:   8]) = [  5.20233E-03 0.00329 -1.39602E-04 0.01562 -5.05259E-05 0.02619 -1.63753E-02 0.00293 ];
INF_SP7                   (idx, [1:   8]) = [  8.85552E-04 0.01635 -1.72344E-04 0.01634 -4.47845E-05 0.03255  3.94020E-04 0.09856 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32392E-01 0.00047  1.10632E+00 0.00664 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34154E-01 0.00064  1.17222E+00 0.00864 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33832E-01 0.00075  1.18670E+00 0.00749 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29263E-01 0.00101  9.85160E-01 0.00669 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43436E+00 0.00047  3.01620E-01 0.00669 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42358E+00 0.00064  2.84868E-01 0.00862 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42554E+00 0.00075  2.81271E-01 0.00752 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45397E+00 0.00101  3.38721E-01 0.00675 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.32561E-03 0.00849  1.82695E-04 0.05356  1.02442E-03 0.02182  9.65947E-04 0.02154  2.89542E-03 0.01205  9.60321E-04 0.02117  2.96807E-04 0.04009 ];
LAMBDA                    (idx, [1:  14]) = [  7.92052E-01 0.02049  1.24915E-02 0.00013  3.14486E-02 0.00047  1.10259E-01 0.00050  3.21207E-01 0.00036  1.34263E+00 0.00060  8.94763E+00 0.00294 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0007' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:45:38 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:32:15 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292338 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.92520E-01  1.00417E+00  1.00044E+00  1.00150E+00  1.00137E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.2E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14016E-02 0.00112  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88598E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.02074E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.02506E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68244E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.50208E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.50128E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.61599E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.15019E-01 0.00115  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000655 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00013E+04 0.00076 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00013E+04 0.00076 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.30974E+02 ;
RUNNING_TIME              (idx, 1)        =  4.66226E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.82450E-01  4.82450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.18550E-01  9.65000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  4.56261E+01  3.35570E+00  2.63308E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  3.91650E-01  2.78500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.81833E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.66225E+01  1.25581E+02 ];
CPU_USAGE                 (idx, 1)        = 4.95412 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99997E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75247E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.24018E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.81339E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.07415E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.65364E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.15422E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.58651E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.69794E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  7.52022E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.18225E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.55630E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.89679E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.96392E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.49257E+07 ;
SR90_ACTIVITY             (idx, 1)        =  5.93398E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.21068E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.25407E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.30987E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.93695E+12 ;
CS137_ACTIVITY            (idx, 1)        =  6.74870E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.05334E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.49623E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.18616E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.20867E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.32604E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 7 ;
BURNUP                     (idx, [1:  2])  = [  5.00000E+00  5.00044E+00 ];
BURN_DAYS                 (idx, 1)        =  1.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.30349E-01 0.00105 ];
U235_FISS                 (idx, [1:   4]) = [  1.02176E+16 0.00069  7.39739E-01 0.00043 ];
U238_FISS                 (idx, [1:   4]) = [  9.27151E+14 0.00271  6.71171E-02 0.00257 ];
PU239_FISS                (idx, [1:   4]) = [  2.57153E+15 0.00166  1.86168E-01 0.00153 ];
PU240_FISS                (idx, [1:   4]) = [  1.44999E+12 0.06763  1.04925E-04 0.06763 ];
PU241_FISS                (idx, [1:   4]) = [  8.96820E+13 0.00863  6.49305E-03 0.00862 ];
U235_CAPT                 (idx, [1:   4]) = [  2.24191E+15 0.00165  1.16576E-01 0.00155 ];
U238_CAPT                 (idx, [1:   4]) = [  8.24087E+15 0.00100  4.28492E-01 0.00070 ];
PU239_CAPT                (idx, [1:   4]) = [  1.43060E+15 0.00221  7.43891E-02 0.00214 ];
PU240_CAPT                (idx, [1:   4]) = [  4.99950E+14 0.00355  2.59967E-02 0.00351 ];
PU241_CAPT                (idx, [1:   4]) = [  3.19280E+13 0.01387  1.66043E-03 0.01388 ];
XE135_CAPT                (idx, [1:   4]) = [  7.27153E+14 0.00281  3.78109E-02 0.00275 ];
SM149_CAPT                (idx, [1:   4]) = [  1.73724E+14 0.00652  9.03401E-03 0.00652 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000655 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.70712E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000655 5.00771E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2886993 2.89108E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2073592 2.07654E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 40070 4.00871E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000655 5.00771E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.72529E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 3.7E-09  4.50057E+05 3.7E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.51112E+16 1.6E-05  3.51112E+16 1.6E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37907E+16 2.7E-06  1.37907E+16 2.7E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.92321E+16 0.00047  1.41157E+16 0.00046  5.11644E+15 0.00113 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.30228E+16 0.00027  2.79064E+16 0.00023  5.11644E+15 0.00113 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.32604E+16 0.00050  3.32604E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.56954E+18 0.00048  4.23133E+17 0.00045  1.14641E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.66677E+14 0.00516 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.32895E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.16619E+18 0.00063 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11932E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11932E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.64121E+00 0.00046 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.90142E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.60379E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24477E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94799E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97169E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.06592E+00 0.00053 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.05737E+00 0.00053 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.54601E+00 1.8E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03690E+02 2.7E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.05717E+00 0.00055  1.05098E+00 0.00053  6.39615E-03 0.00931 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.05638E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.05578E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.05638E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.06492E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73416E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73402E+01 8.5E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.90082E-07 0.00342 ];
IMP_EALF                  (idx, [1:   2]) = [  5.89529E-07 0.00147 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.28297E-01 0.00270 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.28853E-01 0.00110 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.89532E-03 0.00600  1.73185E-04 0.03196  9.66632E-04 0.01444  9.22418E-04 0.01412  2.66346E-03 0.00853  8.89791E-04 0.01451  2.79834E-04 0.02629 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.94273E-01 0.01315  1.06680E-02 0.01851  3.13188E-02 0.00204  1.09979E-01 0.00204  3.21366E-01 0.00025  1.34066E+00 0.00045  8.42835E+00 0.01171 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.19094E-03 0.00876  1.74289E-04 0.05021  1.01598E-03 0.02047  9.51842E-04 0.02154  2.82074E-03 0.01250  9.36648E-04 0.02121  2.91437E-04 0.03770 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.94088E-01 0.01908  1.24911E-02 7.6E-05  3.13849E-02 0.00052  1.10199E-01 0.00051  3.21502E-01 0.00038  1.34106E+00 0.00053  8.97085E+00 0.00376 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.44970E-05 0.00124  2.44887E-05 0.00125  2.58786E-05 0.01354 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.58939E-05 0.00113  2.58850E-05 0.00113  2.73543E-05 0.01354 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.04878E-03 0.00929  1.62846E-04 0.05486  9.82264E-04 0.02172  9.41535E-04 0.02367  2.75587E-03 0.01260  9.21814E-04 0.02349  2.84455E-04 0.04230 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.93876E-01 0.02162  1.24902E-02 1.1E-05  3.13810E-02 0.00061  1.10278E-01 0.00063  3.21515E-01 0.00041  1.34044E+00 0.00075  8.94957E+00 0.00374 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.42028E-05 0.00290  2.41940E-05 0.00290  2.53983E-05 0.03791 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.55822E-05 0.00284  2.55730E-05 0.00283  2.68512E-05 0.03796 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.84346E-03 0.02749  1.67162E-04 0.17074  9.91778E-04 0.07175  8.95891E-04 0.07748  2.63303E-03 0.04322  8.95119E-04 0.07625  2.60485E-04 0.13293 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.01480E-01 0.06883  1.24900E-02 2.9E-05  3.13981E-02 0.00143  1.10545E-01 0.00154  3.21630E-01 0.00127  1.34476E+00 0.00072  8.97804E+00 0.00736 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.87026E-03 0.02640  1.70030E-04 0.16502  1.00829E-03 0.06917  9.05503E-04 0.07353  2.62632E-03 0.04167  8.96004E-04 0.07309  2.64104E-04 0.12974 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.94654E-01 0.06753  1.24900E-02 2.9E-05  3.14006E-02 0.00143  1.10555E-01 0.00154  3.21642E-01 0.00126  1.34455E+00 0.00074  8.97782E+00 0.00736 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.42416E+02 0.02756 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.43539E-05 0.00077 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.57423E-05 0.00053 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.98778E-03 0.00508 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.45871E+02 0.00504 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.75440E-07 0.00070 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.81950E-06 0.00043  2.81918E-06 0.00043  2.86927E-06 0.00534 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.79896E-05 0.00086  3.80073E-05 0.00087  3.53191E-05 0.01005 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.58346E-01 0.00032  6.58004E-01 0.00032  7.30077E-01 0.00977 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04694E+01 0.01312 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.50128E+01 0.00046  3.38645E+01 0.00047 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.51323E+04 0.00302  2.96743E+05 0.00142  6.07983E+05 0.00081  6.50791E+05 0.00088  5.98834E+05 0.00071  6.43051E+05 0.00072  4.36948E+05 0.00066  3.87036E+05 0.00073  2.95317E+05 0.00076  2.41383E+05 0.00078  2.08237E+05 0.00061  1.87666E+05 0.00077  1.73290E+05 0.00082  1.64697E+05 0.00083  1.60640E+05 0.00062  1.38822E+05 0.00107  1.37112E+05 0.00106  1.35791E+05 0.00087  1.33168E+05 0.00108  2.60262E+05 0.00073  2.51422E+05 0.00065  1.81100E+05 0.00085  1.17318E+05 0.00089  1.35659E+05 0.00111  1.27972E+05 0.00118  1.15544E+05 0.00100  1.89181E+05 0.00068  4.34998E+04 0.00156  5.45180E+04 0.00161  4.94280E+04 0.00182  2.86898E+04 0.00175  4.99777E+04 0.00150  3.38045E+04 0.00154  2.85958E+04 0.00150  5.31188E+03 0.00375  5.06384E+03 0.00325  4.83592E+03 0.00394  4.75593E+03 0.00366  4.88230E+03 0.00440  5.06199E+03 0.00453  5.46825E+03 0.00376  5.21802E+03 0.00328  9.96632E+03 0.00260  1.60413E+04 0.00224  2.05831E+04 0.00252  5.44369E+04 0.00119  5.71646E+04 0.00158  6.17244E+04 0.00109  4.05927E+04 0.00133  2.94313E+04 0.00157  2.20950E+04 0.00139  2.59593E+04 0.00155  5.01910E+04 0.00139  6.94288E+04 0.00121  1.35423E+05 0.00115  2.10228E+05 0.00091  3.14370E+05 0.00109  2.01041E+05 0.00118  1.43816E+05 0.00128  1.03426E+05 0.00143  9.27498E+04 0.00115  9.14815E+04 0.00133  7.63902E+04 0.00124  5.16388E+04 0.00159  4.76872E+04 0.00135  4.23797E+04 0.00145  3.58433E+04 0.00159  2.82120E+04 0.00169  1.88579E+04 0.00189  6.69683E+03 0.00244 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.06431E+00 0.00043 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.24415E+18 0.00047  3.25425E+17 0.00105 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37543E-01 0.00011  1.52855E+00 0.00030 ];
INF_CAPT                  (idx, [1:   4]) = [  6.90497E-03 0.00078  3.27029E-02 0.00052 ];
INF_ABS                   (idx, [1:   4]) = [  9.03758E-03 0.00062  6.69333E-02 0.00080 ];
INF_FISS                  (idx, [1:   4]) = [  2.13261E-03 0.00038  3.42304E-02 0.00108 ];
INF_NSF                   (idx, [1:   4]) = [  5.54686E-03 0.00040  8.67024E-02 0.00110 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.60098E+00 4.4E-05  2.53291E+00 3.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04324E+02 3.4E-06  2.03540E+02 5.2E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.79163E-08 0.00032  2.55414E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28495E-01 0.00012  1.46160E+00 0.00035 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43459E-01 0.00023  3.87869E-01 0.00049 ];
INF_SCATT2                (idx, [1:   4]) = [  9.60473E-02 0.00035  9.20937E-02 0.00074 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34867E-03 0.00326  2.76860E-02 0.00217 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02732E-02 0.00236 -8.44415E-03 0.00588 ];
INF_SCATT5                (idx, [1:   4]) = [  1.75614E-04 0.10133  6.43647E-03 0.00939 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10833E-03 0.00344 -1.65529E-02 0.00241 ];
INF_SCATT7                (idx, [1:   4]) = [  7.77595E-04 0.02027  3.06874E-04 0.13940 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28536E-01 0.00012  1.46160E+00 0.00035 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43460E-01 0.00023  3.87869E-01 0.00049 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.60474E-02 0.00035  9.20937E-02 0.00074 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34866E-03 0.00326  2.76860E-02 0.00217 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02731E-02 0.00236 -8.44415E-03 0.00588 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.75533E-04 0.10119  6.43647E-03 0.00939 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10851E-03 0.00345 -1.65529E-02 0.00241 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.77486E-04 0.02030  3.06874E-04 0.13940 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13237E-01 0.00027  9.88193E-01 0.00022 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56321E+00 0.00027  3.37316E-01 0.00022 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.99637E-03 0.00065  6.69333E-02 0.00080 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69440E-02 0.00021  6.81336E-02 0.00094 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 2.0E-07  2.00865E-07 1.00000 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  9.99968E-01 3.2E-05  3.17235E-05 1.00000 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10599E-01 0.00011  1.78963E-02 0.00035  1.17861E-03 0.00424  1.46042E+00 0.00035 ];
INF_S1                    (idx, [1:   8]) = [  2.38243E-01 0.00022  5.21564E-03 0.00100  4.97712E-04 0.00649  3.87371E-01 0.00049 ];
INF_S2                    (idx, [1:   8]) = [  9.76062E-02 0.00035 -1.55890E-03 0.00205  2.68279E-04 0.00764  9.18254E-02 0.00074 ];
INF_S3                    (idx, [1:   8]) = [  9.18966E-03 0.00253 -1.84099E-03 0.00203  9.48858E-05 0.01767  2.75911E-02 0.00219 ];
INF_S4                    (idx, [1:   8]) = [ -9.66709E-03 0.00238 -6.06143E-04 0.00484 -1.83797E-06 0.85516 -8.44232E-03 0.00588 ];
INF_S5                    (idx, [1:   8]) = [  1.54240E-04 0.11278  2.13735E-05 0.08111 -4.06185E-05 0.04177  6.47708E-03 0.00933 ];
INF_S6                    (idx, [1:   8]) = [  5.24808E-03 0.00323 -1.39746E-04 0.01887 -4.86922E-05 0.02852 -1.65043E-02 0.00242 ];
INF_S7                    (idx, [1:   8]) = [  9.45853E-04 0.01670 -1.68259E-04 0.01484 -4.59007E-05 0.02428  3.52775E-04 0.12183 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10640E-01 0.00011  1.78963E-02 0.00035  1.17861E-03 0.00424  1.46042E+00 0.00035 ];
INF_SP1                   (idx, [1:   8]) = [  2.38244E-01 0.00022  5.21564E-03 0.00100  4.97712E-04 0.00649  3.87371E-01 0.00049 ];
INF_SP2                   (idx, [1:   8]) = [  9.76064E-02 0.00035 -1.55890E-03 0.00205  2.68279E-04 0.00764  9.18254E-02 0.00074 ];
INF_SP3                   (idx, [1:   8]) = [  9.18966E-03 0.00253 -1.84099E-03 0.00203  9.48858E-05 0.01767  2.75911E-02 0.00219 ];
INF_SP4                   (idx, [1:   8]) = [ -9.66692E-03 0.00239 -6.06143E-04 0.00484 -1.83797E-06 0.85516 -8.44232E-03 0.00588 ];
INF_SP5                   (idx, [1:   8]) = [  1.54160E-04 0.11266  2.13735E-05 0.08111 -4.06185E-05 0.04177  6.47708E-03 0.00933 ];
INF_SP6                   (idx, [1:   8]) = [  5.24826E-03 0.00324 -1.39746E-04 0.01887 -4.86922E-05 0.02852 -1.65043E-02 0.00242 ];
INF_SP7                   (idx, [1:   8]) = [  9.45745E-04 0.01673 -1.68259E-04 0.01484 -4.59007E-05 0.02428  3.52775E-04 0.12183 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32027E-01 0.00066  1.12929E+00 0.00615 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33556E-01 0.00102  1.21031E+00 0.00811 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33403E-01 0.00105  1.20688E+00 0.00849 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29183E-01 0.00077  9.99694E-01 0.00678 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43663E+00 0.00066  2.95438E-01 0.00612 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42725E+00 0.00102  2.75851E-01 0.00819 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42818E+00 0.00105  2.76662E-01 0.00832 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45446E+00 0.00077  3.33801E-01 0.00673 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.19094E-03 0.00876  1.74289E-04 0.05021  1.01598E-03 0.02047  9.51842E-04 0.02154  2.82074E-03 0.01250  9.36648E-04 0.02121  2.91437E-04 0.03770 ];
LAMBDA                    (idx, [1:  14]) = [  7.94088E-01 0.01908  1.24911E-02 7.6E-05  3.13849E-02 0.00052  1.10199E-01 0.00051  3.21502E-01 0.00038  1.34106E+00 0.00053  8.97085E+00 0.00376 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0007' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:45:38 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:38:18 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292338 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.92981E-01  1.00411E+00  1.00059E+00  1.00178E+00  1.00053E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13975E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88603E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.02650E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.03082E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67834E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.49432E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.49351E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.56564E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.13453E-01 0.00113  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000602 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00012E+04 0.00081 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00012E+04 0.00081 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.61193E+02 ;
RUNNING_TIME              (idx, 1)        =  5.26727E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.82450E-01  4.82450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.37283E-01  9.01666E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  5.16017E+01  3.34923E+00  2.62645E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  4.46700E-01  2.67667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.16167E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  5.26725E+01  1.25317E+02 ];
CPU_USAGE                 (idx, 1)        = 4.95878 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99909E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76389E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.27697E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.80403E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.36870E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.67301E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.16769E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.60394E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.68723E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  8.45653E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.25109E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.15253E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.99383E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.30401E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.55171E+07 ;
SR90_ACTIVITY             (idx, 1)        =  7.02394E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.23307E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.28520E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.34020E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.81759E+12 ;
CS137_ACTIVITY            (idx, 1)        =  8.09702E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.08098E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.47472E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.74604E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.21698E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.37410E+12 0.00048  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 8 ;
BURNUP                     (idx, [1:  2])  = [  6.00000E+00  6.00053E+00 ];
BURN_DAYS                 (idx, 1)        =  1.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.39635E-01 0.00109 ];
U235_FISS                 (idx, [1:   4]) = [  9.79966E+15 0.00071  7.11050E-01 0.00045 ];
U238_FISS                 (idx, [1:   4]) = [  9.38441E+14 0.00280  6.80821E-02 0.00264 ];
PU239_FISS                (idx, [1:   4]) = [  2.90483E+15 0.00147  2.10769E-01 0.00135 ];
PU240_FISS                (idx, [1:   4]) = [  1.83325E+12 0.06128  1.32952E-04 0.06114 ];
PU241_FISS                (idx, [1:   4]) = [  1.31521E+14 0.00689  9.54381E-03 0.00689 ];
U235_CAPT                 (idx, [1:   4]) = [  2.16570E+15 0.00164  1.09741E-01 0.00162 ];
U238_CAPT                 (idx, [1:   4]) = [  8.31724E+15 0.00107  4.21397E-01 0.00071 ];
PU239_CAPT                (idx, [1:   4]) = [  1.61718E+15 0.00205  8.19413E-02 0.00196 ];
PU240_CAPT                (idx, [1:   4]) = [  6.25279E+14 0.00333  3.16809E-02 0.00325 ];
PU241_CAPT                (idx, [1:   4]) = [  4.84110E+13 0.01180  2.45314E-03 0.01182 ];
XE135_CAPT                (idx, [1:   4]) = [  7.29379E+14 0.00314  3.69607E-02 0.00315 ];
SM149_CAPT                (idx, [1:   4]) = [  1.78689E+14 0.00620  9.05454E-03 0.00619 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000602 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.69551E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000602 5.00770E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2920465 2.92464E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2039521 2.04244E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 40616 4.06191E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000602 5.00770E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -1.39698E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 3.7E-09  4.50057E+05 3.7E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.52549E+16 1.7E-05  3.52549E+16 1.7E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37792E+16 2.8E-06  1.37792E+16 2.8E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.97353E+16 0.00043  1.45680E+16 0.00043  5.16724E+15 0.00110 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.35145E+16 0.00025  2.83472E+16 0.00022  5.16724E+15 0.00110 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.37410E+16 0.00048  3.37410E+16 0.00048  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.58983E+18 0.00045  4.28088E+17 0.00043  1.16174E+18 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.74137E+14 0.00519 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.37886E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.18040E+18 0.00059 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11815E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11815E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.62954E+00 0.00047 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.90167E-01 0.00030 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.57757E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24418E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94693E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97168E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.05368E+00 0.00056 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04512E+00 0.00056 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.55856E+00 1.9E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03861E+02 2.8E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04532E+00 0.00057  1.03883E+00 0.00057  6.29199E-03 0.00861 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.04503E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04499E+00 0.00048 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.04503E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.05359E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73171E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73142E+01 8.3E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.04779E-07 0.00343 ];
IMP_EALF                  (idx, [1:   2]) = [  6.05065E-07 0.00144 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.31410E-01 0.00286 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.32435E-01 0.00112 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.87700E-03 0.00575  1.74076E-04 0.03274  9.94032E-04 0.01343  9.09525E-04 0.01440  2.64163E-03 0.00834  8.67020E-04 0.01515  2.90720E-04 0.02566 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.07838E-01 0.01399  1.05246E-02 0.01939  3.13335E-02 0.00037  1.10288E-01 0.00038  3.21440E-01 0.00026  1.33807E+00 0.00062  8.44350E+00 0.01132 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.01437E-03 0.00855  1.76973E-04 0.05068  1.03371E-03 0.02000  9.35540E-04 0.02164  2.69136E-03 0.01298  8.91583E-04 0.02219  2.85195E-04 0.03805 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.95571E-01 0.02065  1.25021E-02 0.00039  3.13260E-02 0.00052  1.10287E-01 0.00056  3.21504E-01 0.00038  1.33822E+00 0.00078  8.96733E+00 0.00384 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.45731E-05 0.00127  2.45626E-05 0.00127  2.62617E-05 0.01252 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.56824E-05 0.00112  2.56715E-05 0.00112  2.74507E-05 0.01251 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.02591E-03 0.00877  1.84880E-04 0.05273  1.00439E-03 0.02224  9.21179E-04 0.02347  2.71217E-03 0.01363  8.99956E-04 0.02316  3.03338E-04 0.04127 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.19351E-01 0.02228  1.24999E-02 0.00050  3.13395E-02 0.00063  1.10248E-01 0.00064  3.21475E-01 0.00042  1.33774E+00 0.00099  8.97200E+00 0.00388 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.41267E-05 0.00298  2.41139E-05 0.00301  2.58926E-05 0.03209 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.52152E-05 0.00291  2.52018E-05 0.00294  2.70595E-05 0.03206 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.07206E-03 0.02972  2.61851E-04 0.14590  9.39437E-04 0.07459  9.85614E-04 0.08040  2.76465E-03 0.04617  8.63068E-04 0.08030  2.57441E-04 0.14012 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.66576E-01 0.07077  1.25056E-02 0.00131  3.12951E-02 0.00155  1.10132E-01 0.00146  3.21473E-01 0.00122  1.33376E+00 0.00310  9.05683E+00 0.00820 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.05514E-03 0.02944  2.56140E-04 0.14539  9.45286E-04 0.07229  9.68253E-04 0.08086  2.74908E-03 0.04433  8.85969E-04 0.07890  2.50408E-04 0.12929 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.75288E-01 0.06980  1.25056E-02 0.00131  3.12974E-02 0.00155  1.10133E-01 0.00145  3.21371E-01 0.00120  1.33497E+00 0.00293  9.05503E+00 0.00816 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.51441E+02 0.02944 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.43658E-05 0.00079 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.54661E-05 0.00057 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.08709E-03 0.00536 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.49863E+02 0.00537 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.72466E-07 0.00070 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.80444E-06 0.00042  2.80428E-06 0.00042  2.82647E-06 0.00549 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.78925E-05 0.00087  3.79102E-05 0.00087  3.51124E-05 0.01059 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.55651E-01 0.00033  6.55359E-01 0.00034  7.17339E-01 0.00930 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05677E+01 0.01351 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.49351E+01 0.00047  3.37227E+01 0.00047 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.53606E+04 0.00391  2.98565E+05 0.00149  6.09376E+05 0.00116  6.51143E+05 0.00066  5.99102E+05 0.00078  6.43242E+05 0.00072  4.36886E+05 0.00065  3.86337E+05 0.00083  2.95790E+05 0.00083  2.41645E+05 0.00059  2.08341E+05 0.00098  1.87502E+05 0.00086  1.73387E+05 0.00087  1.64698E+05 0.00107  1.60474E+05 0.00062  1.38847E+05 0.00096  1.36870E+05 0.00090  1.35871E+05 0.00122  1.33293E+05 0.00084  2.60494E+05 0.00076  2.50852E+05 0.00072  1.81271E+05 0.00079  1.17207E+05 0.00101  1.35428E+05 0.00084  1.28296E+05 0.00082  1.15548E+05 0.00106  1.88961E+05 0.00070  4.34425E+04 0.00170  5.44842E+04 0.00128  4.93955E+04 0.00130  2.87050E+04 0.00188  4.98115E+04 0.00140  3.37917E+04 0.00183  2.84254E+04 0.00213  5.25031E+03 0.00312  4.95545E+03 0.00262  4.67920E+03 0.00447  4.54553E+03 0.00339  4.63560E+03 0.00286  4.91259E+03 0.00342  5.36464E+03 0.00298  5.14604E+03 0.00460  9.88171E+03 0.00328  1.59421E+04 0.00299  2.05024E+04 0.00170  5.41275E+04 0.00133  5.69672E+04 0.00140  6.10613E+04 0.00122  4.01450E+04 0.00182  2.88630E+04 0.00142  2.16942E+04 0.00176  2.55359E+04 0.00156  4.93447E+04 0.00121  6.85455E+04 0.00134  1.34000E+05 0.00148  2.08325E+05 0.00145  3.11959E+05 0.00146  1.99416E+05 0.00156  1.42951E+05 0.00175  1.02663E+05 0.00176  9.23062E+04 0.00154  9.09895E+04 0.00186  7.60361E+04 0.00193  5.13973E+04 0.00172  4.75192E+04 0.00188  4.22084E+04 0.00191  3.56599E+04 0.00214  2.81141E+04 0.00180  1.87797E+04 0.00156  6.66512E+03 0.00271 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.05355E+00 0.00044 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.26238E+18 0.00039  3.27482E+17 0.00142 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37548E-01 0.00010  1.53239E+00 0.00046 ];
INF_CAPT                  (idx, [1:   4]) = [  7.01380E-03 0.00064  3.32303E-02 0.00069 ];
INF_ABS                   (idx, [1:   4]) = [  9.11437E-03 0.00054  6.72155E-02 0.00106 ];
INF_FISS                  (idx, [1:   4]) = [  2.10057E-03 0.00041  3.39852E-02 0.00142 ];
INF_NSF                   (idx, [1:   4]) = [  5.47897E-03 0.00040  8.65502E-02 0.00145 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.60832E+00 6.0E-05  2.54670E+00 3.9E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04422E+02 5.5E-06  2.03727E+02 6.5E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.76942E-08 0.00037  2.55694E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28434E-01 0.00010  1.46517E+00 0.00054 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43508E-01 0.00020  3.88697E-01 0.00061 ];
INF_SCATT2                (idx, [1:   4]) = [  9.60685E-02 0.00028  9.23257E-02 0.00105 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35158E-03 0.00316  2.77695E-02 0.00208 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02548E-02 0.00173 -8.52816E-03 0.00749 ];
INF_SCATT5                (idx, [1:   4]) = [  1.79113E-04 0.10349  6.51807E-03 0.00968 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08713E-03 0.00331 -1.65769E-02 0.00308 ];
INF_SCATT7                (idx, [1:   4]) = [  7.56151E-04 0.01400  3.99956E-04 0.10302 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28475E-01 0.00011  1.46517E+00 0.00054 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43509E-01 0.00020  3.88697E-01 0.00061 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.60686E-02 0.00028  9.23257E-02 0.00105 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35142E-03 0.00316  2.77695E-02 0.00208 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02550E-02 0.00173 -8.52816E-03 0.00749 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.79117E-04 0.10360  6.51807E-03 0.00968 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08726E-03 0.00331 -1.65769E-02 0.00308 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.56036E-04 0.01400  3.99956E-04 0.10302 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12972E-01 0.00024  9.91148E-01 0.00043 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56515E+00 0.00024  3.36312E-01 0.00043 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.07324E-03 0.00053  6.72155E-02 0.00106 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69365E-02 0.00024  6.84135E-02 0.00125 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10612E-01 0.00010  1.78221E-02 0.00038  1.19176E-03 0.00531  1.46398E+00 0.00054 ];
INF_S1                    (idx, [1:   8]) = [  2.38320E-01 0.00020  5.18851E-03 0.00085  5.06571E-04 0.00732  3.88191E-01 0.00061 ];
INF_S2                    (idx, [1:   8]) = [  9.76334E-02 0.00027 -1.56494E-03 0.00213  2.75917E-04 0.00763  9.20498E-02 0.00107 ];
INF_S3                    (idx, [1:   8]) = [  9.18704E-03 0.00254 -1.83546E-03 0.00173  9.92215E-05 0.01719  2.76703E-02 0.00208 ];
INF_S4                    (idx, [1:   8]) = [ -9.65998E-03 0.00183 -5.94836E-04 0.00478 -1.58584E-06 0.78941 -8.52657E-03 0.00749 ];
INF_S5                    (idx, [1:   8]) = [  1.50655E-04 0.11970  2.84584E-05 0.10813 -3.97336E-05 0.03472  6.55781E-03 0.00962 ];
INF_S6                    (idx, [1:   8]) = [  5.22723E-03 0.00331 -1.40093E-04 0.01721 -5.19319E-05 0.02860 -1.65249E-02 0.00308 ];
INF_S7                    (idx, [1:   8]) = [  9.28804E-04 0.01199 -1.72653E-04 0.01446 -4.56003E-05 0.03260  4.45556E-04 0.09193 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10653E-01 0.00010  1.78221E-02 0.00038  1.19176E-03 0.00531  1.46398E+00 0.00054 ];
INF_SP1                   (idx, [1:   8]) = [  2.38321E-01 0.00020  5.18851E-03 0.00085  5.06571E-04 0.00732  3.88191E-01 0.00061 ];
INF_SP2                   (idx, [1:   8]) = [  9.76336E-02 0.00027 -1.56494E-03 0.00213  2.75917E-04 0.00763  9.20498E-02 0.00107 ];
INF_SP3                   (idx, [1:   8]) = [  9.18687E-03 0.00254 -1.83546E-03 0.00173  9.92215E-05 0.01719  2.76703E-02 0.00208 ];
INF_SP4                   (idx, [1:   8]) = [ -9.66013E-03 0.00183 -5.94836E-04 0.00478 -1.58584E-06 0.78941 -8.52657E-03 0.00749 ];
INF_SP5                   (idx, [1:   8]) = [  1.50658E-04 0.11987  2.84584E-05 0.10813 -3.97336E-05 0.03472  6.55781E-03 0.00962 ];
INF_SP6                   (idx, [1:   8]) = [  5.22735E-03 0.00332 -1.40093E-04 0.01721 -5.19319E-05 0.02860 -1.65249E-02 0.00308 ];
INF_SP7                   (idx, [1:   8]) = [  9.28689E-04 0.01199 -1.72653E-04 0.01446 -4.56003E-05 0.03260  4.45556E-04 0.09193 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31914E-01 0.00069  1.13400E+00 0.00584 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33527E-01 0.00097  1.21664E+00 0.00779 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33383E-01 0.00099  1.21902E+00 0.00905 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28900E-01 0.00088  9.98214E-01 0.00582 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43733E+00 0.00069  2.94187E-01 0.00589 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42742E+00 0.00097  2.74381E-01 0.00784 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42830E+00 0.00099  2.73976E-01 0.00897 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45627E+00 0.00089  3.34205E-01 0.00589 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.01437E-03 0.00855  1.76973E-04 0.05068  1.03371E-03 0.02000  9.35540E-04 0.02164  2.69136E-03 0.01298  8.91583E-04 0.02219  2.85195E-04 0.03805 ];
LAMBDA                    (idx, [1:  14]) = [  7.95571E-01 0.02065  1.25021E-02 0.00039  3.13260E-02 0.00052  1.10287E-01 0.00056  3.21504E-01 0.00038  1.33822E+00 0.00078  8.96733E+00 0.00384 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0007' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:45:38 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:44:22 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292338 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.93840E-01  1.00389E+00  1.00087E+00  1.00080E+00  1.00060E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13768E-02 0.00106  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88623E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.03700E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04132E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67561E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.49472E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.49389E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.50968E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.11592E-01 0.00107  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000488 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00010E+04 0.00079 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00010E+04 0.00079 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.91469E+02 ;
RUNNING_TIME              (idx, 1)        =  5.87346E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.82450E-01  4.82450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.57700E-01  9.81667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  5.75860E+01  3.35333E+00  2.63093E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.03517E-01  2.79667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.41833E-02  7.83332E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  5.87342E+01  1.25347E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96248 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00095E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.77288E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.30681E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.79558E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.84192E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.69026E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.17970E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61653E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.67759E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  9.42347E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.30927E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.82302E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.08343E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.60045E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.60093E+07 ;
SR90_ACTIVITY             (idx, 1)        =  8.08843E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.25301E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.31277E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.36715E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.85570E+12 ;
CS137_ACTIVITY            (idx, 1)        =  9.44444E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.10386E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.45542E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.54079E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.22406E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.42593E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 9 ;
BURNUP                     (idx, [1:  2])  = [  7.00000E+00  7.00062E+00 ];
BURN_DAYS                 (idx, 1)        =  1.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.49878E-01 0.00109 ];
U235_FISS                 (idx, [1:   4]) = [  9.43814E+15 0.00079  6.85179E-01 0.00049 ];
U238_FISS                 (idx, [1:   4]) = [  9.56409E+14 0.00281  6.94200E-02 0.00261 ];
PU239_FISS                (idx, [1:   4]) = [  3.19389E+15 0.00142  2.31870E-01 0.00130 ];
PU240_FISS                (idx, [1:   4]) = [  2.45457E+12 0.05118  1.78367E-04 0.05125 ];
PU241_FISS                (idx, [1:   4]) = [  1.77368E+14 0.00652  1.28765E-02 0.00649 ];
U235_CAPT                 (idx, [1:   4]) = [  2.09014E+15 0.00178  1.03202E-01 0.00171 ];
U238_CAPT                 (idx, [1:   4]) = [  8.40475E+15 0.00106  4.14953E-01 0.00074 ];
PU239_CAPT                (idx, [1:   4]) = [  1.77526E+15 0.00198  8.76533E-02 0.00191 ];
PU240_CAPT                (idx, [1:   4]) = [  7.46750E+14 0.00306  3.68677E-02 0.00296 ];
PU241_CAPT                (idx, [1:   4]) = [  6.36369E+13 0.01054  3.14169E-03 0.01050 ];
XE135_CAPT                (idx, [1:   4]) = [  7.32551E+14 0.00311  3.61728E-02 0.00312 ];
SM149_CAPT                (idx, [1:   4]) = [  1.80999E+14 0.00613  8.93768E-03 0.00614 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000488 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.64382E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000488 5.00764E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2951672 2.95597E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2007589 2.01045E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 41227 4.12279E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000488 5.00764E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.68107E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 3.7E-09  4.50057E+05 3.7E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.53846E+16 1.8E-05  3.53846E+16 1.8E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37687E+16 3.2E-06  1.37687E+16 3.2E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.02487E+16 0.00045  1.49925E+16 0.00045  5.25620E+15 0.00117 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.40174E+16 0.00027  2.87612E+16 0.00023  5.25620E+15 0.00117 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.42593E+16 0.00052  3.42593E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.61204E+18 0.00049  4.32845E+17 0.00048  1.17919E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.82555E+14 0.00524 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.42999E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.19865E+18 0.00064 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11699E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11699E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.62038E+00 0.00046 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.89140E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.55254E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24368E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94669E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97070E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.04198E+00 0.00054 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.03339E+00 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.56993E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04016E+02 3.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03341E+00 0.00056  1.02724E+00 0.00055  6.15288E-03 0.00905 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.03324E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03298E+00 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.03324E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.04184E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72887E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72905E+01 8.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.22403E-07 0.00364 ];
IMP_EALF                  (idx, [1:   2]) = [  6.19621E-07 0.00152 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.37309E-01 0.00286 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.35558E-01 0.00117 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.85107E-03 0.00581  1.55357E-04 0.03679  9.70778E-04 0.01449  9.38336E-04 0.01475  2.62975E-03 0.00896  8.75346E-04 0.01461  2.81500E-04 0.02634 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.99796E-01 0.01381  9.84823E-03 0.02322  3.12921E-02 0.00038  1.10212E-01 0.00038  3.21648E-01 0.00026  1.33736E+00 0.00068  8.61493E+00 0.00951 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.97730E-03 0.00861  1.57094E-04 0.05292  1.01581E-03 0.02233  9.33525E-04 0.02251  2.69671E-03 0.01303  8.82612E-04 0.02099  2.91553E-04 0.03998 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.06244E-01 0.02149  1.24958E-02 0.00026  3.12894E-02 0.00054  1.10244E-01 0.00053  3.21570E-01 0.00038  1.33697E+00 0.00094  8.92620E+00 0.00476 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.46390E-05 0.00129  2.46273E-05 0.00129  2.64369E-05 0.01286 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.54577E-05 0.00111  2.54456E-05 0.00112  2.73050E-05 0.01276 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.94748E-03 0.00922  1.63293E-04 0.05482  9.81630E-04 0.02321  9.56116E-04 0.02380  2.65259E-03 0.01427  9.11263E-04 0.02369  2.82589E-04 0.04302 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.99904E-01 0.02311  1.24916E-02 0.00015  3.13017E-02 0.00066  1.10322E-01 0.00066  3.21802E-01 0.00045  1.33610E+00 0.00112  8.93897E+00 0.00606 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.41325E-05 0.00276  2.41283E-05 0.00276  2.39530E-05 0.03456 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.49357E-05 0.00273  2.49313E-05 0.00273  2.47646E-05 0.03458 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.81231E-03 0.03060  1.69400E-04 0.19482  9.35751E-04 0.06910  1.02074E-03 0.07261  2.57494E-03 0.04569  8.66855E-04 0.07529  2.44623E-04 0.13414 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.57221E-01 0.06864  1.24892E-02 4.4E-05  3.11653E-02 0.00169  1.10100E-01 0.00138  3.22038E-01 0.00131  1.33187E+00 0.00376  8.95989E+00 0.01448 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.80954E-03 0.02975  1.72329E-04 0.18683  9.46540E-04 0.06758  1.01446E-03 0.07142  2.56670E-03 0.04472  8.69800E-04 0.07269  2.39717E-04 0.12934 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.53768E-01 0.06754  1.24892E-02 4.3E-05  3.11654E-02 0.00168  1.10139E-01 0.00140  3.21997E-01 0.00130  1.33199E+00 0.00372  8.95906E+00 0.01447 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.41949E+02 0.03079 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.44039E-05 0.00086 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.52151E-05 0.00062 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.92323E-03 0.00601 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.42791E+02 0.00605 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.72145E-07 0.00072 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.79035E-06 0.00044  2.79009E-06 0.00044  2.83013E-06 0.00514 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.79442E-05 0.00087  3.79638E-05 0.00088  3.47932E-05 0.01027 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.53221E-01 0.00034  6.52965E-01 0.00034  7.08470E-01 0.00907 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02648E+01 0.01385 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.49389E+01 0.00048  3.36319E+01 0.00047 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.54503E+04 0.00277  2.97895E+05 0.00130  6.07549E+05 0.00113  6.51336E+05 0.00076  5.98954E+05 0.00076  6.42676E+05 0.00077  4.36189E+05 0.00083  3.86427E+05 0.00078  2.96115E+05 0.00059  2.41358E+05 0.00070  2.08056E+05 0.00074  1.87543E+05 0.00091  1.73049E+05 0.00089  1.64731E+05 0.00079  1.60531E+05 0.00073  1.38678E+05 0.00118  1.36866E+05 0.00080  1.35887E+05 0.00096  1.33202E+05 0.00072  2.60158E+05 0.00067  2.50976E+05 0.00067  1.81131E+05 0.00080  1.17218E+05 0.00112  1.35622E+05 0.00085  1.28307E+05 0.00101  1.15320E+05 0.00096  1.88320E+05 0.00066  4.32679E+04 0.00165  5.43729E+04 0.00109  4.93893E+04 0.00166  2.87366E+04 0.00230  4.98358E+04 0.00155  3.38456E+04 0.00166  2.83629E+04 0.00197  5.18462E+03 0.00358  4.84467E+03 0.00492  4.53489E+03 0.00373  4.39676E+03 0.00332  4.46085E+03 0.00284  4.76937E+03 0.00276  5.28497E+03 0.00406  5.14601E+03 0.00422  9.80064E+03 0.00300  1.58032E+04 0.00246  2.03337E+04 0.00233  5.37272E+04 0.00154  5.64777E+04 0.00133  6.06455E+04 0.00112  3.98321E+04 0.00155  2.86718E+04 0.00149  2.13923E+04 0.00202  2.52160E+04 0.00174  4.87964E+04 0.00164  6.80453E+04 0.00109  1.33365E+05 0.00109  2.07658E+05 0.00136  3.11104E+05 0.00149  1.99225E+05 0.00170  1.42817E+05 0.00174  1.02542E+05 0.00167  9.22108E+04 0.00158  9.09177E+04 0.00161  7.59759E+04 0.00171  5.13588E+04 0.00166  4.75053E+04 0.00183  4.21901E+04 0.00196  3.56811E+04 0.00196  2.81833E+04 0.00212  1.88651E+04 0.00231  6.67291E+03 0.00288 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.04157E+00 0.00056 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.28055E+18 0.00055  3.31518E+17 0.00136 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37843E-01 0.00011  1.53816E+00 0.00044 ];
INF_CAPT                  (idx, [1:   4]) = [  7.11135E-03 0.00047  3.36134E-02 0.00067 ];
INF_ABS                   (idx, [1:   4]) = [  9.18171E-03 0.00041  6.71552E-02 0.00102 ];
INF_FISS                  (idx, [1:   4]) = [  2.07036E-03 0.00054  3.35418E-02 0.00138 ];
INF_NSF                   (idx, [1:   4]) = [  5.41399E-03 0.00053  8.58396E-02 0.00141 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.61500E+00 6.5E-05  2.55918E+00 3.9E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04512E+02 7.0E-06  2.03898E+02 6.6E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.75341E-08 0.00045  2.56006E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28655E-01 0.00011  1.47100E+00 0.00050 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43700E-01 0.00018  3.90551E-01 0.00051 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61489E-02 0.00031  9.27920E-02 0.00089 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34970E-03 0.00332  2.78605E-02 0.00263 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02814E-02 0.00224 -8.68740E-03 0.00788 ];
INF_SCATT5                (idx, [1:   4]) = [  1.53738E-04 0.09879  6.35077E-03 0.00852 ];
INF_SCATT6                (idx, [1:   4]) = [  5.06269E-03 0.00339 -1.67163E-02 0.00362 ];
INF_SCATT7                (idx, [1:   4]) = [  7.30286E-04 0.02344  3.70517E-04 0.13911 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28696E-01 0.00011  1.47100E+00 0.00050 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43700E-01 0.00018  3.90551E-01 0.00051 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61489E-02 0.00031  9.27920E-02 0.00089 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34960E-03 0.00332  2.78605E-02 0.00263 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02817E-02 0.00224 -8.68740E-03 0.00788 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.53790E-04 0.09865  6.35077E-03 0.00852 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.06279E-03 0.00337 -1.67163E-02 0.00362 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.30323E-04 0.02341  3.70517E-04 0.13911 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13038E-01 0.00025  9.95003E-01 0.00039 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56467E+00 0.00025  3.35008E-01 0.00039 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.14081E-03 0.00041  6.71552E-02 0.00102 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69594E-02 0.00017  6.83462E-02 0.00107 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10883E-01 0.00011  1.77714E-02 0.00043  1.18812E-03 0.00384  1.46982E+00 0.00050 ];
INF_S1                    (idx, [1:   8]) = [  2.38522E-01 0.00018  5.17742E-03 0.00067  5.11858E-04 0.00614  3.90040E-01 0.00051 ];
INF_S2                    (idx, [1:   8]) = [  9.77081E-02 0.00030 -1.55922E-03 0.00239  2.74432E-04 0.00723  9.25176E-02 0.00090 ];
INF_S3                    (idx, [1:   8]) = [  9.18051E-03 0.00262 -1.83082E-03 0.00219  9.58492E-05 0.01826  2.77647E-02 0.00265 ];
INF_S4                    (idx, [1:   8]) = [ -9.68623E-03 0.00233 -5.95216E-04 0.00505 -7.04344E-07 1.00000 -8.68670E-03 0.00787 ];
INF_S5                    (idx, [1:   8]) = [  1.25370E-04 0.12372  2.83678E-05 0.07102 -4.08315E-05 0.03719  6.39160E-03 0.00839 ];
INF_S6                    (idx, [1:   8]) = [  5.20232E-03 0.00334 -1.39622E-04 0.01651 -5.24761E-05 0.02841 -1.66638E-02 0.00360 ];
INF_S7                    (idx, [1:   8]) = [  9.04127E-04 0.01796 -1.73842E-04 0.01449 -4.72277E-05 0.02459  4.17744E-04 0.12334 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10924E-01 0.00011  1.77714E-02 0.00043  1.18812E-03 0.00384  1.46982E+00 0.00050 ];
INF_SP1                   (idx, [1:   8]) = [  2.38522E-01 0.00018  5.17742E-03 0.00067  5.11858E-04 0.00614  3.90040E-01 0.00051 ];
INF_SP2                   (idx, [1:   8]) = [  9.77081E-02 0.00030 -1.55922E-03 0.00239  2.74432E-04 0.00723  9.25176E-02 0.00090 ];
INF_SP3                   (idx, [1:   8]) = [  9.18042E-03 0.00261 -1.83082E-03 0.00219  9.58492E-05 0.01826  2.77647E-02 0.00265 ];
INF_SP4                   (idx, [1:   8]) = [ -9.68650E-03 0.00233 -5.95216E-04 0.00505 -7.04344E-07 1.00000 -8.68670E-03 0.00787 ];
INF_SP5                   (idx, [1:   8]) = [  1.25423E-04 0.12355  2.83678E-05 0.07102 -4.08315E-05 0.03719  6.39160E-03 0.00839 ];
INF_SP6                   (idx, [1:   8]) = [  5.20241E-03 0.00333 -1.39622E-04 0.01651 -5.24761E-05 0.02841 -1.66638E-02 0.00360 ];
INF_SP7                   (idx, [1:   8]) = [  9.04165E-04 0.01793 -1.73842E-04 0.01449 -4.72277E-05 0.02459  4.17744E-04 0.12334 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31944E-01 0.00053  1.14500E+00 0.00687 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33529E-01 0.00081  1.23028E+00 0.00962 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33634E-01 0.00101  1.22997E+00 0.00811 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28745E-01 0.00090  1.00708E+00 0.00684 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43714E+00 0.00053  2.91445E-01 0.00675 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42740E+00 0.00081  2.71534E-01 0.00947 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42677E+00 0.00101  2.71443E-01 0.00821 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45726E+00 0.00090  3.31359E-01 0.00679 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.97730E-03 0.00861  1.57094E-04 0.05292  1.01581E-03 0.02233  9.33525E-04 0.02251  2.69671E-03 0.01303  8.82612E-04 0.02099  2.91553E-04 0.03998 ];
LAMBDA                    (idx, [1:  14]) = [  8.06244E-01 0.02149  1.24958E-02 0.00026  3.12894E-02 0.00054  1.10244E-01 0.00053  3.21570E-01 0.00038  1.33697E+00 0.00094  8.92620E+00 0.00476 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0007' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:45:38 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:50:24 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292338 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.97687E-01  1.00360E+00  9.97612E-01  1.00100E+00  1.00010E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13509E-02 0.00113  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88649E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04013E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04443E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67415E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.48815E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.48732E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.47695E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.09801E-01 0.00119  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000679 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00014E+04 0.00081 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00014E+04 0.00081 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.21612E+02 ;
RUNNING_TIME              (idx, 1)        =  6.47696E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.82450E-01  4.82450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.77333E-01  9.48333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.35464E+01  3.33902E+00  2.62133E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.57833E-01  2.64000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.68167E-02  9.16664E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  6.47694E+01  1.25234E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96548 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00034E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78029E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.33501E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.78805E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.55619E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.70917E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.19293E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62581E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.66873E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.04324E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.36109E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.56888E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.18095E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.86354E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.64300E+07 ;
SR90_ACTIVITY             (idx, 1)        =  9.12881E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.27173E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.33808E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.39238E+14 ;
CS134_ACTIVITY            (idx, 1)        =  5.04162E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.07909E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.12698E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.43776E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.63523E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.23109E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.47449E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 10 ;
BURNUP                     (idx, [1:  2])  = [  8.00000E+00  8.00071E+00 ];
BURN_DAYS                 (idx, 1)        =  2.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.59517E-01 0.00112 ];
U235_FISS                 (idx, [1:   4]) = [  9.08847E+15 0.00078  6.60185E-01 0.00053 ];
U238_FISS                 (idx, [1:   4]) = [  9.66147E+14 0.00274  7.01727E-02 0.00259 ];
PU239_FISS                (idx, [1:   4]) = [  3.46850E+15 0.00149  2.51938E-01 0.00130 ];
PU240_FISS                (idx, [1:   4]) = [  3.14995E+12 0.04561  2.28631E-04 0.04563 ];
PU241_FISS                (idx, [1:   4]) = [  2.32916E+14 0.00525  1.69194E-02 0.00523 ];
U235_CAPT                 (idx, [1:   4]) = [  2.01548E+15 0.00185  9.71577E-02 0.00179 ];
U238_CAPT                 (idx, [1:   4]) = [  8.48528E+15 0.00106  4.09004E-01 0.00077 ];
PU239_CAPT                (idx, [1:   4]) = [  1.92248E+15 0.00193  9.26740E-02 0.00187 ];
PU240_CAPT                (idx, [1:   4]) = [  8.68012E+14 0.00282  4.18409E-02 0.00275 ];
PU241_CAPT                (idx, [1:   4]) = [  8.29552E+13 0.00873  3.99957E-03 0.00877 ];
XE135_CAPT                (idx, [1:   4]) = [  7.35427E+14 0.00299  3.54530E-02 0.00298 ];
SM149_CAPT                (idx, [1:   4]) = [  1.86901E+14 0.00566  9.01032E-03 0.00567 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000679 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.92055E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000679 5.00792E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2981012 2.98539E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1978346 1.98120E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 41321 4.13335E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000679 5.00792E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.33299E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 3.7E-09  4.50057E+05 3.7E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.55104E+16 2.0E-05  3.55104E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37585E+16 3.6E-06  1.37585E+16 3.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.07388E+16 0.00045  1.54265E+16 0.00044  5.31230E+15 0.00116 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.44972E+16 0.00027  2.91850E+16 0.00023  5.31230E+15 0.00116 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.47449E+16 0.00049  3.47449E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.63283E+18 0.00048  4.38094E+17 0.00045  1.19474E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.87260E+14 0.00492 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.47845E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.21346E+18 0.00063 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11583E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11583E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.61120E+00 0.00051 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.88787E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.52707E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24322E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94587E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97131E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.03121E+00 0.00056 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.02268E+00 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.58098E+00 2.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04167E+02 3.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.02275E+00 0.00056  1.01672E+00 0.00056  5.96411E-03 0.00922 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.02250E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.02215E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.02250E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.03103E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72702E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72676E+01 9.2E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.33863E-07 0.00349 ];
IMP_EALF                  (idx, [1:   2]) = [  6.33961E-07 0.00159 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.38976E-01 0.00279 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.39586E-01 0.00128 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.78866E-03 0.00594  1.53870E-04 0.03602  9.57057E-04 0.01467  9.35319E-04 0.01462  2.61741E-03 0.00857  8.56154E-04 0.01470  2.68857E-04 0.02546 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.79591E-01 0.01343  9.74702E-03 0.02378  3.12619E-02 0.00039  1.10238E-01 0.00038  3.21787E-01 0.00027  1.33003E+00 0.00217  8.37020E+00 0.01211 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.79731E-03 0.00925  1.58601E-04 0.05719  9.61341E-04 0.02262  9.51853E-04 0.02251  2.60743E-03 0.01333  8.58340E-04 0.02330  2.59743E-04 0.03869 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.64919E-01 0.01985  1.24969E-02 0.00035  3.12594E-02 0.00056  1.10199E-01 0.00054  3.21755E-01 0.00041  1.33503E+00 0.00092  8.90028E+00 0.00524 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.48038E-05 0.00124  2.47944E-05 0.00124  2.64215E-05 0.01374 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.53643E-05 0.00112  2.53547E-05 0.00112  2.70208E-05 0.01374 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.82737E-03 0.00936  1.55901E-04 0.05936  9.66176E-04 0.02374  9.27374E-04 0.02450  2.65754E-03 0.01457  8.48943E-04 0.02441  2.71429E-04 0.04257 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.75761E-01 0.02207  1.24947E-02 0.00040  3.12559E-02 0.00070  1.10318E-01 0.00072  3.21927E-01 0.00046  1.33271E+00 0.00135  8.88705E+00 0.00612 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.43128E-05 0.00296  2.42979E-05 0.00297  2.62882E-05 0.03849 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.48615E-05 0.00290  2.48463E-05 0.00290  2.68712E-05 0.03831 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.63843E-03 0.03063  1.47134E-04 0.19711  8.48744E-04 0.07427  9.45276E-04 0.08045  2.54443E-03 0.04753  8.58064E-04 0.07710  2.94777E-04 0.13132 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.34375E-01 0.07482  1.25176E-02 0.00227  3.12266E-02 0.00170  1.10442E-01 0.00163  3.22212E-01 0.00128  1.33219E+00 0.00293  9.06043E+00 0.01069 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.64583E-03 0.02951  1.54949E-04 0.19690  8.42782E-04 0.07306  9.34383E-04 0.07833  2.55636E-03 0.04647  8.70575E-04 0.07297  2.86781E-04 0.13045 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.33324E-01 0.07382  1.25176E-02 0.00227  3.12372E-02 0.00167  1.10438E-01 0.00163  3.22182E-01 0.00128  1.33203E+00 0.00293  9.05790E+00 0.01068 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.33067E+02 0.03070 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.45728E-05 0.00074 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.51280E-05 0.00051 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.80890E-03 0.00580 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.36470E+02 0.00586 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.69913E-07 0.00069 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.78012E-06 0.00042  2.78017E-06 0.00042  2.76917E-06 0.00571 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.79008E-05 0.00087  3.79193E-05 0.00087  3.48745E-05 0.01048 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.50596E-01 0.00032  6.50435E-01 0.00033  6.90164E-01 0.00949 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04487E+01 0.01438 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.48732E+01 0.00046  3.35726E+01 0.00046 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.63087E+04 0.00293  2.98762E+05 0.00161  6.08702E+05 0.00074  6.51200E+05 0.00064  5.98636E+05 0.00067  6.43336E+05 0.00063  4.36069E+05 0.00071  3.86096E+05 0.00088  2.95736E+05 0.00089  2.41152E+05 0.00072  2.07977E+05 0.00087  1.87769E+05 0.00083  1.73297E+05 0.00088  1.64500E+05 0.00093  1.60499E+05 0.00068  1.38672E+05 0.00062  1.36843E+05 0.00075  1.35730E+05 0.00086  1.33202E+05 0.00109  2.60282E+05 0.00062  2.50912E+05 0.00073  1.81214E+05 0.00090  1.17518E+05 0.00096  1.35394E+05 0.00085  1.28193E+05 0.00089  1.15229E+05 0.00132  1.88399E+05 0.00070  4.33566E+04 0.00134  5.43542E+04 0.00152  4.93111E+04 0.00157  2.87551E+04 0.00242  4.96417E+04 0.00133  3.36470E+04 0.00160  2.82306E+04 0.00208  5.13903E+03 0.00363  4.74554E+03 0.00401  4.38482E+03 0.00365  4.21709E+03 0.00301  4.34280E+03 0.00369  4.62482E+03 0.00386  5.16310E+03 0.00348  5.01619E+03 0.00350  9.71718E+03 0.00295  1.57488E+04 0.00226  2.02576E+04 0.00188  5.36285E+04 0.00146  5.62692E+04 0.00145  6.03156E+04 0.00122  3.95261E+04 0.00140  2.82697E+04 0.00213  2.11072E+04 0.00161  2.48231E+04 0.00169  4.82317E+04 0.00154  6.71574E+04 0.00138  1.32061E+05 0.00097  2.06042E+05 0.00106  3.09220E+05 0.00123  1.98074E+05 0.00122  1.42016E+05 0.00111  1.02320E+05 0.00139  9.19733E+04 0.00147  9.05684E+04 0.00133  7.56034E+04 0.00149  5.11252E+04 0.00141  4.72941E+04 0.00146  4.20750E+04 0.00149  3.56517E+04 0.00154  2.80761E+04 0.00195  1.87667E+04 0.00169  6.65854E+03 0.00204 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.03067E+00 0.00042 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.29875E+18 0.00049  3.34111E+17 0.00100 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37862E-01 0.00010  1.54117E+00 0.00037 ];
INF_CAPT                  (idx, [1:   4]) = [  7.21024E-03 0.00048  3.40471E-02 0.00063 ];
INF_ABS                   (idx, [1:   4]) = [  9.25073E-03 0.00040  6.73005E-02 0.00086 ];
INF_FISS                  (idx, [1:   4]) = [  2.04049E-03 0.00049  3.32534E-02 0.00111 ];
INF_NSF                   (idx, [1:   4]) = [  5.35082E-03 0.00047  8.54983E-02 0.00114 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.62232E+00 6.8E-05  2.57111E+00 5.0E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04606E+02 8.1E-06  2.04062E+02 8.6E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.73621E-08 0.00042  2.56241E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28605E-01 0.00011  1.47385E+00 0.00042 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43663E-01 0.00015  3.91225E-01 0.00050 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61069E-02 0.00026  9.28114E-02 0.00092 ];
INF_SCATT3                (idx, [1:   4]) = [  7.33283E-03 0.00217  2.79258E-02 0.00277 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02689E-02 0.00153 -8.57568E-03 0.00696 ];
INF_SCATT5                (idx, [1:   4]) = [  1.54948E-04 0.12016  6.55183E-03 0.00805 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07682E-03 0.00339 -1.66749E-02 0.00381 ];
INF_SCATT7                (idx, [1:   4]) = [  7.35903E-04 0.02176  3.95854E-04 0.09583 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28648E-01 0.00011  1.47385E+00 0.00042 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43663E-01 0.00015  3.91225E-01 0.00050 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61072E-02 0.00026  9.28114E-02 0.00092 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.33296E-03 0.00217  2.79258E-02 0.00277 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02690E-02 0.00153 -8.57568E-03 0.00696 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.54826E-04 0.11995  6.55183E-03 0.00805 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07667E-03 0.00340 -1.66749E-02 0.00381 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.35913E-04 0.02179  3.95854E-04 0.09583 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12946E-01 0.00028  9.97451E-01 0.00033 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56535E+00 0.00028  3.34186E-01 0.00033 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.20835E-03 0.00041  6.73005E-02 0.00086 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69568E-02 0.00017  6.85085E-02 0.00090 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10905E-01 0.00010  1.77003E-02 0.00045  1.19197E-03 0.00447  1.47266E+00 0.00042 ];
INF_S1                    (idx, [1:   8]) = [  2.38519E-01 0.00016  5.14340E-03 0.00082  5.10899E-04 0.00634  3.90714E-01 0.00051 ];
INF_S2                    (idx, [1:   8]) = [  9.76711E-02 0.00026 -1.56420E-03 0.00255  2.78255E-04 0.01158  9.25331E-02 0.00094 ];
INF_S3                    (idx, [1:   8]) = [  9.16057E-03 0.00166 -1.82774E-03 0.00202  1.03011E-04 0.01941  2.78228E-02 0.00278 ];
INF_S4                    (idx, [1:   8]) = [ -9.68196E-03 0.00152 -5.86961E-04 0.00472  9.28130E-07 1.00000 -8.57661E-03 0.00690 ];
INF_S5                    (idx, [1:   8]) = [  1.21692E-04 0.14958  3.32561E-05 0.09152 -4.39164E-05 0.04170  6.59575E-03 0.00795 ];
INF_S6                    (idx, [1:   8]) = [  5.21652E-03 0.00323 -1.39698E-04 0.01867 -5.34060E-05 0.03500 -1.66215E-02 0.00378 ];
INF_S7                    (idx, [1:   8]) = [  9.07121E-04 0.01708 -1.71218E-04 0.01377 -4.58582E-05 0.03408  4.41712E-04 0.08578 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10947E-01 0.00010  1.77003E-02 0.00045  1.19197E-03 0.00447  1.47266E+00 0.00042 ];
INF_SP1                   (idx, [1:   8]) = [  2.38520E-01 0.00016  5.14340E-03 0.00082  5.10899E-04 0.00634  3.90714E-01 0.00051 ];
INF_SP2                   (idx, [1:   8]) = [  9.76715E-02 0.00026 -1.56420E-03 0.00255  2.78255E-04 0.01158  9.25331E-02 0.00094 ];
INF_SP3                   (idx, [1:   8]) = [  9.16070E-03 0.00167 -1.82774E-03 0.00202  1.03011E-04 0.01941  2.78228E-02 0.00278 ];
INF_SP4                   (idx, [1:   8]) = [ -9.68201E-03 0.00152 -5.86961E-04 0.00472  9.28130E-07 1.00000 -8.57661E-03 0.00690 ];
INF_SP5                   (idx, [1:   8]) = [  1.21570E-04 0.14937  3.32561E-05 0.09152 -4.39164E-05 0.04170  6.59575E-03 0.00795 ];
INF_SP6                   (idx, [1:   8]) = [  5.21636E-03 0.00324 -1.39698E-04 0.01867 -5.34060E-05 0.03500 -1.66215E-02 0.00378 ];
INF_SP7                   (idx, [1:   8]) = [  9.07131E-04 0.01711 -1.71218E-04 0.01377 -4.58582E-05 0.03408  4.41712E-04 0.08578 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31928E-01 0.00067  1.15569E+00 0.00630 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33384E-01 0.00118  1.24076E+00 0.00687 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33476E-01 0.00086  1.24741E+00 0.00939 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28991E-01 0.00086  1.01287E+00 0.00578 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43724E+00 0.00066  2.88701E-01 0.00625 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42831E+00 0.00117  2.68952E-01 0.00675 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42772E+00 0.00086  2.67787E-01 0.00939 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45569E+00 0.00086  3.29364E-01 0.00582 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.79731E-03 0.00925  1.58601E-04 0.05719  9.61341E-04 0.02262  9.51853E-04 0.02251  2.60743E-03 0.01333  8.58340E-04 0.02330  2.59743E-04 0.03869 ];
LAMBDA                    (idx, [1:  14]) = [  7.64919E-01 0.01985  1.24969E-02 0.00035  3.12594E-02 0.00056  1.10199E-01 0.00054  3.21755E-01 0.00041  1.33503E+00 0.00092  8.90028E+00 0.00524 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0007' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:45:38 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:56:25 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292338 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.95305E-01  1.00407E+00  9.99393E-01  1.00192E+00  9.99309E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13306E-02 0.00111  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88669E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04508E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04937E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67131E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.48375E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.48291E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.43957E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.08224E-01 0.00113  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000634 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00013E+04 0.00083 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00013E+04 0.00083 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.51643E+02 ;
RUNNING_TIME              (idx, 1)        =  7.07818E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.82450E-01  4.82450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.96600E-01  9.51667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.94847E+01  3.32778E+00  2.61060E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  6.11900E-01  2.66167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.94000E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.07816E+01  1.24979E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96798 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00032E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78631E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.36095E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.78126E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.57837E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.72825E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.20626E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.63267E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.66060E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.14878E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.40777E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.38715E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.28070E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.10069E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.67970E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.01465E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.28871E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.36105E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.41535E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.37429E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.21362E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.14898E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.42161E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.09087E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.23782E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.52444E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 11 ;
BURNUP                     (idx, [1:  2])  = [  9.00000E+00  9.00080E+00 ];
BURN_DAYS                 (idx, 1)        =  2.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.69862E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  8.75247E+15 0.00081  6.36598E-01 0.00055 ];
U238_FISS                 (idx, [1:   4]) = [  9.83932E+14 0.00281  7.15576E-02 0.00267 ];
PU239_FISS                (idx, [1:   4]) = [  3.71336E+15 0.00133  2.70091E-01 0.00122 ];
PU240_FISS                (idx, [1:   4]) = [  3.51658E+12 0.04518  2.55898E-04 0.04518 ];
PU241_FISS                (idx, [1:   4]) = [  2.86964E+14 0.00520  2.08726E-02 0.00518 ];
U235_CAPT                 (idx, [1:   4]) = [  1.95451E+15 0.00183  9.19633E-02 0.00174 ];
U238_CAPT                 (idx, [1:   4]) = [  8.58100E+15 0.00105  4.03724E-01 0.00071 ];
PU239_CAPT                (idx, [1:   4]) = [  2.06462E+15 0.00186  9.71469E-02 0.00180 ];
PU240_CAPT                (idx, [1:   4]) = [  9.84505E+14 0.00282  4.63201E-02 0.00272 ];
PU241_CAPT                (idx, [1:   4]) = [  1.05845E+14 0.00807  4.98035E-03 0.00806 ];
XE135_CAPT                (idx, [1:   4]) = [  7.41455E+14 0.00307  3.48892E-02 0.00306 ];
SM149_CAPT                (idx, [1:   4]) = [  1.92292E+14 0.00587  9.04896E-03 0.00588 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000634 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.79313E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000634 5.00779E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3010828 3.01515E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1947753 1.95059E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 42053 4.20544E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000634 5.00779E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.68107E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 3.7E-09  4.50057E+05 3.7E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.56259E+16 2.0E-05  3.56259E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37490E+16 3.8E-06  1.37490E+16 3.8E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.12267E+16 0.00045  1.58473E+16 0.00045  5.37939E+15 0.00119 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.49756E+16 0.00027  2.95962E+16 0.00024  5.37939E+15 0.00119 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.52444E+16 0.00049  3.52444E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.65408E+18 0.00047  4.43149E+17 0.00047  1.21094E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.96476E+14 0.00505 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.52721E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.22928E+18 0.00063 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11467E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11467E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.59900E+00 0.00050 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.88485E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.50133E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24391E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94538E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97035E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.01953E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.01096E+00 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.59117E+00 2.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04309E+02 3.8E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.01095E+00 0.00057  1.00519E+00 0.00055  5.76379E-03 0.00980 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.01165E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.01094E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.01165E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.02023E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72383E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72445E+01 9.2E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.54732E-07 0.00378 ];
IMP_EALF                  (idx, [1:   2]) = [  6.48819E-07 0.00160 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.43892E-01 0.00287 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.43028E-01 0.00118 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.85090E-03 0.00607  1.54653E-04 0.03571  9.86202E-04 0.01454  9.15695E-04 0.01521  2.64017E-03 0.00929  8.75635E-04 0.01485  2.78548E-04 0.02656 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.90441E-01 0.01386  9.89953E-03 0.02294  3.12157E-02 0.00042  1.10090E-01 0.00205  3.21810E-01 0.00027  1.33279E+00 0.00085  8.38731E+00 0.01173 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.79460E-03 0.00879  1.58174E-04 0.05199  9.53386E-04 0.02101  9.25883E-04 0.02167  2.60227E-03 0.01337  8.71142E-04 0.02215  2.83747E-04 0.04251 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.94363E-01 0.02146  1.25006E-02 0.00039  3.12005E-02 0.00061  1.10318E-01 0.00061  3.21742E-01 0.00041  1.33364E+00 0.00129  8.85574E+00 0.00554 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.49410E-05 0.00128  2.49281E-05 0.00129  2.70372E-05 0.01421 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.52105E-05 0.00118  2.51975E-05 0.00119  2.73252E-05 0.01417 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.69318E-03 0.00993  1.52920E-04 0.05913  9.31236E-04 0.02282  9.27127E-04 0.02434  2.55120E-03 0.01498  8.51220E-04 0.02505  2.79474E-04 0.04342 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.97836E-01 0.02311  1.25106E-02 0.00079  3.12162E-02 0.00075  1.10351E-01 0.00073  3.21930E-01 0.00048  1.33383E+00 0.00125  8.86526E+00 0.00708 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.45788E-05 0.00305  2.45700E-05 0.00304  2.48914E-05 0.03320 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.48440E-05 0.00300  2.48350E-05 0.00299  2.51668E-05 0.03323 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.69872E-03 0.03007  1.68563E-04 0.17510  9.56490E-04 0.07441  8.72188E-04 0.07849  2.63387E-03 0.04424  7.98677E-04 0.08150  2.68936E-04 0.15734 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  6.93944E-01 0.06760  1.24897E-02 4.1E-05  3.11352E-02 0.00171  1.10433E-01 0.00184  3.21562E-01 0.00132  1.33223E+00 0.00356  8.90617E+00 0.01293 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.73572E-03 0.02964  1.69808E-04 0.17062  9.59164E-04 0.07311  8.74996E-04 0.07730  2.65426E-03 0.04337  8.22503E-04 0.08016  2.54993E-04 0.15844 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  6.73253E-01 0.06560  1.24898E-02 4.1E-05  3.11351E-02 0.00171  1.10441E-01 0.00184  3.21577E-01 0.00131  1.33184E+00 0.00360  8.90617E+00 0.01293 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.33322E+02 0.03042 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.47749E-05 0.00076 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.50424E-05 0.00053 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.71558E-03 0.00615 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.30716E+02 0.00613 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.68488E-07 0.00076 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.76830E-06 0.00042  2.76811E-06 0.00042  2.79848E-06 0.00531 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.79030E-05 0.00091  3.79206E-05 0.00090  3.49126E-05 0.01003 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.48051E-01 0.00035  6.47940E-01 0.00035  6.79655E-01 0.00951 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05974E+01 0.01467 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.48291E+01 0.00051  3.34606E+01 0.00049 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.65995E+04 0.00353  2.99804E+05 0.00154  6.09098E+05 0.00082  6.51445E+05 0.00073  5.98786E+05 0.00069  6.42427E+05 0.00086  4.36379E+05 0.00072  3.85878E+05 0.00064  2.95017E+05 0.00086  2.41043E+05 0.00088  2.07844E+05 0.00069  1.87519E+05 0.00050  1.72894E+05 0.00077  1.64569E+05 0.00078  1.60515E+05 0.00081  1.38360E+05 0.00091  1.36827E+05 0.00066  1.35475E+05 0.00078  1.33146E+05 0.00091  2.60153E+05 0.00080  2.51296E+05 0.00068  1.81263E+05 0.00074  1.17108E+05 0.00109  1.35307E+05 0.00090  1.28319E+05 0.00091  1.15278E+05 0.00115  1.87989E+05 0.00068  4.32771E+04 0.00169  5.43978E+04 0.00155  4.91929E+04 0.00138  2.87109E+04 0.00184  4.96554E+04 0.00157  3.35055E+04 0.00212  2.79314E+04 0.00149  5.13049E+03 0.00240  4.65409E+03 0.00391  4.27472E+03 0.00308  4.09587E+03 0.00371  4.18444E+03 0.00323  4.54516E+03 0.00351  5.10674E+03 0.00319  4.99935E+03 0.00316  9.62368E+03 0.00234  1.55995E+04 0.00320  2.00489E+04 0.00186  5.32056E+04 0.00125  5.59080E+04 0.00123  5.99366E+04 0.00150  3.91172E+04 0.00126  2.79776E+04 0.00167  2.07595E+04 0.00224  2.45095E+04 0.00130  4.76229E+04 0.00132  6.65515E+04 0.00150  1.31186E+05 0.00129  2.04749E+05 0.00174  3.07956E+05 0.00156  1.97575E+05 0.00166  1.41642E+05 0.00189  1.01983E+05 0.00166  9.17671E+04 0.00205  9.02973E+04 0.00203  7.54250E+04 0.00202  5.10143E+04 0.00189  4.71878E+04 0.00188  4.19848E+04 0.00212  3.54965E+04 0.00236  2.80344E+04 0.00198  1.87469E+04 0.00199  6.65040E+03 0.00272 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.01952E+00 0.00055 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.31681E+18 0.00053  3.37303E+17 0.00159 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37895E-01 0.00013  1.54458E+00 0.00048 ];
INF_CAPT                  (idx, [1:   4]) = [  7.30628E-03 0.00067  3.44113E-02 0.00083 ];
INF_ABS                   (idx, [1:   4]) = [  9.31887E-03 0.00055  6.73223E-02 0.00120 ];
INF_FISS                  (idx, [1:   4]) = [  2.01259E-03 0.00051  3.29110E-02 0.00159 ];
INF_NSF                   (idx, [1:   4]) = [  5.29092E-03 0.00050  8.49814E-02 0.00162 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.62891E+00 6.3E-05  2.58215E+00 4.9E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04695E+02 7.0E-06  2.04216E+02 8.5E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.71619E-08 0.00047  2.56484E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28568E-01 0.00013  1.47722E+00 0.00055 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43668E-01 0.00020  3.92047E-01 0.00062 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61150E-02 0.00023  9.29065E-02 0.00089 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32293E-03 0.00297  2.78586E-02 0.00234 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03000E-02 0.00206 -8.63894E-03 0.00605 ];
INF_SCATT5                (idx, [1:   4]) = [  1.60752E-04 0.10090  6.56480E-03 0.00745 ];
INF_SCATT6                (idx, [1:   4]) = [  5.09742E-03 0.00445 -1.67922E-02 0.00284 ];
INF_SCATT7                (idx, [1:   4]) = [  7.54801E-04 0.01899  3.88307E-04 0.12667 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28610E-01 0.00013  1.47722E+00 0.00055 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43669E-01 0.00020  3.92047E-01 0.00062 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61153E-02 0.00023  9.29065E-02 0.00089 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32313E-03 0.00297  2.78586E-02 0.00234 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03000E-02 0.00206 -8.63894E-03 0.00605 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.60811E-04 0.10075  6.56480E-03 0.00745 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.09754E-03 0.00445 -1.67922E-02 0.00284 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.54733E-04 0.01897  3.88307E-04 0.12667 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12821E-01 0.00038  9.99938E-01 0.00042 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56627E+00 0.00038  3.33355E-01 0.00042 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.27715E-03 0.00054  6.73223E-02 0.00120 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69675E-02 0.00019  6.85621E-02 0.00132 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10928E-01 0.00013  1.76404E-02 0.00045  1.19865E-03 0.00391  1.47602E+00 0.00055 ];
INF_S1                    (idx, [1:   8]) = [  2.38544E-01 0.00021  5.12386E-03 0.00097  5.15599E-04 0.00695  3.91532E-01 0.00062 ];
INF_S2                    (idx, [1:   8]) = [  9.76723E-02 0.00023 -1.55725E-03 0.00194  2.82613E-04 0.01076  9.26239E-02 0.00088 ];
INF_S3                    (idx, [1:   8]) = [  9.14015E-03 0.00247 -1.81723E-03 0.00183  1.01093E-04 0.02324  2.77575E-02 0.00235 ];
INF_S4                    (idx, [1:   8]) = [ -9.71126E-03 0.00225 -5.88717E-04 0.00440  1.76294E-06 1.00000 -8.64070E-03 0.00612 ];
INF_S5                    (idx, [1:   8]) = [  1.35027E-04 0.12420  2.57257E-05 0.09589 -3.96297E-05 0.04077  6.60443E-03 0.00745 ];
INF_S6                    (idx, [1:   8]) = [  5.23570E-03 0.00419 -1.38279E-04 0.01543 -4.91077E-05 0.03046 -1.67431E-02 0.00286 ];
INF_S7                    (idx, [1:   8]) = [  9.20961E-04 0.01526 -1.66161E-04 0.01252 -4.50132E-05 0.02832  4.33320E-04 0.11398 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10969E-01 0.00013  1.76404E-02 0.00045  1.19865E-03 0.00391  1.47602E+00 0.00055 ];
INF_SP1                   (idx, [1:   8]) = [  2.38545E-01 0.00021  5.12386E-03 0.00097  5.15599E-04 0.00695  3.91532E-01 0.00062 ];
INF_SP2                   (idx, [1:   8]) = [  9.76725E-02 0.00023 -1.55725E-03 0.00194  2.82613E-04 0.01076  9.26239E-02 0.00088 ];
INF_SP3                   (idx, [1:   8]) = [  9.14036E-03 0.00246 -1.81723E-03 0.00183  1.01093E-04 0.02324  2.77575E-02 0.00235 ];
INF_SP4                   (idx, [1:   8]) = [ -9.71123E-03 0.00225 -5.88717E-04 0.00440  1.76294E-06 1.00000 -8.64070E-03 0.00612 ];
INF_SP5                   (idx, [1:   8]) = [  1.35085E-04 0.12402  2.57257E-05 0.09589 -3.96297E-05 0.04077  6.60443E-03 0.00745 ];
INF_SP6                   (idx, [1:   8]) = [  5.23582E-03 0.00419 -1.38279E-04 0.01543 -4.91077E-05 0.03046 -1.67431E-02 0.00286 ];
INF_SP7                   (idx, [1:   8]) = [  9.20894E-04 0.01523 -1.66161E-04 0.01252 -4.50132E-05 0.02832  4.33320E-04 0.11398 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31628E-01 0.00054  1.16802E+00 0.00743 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.32992E-01 0.00088  1.27139E+00 0.00941 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33176E-01 0.00099  1.25521E+00 0.00886 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28777E-01 0.00076  1.01610E+00 0.00741 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43910E+00 0.00054  2.85760E-01 0.00739 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.43069E+00 0.00088  2.62727E-01 0.00923 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42957E+00 0.00099  2.66059E-01 0.00882 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45704E+00 0.00076  3.28494E-01 0.00758 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.79460E-03 0.00879  1.58174E-04 0.05199  9.53386E-04 0.02101  9.25883E-04 0.02167  2.60227E-03 0.01337  8.71142E-04 0.02215  2.83747E-04 0.04251 ];
LAMBDA                    (idx, [1:  14]) = [  7.94363E-01 0.02146  1.25006E-02 0.00039  3.12005E-02 0.00061  1.10318E-01 0.00061  3.21742E-01 0.00041  1.33364E+00 0.00129  8.85574E+00 0.00554 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0007' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:45:38 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:02:25 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292338 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.92809E-01  1.00396E+00  9.99033E-01  1.00282E+00  1.00138E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.2E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13210E-02 0.00115  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88679E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05017E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05446E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67110E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.48868E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.48783E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.42422E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.08020E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000681 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00014E+04 0.00082 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00014E+04 0.00082 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.81669E+02 ;
RUNNING_TIME              (idx, 1)        =  7.67938E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.82450E-01  4.82450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.17783E-01  1.05000E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.54185E+01  3.33783E+00  2.59597E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  6.68567E-01  2.81000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.28500E-02  7.83332E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.67937E+01  1.24801E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97005 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00012E+00 0.00023 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79128E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.38467E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.77493E+04 ;
TOT_SF_RATE               (idx, 1)        =  4.97928E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.74717E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.21953E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.63748E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.65295E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.25928E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.45029E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  6.27570E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.38054E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.31709E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.71223E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.11425E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.30429E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.38211E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.43647E+14 ;
CS134_ACTIVITY            (idx, 1)        =  7.85101E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.34804E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.16955E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.40658E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  6.96471E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.24419E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.56538E+12 0.00055  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 12 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E+01  1.00009E+01 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.78568E-01 0.00112 ];
U235_FISS                 (idx, [1:   4]) = [  8.44517E+15 0.00084  6.15333E-01 0.00059 ];
U238_FISS                 (idx, [1:   4]) = [  9.90307E+14 0.00281  7.21437E-02 0.00262 ];
PU239_FISS                (idx, [1:   4]) = [  3.92815E+15 0.00133  2.86215E-01 0.00120 ];
PU240_FISS                (idx, [1:   4]) = [  4.26669E+12 0.04006  3.10867E-04 0.04008 ];
PU241_FISS                (idx, [1:   4]) = [  3.48134E+14 0.00433  2.53653E-02 0.00427 ];
U235_CAPT                 (idx, [1:   4]) = [  1.88586E+15 0.00207  8.69726E-02 0.00197 ];
U238_CAPT                 (idx, [1:   4]) = [  8.63878E+15 0.00111  3.98378E-01 0.00073 ];
PU239_CAPT                (idx, [1:   4]) = [  2.18457E+15 0.00176  1.00759E-01 0.00176 ];
PU240_CAPT                (idx, [1:   4]) = [  1.09662E+15 0.00268  5.05735E-02 0.00259 ];
PU241_CAPT                (idx, [1:   4]) = [  1.26249E+14 0.00759  5.82344E-03 0.00760 ];
XE135_CAPT                (idx, [1:   4]) = [  7.37510E+14 0.00311  3.40164E-02 0.00312 ];
SM149_CAPT                (idx, [1:   4]) = [  1.94574E+14 0.00609  8.97434E-03 0.00608 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000681 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.93941E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000681 5.00794E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3036362 3.04084E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1922072 1.92484E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 42247 4.22596E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000681 5.00794E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.44589E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 3.7E-09  4.50057E+05 3.7E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.57350E+16 2.0E-05  3.57350E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37399E+16 3.7E-06  1.37399E+16 3.7E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.16954E+16 0.00048  1.62340E+16 0.00049  5.46143E+15 0.00118 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.54353E+16 0.00029  2.99739E+16 0.00026  5.46143E+15 0.00118 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.56538E+16 0.00055  3.56538E+16 0.00055  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.67324E+18 0.00052  4.47462E+17 0.00049  1.22578E+18 0.00057 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.01364E+14 0.00493 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.57367E+16 0.00030 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.24534E+18 0.00066 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11351E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11351E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.59162E+00 0.00049 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.87609E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.48603E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24206E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94481E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97051E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.00982E+00 0.00059 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.00128E+00 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.60082E+00 2.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04443E+02 3.7E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.00140E+00 0.00059  9.95646E-01 0.00059  5.63659E-03 0.00915 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.00157E+00 0.00030 ];
COL_KEFF                  (idx, [1:   2]) = [  1.00243E+00 0.00055 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.00157E+00 0.00030 ];
ABS_KINF                  (idx, [1:   2]) = [  1.01010E+00 0.00029 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72290E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72283E+01 9.4E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.60819E-07 0.00376 ];
IMP_EALF                  (idx, [1:   2]) = [  6.59434E-07 0.00162 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.46165E-01 0.00290 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.46130E-01 0.00117 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.80680E-03 0.00591  1.52478E-04 0.03576  9.87120E-04 0.01389  9.05659E-04 0.01357  2.60764E-03 0.00919  8.84030E-04 0.01464  2.69870E-04 0.02599 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.82186E-01 0.01364  9.80068E-03 0.02350  3.11904E-02 0.00040  1.10303E-01 0.00040  3.21827E-01 0.00029  1.32594E+00 0.00106  8.33167E+00 0.01212 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.71676E-03 0.00863  1.46413E-04 0.05500  9.65786E-04 0.02162  8.77714E-04 0.02144  2.57895E-03 0.01287  8.80590E-04 0.02313  2.67302E-04 0.04105 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.84732E-01 0.02134  1.25022E-02 0.00041  3.11871E-02 0.00059  1.10253E-01 0.00055  3.21749E-01 0.00041  1.32886E+00 0.00133  8.86774E+00 0.00543 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.51833E-05 0.00130  2.51713E-05 0.00130  2.73092E-05 0.01543 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.52142E-05 0.00115  2.52022E-05 0.00116  2.73332E-05 0.01536 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.62890E-03 0.00927  1.48933E-04 0.05744  9.56735E-04 0.02400  8.74395E-04 0.02383  2.54163E-03 0.01474  8.46337E-04 0.02511  2.60869E-04 0.04494 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.65623E-01 0.02274  1.24992E-02 0.00040  3.11953E-02 0.00072  1.10298E-01 0.00074  3.21735E-01 0.00048  1.33200E+00 0.00132  8.80023E+00 0.00786 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.46081E-05 0.00310  2.46041E-05 0.00311  2.33980E-05 0.03337 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.46368E-05 0.00300  2.46329E-05 0.00302  2.34191E-05 0.03337 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.74539E-03 0.03305  1.87022E-04 0.18161  9.36791E-04 0.07785  8.61516E-04 0.08608  2.55864E-03 0.04745  8.81594E-04 0.07631  3.19825E-04 0.14272 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.15567E-01 0.06826  1.25026E-02 0.00103  3.12345E-02 0.00169  1.10550E-01 0.00180  3.21546E-01 0.00130  1.32735E+00 0.00379  9.16274E+00 0.01007 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.76012E-03 0.03240  1.93799E-04 0.16956  9.49810E-04 0.07499  8.57401E-04 0.08368  2.55860E-03 0.04719  8.77185E-04 0.07395  3.23330E-04 0.13652 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.09792E-01 0.06632  1.25017E-02 0.00096  3.12353E-02 0.00169  1.10530E-01 0.00180  3.21571E-01 0.00130  1.32659E+00 0.00387  9.16489E+00 0.00997 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.34231E+02 0.03294 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.49132E-05 0.00082 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.49434E-05 0.00051 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.65133E-03 0.00650 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.26915E+02 0.00655 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.69318E-07 0.00074 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.75598E-06 0.00041  2.75580E-06 0.00042  2.78609E-06 0.00569 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.80653E-05 0.00090  3.80836E-05 0.00090  3.49424E-05 0.01030 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.46497E-01 0.00035  6.46407E-01 0.00036  6.73430E-01 0.00918 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05633E+01 0.01345 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.48783E+01 0.00048  3.34677E+01 0.00048 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.70894E+04 0.00347  2.99606E+05 0.00142  6.09080E+05 0.00102  6.51145E+05 0.00093  5.98874E+05 0.00064  6.42544E+05 0.00082  4.36440E+05 0.00056  3.85753E+05 0.00080  2.95160E+05 0.00087  2.41083E+05 0.00076  2.07969E+05 0.00094  1.87659E+05 0.00089  1.73104E+05 0.00089  1.64861E+05 0.00085  1.60405E+05 0.00093  1.38405E+05 0.00089  1.36694E+05 0.00092  1.35573E+05 0.00070  1.33255E+05 0.00087  2.59919E+05 0.00094  2.51171E+05 0.00073  1.81085E+05 0.00086  1.17251E+05 0.00078  1.35391E+05 0.00090  1.28139E+05 0.00083  1.15157E+05 0.00101  1.87726E+05 0.00079  4.32699E+04 0.00170  5.43149E+04 0.00141  4.92308E+04 0.00175  2.85569E+04 0.00141  4.96184E+04 0.00149  3.33980E+04 0.00190  2.79378E+04 0.00177  5.04259E+03 0.00389  4.57144E+03 0.00391  4.16913E+03 0.00359  3.98323E+03 0.00389  4.08722E+03 0.00376  4.42470E+03 0.00318  5.00825E+03 0.00313  4.94147E+03 0.00406  9.55979E+03 0.00300  1.55405E+04 0.00218  1.99764E+04 0.00213  5.29824E+04 0.00104  5.58855E+04 0.00128  5.96280E+04 0.00139  3.89611E+04 0.00162  2.77300E+04 0.00167  2.07015E+04 0.00144  2.43203E+04 0.00190  4.72914E+04 0.00147  6.62188E+04 0.00143  1.30636E+05 0.00147  2.04952E+05 0.00124  3.08386E+05 0.00158  1.97743E+05 0.00166  1.42008E+05 0.00165  1.02205E+05 0.00158  9.19393E+04 0.00153  9.05616E+04 0.00171  7.57977E+04 0.00164  5.12566E+04 0.00172  4.74160E+04 0.00206  4.21247E+04 0.00201  3.57011E+04 0.00200  2.81649E+04 0.00216  1.88300E+04 0.00241  6.67641E+03 0.00238 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.01097E+00 0.00062 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.33187E+18 0.00061  3.41407E+17 0.00125 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38095E-01 0.00011  1.54855E+00 0.00041 ];
INF_CAPT                  (idx, [1:   4]) = [  7.39001E-03 0.00071  3.47213E-02 0.00065 ];
INF_ABS                   (idx, [1:   4]) = [  9.37484E-03 0.00060  6.72301E-02 0.00093 ];
INF_FISS                  (idx, [1:   4]) = [  1.98483E-03 0.00051  3.25088E-02 0.00124 ];
INF_NSF                   (idx, [1:   4]) = [  5.23107E-03 0.00050  8.42807E-02 0.00127 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.63553E+00 4.7E-05  2.59255E+00 3.7E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04784E+02 5.7E-06  2.04362E+02 6.6E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.70132E-08 0.00045  2.56801E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28725E-01 0.00012  1.48138E+00 0.00047 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43796E-01 0.00021  3.93255E-01 0.00054 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61772E-02 0.00032  9.31984E-02 0.00115 ];
INF_SCATT3                (idx, [1:   4]) = [  7.30998E-03 0.00302  2.79353E-02 0.00252 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03124E-02 0.00162 -8.75255E-03 0.00784 ];
INF_SCATT5                (idx, [1:   4]) = [  1.79000E-04 0.07970  6.57684E-03 0.00739 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10340E-03 0.00360 -1.68230E-02 0.00335 ];
INF_SCATT7                (idx, [1:   4]) = [  7.61030E-04 0.02316  4.86212E-04 0.07853 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28767E-01 0.00012  1.48138E+00 0.00047 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43797E-01 0.00021  3.93255E-01 0.00054 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61770E-02 0.00032  9.31984E-02 0.00115 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.30952E-03 0.00303  2.79353E-02 0.00252 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03123E-02 0.00162 -8.75255E-03 0.00784 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.78955E-04 0.07952  6.57684E-03 0.00739 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10330E-03 0.00359 -1.68230E-02 0.00335 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.60857E-04 0.02322  4.86212E-04 0.07853 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12804E-01 0.00030  1.00261E+00 0.00035 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56639E+00 0.00029  3.32467E-01 0.00035 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.33233E-03 0.00058  6.72301E-02 0.00093 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69694E-02 0.00026  6.83540E-02 0.00113 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11125E-01 0.00011  1.75996E-02 0.00052  1.18977E-03 0.00429  1.48019E+00 0.00047 ];
INF_S1                    (idx, [1:   8]) = [  2.38677E-01 0.00021  5.11889E-03 0.00115  5.07243E-04 0.00661  3.92748E-01 0.00054 ];
INF_S2                    (idx, [1:   8]) = [  9.77340E-02 0.00032 -1.55682E-03 0.00225  2.76606E-04 0.00764  9.29218E-02 0.00115 ];
INF_S3                    (idx, [1:   8]) = [  9.12808E-03 0.00237 -1.81810E-03 0.00182  9.65098E-05 0.02220  2.78388E-02 0.00254 ];
INF_S4                    (idx, [1:   8]) = [ -9.72255E-03 0.00177 -5.89885E-04 0.00484  5.86179E-07 1.00000 -8.75314E-03 0.00787 ];
INF_S5                    (idx, [1:   8]) = [  1.50454E-04 0.09240  2.85459E-05 0.08589 -3.94023E-05 0.02751  6.61624E-03 0.00738 ];
INF_S6                    (idx, [1:   8]) = [  5.23859E-03 0.00326 -1.35191E-04 0.01978 -5.13254E-05 0.02499 -1.67717E-02 0.00335 ];
INF_S7                    (idx, [1:   8]) = [  9.29996E-04 0.01878 -1.68967E-04 0.01025 -4.36144E-05 0.02504  5.29826E-04 0.07194 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11168E-01 0.00011  1.75996E-02 0.00052  1.18977E-03 0.00429  1.48019E+00 0.00047 ];
INF_SP1                   (idx, [1:   8]) = [  2.38678E-01 0.00021  5.11889E-03 0.00115  5.07243E-04 0.00661  3.92748E-01 0.00054 ];
INF_SP2                   (idx, [1:   8]) = [  9.77338E-02 0.00032 -1.55682E-03 0.00225  2.76606E-04 0.00764  9.29218E-02 0.00115 ];
INF_SP3                   (idx, [1:   8]) = [  9.12762E-03 0.00238 -1.81810E-03 0.00182  9.65098E-05 0.02220  2.78388E-02 0.00254 ];
INF_SP4                   (idx, [1:   8]) = [ -9.72238E-03 0.00177 -5.89885E-04 0.00484  5.86179E-07 1.00000 -8.75314E-03 0.00787 ];
INF_SP5                   (idx, [1:   8]) = [  1.50409E-04 0.09216  2.85459E-05 0.08589 -3.94023E-05 0.02751  6.61624E-03 0.00738 ];
INF_SP6                   (idx, [1:   8]) = [  5.23849E-03 0.00326 -1.35191E-04 0.01978 -5.13254E-05 0.02499 -1.67717E-02 0.00335 ];
INF_SP7                   (idx, [1:   8]) = [  9.29824E-04 0.01883 -1.68967E-04 0.01025 -4.36144E-05 0.02504  5.29826E-04 0.07194 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31817E-01 0.00058  1.14786E+00 0.00895 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33476E-01 0.00079  1.22949E+00 0.01079 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33487E-01 0.00081  1.23473E+00 0.01056 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28563E-01 0.00085  1.01085E+00 0.00824 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43793E+00 0.00058  2.90948E-01 0.00886 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42772E+00 0.00079  2.71889E-01 0.01101 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42765E+00 0.00081  2.70682E-01 0.01046 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45842E+00 0.00085  3.30274E-01 0.00796 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.71676E-03 0.00863  1.46413E-04 0.05500  9.65786E-04 0.02162  8.77714E-04 0.02144  2.57895E-03 0.01287  8.80590E-04 0.02313  2.67302E-04 0.04105 ];
LAMBDA                    (idx, [1:  14]) = [  7.84732E-01 0.02134  1.25022E-02 0.00041  3.11871E-02 0.00059  1.10253E-01 0.00055  3.21749E-01 0.00041  1.32886E+00 0.00133  8.86774E+00 0.00543 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0007' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:45:38 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:08:28 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292338 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.93398E-01  1.00395E+00  9.99737E-01  1.00176E+00  1.00116E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14618E-02 0.00116  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88538E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05715E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06145E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67155E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.48004E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.47919E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.36577E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.12524E-01 0.00116  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000817 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00016E+04 0.00081 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00016E+04 0.00081 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.11862E+02 ;
RUNNING_TIME              (idx, 1)        =  8.28387E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.82450E-01  4.82450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.39417E-01  1.05167E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  8.13832E+01  3.34823E+00  2.61643E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  7.26800E-01  2.88667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.63333E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  8.28385E+01  1.25070E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97186 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99939E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79576E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.39717E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.74924E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.06177E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.78164E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.24391E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61550E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.62482E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.55964E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.52959E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  8.81062E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.58242E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.78579E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.77134E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.35468E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.31237E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.41346E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.45628E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.20992E+13 ;
CS137_ACTIVITY            (idx, 1)        =  1.68372E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.17942E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.36636E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.37817E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.25304E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.68597E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 13 ;
BURNUP                     (idx, [1:  2])  = [  1.25000E+01  1.25011E+01 ];
BURN_DAYS                 (idx, 1)        =  3.12500E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.03662E-01 0.00105 ];
U233_FISS                 (idx, [1:   4]) = [  7.06421E+09 1.00000  5.02639E-07 1.00000 ];
U235_FISS                 (idx, [1:   4]) = [  7.74998E+15 0.00092  5.64642E-01 0.00066 ];
U238_FISS                 (idx, [1:   4]) = [  1.02405E+15 0.00283  7.45960E-02 0.00262 ];
PU239_FISS                (idx, [1:   4]) = [  4.42305E+15 0.00124  3.22260E-01 0.00111 ];
PU240_FISS                (idx, [1:   4]) = [  5.39487E+12 0.03701  3.92863E-04 0.03698 ];
PU241_FISS                (idx, [1:   4]) = [  5.12314E+14 0.00372  3.73272E-02 0.00369 ];
U235_CAPT                 (idx, [1:   4]) = [  1.72723E+15 0.00206  7.55048E-02 0.00199 ];
U238_CAPT                 (idx, [1:   4]) = [  8.87330E+15 0.00105  3.87868E-01 0.00074 ];
PU239_CAPT                (idx, [1:   4]) = [  2.45026E+15 0.00170  1.07116E-01 0.00166 ];
PU240_CAPT                (idx, [1:   4]) = [  1.36136E+15 0.00230  5.95123E-02 0.00225 ];
PU241_CAPT                (idx, [1:   4]) = [  1.82918E+14 0.00659  7.99676E-03 0.00658 ];
XE135_CAPT                (idx, [1:   4]) = [  7.45629E+14 0.00322  3.25981E-02 0.00323 ];
SM149_CAPT                (idx, [1:   4]) = [  2.04535E+14 0.00583  8.94189E-03 0.00583 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000817 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.77718E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000817 5.00778E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3098730 3.10312E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1859369 1.86192E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 42718 4.27339E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000817 5.00778E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.05360E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 3.7E-09  4.50057E+05 3.7E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.59910E+16 2.2E-05  3.59910E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37183E+16 4.3E-06  1.37183E+16 4.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.28480E+16 0.00045  1.72265E+16 0.00046  5.62156E+15 0.00122 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.65663E+16 0.00028  3.09448E+16 0.00025  5.62156E+15 0.00122 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.68597E+16 0.00050  3.68597E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.72594E+18 0.00048  4.60562E+17 0.00046  1.26538E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.15053E+14 0.00486 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.68814E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.28427E+18 0.00064 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11060E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11060E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.57087E+00 0.00050 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.85597E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.43371E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24126E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94444E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96993E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.85450E-01 0.00056 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.77028E-01 0.00056 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.62357E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04765E+02 4.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.77105E-01 0.00057  9.71569E-01 0.00057  5.45954E-03 0.01027 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.77437E-01 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  9.76551E-01 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.77437E-01 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  9.85870E-01 0.00029 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71787E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71795E+01 9.0E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.94948E-07 0.00380 ];
IMP_EALF                  (idx, [1:   2]) = [  6.92315E-07 0.00154 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.55031E-01 0.00292 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.54038E-01 0.00112 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.84082E-03 0.00618  1.59126E-04 0.03797  9.89091E-04 0.01480  9.34713E-04 0.01423  2.58727E-03 0.00920  8.99497E-04 0.01522  2.71118E-04 0.02779 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.75569E-01 0.01421  9.64748E-03 0.02448  3.10991E-02 0.00041  1.10427E-01 0.00043  3.22147E-01 0.00030  1.31566E+00 0.00240  8.24130E+00 0.01292 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.56583E-03 0.00917  1.46687E-04 0.05487  9.53835E-04 0.02130  9.13322E-04 0.02230  2.44036E-03 0.01424  8.59921E-04 0.02378  2.51700E-04 0.04054 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.67175E-01 0.02053  1.25267E-02 0.00074  3.10923E-02 0.00062  1.10416E-01 0.00062  3.22031E-01 0.00044  1.32059E+00 0.00169  8.76226E+00 0.00741 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.56078E-05 0.00136  2.55958E-05 0.00136  2.78513E-05 0.01613 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.50173E-05 0.00123  2.50056E-05 0.00124  2.72115E-05 0.01614 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.58696E-03 0.01041  1.47035E-04 0.06090  9.47150E-04 0.02258  9.32002E-04 0.02431  2.42908E-03 0.01628  8.81527E-04 0.02625  2.50162E-04 0.04586 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.80770E-01 0.02495  1.25121E-02 0.00074  3.11100E-02 0.00074  1.10269E-01 0.00069  3.21933E-01 0.00050  1.32243E+00 0.00189  8.84254E+00 0.00916 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.50651E-05 0.00285  2.50508E-05 0.00286  2.62495E-05 0.03701 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.44869E-05 0.00278  2.44730E-05 0.00279  2.56490E-05 0.03703 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.49370E-03 0.03404  1.29147E-04 0.19951  8.25683E-04 0.08811  9.43087E-04 0.07522  2.46064E-03 0.05179  9.04011E-04 0.08796  2.31137E-04 0.15999 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.07633E-01 0.06962  1.25172E-02 0.00234  3.10763E-02 0.00187  1.10458E-01 0.00175  3.21891E-01 0.00145  1.31640E+00 0.00558  8.97541E+00 0.02013 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.46904E-03 0.03343  1.27774E-04 0.19632  8.38519E-04 0.08589  9.54481E-04 0.07332  2.41455E-03 0.05130  8.95023E-04 0.08598  2.38694E-04 0.15229 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.20075E-01 0.07053  1.25172E-02 0.00234  3.10711E-02 0.00186  1.10435E-01 0.00173  3.21825E-01 0.00143  1.31647E+00 0.00554  8.99492E+00 0.02005 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.19458E+02 0.03374 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.53664E-05 0.00085 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.47814E-05 0.00061 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.57126E-03 0.00669 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.19735E+02 0.00677 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.66224E-07 0.00078 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.73202E-06 0.00046  2.73180E-06 0.00046  2.77308E-06 0.00584 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.80708E-05 0.00093  3.80879E-05 0.00093  3.52871E-05 0.01251 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.41304E-01 0.00033  6.41308E-01 0.00034  6.52208E-01 0.00938 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04845E+01 0.01400 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.47919E+01 0.00050  3.33378E+01 0.00049 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.74026E+04 0.00275  3.00640E+05 0.00215  6.09614E+05 0.00104  6.50886E+05 0.00070  5.98566E+05 0.00073  6.42043E+05 0.00059  4.36576E+05 0.00061  3.86097E+05 0.00071  2.95225E+05 0.00064  2.41356E+05 0.00077  2.08104E+05 0.00075  1.87696E+05 0.00083  1.73120E+05 0.00073  1.64725E+05 0.00090  1.60371E+05 0.00084  1.38421E+05 0.00100  1.36731E+05 0.00091  1.35733E+05 0.00117  1.33188E+05 0.00069  2.59947E+05 0.00082  2.50639E+05 0.00082  1.81255E+05 0.00074  1.17245E+05 0.00068  1.35384E+05 0.00068  1.28607E+05 0.00078  1.14873E+05 0.00106  1.87200E+05 0.00091  4.32578E+04 0.00180  5.40213E+04 0.00176  4.91034E+04 0.00172  2.85961E+04 0.00144  4.94993E+04 0.00142  3.33192E+04 0.00164  2.74910E+04 0.00199  4.87591E+03 0.00335  4.34930E+03 0.00320  3.89483E+03 0.00333  3.74974E+03 0.00352  3.82858E+03 0.00350  4.17807E+03 0.00326  4.87020E+03 0.00346  4.81667E+03 0.00357  9.36324E+03 0.00335  1.53054E+04 0.00279  1.97116E+04 0.00234  5.22685E+04 0.00150  5.51539E+04 0.00117  5.88351E+04 0.00127  3.83843E+04 0.00116  2.71526E+04 0.00124  2.02515E+04 0.00195  2.37854E+04 0.00110  4.62913E+04 0.00167  6.51481E+04 0.00156  1.28979E+05 0.00139  2.02364E+05 0.00139  3.05363E+05 0.00166  1.96356E+05 0.00183  1.41031E+05 0.00181  1.01604E+05 0.00195  9.16095E+04 0.00216  9.01721E+04 0.00198  7.53630E+04 0.00208  5.09807E+04 0.00222  4.71591E+04 0.00221  4.19705E+04 0.00227  3.54862E+04 0.00209  2.80479E+04 0.00223  1.87809E+04 0.00222  6.68253E+03 0.00260 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.84969E-01 0.00054 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.37637E+18 0.00050  3.49606E+17 0.00163 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38371E-01 0.00012  1.55400E+00 0.00051 ];
INF_CAPT                  (idx, [1:   4]) = [  7.58512E-03 0.00064  3.54957E-02 0.00088 ];
INF_ABS                   (idx, [1:   4]) = [  9.50344E-03 0.00050  6.71894E-02 0.00125 ];
INF_FISS                  (idx, [1:   4]) = [  1.91833E-03 0.00038  3.16937E-02 0.00167 ];
INF_NSF                   (idx, [1:   4]) = [  5.08565E-03 0.00037  8.29430E-02 0.00171 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.65109E+00 5.5E-05  2.61702E+00 6.0E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05000E+02 8.9E-06  2.04709E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.66246E-08 0.00041  2.57261E-06 0.00021 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28860E-01 0.00013  1.48675E+00 0.00060 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43877E-01 0.00018  3.94489E-01 0.00067 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62353E-02 0.00027  9.33689E-02 0.00092 ];
INF_SCATT3                (idx, [1:   4]) = [  7.33760E-03 0.00247  2.79822E-02 0.00237 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03205E-02 0.00165 -8.68075E-03 0.00592 ];
INF_SCATT5                (idx, [1:   4]) = [  1.60410E-04 0.09705  6.64197E-03 0.00915 ];
INF_SCATT6                (idx, [1:   4]) = [  5.11177E-03 0.00302 -1.69427E-02 0.00301 ];
INF_SCATT7                (idx, [1:   4]) = [  7.61270E-04 0.01867  4.19542E-04 0.09827 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28902E-01 0.00013  1.48675E+00 0.00060 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43877E-01 0.00018  3.94489E-01 0.00067 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62355E-02 0.00027  9.33689E-02 0.00092 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.33765E-03 0.00246  2.79822E-02 0.00237 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03207E-02 0.00165 -8.68075E-03 0.00592 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.60298E-04 0.09707  6.64197E-03 0.00915 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.11179E-03 0.00302 -1.69427E-02 0.00301 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.61201E-04 0.01866  4.19542E-04 0.09827 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12766E-01 0.00032  1.00692E+00 0.00043 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56667E+00 0.00032  3.31045E-01 0.00043 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.46179E-03 0.00050  6.71894E-02 0.00125 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69732E-02 0.00022  6.84362E-02 0.00138 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11398E-01 0.00012  1.74625E-02 0.00040  1.18063E-03 0.00409  1.48557E+00 0.00060 ];
INF_S1                    (idx, [1:   8]) = [  2.38812E-01 0.00018  5.06547E-03 0.00090  5.04237E-04 0.00827  3.93985E-01 0.00067 ];
INF_S2                    (idx, [1:   8]) = [  9.78002E-02 0.00028 -1.56485E-03 0.00215  2.75990E-04 0.00895  9.30929E-02 0.00093 ];
INF_S3                    (idx, [1:   8]) = [  9.14151E-03 0.00188 -1.80390E-03 0.00151  9.71147E-05 0.02008  2.78851E-02 0.00237 ];
INF_S4                    (idx, [1:   8]) = [ -9.74088E-03 0.00174 -5.79611E-04 0.00419 -4.17173E-07 1.00000 -8.68033E-03 0.00594 ];
INF_S5                    (idx, [1:   8]) = [  1.28852E-04 0.11567  3.15579E-05 0.08578 -3.95872E-05 0.03423  6.68156E-03 0.00904 ];
INF_S6                    (idx, [1:   8]) = [  5.24877E-03 0.00316 -1.36998E-04 0.02153 -5.22119E-05 0.02794 -1.68905E-02 0.00302 ];
INF_S7                    (idx, [1:   8]) = [  9.25697E-04 0.01455 -1.64427E-04 0.01628 -4.63602E-05 0.02992  4.65902E-04 0.08819 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11440E-01 0.00012  1.74625E-02 0.00040  1.18063E-03 0.00409  1.48557E+00 0.00060 ];
INF_SP1                   (idx, [1:   8]) = [  2.38812E-01 0.00018  5.06547E-03 0.00090  5.04237E-04 0.00827  3.93985E-01 0.00067 ];
INF_SP2                   (idx, [1:   8]) = [  9.78003E-02 0.00028 -1.56485E-03 0.00215  2.75990E-04 0.00895  9.30929E-02 0.00093 ];
INF_SP3                   (idx, [1:   8]) = [  9.14155E-03 0.00188 -1.80390E-03 0.00151  9.71147E-05 0.02008  2.78851E-02 0.00237 ];
INF_SP4                   (idx, [1:   8]) = [ -9.74105E-03 0.00174 -5.79611E-04 0.00419 -4.17173E-07 1.00000 -8.68033E-03 0.00594 ];
INF_SP5                   (idx, [1:   8]) = [  1.28740E-04 0.11576  3.15579E-05 0.08578 -3.95872E-05 0.03423  6.68156E-03 0.00904 ];
INF_SP6                   (idx, [1:   8]) = [  5.24879E-03 0.00317 -1.36998E-04 0.02153 -5.22119E-05 0.02794 -1.68905E-02 0.00302 ];
INF_SP7                   (idx, [1:   8]) = [  9.25628E-04 0.01454 -1.64427E-04 0.01628 -4.63602E-05 0.02992  4.65902E-04 0.08819 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32017E-01 0.00069  1.19457E+00 0.00583 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33483E-01 0.00087  1.30317E+00 0.00743 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33734E-01 0.00091  1.28266E+00 0.00665 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28904E-01 0.00106  1.03773E+00 0.00630 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43670E+00 0.00069  2.79268E-01 0.00584 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42768E+00 0.00087  2.56131E-01 0.00752 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42615E+00 0.00091  2.60153E-01 0.00668 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45625E+00 0.00106  3.21519E-01 0.00630 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.56583E-03 0.00917  1.46687E-04 0.05487  9.53835E-04 0.02130  9.13322E-04 0.02230  2.44036E-03 0.01424  8.59921E-04 0.02378  2.51700E-04 0.04054 ];
LAMBDA                    (idx, [1:  14]) = [  7.67175E-01 0.02053  1.25267E-02 0.00074  3.10923E-02 0.00062  1.10416E-01 0.00062  3.22031E-01 0.00044  1.32059E+00 0.00169  8.76226E+00 0.00741 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0007' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:45:38 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:14:30 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292338 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.95623E-01  1.00436E+00  9.99636E-01  1.00026E+00  1.00012E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15061E-02 0.00107  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88494E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06093E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06524E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67310E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.48098E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.48010E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.34769E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.14177E-01 0.00107  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000974 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00019E+04 0.00084 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00019E+04 0.00084 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.41973E+02 ;
RUNNING_TIME              (idx, 1)        =  8.88673E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.82450E-01  4.82450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.60133E-01  1.00000E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  8.73358E+01  3.33750E+00  2.61512E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  7.81933E-01  2.69833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.89500E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  8.88672E+01  1.25091E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97340 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99911E+00 0.00021 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79961E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.44647E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.73666E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.03671E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.83119E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.27918E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61524E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.60871E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.89905E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.61367E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.17985E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.85347E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.19198E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.82832E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.58339E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.33599E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.44869E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.49026E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.71567E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.01847E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.22489E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.33697E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.41619E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.26753E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.78957E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 14 ;
BURNUP                     (idx, [1:  2])  = [  1.50000E+01  1.50014E+01 ];
BURN_DAYS                 (idx, 1)        =  3.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.26839E-01 0.00112 ];
U233_FISS                 (idx, [1:   4]) = [  7.48762E+09 1.00000  5.39084E-07 1.00000 ];
U235_FISS                 (idx, [1:   4]) = [  7.11631E+15 0.00088  5.19625E-01 0.00071 ];
U238_FISS                 (idx, [1:   4]) = [  1.05507E+15 0.00278  7.70282E-02 0.00261 ];
PU239_FISS                (idx, [1:   4]) = [  4.82667E+15 0.00120  3.52425E-01 0.00101 ];
PU240_FISS                (idx, [1:   4]) = [  6.87897E+12 0.03286  5.02601E-04 0.03286 ];
PU241_FISS                (idx, [1:   4]) = [  6.77244E+14 0.00337  4.94478E-02 0.00328 ];
U235_CAPT                 (idx, [1:   4]) = [  1.58739E+15 0.00223  6.63470E-02 0.00218 ];
U238_CAPT                 (idx, [1:   4]) = [  9.07767E+15 0.00106  3.79376E-01 0.00072 ];
PU239_CAPT                (idx, [1:   4]) = [  2.67542E+15 0.00170  1.11822E-01 0.00162 ];
PU240_CAPT                (idx, [1:   4]) = [  1.59973E+15 0.00221  6.68606E-02 0.00213 ];
PU241_CAPT                (idx, [1:   4]) = [  2.41377E+14 0.00543  1.00880E-02 0.00539 ];
XE135_CAPT                (idx, [1:   4]) = [  7.52784E+14 0.00285  3.14643E-02 0.00283 ];
SM149_CAPT                (idx, [1:   4]) = [  2.12889E+14 0.00576  8.89811E-03 0.00575 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000974 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.96994E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000974 5.00797E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3152453 3.15689E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1804536 1.80709E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 43985 4.39943E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000974 5.00797E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.33299E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 3.7E-09  4.50057E+05 3.7E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.62187E+16 2.2E-05  3.62187E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36988E+16 4.4E-06  1.36988E+16 4.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.39371E+16 0.00044  1.81460E+16 0.00047  5.79104E+15 0.00120 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.76359E+16 0.00028  3.18448E+16 0.00026  5.79104E+15 0.00120 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.78957E+16 0.00049  3.78957E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.77310E+18 0.00047  4.72017E+17 0.00047  1.30109E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.33474E+14 0.00508 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.79694E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.32072E+18 0.00064 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10770E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10770E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.54833E+00 0.00055 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.85022E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.39418E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24047E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94275E-01 3.7E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96908E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.64016E-01 0.00060 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.55534E-01 0.00060 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.64393E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05057E+02 4.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.55728E-01 0.00061  9.50395E-01 0.00060  5.13883E-03 0.01012 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.55457E-01 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  9.55862E-01 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.55457E-01 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  9.63934E-01 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71363E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71408E+01 9.4E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.25136E-07 0.00385 ];
IMP_EALF                  (idx, [1:   2]) = [  7.19710E-07 0.00161 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.62453E-01 0.00293 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.61808E-01 0.00113 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.77576E-03 0.00635  1.49890E-04 0.03857  9.85964E-04 0.01472  8.94334E-04 0.01522  2.58119E-03 0.00939  8.88822E-04 0.01615  2.75563E-04 0.02682 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.83391E-01 0.01420  9.46004E-03 0.02544  3.10335E-02 0.00046  1.10621E-01 0.00046  3.22303E-01 0.00031  1.30536E+00 0.00161  8.22281E+00 0.01306 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.47375E-03 0.00915  1.43599E-04 0.05435  9.36082E-04 0.02295  8.41545E-04 0.02394  2.42900E-03 0.01411  8.55654E-04 0.02331  2.67873E-04 0.04248 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.94307E-01 0.02237  1.25122E-02 0.00055  3.10419E-02 0.00065  1.10581E-01 0.00068  3.22255E-01 0.00046  1.30389E+00 0.00244  8.74996E+00 0.00782 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.60787E-05 0.00140  2.60621E-05 0.00140  2.89952E-05 0.01599 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.49195E-05 0.00125  2.49037E-05 0.00126  2.77041E-05 0.01599 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.38060E-03 0.01022  1.47809E-04 0.05884  9.25259E-04 0.02578  8.38728E-04 0.02694  2.41339E-03 0.01483  8.08217E-04 0.02650  2.47195E-04 0.04840 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.59983E-01 0.02619  1.25218E-02 0.00097  3.10456E-02 0.00083  1.10622E-01 0.00087  3.22332E-01 0.00053  1.30507E+00 0.00285  8.65704E+00 0.01139 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.55097E-05 0.00294  2.54942E-05 0.00295  2.65579E-05 0.03675 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.43761E-05 0.00289  2.43613E-05 0.00290  2.53725E-05 0.03675 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.58498E-03 0.03444  1.47826E-04 0.19582  9.89210E-04 0.07757  7.47784E-04 0.08612  2.55096E-03 0.04746  9.17757E-04 0.09188  2.31445E-04 0.15950 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.20482E-01 0.07760  1.24883E-02 5.9E-05  3.09508E-02 0.00188  1.10579E-01 0.00200  3.22630E-01 0.00150  1.29480E+00 0.00758  8.57499E+00 0.02605 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.56827E-03 0.03365  1.41400E-04 0.19125  9.93331E-04 0.07568  7.55399E-04 0.08492  2.52162E-03 0.04635  9.28169E-04 0.08865  2.28353E-04 0.15329 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.16637E-01 0.07545  1.24883E-02 5.8E-05  3.09582E-02 0.00187  1.10599E-01 0.00200  3.22618E-01 0.00151  1.29506E+00 0.00746  8.57337E+00 0.02605 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.20358E+02 0.03468 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.58214E-05 0.00089 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.46733E-05 0.00061 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.45470E-03 0.00603 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.11321E+02 0.00606 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.66243E-07 0.00078 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.71478E-06 0.00043  2.71459E-06 0.00043  2.75162E-06 0.00589 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.83083E-05 0.00094  3.83239E-05 0.00095  3.55811E-05 0.01039 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.37315E-01 0.00034  6.37379E-01 0.00035  6.39697E-01 0.01004 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03973E+01 0.01321 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.48010E+01 0.00050  3.32440E+01 0.00050 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.79844E+04 0.00339  3.02764E+05 0.00187  6.10458E+05 0.00093  6.51514E+05 0.00079  5.99130E+05 0.00063  6.40867E+05 0.00093  4.35820E+05 0.00074  3.85736E+05 0.00074  2.94573E+05 0.00056  2.41221E+05 0.00067  2.07659E+05 0.00091  1.87234E+05 0.00086  1.73005E+05 0.00095  1.64206E+05 0.00074  1.60147E+05 0.00100  1.38559E+05 0.00073  1.36879E+05 0.00090  1.35569E+05 0.00103  1.33147E+05 0.00081  2.59756E+05 0.00072  2.51271E+05 0.00072  1.80941E+05 0.00080  1.17437E+05 0.00105  1.35140E+05 0.00065  1.28866E+05 0.00079  1.14648E+05 0.00088  1.86958E+05 0.00070  4.32513E+04 0.00145  5.40077E+04 0.00130  4.90238E+04 0.00160  2.85853E+04 0.00194  4.94736E+04 0.00173  3.30346E+04 0.00196  2.72811E+04 0.00229  4.78694E+03 0.00335  4.22501E+03 0.00318  3.71890E+03 0.00379  3.58437E+03 0.00316  3.64745E+03 0.00338  4.01222E+03 0.00532  4.68825E+03 0.00333  4.69199E+03 0.00242  9.19148E+03 0.00250  1.50914E+04 0.00311  1.94444E+04 0.00192  5.19327E+04 0.00131  5.47425E+04 0.00177  5.82051E+04 0.00104  3.79437E+04 0.00123  2.68374E+04 0.00137  1.99145E+04 0.00119  2.34161E+04 0.00162  4.57277E+04 0.00149  6.43805E+04 0.00139  1.28058E+05 0.00127  2.01762E+05 0.00126  3.05145E+05 0.00126  1.96477E+05 0.00126  1.41184E+05 0.00139  1.01691E+05 0.00141  9.15331E+04 0.00162  9.03049E+04 0.00161  7.55182E+04 0.00162  5.11889E+04 0.00141  4.73662E+04 0.00139  4.20936E+04 0.00146  3.56383E+04 0.00156  2.81788E+04 0.00152  1.88816E+04 0.00206  6.65102E+03 0.00267 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.64347E-01 0.00046 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.41449E+18 0.00045  3.58653E+17 0.00123 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38497E-01 0.00011  1.55875E+00 0.00041 ];
INF_CAPT                  (idx, [1:   4]) = [  7.76023E-03 0.00057  3.61400E-02 0.00074 ];
INF_ABS                   (idx, [1:   4]) = [  9.62021E-03 0.00050  6.70053E-02 0.00097 ];
INF_FISS                  (idx, [1:   4]) = [  1.85998E-03 0.00061  3.08652E-02 0.00126 ];
INF_NSF                   (idx, [1:   4]) = [  4.95922E-03 0.00059  8.14416E-02 0.00130 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.66628E+00 7.7E-05  2.63862E+00 6.5E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05209E+02 9.6E-06  2.05021E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.63611E-08 0.00040  2.57732E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28879E-01 0.00011  1.49178E+00 0.00047 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43981E-01 0.00022  3.95676E-01 0.00050 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62717E-02 0.00030  9.35788E-02 0.00111 ];
INF_SCATT3                (idx, [1:   4]) = [  7.33461E-03 0.00350  2.79561E-02 0.00256 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03021E-02 0.00178 -8.83857E-03 0.00666 ];
INF_SCATT5                (idx, [1:   4]) = [  1.75611E-04 0.09141  6.60668E-03 0.00577 ];
INF_SCATT6                (idx, [1:   4]) = [  5.11378E-03 0.00288 -1.70979E-02 0.00315 ];
INF_SCATT7                (idx, [1:   4]) = [  7.60658E-04 0.01769  4.73484E-04 0.07422 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28921E-01 0.00011  1.49178E+00 0.00047 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43982E-01 0.00022  3.95676E-01 0.00050 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62721E-02 0.00030  9.35788E-02 0.00111 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.33472E-03 0.00350  2.79561E-02 0.00256 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03019E-02 0.00178 -8.83857E-03 0.00666 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.75631E-04 0.09136  6.60668E-03 0.00577 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.11392E-03 0.00288 -1.70979E-02 0.00315 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.60743E-04 0.01761  4.73484E-04 0.07422 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12529E-01 0.00030  1.01069E+00 0.00036 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56842E+00 0.00030  3.29809E-01 0.00036 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.57750E-03 0.00050  6.70053E-02 0.00097 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69779E-02 0.00022  6.81418E-02 0.00102 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11519E-01 0.00011  1.73597E-02 0.00033  1.17287E-03 0.00401  1.49061E+00 0.00047 ];
INF_S1                    (idx, [1:   8]) = [  2.38947E-01 0.00021  5.03410E-03 0.00096  4.98761E-04 0.00689  3.95178E-01 0.00050 ];
INF_S2                    (idx, [1:   8]) = [  9.78249E-02 0.00030 -1.55316E-03 0.00225  2.69162E-04 0.00913  9.33096E-02 0.00111 ];
INF_S3                    (idx, [1:   8]) = [  9.12593E-03 0.00271 -1.79132E-03 0.00162  9.44597E-05 0.01763  2.78617E-02 0.00255 ];
INF_S4                    (idx, [1:   8]) = [ -9.73310E-03 0.00196 -5.68955E-04 0.00384 -1.79491E-07 1.00000 -8.83839E-03 0.00664 ];
INF_S5                    (idx, [1:   8]) = [  1.42966E-04 0.11567  3.26449E-05 0.07162 -3.96660E-05 0.04355  6.64634E-03 0.00568 ];
INF_S6                    (idx, [1:   8]) = [  5.24853E-03 0.00277 -1.34751E-04 0.01659 -5.14753E-05 0.03431 -1.70464E-02 0.00317 ];
INF_S7                    (idx, [1:   8]) = [  9.29535E-04 0.01410 -1.68877E-04 0.01343 -4.48050E-05 0.02712  5.18289E-04 0.06737 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11562E-01 0.00011  1.73597E-02 0.00033  1.17287E-03 0.00401  1.49061E+00 0.00047 ];
INF_SP1                   (idx, [1:   8]) = [  2.38948E-01 0.00021  5.03410E-03 0.00096  4.98761E-04 0.00689  3.95178E-01 0.00050 ];
INF_SP2                   (idx, [1:   8]) = [  9.78252E-02 0.00030 -1.55316E-03 0.00225  2.69162E-04 0.00913  9.33096E-02 0.00111 ];
INF_SP3                   (idx, [1:   8]) = [  9.12604E-03 0.00271 -1.79132E-03 0.00162  9.44597E-05 0.01763  2.78617E-02 0.00255 ];
INF_SP4                   (idx, [1:   8]) = [ -9.73297E-03 0.00196 -5.68955E-04 0.00384 -1.79491E-07 1.00000 -8.83839E-03 0.00664 ];
INF_SP5                   (idx, [1:   8]) = [  1.42986E-04 0.11560  3.26449E-05 0.07162 -3.96660E-05 0.04355  6.64634E-03 0.00568 ];
INF_SP6                   (idx, [1:   8]) = [  5.24867E-03 0.00278 -1.34751E-04 0.01659 -5.14753E-05 0.03431 -1.70464E-02 0.00317 ];
INF_SP7                   (idx, [1:   8]) = [  9.29620E-04 0.01403 -1.68877E-04 0.01343 -4.48050E-05 0.02712  5.18289E-04 0.06737 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31826E-01 0.00073  1.18776E+00 0.00605 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33470E-01 0.00101  1.29277E+00 0.00716 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33267E-01 0.00091  1.28214E+00 0.00764 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28810E-01 0.00106  1.02967E+00 0.00739 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43788E+00 0.00073  2.80881E-01 0.00594 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42777E+00 0.00100  2.58162E-01 0.00717 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42901E+00 0.00092  2.60337E-01 0.00743 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45685E+00 0.00106  3.24146E-01 0.00727 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.47375E-03 0.00915  1.43599E-04 0.05435  9.36082E-04 0.02295  8.41545E-04 0.02394  2.42900E-03 0.01411  8.55654E-04 0.02331  2.67873E-04 0.04248 ];
LAMBDA                    (idx, [1:  14]) = [  7.94307E-01 0.02237  1.25122E-02 0.00055  3.10419E-02 0.00065  1.10581E-01 0.00068  3.22255E-01 0.00046  1.30389E+00 0.00244  8.74996E+00 0.00782 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0007' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:45:38 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:20:31 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292338 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.96832E-01  1.00429E+00  9.99036E-01  1.00051E+00  9.99333E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15504E-02 0.00115  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88450E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06183E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06613E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67485E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.47416E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.47328E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.32657E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.15437E-01 0.00117  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000807 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00016E+04 0.00088 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00016E+04 0.00088 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.72007E+02 ;
RUNNING_TIME              (idx, 1)        =  9.48807E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.82450E-01  4.82450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.81667E-01  1.05667E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.32722E+01  3.32798E+00  2.60843E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  8.36817E-01  2.68000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.06667E-02  8.83333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.48806E+01  1.24984E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97474 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00063E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80292E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.49803E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.72782E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.54231E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.87936E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.31381E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61863E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.59640E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.27990E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.69397E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.52455E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.12660E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.55351E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.88131E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.80165E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.35862E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.48023E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.52182E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.29340E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.35232E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.27204E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.31235E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.84136E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.28171E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.90563E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 15 ;
BURNUP                     (idx, [1:  2])  = [  1.75000E+01  1.75016E+01 ];
BURN_DAYS                 (idx, 1)        =  4.37500E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.50553E-01 0.00103 ];
U235_FISS                 (idx, [1:   4]) = [  6.53070E+15 0.00106  4.76936E-01 0.00085 ];
U238_FISS                 (idx, [1:   4]) = [  1.08129E+15 0.00259  7.89556E-02 0.00239 ];
PU239_FISS                (idx, [1:   4]) = [  5.19777E+15 0.00114  3.79598E-01 0.00098 ];
PU240_FISS                (idx, [1:   4]) = [  8.25112E+12 0.03024  6.01640E-04 0.03010 ];
PU241_FISS                (idx, [1:   4]) = [  8.58999E+14 0.00297  6.27316E-02 0.00290 ];
U233_CAPT                 (idx, [1:   4]) = [  7.62721E+09 1.00000  3.02618E-07 1.00000 ];
U235_CAPT                 (idx, [1:   4]) = [  1.46134E+15 0.00246  5.82614E-02 0.00237 ];
U238_CAPT                 (idx, [1:   4]) = [  9.31381E+15 0.00102  3.71320E-01 0.00073 ];
PU239_CAPT                (idx, [1:   4]) = [  2.87461E+15 0.00163  1.14614E-01 0.00157 ];
PU240_CAPT                (idx, [1:   4]) = [  1.83635E+15 0.00216  7.32111E-02 0.00204 ];
PU241_CAPT                (idx, [1:   4]) = [  3.07441E+14 0.00526  1.22587E-02 0.00525 ];
XE135_CAPT                (idx, [1:   4]) = [  7.59388E+14 0.00321  3.02795E-02 0.00322 ];
SM149_CAPT                (idx, [1:   4]) = [  2.22330E+14 0.00602  8.86684E-03 0.00608 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000807 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.01961E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000807 5.00802E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3206294 3.21101E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1750606 1.75309E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 43907 4.39160E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000807 5.00802E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -8.38190E-09 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 3.7E-09  4.50057E+05 3.7E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.64334E+16 2.3E-05  3.64334E+16 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36802E+16 4.5E-06  1.36802E+16 4.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.50555E+16 0.00044  1.91113E+16 0.00043  5.94423E+15 0.00125 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.87357E+16 0.00029  3.27915E+16 0.00025  5.94423E+15 0.00125 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.90563E+16 0.00050  3.90563E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.82484E+18 0.00049  4.85590E+17 0.00045  1.33925E+18 0.00056 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.43098E+14 0.00508 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.90788E+16 0.00030 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.35853E+18 0.00067 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10481E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10481E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.52872E+00 0.00054 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.83262E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.34829E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23941E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94272E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96927E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.42061E-01 0.00062 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.33787E-01 0.00062 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.66322E+00 2.8E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05336E+02 4.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.33751E-01 0.00064  9.28882E-01 0.00062  4.90434E-03 0.01098 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.33856E-01 0.00030 ];
COL_KEFF                  (idx, [1:   2]) = [  9.32961E-01 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.33856E-01 0.00030 ];
ABS_KINF                  (idx, [1:   2]) = [  9.42138E-01 0.00029 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71019E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71003E+01 9.3E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.50316E-07 0.00369 ];
IMP_EALF                  (idx, [1:   2]) = [  7.49477E-07 0.00160 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.69461E-01 0.00267 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.69674E-01 0.00121 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.82395E-03 0.00649  1.51822E-04 0.03865  1.02348E-03 0.01515  9.06725E-04 0.01626  2.54589E-03 0.00994  9.18559E-04 0.01469  2.77474E-04 0.02701 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.78585E-01 0.01419  9.47196E-03 0.02544  3.08514E-02 0.00205  1.10773E-01 0.00051  3.22462E-01 0.00034  1.29746E+00 0.00163  8.08821E+00 0.01359 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.28863E-03 0.00933  1.46391E-04 0.06021  9.36760E-04 0.02259  7.92565E-04 0.02379  2.31914E-03 0.01399  8.37015E-04 0.02276  2.56752E-04 0.04326 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.87245E-01 0.02207  1.25194E-02 0.00067  3.09012E-02 0.00063  1.10748E-01 0.00074  3.22633E-01 0.00049  1.29264E+00 0.00262  8.61353E+00 0.00912 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.66519E-05 0.00142  2.66435E-05 0.00143  2.83651E-05 0.01692 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.48816E-05 0.00129  2.48737E-05 0.00130  2.64856E-05 0.01692 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.26528E-03 0.01106  1.41970E-04 0.06503  9.07197E-04 0.02514  8.31410E-04 0.02697  2.29433E-03 0.01692  8.50702E-04 0.02622  2.39673E-04 0.05016 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.67006E-01 0.02597  1.25152E-02 0.00089  3.08931E-02 0.00086  1.10915E-01 0.00096  3.22669E-01 0.00060  1.29359E+00 0.00334  8.65528E+00 0.01250 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.60464E-05 0.00324  2.60348E-05 0.00326  2.57404E-05 0.04308 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.43164E-05 0.00318  2.43056E-05 0.00321  2.40076E-05 0.04290 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.05829E-03 0.03549  1.02590E-04 0.27388  9.48985E-04 0.08395  7.12070E-04 0.09124  2.26078E-03 0.05593  8.53823E-04 0.08493  1.80042E-04 0.16624 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.28689E-01 0.07904  1.25678E-02 0.00438  3.08520E-02 0.00199  1.10874E-01 0.00234  3.23224E-01 0.00169  1.30849E+00 0.00603  8.42227E+00 0.03571 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.03964E-03 0.03460  1.02249E-04 0.26253  9.40978E-04 0.08029  7.22099E-04 0.09234  2.22425E-03 0.05543  8.71344E-04 0.08341  1.78718E-04 0.16446 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.26305E-01 0.07848  1.25678E-02 0.00438  3.08494E-02 0.00199  1.10870E-01 0.00233  3.23142E-01 0.00167  1.30741E+00 0.00603  8.41978E+00 0.03570 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.94813E+02 0.03571 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.63949E-05 0.00087 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.46412E-05 0.00059 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.18930E-03 0.00682 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.96693E+02 0.00689 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.64094E-07 0.00078 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.69479E-06 0.00043  2.69467E-06 0.00043  2.72056E-06 0.00560 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.83710E-05 0.00094  3.83890E-05 0.00094  3.49984E-05 0.01123 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.32754E-01 0.00033  6.32959E-01 0.00034  6.10775E-01 0.01024 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06381E+01 0.01470 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.47328E+01 0.00049  3.32112E+01 0.00051 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.89598E+04 0.00355  3.02204E+05 0.00170  6.10835E+05 0.00079  6.51797E+05 0.00076  5.98033E+05 0.00082  6.41053E+05 0.00078  4.36551E+05 0.00063  3.85645E+05 0.00065  2.95314E+05 0.00068  2.40992E+05 0.00089  2.07830E+05 0.00074  1.87572E+05 0.00095  1.73216E+05 0.00091  1.64672E+05 0.00089  1.60410E+05 0.00098  1.38522E+05 0.00097  1.36976E+05 0.00084  1.35609E+05 0.00082  1.33481E+05 0.00076  2.60115E+05 0.00061  2.50889E+05 0.00062  1.81303E+05 0.00059  1.17567E+05 0.00083  1.35472E+05 0.00094  1.28505E+05 0.00080  1.14868E+05 0.00106  1.85971E+05 0.00081  4.30269E+04 0.00145  5.37118E+04 0.00139  4.88879E+04 0.00157  2.85191E+04 0.00206  4.93152E+04 0.00151  3.29380E+04 0.00187  2.69747E+04 0.00128  4.67299E+03 0.00339  4.04886E+03 0.00313  3.58174E+03 0.00428  3.42577E+03 0.00388  3.49919E+03 0.00307  3.82857E+03 0.00358  4.51806E+03 0.00329  4.58533E+03 0.00353  9.08661E+03 0.00282  1.48274E+04 0.00238  1.92097E+04 0.00177  5.14299E+04 0.00152  5.39887E+04 0.00153  5.77909E+04 0.00126  3.73208E+04 0.00129  2.63805E+04 0.00140  1.95582E+04 0.00157  2.30608E+04 0.00202  4.51518E+04 0.00138  6.36350E+04 0.00153  1.26823E+05 0.00134  2.00175E+05 0.00147  3.03285E+05 0.00136  1.95400E+05 0.00153  1.40489E+05 0.00148  1.01316E+05 0.00166  9.12684E+04 0.00168  8.99770E+04 0.00162  7.52962E+04 0.00201  5.10432E+04 0.00199  4.72132E+04 0.00185  4.20504E+04 0.00183  3.55686E+04 0.00187  2.81044E+04 0.00169  1.87870E+04 0.00207  6.68882E+03 0.00213 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.41227E-01 0.00067 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.45766E+18 0.00061  3.67203E+17 0.00162 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38709E-01 0.00010  1.56122E+00 0.00049 ];
INF_CAPT                  (idx, [1:   4]) = [  7.93148E-03 0.00047  3.67525E-02 0.00094 ];
INF_ABS                   (idx, [1:   4]) = [  9.73381E-03 0.00041  6.68592E-02 0.00127 ];
INF_FISS                  (idx, [1:   4]) = [  1.80233E-03 0.00043  3.01067E-02 0.00168 ];
INF_NSF                   (idx, [1:   4]) = [  4.83183E-03 0.00043  8.00546E-02 0.00173 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.68088E+00 8.9E-05  2.65903E+00 6.4E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05413E+02 1.0E-05  2.05317E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.60115E-08 0.00046  2.58116E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28968E-01 0.00011  1.49429E+00 0.00056 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43945E-01 0.00016  3.96244E-01 0.00062 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62348E-02 0.00032  9.37208E-02 0.00110 ];
INF_SCATT3                (idx, [1:   4]) = [  7.27651E-03 0.00357  2.81275E-02 0.00261 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03123E-02 0.00212 -8.87833E-03 0.00753 ];
INF_SCATT5                (idx, [1:   4]) = [  1.79344E-04 0.12578  6.66565E-03 0.00852 ];
INF_SCATT6                (idx, [1:   4]) = [  5.11670E-03 0.00400 -1.71240E-02 0.00265 ];
INF_SCATT7                (idx, [1:   4]) = [  7.69532E-04 0.02377  3.98721E-04 0.09173 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29011E-01 0.00011  1.49429E+00 0.00056 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43946E-01 0.00016  3.96244E-01 0.00062 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62347E-02 0.00032  9.37208E-02 0.00110 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.27655E-03 0.00358  2.81275E-02 0.00261 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03122E-02 0.00213 -8.87833E-03 0.00753 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.79119E-04 0.12605  6.66565E-03 0.00852 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.11669E-03 0.00399 -1.71240E-02 0.00265 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.69672E-04 0.02375  3.98721E-04 0.09173 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12580E-01 0.00032  1.01298E+00 0.00039 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56804E+00 0.00032  3.29065E-01 0.00039 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.69084E-03 0.00041  6.68592E-02 0.00127 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69756E-02 0.00022  6.80917E-02 0.00112 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11733E-01 0.00010  1.72343E-02 0.00038  1.16081E-03 0.00360  1.49313E+00 0.00056 ];
INF_S1                    (idx, [1:   8]) = [  2.38959E-01 0.00015  4.98644E-03 0.00087  5.00824E-04 0.00558  3.95744E-01 0.00062 ];
INF_S2                    (idx, [1:   8]) = [  9.77946E-02 0.00031 -1.55979E-03 0.00208  2.68666E-04 0.00914  9.34521E-02 0.00110 ];
INF_S3                    (idx, [1:   8]) = [  9.05666E-03 0.00281 -1.78015E-03 0.00182  9.43397E-05 0.01862  2.80332E-02 0.00264 ];
INF_S4                    (idx, [1:   8]) = [ -9.75146E-03 0.00225 -5.60881E-04 0.00375  1.53829E-06 1.00000 -8.87987E-03 0.00756 ];
INF_S5                    (idx, [1:   8]) = [  1.43443E-04 0.15484  3.59006E-05 0.08238 -4.10693E-05 0.04296  6.70672E-03 0.00853 ];
INF_S6                    (idx, [1:   8]) = [  5.25095E-03 0.00378 -1.34245E-04 0.02132 -5.12063E-05 0.02661 -1.70728E-02 0.00266 ];
INF_S7                    (idx, [1:   8]) = [  9.34113E-04 0.01911 -1.64581E-04 0.01263 -4.63987E-05 0.02542  4.45120E-04 0.08282 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11776E-01 0.00010  1.72343E-02 0.00038  1.16081E-03 0.00360  1.49313E+00 0.00056 ];
INF_SP1                   (idx, [1:   8]) = [  2.38959E-01 0.00015  4.98644E-03 0.00087  5.00824E-04 0.00558  3.95744E-01 0.00062 ];
INF_SP2                   (idx, [1:   8]) = [  9.77945E-02 0.00031 -1.55979E-03 0.00208  2.68666E-04 0.00914  9.34521E-02 0.00110 ];
INF_SP3                   (idx, [1:   8]) = [  9.05670E-03 0.00281 -1.78015E-03 0.00182  9.43397E-05 0.01862  2.80332E-02 0.00264 ];
INF_SP4                   (idx, [1:   8]) = [ -9.75133E-03 0.00226 -5.60881E-04 0.00375  1.53829E-06 1.00000 -8.87987E-03 0.00756 ];
INF_SP5                   (idx, [1:   8]) = [  1.43218E-04 0.15520  3.59006E-05 0.08238 -4.10693E-05 0.04296  6.70672E-03 0.00853 ];
INF_SP6                   (idx, [1:   8]) = [  5.25093E-03 0.00377 -1.34245E-04 0.02132 -5.12063E-05 0.02661 -1.70728E-02 0.00266 ];
INF_SP7                   (idx, [1:   8]) = [  9.34253E-04 0.01909 -1.64581E-04 0.01263 -4.63987E-05 0.02542  4.45120E-04 0.08282 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31828E-01 0.00067  1.21218E+00 0.00739 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33591E-01 0.00093  1.29894E+00 0.00855 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33383E-01 0.00089  1.32158E+00 0.01064 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28587E-01 0.00089  1.05625E+00 0.00798 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43786E+00 0.00066  2.75356E-01 0.00756 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42703E+00 0.00093  2.57076E-01 0.00869 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42829E+00 0.00089  2.52917E-01 0.01077 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45826E+00 0.00089  3.16074E-01 0.00817 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.28863E-03 0.00933  1.46391E-04 0.06021  9.36760E-04 0.02259  7.92565E-04 0.02379  2.31914E-03 0.01399  8.37015E-04 0.02276  2.56752E-04 0.04326 ];
LAMBDA                    (idx, [1:  14]) = [  7.87245E-01 0.02207  1.25194E-02 0.00067  3.09012E-02 0.00063  1.10748E-01 0.00074  3.22633E-01 0.00049  1.29264E+00 0.00262  8.61353E+00 0.00912 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0007' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:45:38 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:26:32 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292338 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.97082E-01  1.00306E+00  9.98299E-01  1.00153E+00  1.00003E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.3E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15765E-02 0.00101  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88424E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06349E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06779E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67814E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.47539E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.47450E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.32071E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.16562E-01 0.00105  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000855 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00017E+04 0.00091 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00017E+04 0.00091 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.02118E+02 ;
RUNNING_TIME              (idx, 1)        =  1.00909E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.82450E-01  4.82450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.02900E-01  1.03500E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.92247E+01  3.34108E+00  2.61140E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  8.91150E-01  2.70333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.25333E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.00909E+02  1.24965E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97595 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00072E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80587E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.55560E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.72124E+04 ;
TOT_SF_RATE               (idx, 1)        =  5.70951E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.93210E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.35202E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62347E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.58600E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.70685E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.77408E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.91882E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.42667E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.88029E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.93141E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.01011E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.37979E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.50876E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.55115E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.93450E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.68520E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.32558E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.29030E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.66496E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.29697E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.01017E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 16 ;
BURNUP                     (idx, [1:  2])  = [  2.00000E+01  2.00018E+01 ];
BURN_DAYS                 (idx, 1)        =  5.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.75227E-01 0.00111 ];
U233_FISS                 (idx, [1:   4]) = [  8.11163E+09 1.00000  5.97193E-07 1.00000 ];
U235_FISS                 (idx, [1:   4]) = [  5.98289E+15 0.00108  4.37982E-01 0.00089 ];
U238_FISS                 (idx, [1:   4]) = [  1.10437E+15 0.00267  8.08408E-02 0.00254 ];
PU239_FISS                (idx, [1:   4]) = [  5.51198E+15 0.00117  4.03506E-01 0.00098 ];
PU240_FISS                (idx, [1:   4]) = [  1.03082E+13 0.02738  7.54468E-04 0.02733 ];
PU241_FISS                (idx, [1:   4]) = [  1.03255E+15 0.00281  7.55861E-02 0.00272 ];
U235_CAPT                 (idx, [1:   4]) = [  1.34095E+15 0.00245  5.12839E-02 0.00239 ];
U238_CAPT                 (idx, [1:   4]) = [  9.54402E+15 0.00106  3.64985E-01 0.00076 ];
PU239_CAPT                (idx, [1:   4]) = [  3.04114E+15 0.00163  1.16312E-01 0.00158 ];
PU240_CAPT                (idx, [1:   4]) = [  2.05577E+15 0.00207  7.86209E-02 0.00198 ];
PU241_CAPT                (idx, [1:   4]) = [  3.68237E+14 0.00462  1.40828E-02 0.00457 ];
XE135_CAPT                (idx, [1:   4]) = [  7.66347E+14 0.00316  2.93115E-02 0.00317 ];
SM149_CAPT                (idx, [1:   4]) = [  2.27625E+14 0.00615  8.70626E-03 0.00615 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000855 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.10336E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000855 5.00810E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3255391 3.26018E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1700834 1.70329E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44630 4.46339E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000855 5.00810E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.44589E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 3.7E-09  4.50057E+05 3.7E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.66290E+16 2.1E-05  3.66290E+16 2.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36630E+16 4.2E-06  1.36630E+16 4.2E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.61365E+16 0.00045  2.00223E+16 0.00046  6.11418E+15 0.00116 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.97995E+16 0.00030  3.36854E+16 0.00027  6.11418E+15 0.00116 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.01017E+16 0.00050  4.01017E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.87229E+18 0.00048  4.97547E+17 0.00048  1.37474E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.58054E+14 0.00498 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.01576E+16 0.00031 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.39542E+18 0.00063 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10191E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10191E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.50811E+00 0.00056 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.82103E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.31138E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23798E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94182E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96873E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.21509E-01 0.00061 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.13284E-01 0.00062 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.68089E+00 2.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05594E+02 4.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.13377E-01 0.00063  9.08502E-01 0.00062  4.78162E-03 0.01098 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.13633E-01 0.00031 ];
COL_KEFF                  (idx, [1:   2]) = [  9.13513E-01 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.13633E-01 0.00031 ];
ABS_KINF                  (idx, [1:   2]) = [  9.21864E-01 0.00030 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70715E+01 0.00023 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70677E+01 9.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.73856E-07 0.00397 ];
IMP_EALF                  (idx, [1:   2]) = [  7.74368E-07 0.00170 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.75538E-01 0.00282 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.76020E-01 0.00114 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.93650E-03 0.00673  1.60458E-04 0.03516  1.06127E-03 0.01536  9.27149E-04 0.01587  2.60329E-03 0.00951  9.14785E-04 0.01555  2.69552E-04 0.02842 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.48466E-01 0.01486  9.74860E-03 0.02407  3.08479E-02 0.00044  1.10801E-01 0.00050  3.22399E-01 0.00031  1.29152E+00 0.00182  7.73068E+00 0.01615 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.22330E-03 0.00977  1.45000E-04 0.05435  9.24329E-04 0.02256  8.38953E-04 0.02479  2.27974E-03 0.01480  7.85016E-04 0.02347  2.50264E-04 0.04397 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.71047E-01 0.02310  1.25643E-02 0.00102  3.08812E-02 0.00064  1.10871E-01 0.00075  3.22479E-01 0.00048  1.29080E+00 0.00269  8.55131E+00 0.00945 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.73529E-05 0.00151  2.73464E-05 0.00152  2.85761E-05 0.01668 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.49777E-05 0.00132  2.49717E-05 0.00132  2.60965E-05 0.01668 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.22541E-03 0.01114  1.34800E-04 0.06861  9.45481E-04 0.02620  8.35996E-04 0.02668  2.25935E-03 0.01661  8.02585E-04 0.02753  2.47195E-04 0.05033 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.65905E-01 0.02753  1.25715E-02 0.00166  3.08666E-02 0.00083  1.10788E-01 0.00096  3.22360E-01 0.00063  1.29983E+00 0.00307  8.42519E+00 0.01452 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.68523E-05 0.00325  2.68408E-05 0.00327  2.71981E-05 0.04423 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.45217E-05 0.00319  2.45114E-05 0.00321  2.48247E-05 0.04425 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.25662E-03 0.03470  1.22674E-04 0.23575  9.27023E-04 0.08009  7.66629E-04 0.08979  2.32933E-03 0.05219  8.74617E-04 0.08792  2.36348E-04 0.15765 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.36054E-01 0.08338  1.26087E-02 0.00524  3.09137E-02 0.00197  1.10718E-01 0.00223  3.22277E-01 0.00161  1.29710E+00 0.00748  8.18975E+00 0.03892 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.26801E-03 0.03341  1.17191E-04 0.22260  9.07429E-04 0.07799  7.77298E-04 0.08466  2.34309E-03 0.05063  8.79336E-04 0.08510  2.43670E-04 0.15184 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.42057E-01 0.08225  1.26087E-02 0.00524  3.09120E-02 0.00196  1.10710E-01 0.00223  3.22191E-01 0.00161  1.29701E+00 0.00750  8.18975E+00 0.03892 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.97726E+02 0.03520 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.71125E-05 0.00092 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.47592E-05 0.00069 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.29222E-03 0.00745 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.95360E+02 0.00763 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.64722E-07 0.00074 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.67924E-06 0.00043  2.67913E-06 0.00044  2.69910E-06 0.00574 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.86487E-05 0.00091  3.86664E-05 0.00092  3.54167E-05 0.01142 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.29075E-01 0.00035  6.29358E-01 0.00035  5.94612E-01 0.01046 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08660E+01 0.01401 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.47450E+01 0.00047  3.32035E+01 0.00053 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.89350E+04 0.00331  3.02963E+05 0.00166  6.10341E+05 0.00093  6.51399E+05 0.00064  5.97827E+05 0.00080  6.42121E+05 0.00066  4.35615E+05 0.00080  3.85531E+05 0.00067  2.94778E+05 0.00096  2.40747E+05 0.00063  2.07651E+05 0.00077  1.86863E+05 0.00089  1.73028E+05 0.00089  1.64506E+05 0.00097  1.60173E+05 0.00079  1.38405E+05 0.00103  1.36879E+05 0.00084  1.35667E+05 0.00098  1.33310E+05 0.00100  2.60230E+05 0.00064  2.51019E+05 0.00074  1.81035E+05 0.00107  1.17535E+05 0.00089  1.35449E+05 0.00090  1.28841E+05 0.00089  1.14760E+05 0.00096  1.85745E+05 0.00072  4.31301E+04 0.00103  5.34977E+04 0.00106  4.86611E+04 0.00138  2.84301E+04 0.00151  4.89830E+04 0.00169  3.26523E+04 0.00168  2.67394E+04 0.00143  4.59662E+03 0.00309  3.97456E+03 0.00419  3.47300E+03 0.00384  3.30575E+03 0.00380  3.37009E+03 0.00363  3.72347E+03 0.00367  4.44870E+03 0.00292  4.52468E+03 0.00392  8.95434E+03 0.00282  1.46706E+04 0.00214  1.90390E+04 0.00192  5.10535E+04 0.00126  5.36055E+04 0.00179  5.73794E+04 0.00129  3.70308E+04 0.00132  2.60901E+04 0.00145  1.94175E+04 0.00154  2.28080E+04 0.00182  4.46831E+04 0.00160  6.32983E+04 0.00156  1.26111E+05 0.00131  1.99771E+05 0.00146  3.03524E+05 0.00164  1.95723E+05 0.00162  1.40906E+05 0.00164  1.01594E+05 0.00168  9.15740E+04 0.00172  9.02742E+04 0.00172  7.54166E+04 0.00177  5.12235E+04 0.00211  4.72911E+04 0.00181  4.21012E+04 0.00192  3.56698E+04 0.00194  2.81978E+04 0.00162  1.88814E+04 0.00233  6.70835E+03 0.00256 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.21742E-01 0.00048 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.49551E+18 0.00052  3.76811E+17 0.00141 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38971E-01 0.00012  1.56417E+00 0.00041 ];
INF_CAPT                  (idx, [1:   4]) = [  8.08869E-03 0.00059  3.72632E-02 0.00077 ];
INF_ABS                   (idx, [1:   4]) = [  9.83828E-03 0.00052  6.65845E-02 0.00104 ];
INF_FISS                  (idx, [1:   4]) = [  1.74959E-03 0.00038  2.93213E-02 0.00140 ];
INF_NSF                   (idx, [1:   4]) = [  4.71382E-03 0.00039  7.85145E-02 0.00143 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.69424E+00 7.1E-05  2.67773E+00 4.5E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05604E+02 8.7E-06  2.05592E+02 8.6E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.57893E-08 0.00040  2.58472E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29125E-01 0.00012  1.49760E+00 0.00048 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44026E-01 0.00023  3.96913E-01 0.00048 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62960E-02 0.00029  9.36363E-02 0.00108 ];
INF_SCATT3                (idx, [1:   4]) = [  7.31417E-03 0.00314  2.80617E-02 0.00253 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03026E-02 0.00233 -8.93810E-03 0.00693 ];
INF_SCATT5                (idx, [1:   4]) = [  1.75989E-04 0.11775  6.72756E-03 0.00696 ];
INF_SCATT6                (idx, [1:   4]) = [  5.12449E-03 0.00277 -1.71059E-02 0.00315 ];
INF_SCATT7                (idx, [1:   4]) = [  7.51720E-04 0.02044  4.30790E-04 0.11504 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29169E-01 0.00012  1.49760E+00 0.00048 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44026E-01 0.00023  3.96913E-01 0.00048 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62961E-02 0.00029  9.36363E-02 0.00108 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.31403E-03 0.00314  2.80617E-02 0.00253 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03026E-02 0.00233 -8.93810E-03 0.00693 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.75981E-04 0.11796  6.72756E-03 0.00696 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.12446E-03 0.00277 -1.71059E-02 0.00315 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.51824E-04 0.02040  4.30790E-04 0.11504 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12527E-01 0.00032  1.01575E+00 0.00039 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56843E+00 0.00032  3.28166E-01 0.00039 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.79482E-03 0.00054  6.65845E-02 0.00104 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69963E-02 0.00026  6.77382E-02 0.00126 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11975E-01 0.00011  1.71502E-02 0.00052  1.16714E-03 0.00502  1.49643E+00 0.00048 ];
INF_S1                    (idx, [1:   8]) = [  2.39068E-01 0.00023  4.95836E-03 0.00097  5.03797E-04 0.00566  3.96410E-01 0.00049 ];
INF_S2                    (idx, [1:   8]) = [  9.78555E-02 0.00029 -1.55950E-03 0.00278  2.75160E-04 0.00755  9.33612E-02 0.00108 ];
INF_S3                    (idx, [1:   8]) = [  9.09008E-03 0.00238 -1.77591E-03 0.00185  9.76899E-05 0.01749  2.79640E-02 0.00255 ];
INF_S4                    (idx, [1:   8]) = [ -9.74937E-03 0.00247 -5.53192E-04 0.00539  5.42256E-07 1.00000 -8.93864E-03 0.00692 ];
INF_S5                    (idx, [1:   8]) = [  1.36777E-04 0.16103  3.92116E-05 0.07152 -3.85112E-05 0.03889  6.76607E-03 0.00695 ];
INF_S6                    (idx, [1:   8]) = [  5.26076E-03 0.00289 -1.36266E-04 0.02179 -5.08897E-05 0.02663 -1.70550E-02 0.00316 ];
INF_S7                    (idx, [1:   8]) = [  9.20484E-04 0.01617 -1.68763E-04 0.01279 -4.65755E-05 0.02046  4.77365E-04 0.10387 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12018E-01 0.00011  1.71502E-02 0.00052  1.16714E-03 0.00502  1.49643E+00 0.00048 ];
INF_SP1                   (idx, [1:   8]) = [  2.39068E-01 0.00023  4.95836E-03 0.00097  5.03797E-04 0.00566  3.96410E-01 0.00049 ];
INF_SP2                   (idx, [1:   8]) = [  9.78556E-02 0.00029 -1.55950E-03 0.00278  2.75160E-04 0.00755  9.33612E-02 0.00108 ];
INF_SP3                   (idx, [1:   8]) = [  9.08994E-03 0.00238 -1.77591E-03 0.00185  9.76899E-05 0.01749  2.79640E-02 0.00255 ];
INF_SP4                   (idx, [1:   8]) = [ -9.74937E-03 0.00247 -5.53192E-04 0.00539  5.42256E-07 1.00000 -8.93864E-03 0.00692 ];
INF_SP5                   (idx, [1:   8]) = [  1.36769E-04 0.16130  3.92116E-05 0.07152 -3.85112E-05 0.03889  6.76607E-03 0.00695 ];
INF_SP6                   (idx, [1:   8]) = [  5.26073E-03 0.00289 -1.36266E-04 0.02179 -5.08897E-05 0.02663 -1.70550E-02 0.00316 ];
INF_SP7                   (idx, [1:   8]) = [  9.20588E-04 0.01614 -1.68763E-04 0.01279 -4.65755E-05 0.02046  4.77365E-04 0.10387 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31725E-01 0.00048  1.20197E+00 0.00681 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33182E-01 0.00092  1.30317E+00 0.00856 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33486E-01 0.00092  1.31072E+00 0.00893 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28582E-01 0.00078  1.03677E+00 0.00667 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43849E+00 0.00048  2.77630E-01 0.00678 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42953E+00 0.00092  2.56228E-01 0.00839 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42766E+00 0.00092  2.54812E-01 0.00914 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45828E+00 0.00078  3.21850E-01 0.00661 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.22330E-03 0.00977  1.45000E-04 0.05435  9.24329E-04 0.02256  8.38953E-04 0.02479  2.27974E-03 0.01480  7.85016E-04 0.02347  2.50264E-04 0.04397 ];
LAMBDA                    (idx, [1:  14]) = [  7.71047E-01 0.02310  1.25643E-02 0.00102  3.08812E-02 0.00064  1.10871E-01 0.00075  3.22479E-01 0.00048  1.29080E+00 0.00269  8.55131E+00 0.00945 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0007' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:45:38 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:32:36 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292338 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.96458E-01  1.00235E+00  9.98981E-01  1.00147E+00  1.00074E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15907E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88409E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06760E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.07190E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68068E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.48147E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.48056E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.31319E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.17845E-01 0.00112  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001341 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00027E+04 0.00091 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00027E+04 0.00091 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.32424E+02 ;
RUNNING_TIME              (idx, 1)        =  1.06977E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.82450E-01  4.82450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.24900E-01  1.07500E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.05215E+02  3.35700E+00  2.63297E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  9.46517E-01  2.76833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.43000E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.06977E+02  1.25130E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97700 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00005E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80850E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.57084E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68328E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.25606E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.01005E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.41025E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.56076E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.54222E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  3.71161E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.88794E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.86889E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.93957E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  8.42719E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.99398E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.39961E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.35003E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.51122E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.53391E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.39603E+13 ;
CS137_ACTIVITY            (idx, 1)        =  3.34773E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.34150E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.23743E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.04555E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.31224E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.21926E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 17 ;
BURNUP                     (idx, [1:  2])  = [  2.50000E+01  2.50023E+01 ];
BURN_DAYS                 (idx, 1)        =  6.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.17341E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  5.03035E+15 0.00122  3.68646E-01 0.00107 ];
U238_FISS                 (idx, [1:   4]) = [  1.16054E+15 0.00274  8.50380E-02 0.00256 ];
PU239_FISS                (idx, [1:   4]) = [  6.04131E+15 0.00117  4.42717E-01 0.00092 ];
PU240_FISS                (idx, [1:   4]) = [  1.28611E+13 0.02433  9.42600E-04 0.02435 ];
PU241_FISS                (idx, [1:   4]) = [  1.37539E+15 0.00255  1.00790E-01 0.00244 ];
U235_CAPT                 (idx, [1:   4]) = [  1.12791E+15 0.00289  3.99558E-02 0.00283 ];
U238_CAPT                 (idx, [1:   4]) = [  9.96112E+15 0.00106  3.52857E-01 0.00079 ];
PU239_CAPT                (idx, [1:   4]) = [  3.32409E+15 0.00154  1.17763E-01 0.00151 ];
PU240_CAPT                (idx, [1:   4]) = [  2.44006E+15 0.00191  8.64337E-02 0.00176 ];
PU241_CAPT                (idx, [1:   4]) = [  4.92921E+14 0.00420  1.74607E-02 0.00413 ];
XE135_CAPT                (idx, [1:   4]) = [  7.74394E+14 0.00310  2.74356E-02 0.00311 ];
SM149_CAPT                (idx, [1:   4]) = [  2.42943E+14 0.00574  8.60789E-03 0.00578 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001341 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.08270E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001341 5.00808E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3340533 3.34525E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1615149 1.61717E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45659 4.56585E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001341 5.00808E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.58794E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 3.7E-09  4.50057E+05 3.7E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.69803E+16 2.2E-05  3.69803E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36319E+16 4.2E-06  1.36319E+16 4.2E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.82213E+16 0.00045  2.17562E+16 0.00045  6.46513E+15 0.00122 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.18532E+16 0.00031  3.53880E+16 0.00028  6.46513E+15 0.00122 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.21926E+16 0.00052  4.21926E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.96934E+18 0.00051  5.21831E+17 0.00047  1.44751E+18 0.00057 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.85342E+14 0.00500 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.22385E+16 0.00031 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.47071E+18 0.00067 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.09614E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.09614E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.47131E+00 0.00059 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.78580E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.25075E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23670E+00 0.00038 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94043E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96806E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.85462E-01 0.00065 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.77377E-01 0.00065 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.71278E+00 2.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06064E+02 4.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.77260E-01 0.00066  8.72980E-01 0.00066  4.39676E-03 0.01127 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.76969E-01 0.00031 ];
COL_KEFF                  (idx, [1:   2]) = [  8.76580E-01 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.76969E-01 0.00031 ];
ABS_KINF                  (idx, [1:   2]) = [  8.85055E-01 0.00031 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70028E+01 0.00024 ];
IMP_ALF                   (idx, [1:   2]) = [  1.69991E+01 9.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  8.29123E-07 0.00412 ];
IMP_EALF                  (idx, [1:   2]) = [  8.29280E-07 0.00165 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.89615E-01 0.00281 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.90200E-01 0.00118 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.89284E-03 0.00642  1.48002E-04 0.03819  1.05054E-03 0.01519  9.19972E-04 0.01597  2.54139E-03 0.00950  9.62598E-04 0.01529  2.70338E-04 0.02797 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.51162E-01 0.01457  9.27864E-03 0.02669  3.07294E-02 0.00044  1.11018E-01 0.00055  3.22921E-01 0.00034  1.27186E+00 0.00209  7.58160E+00 0.01703 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.95479E-03 0.00937  1.32093E-04 0.06049  8.65365E-04 0.02356  7.95094E-04 0.02589  2.10521E-03 0.01444  8.18132E-04 0.02330  2.38892E-04 0.04551 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.77459E-01 0.02476  1.25739E-02 0.00110  3.07406E-02 0.00066  1.11036E-01 0.00083  3.22936E-01 0.00054  1.27470E+00 0.00295  8.23565E+00 0.01191 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.85811E-05 0.00156  2.85715E-05 0.00156  3.07386E-05 0.01689 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.50670E-05 0.00138  2.50586E-05 0.00138  2.69539E-05 0.01684 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.01835E-03 0.01139  1.29392E-04 0.06970  9.01514E-04 0.02718  7.95938E-04 0.02719  2.14139E-03 0.01755  8.13554E-04 0.02682  2.36568E-04 0.05231 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.59538E-01 0.02871  1.25655E-02 0.00164  3.07502E-02 0.00089  1.11107E-01 0.00106  3.22527E-01 0.00070  1.27328E+00 0.00410  8.32270E+00 0.01653 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.77850E-05 0.00369  2.77638E-05 0.00371  2.98790E-05 0.04885 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.43703E-05 0.00365  2.43517E-05 0.00367  2.61924E-05 0.04869 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.05805E-03 0.03883  8.66666E-05 0.22767  8.91304E-04 0.08899  7.93481E-04 0.10118  2.24912E-03 0.05821  7.97227E-04 0.09229  2.40258E-04 0.16823 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.64338E-01 0.08128  1.25620E-02 0.00410  3.07461E-02 0.00211  1.10724E-01 0.00227  3.23129E-01 0.00190  1.28295E+00 0.00863  8.68110E+00 0.03327 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.03649E-03 0.03867  8.66585E-05 0.22177  8.50760E-04 0.08617  8.09493E-04 0.10102  2.22789E-03 0.05707  8.24718E-04 0.09143  2.36974E-04 0.17241 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.59613E-01 0.08124  1.25620E-02 0.00410  3.07481E-02 0.00210  1.10726E-01 0.00226  3.23076E-01 0.00189  1.28287E+00 0.00861  8.68139E+00 0.03327 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.83072E+02 0.03902 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.82460E-05 0.00097 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.47737E-05 0.00071 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.04332E-03 0.00736 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.78662E+02 0.00748 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.66100E-07 0.00075 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.65458E-06 0.00043  2.65443E-06 0.00043  2.68481E-06 0.00615 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.91366E-05 0.00091  3.91562E-05 0.00091  3.53680E-05 0.01200 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.23008E-01 0.00035  6.23411E-01 0.00035  5.66145E-01 0.00975 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08917E+01 0.01544 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.48056E+01 0.00050  3.31701E+01 0.00057 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.97052E+04 0.00358  3.04576E+05 0.00176  6.12042E+05 0.00065  6.51379E+05 0.00099  5.97714E+05 0.00080  6.41188E+05 0.00076  4.35114E+05 0.00050  3.85399E+05 0.00061  2.94645E+05 0.00070  2.40640E+05 0.00086  2.07597E+05 0.00118  1.86999E+05 0.00075  1.72987E+05 0.00092  1.64394E+05 0.00083  1.60196E+05 0.00080  1.38530E+05 0.00111  1.36809E+05 0.00102  1.35580E+05 0.00081  1.33332E+05 0.00086  2.59878E+05 0.00058  2.51392E+05 0.00072  1.81382E+05 0.00087  1.17599E+05 0.00099  1.35211E+05 0.00077  1.28778E+05 0.00080  1.14631E+05 0.00095  1.85176E+05 0.00070  4.29151E+04 0.00177  5.31785E+04 0.00140  4.83528E+04 0.00137  2.83438E+04 0.00215  4.88922E+04 0.00163  3.21708E+04 0.00168  2.60827E+04 0.00184  4.41766E+03 0.00351  3.77788E+03 0.00334  3.28754E+03 0.00329  3.17400E+03 0.00336  3.21959E+03 0.00358  3.55234E+03 0.00322  4.24104E+03 0.00392  4.34843E+03 0.00375  8.71798E+03 0.00325  1.43774E+04 0.00245  1.88939E+04 0.00197  5.02975E+04 0.00134  5.29402E+04 0.00132  5.64458E+04 0.00146  3.66144E+04 0.00144  2.58670E+04 0.00121  1.91103E+04 0.00209  2.25387E+04 0.00160  4.42423E+04 0.00127  6.27206E+04 0.00148  1.25616E+05 0.00121  1.99646E+05 0.00117  3.03801E+05 0.00114  1.96224E+05 0.00139  1.41305E+05 0.00129  1.02092E+05 0.00133  9.20936E+04 0.00121  9.08644E+04 0.00147  7.59334E+04 0.00162  5.15692E+04 0.00157  4.77229E+04 0.00127  4.25079E+04 0.00144  3.59113E+04 0.00174  2.84324E+04 0.00153  1.90904E+04 0.00174  6.78223E+03 0.00241 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.84659E-01 0.00047 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.57275E+18 0.00046  3.96624E+17 0.00105 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39342E-01 0.00011  1.56943E+00 0.00032 ];
INF_CAPT                  (idx, [1:   4]) = [  8.34510E-03 0.00061  3.80663E-02 0.00061 ];
INF_ABS                   (idx, [1:   4]) = [  1.00035E-02 0.00050  6.58648E-02 0.00080 ];
INF_FISS                  (idx, [1:   4]) = [  1.65844E-03 0.00038  2.77985E-02 0.00107 ];
INF_NSF                   (idx, [1:   4]) = [  4.51043E-03 0.00037  7.53661E-02 0.00109 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.71968E+00 8.1E-05  2.71115E+00 4.9E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05963E+02 7.0E-06  2.06088E+02 9.4E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.53350E-08 0.00040  2.59161E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29332E-01 0.00011  1.50354E+00 0.00036 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44133E-01 0.00016  3.98321E-01 0.00041 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63298E-02 0.00030  9.39133E-02 0.00099 ];
INF_SCATT3                (idx, [1:   4]) = [  7.29776E-03 0.00267  2.81385E-02 0.00222 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03673E-02 0.00156 -9.01271E-03 0.00757 ];
INF_SCATT5                (idx, [1:   4]) = [  1.69187E-04 0.09816  6.73434E-03 0.00761 ];
INF_SCATT6                (idx, [1:   4]) = [  5.12386E-03 0.00376 -1.72837E-02 0.00232 ];
INF_SCATT7                (idx, [1:   4]) = [  7.64222E-04 0.02268  4.66698E-04 0.09642 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29376E-01 0.00011  1.50354E+00 0.00036 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44133E-01 0.00016  3.98321E-01 0.00041 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63295E-02 0.00030  9.39133E-02 0.00099 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.29794E-03 0.00267  2.81385E-02 0.00222 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03673E-02 0.00156 -9.01271E-03 0.00757 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.69139E-04 0.09822  6.73434E-03 0.00761 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.12403E-03 0.00377 -1.72837E-02 0.00232 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.64250E-04 0.02262  4.66698E-04 0.09642 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12393E-01 0.00026  1.01970E+00 0.00029 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56942E+00 0.00026  3.26895E-01 0.00029 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.96017E-03 0.00051  6.58648E-02 0.00080 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69995E-02 0.00024  6.70341E-02 0.00101 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12342E-01 0.00011  1.69898E-02 0.00042  1.14498E-03 0.00442  1.50239E+00 0.00037 ];
INF_S1                    (idx, [1:   8]) = [  2.39228E-01 0.00016  4.90475E-03 0.00087  4.92380E-04 0.00634  3.97828E-01 0.00041 ];
INF_S2                    (idx, [1:   8]) = [  9.78726E-02 0.00028 -1.54277E-03 0.00238  2.68326E-04 0.00826  9.36449E-02 0.00099 ];
INF_S3                    (idx, [1:   8]) = [  9.06084E-03 0.00210 -1.76308E-03 0.00150  9.79924E-05 0.02186  2.80405E-02 0.00224 ];
INF_S4                    (idx, [1:   8]) = [ -9.81655E-03 0.00167 -5.50720E-04 0.00398  6.57165E-07 1.00000 -9.01336E-03 0.00748 ];
INF_S5                    (idx, [1:   8]) = [  1.26115E-04 0.13356  4.30715E-05 0.05706 -3.75875E-05 0.03889  6.77193E-03 0.00756 ];
INF_S6                    (idx, [1:   8]) = [  5.25315E-03 0.00355 -1.29298E-04 0.01672 -4.80804E-05 0.03076 -1.72356E-02 0.00234 ];
INF_S7                    (idx, [1:   8]) = [  9.26504E-04 0.01791 -1.62282E-04 0.01275 -4.33939E-05 0.03250  5.10092E-04 0.08840 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12386E-01 0.00011  1.69898E-02 0.00042  1.14498E-03 0.00442  1.50239E+00 0.00037 ];
INF_SP1                   (idx, [1:   8]) = [  2.39229E-01 0.00016  4.90475E-03 0.00087  4.92380E-04 0.00634  3.97828E-01 0.00041 ];
INF_SP2                   (idx, [1:   8]) = [  9.78723E-02 0.00028 -1.54277E-03 0.00238  2.68326E-04 0.00826  9.36449E-02 0.00099 ];
INF_SP3                   (idx, [1:   8]) = [  9.06102E-03 0.00210 -1.76308E-03 0.00150  9.79924E-05 0.02186  2.80405E-02 0.00224 ];
INF_SP4                   (idx, [1:   8]) = [ -9.81657E-03 0.00168 -5.50720E-04 0.00398  6.57165E-07 1.00000 -9.01336E-03 0.00748 ];
INF_SP5                   (idx, [1:   8]) = [  1.26067E-04 0.13366  4.30715E-05 0.05706 -3.75875E-05 0.03889  6.77193E-03 0.00756 ];
INF_SP6                   (idx, [1:   8]) = [  5.25333E-03 0.00356 -1.29298E-04 0.01672 -4.80804E-05 0.03076 -1.72356E-02 0.00234 ];
INF_SP7                   (idx, [1:   8]) = [  9.26532E-04 0.01787 -1.62282E-04 0.01275 -4.33939E-05 0.03250  5.10092E-04 0.08840 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31905E-01 0.00055  1.21818E+00 0.00711 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33929E-01 0.00085  1.33009E+00 0.00946 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33586E-01 0.00098  1.32169E+00 0.00890 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28296E-01 0.00098  1.04957E+00 0.00788 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43738E+00 0.00055  2.73960E-01 0.00704 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42496E+00 0.00085  2.51130E-01 0.00913 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42706E+00 0.00098  2.52682E-01 0.00891 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.46012E+00 0.00098  3.18069E-01 0.00795 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.95479E-03 0.00937  1.32093E-04 0.06049  8.65365E-04 0.02356  7.95094E-04 0.02589  2.10521E-03 0.01444  8.18132E-04 0.02330  2.38892E-04 0.04551 ];
LAMBDA                    (idx, [1:  14]) = [  7.77459E-01 0.02476  1.25739E-02 0.00110  3.07406E-02 0.00066  1.11036E-01 0.00083  3.22936E-01 0.00054  1.27470E+00 0.00295  8.23565E+00 0.01191 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0007' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:45:38 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:38:42 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292338 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.95760E-01  1.00395E+00  1.00044E+00  1.00045E+00  9.99407E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16424E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88358E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06296E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06727E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68506E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.47717E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.47625E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.32752E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.19858E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001407 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00028E+04 0.00099 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00028E+04 0.00099 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.62816E+02 ;
RUNNING_TIME              (idx, 1)        =  1.13062E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.82450E-01  4.82450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.47467E-01  1.10833E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.11221E+02  3.36722E+00  2.63895E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.00225E+00  2.72667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.69333E-02  9.16668E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.13062E+02  1.25207E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97794 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99973E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81085E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.69622E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68088E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.35639E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.11380E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.48789E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.58238E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.53205E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  4.92917E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.04635E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.03707E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.59885E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  8.92098E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.08647E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.75680E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.38597E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.55467E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.58192E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.06413E+13 ;
CS137_ACTIVITY            (idx, 1)        =  4.00608E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.45666E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.20742E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.65427E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.34228E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.41611E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 18 ;
BURNUP                     (idx, [1:  2])  = [  3.00000E+01  3.00028E+01 ];
BURN_DAYS                 (idx, 1)        =  7.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.60077E-01 0.00116 ];
U235_FISS                 (idx, [1:   4]) = [  4.16168E+15 0.00139  3.06067E-01 0.00128 ];
U238_FISS                 (idx, [1:   4]) = [  1.20901E+15 0.00281  8.88947E-02 0.00258 ];
PU239_FISS                (idx, [1:   4]) = [  6.48952E+15 0.00117  4.77231E-01 0.00089 ];
PU240_FISS                (idx, [1:   4]) = [  1.57099E+13 0.02430  1.15543E-03 0.02429 ];
PU241_FISS                (idx, [1:   4]) = [  1.68892E+15 0.00225  1.24202E-01 0.00213 ];
U235_CAPT                 (idx, [1:   4]) = [  9.33279E+14 0.00297  3.08728E-02 0.00291 ];
U238_CAPT                 (idx, [1:   4]) = [  1.03969E+16 0.00111  3.43907E-01 0.00079 ];
PU239_CAPT                (idx, [1:   4]) = [  3.57684E+15 0.00140  1.18339E-01 0.00147 ];
PU240_CAPT                (idx, [1:   4]) = [  2.78197E+15 0.00188  9.20225E-02 0.00172 ];
PU241_CAPT                (idx, [1:   4]) = [  6.09391E+14 0.00385  2.01581E-02 0.00379 ];
XE135_CAPT                (idx, [1:   4]) = [  7.89095E+14 0.00325  2.61045E-02 0.00323 ];
SM149_CAPT                (idx, [1:   4]) = [  2.50695E+14 0.00612  8.29274E-03 0.00607 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001407 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.30069E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001407 5.00830E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3417837 3.42268E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1537639 1.53968E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45931 4.59356E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001407 5.00830E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.00469E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 3.7E-09  4.50057E+05 3.7E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.72960E+16 2.3E-05  3.72960E+16 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36039E+16 4.8E-06  1.36039E+16 4.8E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.02040E+16 0.00045  2.34569E+16 0.00045  6.74717E+15 0.00128 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.38079E+16 0.00031  3.70607E+16 0.00029  6.74717E+15 0.00128 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.41611E+16 0.00052  4.41611E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.05918E+18 0.00051  5.45616E+17 0.00049  1.51356E+18 0.00057 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.05741E+14 0.00486 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.42136E+16 0.00032 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.53746E+18 0.00068 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.09037E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.09037E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.43098E+00 0.00060 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.77166E-01 0.00035 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.19826E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23618E+00 0.00040 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94020E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96774E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.52053E-01 0.00069 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.44224E-01 0.00069 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.74157E+00 2.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06488E+02 4.8E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.44228E-01 0.00070  8.40131E-01 0.00070  4.09331E-03 0.01199 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.44975E-01 0.00032 ];
COL_KEFF                  (idx, [1:   2]) = [  8.44656E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.44975E-01 0.00032 ];
ABS_KINF                  (idx, [1:   2]) = [  8.52812E-01 0.00031 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.69396E+01 0.00026 ];
IMP_ALF                   (idx, [1:   2]) = [  1.69415E+01 0.00010 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  8.83741E-07 0.00435 ];
IMP_EALF                  (idx, [1:   2]) = [  8.78502E-07 0.00171 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.03089E-01 0.00302 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.03284E-01 0.00119 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.92902E-03 0.00683  1.29820E-04 0.04455  1.09105E-03 0.01430  9.29360E-04 0.01585  2.53293E-03 0.01029  9.71442E-04 0.01531  2.74410E-04 0.02847 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.48651E-01 0.01537  8.30170E-03 0.03230  3.06135E-02 0.00042  1.11174E-01 0.00059  3.23022E-01 0.00038  1.25807E+00 0.00243  7.22207E+00 0.01930 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.79141E-03 0.01030  9.74244E-05 0.06855  9.04736E-04 0.02345  7.65682E-04 0.02476  1.99175E-03 0.01608  8.17176E-04 0.02366  2.14634E-04 0.04701 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.32593E-01 0.02375  1.26103E-02 0.00147  3.06129E-02 0.00062  1.11143E-01 0.00086  3.22754E-01 0.00058  1.25813E+00 0.00344  8.04370E+00 0.01396 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.98142E-05 0.00157  2.98030E-05 0.00158  3.21186E-05 0.02045 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.51630E-05 0.00136  2.51536E-05 0.00136  2.71043E-05 0.02043 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.85764E-03 0.01221  1.05076E-04 0.07961  8.94732E-04 0.02706  7.85081E-04 0.02867  2.04524E-03 0.01894  8.03695E-04 0.02866  2.23823E-04 0.05327 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.48768E-01 0.02896  1.26102E-02 0.00228  3.06063E-02 0.00089  1.11114E-01 0.00110  3.23019E-01 0.00078  1.26694E+00 0.00434  8.02046E+00 0.02041 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.92037E-05 0.00366  2.91913E-05 0.00363  2.82015E-05 0.04925 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.46461E-05 0.00353  2.46357E-05 0.00351  2.37911E-05 0.04919 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.79101E-03 0.04048  9.75901E-05 0.26223  8.72010E-04 0.10082  8.00992E-04 0.09118  2.06156E-03 0.05960  6.97364E-04 0.09376  2.61492E-04 0.18782 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.97834E-01 0.09180  1.25888E-02 0.00554  3.06373E-02 0.00214  1.11265E-01 0.00282  3.23417E-01 0.00202  1.25756E+00 0.01115  7.79532E+00 0.05158 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.79540E-03 0.03932  9.84093E-05 0.26109  8.68998E-04 0.09792  8.09834E-04 0.08974  2.08639E-03 0.05796  6.83786E-04 0.09054  2.47990E-04 0.18395 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.88510E-01 0.09209  1.25888E-02 0.00554  3.06375E-02 0.00214  1.11292E-01 0.00281  3.23372E-01 0.00201  1.25784E+00 0.01113  7.78479E+00 0.05199 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.66369E+02 0.04142 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.95245E-05 0.00098 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.49195E-05 0.00070 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.83450E-03 0.00788 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.63734E+02 0.00782 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.65253E-07 0.00084 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.63211E-06 0.00045  2.63202E-06 0.00045  2.64760E-06 0.00642 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.93737E-05 0.00100  3.93927E-05 0.00101  3.55024E-05 0.01167 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.17829E-01 0.00036  6.18362E-01 0.00036  5.41078E-01 0.01103 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05825E+01 0.01399 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.47625E+01 0.00053  3.31578E+01 0.00057 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.10008E+04 0.00285  3.05270E+05 0.00154  6.10630E+05 0.00091  6.49155E+05 0.00066  5.98340E+05 0.00064  6.41613E+05 0.00051  4.35546E+05 0.00064  3.85274E+05 0.00061  2.95086E+05 0.00066  2.41177E+05 0.00077  2.07939E+05 0.00077  1.87173E+05 0.00076  1.73095E+05 0.00092  1.64583E+05 0.00114  1.60135E+05 0.00073  1.38382E+05 0.00099  1.36989E+05 0.00113  1.35682E+05 0.00082  1.33253E+05 0.00082  2.60087E+05 0.00063  2.51584E+05 0.00084  1.81502E+05 0.00102  1.17597E+05 0.00099  1.35212E+05 0.00072  1.29045E+05 0.00089  1.14396E+05 0.00100  1.84414E+05 0.00094  4.28900E+04 0.00236  5.27547E+04 0.00152  4.80945E+04 0.00149  2.83014E+04 0.00176  4.86324E+04 0.00177  3.19333E+04 0.00228  2.56707E+04 0.00207  4.29286E+03 0.00388  3.61023E+03 0.00416  3.15245E+03 0.00521  3.05831E+03 0.00335  3.09951E+03 0.00336  3.39584E+03 0.00430  4.09887E+03 0.00318  4.25713E+03 0.00492  8.52621E+03 0.00275  1.41918E+04 0.00238  1.85236E+04 0.00212  4.98756E+04 0.00150  5.23815E+04 0.00143  5.59491E+04 0.00136  3.63442E+04 0.00164  2.55555E+04 0.00195  1.88525E+04 0.00194  2.22739E+04 0.00152  4.36853E+04 0.00182  6.21537E+04 0.00178  1.24789E+05 0.00175  1.98625E+05 0.00143  3.02790E+05 0.00171  1.96001E+05 0.00190  1.41201E+05 0.00169  1.01965E+05 0.00185  9.18743E+04 0.00170  9.07893E+04 0.00172  7.59837E+04 0.00174  5.15390E+04 0.00181  4.77146E+04 0.00181  4.25494E+04 0.00188  3.60326E+04 0.00163  2.84556E+04 0.00185  1.91181E+04 0.00212  6.77817E+03 0.00206 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.52488E-01 0.00063 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.64556E+18 0.00064  4.13657E+17 0.00155 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39763E-01 0.00014  1.56959E+00 0.00036 ];
INF_CAPT                  (idx, [1:   4]) = [  8.57985E-03 0.00060  3.88906E-02 0.00096 ];
INF_ABS                   (idx, [1:   4]) = [  1.01565E-02 0.00051  6.55112E-02 0.00120 ];
INF_FISS                  (idx, [1:   4]) = [  1.57661E-03 0.00047  2.66206E-02 0.00157 ];
INF_NSF                   (idx, [1:   4]) = [  4.32543E-03 0.00046  7.29704E-02 0.00162 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.74350E+00 5.0E-05  2.74112E+00 7.0E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06293E+02 8.8E-06  2.06534E+02 1.4E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.49776E-08 0.00048  2.59533E-06 0.00013 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29610E-01 0.00015  1.50402E+00 0.00043 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44151E-01 0.00018  3.98248E-01 0.00060 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63310E-02 0.00024  9.38637E-02 0.00100 ];
INF_SCATT3                (idx, [1:   4]) = [  7.31179E-03 0.00352  2.81628E-02 0.00231 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03093E-02 0.00161 -8.99263E-03 0.00505 ];
INF_SCATT5                (idx, [1:   4]) = [  2.03476E-04 0.07824  6.73958E-03 0.00876 ];
INF_SCATT6                (idx, [1:   4]) = [  5.14802E-03 0.00346 -1.72965E-02 0.00298 ];
INF_SCATT7                (idx, [1:   4]) = [  7.60637E-04 0.01607  4.36664E-04 0.12123 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29654E-01 0.00015  1.50402E+00 0.00043 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44151E-01 0.00018  3.98248E-01 0.00060 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63311E-02 0.00024  9.38637E-02 0.00100 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.31170E-03 0.00351  2.81628E-02 0.00231 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03094E-02 0.00160 -8.99263E-03 0.00505 ];
INF_SCATTP5               (idx, [1:   4]) = [  2.03540E-04 0.07793  6.73958E-03 0.00876 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.14792E-03 0.00345 -1.72965E-02 0.00298 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.60474E-04 0.01603  4.36664E-04 0.12123 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12567E-01 0.00037  1.02102E+00 0.00028 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56814E+00 0.00037  3.26471E-01 0.00028 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.01119E-02 0.00050  6.55112E-02 0.00120 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70085E-02 0.00021  6.67113E-02 0.00133 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12755E-01 0.00014  1.68551E-02 0.00048  1.14518E-03 0.00447  1.50288E+00 0.00043 ];
INF_S1                    (idx, [1:   8]) = [  2.39284E-01 0.00018  4.86700E-03 0.00094  4.95725E-04 0.00647  3.97752E-01 0.00060 ];
INF_S2                    (idx, [1:   8]) = [  9.78692E-02 0.00024 -1.53820E-03 0.00240  2.71045E-04 0.00807  9.35927E-02 0.00101 ];
INF_S3                    (idx, [1:   8]) = [  9.05469E-03 0.00268 -1.74290E-03 0.00191  9.91259E-05 0.01921  2.80636E-02 0.00233 ];
INF_S4                    (idx, [1:   8]) = [ -9.77235E-03 0.00171 -5.36943E-04 0.00518  2.72333E-06 0.40925 -8.99535E-03 0.00506 ];
INF_S5                    (idx, [1:   8]) = [  1.59171E-04 0.09607  4.43052E-05 0.06080 -3.95599E-05 0.03748  6.77914E-03 0.00876 ];
INF_S6                    (idx, [1:   8]) = [  5.28170E-03 0.00328 -1.33678E-04 0.01458 -5.09423E-05 0.03232 -1.72456E-02 0.00296 ];
INF_S7                    (idx, [1:   8]) = [  9.28012E-04 0.01323 -1.67375E-04 0.00937 -4.67932E-05 0.02864  4.83457E-04 0.10977 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12799E-01 0.00014  1.68551E-02 0.00048  1.14518E-03 0.00447  1.50288E+00 0.00043 ];
INF_SP1                   (idx, [1:   8]) = [  2.39284E-01 0.00018  4.86700E-03 0.00094  4.95725E-04 0.00647  3.97752E-01 0.00060 ];
INF_SP2                   (idx, [1:   8]) = [  9.78693E-02 0.00024 -1.53820E-03 0.00240  2.71045E-04 0.00807  9.35927E-02 0.00101 ];
INF_SP3                   (idx, [1:   8]) = [  9.05460E-03 0.00267 -1.74290E-03 0.00191  9.91259E-05 0.01921  2.80636E-02 0.00233 ];
INF_SP4                   (idx, [1:   8]) = [ -9.77241E-03 0.00171 -5.36943E-04 0.00518  2.72333E-06 0.40925 -8.99535E-03 0.00506 ];
INF_SP5                   (idx, [1:   8]) = [  1.59234E-04 0.09566  4.43052E-05 0.06080 -3.95599E-05 0.03748  6.77914E-03 0.00876 ];
INF_SP6                   (idx, [1:   8]) = [  5.28160E-03 0.00328 -1.33678E-04 0.01458 -5.09423E-05 0.03232 -1.72456E-02 0.00296 ];
INF_SP7                   (idx, [1:   8]) = [  9.27849E-04 0.01320 -1.67375E-04 0.00937 -4.67932E-05 0.02864  4.83457E-04 0.10977 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31996E-01 0.00064  1.24122E+00 0.00896 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33600E-01 0.00096  1.34050E+00 0.00982 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33932E-01 0.00090  1.36072E+00 0.00974 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28544E-01 0.00101  1.06964E+00 0.01045 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43682E+00 0.00064  2.69059E-01 0.00877 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42697E+00 0.00095  2.49229E-01 0.00963 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42494E+00 0.00090  2.45517E-01 0.00958 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45854E+00 0.00101  3.12431E-01 0.01024 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.79141E-03 0.01030  9.74244E-05 0.06855  9.04736E-04 0.02345  7.65682E-04 0.02476  1.99175E-03 0.01608  8.17176E-04 0.02366  2.14634E-04 0.04701 ];
LAMBDA                    (idx, [1:  14]) = [  7.32593E-01 0.02375  1.26103E-02 0.00147  3.06129E-02 0.00062  1.11143E-01 0.00086  3.22754E-01 0.00058  1.25813E+00 0.00344  8.04370E+00 0.01396 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0007' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:45:38 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:44:43 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292338 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.90099E-01  9.64963E-01  1.01499E+00  1.01707E+00  1.01288E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16818E-02 0.00108  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88318E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05394E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05827E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68735E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.46638E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.46548E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.34985E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.20639E-01 0.00111  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000814 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00016E+04 0.00097 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00016E+04 0.00097 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.92922E+02 ;
RUNNING_TIME              (idx, 1)        =  1.19090E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.82450E-01  4.82450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.69500E-01  1.08167E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.17172E+02  3.32117E+00  2.62997E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.05632E+00  2.74500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.87667E-02  9.16664E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.19089E+02  1.25163E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97879 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00016E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81253E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.82475E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68289E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.96810E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.21267E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.56315E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61204E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.52654E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  6.37598E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.20718E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.44023E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.02805E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  9.35741E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.17913E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.08546E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.42264E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.59568E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.62933E+14 ;
CS134_ACTIVITY            (idx, 1)        =  7.89328E+13 ;
CS137_ACTIVITY            (idx, 1)        =  4.66029E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.57261E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.18253E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.35843E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.37141E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.60665E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 19 ;
BURNUP                     (idx, [1:  2])  = [  3.50000E+01  3.50032E+01 ];
BURN_DAYS                 (idx, 1)        =  8.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  8.01642E-01 0.00109 ];
U235_FISS                 (idx, [1:   4]) = [  3.38591E+15 0.00161  2.49184E-01 0.00150 ];
U238_FISS                 (idx, [1:   4]) = [  1.26173E+15 0.00288  9.28374E-02 0.00267 ];
PU239_FISS                (idx, [1:   4]) = [  6.87443E+15 0.00107  5.05900E-01 0.00080 ];
PU240_FISS                (idx, [1:   4]) = [  1.86506E+13 0.02269  1.37272E-03 0.02266 ];
PU241_FISS                (idx, [1:   4]) = [  2.00229E+15 0.00217  1.47348E-01 0.00202 ];
U235_CAPT                 (idx, [1:   4]) = [  7.62030E+14 0.00326  2.37120E-02 0.00327 ];
U238_CAPT                 (idx, [1:   4]) = [  1.08030E+16 0.00105  3.36107E-01 0.00076 ];
PU239_CAPT                (idx, [1:   4]) = [  3.76768E+15 0.00152  1.17235E-01 0.00150 ];
PU240_CAPT                (idx, [1:   4]) = [  3.11348E+15 0.00182  9.68668E-02 0.00167 ];
PU241_CAPT                (idx, [1:   4]) = [  7.09865E+14 0.00368  2.20867E-02 0.00364 ];
XE135_CAPT                (idx, [1:   4]) = [  7.99712E+14 0.00336  2.48823E-02 0.00333 ];
SM149_CAPT                (idx, [1:   4]) = [  2.61858E+14 0.00579  8.14778E-03 0.00578 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000814 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.25080E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000814 5.00825E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3483108 3.48847E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1472896 1.47497E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44810 4.48155E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000814 5.00825E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.77420E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 3.7E-09  4.50057E+05 3.7E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.75781E+16 2.1E-05  3.75781E+16 2.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35788E+16 4.3E-06  1.35788E+16 4.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.21252E+16 0.00044  2.51373E+16 0.00046  6.98790E+15 0.00118 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.57040E+16 0.00031  3.87161E+16 0.00030  6.98790E+15 0.00118 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.60665E+16 0.00051  4.60665E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.14485E+18 0.00050  5.69402E+17 0.00051  1.57545E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.12956E+14 0.00495 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.61170E+16 0.00032 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.59881E+18 0.00064 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.08461E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.08461E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.39907E+00 0.00066 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.76459E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.13795E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23572E+00 0.00041 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94124E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96895E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.23874E-01 0.00068 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.16489E-01 0.00069 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.76741E+00 2.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06869E+02 4.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.16469E-01 0.00069  8.12623E-01 0.00069  3.86654E-03 0.01196 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.16240E-01 0.00032 ];
COL_KEFF                  (idx, [1:   2]) = [  8.15842E-01 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.16240E-01 0.00032 ];
ABS_KINF                  (idx, [1:   2]) = [  8.23625E-01 0.00031 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.68787E+01 0.00028 ];
IMP_ALF                   (idx, [1:   2]) = [  1.68790E+01 0.00011 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  9.40001E-07 0.00473 ];
IMP_EALF                  (idx, [1:   2]) = [  9.35309E-07 0.00187 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.15520E-01 0.00297 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.16434E-01 0.00124 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.04337E-03 0.00608  1.43564E-04 0.04199  1.13072E-03 0.01505  9.37272E-04 0.01600  2.55220E-03 0.00981  1.00202E-03 0.01532  2.77593E-04 0.03062 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.20387E-01 0.01524  8.70704E-03 0.03003  3.05115E-02 0.00043  1.11521E-01 0.00062  3.23220E-01 0.00040  1.23958E+00 0.00268  6.83871E+00 0.02083 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.81954E-03 0.00955  1.05032E-04 0.06737  9.04274E-04 0.02433  7.28399E-04 0.02364  2.03779E-03 0.01399  8.06843E-04 0.02473  2.37206E-04 0.04850 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.41792E-01 0.02395  1.26149E-02 0.00141  3.05115E-02 0.00061  1.11582E-01 0.00093  3.23310E-01 0.00062  1.24086E+00 0.00384  7.69010E+00 0.01535 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.09397E-05 0.00159  3.09257E-05 0.00159  3.43269E-05 0.01916 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.52553E-05 0.00143  2.52438E-05 0.00144  2.80254E-05 0.01917 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.73206E-03 0.01204  1.06431E-04 0.08091  8.98993E-04 0.02910  7.36024E-04 0.03063  1.99733E-03 0.01791  7.54463E-04 0.03096  2.38821E-04 0.05855 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.28370E-01 0.02980  1.26076E-02 0.00220  3.04797E-02 0.00084  1.11460E-01 0.00124  3.23125E-01 0.00087  1.23590E+00 0.00541  7.54626E+00 0.02347 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.05118E-05 0.00382  3.05015E-05 0.00385  2.78362E-05 0.04800 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.49057E-05 0.00376  2.48971E-05 0.00378  2.27274E-05 0.04792 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.62621E-03 0.03948  9.61172E-05 0.30041  9.08184E-04 0.09846  7.73175E-04 0.10709  1.92527E-03 0.05876  6.51113E-04 0.11757  2.72346E-04 0.19169 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.19586E-01 0.09404  1.25504E-02 0.00492  3.04519E-02 0.00199  1.11698E-01 0.00305  3.23840E-01 0.00212  1.24656E+00 0.01309  7.32599E+00 0.06174 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.62715E-03 0.03849  1.04093E-04 0.29306  8.82617E-04 0.09472  7.73253E-04 0.10512  1.93128E-03 0.05686  6.67623E-04 0.11833  2.68289E-04 0.19164 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.10370E-01 0.09273  1.25504E-02 0.00492  3.04468E-02 0.00198  1.11675E-01 0.00304  3.23877E-01 0.00210  1.24566E+00 0.01313  7.31127E+00 0.06184 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.52237E+02 0.03931 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.06453E-05 0.00097 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.50149E-05 0.00069 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.56902E-03 0.00715 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.49129E+02 0.00716 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.62802E-07 0.00077 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.61272E-06 0.00041  2.61251E-06 0.00041  2.65877E-06 0.00606 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.95304E-05 0.00094  3.95489E-05 0.00095  3.59508E-05 0.01169 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.11885E-01 0.00036  6.12494E-01 0.00037  5.21282E-01 0.00986 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08295E+01 0.01470 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.46548E+01 0.00050  3.30940E+01 0.00058 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.15953E+04 0.00328  3.06039E+05 0.00082  6.10250E+05 0.00090  6.50486E+05 0.00082  5.97657E+05 0.00076  6.40826E+05 0.00068  4.35826E+05 0.00054  3.86122E+05 0.00083  2.95321E+05 0.00084  2.41285E+05 0.00088  2.07954E+05 0.00097  1.87290E+05 0.00099  1.73443E+05 0.00075  1.64727E+05 0.00088  1.60593E+05 0.00082  1.38851E+05 0.00085  1.36945E+05 0.00063  1.35686E+05 0.00081  1.33524E+05 0.00099  2.60124E+05 0.00067  2.51716E+05 0.00066  1.81196E+05 0.00090  1.17747E+05 0.00100  1.35094E+05 0.00080  1.29160E+05 0.00106  1.14320E+05 0.00093  1.84077E+05 0.00088  4.26839E+04 0.00115  5.22632E+04 0.00106  4.77377E+04 0.00145  2.81146E+04 0.00179  4.83084E+04 0.00158  3.14785E+04 0.00177  2.52881E+04 0.00192  4.20778E+03 0.00475  3.49477E+03 0.00303  3.08823E+03 0.00477  3.00038E+03 0.00420  3.03126E+03 0.00446  3.28878E+03 0.00349  3.95486E+03 0.00416  4.13126E+03 0.00441  8.32460E+03 0.00267  1.39680E+04 0.00208  1.83004E+04 0.00175  4.91676E+04 0.00179  5.17498E+04 0.00176  5.54271E+04 0.00084  3.59700E+04 0.00144  2.52467E+04 0.00164  1.86200E+04 0.00165  2.20130E+04 0.00195  4.31023E+04 0.00161  6.13771E+04 0.00161  1.23421E+05 0.00167  1.96826E+05 0.00139  3.00743E+05 0.00148  1.94787E+05 0.00149  1.40530E+05 0.00152  1.01502E+05 0.00166  9.16379E+04 0.00133  9.04479E+04 0.00199  7.56810E+04 0.00179  5.12939E+04 0.00181  4.76094E+04 0.00177  4.23607E+04 0.00194  3.58747E+04 0.00176  2.84004E+04 0.00176  1.90572E+04 0.00199  6.77328E+03 0.00221 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.23221E-01 0.00045 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.71637E+18 0.00045  4.28521E+17 0.00128 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.40008E-01 8.6E-05  1.56817E+00 0.00038 ];
INF_CAPT                  (idx, [1:   4]) = [  8.80658E-03 0.00058  3.96988E-02 0.00077 ];
INF_ABS                   (idx, [1:   4]) = [  1.03166E-02 0.00052  6.53433E-02 0.00095 ];
INF_FISS                  (idx, [1:   4]) = [  1.51001E-03 0.00049  2.56445E-02 0.00124 ];
INF_NSF                   (idx, [1:   4]) = [  4.17461E-03 0.00049  7.09857E-02 0.00128 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.76463E+00 6.1E-05  2.76807E+00 5.4E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06589E+02 8.4E-06  2.06935E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.45768E-08 0.00036  2.59857E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29691E-01 9.4E-05  1.50278E+00 0.00044 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43980E-01 0.00021  3.97519E-01 0.00049 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63295E-02 0.00028  9.36052E-02 0.00100 ];
INF_SCATT3                (idx, [1:   4]) = [  7.26597E-03 0.00290  2.79095E-02 0.00319 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03189E-02 0.00208 -9.04352E-03 0.00701 ];
INF_SCATT5                (idx, [1:   4]) = [  2.00714E-04 0.09429  6.62978E-03 0.00868 ];
INF_SCATT6                (idx, [1:   4]) = [  5.16535E-03 0.00385 -1.73446E-02 0.00324 ];
INF_SCATT7                (idx, [1:   4]) = [  7.84023E-04 0.01445  4.87441E-04 0.09105 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29736E-01 9.4E-05  1.50278E+00 0.00044 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43981E-01 0.00021  3.97519E-01 0.00049 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63299E-02 0.00028  9.36052E-02 0.00100 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.26594E-03 0.00290  2.79095E-02 0.00319 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03190E-02 0.00207 -9.04352E-03 0.00701 ];
INF_SCATTP5               (idx, [1:   4]) = [  2.00774E-04 0.09410  6.62978E-03 0.00868 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.16547E-03 0.00385 -1.73446E-02 0.00324 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.83859E-04 0.01446  4.87441E-04 0.09105 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12810E-01 0.00024  1.02121E+00 0.00033 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56635E+00 0.00024  3.26411E-01 0.00033 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.02723E-02 0.00053  6.53433E-02 0.00095 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70094E-02 0.00018  6.65288E-02 0.00116 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12999E-01 9.0E-05  1.66924E-02 0.00044  1.13444E-03 0.00475  1.50164E+00 0.00044 ];
INF_S1                    (idx, [1:   8]) = [  2.39175E-01 0.00021  4.80509E-03 0.00068  4.95411E-04 0.00810  3.97024E-01 0.00049 ];
INF_S2                    (idx, [1:   8]) = [  9.78675E-02 0.00029 -1.53801E-03 0.00256  2.71988E-04 0.01246  9.33333E-02 0.00101 ];
INF_S3                    (idx, [1:   8]) = [  8.99327E-03 0.00237 -1.72729E-03 0.00184  9.90841E-05 0.01925  2.78105E-02 0.00321 ];
INF_S4                    (idx, [1:   8]) = [ -9.79032E-03 0.00216 -5.28584E-04 0.00439  3.32849E-06 0.62761 -9.04685E-03 0.00703 ];
INF_S5                    (idx, [1:   8]) = [  1.53838E-04 0.12340  4.68757E-05 0.05136 -3.55053E-05 0.04952  6.66529E-03 0.00859 ];
INF_S6                    (idx, [1:   8]) = [  5.29161E-03 0.00368 -1.26265E-04 0.01582 -4.56166E-05 0.04062 -1.72990E-02 0.00323 ];
INF_S7                    (idx, [1:   8]) = [  9.48857E-04 0.01163 -1.64834E-04 0.01142 -4.39765E-05 0.03657  5.31417E-04 0.08320 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.13043E-01 9.0E-05  1.66924E-02 0.00044  1.13444E-03 0.00475  1.50164E+00 0.00044 ];
INF_SP1                   (idx, [1:   8]) = [  2.39176E-01 0.00021  4.80509E-03 0.00068  4.95411E-04 0.00810  3.97024E-01 0.00049 ];
INF_SP2                   (idx, [1:   8]) = [  9.78679E-02 0.00029 -1.53801E-03 0.00256  2.71988E-04 0.01246  9.33333E-02 0.00101 ];
INF_SP3                   (idx, [1:   8]) = [  8.99323E-03 0.00237 -1.72729E-03 0.00184  9.90841E-05 0.01925  2.78105E-02 0.00321 ];
INF_SP4                   (idx, [1:   8]) = [ -9.79042E-03 0.00215 -5.28584E-04 0.00439  3.32849E-06 0.62761 -9.04685E-03 0.00703 ];
INF_SP5                   (idx, [1:   8]) = [  1.53899E-04 0.12313  4.68757E-05 0.05136 -3.55053E-05 0.04952  6.66529E-03 0.00859 ];
INF_SP6                   (idx, [1:   8]) = [  5.29174E-03 0.00368 -1.26265E-04 0.01582 -4.56166E-05 0.04062 -1.72990E-02 0.00323 ];
INF_SP7                   (idx, [1:   8]) = [  9.48693E-04 0.01164 -1.64834E-04 0.01142 -4.39765E-05 0.03657  5.31417E-04 0.08320 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31851E-01 0.00058  1.22338E+00 0.00646 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33701E-01 0.00089  1.33969E+00 0.00930 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33631E-01 0.00081  1.33410E+00 0.00874 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28310E-01 0.00098  1.04715E+00 0.00565 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43772E+00 0.00058  2.72740E-01 0.00642 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42635E+00 0.00089  2.49327E-01 0.00924 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42677E+00 0.00081  2.50326E-01 0.00894 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.46003E+00 0.00098  3.18568E-01 0.00562 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.81954E-03 0.00955  1.05032E-04 0.06737  9.04274E-04 0.02433  7.28399E-04 0.02364  2.03779E-03 0.01399  8.06843E-04 0.02473  2.37206E-04 0.04850 ];
LAMBDA                    (idx, [1:  14]) = [  7.41792E-01 0.02395  1.26149E-02 0.00141  3.05115E-02 0.00061  1.11582E-01 0.00093  3.23310E-01 0.00062  1.24086E+00 0.00384  7.69010E+00 0.01535 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0007' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:45:38 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:50:49 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292338 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.93564E-01  1.00348E+00  9.93385E-01  1.00819E+00  1.00138E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.17558E-02 0.00114  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88244E-01 1.4E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05127E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05561E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69032E+00 0.00024  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.45984E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.45895E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.34811E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.23023E-01 0.00115  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000685 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00014E+04 0.00099 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00014E+04 0.00099 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  6.23371E+02 ;
RUNNING_TIME              (idx, 1)        =  1.25186E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.82450E-01  4.82450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.97650E-01  1.64833E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.23183E+02  3.44010E+00  2.57075E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.11378E+00  2.95167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  5.20000E-02  7.00001E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.25186E+02  1.25186E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97954 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99918E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81410E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.96729E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.69120E+04 ;
TOT_SF_RATE               (idx, 1)        =  6.15450E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.31232E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.63966E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.65493E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.52719E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  8.05513E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.37559E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  7.08006E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.10099E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  9.75077E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.27460E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.38882E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.46583E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.63869E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.68244E+14 ;
CS134_ACTIVITY            (idx, 1)        =  9.84979E+13 ;
CS137_ACTIVITY            (idx, 1)        =  5.31056E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.70041E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.16316E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.11555E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.40179E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.78427E+12 0.00054  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 20 ;
BURNUP                     (idx, [1:  2])  = [  4.00000E+01  4.00038E+01 ];
BURN_DAYS                 (idx, 1)        =  1.00000E+03 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  8.38897E-01 0.00120 ];
U235_FISS                 (idx, [1:   4]) = [  2.75019E+15 0.00184  2.02660E-01 0.00177 ];
U238_FISS                 (idx, [1:   4]) = [  1.30448E+15 0.00282  9.61078E-02 0.00263 ];
PU239_FISS                (idx, [1:   4]) = [  7.18885E+15 0.00104  5.29717E-01 0.00080 ];
PU240_FISS                (idx, [1:   4]) = [  2.03441E+13 0.02221  1.49844E-03 0.02216 ];
PU241_FISS                (idx, [1:   4]) = [  2.24821E+15 0.00209  1.65652E-01 0.00193 ];
U235_CAPT                 (idx, [1:   4]) = [  6.15860E+14 0.00397  1.81558E-02 0.00397 ];
U238_CAPT                 (idx, [1:   4]) = [  1.11839E+16 0.00114  3.29656E-01 0.00083 ];
PU239_CAPT                (idx, [1:   4]) = [  3.94005E+15 0.00146  1.16156E-01 0.00147 ];
PU240_CAPT                (idx, [1:   4]) = [  3.39873E+15 0.00180  1.00181E-01 0.00162 ];
PU241_CAPT                (idx, [1:   4]) = [  8.05497E+14 0.00348  2.37448E-02 0.00344 ];
XE135_CAPT                (idx, [1:   4]) = [  8.06081E+14 0.00335  2.37633E-02 0.00334 ];
SM149_CAPT                (idx, [1:   4]) = [  2.68391E+14 0.00617  7.91197E-03 0.00616 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000685 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.29345E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000685 5.00829E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3539849 3.54536E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1416327 1.41841E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44509 4.45248E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000685 5.00829E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.84288E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 3.7E-09  4.50057E+05 3.7E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.78174E+16 2.3E-05  3.78174E+16 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35577E+16 4.8E-06  1.35577E+16 4.8E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.39147E+16 0.00048  2.66897E+16 0.00050  7.22491E+15 0.00133 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.74723E+16 0.00034  4.02474E+16 0.00033  7.22491E+15 0.00133 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.78427E+16 0.00054  4.78427E+16 0.00054  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.22536E+18 0.00052  5.91361E+17 0.00050  1.63399E+18 0.00059 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.26067E+14 0.00504 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.78984E+16 0.00035 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.65735E+18 0.00071 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.07885E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.07885E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.36928E+00 0.00063 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.75843E-01 0.00036 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.08272E-01 0.00038 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23586E+00 0.00042 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94229E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96848E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  7.98534E-01 0.00068 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  7.91423E-01 0.00068 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.78937E+00 2.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.07191E+02 4.8E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  7.91441E-01 0.00070  7.87800E-01 0.00069  3.62377E-03 0.01256 ];
IMP_KEFF                  (idx, [1:   2]) = [  7.90900E-01 0.00035 ];
COL_KEFF                  (idx, [1:   2]) = [  7.90569E-01 0.00054 ];
ABS_KEFF                  (idx, [1:   2]) = [  7.90900E-01 0.00035 ];
ABS_KINF                  (idx, [1:   2]) = [  7.98009E-01 0.00034 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.68212E+01 0.00029 ];
IMP_ALF                   (idx, [1:   2]) = [  1.68234E+01 0.00011 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  9.95924E-07 0.00489 ];
IMP_EALF                  (idx, [1:   2]) = [  9.88725E-07 0.00182 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.28576E-01 0.00293 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.27815E-01 0.00119 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.15755E-03 0.00685  1.30591E-04 0.04212  1.15885E-03 0.01534  9.48307E-04 0.01670  2.57045E-03 0.01009  1.05239E-03 0.01597  2.96956E-04 0.02854 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.43201E-01 0.01541  8.45079E-03 0.03174  3.03849E-02 0.00037  1.11516E-01 0.00211  3.23364E-01 0.00044  1.22316E+00 0.00259  7.12741E+00 0.01925 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.67968E-03 0.01054  1.04690E-04 0.06546  8.91917E-04 0.02241  7.17026E-04 0.02566  1.95662E-03 0.01617  7.81618E-04 0.02513  2.27804E-04 0.04555 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.52976E-01 0.02396  1.26823E-02 0.00170  3.03880E-02 0.00056  1.11693E-01 0.00094  3.23530E-01 0.00065  1.22017E+00 0.00385  7.84019E+00 0.01542 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.19636E-05 0.00166  3.19513E-05 0.00166  3.45730E-05 0.01919 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.52903E-05 0.00145  2.52804E-05 0.00145  2.73728E-05 0.01927 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.57105E-03 0.01265  1.06182E-04 0.07963  8.62295E-04 0.02795  6.92155E-04 0.03060  1.94487E-03 0.01966  7.54777E-04 0.03068  2.10773E-04 0.05798 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.26864E-01 0.03177  1.26577E-02 0.00265  3.04342E-02 0.00083  1.11746E-01 0.00133  3.23420E-01 0.00089  1.22587E+00 0.00567  7.76662E+00 0.02456 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.10413E-05 0.00379  3.10301E-05 0.00378  2.88451E-05 0.05156 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.45627E-05 0.00376  2.45540E-05 0.00374  2.28017E-05 0.05144 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.14261E-03 0.04413  1.04315E-04 0.25018  6.99010E-04 0.10646  7.47084E-04 0.10953  1.75921E-03 0.06563  6.01679E-04 0.11353  2.31309E-04 0.19830 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.19260E-01 0.09391  1.26993E-02 0.00768  3.03883E-02 0.00204  1.11895E-01 0.00308  3.23108E-01 0.00229  1.22944E+00 0.01386  7.79708E+00 0.05993 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.15922E-03 0.04343  1.08788E-04 0.25189  6.77215E-04 0.10365  7.40388E-04 0.10726  1.78834E-03 0.06342  6.27239E-04 0.11262  2.17257E-04 0.19011 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.12843E-01 0.09258  1.26993E-02 0.00768  3.03944E-02 0.00205  1.11912E-01 0.00308  3.23170E-01 0.00228  1.23166E+00 0.01370  7.79951E+00 0.05992 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.34030E+02 0.04423 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.15557E-05 0.00100 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.49683E-05 0.00070 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.42433E-03 0.00861 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.40319E+02 0.00871 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.61075E-07 0.00085 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.59602E-06 0.00046  2.59586E-06 0.00046  2.62861E-06 0.00621 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.97212E-05 0.00101  3.97389E-05 0.00101  3.61222E-05 0.01248 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.06492E-01 0.00038  6.07216E-01 0.00038  5.02066E-01 0.01140 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08137E+01 0.01471 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.45895E+01 0.00053  3.30459E+01 0.00059 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.19426E+04 0.00305  3.05986E+05 0.00128  6.11069E+05 0.00097  6.50468E+05 0.00090  5.99110E+05 0.00077  6.42076E+05 0.00086  4.35768E+05 0.00048  3.86177E+05 0.00082  2.95465E+05 0.00079  2.41321E+05 0.00089  2.07944E+05 0.00085  1.87362E+05 0.00087  1.73368E+05 0.00061  1.64532E+05 0.00102  1.60573E+05 0.00071  1.38727E+05 0.00074  1.37095E+05 0.00096  1.35694E+05 0.00117  1.33521E+05 0.00106  2.60346E+05 0.00065  2.51937E+05 0.00057  1.81493E+05 0.00074  1.17740E+05 0.00081  1.35150E+05 0.00059  1.29123E+05 0.00074  1.13952E+05 0.00114  1.83058E+05 0.00077  4.24533E+04 0.00183  5.16295E+04 0.00117  4.74929E+04 0.00165  2.79915E+04 0.00186  4.80460E+04 0.00162  3.11858E+04 0.00204  2.48239E+04 0.00201  4.10471E+03 0.00345  3.41429E+03 0.00341  3.00591E+03 0.00406  2.90900E+03 0.00404  2.94210E+03 0.00394  3.20082E+03 0.00422  3.89394E+03 0.00345  4.05826E+03 0.00349  8.26260E+03 0.00251  1.37567E+04 0.00208  1.80397E+04 0.00232  4.87091E+04 0.00161  5.11316E+04 0.00159  5.47929E+04 0.00172  3.55686E+04 0.00195  2.50200E+04 0.00156  1.85268E+04 0.00221  2.17817E+04 0.00192  4.27562E+04 0.00148  6.10591E+04 0.00196  1.22850E+05 0.00191  1.95961E+05 0.00195  2.99238E+05 0.00199  1.94246E+05 0.00201  1.40201E+05 0.00205  1.01291E+05 0.00217  9.12143E+04 0.00232  9.01903E+04 0.00233  7.55401E+04 0.00233  5.11472E+04 0.00210  4.74116E+04 0.00231  4.22719E+04 0.00248  3.58661E+04 0.00211  2.82753E+04 0.00249  1.90017E+04 0.00240  6.72212E+03 0.00173 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  7.97672E-01 0.00053 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.78236E+18 0.00051  4.43037E+17 0.00173 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.40212E-01 9.4E-05  1.56761E+00 0.00051 ];
INF_CAPT                  (idx, [1:   4]) = [  9.00830E-03 0.00082  4.03146E-02 0.00104 ];
INF_ABS                   (idx, [1:   4]) = [  1.04632E-02 0.00073  6.50693E-02 0.00128 ];
INF_FISS                  (idx, [1:   4]) = [  1.45489E-03 0.00043  2.47546E-02 0.00169 ];
INF_NSF                   (idx, [1:   4]) = [  4.04873E-03 0.00048  6.90882E-02 0.00175 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.78284E+00 8.5E-05  2.79092E+00 7.4E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06840E+02 1.0E-05  2.07274E+02 1.5E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.42083E-08 0.00035  2.60038E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29746E-01 9.5E-05  1.50250E+00 0.00059 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43900E-01 0.00021  3.97385E-01 0.00066 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62632E-02 0.00032  9.35868E-02 0.00106 ];
INF_SCATT3                (idx, [1:   4]) = [  7.21062E-03 0.00349  2.80919E-02 0.00314 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03740E-02 0.00205 -8.92588E-03 0.00630 ];
INF_SCATT5                (idx, [1:   4]) = [  1.77007E-04 0.08204  6.73930E-03 0.00916 ];
INF_SCATT6                (idx, [1:   4]) = [  5.14795E-03 0.00288 -1.73116E-02 0.00273 ];
INF_SCATT7                (idx, [1:   4]) = [  7.68240E-04 0.01813  4.29549E-04 0.11340 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29791E-01 9.5E-05  1.50250E+00 0.00059 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43901E-01 0.00021  3.97385E-01 0.00066 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62634E-02 0.00032  9.35868E-02 0.00106 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.21094E-03 0.00349  2.80919E-02 0.00314 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03743E-02 0.00205 -8.92588E-03 0.00630 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.76542E-04 0.08251  6.73930E-03 0.00916 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.14775E-03 0.00287 -1.73116E-02 0.00273 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.68173E-04 0.01816  4.29549E-04 0.11340 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12793E-01 0.00028  1.02172E+00 0.00048 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56647E+00 0.00028  3.26250E-01 0.00048 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.04187E-02 0.00074  6.50693E-02 0.00128 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70116E-02 0.00026  6.62300E-02 0.00153 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.13201E-01 9.2E-05  1.65458E-02 0.00052  1.12507E-03 0.00587  1.50138E+00 0.00059 ];
INF_S1                    (idx, [1:   8]) = [  2.39140E-01 0.00021  4.75939E-03 0.00085  4.88950E-04 0.00605  3.96896E-01 0.00066 ];
INF_S2                    (idx, [1:   8]) = [  9.77981E-02 0.00031 -1.53496E-03 0.00190  2.66800E-04 0.00798  9.33200E-02 0.00107 ];
INF_S3                    (idx, [1:   8]) = [  8.92861E-03 0.00275 -1.71799E-03 0.00160  9.57723E-05 0.02124  2.79961E-02 0.00314 ];
INF_S4                    (idx, [1:   8]) = [ -9.85627E-03 0.00210 -5.17746E-04 0.00463  3.60466E-06 0.40091 -8.92948E-03 0.00631 ];
INF_S5                    (idx, [1:   8]) = [  1.23451E-04 0.11709  5.35564E-05 0.04896 -3.45549E-05 0.04702  6.77386E-03 0.00910 ];
INF_S6                    (idx, [1:   8]) = [  5.27640E-03 0.00272 -1.28448E-04 0.01555 -4.74060E-05 0.01737 -1.72642E-02 0.00274 ];
INF_S7                    (idx, [1:   8]) = [  9.32613E-04 0.01483 -1.64373E-04 0.01299 -4.34835E-05 0.02710  4.73032E-04 0.10330 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.13245E-01 9.2E-05  1.65458E-02 0.00052  1.12507E-03 0.00587  1.50138E+00 0.00059 ];
INF_SP1                   (idx, [1:   8]) = [  2.39141E-01 0.00021  4.75939E-03 0.00085  4.88950E-04 0.00605  3.96896E-01 0.00066 ];
INF_SP2                   (idx, [1:   8]) = [  9.77983E-02 0.00031 -1.53496E-03 0.00190  2.66800E-04 0.00798  9.33200E-02 0.00107 ];
INF_SP3                   (idx, [1:   8]) = [  8.92892E-03 0.00274 -1.71799E-03 0.00160  9.57723E-05 0.02124  2.79961E-02 0.00314 ];
INF_SP4                   (idx, [1:   8]) = [ -9.85651E-03 0.00209 -5.17746E-04 0.00463  3.60466E-06 0.40091 -8.92948E-03 0.00631 ];
INF_SP5                   (idx, [1:   8]) = [  1.22985E-04 0.11788  5.35564E-05 0.04896 -3.45549E-05 0.04702  6.77386E-03 0.00910 ];
INF_SP6                   (idx, [1:   8]) = [  5.27620E-03 0.00271 -1.28448E-04 0.01555 -4.74060E-05 0.01737 -1.72642E-02 0.00274 ];
INF_SP7                   (idx, [1:   8]) = [  9.32547E-04 0.01485 -1.64373E-04 0.01299 -4.34835E-05 0.02710  4.73032E-04 0.10330 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31837E-01 0.00046  1.23177E+00 0.00758 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33514E-01 0.00075  1.33552E+00 0.00839 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33624E-01 0.00092  1.34734E+00 0.01218 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28457E-01 0.00090  1.06021E+00 0.00619 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43780E+00 0.00046  2.70983E-01 0.00755 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42748E+00 0.00075  2.50019E-01 0.00855 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42682E+00 0.00092  2.48235E-01 0.01158 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45909E+00 0.00089  3.14695E-01 0.00628 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.67968E-03 0.01054  1.04690E-04 0.06546  8.91917E-04 0.02241  7.17026E-04 0.02566  1.95662E-03 0.01617  7.81618E-04 0.02513  2.27804E-04 0.04555 ];
LAMBDA                    (idx, [1:  14]) = [  7.52976E-01 0.02396  1.26823E-02 0.00170  3.03880E-02 0.00056  1.11693E-01 0.00094  3.23530E-01 0.00065  1.22017E+00 0.00385  7.84019E+00 0.01542 ];

