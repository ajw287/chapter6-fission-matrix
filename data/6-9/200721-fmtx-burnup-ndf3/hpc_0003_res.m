
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0003' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:41 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Mon Jul 20 23:44:36 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284841 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99188E-01  1.00374E+00  9.99397E-01  1.00385E+00  9.93828E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.6E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13041E-02 0.00103  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88696E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.95609E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.96030E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.72441E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.74089E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.74005E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.58284E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.47701E-01 0.00109  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000471 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00009E+04 0.00073 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00009E+04 0.00073 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.78533E+01 ;
RUNNING_TIME              (idx, 1)        =  3.91633E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  4.31417E-01  4.31417E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.30000E-03  1.30000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.48340E+00  3.48340E+00  0.00000E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.91600E+00  0.00000E+00 ];
CPU_USAGE                 (idx, 1)        = 4.55867 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99857E+00 0.00021 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  8.78007E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.63627E+08 ;
TOT_DECAY_HEAT            (idx, 1)        =  6.57878E-04 ;
TOT_SF_RATE               (idx, 1)        =  7.42888E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  8.63627E+08 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  6.57878E-04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  7.90295E+03 ;
INGESTION_TOXICITY        (idx, 1)        =  4.17170E+01 ;
ACTINIDE_INH_TOX          (idx, 1)        =  7.90295E+03 ;
ACTINIDE_ING_TOX          (idx, 1)        =  4.17170E+01 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  1.09832E+08 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  8.63261E+08 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  3.53344E+08 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.03212E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 0 ;
BURNUP                     (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
BURN_DAYS                 (idx, 1)        =  0.00000E+00 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.10334E-01 0.00117 ];
U235_FISS                 (idx, [1:   4]) = [  1.30320E+16 0.00059  9.39533E-01 0.00017 ];
U238_FISS                 (idx, [1:   4]) = [  8.37382E+14 0.00282  6.03629E-02 0.00267 ];
U235_CAPT                 (idx, [1:   4]) = [  2.76904E+15 0.00144  1.70509E-01 0.00138 ];
U238_CAPT                 (idx, [1:   4]) = [  8.00760E+15 0.00102  4.93026E-01 0.00065 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000471 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.30537E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000471 5.00731E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2674392 2.67816E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2284369 2.28742E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 41710 4.17230E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000471 5.00731E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.56348E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50058E+05 5.4E-09  4.50058E+05 5.4E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.41093E+16 1.1E-05  3.41093E+16 1.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38679E+16 1.3E-06  1.38679E+16 1.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.62256E+16 0.00049  1.08794E+16 0.00051  5.34616E+15 0.00097 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.00935E+16 0.00026  2.47473E+16 0.00022  5.34616E+15 0.00097 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.03212E+16 0.00050  3.03212E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.48408E+18 0.00046  3.99052E+17 0.00044  1.08503E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.53047E+14 0.00512 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.03465E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.13554E+18 0.00057 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12515E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.77577E+00 0.00039 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.63203E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.81102E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.22932E+00 0.00032 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94598E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97041E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.13470E+00 0.00050 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.12524E+00 0.00051 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.45958E+00 1.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02557E+02 1.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.12513E+00 0.00052  1.11724E+00 0.00052  7.99059E-03 0.00763 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.12565E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.12507E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.12565E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.13513E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.76354E+01 0.00018 ];
IMP_ALF                   (idx, [1:   2]) = [  1.76350E+01 7.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.39738E-07 0.00323 ];
IMP_EALF                  (idx, [1:   2]) = [  4.38995E-07 0.00139 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.03763E-01 0.00294 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.03540E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.37596E-03 0.00528  1.75483E-04 0.03166  1.00159E-03 0.01333  1.00087E-03 0.01324  2.92300E-03 0.00779  9.59867E-04 0.01337  3.15158E-04 0.02385 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.09180E-01 0.01211  1.07171E-02 0.01821  3.16605E-02 0.00021  1.10176E-01 0.00027  3.20595E-01 0.00021  1.34597E+00 0.00016  8.78713E+00 0.00479 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.11716E-03 0.00763  1.80305E-04 0.04614  1.14570E-03 0.01967  1.10223E-03 0.02007  3.25205E-03 0.01147  1.07141E-03 0.01971  3.65468E-04 0.03606 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.25259E-01 0.01871  1.24908E-02 2.9E-06  3.16750E-02 0.00027  1.10177E-01 0.00040  3.20756E-01 0.00031  1.34554E+00 0.00024  8.87674E+00 0.00195 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.78720E-05 0.00112  2.78568E-05 0.00112  3.00382E-05 0.01113 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.13548E-05 0.00096  3.13378E-05 0.00097  3.37918E-05 0.01109 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.10689E-03 0.00776  1.93560E-04 0.04688  1.10874E-03 0.02062  1.12466E-03 0.01976  3.23130E-03 0.01214  1.08241E-03 0.02049  3.66223E-04 0.03616 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.28470E-01 0.01912  1.24907E-02 3.0E-06  3.16603E-02 0.00033  1.10162E-01 0.00043  3.20838E-01 0.00034  1.34536E+00 0.00028  8.89580E+00 0.00235 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.78753E-05 0.00252  2.78556E-05 0.00252  3.00341E-05 0.02573 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.13594E-05 0.00247  3.13372E-05 0.00248  3.38006E-05 0.02577 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.10387E-03 0.02462  1.97452E-04 0.15049  1.05136E-03 0.06436  1.21501E-03 0.06244  3.21320E-03 0.03597  1.06719E-03 0.06245  3.59666E-04 0.10822 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.11109E-01 0.05883  1.24910E-02 1.0E-05  3.16633E-02 0.00080  1.10114E-01 0.00098  3.21223E-01 0.00103  1.34662E+00 0.00057  8.93478E+00 0.00533 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.05636E-03 0.02405  1.99178E-04 0.14626  1.05333E-03 0.06228  1.19906E-03 0.06104  3.16402E-03 0.03465  1.08387E-03 0.05923  3.56906E-04 0.10522 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.20405E-01 0.05674  1.24909E-02 1.0E-05  3.16679E-02 0.00077  1.10131E-01 0.00098  3.21175E-01 0.00101  1.34656E+00 0.00057  8.93837E+00 0.00533 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.56024E+02 0.02485 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.78455E-05 0.00068 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.13255E-05 0.00045 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.13659E-03 0.00509 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.56371E+02 0.00516 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.50711E-07 0.00060 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87778E-06 0.00043  2.87749E-06 0.00043  2.91673E-06 0.00474 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.36873E-05 0.00077  4.37086E-05 0.00078  4.08457E-05 0.00792 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.78785E-01 0.00031  6.78119E-01 0.00032  7.94108E-01 0.00859 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.01371E+01 0.01267 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.74005E+01 0.00044  3.66471E+01 0.00045 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.15108E+04 0.00354  2.90772E+05 0.00152  6.02658E+05 0.00080  6.49738E+05 0.00073  5.98199E+05 0.00071  6.42920E+05 0.00075  4.36116E+05 0.00073  3.86077E+05 0.00066  2.95376E+05 0.00071  2.41491E+05 0.00093  2.08079E+05 0.00075  1.87690E+05 0.00062  1.73072E+05 0.00067  1.64618E+05 0.00091  1.60297E+05 0.00095  1.38180E+05 0.00061  1.36832E+05 0.00089  1.35614E+05 0.00099  1.33122E+05 0.00091  2.60197E+05 0.00089  2.50599E+05 0.00058  1.81064E+05 0.00071  1.17138E+05 0.00110  1.35434E+05 0.00077  1.28299E+05 0.00088  1.16574E+05 0.00126  1.91963E+05 0.00077  4.38962E+04 0.00157  5.49840E+04 0.00133  5.00606E+04 0.00116  2.90038E+04 0.00181  5.05559E+04 0.00165  3.43072E+04 0.00185  2.92667E+04 0.00152  5.57113E+03 0.00282  5.54016E+03 0.00458  5.69258E+03 0.00371  5.82983E+03 0.00279  5.84690E+03 0.00414  5.70350E+03 0.00403  5.89420E+03 0.00320  5.56237E+03 0.00415  1.04827E+04 0.00251  1.67372E+04 0.00203  2.14122E+04 0.00206  5.63774E+04 0.00157  5.94379E+04 0.00142  6.56863E+04 0.00102  4.54899E+04 0.00132  3.43988E+04 0.00151  2.67151E+04 0.00157  3.19758E+04 0.00173  6.12950E+04 0.00142  8.34735E+04 0.00115  1.61738E+05 0.00073  2.50259E+05 0.00097  3.74533E+05 0.00097  2.39663E+05 0.00106  1.71452E+05 0.00108  1.23073E+05 0.00107  1.10564E+05 0.00108  1.08709E+05 0.00121  9.05706E+04 0.00128  6.11576E+04 0.00137  5.65961E+04 0.00132  5.02127E+04 0.00134  4.24134E+04 0.00140  3.33411E+04 0.00154  2.22863E+04 0.00190  7.91255E+03 0.00174 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.13454E+00 0.00057 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.13346E+18 0.00053  3.50656E+17 0.00093 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38196E-01 0.00012  1.49875E+00 0.00031 ];
INF_CAPT                  (idx, [1:   4]) = [  6.30340E-03 0.00050  2.58991E-02 0.00027 ];
INF_ABS                   (idx, [1:   4]) = [  8.48993E-03 0.00047  5.83852E-02 0.00065 ];
INF_FISS                  (idx, [1:   4]) = [  2.18653E-03 0.00048  3.24861E-02 0.00096 ];
INF_NSF                   (idx, [1:   4]) = [  5.60791E-03 0.00046  7.91589E-02 0.00096 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.56475E+00 5.1E-05  2.43670E+00 3.8E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03876E+02 6.9E-06  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  5.92526E-08 0.00039  2.56265E-06 0.00012 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29697E-01 0.00013  1.44035E+00 0.00034 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44456E-01 0.00023  3.79770E-01 0.00042 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63952E-02 0.00025  9.00326E-02 0.00106 ];
INF_SCATT3                (idx, [1:   4]) = [  7.39727E-03 0.00289  2.71273E-02 0.00262 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03305E-02 0.00203 -8.34818E-03 0.00720 ];
INF_SCATT5                (idx, [1:   4]) = [  1.34409E-04 0.12318  6.33925E-03 0.00795 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08059E-03 0.00318 -1.62780E-02 0.00303 ];
INF_SCATT7                (idx, [1:   4]) = [  7.62119E-04 0.02109  2.73319E-04 0.13350 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29736E-01 0.00013  1.44035E+00 0.00034 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44457E-01 0.00023  3.79770E-01 0.00042 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63953E-02 0.00025  9.00326E-02 0.00106 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.39732E-03 0.00289  2.71273E-02 0.00262 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03303E-02 0.00203 -8.34818E-03 0.00720 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.34248E-04 0.12312  6.33925E-03 0.00795 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08061E-03 0.00318 -1.62780E-02 0.00303 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.62086E-04 0.02107  2.73319E-04 0.13350 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13618E-01 0.00033  9.67960E-01 0.00028 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56042E+00 0.00033  3.44367E-01 0.00028 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.45085E-03 0.00048  5.83852E-02 0.00065 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69675E-02 0.00022  5.94444E-02 0.00074 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11229E-01 0.00012  1.84687E-02 0.00034  1.04039E-03 0.00394  1.43931E+00 0.00034 ];
INF_S1                    (idx, [1:   8]) = [  2.39054E-01 0.00022  5.40183E-03 0.00109  4.46922E-04 0.00694  3.79323E-01 0.00042 ];
INF_S2                    (idx, [1:   8]) = [  9.79868E-02 0.00024 -1.59160E-03 0.00317  2.43358E-04 0.00807  8.97892E-02 0.00107 ];
INF_S3                    (idx, [1:   8]) = [  9.30294E-03 0.00228 -1.90568E-03 0.00183  8.80069E-05 0.01894  2.70393E-02 0.00265 ];
INF_S4                    (idx, [1:   8]) = [ -9.69118E-03 0.00207 -6.39314E-04 0.00361 -1.49144E-06 0.86507 -8.34669E-03 0.00719 ];
INF_S5                    (idx, [1:   8]) = [  1.18867E-04 0.12463  1.55424E-05 0.22160 -3.54495E-05 0.04019  6.37470E-03 0.00790 ];
INF_S6                    (idx, [1:   8]) = [  5.22454E-03 0.00302 -1.43945E-04 0.01600 -4.62028E-05 0.02884 -1.62318E-02 0.00306 ];
INF_S7                    (idx, [1:   8]) = [  9.33023E-04 0.01752 -1.70904E-04 0.01242 -4.01275E-05 0.03771  3.13447E-04 0.11542 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11268E-01 0.00012  1.84687E-02 0.00034  1.04039E-03 0.00394  1.43931E+00 0.00034 ];
INF_SP1                   (idx, [1:   8]) = [  2.39055E-01 0.00022  5.40183E-03 0.00109  4.46922E-04 0.00694  3.79323E-01 0.00042 ];
INF_SP2                   (idx, [1:   8]) = [  9.79869E-02 0.00024 -1.59160E-03 0.00317  2.43358E-04 0.00807  8.97892E-02 0.00107 ];
INF_SP3                   (idx, [1:   8]) = [  9.30300E-03 0.00227 -1.90568E-03 0.00183  8.80069E-05 0.01894  2.70393E-02 0.00265 ];
INF_SP4                   (idx, [1:   8]) = [ -9.69101E-03 0.00207 -6.39314E-04 0.00361 -1.49144E-06 0.86507 -8.34669E-03 0.00719 ];
INF_SP5                   (idx, [1:   8]) = [  1.18705E-04 0.12451  1.55424E-05 0.22160 -3.54495E-05 0.04019  6.37470E-03 0.00790 ];
INF_SP6                   (idx, [1:   8]) = [  5.22456E-03 0.00302 -1.43945E-04 0.01600 -4.62028E-05 0.02884 -1.62318E-02 0.00306 ];
INF_SP7                   (idx, [1:   8]) = [  9.32990E-04 0.01750 -1.70904E-04 0.01242 -4.01275E-05 0.03771  3.13447E-04 0.11542 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32513E-01 0.00057  1.07834E+00 0.00617 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34157E-01 0.00112  1.15337E+00 0.00799 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34214E-01 0.00073  1.13537E+00 0.00717 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29248E-01 0.00103  9.67771E-01 0.00659 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43362E+00 0.00057  3.09392E-01 0.00601 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42359E+00 0.00111  2.89435E-01 0.00771 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42322E+00 0.00073  2.93947E-01 0.00705 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45407E+00 0.00103  3.44792E-01 0.00657 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.11716E-03 0.00763  1.80305E-04 0.04614  1.14570E-03 0.01967  1.10223E-03 0.02007  3.25205E-03 0.01147  1.07141E-03 0.01971  3.65468E-04 0.03606 ];
LAMBDA                    (idx, [1:  14]) = [  8.25259E-01 0.01871  1.24908E-02 2.9E-06  3.16750E-02 0.00027  1.10177E-01 0.00040  3.20756E-01 0.00031  1.34554E+00 0.00024  8.87674E+00 0.00195 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0003' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:41 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Mon Jul 20 23:50:51 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284841 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.92457E-01  1.00387E+00  1.00094E+00  1.00417E+00  9.98553E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12862E-02 0.00105  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88714E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.97663E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.98084E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.71689E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.71289E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.71204E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.39100E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.41547E-01 0.00106  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000582 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00012E+04 0.00076 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00012E+04 0.00076 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.90571E+01 ;
RUNNING_TIME              (idx, 1)        =  1.01629E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.31417E-01  4.31417E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.44000E-02  6.40000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.65963E+00  3.47065E+00  2.70558E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.67000E-02  2.71167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.35000E-03  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.01627E+01  1.29027E+02 ];
CPU_USAGE                 (idx, 1)        = 4.82708 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00002E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.44224E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  7.91900E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.75874E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.42874E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.23300E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  8.74343E+02 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  6.68598E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.67129E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.23110E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  1.76211E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.46656E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  3.57696E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.84446E+06 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  1.40441E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.28288E+11 ;
TE132_ACTIVITY            (idx, 1)        =  2.51461E+14 ;
I131_ACTIVITY             (idx, 1)        =  7.42481E+13 ;
I132_ACTIVITY             (idx, 1)        =  2.43645E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.63874E+07 ;
CS137_ACTIVITY            (idx, 1)        =  1.34667E+11 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  6.60504E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.65103E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.32392E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  9.12846E+16 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.14274E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 1 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E-01  1.00009E-01 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+00 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.22526E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  1.29748E+16 0.00058  9.35402E-01 0.00018 ];
U238_FISS                 (idx, [1:   4]) = [  8.69207E+14 0.00278  6.26612E-02 0.00268 ];
PU239_FISS                (idx, [1:   4]) = [  2.48851E+13 0.01528  1.79365E-03 0.01524 ];
U235_CAPT                 (idx, [1:   4]) = [  2.78091E+15 0.00153  1.60434E-01 0.00138 ];
U238_CAPT                 (idx, [1:   4]) = [  8.19457E+15 0.00103  4.72729E-01 0.00066 ];
PU239_CAPT                (idx, [1:   4]) = [  1.36560E+13 0.02097  7.87794E-04 0.02094 ];
PU240_CAPT                (idx, [1:   4]) = [  1.75321E+11 0.18385  1.01275E-05 0.18383 ];
XE135_CAPT                (idx, [1:   4]) = [  7.22901E+14 0.00297  4.17090E-02 0.00297 ];
SM149_CAPT                (idx, [1:   4]) = [  2.15234E+13 0.01728  1.24121E-03 0.01722 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000582 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.21266E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000582 5.00721E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2754084 2.75775E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2203965 2.20692E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 42533 4.25393E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000582 5.00721E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -9.31323E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50058E+05 5.4E-09  4.50058E+05 5.4E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.41286E+16 1.2E-05  3.41286E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38665E+16 1.4E-06  1.38665E+16 1.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.73357E+16 0.00048  1.18756E+16 0.00049  5.46011E+15 0.00100 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.12022E+16 0.00027  2.57421E+16 0.00022  5.46011E+15 0.00100 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.14274E+16 0.00051  3.14274E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.53010E+18 0.00046  4.10480E+17 0.00044  1.11962E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.67413E+14 0.00535 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.14696E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.16814E+18 0.00058 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12503E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12503E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.69202E+00 0.00042 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.67671E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.81020E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23873E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94464E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97011E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.09570E+00 0.00051 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.08638E+00 0.00051 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46123E+00 1.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02578E+02 1.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.08634E+00 0.00053  1.07876E+00 0.00051  7.61538E-03 0.00806 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.08609E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.08609E+00 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.08609E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.09541E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75599E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75598E+01 8.3E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.74291E-07 0.00328 ];
IMP_EALF                  (idx, [1:   2]) = [  4.73284E-07 0.00145 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.11655E-01 0.00288 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.11171E-01 0.00121 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.56592E-03 0.00545  1.80476E-04 0.03159  1.03825E-03 0.01289  1.02825E-03 0.01228  3.02688E-03 0.00798  9.71350E-04 0.01354  3.20707E-04 0.02375 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.03775E-01 0.01217  1.07171E-02 0.01821  3.16487E-02 0.00022  1.10203E-01 0.00026  3.20672E-01 0.00022  1.34571E+00 0.00016  8.71822E+00 0.00661 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.13688E-03 0.00816  1.92599E-04 0.05005  1.14541E-03 0.01931  1.13953E-03 0.02027  3.25514E-03 0.01131  1.05802E-03 0.02057  3.46181E-04 0.03736 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.95706E-01 0.01901  1.24908E-02 3.5E-06  3.16541E-02 0.00031  1.10227E-01 0.00039  3.20586E-01 0.00032  1.34512E+00 0.00023  8.91496E+00 0.00211 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.76404E-05 0.00110  2.76250E-05 0.00110  2.97904E-05 0.01100 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.00232E-05 0.00100  3.00065E-05 0.00100  3.23583E-05 0.01098 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.00792E-03 0.00816  1.86772E-04 0.04700  1.08649E-03 0.02037  1.12537E-03 0.02063  3.22669E-03 0.01199  1.04030E-03 0.02081  3.42314E-04 0.03971 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.04523E-01 0.02084  1.24907E-02 4.4E-06  3.16322E-02 0.00039  1.10249E-01 0.00048  3.20718E-01 0.00035  1.34518E+00 0.00027  8.90234E+00 0.00247 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.76261E-05 0.00252  2.76084E-05 0.00252  2.97352E-05 0.03278 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.00086E-05 0.00250  2.99894E-05 0.00250  3.22963E-05 0.03271 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.96074E-03 0.02563  1.82418E-04 0.14772  1.05920E-03 0.06298  1.10927E-03 0.06804  3.25576E-03 0.03776  9.87975E-04 0.06408  3.66117E-04 0.10986 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.36453E-01 0.05491  1.24905E-02 1.2E-05  3.16208E-02 0.00091  1.10274E-01 0.00114  3.20482E-01 0.00094  1.34520E+00 0.00064  8.86106E+00 0.00479 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.91317E-03 0.02502  1.77641E-04 0.14419  1.07300E-03 0.06079  1.06398E-03 0.06670  3.23533E-03 0.03667  9.99320E-04 0.06400  3.63900E-04 0.10370 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.36381E-01 0.05278  1.24905E-02 1.2E-05  3.16198E-02 0.00091  1.10275E-01 0.00114  3.20521E-01 0.00093  1.34509E+00 0.00064  8.86640E+00 0.00482 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.53638E+02 0.02594 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.76022E-05 0.00071 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.99812E-05 0.00048 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.02192E-03 0.00470 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.54505E+02 0.00483 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.40678E-07 0.00062 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87411E-06 0.00042  2.87387E-06 0.00042  2.90985E-06 0.00481 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.27285E-05 0.00076  4.27501E-05 0.00076  3.97289E-05 0.00796 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.78611E-01 0.00031  6.78109E-01 0.00032  7.65784E-01 0.00844 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.01539E+01 0.01237 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.71204E+01 0.00044  3.59473E+01 0.00047 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.14592E+04 0.00288  2.90979E+05 0.00128  6.02820E+05 0.00101  6.49222E+05 0.00091  5.97551E+05 0.00071  6.41906E+05 0.00077  4.35368E+05 0.00054  3.85774E+05 0.00051  2.94738E+05 0.00084  2.41486E+05 0.00083  2.08223E+05 0.00086  1.87254E+05 0.00062  1.72958E+05 0.00084  1.64741E+05 0.00104  1.60330E+05 0.00111  1.38321E+05 0.00082  1.36754E+05 0.00125  1.35285E+05 0.00085  1.33339E+05 0.00079  2.59980E+05 0.00051  2.50600E+05 0.00072  1.81054E+05 0.00063  1.17195E+05 0.00088  1.35594E+05 0.00087  1.28097E+05 0.00100  1.16652E+05 0.00106  1.91998E+05 0.00076  4.38273E+04 0.00162  5.49397E+04 0.00133  4.99402E+04 0.00160  2.89411E+04 0.00141  5.03702E+04 0.00134  3.43010E+04 0.00171  2.92696E+04 0.00235  5.61069E+03 0.00413  5.53841E+03 0.00382  5.70151E+03 0.00370  5.85728E+03 0.00294  5.80118E+03 0.00307  5.70301E+03 0.00271  5.88629E+03 0.00371  5.55869E+03 0.00444  1.05016E+04 0.00253  1.67580E+04 0.00253  2.12823E+04 0.00195  5.61906E+04 0.00179  5.94337E+04 0.00163  6.52516E+04 0.00108  4.54278E+04 0.00155  3.41506E+04 0.00171  2.65352E+04 0.00178  3.15934E+04 0.00176  6.04471E+04 0.00139  8.24381E+04 0.00127  1.58998E+05 0.00096  2.45356E+05 0.00098  3.65893E+05 0.00128  2.33644E+05 0.00117  1.67147E+05 0.00115  1.19962E+05 0.00122  1.07844E+05 0.00145  1.05959E+05 0.00149  8.83347E+04 0.00132  5.97548E+04 0.00133  5.51443E+04 0.00161  4.90315E+04 0.00139  4.14484E+04 0.00149  3.25980E+04 0.00134  2.18527E+04 0.00132  7.71078E+03 0.00162 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.09541E+00 0.00046 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.17407E+18 0.00045  3.56065E+17 0.00111 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38251E-01 9.8E-05  1.50597E+00 0.00036 ];
INF_CAPT                  (idx, [1:   4]) = [  6.31868E-03 0.00058  2.78544E-02 0.00036 ];
INF_ABS                   (idx, [1:   4]) = [  8.50222E-03 0.00047  5.96040E-02 0.00076 ];
INF_FISS                  (idx, [1:   4]) = [  2.18353E-03 0.00067  3.17496E-02 0.00111 ];
INF_NSF                   (idx, [1:   4]) = [  5.60164E-03 0.00068  7.73930E-02 0.00111 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.56540E+00 6.1E-05  2.43760E+00 3.9E-07 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03882E+02 6.6E-06  2.02282E+02 6.1E-08 ];
INF_INVV                  (idx, [1:   4]) = [  5.92278E-08 0.00042  2.55803E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29747E-01 0.00010  1.44638E+00 0.00040 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44471E-01 0.00022  3.82131E-01 0.00049 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63874E-02 0.00034  9.07644E-02 0.00084 ];
INF_SCATT3                (idx, [1:   4]) = [  7.33802E-03 0.00257  2.74011E-02 0.00240 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03580E-02 0.00159 -8.29518E-03 0.00606 ];
INF_SCATT5                (idx, [1:   4]) = [  1.19304E-04 0.15521  6.36980E-03 0.00761 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08038E-03 0.00326 -1.63696E-02 0.00276 ];
INF_SCATT7                (idx, [1:   4]) = [  7.56984E-04 0.01543  3.08176E-04 0.18823 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29785E-01 0.00010  1.44638E+00 0.00040 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44471E-01 0.00022  3.82131E-01 0.00049 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63873E-02 0.00034  9.07644E-02 0.00084 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.33790E-03 0.00257  2.74011E-02 0.00240 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03578E-02 0.00159 -8.29518E-03 0.00606 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.19211E-04 0.15545  6.36980E-03 0.00761 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08020E-03 0.00327 -1.63696E-02 0.00276 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.57189E-04 0.01539  3.08176E-04 0.18823 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13594E-01 0.00023  9.70710E-01 0.00032 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56060E+00 0.00023  3.43392E-01 0.00032 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.46360E-03 0.00048  5.96040E-02 0.00076 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69814E-02 0.00019  6.06642E-02 0.00089 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11270E-01 9.9E-05  1.84770E-02 0.00031  1.06556E-03 0.00483  1.44531E+00 0.00040 ];
INF_S1                    (idx, [1:   8]) = [  2.39071E-01 0.00022  5.39966E-03 0.00091  4.51278E-04 0.00570  3.81679E-01 0.00049 ];
INF_S2                    (idx, [1:   8]) = [  9.79773E-02 0.00033 -1.58986E-03 0.00263  2.49203E-04 0.00807  9.05152E-02 0.00084 ];
INF_S3                    (idx, [1:   8]) = [  9.23708E-03 0.00221 -1.89906E-03 0.00169  8.62639E-05 0.02259  2.73148E-02 0.00242 ];
INF_S4                    (idx, [1:   8]) = [ -9.72045E-03 0.00178 -6.37510E-04 0.00449 -2.89029E-06 0.58075 -8.29229E-03 0.00607 ];
INF_S5                    (idx, [1:   8]) = [  1.03921E-04 0.17169  1.53829E-05 0.17753 -3.87709E-05 0.03320  6.40857E-03 0.00758 ];
INF_S6                    (idx, [1:   8]) = [  5.22216E-03 0.00295 -1.41786E-04 0.01689 -4.78989E-05 0.03611 -1.63217E-02 0.00280 ];
INF_S7                    (idx, [1:   8]) = [  9.26568E-04 0.01194 -1.69584E-04 0.01216 -4.21927E-05 0.03361  3.50368E-04 0.16602 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11308E-01 9.9E-05  1.84770E-02 0.00031  1.06556E-03 0.00483  1.44531E+00 0.00040 ];
INF_SP1                   (idx, [1:   8]) = [  2.39072E-01 0.00022  5.39966E-03 0.00091  4.51278E-04 0.00570  3.81679E-01 0.00049 ];
INF_SP2                   (idx, [1:   8]) = [  9.79772E-02 0.00033 -1.58986E-03 0.00263  2.49203E-04 0.00807  9.05152E-02 0.00084 ];
INF_SP3                   (idx, [1:   8]) = [  9.23696E-03 0.00221 -1.89906E-03 0.00169  8.62639E-05 0.02259  2.73148E-02 0.00242 ];
INF_SP4                   (idx, [1:   8]) = [ -9.72026E-03 0.00178 -6.37510E-04 0.00449 -2.89029E-06 0.58075 -8.29229E-03 0.00607 ];
INF_SP5                   (idx, [1:   8]) = [  1.03829E-04 0.17196  1.53829E-05 0.17753 -3.87709E-05 0.03320  6.40857E-03 0.00758 ];
INF_SP6                   (idx, [1:   8]) = [  5.22198E-03 0.00295 -1.41786E-04 0.01689 -4.78989E-05 0.03611 -1.63217E-02 0.00280 ];
INF_SP7                   (idx, [1:   8]) = [  9.26773E-04 0.01190 -1.69584E-04 0.01216 -4.21927E-05 0.03361  3.50368E-04 0.16602 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32651E-01 0.00041  1.08025E+00 0.00518 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34487E-01 0.00083  1.14717E+00 0.00705 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34447E-01 0.00080  1.15479E+00 0.00649 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29113E-01 0.00102  9.63091E-01 0.00642 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43277E+00 0.00041  3.08771E-01 0.00523 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42157E+00 0.00083  2.90918E-01 0.00708 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42181E+00 0.00080  2.88943E-01 0.00645 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45492E+00 0.00102  3.46451E-01 0.00643 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.13688E-03 0.00816  1.92599E-04 0.05005  1.14541E-03 0.01931  1.13953E-03 0.02027  3.25514E-03 0.01131  1.05802E-03 0.02057  3.46181E-04 0.03736 ];
LAMBDA                    (idx, [1:  14]) = [  7.95706E-01 0.01901  1.24908E-02 3.5E-06  3.16541E-02 0.00031  1.10227E-01 0.00039  3.20586E-01 0.00032  1.34512E+00 0.00023  8.91496E+00 0.00211 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0003' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:41 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Mon Jul 20 23:57:05 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284841 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.93268E-01  1.00413E+00  1.00024E+00  1.00376E+00  9.98607E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.1E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13018E-02 0.00106  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88698E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.97954E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.98377E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.71516E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.69650E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.69565E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.33252E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.39847E-01 0.00108  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000790 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00016E+04 0.00074 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00016E+04 0.00074 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  8.02253E+01 ;
RUNNING_TIME              (idx, 1)        =  1.64019E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.31417E-01  4.31417E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.97000E-02  7.28333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.58286E+01  3.46640E+00  2.70253E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.10983E-01  2.73000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  6.66667E-03  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.64018E+01  1.28764E+02 ];
CPU_USAGE                 (idx, 1)        = 4.89122 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00015E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.59949E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.77354E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.83928E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.44297E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.62984E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.13902E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.14367E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.72535E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.69285E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.02550E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  9.00911E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.65047E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  1.79193E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  2.36046E+07 ;
SR90_ACTIVITY             (idx, 1)        =  6.37420E+11 ;
TE132_ACTIVITY            (idx, 1)        =  5.63505E+14 ;
I131_ACTIVITY             (idx, 1)        =  2.65398E+14 ;
I132_ACTIVITY             (idx, 1)        =  5.66450E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.81344E+09 ;
CS137_ACTIVITY            (idx, 1)        =  6.73823E+11 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.70522E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.63970E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  6.87309E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.12776E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.17439E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 2 ;
BURNUP                     (idx, [1:  2])  = [  5.00000E-01  5.00045E-01 ];
BURN_DAYS                 (idx, 1)        =  1.25000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.23804E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  1.26684E+16 0.00059  9.14164E-01 0.00021 ];
U238_FISS                 (idx, [1:   4]) = [  8.75499E+14 0.00274  6.31661E-02 0.00255 ];
PU239_FISS                (idx, [1:   4]) = [  3.11614E+14 0.00451  2.24841E-02 0.00444 ];
PU240_FISS                (idx, [1:   4]) = [  3.81853E+10 0.40624  2.74518E-06 0.40624 ];
PU241_FISS                (idx, [1:   4]) = [  1.00657E+11 0.26118  7.34217E-06 0.26184 ];
U235_CAPT                 (idx, [1:   4]) = [  2.71577E+15 0.00143  1.53772E-01 0.00136 ];
U238_CAPT                 (idx, [1:   4]) = [  8.24778E+15 0.00102  4.66955E-01 0.00066 ];
PU239_CAPT                (idx, [1:   4]) = [  1.75471E+14 0.00597  9.93374E-03 0.00590 ];
PU240_CAPT                (idx, [1:   4]) = [  7.70355E+12 0.02709  4.35949E-04 0.02699 ];
PU241_CAPT                (idx, [1:   4]) = [  5.05318E+10 0.35114  2.87592E-06 0.35112 ];
XE135_CAPT                (idx, [1:   4]) = [  7.26409E+14 0.00282  4.11338E-02 0.00284 ];
SM149_CAPT                (idx, [1:   4]) = [  1.26475E+14 0.00670  7.16170E-03 0.00672 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000790 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.25150E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000790 5.00725E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2778300 2.78196E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2180100 2.18289E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 42390 4.24012E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000790 5.00725E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.00469E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50058E+05 5.4E-09  4.50058E+05 5.4E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.42367E+16 1.2E-05  3.42367E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38583E+16 1.4E-06  1.38583E+16 1.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.76770E+16 0.00045  1.22155E+16 0.00048  5.46154E+15 0.00103 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.15353E+16 0.00025  2.60738E+16 0.00022  5.46154E+15 0.00103 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.17439E+16 0.00051  3.17439E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.54238E+18 0.00046  4.14103E+17 0.00046  1.12828E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.69218E+14 0.00503 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.18045E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.17469E+18 0.00058 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12456E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12456E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.67381E+00 0.00043 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.70192E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.80742E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23959E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94498E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97005E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.08778E+00 0.00051 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07856E+00 0.00052 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.47048E+00 1.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02697E+02 1.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07833E+00 0.00053  1.07112E+00 0.00052  7.43120E-03 0.00808 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07808E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07867E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07808E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.08730E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75347E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75325E+01 8.4E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.86410E-07 0.00330 ];
IMP_EALF                  (idx, [1:   2]) = [  4.86391E-07 0.00147 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.13900E-01 0.00289 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.13995E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.51002E-03 0.00575  1.78036E-04 0.03230  1.02118E-03 0.01404  1.02438E-03 0.01378  3.00199E-03 0.00789  9.66673E-04 0.01375  3.17772E-04 0.02436 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.05570E-01 0.01257  1.07920E-02 0.01776  3.16138E-02 0.00026  1.10182E-01 0.00029  3.20640E-01 0.00022  1.34552E+00 0.00017  8.58936E+00 0.00857 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.87619E-03 0.00828  1.95118E-04 0.04847  1.07196E-03 0.01990  1.09699E-03 0.02099  3.17702E-03 0.01159  1.00364E-03 0.02045  3.31462E-04 0.03578 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.95575E-01 0.01790  1.24907E-02 3.6E-06  3.16081E-02 0.00037  1.10162E-01 0.00040  3.20567E-01 0.00031  1.34598E+00 0.00023  8.86684E+00 0.00195 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.73874E-05 0.00119  2.73761E-05 0.00120  2.90097E-05 0.01104 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.95283E-05 0.00106  2.95161E-05 0.00106  3.12813E-05 0.01103 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.88969E-03 0.00823  1.97742E-04 0.04772  1.05797E-03 0.02069  1.09582E-03 0.02151  3.15955E-03 0.01167  1.03023E-03 0.02061  3.48378E-04 0.03555 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.19242E-01 0.01889  1.24908E-02 4.0E-06  3.16142E-02 0.00042  1.10161E-01 0.00044  3.20765E-01 0.00035  1.34552E+00 0.00028  8.89222E+00 0.00250 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.72273E-05 0.00257  2.72165E-05 0.00259  2.85233E-05 0.02741 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.93558E-05 0.00251  2.93441E-05 0.00253  3.07642E-05 0.02744 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.97513E-03 0.02653  2.00070E-04 0.14339  1.01011E-03 0.07028  1.08418E-03 0.06558  3.24900E-03 0.03723  1.07748E-03 0.07055  3.54289E-04 0.11851 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.24386E-01 0.05963  1.24908E-02 9.0E-06  3.16144E-02 0.00097  1.10051E-01 0.00102  3.20897E-01 0.00103  1.34521E+00 0.00063  8.86848E+00 0.00514 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.02399E-03 0.02559  1.92924E-04 0.13818  1.03343E-03 0.06732  1.08703E-03 0.06496  3.27167E-03 0.03601  1.07759E-03 0.06917  3.61354E-04 0.11629 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.20779E-01 0.05833  1.24908E-02 9.0E-06  3.16052E-02 0.00098  1.10076E-01 0.00102  3.20880E-01 0.00102  1.34525E+00 0.00063  8.86352E+00 0.00508 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.56772E+02 0.02659 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.73255E-05 0.00073 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.94620E-05 0.00053 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.95014E-03 0.00534 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.54409E+02 0.00538 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.34950E-07 0.00065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87575E-06 0.00044  2.87561E-06 0.00043  2.89616E-06 0.00499 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.22424E-05 0.00079  4.22653E-05 0.00080  3.90707E-05 0.00881 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.78362E-01 0.00032  6.77911E-01 0.00033  7.60182E-01 0.00911 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.00801E+01 0.01276 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.69565E+01 0.00046  3.57377E+01 0.00048 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.24101E+04 0.00272  2.91171E+05 0.00131  6.02467E+05 0.00086  6.49117E+05 0.00073  5.99005E+05 0.00066  6.42643E+05 0.00063  4.36429E+05 0.00081  3.86233E+05 0.00066  2.95426E+05 0.00063  2.41362E+05 0.00096  2.08376E+05 0.00071  1.87789E+05 0.00055  1.72989E+05 0.00102  1.64406E+05 0.00091  1.60512E+05 0.00070  1.38743E+05 0.00099  1.36817E+05 0.00094  1.35584E+05 0.00077  1.33116E+05 0.00109  2.59925E+05 0.00069  2.50802E+05 0.00073  1.81163E+05 0.00084  1.17588E+05 0.00091  1.35635E+05 0.00105  1.28115E+05 0.00107  1.16639E+05 0.00118  1.91847E+05 0.00059  4.38487E+04 0.00151  5.49909E+04 0.00129  4.98736E+04 0.00135  2.89708E+04 0.00138  5.03271E+04 0.00135  3.42125E+04 0.00154  2.92645E+04 0.00140  5.56780E+03 0.00408  5.53643E+03 0.00421  5.65867E+03 0.00386  5.85389E+03 0.00343  5.76821E+03 0.00341  5.69213E+03 0.00400  5.91763E+03 0.00364  5.60897E+03 0.00345  1.05299E+04 0.00261  1.67877E+04 0.00278  2.13275E+04 0.00145  5.62303E+04 0.00144  5.95254E+04 0.00169  6.53986E+04 0.00157  4.50211E+04 0.00102  3.38511E+04 0.00187  2.61413E+04 0.00161  3.10993E+04 0.00175  5.95592E+04 0.00111  8.13006E+04 0.00134  1.56778E+05 0.00112  2.42182E+05 0.00123  3.61222E+05 0.00134  2.30677E+05 0.00150  1.65076E+05 0.00155  1.18699E+05 0.00174  1.06407E+05 0.00165  1.04773E+05 0.00166  8.72840E+04 0.00192  5.90623E+04 0.00182  5.45200E+04 0.00189  4.84976E+04 0.00180  4.10295E+04 0.00177  3.22515E+04 0.00177  2.16128E+04 0.00169  7.63691E+03 0.00242 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.08789E+00 0.00047 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.18695E+18 0.00051  3.55468E+17 0.00126 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38185E-01 0.00011  1.50765E+00 0.00051 ];
INF_CAPT                  (idx, [1:   4]) = [  6.34253E-03 0.00062  2.85532E-02 0.00042 ];
INF_ABS                   (idx, [1:   4]) = [  8.51191E-03 0.00051  6.03016E-02 0.00087 ];
INF_FISS                  (idx, [1:   4]) = [  2.16938E-03 0.00052  3.17484E-02 0.00127 ];
INF_NSF                   (idx, [1:   4]) = [  5.57372E-03 0.00053  7.77179E-02 0.00128 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.56927E+00 5.8E-05  2.44793E+00 5.8E-06 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03927E+02 6.2E-06  2.02416E+02 9.1E-07 ];
INF_INVV                  (idx, [1:   4]) = [  5.91809E-08 0.00038  2.55761E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29680E-01 0.00011  1.44737E+00 0.00057 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44394E-01 0.00020  3.82614E-01 0.00064 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63553E-02 0.00028  9.08797E-02 0.00085 ];
INF_SCATT3                (idx, [1:   4]) = [  7.40550E-03 0.00396  2.73298E-02 0.00214 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03142E-02 0.00264 -8.40145E-03 0.00573 ];
INF_SCATT5                (idx, [1:   4]) = [  1.50907E-04 0.14508  6.30055E-03 0.00664 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10153E-03 0.00293 -1.63112E-02 0.00267 ];
INF_SCATT7                (idx, [1:   4]) = [  7.72605E-04 0.01860  3.76124E-04 0.09446 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29719E-01 0.00011  1.44737E+00 0.00057 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44394E-01 0.00020  3.82614E-01 0.00064 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63557E-02 0.00028  9.08797E-02 0.00085 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.40551E-03 0.00395  2.73298E-02 0.00214 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03144E-02 0.00264 -8.40145E-03 0.00573 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.51061E-04 0.14481  6.30055E-03 0.00664 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10175E-03 0.00293 -1.63112E-02 0.00267 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.72581E-04 0.01859  3.76124E-04 0.09446 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13613E-01 0.00030  9.71918E-01 0.00042 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56045E+00 0.00030  3.42966E-01 0.00042 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.47313E-03 0.00052  6.03016E-02 0.00087 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69587E-02 0.00026  6.13557E-02 0.00109 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11226E-01 0.00011  1.84540E-02 0.00050  1.07807E-03 0.00442  1.44629E+00 0.00057 ];
INF_S1                    (idx, [1:   8]) = [  2.39001E-01 0.00019  5.39276E-03 0.00104  4.62445E-04 0.00737  3.82151E-01 0.00064 ];
INF_S2                    (idx, [1:   8]) = [  9.79480E-02 0.00028 -1.59262E-03 0.00248  2.55358E-04 0.00833  9.06243E-02 0.00086 ];
INF_S3                    (idx, [1:   8]) = [  9.30217E-03 0.00312 -1.89667E-03 0.00142  9.19057E-05 0.01738  2.72379E-02 0.00214 ];
INF_S4                    (idx, [1:   8]) = [ -9.68283E-03 0.00283 -6.31376E-04 0.00458  1.20724E-06 1.00000 -8.40266E-03 0.00577 ];
INF_S5                    (idx, [1:   8]) = [  1.33286E-04 0.16046  1.76213E-05 0.13255 -3.71437E-05 0.03841  6.33770E-03 0.00660 ];
INF_S6                    (idx, [1:   8]) = [  5.24453E-03 0.00274 -1.43005E-04 0.01482 -4.82342E-05 0.02634 -1.62629E-02 0.00268 ];
INF_S7                    (idx, [1:   8]) = [  9.44107E-04 0.01468 -1.71502E-04 0.01400 -4.27569E-05 0.02580  4.18881E-04 0.08475 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11265E-01 0.00011  1.84540E-02 0.00050  1.07807E-03 0.00442  1.44629E+00 0.00057 ];
INF_SP1                   (idx, [1:   8]) = [  2.39001E-01 0.00019  5.39276E-03 0.00104  4.62445E-04 0.00737  3.82151E-01 0.00064 ];
INF_SP2                   (idx, [1:   8]) = [  9.79483E-02 0.00028 -1.59262E-03 0.00248  2.55358E-04 0.00833  9.06243E-02 0.00086 ];
INF_SP3                   (idx, [1:   8]) = [  9.30218E-03 0.00312 -1.89667E-03 0.00142  9.19057E-05 0.01738  2.72379E-02 0.00214 ];
INF_SP4                   (idx, [1:   8]) = [ -9.68302E-03 0.00282 -6.31376E-04 0.00458  1.20724E-06 1.00000 -8.40266E-03 0.00577 ];
INF_SP5                   (idx, [1:   8]) = [  1.33440E-04 0.16016  1.76213E-05 0.13255 -3.71437E-05 0.03841  6.33770E-03 0.00660 ];
INF_SP6                   (idx, [1:   8]) = [  5.24476E-03 0.00274 -1.43005E-04 0.01482 -4.82342E-05 0.02634 -1.62629E-02 0.00268 ];
INF_SP7                   (idx, [1:   8]) = [  9.44084E-04 0.01468 -1.71502E-04 0.01400 -4.27569E-05 0.02580  4.18881E-04 0.08475 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32546E-01 0.00066  1.07736E+00 0.00514 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34232E-01 0.00090  1.14712E+00 0.00799 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34096E-01 0.00079  1.13883E+00 0.00613 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29383E-01 0.00093  9.67267E-01 0.00453 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43342E+00 0.00066  3.09595E-01 0.00515 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42312E+00 0.00090  2.91039E-01 0.00820 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42394E+00 0.00079  2.92964E-01 0.00618 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45320E+00 0.00093  3.44782E-01 0.00449 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.87619E-03 0.00828  1.95118E-04 0.04847  1.07196E-03 0.01990  1.09699E-03 0.02099  3.17702E-03 0.01159  1.00364E-03 0.02045  3.31462E-04 0.03578 ];
LAMBDA                    (idx, [1:  14]) = [  7.95575E-01 0.01790  1.24907E-02 3.6E-06  3.16081E-02 0.00037  1.10162E-01 0.00040  3.20567E-01 0.00031  1.34598E+00 0.00023  8.86684E+00 0.00195 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0003' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:41 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:03:18 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284841 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.92830E-01  1.00344E+00  1.00085E+00  1.00408E+00  9.98804E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12957E-02 0.00102  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88704E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.98801E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.99224E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.70998E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.67884E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.67799E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.23918E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.36480E-01 0.00108  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000496 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00010E+04 0.00076 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00010E+04 0.00076 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.11298E+02 ;
RUNNING_TIME              (idx, 1)        =  2.26218E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.31417E-01  4.31417E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  4.46000E-02  7.06666E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  2.19805E+01  3.45313E+00  2.69875E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.63783E-01  2.60500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  9.91667E-03  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  2.26217E+01  1.28555E+02 ];
CPU_USAGE                 (idx, 1)        = 4.91994 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99947E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.67016E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.98278E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.85539E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.49896E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.66072E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.15970E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.32204E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.73939E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  3.56621E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.50250E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.03755E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.86570E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  2.52867E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  2.81593E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.26274E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.04901E+14 ;
I131_ACTIVITY             (idx, 1)        =  3.60209E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.10197E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.68185E+10 ;
CS137_ACTIVITY            (idx, 1)        =  1.34860E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.90558E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.61945E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.53024E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.16715E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.20021E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 3 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E+00  1.00009E+00 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.22616E-01 0.00105 ];
U235_FISS                 (idx, [1:   4]) = [  1.22443E+16 0.00061  8.85324E-01 0.00023 ];
U238_FISS                 (idx, [1:   4]) = [  8.87405E+14 0.00269  6.41503E-02 0.00247 ];
PU239_FISS                (idx, [1:   4]) = [  6.94754E+14 0.00296  5.02344E-02 0.00291 ];
PU240_FISS                (idx, [1:   4]) = [  8.96843E+10 0.26381  6.49457E-06 0.26378 ];
PU241_FISS                (idx, [1:   4]) = [  1.18181E+12 0.07312  8.54528E-05 0.07303 ];
U235_CAPT                 (idx, [1:   4]) = [  2.65094E+15 0.00152  1.47728E-01 0.00146 ];
U238_CAPT                 (idx, [1:   4]) = [  8.26120E+15 0.00097  4.60331E-01 0.00066 ];
PU239_CAPT                (idx, [1:   4]) = [  3.90640E+14 0.00394  2.17695E-02 0.00393 ];
PU240_CAPT                (idx, [1:   4]) = [  3.36183E+13 0.01438  1.87295E-03 0.01434 ];
PU241_CAPT                (idx, [1:   4]) = [  5.04153E+11 0.10936  2.80574E-05 0.10942 ];
XE135_CAPT                (idx, [1:   4]) = [  7.26979E+14 0.00280  4.05128E-02 0.00278 ];
SM149_CAPT                (idx, [1:   4]) = [  1.48412E+14 0.00659  8.26906E-03 0.00653 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000496 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.13591E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000496 5.00714E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2800006 2.80384E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2158157 2.16096E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 42333 4.23391E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000496 5.00714E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.96046E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50058E+05 5.4E-09  4.50058E+05 5.4E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.43793E+16 1.2E-05  3.43793E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38475E+16 1.5E-06  1.38475E+16 1.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.79396E+16 0.00045  1.24846E+16 0.00044  5.45497E+15 0.00096 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.17870E+16 0.00025  2.63321E+16 0.00021  5.45497E+15 0.00096 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.20021E+16 0.00050  3.20021E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.55026E+18 0.00046  4.15747E+17 0.00043  1.13451E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.71047E+14 0.00515 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.20581E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.17854E+18 0.00057 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12398E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12398E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.66386E+00 0.00046 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.71750E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.79876E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23968E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94531E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96984E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.08220E+00 0.00053 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07303E+00 0.00053 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.48271E+00 1.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02856E+02 1.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07305E+00 0.00054  1.06578E+00 0.00053  7.25340E-03 0.00826 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07401E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07441E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07401E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.08318E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75032E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75069E+01 7.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.01978E-07 0.00332 ];
IMP_EALF                  (idx, [1:   2]) = [  4.98985E-07 0.00138 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.16602E-01 0.00259 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.16195E-01 0.00117 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.41826E-03 0.00551  1.86055E-04 0.03385  1.03293E-03 0.01344  9.82441E-04 0.01462  2.94622E-03 0.00809  9.50650E-04 0.01379  3.19959E-04 0.02398 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.11305E-01 0.01260  1.02922E-02 0.02069  3.15925E-02 0.00026  1.10166E-01 0.00030  3.20923E-01 0.00023  1.34490E+00 0.00021  8.71825E+00 0.00627 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.80323E-03 0.00813  2.07601E-04 0.04940  1.10087E-03 0.02108  1.02580E-03 0.02118  3.10930E-03 0.01216  1.01211E-03 0.02161  3.47541E-04 0.03480 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.23764E-01 0.01851  1.24905E-02 6.2E-06  3.15896E-02 0.00037  1.10188E-01 0.00043  3.21068E-01 0.00034  1.34515E+00 0.00023  8.86458E+00 0.00188 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.71662E-05 0.00114  2.71531E-05 0.00114  2.90400E-05 0.01141 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.91469E-05 0.00103  2.91328E-05 0.00103  3.11565E-05 0.01138 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.75384E-03 0.00844  1.89918E-04 0.05119  1.12617E-03 0.02010  1.04481E-03 0.02144  3.07189E-03 0.01293  9.77751E-04 0.02178  3.43301E-04 0.03681 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.20873E-01 0.01986  1.24905E-02 8.0E-06  3.16090E-02 0.00040  1.10173E-01 0.00047  3.20915E-01 0.00038  1.34482E+00 0.00030  8.88444E+00 0.00244 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.70008E-05 0.00259  2.69902E-05 0.00258  2.90492E-05 0.02955 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.89693E-05 0.00254  2.89580E-05 0.00254  3.11612E-05 0.02948 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.77019E-03 0.02822  1.67189E-04 0.15808  1.12998E-03 0.06391  9.59932E-04 0.06993  3.13579E-03 0.04096  1.01843E-03 0.06620  3.58873E-04 0.12732 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.89060E-01 0.05939  1.24903E-02 2.4E-05  3.16168E-02 0.00096  1.10245E-01 0.00122  3.21350E-01 0.00113  1.34390E+00 0.00068  8.86631E+00 0.00541 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.76247E-03 0.02760  1.67407E-04 0.15117  1.12622E-03 0.06210  9.77541E-04 0.06823  3.11482E-03 0.04063  1.01636E-03 0.06392  3.60125E-04 0.12546 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.89955E-01 0.05784  1.24903E-02 2.4E-05  3.16164E-02 0.00095  1.10272E-01 0.00123  3.21413E-01 0.00112  1.34403E+00 0.00067  8.86754E+00 0.00543 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.51204E+02 0.02810 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.70924E-05 0.00073 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.90674E-05 0.00052 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.69059E-03 0.00461 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.47011E+02 0.00466 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.29384E-07 0.00060 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87310E-06 0.00043  2.87285E-06 0.00043  2.90748E-06 0.00487 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.17664E-05 0.00072  4.17853E-05 0.00073  3.91418E-05 0.00894 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.77531E-01 0.00032  6.77132E-01 0.00032  7.51297E-01 0.00889 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03267E+01 0.01396 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.67799E+01 0.00043  3.55007E+01 0.00046 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.28773E+04 0.00356  2.92891E+05 0.00133  6.04730E+05 0.00083  6.49255E+05 0.00055  5.97909E+05 0.00069  6.42303E+05 0.00083  4.36357E+05 0.00073  3.86006E+05 0.00063  2.95066E+05 0.00085  2.41033E+05 0.00091  2.07983E+05 0.00095  1.87541E+05 0.00054  1.73065E+05 0.00078  1.64500E+05 0.00090  1.60196E+05 0.00084  1.38148E+05 0.00081  1.36587E+05 0.00104  1.35550E+05 0.00075  1.33020E+05 0.00103  2.59754E+05 0.00064  2.50683E+05 0.00067  1.81055E+05 0.00087  1.17525E+05 0.00110  1.35656E+05 0.00087  1.28346E+05 0.00107  1.16651E+05 0.00089  1.91572E+05 0.00074  4.39020E+04 0.00185  5.49455E+04 0.00177  4.99443E+04 0.00163  2.89314E+04 0.00241  5.05397E+04 0.00174  3.42830E+04 0.00173  2.91836E+04 0.00193  5.61040E+03 0.00367  5.55507E+03 0.00335  5.61783E+03 0.00286  5.77427E+03 0.00385  5.75587E+03 0.00302  5.65199E+03 0.00421  5.88691E+03 0.00323  5.57588E+03 0.00356  1.04793E+04 0.00293  1.67524E+04 0.00209  2.13722E+04 0.00177  5.61425E+04 0.00135  5.92732E+04 0.00193  6.49654E+04 0.00119  4.44616E+04 0.00163  3.32679E+04 0.00124  2.56045E+04 0.00144  3.03794E+04 0.00181  5.83802E+04 0.00108  7.98722E+04 0.00122  1.54528E+05 0.00088  2.38789E+05 0.00099  3.56585E+05 0.00084  2.27675E+05 0.00098  1.63041E+05 0.00115  1.17053E+05 0.00082  1.05225E+05 0.00104  1.03563E+05 0.00119  8.63855E+04 0.00120  5.83329E+04 0.00135  5.39259E+04 0.00130  4.78504E+04 0.00139  4.05791E+04 0.00139  3.18705E+04 0.00162  2.13599E+04 0.00118  7.54949E+03 0.00213 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.08359E+00 0.00047 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.19658E+18 0.00047  3.53706E+17 0.00088 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38099E-01 8.9E-05  1.51169E+00 0.00030 ];
INF_CAPT                  (idx, [1:   4]) = [  6.37658E-03 0.00051  2.91495E-02 0.00034 ];
INF_ABS                   (idx, [1:   4]) = [  8.53197E-03 0.00044  6.10127E-02 0.00063 ];
INF_FISS                  (idx, [1:   4]) = [  2.15539E-03 0.00042  3.18632E-02 0.00091 ];
INF_NSF                   (idx, [1:   4]) = [  5.54813E-03 0.00041  7.84408E-02 0.00091 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.57407E+00 6.3E-05  2.46180E+00 9.7E-06 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03986E+02 5.7E-06  2.02597E+02 1.5E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.91547E-08 0.00038  2.55856E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29572E-01 9.2E-05  1.45069E+00 0.00033 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44388E-01 0.00017  3.83494E-01 0.00041 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63663E-02 0.00030  9.10514E-02 0.00096 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34508E-03 0.00208  2.75569E-02 0.00191 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03326E-02 0.00186 -8.36283E-03 0.00651 ];
INF_SCATT5                (idx, [1:   4]) = [  1.09690E-04 0.19338  6.40891E-03 0.00885 ];
INF_SCATT6                (idx, [1:   4]) = [  5.04970E-03 0.00310 -1.63731E-02 0.00277 ];
INF_SCATT7                (idx, [1:   4]) = [  7.48947E-04 0.01426  3.45641E-04 0.13317 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29610E-01 9.2E-05  1.45069E+00 0.00033 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44389E-01 0.00017  3.83494E-01 0.00041 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63664E-02 0.00030  9.10514E-02 0.00096 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34478E-03 0.00208  2.75569E-02 0.00191 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03326E-02 0.00187 -8.36283E-03 0.00651 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.09900E-04 0.19283  6.40891E-03 0.00885 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.04978E-03 0.00309 -1.63731E-02 0.00277 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.48802E-04 0.01424  3.45641E-04 0.13317 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13418E-01 0.00024  9.75171E-01 0.00031 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56188E+00 0.00024  3.41821E-01 0.00031 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.49381E-03 0.00044  6.10127E-02 0.00063 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69579E-02 0.00016  6.20802E-02 0.00064 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11141E-01 9.0E-05  1.84307E-02 0.00034  1.08755E-03 0.00371  1.44961E+00 0.00033 ];
INF_S1                    (idx, [1:   8]) = [  2.38999E-01 0.00018  5.38964E-03 0.00072  4.65709E-04 0.00774  3.83028E-01 0.00041 ];
INF_S2                    (idx, [1:   8]) = [  9.79547E-02 0.00029 -1.58842E-03 0.00252  2.50775E-04 0.00982  9.08007E-02 0.00097 ];
INF_S3                    (idx, [1:   8]) = [  9.23946E-03 0.00164 -1.89438E-03 0.00163  8.87359E-05 0.02204  2.74682E-02 0.00192 ];
INF_S4                    (idx, [1:   8]) = [ -9.70401E-03 0.00189 -6.28615E-04 0.00504  2.68216E-06 0.54500 -8.36552E-03 0.00644 ];
INF_S5                    (idx, [1:   8]) = [  9.66411E-05 0.21106  1.30484E-05 0.24603 -3.57992E-05 0.04729  6.44471E-03 0.00887 ];
INF_S6                    (idx, [1:   8]) = [  5.19997E-03 0.00312 -1.50265E-04 0.01287 -4.65051E-05 0.03224 -1.63266E-02 0.00276 ];
INF_S7                    (idx, [1:   8]) = [  9.23400E-04 0.01143 -1.74452E-04 0.01313 -4.24160E-05 0.03107  3.88057E-04 0.11861 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11179E-01 9.0E-05  1.84307E-02 0.00034  1.08755E-03 0.00371  1.44961E+00 0.00033 ];
INF_SP1                   (idx, [1:   8]) = [  2.38999E-01 0.00018  5.38964E-03 0.00072  4.65709E-04 0.00774  3.83028E-01 0.00041 ];
INF_SP2                   (idx, [1:   8]) = [  9.79548E-02 0.00029 -1.58842E-03 0.00252  2.50775E-04 0.00982  9.08007E-02 0.00097 ];
INF_SP3                   (idx, [1:   8]) = [  9.23916E-03 0.00164 -1.89438E-03 0.00163  8.87359E-05 0.02204  2.74682E-02 0.00192 ];
INF_SP4                   (idx, [1:   8]) = [ -9.70401E-03 0.00190 -6.28615E-04 0.00504  2.68216E-06 0.54500 -8.36552E-03 0.00644 ];
INF_SP5                   (idx, [1:   8]) = [  9.68513E-05 0.21038  1.30484E-05 0.24603 -3.57992E-05 0.04729  6.44471E-03 0.00887 ];
INF_SP6                   (idx, [1:   8]) = [  5.20004E-03 0.00311 -1.50265E-04 0.01287 -4.65051E-05 0.03224 -1.63266E-02 0.00276 ];
INF_SP7                   (idx, [1:   8]) = [  9.23254E-04 0.01142 -1.74452E-04 0.01313 -4.24160E-05 0.03107  3.88057E-04 0.11861 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32409E-01 0.00064  1.09106E+00 0.00604 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34278E-01 0.00089  1.15706E+00 0.00856 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33948E-01 0.00079  1.16737E+00 0.00725 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29081E-01 0.00103  9.73178E-01 0.00640 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43427E+00 0.00064  3.05779E-01 0.00600 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42284E+00 0.00089  2.88582E-01 0.00836 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42484E+00 0.00079  2.85899E-01 0.00716 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45513E+00 0.00103  3.42855E-01 0.00636 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.80323E-03 0.00813  2.07601E-04 0.04940  1.10087E-03 0.02108  1.02580E-03 0.02118  3.10930E-03 0.01216  1.01211E-03 0.02161  3.47541E-04 0.03480 ];
LAMBDA                    (idx, [1:  14]) = [  8.23764E-01 0.01851  1.24905E-02 6.2E-06  3.15896E-02 0.00037  1.10188E-01 0.00043  3.21068E-01 0.00034  1.34515E+00 0.00023  8.86458E+00 0.00188 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0003' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:41 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:09:31 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284841 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.93142E-01  1.00418E+00  1.00064E+00  1.00449E+00  9.97552E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12983E-02 0.00104  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88702E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.00035E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.00458E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.70437E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.65052E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.64967E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.09745E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.31939E-01 0.00106  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000671 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00013E+04 0.00074 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00013E+04 0.00074 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.42320E+02 ;
RUNNING_TIME              (idx, 1)        =  2.88321E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.31417E-01  4.31417E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  6.07333E-02  7.76666E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  2.81214E+01  3.44710E+00  2.69383E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  2.16367E-01  2.61667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.31000E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  2.88319E+01  1.28269E+02 ];
CPU_USAGE                 (idx, 1)        = 4.93617 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99976E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.70988E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.14234E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.85543E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.73637E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.66768E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.16435E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.47463E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.73897E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  4.76792E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.86353E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.31235E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.91035E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  3.45557E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.17249E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.47833E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.14196E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.08676E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.21290E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.56779E+11 ;
CS137_ACTIVITY            (idx, 1)        =  2.69957E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.02218E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.58115E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.34642E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.19313E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.25179E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 4 ;
BURNUP                     (idx, [1:  2])  = [  2.00000E+00  2.00017E+00 ];
BURN_DAYS                 (idx, 1)        =  5.00000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.24721E-01 0.00103 ];
U235_FISS                 (idx, [1:   4]) = [  1.15700E+16 0.00066  8.36087E-01 0.00032 ];
U238_FISS                 (idx, [1:   4]) = [  9.01714E+14 0.00282  6.51534E-02 0.00268 ];
PU239_FISS                (idx, [1:   4]) = [  1.35281E+15 0.00227  9.77543E-02 0.00215 ];
PU240_FISS                (idx, [1:   4]) = [  3.65489E+11 0.12858  2.63709E-05 0.12865 ];
PU241_FISS                (idx, [1:   4]) = [  9.94798E+12 0.02437  7.18893E-04 0.02438 ];
U235_CAPT                 (idx, [1:   4]) = [  2.49804E+15 0.00155  1.35398E-01 0.00148 ];
U238_CAPT                 (idx, [1:   4]) = [  8.30678E+15 0.00100  4.50208E-01 0.00068 ];
PU239_CAPT                (idx, [1:   4]) = [  7.55443E+14 0.00276  4.09462E-02 0.00271 ];
PU240_CAPT                (idx, [1:   4]) = [  1.31094E+14 0.00748  7.10562E-03 0.00746 ];
PU241_CAPT                (idx, [1:   4]) = [  3.84173E+12 0.04212  2.08022E-04 0.04210 ];
XE135_CAPT                (idx, [1:   4]) = [  7.27718E+14 0.00294  3.94445E-02 0.00291 ];
SM149_CAPT                (idx, [1:   4]) = [  1.58473E+14 0.00604  8.59043E-03 0.00605 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000671 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.38183E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000671 5.00738E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2833092 2.83696E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2125059 2.12790E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 42520 4.25217E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000671 5.00738E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -2.88710E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50058E+05 5.4E-09  4.50058E+05 5.4E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.46273E+16 1.3E-05  3.46273E+16 1.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38285E+16 1.9E-06  1.38285E+16 1.9E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.84635E+16 0.00048  1.30025E+16 0.00046  5.46101E+15 0.00112 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.22920E+16 0.00027  2.68310E+16 0.00022  5.46101E+15 0.00112 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.25179E+16 0.00050  3.25179E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.56777E+18 0.00049  4.20121E+17 0.00044  1.14765E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.76571E+14 0.00499 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.25685E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.18841E+18 0.00064 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12281E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12281E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.65437E+00 0.00043 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.74578E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.77296E-01 0.00030 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23844E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94495E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96985E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.07480E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06566E+00 0.00052 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.50406E+00 1.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03135E+02 1.9E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06566E+00 0.00054  1.05867E+00 0.00052  6.98422E-03 0.00895 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06484E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06500E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06484E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.07397E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74651E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74613E+01 8.3E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.21555E-07 0.00345 ];
IMP_EALF                  (idx, [1:   2]) = [  5.22289E-07 0.00145 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.20407E-01 0.00296 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.20615E-01 0.00110 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.27645E-03 0.00598  1.77770E-04 0.03287  1.01515E-03 0.01404  1.00105E-03 0.01340  2.83561E-03 0.00870  9.32259E-04 0.01496  3.14614E-04 0.02316 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.14196E-01 0.01200  1.04419E-02 0.01983  3.15203E-02 0.00028  1.10104E-01 0.00029  3.21108E-01 0.00024  1.34408E+00 0.00024  8.70681E+00 0.00725 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.56766E-03 0.00844  1.90664E-04 0.05052  1.04725E-03 0.02039  1.03051E-03 0.01980  2.98088E-03 0.01249  9.76494E-04 0.02263  3.41867E-04 0.03413 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.31875E-01 0.01801  1.24902E-02 9.0E-06  3.15206E-02 0.00043  1.10090E-01 0.00039  3.21090E-01 0.00036  1.34395E+00 0.00032  8.92094E+00 0.00219 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.66490E-05 0.00124  2.66399E-05 0.00125  2.78413E-05 0.01167 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.83944E-05 0.00111  2.83846E-05 0.00112  2.96707E-05 0.01169 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.53398E-03 0.00917  1.91398E-04 0.05399  1.02331E-03 0.02186  1.03803E-03 0.02205  2.98636E-03 0.01301  9.71809E-04 0.02253  3.23082E-04 0.03713 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.06414E-01 0.01917  1.24903E-02 1.1E-05  3.15077E-02 0.00052  1.10195E-01 0.00054  3.21100E-01 0.00038  1.34449E+00 0.00033  8.88875E+00 0.00254 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.64841E-05 0.00267  2.64852E-05 0.00268  2.59928E-05 0.02784 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.82173E-05 0.00257  2.82184E-05 0.00258  2.76961E-05 0.02785 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.61051E-03 0.02719  1.66709E-04 0.13965  1.00770E-03 0.06812  1.10216E-03 0.06610  2.93094E-03 0.04164  1.03764E-03 0.06543  3.65370E-04 0.11701 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  9.26975E-01 0.06943  1.24905E-02 1.6E-05  3.14743E-02 0.00131  1.10163E-01 0.00117  3.20994E-01 0.00109  1.34247E+00 0.00153  8.95227E+00 0.00624 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.55815E-03 0.02666  1.72792E-04 0.13880  1.00238E-03 0.06705  1.07466E-03 0.06483  2.90808E-03 0.04099  1.03765E-03 0.06433  3.62585E-04 0.11550 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  9.27020E-01 0.06825  1.24905E-02 1.6E-05  3.14785E-02 0.00130  1.10183E-01 0.00118  3.21020E-01 0.00108  1.34246E+00 0.00152  8.95241E+00 0.00624 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.50705E+02 0.02762 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.65452E-05 0.00077 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.82837E-05 0.00052 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.49617E-03 0.00532 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.44811E+02 0.00540 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.20465E-07 0.00066 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.86128E-06 0.00042  2.86108E-06 0.00042  2.88850E-06 0.00496 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.10811E-05 0.00085  4.11070E-05 0.00085  3.72678E-05 0.00954 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.74963E-01 0.00030  6.74600E-01 0.00031  7.47031E-01 0.00949 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02097E+01 0.01289 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.64967E+01 0.00048  3.51461E+01 0.00048 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.34732E+04 0.00321  2.93438E+05 0.00161  6.04749E+05 0.00098  6.49970E+05 0.00084  5.98938E+05 0.00079  6.42289E+05 0.00054  4.36329E+05 0.00081  3.85753E+05 0.00055  2.95228E+05 0.00061  2.41123E+05 0.00079  2.07630E+05 0.00088  1.87398E+05 0.00072  1.72821E+05 0.00084  1.64199E+05 0.00059  1.60384E+05 0.00092  1.38427E+05 0.00111  1.36858E+05 0.00100  1.35555E+05 0.00091  1.33173E+05 0.00091  2.59825E+05 0.00057  2.50751E+05 0.00080  1.81153E+05 0.00083  1.17166E+05 0.00088  1.35638E+05 0.00081  1.28379E+05 0.00081  1.16387E+05 0.00073  1.91208E+05 0.00077  4.37591E+04 0.00107  5.49546E+04 0.00108  4.97848E+04 0.00126  2.90862E+04 0.00162  5.03602E+04 0.00168  3.43084E+04 0.00206  2.90851E+04 0.00206  5.53632E+03 0.00359  5.41636E+03 0.00343  5.47779E+03 0.00512  5.56492E+03 0.00321  5.53854E+03 0.00396  5.53074E+03 0.00348  5.83193E+03 0.00388  5.49713E+03 0.00348  1.04039E+04 0.00345  1.66605E+04 0.00241  2.12677E+04 0.00245  5.60649E+04 0.00138  5.89342E+04 0.00113  6.41657E+04 0.00131  4.35978E+04 0.00143  3.22747E+04 0.00163  2.47153E+04 0.00179  2.93019E+04 0.00171  5.63675E+04 0.00133  7.74432E+04 0.00099  1.50599E+05 0.00104  2.33470E+05 0.00088  3.49054E+05 0.00095  2.23455E+05 0.00095  1.59768E+05 0.00112  1.14828E+05 0.00125  1.03156E+05 0.00124  1.01587E+05 0.00104  8.46601E+04 0.00133  5.73201E+04 0.00115  5.29580E+04 0.00129  4.71355E+04 0.00139  3.97898E+04 0.00145  3.13415E+04 0.00127  2.09953E+04 0.00130  7.42178E+03 0.00194 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.07414E+00 0.00044 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.21590E+18 0.00048  3.51900E+17 0.00105 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38055E-01 0.00010  1.51807E+00 0.00037 ];
INF_CAPT                  (idx, [1:   4]) = [  6.48171E-03 0.00061  3.00748E-02 0.00049 ];
INF_ABS                   (idx, [1:   4]) = [  8.60728E-03 0.00051  6.20327E-02 0.00081 ];
INF_FISS                  (idx, [1:   4]) = [  2.12557E-03 0.00061  3.19579E-02 0.00111 ];
INF_NSF                   (idx, [1:   4]) = [  5.49092E-03 0.00060  7.94426E-02 0.00113 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.58327E+00 5.9E-05  2.48585E+00 1.9E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04102E+02 5.4E-06  2.02912E+02 3.0E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.89691E-08 0.00044  2.56073E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29458E-01 0.00011  1.45602E+00 0.00041 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44297E-01 0.00019  3.85280E-01 0.00053 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63105E-02 0.00030  9.13293E-02 0.00059 ];
INF_SCATT3                (idx, [1:   4]) = [  7.33454E-03 0.00292  2.74417E-02 0.00219 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03543E-02 0.00186 -8.53974E-03 0.00615 ];
INF_SCATT5                (idx, [1:   4]) = [  1.55612E-04 0.10336  6.31069E-03 0.00926 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07813E-03 0.00291 -1.64453E-02 0.00231 ];
INF_SCATT7                (idx, [1:   4]) = [  7.44649E-04 0.01763  3.86483E-04 0.09050 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29498E-01 0.00011  1.45602E+00 0.00041 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44298E-01 0.00019  3.85280E-01 0.00053 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63107E-02 0.00030  9.13293E-02 0.00059 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.33443E-03 0.00291  2.74417E-02 0.00219 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03545E-02 0.00187 -8.53974E-03 0.00615 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.55733E-04 0.10355  6.31069E-03 0.00926 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07778E-03 0.00291 -1.64453E-02 0.00231 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.44461E-04 0.01767  3.86483E-04 0.09050 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13347E-01 0.00034  9.79961E-01 0.00033 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56240E+00 0.00034  3.40150E-01 0.00033 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.56779E-03 0.00052  6.20327E-02 0.00081 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69593E-02 0.00015  6.31723E-02 0.00071 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11095E-01 0.00011  1.83629E-02 0.00030  1.11694E-03 0.00346  1.45490E+00 0.00041 ];
INF_S1                    (idx, [1:   8]) = [  2.38927E-01 0.00019  5.37036E-03 0.00079  4.77446E-04 0.00696  3.84803E-01 0.00053 ];
INF_S2                    (idx, [1:   8]) = [  9.78911E-02 0.00029 -1.58059E-03 0.00210  2.63235E-04 0.00879  9.10661E-02 0.00059 ];
INF_S3                    (idx, [1:   8]) = [  9.21960E-03 0.00228 -1.88506E-03 0.00148  9.49580E-05 0.01978  2.73467E-02 0.00222 ];
INF_S4                    (idx, [1:   8]) = [ -9.71859E-03 0.00204 -6.35691E-04 0.00469  1.80151E-06 0.87433 -8.54154E-03 0.00620 ];
INF_S5                    (idx, [1:   8]) = [  1.44724E-04 0.11165  1.08876E-05 0.21306 -3.59254E-05 0.04682  6.34661E-03 0.00931 ];
INF_S6                    (idx, [1:   8]) = [  5.22538E-03 0.00263 -1.47243E-04 0.01564 -4.96037E-05 0.02978 -1.63957E-02 0.00232 ];
INF_S7                    (idx, [1:   8]) = [  9.13825E-04 0.01348 -1.69176E-04 0.01648 -4.45594E-05 0.01996  4.31043E-04 0.08062 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11135E-01 0.00011  1.83629E-02 0.00030  1.11694E-03 0.00346  1.45490E+00 0.00041 ];
INF_SP1                   (idx, [1:   8]) = [  2.38928E-01 0.00019  5.37036E-03 0.00079  4.77446E-04 0.00696  3.84803E-01 0.00053 ];
INF_SP2                   (idx, [1:   8]) = [  9.78913E-02 0.00029 -1.58059E-03 0.00210  2.63235E-04 0.00879  9.10661E-02 0.00059 ];
INF_SP3                   (idx, [1:   8]) = [  9.21949E-03 0.00227 -1.88506E-03 0.00148  9.49580E-05 0.01978  2.73467E-02 0.00222 ];
INF_SP4                   (idx, [1:   8]) = [ -9.71877E-03 0.00204 -6.35691E-04 0.00469  1.80151E-06 0.87433 -8.54154E-03 0.00620 ];
INF_SP5                   (idx, [1:   8]) = [  1.44846E-04 0.11186  1.08876E-05 0.21306 -3.59254E-05 0.04682  6.34661E-03 0.00931 ];
INF_SP6                   (idx, [1:   8]) = [  5.22503E-03 0.00263 -1.47243E-04 0.01564 -4.96037E-05 0.02978 -1.63957E-02 0.00232 ];
INF_SP7                   (idx, [1:   8]) = [  9.13638E-04 0.01350 -1.69176E-04 0.01648 -4.45594E-05 0.01996  4.31043E-04 0.08062 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32220E-01 0.00072  1.10403E+00 0.00700 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34080E-01 0.00086  1.17849E+00 0.01030 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33741E-01 0.00102  1.17789E+00 0.00703 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28917E-01 0.00081  9.81963E-01 0.00769 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43544E+00 0.00072  3.02284E-01 0.00706 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42404E+00 0.00086  2.83585E-01 0.01053 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42612E+00 0.00102  2.83325E-01 0.00699 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45615E+00 0.00081  3.39941E-01 0.00773 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.56766E-03 0.00844  1.90664E-04 0.05052  1.04725E-03 0.02039  1.03051E-03 0.01980  2.98088E-03 0.01249  9.76494E-04 0.02263  3.41867E-04 0.03413 ];
LAMBDA                    (idx, [1:  14]) = [  8.31875E-01 0.01801  1.24902E-02 9.0E-06  3.15206E-02 0.00043  1.10090E-01 0.00039  3.21090E-01 0.00036  1.34395E+00 0.00032  8.92094E+00 0.00219 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0003' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:41 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:15:42 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284841 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.92388E-01  1.00254E+00  1.00298E+00  1.00269E+00  9.99407E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.2E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13134E-02 0.00108  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88687E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.01031E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.01456E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69692E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.62311E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.62225E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.97285E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.28189E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000893 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00018E+04 0.00076 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00018E+04 0.00076 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.73204E+02 ;
RUNNING_TIME              (idx, 1)        =  3.50154E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.31417E-01  4.31417E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  7.86000E-02  8.78333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.42289E+01  3.41962E+00  2.68793E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  2.73717E-01  2.92833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.65667E-02  8.16667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.50149E+01  1.28182E+02 ];
CPU_USAGE                 (idx, 1)        = 4.94652 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00050E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.73526E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.21031E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.83962E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.21485E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.68189E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.17416E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.52839E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.72218E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  5.75185E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.02219E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.67308E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.98280E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.07877E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.32391E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.65233E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.17440E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.18551E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.25821E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.43963E+11 ;
CS137_ACTIVITY            (idx, 1)        =  4.05043E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.06458E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.54467E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.48184E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.20616E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.30460E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 5 ;
BURNUP                     (idx, [1:  2])  = [  3.00000E+00  3.00027E+00 ];
BURN_DAYS                 (idx, 1)        =  7.50000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.32253E-01 0.00102 ];
U235_FISS                 (idx, [1:   4]) = [  1.09606E+16 0.00066  7.93587E-01 0.00035 ];
U238_FISS                 (idx, [1:   4]) = [  9.19396E+14 0.00277  6.65613E-02 0.00264 ];
PU239_FISS                (idx, [1:   4]) = [  1.89763E+15 0.00179  1.37394E-01 0.00169 ];
PU240_FISS                (idx, [1:   4]) = [  7.00447E+11 0.09315  5.06794E-05 0.09312 ];
PU241_FISS                (idx, [1:   4]) = [  2.90391E+13 0.01527  2.10279E-03 0.01528 ];
U235_CAPT                 (idx, [1:   4]) = [  2.38573E+15 0.00165  1.25571E-01 0.00158 ];
U238_CAPT                 (idx, [1:   4]) = [  8.38473E+15 0.00098  4.41286E-01 0.00064 ];
PU239_CAPT                (idx, [1:   4]) = [  1.05749E+15 0.00241  5.56603E-02 0.00237 ];
PU240_CAPT                (idx, [1:   4]) = [  2.56449E+14 0.00510  1.34984E-02 0.00509 ];
PU241_CAPT                (idx, [1:   4]) = [  1.03121E+13 0.02465  5.42827E-04 0.02462 ];
XE135_CAPT                (idx, [1:   4]) = [  7.35946E+14 0.00291  3.87381E-02 0.00291 ];
SM149_CAPT                (idx, [1:   4]) = [  1.63277E+14 0.00608  8.59448E-03 0.00609 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000893 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.54689E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000893 5.00755E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2870865 2.87477E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2087083 2.08983E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 42945 4.29473E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000893 5.00755E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.28408E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50058E+05 5.4E-09  4.50058E+05 5.4E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.48372E+16 1.5E-05  3.48372E+16 1.5E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38122E+16 2.4E-06  1.38122E+16 2.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.89879E+16 0.00044  1.35216E+16 0.00043  5.46635E+15 0.00104 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.28002E+16 0.00026  2.73338E+16 0.00021  5.46635E+15 0.00104 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.30460E+16 0.00050  3.30460E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.58617E+18 0.00047  4.24780E+17 0.00043  1.16139E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.83885E+14 0.00491 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.30841E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.19866E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12165E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12165E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.64199E+00 0.00045 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.76904E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.73223E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23832E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94452E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96942E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.06341E+00 0.00050 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.05428E+00 0.00051 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.52220E+00 1.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03373E+02 2.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.05448E+00 0.00053  1.04758E+00 0.00052  6.69920E-03 0.00878 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.05461E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.05434E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.05461E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.06375E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74195E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74224E+01 7.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.46005E-07 0.00353 ];
IMP_EALF                  (idx, [1:   2]) = [  5.42956E-07 0.00137 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.25468E-01 0.00283 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.24663E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.19281E-03 0.00567  1.75590E-04 0.03304  1.00929E-03 0.01402  9.84746E-04 0.01454  2.81121E-03 0.00839  9.18447E-04 0.01410  2.93535E-04 0.02593 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.92511E-01 0.01317  1.03169E-02 0.02055  3.14504E-02 0.00034  1.10140E-01 0.00031  3.21119E-01 0.00025  1.34220E+00 0.00037  8.57472E+00 0.00984 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.46465E-03 0.00805  1.74351E-04 0.04692  1.05237E-03 0.02058  1.04647E-03 0.02222  2.90574E-03 0.01204  9.71922E-04 0.02157  3.13799E-04 0.03877 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.03790E-01 0.01978  1.24902E-02 8.5E-06  3.14415E-02 0.00049  1.10193E-01 0.00050  3.21049E-01 0.00036  1.34193E+00 0.00045  8.96398E+00 0.00254 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.64482E-05 0.00123  2.64373E-05 0.00123  2.81627E-05 0.01333 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.78853E-05 0.00111  2.78738E-05 0.00112  2.96865E-05 0.01327 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.35363E-03 0.00893  1.86211E-04 0.05165  9.97541E-04 0.02305  1.02302E-03 0.02249  2.89400E-03 0.01298  9.46102E-04 0.02200  3.06758E-04 0.04226 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.99161E-01 0.02123  1.24901E-02 1.2E-05  3.14363E-02 0.00059  1.10097E-01 0.00053  3.21075E-01 0.00041  1.34168E+00 0.00049  8.95902E+00 0.00324 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.63507E-05 0.00274  2.63300E-05 0.00275  2.86263E-05 0.03104 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.77817E-05 0.00267  2.77599E-05 0.00268  3.01786E-05 0.03100 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.41324E-03 0.02657  1.76596E-04 0.17462  9.51194E-04 0.07081  1.04992E-03 0.06635  3.00964E-03 0.04214  9.48553E-04 0.07230  2.77335E-04 0.13071 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.59540E-01 0.06315  1.24895E-02 3.4E-05  3.14186E-02 0.00137  1.10107E-01 0.00129  3.20789E-01 0.00106  1.34241E+00 0.00167  9.02891E+00 0.00707 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.44837E-03 0.02589  1.76148E-04 0.16961  9.47984E-04 0.06926  1.08004E-03 0.06460  3.01841E-03 0.04086  9.42165E-04 0.07013  2.83628E-04 0.12928 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.63169E-01 0.06296  1.24895E-02 3.4E-05  3.14201E-02 0.00136  1.10113E-01 0.00129  3.20757E-01 0.00104  1.34230E+00 0.00168  9.02862E+00 0.00706 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.44561E+02 0.02676 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.63590E-05 0.00073 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.77913E-05 0.00053 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.38428E-03 0.00475 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.42252E+02 0.00478 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.12018E-07 0.00064 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.84439E-06 0.00043  2.84419E-06 0.00043  2.87416E-06 0.00516 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.05467E-05 0.00078  4.05692E-05 0.00079  3.73253E-05 0.00909 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.70893E-01 0.00032  6.70570E-01 0.00032  7.35503E-01 0.00925 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03157E+01 0.01377 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.62225E+01 0.00045  3.48600E+01 0.00048 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.41467E+04 0.00240  2.94737E+05 0.00163  6.05962E+05 0.00071  6.51111E+05 0.00068  5.97482E+05 0.00073  6.41907E+05 0.00069  4.36122E+05 0.00087  3.86055E+05 0.00090  2.95135E+05 0.00069  2.41055E+05 0.00072  2.08051E+05 0.00078  1.87517E+05 0.00071  1.73295E+05 0.00083  1.64574E+05 0.00084  1.60374E+05 0.00087  1.38440E+05 0.00083  1.36654E+05 0.00099  1.35566E+05 0.00117  1.32937E+05 0.00096  2.59711E+05 0.00077  2.50893E+05 0.00062  1.81132E+05 0.00058  1.17219E+05 0.00117  1.35719E+05 0.00088  1.28436E+05 0.00093  1.16106E+05 0.00115  1.91132E+05 0.00060  4.38310E+04 0.00139  5.48259E+04 0.00133  4.97268E+04 0.00112  2.89082E+04 0.00154  5.02172E+04 0.00158  3.41249E+04 0.00233  2.90581E+04 0.00194  5.45870E+03 0.00377  5.34716E+03 0.00342  5.26603E+03 0.00302  5.31100E+03 0.00339  5.30111E+03 0.00321  5.37724E+03 0.00333  5.70707E+03 0.00331  5.36171E+03 0.00346  1.02748E+04 0.00283  1.65024E+04 0.00283  2.09505E+04 0.00213  5.54776E+04 0.00174  5.84157E+04 0.00158  6.34383E+04 0.00117  4.28286E+04 0.00156  3.13529E+04 0.00121  2.38508E+04 0.00166  2.82436E+04 0.00155  5.46928E+04 0.00141  7.53975E+04 0.00096  1.47212E+05 0.00090  2.28402E+05 0.00117  3.42257E+05 0.00115  2.18753E+05 0.00133  1.56835E+05 0.00120  1.12752E+05 0.00133  1.01265E+05 0.00121  9.97902E+04 0.00111  8.32960E+04 0.00123  5.63890E+04 0.00110  5.20807E+04 0.00119  4.63086E+04 0.00105  3.91417E+04 0.00118  3.08092E+04 0.00133  2.06381E+04 0.00160  7.30172E+03 0.00179 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.06347E+00 0.00045 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.23567E+18 0.00040  3.50527E+17 0.00103 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38033E-01 0.00010  1.52299E+00 0.00032 ];
INF_CAPT                  (idx, [1:   4]) = [  6.60535E-03 0.00076  3.08877E-02 0.00046 ];
INF_ABS                   (idx, [1:   4]) = [  8.69891E-03 0.00062  6.29173E-02 0.00074 ];
INF_FISS                  (idx, [1:   4]) = [  2.09356E-03 0.00044  3.20296E-02 0.00101 ];
INF_NSF                   (idx, [1:   4]) = [  5.42622E-03 0.00044  8.02709E-02 0.00103 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.59186E+00 4.7E-05  2.50615E+00 3.0E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04212E+02 4.7E-06  2.03180E+02 4.9E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.86596E-08 0.00044  2.56282E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29329E-01 0.00011  1.46007E+00 0.00036 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44301E-01 0.00020  3.86440E-01 0.00049 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63762E-02 0.00028  9.15885E-02 0.00083 ];
INF_SCATT3                (idx, [1:   4]) = [  7.36761E-03 0.00266  2.75968E-02 0.00205 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03352E-02 0.00181 -8.51237E-03 0.00515 ];
INF_SCATT5                (idx, [1:   4]) = [  9.91818E-05 0.14570  6.41396E-03 0.00649 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07719E-03 0.00252 -1.64804E-02 0.00326 ];
INF_SCATT7                (idx, [1:   4]) = [  7.44932E-04 0.02097  3.75276E-04 0.13077 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29369E-01 0.00011  1.46007E+00 0.00036 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44301E-01 0.00020  3.86440E-01 0.00049 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63765E-02 0.00028  9.15885E-02 0.00083 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.36728E-03 0.00265  2.75968E-02 0.00205 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03347E-02 0.00181 -8.51237E-03 0.00515 ];
INF_SCATTP5               (idx, [1:   4]) = [  9.91204E-05 0.14626  6.41396E-03 0.00649 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07710E-03 0.00252 -1.64804E-02 0.00326 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.44570E-04 0.02096  3.75276E-04 0.13077 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13158E-01 0.00029  9.84172E-01 0.00026 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56379E+00 0.00029  3.38695E-01 0.00026 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.65855E-03 0.00063  6.29173E-02 0.00074 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69524E-02 0.00027  6.40418E-02 0.00082 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11081E-01 9.8E-05  1.82480E-02 0.00051  1.11775E-03 0.00357  1.45895E+00 0.00036 ];
INF_S1                    (idx, [1:   8]) = [  2.38967E-01 0.00019  5.33350E-03 0.00110  4.77410E-04 0.00770  3.85963E-01 0.00049 ];
INF_S2                    (idx, [1:   8]) = [  9.79526E-02 0.00027 -1.57638E-03 0.00214  2.61150E-04 0.00998  9.13274E-02 0.00084 ];
INF_S3                    (idx, [1:   8]) = [  9.24286E-03 0.00203 -1.87524E-03 0.00172  9.14385E-05 0.02130  2.75054E-02 0.00208 ];
INF_S4                    (idx, [1:   8]) = [ -9.71152E-03 0.00192 -6.23674E-04 0.00440  5.03024E-07 1.00000 -8.51287E-03 0.00514 ];
INF_S5                    (idx, [1:   8]) = [  8.29724E-05 0.18004  1.62094E-05 0.14619 -3.76456E-05 0.03728  6.45161E-03 0.00643 ];
INF_S6                    (idx, [1:   8]) = [  5.22133E-03 0.00248 -1.44135E-04 0.01486 -4.83079E-05 0.02658 -1.64321E-02 0.00325 ];
INF_S7                    (idx, [1:   8]) = [  9.14850E-04 0.01671 -1.69917E-04 0.01249 -4.34526E-05 0.03114  4.18729E-04 0.11697 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11121E-01 9.8E-05  1.82480E-02 0.00051  1.11775E-03 0.00357  1.45895E+00 0.00036 ];
INF_SP1                   (idx, [1:   8]) = [  2.38968E-01 0.00019  5.33350E-03 0.00110  4.77410E-04 0.00770  3.85963E-01 0.00049 ];
INF_SP2                   (idx, [1:   8]) = [  9.79529E-02 0.00027 -1.57638E-03 0.00214  2.61150E-04 0.00998  9.13274E-02 0.00084 ];
INF_SP3                   (idx, [1:   8]) = [  9.24252E-03 0.00203 -1.87524E-03 0.00172  9.14385E-05 0.02130  2.75054E-02 0.00208 ];
INF_SP4                   (idx, [1:   8]) = [ -9.71104E-03 0.00191 -6.23674E-04 0.00440  5.03024E-07 1.00000 -8.51287E-03 0.00514 ];
INF_SP5                   (idx, [1:   8]) = [  8.29110E-05 0.18068  1.62094E-05 0.14619 -3.76456E-05 0.03728  6.45161E-03 0.00643 ];
INF_SP6                   (idx, [1:   8]) = [  5.22124E-03 0.00248 -1.44135E-04 0.01486 -4.83079E-05 0.02658 -1.64321E-02 0.00325 ];
INF_SP7                   (idx, [1:   8]) = [  9.14487E-04 0.01669 -1.69917E-04 0.01249 -4.34526E-05 0.03114  4.18729E-04 0.11697 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32287E-01 0.00075  1.11250E+00 0.00605 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34002E-01 0.00096  1.19825E+00 0.00775 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33922E-01 0.00100  1.18205E+00 0.00741 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29016E-01 0.00104  9.84823E-01 0.00547 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43502E+00 0.00075  2.99881E-01 0.00588 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42452E+00 0.00096  2.78577E-01 0.00759 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42501E+00 0.00100  2.82359E-01 0.00723 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45554E+00 0.00104  3.38708E-01 0.00536 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.46465E-03 0.00805  1.74351E-04 0.04692  1.05237E-03 0.02058  1.04647E-03 0.02222  2.90574E-03 0.01204  9.71922E-04 0.02157  3.13799E-04 0.03877 ];
LAMBDA                    (idx, [1:  14]) = [  8.03790E-01 0.01978  1.24902E-02 8.5E-06  3.14415E-02 0.00049  1.10193E-01 0.00050  3.21049E-01 0.00036  1.34193E+00 0.00045  8.96398E+00 0.00254 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0003' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:41 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:21:48 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284841 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00186E+00  9.98982E-01  1.00066E+00  1.00150E+00  9.97002E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.3E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12909E-02 0.00106  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88709E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.02343E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.02767E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69017E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.61026E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.60940E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.86741E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.24665E-01 0.00107  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000790 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00016E+04 0.00078 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00016E+04 0.00078 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.03714E+02 ;
RUNNING_TIME              (idx, 1)        =  4.11233E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.31417E-01  4.31417E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  9.57333E-02  8.45000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  4.02655E+01  3.39330E+00  2.64325E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  3.27317E-01  2.67333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.97833E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.11231E+01  1.26988E+02 ];
CPU_USAGE                 (idx, 1)        = 4.95374 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00061E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75308E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.25843E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.82516E+04 ;
TOT_SF_RATE               (idx, 1)        =  9.16332E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.69834E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.18558E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.56007E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.70657E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  6.68548E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.12744E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.12357E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.06563E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.56190E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.42087E+07 ;
SR90_ACTIVITY             (idx, 1)        =  4.79001E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.20281E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.23361E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.29753E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.20330E+12 ;
CS137_ACTIVITY            (idx, 1)        =  5.40083E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.09520E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.51380E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  8.30424E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.21613E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.35711E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 6 ;
BURNUP                     (idx, [1:  2])  = [  4.00000E+00  4.00035E+00 ];
BURN_DAYS                 (idx, 1)        =  1.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.40052E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  1.04481E+16 0.00066  7.57106E-01 0.00039 ];
U238_FISS                 (idx, [1:   4]) = [  9.32082E+14 0.00267  6.75335E-02 0.00252 ];
PU239_FISS                (idx, [1:   4]) = [  2.35548E+15 0.00165  1.70680E-01 0.00151 ];
PU240_FISS                (idx, [1:   4]) = [  9.50974E+11 0.08476  6.90081E-05 0.08490 ];
PU241_FISS                (idx, [1:   4]) = [  5.88502E+13 0.01040  4.26414E-03 0.01037 ];
U235_CAPT                 (idx, [1:   4]) = [  2.27743E+15 0.00171  1.16603E-01 0.00157 ];
U238_CAPT                 (idx, [1:   4]) = [  8.44872E+15 0.00107  4.32551E-01 0.00072 ];
PU239_CAPT                (idx, [1:   4]) = [  1.31187E+15 0.00217  6.71733E-02 0.00215 ];
PU240_CAPT                (idx, [1:   4]) = [  3.91958E+14 0.00414  2.00690E-02 0.00411 ];
PU241_CAPT                (idx, [1:   4]) = [  2.13487E+13 0.01895  1.09309E-03 0.01894 ];
XE135_CAPT                (idx, [1:   4]) = [  7.37245E+14 0.00310  3.77511E-02 0.00310 ];
SM149_CAPT                (idx, [1:   4]) = [  1.68559E+14 0.00617  8.63172E-03 0.00619 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000790 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.60585E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000790 5.00761E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2904918 2.90893E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2052660 2.05547E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 43212 4.32081E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000790 5.00761E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.12227E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50058E+05 5.4E-09  4.50058E+05 5.4E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.50208E+16 1.7E-05  3.50208E+16 1.7E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37979E+16 2.9E-06  1.37979E+16 2.9E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.95301E+16 0.00046  1.40099E+16 0.00046  5.52022E+15 0.00112 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.33280E+16 0.00027  2.78078E+16 0.00023  5.52022E+15 0.00112 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.35711E+16 0.00051  3.35711E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.60671E+18 0.00048  4.29292E+17 0.00047  1.17741E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.90151E+14 0.00517 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.36182E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.21336E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12048E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12048E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.63122E+00 0.00043 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.77431E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.70276E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23834E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94396E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96945E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.05254E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04345E+00 0.00053 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.53812E+00 2.0E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03584E+02 2.9E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04343E+00 0.00054  1.03678E+00 0.00053  6.67207E-03 0.00876 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.04334E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04332E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.04334E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.05243E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73868E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73885E+01 9.0E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.63994E-07 0.00341 ];
IMP_EALF                  (idx, [1:   2]) = [  5.61753E-07 0.00156 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.28709E-01 0.00276 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.28858E-01 0.00122 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.12570E-03 0.00564  1.81994E-04 0.03304  1.00223E-03 0.01474  9.56667E-04 0.01524  2.75551E-03 0.00837  9.24225E-04 0.01428  3.05075E-04 0.02572 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.15474E-01 0.01381  1.03435E-02 0.02040  3.13949E-02 0.00035  1.10239E-01 0.00035  3.21342E-01 0.00025  1.34152E+00 0.00044  8.49736E+00 0.01076 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.30430E-03 0.00844  1.85124E-04 0.04887  1.02799E-03 0.02228  9.64305E-04 0.02108  2.86018E-03 0.01267  9.46098E-04 0.02110  3.20602E-04 0.03918 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.28548E-01 0.02111  1.24918E-02 8.4E-05  3.14048E-02 0.00048  1.10241E-01 0.00051  3.21422E-01 0.00039  1.34198E+00 0.00065  8.96010E+00 0.00313 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.62981E-05 0.00127  2.62889E-05 0.00127  2.77128E-05 0.01281 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.74364E-05 0.00116  2.74268E-05 0.00116  2.89069E-05 0.01275 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.38711E-03 0.00882  1.98174E-04 0.04920  1.04900E-03 0.02356  1.00132E-03 0.02311  2.83524E-03 0.01325  9.87764E-04 0.02307  3.15618E-04 0.03989 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.12957E-01 0.02154  1.24902E-02 1.2E-05  3.14095E-02 0.00060  1.10221E-01 0.00058  3.21336E-01 0.00042  1.34225E+00 0.00048  8.96784E+00 0.00351 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.59737E-05 0.00301  2.59639E-05 0.00302  2.70547E-05 0.03187 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.70966E-05 0.00293  2.70865E-05 0.00294  2.82327E-05 0.03191 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.20676E-03 0.02737  1.74315E-04 0.16994  9.56827E-04 0.07373  1.04203E-03 0.06788  2.75348E-03 0.04049  9.47689E-04 0.07317  3.32414E-04 0.12102 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.73328E-01 0.06766  1.24904E-02 2.4E-05  3.14210E-02 0.00143  1.09991E-01 0.00124  3.21366E-01 0.00115  1.34091E+00 0.00213  8.91890E+00 0.00606 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.18587E-03 0.02647  1.67078E-04 0.17037  9.66121E-04 0.06988  1.03495E-03 0.06737  2.73259E-03 0.03904  9.46596E-04 0.07335  3.38542E-04 0.11741 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.66542E-01 0.06509  1.24904E-02 2.5E-05  3.14144E-02 0.00142  1.09991E-01 0.00123  3.21403E-01 0.00114  1.34115E+00 0.00211  8.91649E+00 0.00604 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.40967E+02 0.02762 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.61969E-05 0.00081 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.73302E-05 0.00055 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.32599E-03 0.00513 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.41630E+02 0.00530 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.07639E-07 0.00070 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.82962E-06 0.00044  2.82949E-06 0.00044  2.84909E-06 0.00534 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.02959E-05 0.00088  4.03171E-05 0.00087  3.70452E-05 0.00918 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.67937E-01 0.00034  6.67634E-01 0.00034  7.29471E-01 0.00912 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05408E+01 0.01470 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.60940E+01 0.00047  3.46593E+01 0.00045 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.45842E+04 0.00257  2.96793E+05 0.00132  6.06397E+05 0.00096  6.50882E+05 0.00067  5.98624E+05 0.00060  6.41365E+05 0.00069  4.35428E+05 0.00062  3.85373E+05 0.00048  2.94754E+05 0.00047  2.41103E+05 0.00056  2.07791E+05 0.00088  1.87384E+05 0.00078  1.72855E+05 0.00095  1.64164E+05 0.00084  1.60250E+05 0.00075  1.38320E+05 0.00097  1.36818E+05 0.00107  1.35373E+05 0.00104  1.33012E+05 0.00084  2.59667E+05 0.00067  2.50772E+05 0.00061  1.81123E+05 0.00108  1.17326E+05 0.00089  1.35739E+05 0.00084  1.28431E+05 0.00098  1.16141E+05 0.00114  1.90434E+05 0.00079  4.36564E+04 0.00173  5.47876E+04 0.00090  4.96378E+04 0.00133  2.89288E+04 0.00248  5.02732E+04 0.00146  3.41191E+04 0.00199  2.88525E+04 0.00121  5.39315E+03 0.00320  5.18410E+03 0.00378  5.04936E+03 0.00419  5.02707E+03 0.00387  5.06418E+03 0.00319  5.22855E+03 0.00318  5.58685E+03 0.00358  5.33839E+03 0.00343  1.01501E+04 0.00282  1.62946E+04 0.00278  2.08677E+04 0.00220  5.52304E+04 0.00148  5.81130E+04 0.00154  6.29471E+04 0.00152  4.21470E+04 0.00128  3.06831E+04 0.00161  2.32375E+04 0.00161  2.75462E+04 0.00135  5.33361E+04 0.00122  7.40019E+04 0.00121  1.44740E+05 0.00110  2.25262E+05 0.00134  3.38498E+05 0.00117  2.16519E+05 0.00127  1.55293E+05 0.00137  1.11574E+05 0.00168  1.00420E+05 0.00168  9.89433E+04 0.00150  8.26369E+04 0.00169  5.58798E+04 0.00166  5.16228E+04 0.00180  4.58919E+04 0.00180  3.88824E+04 0.00180  3.06688E+04 0.00195  2.04898E+04 0.00206  7.24234E+03 0.00248 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.05241E+00 0.00050 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.25481E+18 0.00054  3.51922E+17 0.00122 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38082E-01 0.00011  1.52930E+00 0.00045 ];
INF_CAPT                  (idx, [1:   4]) = [  6.72220E-03 0.00058  3.15299E-02 0.00056 ];
INF_ABS                   (idx, [1:   4]) = [  8.78555E-03 0.00047  6.33862E-02 0.00093 ];
INF_FISS                  (idx, [1:   4]) = [  2.06335E-03 0.00053  3.18563E-02 0.00130 ];
INF_NSF                   (idx, [1:   4]) = [  5.36489E-03 0.00052  8.03991E-02 0.00132 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.60009E+00 6.0E-05  2.52381E+00 3.8E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04317E+02 7.3E-06  2.03415E+02 6.3E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.84374E-08 0.00037  2.56600E-06 0.00020 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29296E-01 0.00012  1.46591E+00 0.00052 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44353E-01 0.00018  3.88037E-01 0.00061 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63743E-02 0.00037  9.19550E-02 0.00087 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34401E-03 0.00350  2.74728E-02 0.00269 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03365E-02 0.00205 -8.64526E-03 0.00471 ];
INF_SCATT5                (idx, [1:   4]) = [  1.49404E-04 0.11672  6.42710E-03 0.00676 ];
INF_SCATT6                (idx, [1:   4]) = [  5.05931E-03 0.00308 -1.66906E-02 0.00275 ];
INF_SCATT7                (idx, [1:   4]) = [  7.41642E-04 0.02197  3.90538E-04 0.09948 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29337E-01 0.00012  1.46591E+00 0.00052 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44353E-01 0.00018  3.88037E-01 0.00061 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63745E-02 0.00037  9.19550E-02 0.00087 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34386E-03 0.00349  2.74728E-02 0.00269 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03364E-02 0.00204 -8.64526E-03 0.00471 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.49269E-04 0.11701  6.42710E-03 0.00676 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.05929E-03 0.00308 -1.66906E-02 0.00275 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.41878E-04 0.02192  3.90538E-04 0.09948 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12941E-01 0.00029  9.88746E-01 0.00044 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56538E+00 0.00029  3.37129E-01 0.00044 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.74485E-03 0.00046  6.33862E-02 0.00093 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69618E-02 0.00020  6.45206E-02 0.00119 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11120E-01 0.00011  1.81760E-02 0.00037  1.13116E-03 0.00412  1.46478E+00 0.00052 ];
INF_S1                    (idx, [1:   8]) = [  2.39051E-01 0.00018  5.30198E-03 0.00090  4.87425E-04 0.00948  3.87549E-01 0.00061 ];
INF_S2                    (idx, [1:   8]) = [  9.79619E-02 0.00037 -1.58763E-03 0.00229  2.66845E-04 0.01372  9.16881E-02 0.00087 ];
INF_S3                    (idx, [1:   8]) = [  9.21364E-03 0.00273 -1.86964E-03 0.00189  9.25131E-05 0.02191  2.73803E-02 0.00271 ];
INF_S4                    (idx, [1:   8]) = [ -9.72591E-03 0.00223 -6.10611E-04 0.00472 -6.07609E-07 1.00000 -8.64465E-03 0.00473 ];
INF_S5                    (idx, [1:   8]) = [  1.25142E-04 0.13682  2.42628E-05 0.10277 -3.99532E-05 0.04288  6.46705E-03 0.00678 ];
INF_S6                    (idx, [1:   8]) = [  5.20863E-03 0.00297 -1.49320E-04 0.01531 -4.98836E-05 0.02640 -1.66407E-02 0.00273 ];
INF_S7                    (idx, [1:   8]) = [  9.21213E-04 0.01717 -1.79571E-04 0.01562 -4.54722E-05 0.03712  4.36010E-04 0.08861 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11161E-01 0.00011  1.81760E-02 0.00037  1.13116E-03 0.00412  1.46478E+00 0.00052 ];
INF_SP1                   (idx, [1:   8]) = [  2.39051E-01 0.00018  5.30198E-03 0.00090  4.87425E-04 0.00948  3.87549E-01 0.00061 ];
INF_SP2                   (idx, [1:   8]) = [  9.79621E-02 0.00037 -1.58763E-03 0.00229  2.66845E-04 0.01372  9.16881E-02 0.00087 ];
INF_SP3                   (idx, [1:   8]) = [  9.21349E-03 0.00272 -1.86964E-03 0.00189  9.25131E-05 0.02191  2.73803E-02 0.00271 ];
INF_SP4                   (idx, [1:   8]) = [ -9.72580E-03 0.00222 -6.10611E-04 0.00472 -6.07609E-07 1.00000 -8.64465E-03 0.00473 ];
INF_SP5                   (idx, [1:   8]) = [  1.25007E-04 0.13714  2.42628E-05 0.10277 -3.99532E-05 0.04288  6.46705E-03 0.00678 ];
INF_SP6                   (idx, [1:   8]) = [  5.20861E-03 0.00297 -1.49320E-04 0.01531 -4.98836E-05 0.02640 -1.66407E-02 0.00273 ];
INF_SP7                   (idx, [1:   8]) = [  9.21449E-04 0.01713 -1.79571E-04 0.01562 -4.54722E-05 0.03712  4.36010E-04 0.08861 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31978E-01 0.00055  1.13242E+00 0.00554 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33706E-01 0.00105  1.20956E+00 0.00810 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33712E-01 0.00082  1.21835E+00 0.00742 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28600E-01 0.00091  9.99541E-01 0.00551 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43693E+00 0.00055  2.94574E-01 0.00561 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42633E+00 0.00104  2.76022E-01 0.00818 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42628E+00 0.00082  2.73965E-01 0.00761 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45818E+00 0.00091  3.33735E-01 0.00563 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.30430E-03 0.00844  1.85124E-04 0.04887  1.02799E-03 0.02228  9.64305E-04 0.02108  2.86018E-03 0.01267  9.46098E-04 0.02110  3.20602E-04 0.03918 ];
LAMBDA                    (idx, [1:  14]) = [  8.28548E-01 0.02111  1.24918E-02 8.4E-05  3.14048E-02 0.00048  1.10241E-01 0.00051  3.21422E-01 0.00039  1.34198E+00 0.00065  8.96010E+00 0.00313 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0003' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:41 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:27:56 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284841 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00062E+00  9.99537E-01  1.00008E+00  1.00183E+00  9.97937E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.5E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12953E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88705E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.03190E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.03614E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68494E+00 0.00024  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.59828E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.59742E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.79078E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.22638E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000720 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00014E+04 0.00082 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00014E+04 0.00082 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.34301E+02 ;
RUNNING_TIME              (idx, 1)        =  4.72468E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.31417E-01  4.31417E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.13467E-01  8.48333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  4.63167E+01  3.39808E+00  2.65317E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  3.81483E-01  2.61000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.30833E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.72466E+01  1.26750E+02 ];
CPU_USAGE                 (idx, 1)        = 4.95910 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99807E+00 0.00021 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76629E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.29475E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.81278E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.09486E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.71288E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.19567E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.58184E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.69319E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  7.61944E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.20807E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.65942E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.14191E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.96003E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.49388E+07 ;
SR90_ACTIVITY             (idx, 1)        =  5.89557E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.22805E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.27098E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.33211E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.92884E+12 ;
CS137_ACTIVITY            (idx, 1)        =  6.75063E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.11873E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.48774E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.23339E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.22401E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.41100E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 7 ;
BURNUP                     (idx, [1:  2])  = [  5.00000E+00  5.00044E+00 ];
BURN_DAYS                 (idx, 1)        =  1.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.49338E-01 0.00112 ];
U235_FISS                 (idx, [1:   4]) = [  9.99172E+15 0.00072  7.24527E-01 0.00043 ];
U238_FISS                 (idx, [1:   4]) = [  9.48214E+14 0.00281  6.87469E-02 0.00263 ];
PU239_FISS                (idx, [1:   4]) = [  2.74771E+15 0.00155  1.99243E-01 0.00143 ];
PU240_FISS                (idx, [1:   4]) = [  1.35887E+12 0.07016  9.84151E-05 0.07006 ];
PU241_FISS                (idx, [1:   4]) = [  9.66847E+13 0.00844  7.01099E-03 0.00843 ];
U235_CAPT                 (idx, [1:   4]) = [  2.19072E+15 0.00176  1.09135E-01 0.00170 ];
U238_CAPT                 (idx, [1:   4]) = [  8.53060E+15 0.00105  4.24926E-01 0.00070 ];
PU239_CAPT                (idx, [1:   4]) = [  1.53054E+15 0.00215  7.62459E-02 0.00208 ];
PU240_CAPT                (idx, [1:   4]) = [  5.28929E+14 0.00365  2.63476E-02 0.00357 ];
PU241_CAPT                (idx, [1:   4]) = [  3.47282E+13 0.01309  1.73015E-03 0.01310 ];
XE135_CAPT                (idx, [1:   4]) = [  7.40850E+14 0.00298  3.69071E-02 0.00295 ];
SM149_CAPT                (idx, [1:   4]) = [  1.74339E+14 0.00596  8.68603E-03 0.00598 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000720 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.70117E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000720 5.00770E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2938397 2.94260E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2018851 2.02162E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 43472 4.34849E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000720 5.00770E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.09782E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50058E+05 5.4E-09  4.50058E+05 5.4E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.51868E+16 1.7E-05  3.51868E+16 1.7E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37848E+16 2.9E-06  1.37848E+16 2.9E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.00739E+16 0.00045  1.45001E+16 0.00047  5.57372E+15 0.00107 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.38587E+16 0.00027  2.82850E+16 0.00024  5.57372E+15 0.00107 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.41100E+16 0.00051  3.41100E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.62876E+18 0.00046  4.34691E+17 0.00047  1.19407E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.96691E+14 0.00485 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.41554E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.22883E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11932E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11932E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.62015E+00 0.00047 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.78045E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.66982E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23835E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94344E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96942E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.04109E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.03204E+00 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.55257E+00 1.9E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03777E+02 2.9E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03213E+00 0.00057  1.02580E+00 0.00055  6.24071E-03 0.00856 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.03183E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03170E+00 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.03183E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.04088E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73529E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73549E+01 8.6E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.83715E-07 0.00367 ];
IMP_EALF                  (idx, [1:   2]) = [  5.80963E-07 0.00149 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.33448E-01 0.00281 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.33331E-01 0.00110 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.02243E-03 0.00573  1.67769E-04 0.03440  9.95478E-04 0.01395  9.47019E-04 0.01472  2.72115E-03 0.00808  8.99115E-04 0.01463  2.91899E-04 0.02556 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.01240E-01 0.01305  1.02702E-02 0.02083  3.13427E-02 0.00038  1.10249E-01 0.00036  3.21391E-01 0.00025  1.33825E+00 0.00060  8.55089E+00 0.01042 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.12423E-03 0.00835  1.75989E-04 0.05237  9.87353E-04 0.02158  9.56684E-04 0.02115  2.76825E-03 0.01262  9.33195E-04 0.02304  3.02757E-04 0.03662 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.17968E-01 0.01950  1.24912E-02 6.6E-05  3.13489E-02 0.00053  1.10315E-01 0.00053  3.21422E-01 0.00037  1.33794E+00 0.00104  8.98576E+00 0.00364 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.63273E-05 0.00131  2.63139E-05 0.00132  2.85164E-05 0.01334 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.71682E-05 0.00114  2.71543E-05 0.00115  2.94233E-05 0.01329 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.03579E-03 0.00870  1.77352E-04 0.05065  9.38478E-04 0.02223  9.32719E-04 0.02406  2.75676E-03 0.01209  9.32581E-04 0.02384  2.97902E-04 0.04084 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.21613E-01 0.02171  1.24918E-02 0.00017  3.13622E-02 0.00064  1.10325E-01 0.00065  3.21446E-01 0.00041  1.33847E+00 0.00108  8.98649E+00 0.00381 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.61463E-05 0.00285  2.61295E-05 0.00286  2.83960E-05 0.03201 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.69819E-05 0.00279  2.69645E-05 0.00280  2.93053E-05 0.03202 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.25062E-03 0.03068  1.36168E-04 0.17649  1.04932E-03 0.07601  1.00428E-03 0.07479  2.84995E-03 0.04487  9.20663E-04 0.07301  2.90238E-04 0.13917 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.41834E-01 0.07044  1.24903E-02 2.9E-05  3.12705E-02 0.00156  1.10180E-01 0.00143  3.21494E-01 0.00121  1.34056E+00 0.00171  9.13759E+00 0.00754 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.21456E-03 0.02984  1.46155E-04 0.17548  1.06031E-03 0.07295  9.71655E-04 0.07400  2.82610E-03 0.04341  9.24904E-04 0.07123  2.85440E-04 0.13344 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.35449E-01 0.06885  1.24903E-02 2.9E-05  3.12758E-02 0.00155  1.10175E-01 0.00142  3.21504E-01 0.00120  1.34044E+00 0.00170  9.13892E+00 0.00755 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.40701E+02 0.03094 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.62208E-05 0.00082 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.70587E-05 0.00058 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.08776E-03 0.00589 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.32294E+02 0.00602 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.03309E-07 0.00068 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.81254E-06 0.00042  2.81233E-06 0.00042  2.84742E-06 0.00560 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.01109E-05 0.00085  4.01299E-05 0.00085  3.71508E-05 0.00922 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.64630E-01 0.00033  6.64417E-01 0.00033  7.12208E-01 0.00919 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02762E+01 0.01317 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.59742E+01 0.00047  3.44876E+01 0.00049 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.49945E+04 0.00331  2.96996E+05 0.00141  6.07912E+05 0.00062  6.50696E+05 0.00070  5.97988E+05 0.00087  6.41735E+05 0.00046  4.35926E+05 0.00070  3.85583E+05 0.00078  2.94645E+05 0.00068  2.41474E+05 0.00083  2.08022E+05 0.00067  1.87177E+05 0.00089  1.73135E+05 0.00067  1.64577E+05 0.00094  1.60242E+05 0.00091  1.38385E+05 0.00111  1.36679E+05 0.00069  1.35533E+05 0.00097  1.33044E+05 0.00115  2.59840E+05 0.00054  2.50792E+05 0.00082  1.81309E+05 0.00080  1.17488E+05 0.00097  1.35697E+05 0.00066  1.28413E+05 0.00105  1.15861E+05 0.00111  1.89902E+05 0.00049  4.35935E+04 0.00141  5.47787E+04 0.00151  4.95933E+04 0.00147  2.89449E+04 0.00211  5.01608E+04 0.00119  3.40164E+04 0.00150  2.86576E+04 0.00173  5.33893E+03 0.00343  5.09854E+03 0.00435  4.88214E+03 0.00322  4.76713E+03 0.00358  4.85393E+03 0.00437  5.03267E+03 0.00383  5.49989E+03 0.00296  5.25001E+03 0.00376  1.00273E+04 0.00279  1.61341E+04 0.00211  2.07102E+04 0.00250  5.50139E+04 0.00141  5.76273E+04 0.00102  6.22905E+04 0.00110  4.14696E+04 0.00109  3.01711E+04 0.00168  2.28018E+04 0.00187  2.69648E+04 0.00163  5.23269E+04 0.00166  7.27273E+04 0.00160  1.42724E+05 0.00131  2.22724E+05 0.00118  3.34626E+05 0.00127  2.14525E+05 0.00147  1.53799E+05 0.00142  1.10679E+05 0.00147  9.95209E+04 0.00141  9.80717E+04 0.00170  8.20493E+04 0.00168  5.55131E+04 0.00168  5.12602E+04 0.00178  4.55430E+04 0.00170  3.85646E+04 0.00165  3.03232E+04 0.00194  2.03864E+04 0.00193  7.19873E+03 0.00196 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.04075E+00 0.00043 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.27510E+18 0.00032  3.53701E+17 0.00124 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38122E-01 9.9E-05  1.53436E+00 0.00040 ];
INF_CAPT                  (idx, [1:   4]) = [  6.84088E-03 0.00054  3.20957E-02 0.00056 ];
INF_ABS                   (idx, [1:   4]) = [  8.87490E-03 0.00047  6.37422E-02 0.00090 ];
INF_FISS                  (idx, [1:   4]) = [  2.03403E-03 0.00042  3.16465E-02 0.00124 ];
INF_NSF                   (idx, [1:   4]) = [  5.30508E-03 0.00043  8.03723E-02 0.00126 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.60817E+00 5.8E-05  2.53969E+00 3.2E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04419E+02 5.4E-06  2.03629E+02 5.4E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.81857E-08 0.00037  2.56863E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29249E-01 9.8E-05  1.47060E+00 0.00046 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44283E-01 0.00018  3.89221E-01 0.00050 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63575E-02 0.00036  9.21515E-02 0.00096 ];
INF_SCATT3                (idx, [1:   4]) = [  7.38481E-03 0.00249  2.77498E-02 0.00229 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03206E-02 0.00215 -8.68046E-03 0.00492 ];
INF_SCATT5                (idx, [1:   4]) = [  1.48224E-04 0.12967  6.43361E-03 0.00547 ];
INF_SCATT6                (idx, [1:   4]) = [  5.09377E-03 0.00285 -1.66574E-02 0.00317 ];
INF_SCATT7                (idx, [1:   4]) = [  7.45702E-04 0.02000  4.43663E-04 0.11207 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29291E-01 9.8E-05  1.47060E+00 0.00046 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44283E-01 0.00018  3.89221E-01 0.00050 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63579E-02 0.00036  9.21515E-02 0.00096 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.38472E-03 0.00250  2.77498E-02 0.00229 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03204E-02 0.00215 -8.68046E-03 0.00492 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.48281E-04 0.12955  6.43361E-03 0.00547 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.09353E-03 0.00285 -1.66574E-02 0.00317 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.45610E-04 0.02005  4.43663E-04 0.11207 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12959E-01 0.00029  9.92642E-01 0.00033 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56525E+00 0.00029  3.35805E-01 0.00033 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.83370E-03 0.00046  6.37422E-02 0.00090 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69540E-02 0.00021  6.48937E-02 0.00115 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11168E-01 9.7E-05  1.80817E-02 0.00026  1.12910E-03 0.00403  1.46947E+00 0.00046 ];
INF_S1                    (idx, [1:   8]) = [  2.39007E-01 0.00018  5.27550E-03 0.00098  4.84285E-04 0.00639  3.88737E-01 0.00051 ];
INF_S2                    (idx, [1:   8]) = [  9.79345E-02 0.00035 -1.57701E-03 0.00240  2.62366E-04 0.00790  9.18892E-02 0.00096 ];
INF_S3                    (idx, [1:   8]) = [  9.24346E-03 0.00193 -1.85865E-03 0.00155  9.10198E-05 0.01646  2.76588E-02 0.00230 ];
INF_S4                    (idx, [1:   8]) = [ -9.70894E-03 0.00220 -6.11623E-04 0.00472 -1.11952E-06 1.00000 -8.67934E-03 0.00494 ];
INF_S5                    (idx, [1:   8]) = [  1.29725E-04 0.14182  1.84986E-05 0.14459 -3.75049E-05 0.04008  6.47112E-03 0.00537 ];
INF_S6                    (idx, [1:   8]) = [  5.23988E-03 0.00274 -1.46114E-04 0.01092 -4.80613E-05 0.03054 -1.66094E-02 0.00319 ];
INF_S7                    (idx, [1:   8]) = [  9.18741E-04 0.01582 -1.73039E-04 0.01366 -4.23335E-05 0.03419  4.85997E-04 0.10138 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11209E-01 9.7E-05  1.80817E-02 0.00026  1.12910E-03 0.00403  1.46947E+00 0.00046 ];
INF_SP1                   (idx, [1:   8]) = [  2.39008E-01 0.00018  5.27550E-03 0.00098  4.84285E-04 0.00639  3.88737E-01 0.00051 ];
INF_SP2                   (idx, [1:   8]) = [  9.79350E-02 0.00035 -1.57701E-03 0.00240  2.62366E-04 0.00790  9.18892E-02 0.00096 ];
INF_SP3                   (idx, [1:   8]) = [  9.24337E-03 0.00193 -1.85865E-03 0.00155  9.10198E-05 0.01646  2.76588E-02 0.00230 ];
INF_SP4                   (idx, [1:   8]) = [ -9.70874E-03 0.00219 -6.11623E-04 0.00472 -1.11952E-06 1.00000 -8.67934E-03 0.00494 ];
INF_SP5                   (idx, [1:   8]) = [  1.29782E-04 0.14168  1.84986E-05 0.14459 -3.75049E-05 0.04008  6.47112E-03 0.00537 ];
INF_SP6                   (idx, [1:   8]) = [  5.23965E-03 0.00274 -1.46114E-04 0.01092 -4.80613E-05 0.03054 -1.66094E-02 0.00319 ];
INF_SP7                   (idx, [1:   8]) = [  9.18649E-04 0.01586 -1.73039E-04 0.01366 -4.23335E-05 0.03419  4.85997E-04 0.10138 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32333E-01 0.00058  1.14252E+00 0.00639 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33866E-01 0.00095  1.22761E+00 0.00709 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33919E-01 0.00079  1.22448E+00 0.00883 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29282E-01 0.00091  1.00687E+00 0.00761 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43474E+00 0.00058  2.92036E-01 0.00634 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42535E+00 0.00095  2.71862E-01 0.00717 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42502E+00 0.00079  2.72726E-01 0.00868 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45384E+00 0.00091  3.31521E-01 0.00763 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.12423E-03 0.00835  1.75989E-04 0.05237  9.87353E-04 0.02158  9.56684E-04 0.02115  2.76825E-03 0.01262  9.33195E-04 0.02304  3.02757E-04 0.03662 ];
LAMBDA                    (idx, [1:  14]) = [  8.17968E-01 0.01950  1.24912E-02 6.6E-05  3.13489E-02 0.00053  1.10315E-01 0.00053  3.21422E-01 0.00037  1.33794E+00 0.00104  8.98576E+00 0.00364 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0003' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:41 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:34:04 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284841 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.96707E-01  1.00041E+00  1.00246E+00  1.00310E+00  9.97324E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12931E-02 0.00107  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88707E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04076E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04500E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68104E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.58824E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.58736E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.71696E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.20622E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000925 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00019E+04 0.00081 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00019E+04 0.00081 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.64989E+02 ;
RUNNING_TIME              (idx, 1)        =  5.33906E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.31417E-01  4.31417E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.32483E-01  9.16667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  5.23838E+01  3.39565E+00  2.67143E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  4.38700E-01  2.77667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.57500E-02  8.16667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  5.33905E+01  1.27111E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96321 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99958E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.77628E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.33008E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.80245E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.40610E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.73233E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.20919E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.59773E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.68150E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  8.58067E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.27670E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.28144E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.24005E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.29923E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.55270E+07 ;
SR90_ACTIVITY             (idx, 1)        =  6.97177E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.25129E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.30368E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.36370E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.81344E+12 ;
CS137_ACTIVITY            (idx, 1)        =  8.09969E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.14538E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.46482E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.82075E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.23226E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.46426E+12 0.00048  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 8 ;
BURNUP                     (idx, [1:  2])  = [  6.00000E+00  6.00054E+00 ];
BURN_DAYS                 (idx, 1)        =  1.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.60075E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  9.55855E+15 0.00076  6.94344E-01 0.00047 ];
U238_FISS                 (idx, [1:   4]) = [  9.64296E+14 0.00264  7.00399E-02 0.00248 ];
PU239_FISS                (idx, [1:   4]) = [  3.09292E+15 0.00148  2.24674E-01 0.00136 ];
PU240_FISS                (idx, [1:   4]) = [  2.11589E+12 0.05585  1.53510E-04 0.05589 ];
PU241_FISS                (idx, [1:   4]) = [  1.42564E+14 0.00707  1.03560E-02 0.00705 ];
U235_CAPT                 (idx, [1:   4]) = [  2.09918E+15 0.00179  1.01784E-01 0.00170 ];
U238_CAPT                 (idx, [1:   4]) = [  8.61644E+15 0.00099  4.17771E-01 0.00070 ];
PU239_CAPT                (idx, [1:   4]) = [  1.72581E+15 0.00199  8.36807E-02 0.00192 ];
PU240_CAPT                (idx, [1:   4]) = [  6.66229E+14 0.00335  3.23042E-02 0.00330 ];
PU241_CAPT                (idx, [1:   4]) = [  5.18428E+13 0.01233  2.51408E-03 0.01233 ];
XE135_CAPT                (idx, [1:   4]) = [  7.41603E+14 0.00314  3.59613E-02 0.00314 ];
SM149_CAPT                (idx, [1:   4]) = [  1.79965E+14 0.00617  8.72813E-03 0.00622 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000925 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.65455E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000925 5.00765E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2972591 2.97671E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1984381 1.98699E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 43953 4.39554E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000925 5.00765E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 1.86265E-09 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50058E+05 5.4E-09  4.50058E+05 5.4E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.53360E+16 1.8E-05  3.53360E+16 1.8E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37729E+16 3.2E-06  1.37729E+16 3.2E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.06031E+16 0.00045  1.49688E+16 0.00046  5.63436E+15 0.00114 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.43760E+16 0.00027  2.87416E+16 0.00024  5.63436E+15 0.00114 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.46426E+16 0.00048  3.46426E+16 0.00048  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.65032E+18 0.00048  4.39823E+17 0.00045  1.21050E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.04579E+14 0.00488 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.46806E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.24450E+18 0.00063 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11816E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11816E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.61125E+00 0.00048 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.77441E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.63536E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23763E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94302E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96889E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.02863E+00 0.00056 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.01959E+00 0.00056 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.56562E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03955E+02 3.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.01948E+00 0.00058  1.01334E+00 0.00056  6.24594E-03 0.00927 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.02050E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.02013E+00 0.00048 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.02050E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.02955E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73243E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73233E+01 8.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.00471E-07 0.00350 ];
IMP_EALF                  (idx, [1:   2]) = [  5.99570E-07 0.00150 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.37605E-01 0.00282 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.37157E-01 0.00122 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.98635E-03 0.00595  1.61910E-04 0.03614  1.02717E-03 0.01294  9.42554E-04 0.01493  2.67031E-03 0.00889  9.02709E-04 0.01429  2.81701E-04 0.02572 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.88964E-01 0.01333  9.89978E-03 0.02294  3.13055E-02 0.00038  1.10215E-01 0.00036  3.21660E-01 0.00027  1.33589E+00 0.00073  8.39312E+00 0.01170 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.07187E-03 0.00839  1.57935E-04 0.05212  1.04189E-03 0.02051  9.59073E-04 0.02103  2.72326E-03 0.01273  9.20366E-04 0.02213  2.69349E-04 0.03988 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.68799E-01 0.01991  1.24986E-02 0.00036  3.12846E-02 0.00053  1.10143E-01 0.00051  3.21772E-01 0.00039  1.33580E+00 0.00105  8.94348E+00 0.00427 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.63769E-05 0.00128  2.63665E-05 0.00128  2.78786E-05 0.01341 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.68860E-05 0.00113  2.68754E-05 0.00113  2.84202E-05 0.01342 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.11588E-03 0.00937  1.67391E-04 0.05512  1.04624E-03 0.02179  9.83161E-04 0.02268  2.72871E-03 0.01359  8.98390E-04 0.02357  2.91987E-04 0.04094 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.87203E-01 0.02177  1.25012E-02 0.00053  3.12982E-02 0.00067  1.10213E-01 0.00063  3.21775E-01 0.00042  1.33416E+00 0.00138  8.90559E+00 0.00525 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.60811E-05 0.00290  2.60659E-05 0.00291  2.63004E-05 0.03325 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.65846E-05 0.00284  2.65691E-05 0.00285  2.68162E-05 0.03327 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.08848E-03 0.02892  1.55507E-04 0.18776  1.02250E-03 0.07288  1.08559E-03 0.07100  2.70758E-03 0.04487  8.48701E-04 0.07685  2.68602E-04 0.13954 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.13702E-01 0.06428  1.24894E-02 4.4E-05  3.12582E-02 0.00154  1.10148E-01 0.00136  3.21117E-01 0.00118  1.32906E+00 0.00371  8.74089E+00 0.01508 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.08480E-03 0.02806  1.59919E-04 0.18693  1.01788E-03 0.06985  1.06929E-03 0.06980  2.70177E-03 0.04358  8.60929E-04 0.07566  2.75014E-04 0.13186 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.34553E-01 0.06411  1.24894E-02 4.4E-05  3.12633E-02 0.00152  1.10163E-01 0.00136  3.21160E-01 0.00117  1.32920E+00 0.00369  8.74790E+00 0.01508 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.34879E+02 0.02927 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.61978E-05 0.00084 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.67035E-05 0.00059 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.04539E-03 0.00590 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.30843E+02 0.00597 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.99991E-07 0.00069 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.80044E-06 0.00042  2.80019E-06 0.00042  2.84150E-06 0.00563 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.99866E-05 0.00083  4.00107E-05 0.00084  3.62129E-05 0.00974 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.61212E-01 0.00032  6.61022E-01 0.00033  7.05424E-01 0.00934 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05463E+01 0.01270 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.58736E+01 0.00047  3.43447E+01 0.00046 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.54554E+04 0.00331  2.98224E+05 0.00118  6.07729E+05 0.00087  6.51663E+05 0.00078  5.98097E+05 0.00078  6.41558E+05 0.00083  4.35305E+05 0.00058  3.85336E+05 0.00079  2.94349E+05 0.00083  2.40912E+05 0.00093  2.07753E+05 0.00076  1.87068E+05 0.00075  1.72886E+05 0.00079  1.64084E+05 0.00114  1.59891E+05 0.00064  1.38193E+05 0.00099  1.36559E+05 0.00092  1.35384E+05 0.00095  1.33211E+05 0.00073  2.59820E+05 0.00048  2.51017E+05 0.00057  1.80933E+05 0.00084  1.17349E+05 0.00091  1.35645E+05 0.00093  1.28754E+05 0.00102  1.15956E+05 0.00131  1.89562E+05 0.00078  4.35437E+04 0.00136  5.48033E+04 0.00141  4.97214E+04 0.00136  2.89692E+04 0.00199  5.01835E+04 0.00156  3.38590E+04 0.00165  2.84750E+04 0.00175  5.28554E+03 0.00310  4.95390E+03 0.00387  4.66619E+03 0.00311  4.57608E+03 0.00267  4.66002E+03 0.00376  4.89113E+03 0.00325  5.42074E+03 0.00339  5.15669E+03 0.00453  9.93318E+03 0.00231  1.59000E+04 0.00215  2.05015E+04 0.00211  5.44319E+04 0.00151  5.72734E+04 0.00155  6.18084E+04 0.00136  4.10734E+04 0.00185  2.97382E+04 0.00176  2.23335E+04 0.00203  2.64280E+04 0.00149  5.13446E+04 0.00158  7.17956E+04 0.00104  1.40938E+05 0.00110  2.20522E+05 0.00094  3.31505E+05 0.00114  2.12975E+05 0.00127  1.52717E+05 0.00117  1.09923E+05 0.00101  9.88232E+04 0.00138  9.74311E+04 0.00125  8.13219E+04 0.00135  5.51104E+04 0.00142  5.09098E+04 0.00144  4.53744E+04 0.00139  3.83205E+04 0.00146  3.01787E+04 0.00155  2.02106E+04 0.00124  7.16941E+03 0.00185 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.02918E+00 0.00054 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.29435E+18 0.00050  3.55992E+17 0.00110 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38139E-01 0.00012  1.53928E+00 0.00038 ];
INF_CAPT                  (idx, [1:   4]) = [  6.95558E-03 0.00052  3.25881E-02 0.00053 ];
INF_ABS                   (idx, [1:   4]) = [  8.96117E-03 0.00044  6.39899E-02 0.00082 ];
INF_FISS                  (idx, [1:   4]) = [  2.00559E-03 0.00055  3.14018E-02 0.00113 ];
INF_NSF                   (idx, [1:   4]) = [  5.24566E-03 0.00056  8.02010E-02 0.00115 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.61552E+00 7.3E-05  2.55403E+00 3.6E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04518E+02 7.8E-06  2.03824E+02 6.1E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.79591E-08 0.00047  2.57090E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29172E-01 0.00012  1.47528E+00 0.00043 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44273E-01 0.00022  3.90872E-01 0.00051 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63649E-02 0.00024  9.25089E-02 0.00102 ];
INF_SCATT3                (idx, [1:   4]) = [  7.37039E-03 0.00341  2.77664E-02 0.00284 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03157E-02 0.00221 -8.73719E-03 0.00663 ];
INF_SCATT5                (idx, [1:   4]) = [  1.45598E-04 0.12412  6.46811E-03 0.00791 ];
INF_SCATT6                (idx, [1:   4]) = [  5.09684E-03 0.00278 -1.67324E-02 0.00254 ];
INF_SCATT7                (idx, [1:   4]) = [  7.38133E-04 0.01468  4.43097E-04 0.09766 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29213E-01 0.00012  1.47528E+00 0.00043 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44273E-01 0.00022  3.90872E-01 0.00051 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63650E-02 0.00024  9.25089E-02 0.00102 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.37053E-03 0.00342  2.77664E-02 0.00284 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03158E-02 0.00221 -8.73719E-03 0.00663 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.45416E-04 0.12390  6.46811E-03 0.00791 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.09689E-03 0.00279 -1.67324E-02 0.00254 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.38241E-04 0.01468  4.43097E-04 0.09766 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12775E-01 0.00029  9.95832E-01 0.00033 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56661E+00 0.00029  3.34729E-01 0.00033 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.92019E-03 0.00044  6.39899E-02 0.00082 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69649E-02 0.00021  6.51386E-02 0.00092 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11174E-01 0.00012  1.79978E-02 0.00038  1.13446E-03 0.00374  1.47414E+00 0.00043 ];
INF_S1                    (idx, [1:   8]) = [  2.39032E-01 0.00021  5.24071E-03 0.00080  4.87196E-04 0.00485  3.90384E-01 0.00051 ];
INF_S2                    (idx, [1:   8]) = [  9.79471E-02 0.00023 -1.58222E-03 0.00287  2.61716E-04 0.00833  9.22472E-02 0.00102 ];
INF_S3                    (idx, [1:   8]) = [  9.22997E-03 0.00274 -1.85958E-03 0.00156  9.67414E-05 0.01903  2.76697E-02 0.00285 ];
INF_S4                    (idx, [1:   8]) = [ -9.71344E-03 0.00236 -6.02250E-04 0.00446  1.92229E-06 0.90574 -8.73911E-03 0.00664 ];
INF_S5                    (idx, [1:   8]) = [  1.18076E-04 0.14492  2.75223E-05 0.09264 -3.87630E-05 0.04377  6.50688E-03 0.00785 ];
INF_S6                    (idx, [1:   8]) = [  5.24249E-03 0.00262 -1.45652E-04 0.01657 -4.85513E-05 0.03447 -1.66839E-02 0.00252 ];
INF_S7                    (idx, [1:   8]) = [  9.09201E-04 0.01195 -1.71068E-04 0.01177 -4.37496E-05 0.02401  4.86847E-04 0.08893 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11215E-01 0.00012  1.79978E-02 0.00038  1.13446E-03 0.00374  1.47414E+00 0.00043 ];
INF_SP1                   (idx, [1:   8]) = [  2.39033E-01 0.00021  5.24071E-03 0.00080  4.87196E-04 0.00485  3.90384E-01 0.00051 ];
INF_SP2                   (idx, [1:   8]) = [  9.79472E-02 0.00023 -1.58222E-03 0.00287  2.61716E-04 0.00833  9.22472E-02 0.00102 ];
INF_SP3                   (idx, [1:   8]) = [  9.23011E-03 0.00274 -1.85958E-03 0.00156  9.67414E-05 0.01903  2.76697E-02 0.00285 ];
INF_SP4                   (idx, [1:   8]) = [ -9.71358E-03 0.00236 -6.02250E-04 0.00446  1.92229E-06 0.90574 -8.73911E-03 0.00664 ];
INF_SP5                   (idx, [1:   8]) = [  1.17894E-04 0.14465  2.75223E-05 0.09264 -3.87630E-05 0.04377  6.50688E-03 0.00785 ];
INF_SP6                   (idx, [1:   8]) = [  5.24254E-03 0.00263 -1.45652E-04 0.01657 -4.85513E-05 0.03447 -1.66839E-02 0.00252 ];
INF_SP7                   (idx, [1:   8]) = [  9.09310E-04 0.01195 -1.71068E-04 0.01177 -4.37496E-05 0.02401  4.86847E-04 0.08893 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31943E-01 0.00065  1.15102E+00 0.00594 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33869E-01 0.00071  1.23653E+00 0.00842 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33289E-01 0.00070  1.24210E+00 0.00789 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28746E-01 0.00119  1.00879E+00 0.00620 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43715E+00 0.00065  2.89840E-01 0.00588 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42531E+00 0.00071  2.70029E-01 0.00839 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42886E+00 0.00070  2.68761E-01 0.00780 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45727E+00 0.00119  3.30730E-01 0.00614 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.07187E-03 0.00839  1.57935E-04 0.05212  1.04189E-03 0.02051  9.59073E-04 0.02103  2.72326E-03 0.01273  9.20366E-04 0.02213  2.69349E-04 0.03988 ];
LAMBDA                    (idx, [1:  14]) = [  7.68799E-01 0.01991  1.24986E-02 0.00036  3.12846E-02 0.00053  1.10143E-01 0.00051  3.21772E-01 0.00039  1.33580E+00 0.00105  8.94348E+00 0.00427 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0003' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:41 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:40:12 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284841 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.98014E-01  9.99007E-01  1.00304E+00  1.00151E+00  9.98424E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12815E-02 0.00108  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88719E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04400E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04824E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67762E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.57705E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.57616E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.67195E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.18643E-01 0.00108  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000808 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00016E+04 0.00083 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00016E+04 0.00083 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.95606E+02 ;
RUNNING_TIME              (idx, 1)        =  5.95208E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.31417E-01  4.31417E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.51150E-01  9.03333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  5.84381E+01  3.39250E+00  2.66182E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  4.95267E-01  2.77833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.84500E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  5.95206E+01  1.26963E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96643 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00008E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78407E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.35949E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.79340E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.90722E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.75012E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.22160E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.60934E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.67122E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  9.57702E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.33475E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.98212E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.33227E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.59490E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.60152E+07 ;
SR90_ACTIVITY             (idx, 1)        =  8.02119E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.27193E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.33243E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.39168E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.85772E+12 ;
CS137_ACTIVITY            (idx, 1)        =  9.44786E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.16810E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.44466E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.65876E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.23942E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.51374E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 9 ;
BURNUP                     (idx, [1:  2])  = [  7.00000E+00  7.00062E+00 ];
BURN_DAYS                 (idx, 1)        =  1.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.70086E-01 0.00110 ];
U235_FISS                 (idx, [1:   4]) = [  9.17307E+15 0.00077  6.66422E-01 0.00051 ];
U238_FISS                 (idx, [1:   4]) = [  9.75864E+14 0.00266  7.08882E-02 0.00250 ];
PU239_FISS                (idx, [1:   4]) = [  3.41053E+15 0.00144  2.47766E-01 0.00125 ];
PU240_FISS                (idx, [1:   4]) = [  2.46285E+12 0.05378  1.78866E-04 0.05372 ];
PU241_FISS                (idx, [1:   4]) = [  1.95963E+14 0.00588  1.42351E-02 0.00582 ];
U235_CAPT                 (idx, [1:   4]) = [  2.01890E+15 0.00198  9.56278E-02 0.00186 ];
U238_CAPT                 (idx, [1:   4]) = [  8.70249E+15 0.00105  4.12193E-01 0.00072 ];
PU239_CAPT                (idx, [1:   4]) = [  1.89828E+15 0.00193  8.99222E-02 0.00190 ];
PU240_CAPT                (idx, [1:   4]) = [  8.01459E+14 0.00310  3.79612E-02 0.00301 ];
PU241_CAPT                (idx, [1:   4]) = [  7.02203E+13 0.00952  3.32623E-03 0.00951 ];
XE135_CAPT                (idx, [1:   4]) = [  7.39114E+14 0.00308  3.50119E-02 0.00306 ];
SM149_CAPT                (idx, [1:   4]) = [  1.84516E+14 0.00627  8.74093E-03 0.00626 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000808 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.63004E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000808 5.00763E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3000037 3.00417E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1956129 1.95881E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44642 4.46477E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000808 5.00763E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.09782E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50058E+05 5.4E-09  4.50058E+05 5.4E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.54762E+16 1.9E-05  3.54762E+16 1.9E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37615E+16 3.4E-06  1.37615E+16 3.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.11185E+16 0.00047  1.54381E+16 0.00047  5.68040E+15 0.00113 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.48800E+16 0.00029  2.91996E+16 0.00025  5.68040E+15 0.00113 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.51374E+16 0.00052  3.51374E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.67030E+18 0.00050  4.44805E+17 0.00045  1.22549E+18 0.00056 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.13822E+14 0.00495 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.51938E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.25832E+18 0.00065 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11700E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11700E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.60185E+00 0.00049 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.78545E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.60527E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23711E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94206E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96846E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.01901E+00 0.00057 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.00991E+00 0.00057 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.57793E+00 2.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04123E+02 3.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.00985E+00 0.00059  1.00400E+00 0.00057  5.90975E-03 0.00959 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.00960E+00 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  1.00978E+00 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.00960E+00 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  1.01870E+00 0.00029 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73023E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73022E+01 8.4E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.13923E-07 0.00360 ];
IMP_EALF                  (idx, [1:   2]) = [  6.12385E-07 0.00146 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.39769E-01 0.00284 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.40150E-01 0.00118 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.94767E-03 0.00617  1.61271E-04 0.03619  9.93386E-04 0.01397  9.54923E-04 0.01452  2.66926E-03 0.00888  8.91574E-04 0.01528  2.77257E-04 0.02780 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.85170E-01 0.01386  9.75549E-03 0.02378  3.12819E-02 0.00040  1.10317E-01 0.00041  3.21747E-01 0.00027  1.33187E+00 0.00103  8.51727E+00 0.01116 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.95370E-03 0.00898  1.64799E-04 0.05751  1.00602E-03 0.02095  9.59977E-04 0.02212  2.65310E-03 0.01328  8.95991E-04 0.02302  2.73807E-04 0.04159 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.76374E-01 0.02080  1.25058E-02 0.00047  3.12933E-02 0.00054  1.10345E-01 0.00058  3.21784E-01 0.00041  1.33280E+00 0.00121  9.02343E+00 0.00427 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.64053E-05 0.00131  2.63969E-05 0.00131  2.78879E-05 0.01306 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.66608E-05 0.00116  2.66524E-05 0.00116  2.81590E-05 0.01305 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.84589E-03 0.00979  1.56170E-04 0.05885  1.00613E-03 0.02253  9.38967E-04 0.02424  2.59727E-03 0.01370  8.60935E-04 0.02536  2.86413E-04 0.04490 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.97350E-01 0.02284  1.25013E-02 0.00057  3.12834E-02 0.00069  1.10276E-01 0.00069  3.21889E-01 0.00047  1.33458E+00 0.00123  9.01503E+00 0.00505 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.61325E-05 0.00306  2.61300E-05 0.00307  2.68561E-05 0.03186 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.63856E-05 0.00300  2.63831E-05 0.00301  2.71230E-05 0.03190 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.78384E-03 0.03008  1.44614E-04 0.19555  9.65969E-04 0.06937  8.97240E-04 0.07900  2.66274E-03 0.04562  7.74090E-04 0.08972  3.39181E-04 0.14783 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.72930E-01 0.07755  1.24894E-02 4.3E-05  3.13120E-02 0.00154  1.10497E-01 0.00168  3.22037E-01 0.00132  1.32235E+00 0.00486  9.04453E+00 0.00769 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.79019E-03 0.02936  1.55135E-04 0.19210  9.71008E-04 0.06621  8.93056E-04 0.07780  2.68372E-03 0.04422  7.56109E-04 0.08853  3.31160E-04 0.14024 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.69290E-01 0.07661  1.24894E-02 4.3E-05  3.13109E-02 0.00154  1.10494E-01 0.00167  3.22029E-01 0.00129  1.32263E+00 0.00476  9.04347E+00 0.00769 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.23070E+02 0.03072 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.62620E-05 0.00082 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.65163E-05 0.00057 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.85172E-03 0.00558 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.22914E+02 0.00568 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.96673E-07 0.00068 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.78571E-06 0.00041  2.78555E-06 0.00041  2.80970E-06 0.00510 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.98457E-05 0.00088  3.98690E-05 0.00088  3.61307E-05 0.00954 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.58172E-01 0.00034  6.58043E-01 0.00034  6.93436E-01 0.00947 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05080E+01 0.01418 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.57616E+01 0.00046  3.42284E+01 0.00047 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.55894E+04 0.00277  2.97999E+05 0.00105  6.08353E+05 0.00067  6.50827E+05 0.00070  5.98397E+05 0.00076  6.41374E+05 0.00060  4.35271E+05 0.00059  3.85114E+05 0.00109  2.94889E+05 0.00080  2.40945E+05 0.00095  2.07632E+05 0.00070  1.87486E+05 0.00079  1.72790E+05 0.00088  1.64230E+05 0.00089  1.60383E+05 0.00086  1.38414E+05 0.00082  1.36596E+05 0.00098  1.35342E+05 0.00111  1.33044E+05 0.00104  2.59655E+05 0.00072  2.50844E+05 0.00080  1.81091E+05 0.00101  1.17252E+05 0.00102  1.35591E+05 0.00077  1.28518E+05 0.00108  1.15773E+05 0.00101  1.89347E+05 0.00105  4.35672E+04 0.00158  5.45308E+04 0.00134  4.95298E+04 0.00130  2.88590E+04 0.00193  5.00085E+04 0.00116  3.37947E+04 0.00187  2.84513E+04 0.00185  5.20067E+03 0.00316  4.85950E+03 0.00357  4.51259E+03 0.00296  4.40310E+03 0.00230  4.50841E+03 0.00289  4.77737E+03 0.00278  5.30699E+03 0.00290  5.11247E+03 0.00397  9.86583E+03 0.00375  1.59297E+04 0.00207  2.04115E+04 0.00261  5.41652E+04 0.00166  5.69537E+04 0.00121  6.12820E+04 0.00151  4.04750E+04 0.00166  2.92122E+04 0.00164  2.19941E+04 0.00153  2.60294E+04 0.00162  5.05899E+04 0.00120  7.06159E+04 0.00149  1.39198E+05 0.00143  2.18291E+05 0.00106  3.28745E+05 0.00132  2.11232E+05 0.00124  1.51667E+05 0.00109  1.09222E+05 0.00121  9.81714E+04 0.00129  9.67684E+04 0.00148  8.07972E+04 0.00163  5.47023E+04 0.00143  5.05745E+04 0.00170  4.49694E+04 0.00139  3.80929E+04 0.00135  3.00469E+04 0.00167  2.01333E+04 0.00205  7.13635E+03 0.00236 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.01888E+00 0.00059 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.31247E+18 0.00042  3.57857E+17 0.00126 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38269E-01 9.0E-05  1.54217E+00 0.00037 ];
INF_CAPT                  (idx, [1:   4]) = [  7.07059E-03 0.00056  3.30850E-02 0.00063 ];
INF_ABS                   (idx, [1:   4]) = [  9.04529E-03 0.00045  6.43040E-02 0.00095 ];
INF_FISS                  (idx, [1:   4]) = [  1.97470E-03 0.00050  3.12190E-02 0.00129 ];
INF_NSF                   (idx, [1:   4]) = [  5.17928E-03 0.00050  8.01552E-02 0.00131 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.62282E+00 5.9E-05  2.56751E+00 4.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04617E+02 7.2E-06  2.04009E+02 6.9E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.77660E-08 0.00039  2.57329E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29221E-01 9.2E-05  1.47789E+00 0.00042 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44279E-01 0.00018  3.91425E-01 0.00051 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63428E-02 0.00026  9.25092E-02 0.00090 ];
INF_SCATT3                (idx, [1:   4]) = [  7.33012E-03 0.00316  2.76983E-02 0.00292 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03509E-02 0.00185 -8.69771E-03 0.00668 ];
INF_SCATT5                (idx, [1:   4]) = [  1.30496E-04 0.14812  6.58635E-03 0.00857 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07238E-03 0.00368 -1.67833E-02 0.00278 ];
INF_SCATT7                (idx, [1:   4]) = [  7.34037E-04 0.02034  4.12371E-04 0.10464 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29262E-01 9.2E-05  1.47789E+00 0.00042 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44280E-01 0.00018  3.91425E-01 0.00051 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63432E-02 0.00026  9.25092E-02 0.00090 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32981E-03 0.00315  2.76983E-02 0.00292 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03507E-02 0.00185 -8.69771E-03 0.00668 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.30559E-04 0.14805  6.58635E-03 0.00857 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07228E-03 0.00366 -1.67833E-02 0.00278 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.34021E-04 0.02038  4.12371E-04 0.10464 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12802E-01 0.00026  9.98466E-01 0.00029 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56641E+00 0.00026  3.33846E-01 0.00029 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.00444E-03 0.00046  6.43040E-02 0.00095 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69715E-02 0.00025  6.54296E-02 0.00102 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11298E-01 8.9E-05  1.79234E-02 0.00035  1.15110E-03 0.00451  1.47674E+00 0.00042 ];
INF_S1                    (idx, [1:   8]) = [  2.39057E-01 0.00018  5.22139E-03 0.00072  4.94976E-04 0.00418  3.90930E-01 0.00051 ];
INF_S2                    (idx, [1:   8]) = [  9.79211E-02 0.00024 -1.57827E-03 0.00224  2.65548E-04 0.00801  9.22437E-02 0.00090 ];
INF_S3                    (idx, [1:   8]) = [  9.17749E-03 0.00235 -1.84736E-03 0.00223  9.51003E-05 0.01845  2.76032E-02 0.00292 ];
INF_S4                    (idx, [1:   8]) = [ -9.75346E-03 0.00195 -5.97443E-04 0.00460 -2.39188E-06 0.66060 -8.69532E-03 0.00670 ];
INF_S5                    (idx, [1:   8]) = [  1.04712E-04 0.17627  2.57839E-05 0.09681 -4.06517E-05 0.03402  6.62700E-03 0.00850 ];
INF_S6                    (idx, [1:   8]) = [  5.21505E-03 0.00340 -1.42670E-04 0.01699 -5.03197E-05 0.03305 -1.67330E-02 0.00282 ];
INF_S7                    (idx, [1:   8]) = [  9.07566E-04 0.01523 -1.73530E-04 0.01399 -4.43120E-05 0.03517  4.56683E-04 0.09354 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11339E-01 8.9E-05  1.79234E-02 0.00035  1.15110E-03 0.00451  1.47674E+00 0.00042 ];
INF_SP1                   (idx, [1:   8]) = [  2.39058E-01 0.00018  5.22139E-03 0.00072  4.94976E-04 0.00418  3.90930E-01 0.00051 ];
INF_SP2                   (idx, [1:   8]) = [  9.79215E-02 0.00024 -1.57827E-03 0.00224  2.65548E-04 0.00801  9.22437E-02 0.00090 ];
INF_SP3                   (idx, [1:   8]) = [  9.17718E-03 0.00235 -1.84736E-03 0.00223  9.51003E-05 0.01845  2.76032E-02 0.00292 ];
INF_SP4                   (idx, [1:   8]) = [ -9.75329E-03 0.00196 -5.97443E-04 0.00460 -2.39188E-06 0.66060 -8.69532E-03 0.00670 ];
INF_SP5                   (idx, [1:   8]) = [  1.04775E-04 0.17612  2.57839E-05 0.09681 -4.06517E-05 0.03402  6.62700E-03 0.00850 ];
INF_SP6                   (idx, [1:   8]) = [  5.21495E-03 0.00339 -1.42670E-04 0.01699 -5.03197E-05 0.03305 -1.67330E-02 0.00282 ];
INF_SP7                   (idx, [1:   8]) = [  9.07551E-04 0.01527 -1.73530E-04 0.01399 -4.43120E-05 0.03517  4.56683E-04 0.09354 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32166E-01 0.00058  1.15331E+00 0.00706 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33593E-01 0.00087  1.24410E+00 0.00918 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34043E-01 0.00101  1.23804E+00 0.00826 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28938E-01 0.00081  1.01205E+00 0.00859 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43577E+00 0.00058  2.89362E-01 0.00695 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42701E+00 0.00087  2.68446E-01 0.00875 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42427E+00 0.00100  2.69689E-01 0.00833 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45602E+00 0.00081  3.29953E-01 0.00867 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.95370E-03 0.00898  1.64799E-04 0.05751  1.00602E-03 0.02095  9.59977E-04 0.02212  2.65310E-03 0.01328  8.95991E-04 0.02302  2.73807E-04 0.04159 ];
LAMBDA                    (idx, [1:  14]) = [  7.76374E-01 0.02080  1.25058E-02 0.00047  3.12933E-02 0.00054  1.10345E-01 0.00058  3.21784E-01 0.00041  1.33280E+00 0.00121  9.02343E+00 0.00427 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0003' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:41 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:46:19 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284841 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.96380E-01  1.00024E+00  1.00292E+00  1.00103E+00  9.99421E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13096E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88690E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04701E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05126E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67653E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.56775E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.56686E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.63268E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.18122E-01 0.00111  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000842 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00017E+04 0.00078 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00017E+04 0.00078 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.26130E+02 ;
RUNNING_TIME              (idx, 1)        =  6.56321E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.31417E-01  4.31417E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.69883E-01  9.05000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.44742E+01  3.38397E+00  2.65205E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.51233E-01  2.73500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.11500E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  6.56319E+01  1.26805E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96906 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99994E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79043E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.38793E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.78529E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.66772E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.77031E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.23570E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61759E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.66169E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.06167E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.38682E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.75925E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.43556E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.85747E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.64326E+07 ;
SR90_ACTIVITY             (idx, 1)        =  9.04536E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.29107E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.35861E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.41763E+14 ;
CS134_ACTIVITY            (idx, 1)        =  5.05136E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.07951E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.19183E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.42597E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.81947E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.24672E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.56115E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 10 ;
BURNUP                     (idx, [1:  2])  = [  8.00000E+00  8.00071E+00 ];
BURN_DAYS                 (idx, 1)        =  2.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.80544E-01 0.00105 ];
U235_FISS                 (idx, [1:   4]) = [  8.81668E+15 0.00077  6.41068E-01 0.00051 ];
U238_FISS                 (idx, [1:   4]) = [  9.88788E+14 0.00272  7.18862E-02 0.00256 ];
PU239_FISS                (idx, [1:   4]) = [  3.68339E+15 0.00127  2.67826E-01 0.00115 ];
PU240_FISS                (idx, [1:   4]) = [  3.01040E+12 0.04452  2.18930E-04 0.04451 ];
PU241_FISS                (idx, [1:   4]) = [  2.54084E+14 0.00510  1.84730E-02 0.00502 ];
U235_CAPT                 (idx, [1:   4]) = [  1.93599E+15 0.00187  8.96498E-02 0.00182 ];
U238_CAPT                 (idx, [1:   4]) = [  8.78465E+15 0.00101  4.06760E-01 0.00073 ];
PU239_CAPT                (idx, [1:   4]) = [  2.04050E+15 0.00194  9.44849E-02 0.00184 ];
PU240_CAPT                (idx, [1:   4]) = [  9.27662E+14 0.00290  4.29525E-02 0.00280 ];
PU241_CAPT                (idx, [1:   4]) = [  9.35323E+13 0.00822  4.33185E-03 0.00827 ];
XE135_CAPT                (idx, [1:   4]) = [  7.47141E+14 0.00323  3.45994E-02 0.00323 ];
SM149_CAPT                (idx, [1:   4]) = [  1.89393E+14 0.00616  8.77014E-03 0.00614 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000842 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.71467E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000842 5.00771E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3027933 3.03216E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1928427 1.93107E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44482 4.44820E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000842 5.00771E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.98492E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50058E+05 5.4E-09  4.50058E+05 5.4E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.56071E+16 1.9E-05  3.56071E+16 1.9E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37508E+16 3.5E-06  1.37508E+16 3.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.16023E+16 0.00043  1.58760E+16 0.00045  5.72637E+15 0.00111 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.53532E+16 0.00026  2.96268E+16 0.00024  5.72637E+15 0.00111 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.56115E+16 0.00049  3.56115E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.69032E+18 0.00046  4.49958E+17 0.00043  1.24037E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.16828E+14 0.00474 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.56700E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.27197E+18 0.00061 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11583E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11583E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.59265E+00 0.00046 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.78557E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.57742E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23720E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94214E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96872E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.00896E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.99985E-01 0.00052 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.58945E+00 2.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04281E+02 3.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.99786E-01 0.00054  9.94179E-01 0.00053  5.80549E-03 0.00934 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.99839E-01 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  9.99995E-01 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.99839E-01 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.00881E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72755E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72757E+01 8.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.30647E-07 0.00361 ];
IMP_EALF                  (idx, [1:   2]) = [  6.28815E-07 0.00152 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.43896E-01 0.00279 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.44499E-01 0.00114 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.90857E-03 0.00608  1.66474E-04 0.03613  9.79653E-04 0.01376  9.26455E-04 0.01550  2.61640E-03 0.00888  9.30892E-04 0.01426  2.88696E-04 0.02618 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.09424E-01 0.01384  1.00274E-02 0.02225  3.12145E-02 0.00039  1.10310E-01 0.00041  3.21939E-01 0.00029  1.33108E+00 0.00093  8.32704E+00 0.01272 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.87337E-03 0.00856  1.56199E-04 0.05349  9.81458E-04 0.02140  9.14430E-04 0.02186  2.59809E-03 0.01327  9.34627E-04 0.02180  2.88572E-04 0.04065 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.20556E-01 0.02152  1.24988E-02 0.00031  3.11914E-02 0.00059  1.10361E-01 0.00060  3.21909E-01 0.00041  1.33137E+00 0.00117  8.94735E+00 0.00493 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.64889E-05 0.00127  2.64777E-05 0.00126  2.84918E-05 0.01437 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.64794E-05 0.00114  2.64682E-05 0.00114  2.84681E-05 0.01426 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.80713E-03 0.00944  1.56483E-04 0.05935  9.47459E-04 0.02301  9.24895E-04 0.02529  2.56887E-03 0.01396  9.28943E-04 0.02377  2.80472E-04 0.04439 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.04160E-01 0.02270  1.24978E-02 0.00045  3.11755E-02 0.00075  1.10354E-01 0.00077  3.21936E-01 0.00048  1.32861E+00 0.00194  8.89857E+00 0.00743 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.61877E-05 0.00277  2.61704E-05 0.00277  2.80502E-05 0.04214 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.61794E-05 0.00275  2.61620E-05 0.00275  2.80414E-05 0.04205 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.73395E-03 0.03236  1.80438E-04 0.19315  1.00376E-03 0.07678  9.13680E-04 0.07756  2.43827E-03 0.04825  9.47616E-04 0.08608  2.50185E-04 0.15341 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.29184E-01 0.07245  1.24896E-02 4.3E-05  3.12106E-02 0.00173  1.10665E-01 0.00174  3.22202E-01 0.00137  1.32758E+00 0.00401  8.69668E+00 0.02298 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.72001E-03 0.03126  1.79604E-04 0.18620  9.87044E-04 0.07598  9.08271E-04 0.07515  2.45217E-03 0.04714  9.43787E-04 0.08603  2.49134E-04 0.14937 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.22506E-01 0.07112  1.24896E-02 4.3E-05  3.12142E-02 0.00172  1.10655E-01 0.00174  3.22237E-01 0.00137  1.32744E+00 0.00404  8.69517E+00 0.02297 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.19590E+02 0.03259 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.63354E-05 0.00074 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.63260E-05 0.00051 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.84009E-03 0.00551 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.21795E+02 0.00552 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.93313E-07 0.00071 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.77725E-06 0.00045  2.77720E-06 0.00045  2.78575E-06 0.00562 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.97120E-05 0.00088  3.97336E-05 0.00089  3.61206E-05 0.00992 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.55434E-01 0.00035  6.55344E-01 0.00035  6.84856E-01 0.01004 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04051E+01 0.01415 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.56686E+01 0.00048  3.41123E+01 0.00047 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.72771E+04 0.00253  2.98693E+05 0.00133  6.08421E+05 0.00093  6.51307E+05 0.00076  5.98284E+05 0.00076  6.41477E+05 0.00074  4.35737E+05 0.00050  3.85292E+05 0.00089  2.95191E+05 0.00086  2.40926E+05 0.00080  2.07787E+05 0.00083  1.87246E+05 0.00071  1.72766E+05 0.00077  1.64293E+05 0.00092  1.60007E+05 0.00081  1.38620E+05 0.00084  1.36610E+05 0.00107  1.35280E+05 0.00097  1.32989E+05 0.00087  2.59766E+05 0.00059  2.51089E+05 0.00073  1.81048E+05 0.00078  1.17499E+05 0.00109  1.35808E+05 0.00088  1.28872E+05 0.00098  1.15669E+05 0.00113  1.89120E+05 0.00077  4.34763E+04 0.00179  5.46198E+04 0.00173  4.95612E+04 0.00179  2.87940E+04 0.00190  4.98365E+04 0.00131  3.38331E+04 0.00176  2.84052E+04 0.00173  5.16827E+03 0.00386  4.80190E+03 0.00390  4.38706E+03 0.00337  4.23566E+03 0.00349  4.29654E+03 0.00354  4.62962E+03 0.00316  5.17707E+03 0.00415  5.03390E+03 0.00335  9.77818E+03 0.00308  1.58257E+04 0.00290  2.03130E+04 0.00214  5.39096E+04 0.00149  5.66545E+04 0.00196  6.08439E+04 0.00148  4.00798E+04 0.00161  2.88487E+04 0.00159  2.17099E+04 0.00129  2.55788E+04 0.00182  4.98433E+04 0.00143  6.98281E+04 0.00105  1.37942E+05 0.00105  2.16085E+05 0.00115  3.25901E+05 0.00101  2.09570E+05 0.00113  1.50524E+05 0.00119  1.08494E+05 0.00109  9.74171E+04 0.00115  9.61065E+04 0.00107  8.03084E+04 0.00112  5.44269E+04 0.00124  5.03220E+04 0.00118  4.48537E+04 0.00128  3.78884E+04 0.00137  2.98522E+04 0.00180  2.00264E+04 0.00173  7.08829E+03 0.00246 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.00897E+00 0.00056 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.33065E+18 0.00051  3.59709E+17 0.00083 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38270E-01 0.00011  1.54499E+00 0.00025 ];
INF_CAPT                  (idx, [1:   4]) = [  7.17082E-03 0.00065  3.35315E-02 0.00044 ];
INF_ABS                   (idx, [1:   4]) = [  9.12095E-03 0.00055  6.45501E-02 0.00064 ];
INF_FISS                  (idx, [1:   4]) = [  1.95012E-03 0.00045  3.10186E-02 0.00088 ];
INF_NSF                   (idx, [1:   4]) = [  5.12930E-03 0.00047  8.00266E-02 0.00089 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.63025E+00 5.1E-05  2.57996E+00 4.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04712E+02 6.6E-06  2.04181E+02 7.1E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.75831E-08 0.00045  2.57525E-06 0.00012 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29151E-01 0.00012  1.48045E+00 0.00029 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44295E-01 0.00017  3.92216E-01 0.00037 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64013E-02 0.00021  9.28478E-02 0.00076 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35092E-03 0.00259  2.79364E-02 0.00250 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03206E-02 0.00180 -8.72952E-03 0.00819 ];
INF_SCATT5                (idx, [1:   4]) = [  1.63146E-04 0.09968  6.52433E-03 0.00868 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08013E-03 0.00286 -1.68826E-02 0.00264 ];
INF_SCATT7                (idx, [1:   4]) = [  7.46856E-04 0.02127  4.55581E-04 0.10034 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29192E-01 0.00012  1.48045E+00 0.00029 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44295E-01 0.00017  3.92216E-01 0.00037 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64015E-02 0.00021  9.28478E-02 0.00076 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35061E-03 0.00259  2.79364E-02 0.00250 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03205E-02 0.00180 -8.72952E-03 0.00819 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.62909E-04 0.10009  6.52433E-03 0.00868 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08012E-03 0.00287 -1.68826E-02 0.00264 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.46732E-04 0.02130  4.55581E-04 0.10034 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12648E-01 0.00032  1.00057E+00 0.00023 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56754E+00 0.00032  3.33143E-01 0.00024 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.07965E-03 0.00056  6.45501E-02 0.00064 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69564E-02 0.00020  6.56758E-02 0.00082 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11314E-01 0.00011  1.78369E-02 0.00044  1.13744E-03 0.00389  1.47931E+00 0.00029 ];
INF_S1                    (idx, [1:   8]) = [  2.39105E-01 0.00016  5.19033E-03 0.00105  4.84477E-04 0.00668  3.91732E-01 0.00037 ];
INF_S2                    (idx, [1:   8]) = [  9.79787E-02 0.00022 -1.57735E-03 0.00194  2.65837E-04 0.00664  9.25819E-02 0.00076 ];
INF_S3                    (idx, [1:   8]) = [  9.19348E-03 0.00212 -1.84255E-03 0.00170  9.57598E-05 0.02310  2.78406E-02 0.00251 ];
INF_S4                    (idx, [1:   8]) = [ -9.71869E-03 0.00188 -6.01935E-04 0.00422 -2.30961E-06 0.62096 -8.72721E-03 0.00820 ];
INF_S5                    (idx, [1:   8]) = [  1.39898E-04 0.10494  2.32486E-05 0.13554 -4.05803E-05 0.03273  6.56491E-03 0.00860 ];
INF_S6                    (idx, [1:   8]) = [  5.21963E-03 0.00263 -1.39506E-04 0.01379 -4.92543E-05 0.02788 -1.68333E-02 0.00267 ];
INF_S7                    (idx, [1:   8]) = [  9.14267E-04 0.01667 -1.67410E-04 0.01221 -4.62954E-05 0.02132  5.01876E-04 0.09135 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11355E-01 0.00011  1.78369E-02 0.00044  1.13744E-03 0.00389  1.47931E+00 0.00029 ];
INF_SP1                   (idx, [1:   8]) = [  2.39105E-01 0.00016  5.19033E-03 0.00105  4.84477E-04 0.00668  3.91732E-01 0.00037 ];
INF_SP2                   (idx, [1:   8]) = [  9.79789E-02 0.00022 -1.57735E-03 0.00194  2.65837E-04 0.00664  9.25819E-02 0.00076 ];
INF_SP3                   (idx, [1:   8]) = [  9.19317E-03 0.00211 -1.84255E-03 0.00170  9.57598E-05 0.02310  2.78406E-02 0.00251 ];
INF_SP4                   (idx, [1:   8]) = [ -9.71857E-03 0.00188 -6.01935E-04 0.00422 -2.30961E-06 0.62096 -8.72721E-03 0.00820 ];
INF_SP5                   (idx, [1:   8]) = [  1.39661E-04 0.10547  2.32486E-05 0.13554 -4.05803E-05 0.03273  6.56491E-03 0.00860 ];
INF_SP6                   (idx, [1:   8]) = [  5.21963E-03 0.00264 -1.39506E-04 0.01379 -4.92543E-05 0.02788 -1.68333E-02 0.00267 ];
INF_SP7                   (idx, [1:   8]) = [  9.14143E-04 0.01669 -1.67410E-04 0.01221 -4.62954E-05 0.02132  5.01876E-04 0.09135 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31693E-01 0.00070  1.15718E+00 0.00551 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33597E-01 0.00097  1.24540E+00 0.00679 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33309E-01 0.00089  1.24657E+00 0.00706 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28259E-01 0.00100  1.01393E+00 0.00688 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43870E+00 0.00070  2.88266E-01 0.00554 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42699E+00 0.00097  2.67949E-01 0.00680 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42875E+00 0.00089  2.67717E-01 0.00699 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.46036E+00 0.00100  3.29133E-01 0.00697 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.87337E-03 0.00856  1.56199E-04 0.05349  9.81458E-04 0.02140  9.14430E-04 0.02186  2.59809E-03 0.01327  9.34627E-04 0.02180  2.88572E-04 0.04065 ];
LAMBDA                    (idx, [1:  14]) = [  8.20556E-01 0.02152  1.24988E-02 0.00031  3.11914E-02 0.00059  1.10361E-01 0.00060  3.21909E-01 0.00041  1.33137E+00 0.00117  8.94735E+00 0.00493 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0003' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:41 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:52:26 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284841 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.95655E-01  1.00074E+00  1.00306E+00  1.00231E+00  9.98233E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12834E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88717E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05289E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05713E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67417E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.56171E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.56081E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.58583E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.16205E-01 0.00115  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000917 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00018E+04 0.00088 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00018E+04 0.00088 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.56663E+02 ;
RUNNING_TIME              (idx, 1)        =  7.17450E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.31417E-01  4.31417E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.88767E-01  9.10000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.05114E+01  3.37940E+00  2.65785E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  6.07483E-01  2.74167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.28667E-02  8.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.17448E+01  1.26778E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97126 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99890E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79579E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.41154E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.77794E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.75783E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.78777E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.24792E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62374E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.65312E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.17031E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.43255E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.60882E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.52911E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.09428E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.67964E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.00459E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.30830E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.38225E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.44101E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.38995E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.21413E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.21138E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.40924E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.36768E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.25298E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.61180E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 11 ;
BURNUP                     (idx, [1:  2])  = [  9.00000E+00  9.00080E+00 ];
BURN_DAYS                 (idx, 1)        =  2.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.90049E-01 0.00115 ];
U235_FISS                 (idx, [1:   4]) = [  8.48983E+15 0.00085  6.17355E-01 0.00057 ];
U238_FISS                 (idx, [1:   4]) = [  1.00055E+15 0.00272  7.27518E-02 0.00259 ];
PU239_FISS                (idx, [1:   4]) = [  3.93450E+15 0.00127  2.86113E-01 0.00115 ];
PU240_FISS                (idx, [1:   4]) = [  3.45347E+12 0.04628  2.50886E-04 0.04629 ];
PU241_FISS                (idx, [1:   4]) = [  3.15531E+14 0.00494  2.29455E-02 0.00492 ];
U235_CAPT                 (idx, [1:   4]) = [  1.86764E+15 0.00185  8.45249E-02 0.00178 ];
U238_CAPT                 (idx, [1:   4]) = [  8.86737E+15 0.00113  4.01268E-01 0.00074 ];
PU239_CAPT                (idx, [1:   4]) = [  2.17866E+15 0.00186  9.86015E-02 0.00180 ];
PU240_CAPT                (idx, [1:   4]) = [  1.04933E+15 0.00278  4.74900E-02 0.00274 ];
PU241_CAPT                (idx, [1:   4]) = [  1.13657E+14 0.00792  5.14282E-03 0.00786 ];
XE135_CAPT                (idx, [1:   4]) = [  7.46455E+14 0.00309  3.37842E-02 0.00308 ];
SM149_CAPT                (idx, [1:   4]) = [  1.94810E+14 0.00595  8.81770E-03 0.00597 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000917 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.76907E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000917 5.00777E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3054720 3.05895E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1901229 1.90385E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44968 4.49688E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000917 5.00777E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -7.91624E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50058E+05 5.4E-09  4.50058E+05 5.4E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.57255E+16 2.0E-05  3.57255E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37410E+16 3.7E-06  1.37410E+16 3.7E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.20856E+16 0.00046  1.62922E+16 0.00047  5.79342E+15 0.00111 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.58266E+16 0.00028  3.00332E+16 0.00025  5.79342E+15 0.00111 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.61180E+16 0.00052  3.61180E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.71150E+18 0.00048  4.54708E+17 0.00047  1.25679E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.24881E+14 0.00510 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.61515E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.28789E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11467E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11467E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.58420E+00 0.00050 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.77905E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.55329E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23690E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94143E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96845E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.98856E-01 0.00059 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.89873E-01 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.59992E+00 2.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04428E+02 3.7E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.89884E-01 0.00060  9.84149E-01 0.00059  5.72458E-03 0.00925 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.89802E-01 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  9.89265E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.89802E-01 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  9.98790E-01 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72563E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72556E+01 9.4E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.43050E-07 0.00375 ];
IMP_EALF                  (idx, [1:   2]) = [  6.41655E-07 0.00163 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.46403E-01 0.00277 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.47295E-01 0.00122 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.91251E-03 0.00591  1.69474E-04 0.03416  1.00245E-03 0.01418  9.20213E-04 0.01514  2.63935E-03 0.00848  9.09320E-04 0.01531  2.71702E-04 0.02757 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.78455E-01 0.01448  1.03057E-02 0.02069  3.11780E-02 0.00041  1.10295E-01 0.00042  3.21888E-01 0.00029  1.32846E+00 0.00102  8.13965E+00 0.01393 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.74540E-03 0.00834  1.71549E-04 0.05332  9.93821E-04 0.02080  8.73981E-04 0.02222  2.56766E-03 0.01278  8.64158E-04 0.02246  2.74232E-04 0.04243 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.95519E-01 0.02279  1.25030E-02 0.00039  3.11814E-02 0.00057  1.10334E-01 0.00061  3.22124E-01 0.00043  1.32783E+00 0.00154  8.86312E+00 0.00528 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.66152E-05 0.00132  2.66067E-05 0.00133  2.80816E-05 0.01460 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.63416E-05 0.00120  2.63332E-05 0.00121  2.77936E-05 0.01460 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.78660E-03 0.00928  1.73366E-04 0.05608  9.92737E-04 0.02395  8.88105E-04 0.02520  2.59922E-03 0.01390  8.72071E-04 0.02486  2.61104E-04 0.04594 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.70028E-01 0.02394  1.25007E-02 0.00055  3.11673E-02 0.00072  1.10350E-01 0.00077  3.22076E-01 0.00050  1.32910E+00 0.00181  8.83912E+00 0.00726 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.61728E-05 0.00305  2.61586E-05 0.00307  2.67345E-05 0.03667 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.59028E-05 0.00297  2.58886E-05 0.00299  2.64655E-05 0.03672 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.95269E-03 0.03063  1.99247E-04 0.15482  9.16336E-04 0.08136  1.09640E-03 0.07926  2.58387E-03 0.04536  8.96377E-04 0.07926  2.60458E-04 0.15044 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.83785E-01 0.07447  1.24896E-02 3.6E-05  3.11760E-02 0.00173  1.10391E-01 0.00166  3.22409E-01 0.00139  1.33271E+00 0.00309  8.84500E+00 0.01967 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.95905E-03 0.03020  1.93655E-04 0.15439  9.12728E-04 0.07982  1.07926E-03 0.07943  2.59806E-03 0.04456  9.08401E-04 0.07767  2.66945E-04 0.14891 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.86629E-01 0.07341  1.24896E-02 3.7E-05  3.11764E-02 0.00172  1.10405E-01 0.00165  3.22401E-01 0.00138  1.33305E+00 0.00297  8.84280E+00 0.01967 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.29270E+02 0.03117 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.64337E-05 0.00083 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.61616E-05 0.00058 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.91289E-03 0.00600 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.23785E+02 0.00611 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.91554E-07 0.00071 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.76165E-06 0.00043  2.76159E-06 0.00043  2.77059E-06 0.00524 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.96786E-05 0.00085  3.97013E-05 0.00085  3.59265E-05 0.01073 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.52995E-01 0.00034  6.52940E-01 0.00034  6.73702E-01 0.00899 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06727E+01 0.01430 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.56081E+01 0.00047  3.40179E+01 0.00050 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.69593E+04 0.00365  2.99263E+05 0.00193  6.08919E+05 0.00091  6.50904E+05 0.00085  5.98647E+05 0.00075  6.41603E+05 0.00069  4.35830E+05 0.00064  3.84899E+05 0.00072  2.94289E+05 0.00076  2.40494E+05 0.00072  2.07784E+05 0.00079  1.86858E+05 0.00067  1.72811E+05 0.00088  1.64162E+05 0.00081  1.59896E+05 0.00092  1.38416E+05 0.00094  1.36583E+05 0.00112  1.35104E+05 0.00086  1.33043E+05 0.00081  2.59741E+05 0.00062  2.50879E+05 0.00067  1.81264E+05 0.00067  1.17333E+05 0.00089  1.35675E+05 0.00094  1.28488E+05 0.00093  1.15700E+05 0.00074  1.88942E+05 0.00067  4.33981E+04 0.00120  5.45415E+04 0.00134  4.95577E+04 0.00142  2.88324E+04 0.00157  4.99334E+04 0.00179  3.35766E+04 0.00176  2.81510E+04 0.00173  5.07518E+03 0.00398  4.65469E+03 0.00354  4.26794E+03 0.00399  4.08648E+03 0.00404  4.22800E+03 0.00369  4.52103E+03 0.00500  5.08472E+03 0.00403  4.99404E+03 0.00377  9.66522E+03 0.00253  1.56774E+04 0.00242  2.01961E+04 0.00244  5.34593E+04 0.00136  5.62689E+04 0.00136  6.05980E+04 0.00114  3.98756E+04 0.00167  2.85819E+04 0.00140  2.14499E+04 0.00197  2.52958E+04 0.00121  4.92086E+04 0.00157  6.90620E+04 0.00125  1.36526E+05 0.00121  2.14909E+05 0.00131  3.24339E+05 0.00117  2.08545E+05 0.00138  1.49793E+05 0.00153  1.08009E+05 0.00154  9.71248E+04 0.00123  9.57461E+04 0.00173  8.01088E+04 0.00110  5.42218E+04 0.00166  5.01167E+04 0.00151  4.46274E+04 0.00178  3.78112E+04 0.00171  2.98273E+04 0.00173  1.99728E+04 0.00185  7.08338E+03 0.00172 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.98243E-01 0.00041 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.34872E+18 0.00044  3.62823E+17 0.00107 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38353E-01 0.00013  1.54851E+00 0.00037 ];
INF_CAPT                  (idx, [1:   4]) = [  7.25770E-03 0.00061  3.38962E-02 0.00053 ];
INF_ABS                   (idx, [1:   4]) = [  9.17980E-03 0.00049  6.46295E-02 0.00079 ];
INF_FISS                  (idx, [1:   4]) = [  1.92211E-03 0.00057  3.07333E-02 0.00109 ];
INF_NSF                   (idx, [1:   4]) = [  5.06819E-03 0.00057  7.96405E-02 0.00111 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.63679E+00 7.4E-05  2.59134E+00 4.9E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04803E+02 7.8E-06  2.04340E+02 8.5E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.73894E-08 0.00042  2.57732E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29165E-01 0.00014  1.48386E+00 0.00043 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44353E-01 0.00021  3.93095E-01 0.00055 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64076E-02 0.00030  9.29678E-02 0.00081 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35097E-03 0.00236  2.79010E-02 0.00213 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03209E-02 0.00161 -8.70931E-03 0.00652 ];
INF_SCATT5                (idx, [1:   4]) = [  1.33852E-04 0.09032  6.61419E-03 0.00996 ];
INF_SCATT6                (idx, [1:   4]) = [  5.05557E-03 0.00192 -1.69338E-02 0.00338 ];
INF_SCATT7                (idx, [1:   4]) = [  7.07694E-04 0.01779  4.56776E-04 0.08997 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29206E-01 0.00014  1.48386E+00 0.00043 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44354E-01 0.00021  3.93095E-01 0.00055 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64076E-02 0.00030  9.29678E-02 0.00081 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35091E-03 0.00235  2.79010E-02 0.00213 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03211E-02 0.00161 -8.70931E-03 0.00652 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.33514E-04 0.09075  6.61419E-03 0.00996 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.05571E-03 0.00192 -1.69338E-02 0.00338 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.07721E-04 0.01783  4.56776E-04 0.08997 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12589E-01 0.00028  1.00306E+00 0.00031 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56798E+00 0.00028  3.32319E-01 0.00031 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.13819E-03 0.00049  6.46295E-02 0.00079 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69697E-02 0.00016  6.57862E-02 0.00099 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11384E-01 0.00013  1.77810E-02 0.00039  1.13538E-03 0.00492  1.48272E+00 0.00043 ];
INF_S1                    (idx, [1:   8]) = [  2.39191E-01 0.00021  5.16228E-03 0.00080  4.90570E-04 0.00663  3.92604E-01 0.00056 ];
INF_S2                    (idx, [1:   8]) = [  9.79898E-02 0.00029 -1.58217E-03 0.00272  2.67617E-04 0.01069  9.27002E-02 0.00081 ];
INF_S3                    (idx, [1:   8]) = [  9.18229E-03 0.00172 -1.83132E-03 0.00183  9.56254E-05 0.02422  2.78054E-02 0.00216 ];
INF_S4                    (idx, [1:   8]) = [ -9.73756E-03 0.00166 -5.83342E-04 0.00309  7.11488E-07 1.00000 -8.71002E-03 0.00655 ];
INF_S5                    (idx, [1:   8]) = [  1.00849E-04 0.11949  3.30036E-05 0.06255 -4.01984E-05 0.04799  6.65439E-03 0.00992 ];
INF_S6                    (idx, [1:   8]) = [  5.19636E-03 0.00192 -1.40797E-04 0.01173 -5.22705E-05 0.02692 -1.68815E-02 0.00339 ];
INF_S7                    (idx, [1:   8]) = [  8.78783E-04 0.01385 -1.71089E-04 0.01383 -4.62736E-05 0.02544  5.03050E-04 0.08138 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11425E-01 0.00013  1.77810E-02 0.00039  1.13538E-03 0.00492  1.48272E+00 0.00043 ];
INF_SP1                   (idx, [1:   8]) = [  2.39192E-01 0.00021  5.16228E-03 0.00080  4.90570E-04 0.00663  3.92604E-01 0.00056 ];
INF_SP2                   (idx, [1:   8]) = [  9.79897E-02 0.00029 -1.58217E-03 0.00272  2.67617E-04 0.01069  9.27002E-02 0.00081 ];
INF_SP3                   (idx, [1:   8]) = [  9.18223E-03 0.00171 -1.83132E-03 0.00183  9.56254E-05 0.02422  2.78054E-02 0.00216 ];
INF_SP4                   (idx, [1:   8]) = [ -9.73771E-03 0.00166 -5.83342E-04 0.00309  7.11488E-07 1.00000 -8.71002E-03 0.00655 ];
INF_SP5                   (idx, [1:   8]) = [  1.00511E-04 0.12014  3.30036E-05 0.06255 -4.01984E-05 0.04799  6.65439E-03 0.00992 ];
INF_SP6                   (idx, [1:   8]) = [  5.19650E-03 0.00192 -1.40797E-04 0.01173 -5.22705E-05 0.02692 -1.68815E-02 0.00339 ];
INF_SP7                   (idx, [1:   8]) = [  8.78809E-04 0.01388 -1.71089E-04 0.01383 -4.62736E-05 0.02544  5.03050E-04 0.08138 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32092E-01 0.00059  1.17295E+00 0.00620 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33473E-01 0.00083  1.27548E+00 0.00755 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33966E-01 0.00091  1.26131E+00 0.00727 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28912E-01 0.00087  1.02060E+00 0.00733 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43622E+00 0.00059  2.84442E-01 0.00613 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42774E+00 0.00083  2.61692E-01 0.00746 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42474E+00 0.00091  2.64611E-01 0.00728 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45619E+00 0.00087  3.27024E-01 0.00729 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.74540E-03 0.00834  1.71549E-04 0.05332  9.93821E-04 0.02080  8.73981E-04 0.02222  2.56766E-03 0.01278  8.64158E-04 0.02246  2.74232E-04 0.04243 ];
LAMBDA                    (idx, [1:  14]) = [  7.95519E-01 0.02279  1.25030E-02 0.00039  3.11814E-02 0.00057  1.10334E-01 0.00061  3.22124E-01 0.00043  1.32783E+00 0.00154  8.86312E+00 0.00528 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0003' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:41 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:58:31 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284841 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.97879E-01  9.99719E-01  1.00353E+00  1.00155E+00  9.97317E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12608E-02 0.00107  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88739E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05734E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06158E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67303E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.55955E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.55864E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.55627E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.14680E-01 0.00113  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000715 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00014E+04 0.00089 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00014E+04 0.00089 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.87126E+02 ;
RUNNING_TIME              (idx, 1)        =  7.78438E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.31417E-01  4.31417E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.07950E-01  9.33333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.65351E+01  3.37383E+00  2.64988E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  6.62900E-01  2.73667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.46833E-02  9.16668E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.78436E+01  1.26665E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97311 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00023E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80017E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.43485E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.77156E+04 ;
TOT_SF_RATE               (idx, 1)        =  5.24969E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.80602E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.26072E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62880E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.64546E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.28411E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.47480E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  6.53028E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.62665E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.31084E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.71214E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.10245E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.32420E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.40371E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.46247E+14 ;
CS134_ACTIVITY            (idx, 1)        =  7.87118E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.34863E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.23133E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.39397E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  7.36039E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.25922E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.65966E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 12 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E+01  1.00009E+01 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.00213E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  8.16979E+15 0.00089  5.94508E-01 0.00062 ];
U238_FISS                 (idx, [1:   4]) = [  1.01593E+15 0.00270  7.39177E-02 0.00251 ];
PU239_FISS                (idx, [1:   4]) = [  4.16295E+15 0.00129  3.02937E-01 0.00113 ];
PU240_FISS                (idx, [1:   4]) = [  4.33150E+12 0.04042  3.15219E-04 0.04039 ];
PU241_FISS                (idx, [1:   4]) = [  3.80220E+14 0.00461  2.76693E-02 0.00458 ];
U235_CAPT                 (idx, [1:   4]) = [  1.80456E+15 0.00197  7.99279E-02 0.00192 ];
U238_CAPT                 (idx, [1:   4]) = [  8.95027E+15 0.00106  3.96393E-01 0.00075 ];
PU239_CAPT                (idx, [1:   4]) = [  2.30495E+15 0.00178  1.02088E-01 0.00168 ];
PU240_CAPT                (idx, [1:   4]) = [  1.17490E+15 0.00249  5.20381E-02 0.00243 ];
PU241_CAPT                (idx, [1:   4]) = [  1.38711E+14 0.00686  6.14356E-03 0.00682 ];
XE135_CAPT                (idx, [1:   4]) = [  7.50027E+14 0.00310  3.32228E-02 0.00312 ];
SM149_CAPT                (idx, [1:   4]) = [  1.96097E+14 0.00601  8.68514E-03 0.00597 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000715 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.88677E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000715 5.00789E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3080243 3.08475E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1874969 1.87762E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45503 4.55204E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000715 5.00789E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -2.79397E-09 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50058E+05 5.4E-09  4.50058E+05 5.4E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.58400E+16 1.9E-05  3.58400E+16 1.9E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37315E+16 3.8E-06  1.37315E+16 3.8E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.25809E+16 0.00045  1.67168E+16 0.00045  5.86410E+15 0.00110 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.63123E+16 0.00028  3.04482E+16 0.00025  5.86410E+15 0.00110 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.65966E+16 0.00049  3.65966E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.73287E+18 0.00047  4.59835E+17 0.00044  1.27303E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.33229E+14 0.00499 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.66456E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.30422E+18 0.00061 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11351E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11351E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.57447E+00 0.00054 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.77887E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.52867E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23706E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94099E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96778E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.89095E-01 0.00063 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.80090E-01 0.00063 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.61006E+00 2.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04570E+02 3.8E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.80025E-01 0.00064  9.74550E-01 0.00063  5.54095E-03 0.00982 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.79593E-01 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  9.79444E-01 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.79593E-01 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  9.88594E-01 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72318E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72324E+01 8.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.58803E-07 0.00361 ];
IMP_EALF                  (idx, [1:   2]) = [  6.56654E-07 0.00154 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.51102E-01 0.00273 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.50849E-01 0.00114 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.84870E-03 0.00636  1.68192E-04 0.03646  9.69082E-04 0.01462  9.19101E-04 0.01510  2.60789E-03 0.00901  9.06201E-04 0.01475  2.78235E-04 0.02765 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.93562E-01 0.01430  9.73243E-03 0.02392  3.11411E-02 0.00043  1.10335E-01 0.00041  3.22165E-01 0.00029  1.32545E+00 0.00101  8.43569E+00 0.01182 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.62769E-03 0.00924  1.70606E-04 0.05152  9.28607E-04 0.02117  8.92496E-04 0.02306  2.48283E-03 0.01342  8.74403E-04 0.02290  2.78751E-04 0.04088 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.07056E-01 0.02108  1.25071E-02 0.00048  3.11456E-02 0.00061  1.10391E-01 0.00059  3.22365E-01 0.00044  1.32735E+00 0.00130  8.88514E+00 0.00609 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.67567E-05 0.00142  2.67454E-05 0.00143  2.88526E-05 0.01598 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.62154E-05 0.00118  2.62044E-05 0.00119  2.82599E-05 0.01592 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.65071E-03 0.00998  1.72138E-04 0.05921  9.25114E-04 0.02500  8.83781E-04 0.02432  2.52269E-03 0.01464  8.71214E-04 0.02412  2.75771E-04 0.04369 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.99535E-01 0.02303  1.25034E-02 0.00059  3.11594E-02 0.00079  1.10347E-01 0.00075  3.22229E-01 0.00049  1.32075E+00 0.00249  8.87341E+00 0.00785 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.63676E-05 0.00327  2.63545E-05 0.00327  2.82404E-05 0.04010 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.58364E-05 0.00323  2.58236E-05 0.00322  2.76466E-05 0.03988 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.75586E-03 0.03154  1.81440E-04 0.17009  7.79684E-04 0.08438  8.64754E-04 0.08181  2.82460E-03 0.04792  7.77689E-04 0.08399  3.27699E-04 0.13167 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.60964E-01 0.07230  1.25112E-02 0.00171  3.11648E-02 0.00184  1.10224E-01 0.00174  3.22699E-01 0.00144  1.31012E+00 0.00627  8.97242E+00 0.01620 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.69092E-03 0.03121  1.86471E-04 0.17352  7.84418E-04 0.08128  8.60269E-04 0.08005  2.77647E-03 0.04694  7.64333E-04 0.08255  3.18953E-04 0.12893 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.62685E-01 0.07235  1.25112E-02 0.00171  3.11567E-02 0.00184  1.10231E-01 0.00174  3.22665E-01 0.00142  1.31028E+00 0.00622  8.97140E+00 0.01619 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.18959E+02 0.03131 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.65783E-05 0.00089 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.60417E-05 0.00058 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.60238E-03 0.00573 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.10861E+02 0.00579 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.90429E-07 0.00069 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.75094E-06 0.00042  2.75075E-06 0.00042  2.78163E-06 0.00577 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.97069E-05 0.00085  3.97270E-05 0.00085  3.63077E-05 0.01027 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.50562E-01 0.00034  6.50559E-01 0.00034  6.64573E-01 0.00987 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06673E+01 0.01423 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.55864E+01 0.00046  3.39761E+01 0.00049 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.72664E+04 0.00331  2.99509E+05 0.00165  6.09457E+05 0.00101  6.51087E+05 0.00076  5.98620E+05 0.00084  6.40532E+05 0.00071  4.35095E+05 0.00084  3.85133E+05 0.00070  2.94840E+05 0.00071  2.41066E+05 0.00071  2.07400E+05 0.00073  1.86982E+05 0.00102  1.72804E+05 0.00082  1.64158E+05 0.00099  1.60088E+05 0.00082  1.38342E+05 0.00105  1.36721E+05 0.00114  1.35402E+05 0.00101  1.33204E+05 0.00075  2.59987E+05 0.00064  2.50925E+05 0.00088  1.81158E+05 0.00088  1.17557E+05 0.00104  1.35584E+05 0.00092  1.28625E+05 0.00084  1.15515E+05 0.00060  1.88641E+05 0.00074  4.34184E+04 0.00158  5.44440E+04 0.00181  4.94135E+04 0.00181  2.88015E+04 0.00163  4.98152E+04 0.00104  3.35696E+04 0.00150  2.79773E+04 0.00214  5.03044E+03 0.00399  4.56443E+03 0.00348  4.13289E+03 0.00306  3.95581E+03 0.00330  4.04011E+03 0.00366  4.42963E+03 0.00345  5.04640E+03 0.00368  4.90731E+03 0.00437  9.60011E+03 0.00303  1.55773E+04 0.00221  1.99793E+04 0.00238  5.32707E+04 0.00145  5.60142E+04 0.00152  6.02169E+04 0.00131  3.96367E+04 0.00209  2.83492E+04 0.00175  2.11716E+04 0.00175  2.49944E+04 0.00179  4.88260E+04 0.00138  6.86511E+04 0.00106  1.36003E+05 0.00117  2.14056E+05 0.00129  3.23552E+05 0.00118  2.08005E+05 0.00138  1.49605E+05 0.00131  1.07672E+05 0.00140  9.70048E+04 0.00131  9.56532E+04 0.00138  7.99902E+04 0.00161  5.41546E+04 0.00154  5.00700E+04 0.00129  4.45820E+04 0.00168  3.76540E+04 0.00168  2.97446E+04 0.00200  1.99578E+04 0.00213  7.06449E+03 0.00203 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.88443E-01 0.00050 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.36646E+18 0.00050  3.66447E+17 0.00119 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38491E-01 0.00012  1.55116E+00 0.00039 ];
INF_CAPT                  (idx, [1:   4]) = [  7.35119E-03 0.00080  3.42122E-02 0.00055 ];
INF_ABS                   (idx, [1:   4]) = [  9.24904E-03 0.00066  6.46125E-02 0.00085 ];
INF_FISS                  (idx, [1:   4]) = [  1.89785E-03 0.00059  3.04003E-02 0.00120 ];
INF_NSF                   (idx, [1:   4]) = [  5.01703E-03 0.00059  7.91098E-02 0.00122 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.64353E+00 5.0E-05  2.60227E+00 4.3E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04895E+02 6.1E-06  2.04494E+02 7.5E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.72047E-08 0.00036  2.57891E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29236E-01 0.00012  1.48655E+00 0.00044 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44361E-01 0.00024  3.93823E-01 0.00051 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64093E-02 0.00035  9.30273E-02 0.00078 ];
INF_SCATT3                (idx, [1:   4]) = [  7.33721E-03 0.00299  2.78683E-02 0.00292 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03373E-02 0.00211 -8.82137E-03 0.00500 ];
INF_SCATT5                (idx, [1:   4]) = [  1.46519E-04 0.13507  6.55216E-03 0.00512 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08712E-03 0.00309 -1.69840E-02 0.00368 ];
INF_SCATT7                (idx, [1:   4]) = [  7.45060E-04 0.02289  5.13717E-04 0.10549 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29279E-01 0.00012  1.48655E+00 0.00044 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44361E-01 0.00024  3.93823E-01 0.00051 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64097E-02 0.00035  9.30273E-02 0.00078 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.33706E-03 0.00299  2.78683E-02 0.00292 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03373E-02 0.00211 -8.82137E-03 0.00500 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.46578E-04 0.13524  6.55216E-03 0.00512 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08699E-03 0.00309 -1.69840E-02 0.00368 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.45237E-04 0.02294  5.13717E-04 0.10549 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12591E-01 0.00030  1.00512E+00 0.00034 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56796E+00 0.00030  3.31635E-01 0.00034 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.20680E-03 0.00064  6.46125E-02 0.00085 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69721E-02 0.00020  6.57489E-02 0.00093 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11519E-01 0.00012  1.77175E-02 0.00036  1.14007E-03 0.00439  1.48541E+00 0.00044 ];
INF_S1                    (idx, [1:   8]) = [  2.39214E-01 0.00023  5.14692E-03 0.00086  4.88853E-04 0.00591  3.93334E-01 0.00051 ];
INF_S2                    (idx, [1:   8]) = [  9.79849E-02 0.00035 -1.57559E-03 0.00260  2.64128E-04 0.00927  9.27632E-02 0.00079 ];
INF_S3                    (idx, [1:   8]) = [  9.16734E-03 0.00248 -1.83012E-03 0.00162  9.64201E-05 0.02184  2.77719E-02 0.00293 ];
INF_S4                    (idx, [1:   8]) = [ -9.74805E-03 0.00219 -5.89204E-04 0.00393 -3.45971E-07 1.00000 -8.82102E-03 0.00497 ];
INF_S5                    (idx, [1:   8]) = [  1.16630E-04 0.16638  2.98893E-05 0.07302 -3.81068E-05 0.03570  6.59027E-03 0.00512 ];
INF_S6                    (idx, [1:   8]) = [  5.22735E-03 0.00317 -1.40229E-04 0.02240 -4.86666E-05 0.03207 -1.69354E-02 0.00370 ];
INF_S7                    (idx, [1:   8]) = [  9.15906E-04 0.01893 -1.70846E-04 0.01187 -4.36309E-05 0.03192  5.57348E-04 0.09740 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11561E-01 0.00012  1.77175E-02 0.00036  1.14007E-03 0.00439  1.48541E+00 0.00044 ];
INF_SP1                   (idx, [1:   8]) = [  2.39214E-01 0.00023  5.14692E-03 0.00086  4.88853E-04 0.00591  3.93334E-01 0.00051 ];
INF_SP2                   (idx, [1:   8]) = [  9.79853E-02 0.00035 -1.57559E-03 0.00260  2.64128E-04 0.00927  9.27632E-02 0.00079 ];
INF_SP3                   (idx, [1:   8]) = [  9.16719E-03 0.00248 -1.83012E-03 0.00162  9.64201E-05 0.02184  2.77719E-02 0.00293 ];
INF_SP4                   (idx, [1:   8]) = [ -9.74805E-03 0.00220 -5.89204E-04 0.00393 -3.45971E-07 1.00000 -8.82102E-03 0.00497 ];
INF_SP5                   (idx, [1:   8]) = [  1.16688E-04 0.16653  2.98893E-05 0.07302 -3.81068E-05 0.03570  6.59027E-03 0.00512 ];
INF_SP6                   (idx, [1:   8]) = [  5.22722E-03 0.00317 -1.40229E-04 0.02240 -4.86666E-05 0.03207 -1.69354E-02 0.00370 ];
INF_SP7                   (idx, [1:   8]) = [  9.16083E-04 0.01898 -1.70846E-04 0.01187 -4.36309E-05 0.03192  5.57348E-04 0.09740 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32229E-01 0.00056  1.17219E+00 0.00626 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33453E-01 0.00079  1.26762E+00 0.00900 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34169E-01 0.00091  1.27467E+00 0.00740 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29136E-01 0.00071  1.01536E+00 0.00601 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43537E+00 0.00056  2.84636E-01 0.00626 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42786E+00 0.00079  2.63489E-01 0.00929 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42350E+00 0.00091  2.61847E-01 0.00734 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45476E+00 0.00071  3.28574E-01 0.00598 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.62769E-03 0.00924  1.70606E-04 0.05152  9.28607E-04 0.02117  8.92496E-04 0.02306  2.48283E-03 0.01342  8.74403E-04 0.02290  2.78751E-04 0.04088 ];
LAMBDA                    (idx, [1:  14]) = [  8.07056E-01 0.02108  1.25071E-02 0.00048  3.11456E-02 0.00061  1.10391E-01 0.00059  3.22365E-01 0.00044  1.32735E+00 0.00130  8.88514E+00 0.00609 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0003' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:41 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:04:37 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284841 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.97742E-01  9.99075E-01  1.00272E+00  1.00218E+00  9.98282E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14041E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88596E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06047E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06471E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67314E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.54649E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.54558E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.50775E+00 0.00036  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.18924E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001046 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00021E+04 0.00091 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00021E+04 0.00091 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.17562E+02 ;
RUNNING_TIME              (idx, 1)        =  8.39375E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.31417E-01  4.31417E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.27683E-01  9.68333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  8.25525E+01  3.36583E+00  2.65158E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  7.19100E-01  2.73000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.65333E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  8.39373E+01  1.26633E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97468 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00121E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80391E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.45140E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.74663E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.12769E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.84168E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.28597E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.60969E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.61800E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.59375E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.55559E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  9.15673E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.83444E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.78079E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.77215E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.33810E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.33611E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.43807E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.48658E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.21817E+13 ;
CS137_ACTIVITY            (idx, 1)        =  1.68455E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.24462E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.35329E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.46354E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.26870E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.77424E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 13 ;
BURNUP                     (idx, [1:  2])  = [  1.25000E+01  1.25011E+01 ];
BURN_DAYS                 (idx, 1)        =  3.12500E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.25886E-01 0.00115 ];
U235_FISS                 (idx, [1:   4]) = [  7.41063E+15 0.00100  5.40679E-01 0.00069 ];
U238_FISS                 (idx, [1:   4]) = [  1.04916E+15 0.00263  7.65401E-02 0.00246 ];
PU239_FISS                (idx, [1:   4]) = [  4.66856E+15 0.00120  3.40629E-01 0.00102 ];
PU240_FISS                (idx, [1:   4]) = [  5.67658E+12 0.03519  4.13971E-04 0.03523 ];
PU241_FISS                (idx, [1:   4]) = [  5.60743E+14 0.00351  4.09153E-02 0.00348 ];
U235_CAPT                 (idx, [1:   4]) = [  1.63864E+15 0.00214  6.89936E-02 0.00209 ];
U238_CAPT                 (idx, [1:   4]) = [  9.17836E+15 0.00108  3.86410E-01 0.00076 ];
PU239_CAPT                (idx, [1:   4]) = [  2.59267E+15 0.00179  1.09164E-01 0.00176 ];
PU240_CAPT                (idx, [1:   4]) = [  1.45089E+15 0.00253  6.10821E-02 0.00241 ];
PU241_CAPT                (idx, [1:   4]) = [  2.03273E+14 0.00629  8.55859E-03 0.00627 ];
XE135_CAPT                (idx, [1:   4]) = [  7.58381E+14 0.00313  3.19313E-02 0.00311 ];
SM149_CAPT                (idx, [1:   4]) = [  2.08030E+14 0.00589  8.76081E-03 0.00595 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001046 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.84078E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001046 5.00784E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3142226 3.14657E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1813358 1.81581E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45462 4.54612E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001046 5.00784E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -7.91624E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50058E+05 5.4E-09  4.50058E+05 5.4E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.61091E+16 2.0E-05  3.61091E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37087E+16 3.9E-06  1.37087E+16 3.9E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.37551E+16 0.00047  1.77500E+16 0.00047  6.00512E+15 0.00114 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.74638E+16 0.00030  3.14586E+16 0.00026  6.00512E+15 0.00114 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.77424E+16 0.00052  3.77424E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.78260E+18 0.00050  4.72912E+17 0.00048  1.30969E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.43202E+14 0.00456 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.78070E+16 0.00031 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.34010E+18 0.00064 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11061E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11061E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.55132E+00 0.00056 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.77935E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.47388E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23568E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94097E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96792E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.65348E-01 0.00063 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.56571E-01 0.00063 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.63404E+00 2.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04910E+02 3.9E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.56541E-01 0.00064  9.51356E-01 0.00063  5.21449E-03 0.01004 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.56653E-01 0.00031 ];
COL_KEFF                  (idx, [1:   2]) = [  9.56853E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.56653E-01 0.00031 ];
ABS_KINF                  (idx, [1:   2]) = [  9.65428E-01 0.00030 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71825E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71840E+01 9.2E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.92269E-07 0.00374 ];
IMP_EALF                  (idx, [1:   2]) = [  6.89227E-07 0.00158 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.59748E-01 0.00277 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.59166E-01 0.00120 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.85175E-03 0.00618  1.62063E-04 0.03474  1.01865E-03 0.01541  9.04968E-04 0.01540  2.61319E-03 0.00905  8.76526E-04 0.01551  2.76349E-04 0.02753 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.82134E-01 0.01488  1.01179E-02 0.02183  3.10437E-02 0.00043  1.10460E-01 0.00046  3.22287E-01 0.00030  1.31042E+00 0.00312  8.14998E+00 0.01429 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.52224E-03 0.00892  1.52920E-04 0.05722  9.53222E-04 0.02214  8.44741E-04 0.02467  2.48425E-03 0.01362  8.29149E-04 0.02325  2.57963E-04 0.04136 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.82180E-01 0.02214  1.25212E-02 0.00070  3.10769E-02 0.00061  1.10527E-01 0.00065  3.22261E-01 0.00044  1.31740E+00 0.00169  8.82487E+00 0.00755 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.72358E-05 0.00138  2.72266E-05 0.00138  2.93957E-05 0.01672 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.60470E-05 0.00123  2.60383E-05 0.00123  2.81083E-05 0.01668 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.44521E-03 0.01022  1.56254E-04 0.05795  9.74052E-04 0.02293  8.46971E-04 0.02686  2.39911E-03 0.01544  8.11740E-04 0.02631  2.57083E-04 0.04533 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.79329E-01 0.02474  1.25120E-02 0.00082  3.10597E-02 0.00078  1.10374E-01 0.00083  3.22332E-01 0.00052  1.31524E+00 0.00254  8.81171E+00 0.00979 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.68571E-05 0.00308  2.68409E-05 0.00307  2.76264E-05 0.04425 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.56856E-05 0.00304  2.56702E-05 0.00303  2.64008E-05 0.04419 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.38990E-03 0.03503  1.40370E-04 0.18846  9.09928E-04 0.08427  8.76164E-04 0.08644  2.40145E-03 0.05037  7.72111E-04 0.08718  2.89875E-04 0.15450 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  6.94590E-01 0.06546  1.25154E-02 0.00206  3.10357E-02 0.00191  1.10556E-01 0.00202  3.22327E-01 0.00146  1.31390E+00 0.00588  8.49153E+00 0.02716 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.38028E-03 0.03442  1.29013E-04 0.19491  9.13605E-04 0.08270  8.66895E-04 0.08615  2.42425E-03 0.04896  7.64649E-04 0.08704  2.81860E-04 0.14968 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  6.92798E-01 0.06466  1.25154E-02 0.00206  3.10476E-02 0.00189  1.10556E-01 0.00201  3.22318E-01 0.00145  1.31475E+00 0.00578  8.51096E+00 0.02680 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.01690E+02 0.03504 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.70242E-05 0.00092 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.58438E-05 0.00059 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.37816E-03 0.00561 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.99062E+02 0.00562 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.86292E-07 0.00069 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.72759E-06 0.00043  2.72747E-06 0.00043  2.75125E-06 0.00586 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.96548E-05 0.00086  3.96712E-05 0.00086  3.69816E-05 0.01048 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.45129E-01 0.00035  6.45197E-01 0.00035  6.47459E-01 0.01016 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07504E+01 0.01429 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.54558E+01 0.00047  3.38274E+01 0.00049 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.75002E+04 0.00334  3.00772E+05 0.00162  6.10408E+05 0.00065  6.50956E+05 0.00087  5.98242E+05 0.00083  6.41007E+05 0.00080  4.35153E+05 0.00068  3.85254E+05 0.00070  2.94962E+05 0.00084  2.40953E+05 0.00064  2.07907E+05 0.00081  1.87201E+05 0.00083  1.72756E+05 0.00087  1.64251E+05 0.00099  1.60257E+05 0.00080  1.38136E+05 0.00090  1.36549E+05 0.00102  1.35490E+05 0.00081  1.33168E+05 0.00082  2.59743E+05 0.00077  2.51165E+05 0.00085  1.81352E+05 0.00110  1.17629E+05 0.00071  1.35859E+05 0.00103  1.28995E+05 0.00077  1.15242E+05 0.00103  1.88182E+05 0.00065  4.31967E+04 0.00154  5.43536E+04 0.00154  4.92486E+04 0.00217  2.86232E+04 0.00197  4.96358E+04 0.00156  3.33400E+04 0.00215  2.75374E+04 0.00202  4.88908E+03 0.00319  4.39011E+03 0.00354  3.88366E+03 0.00298  3.71683E+03 0.00470  3.80136E+03 0.00299  4.17186E+03 0.00511  4.85039E+03 0.00311  4.82483E+03 0.00336  9.37125E+03 0.00219  1.53919E+04 0.00255  1.97380E+04 0.00243  5.25626E+04 0.00145  5.54519E+04 0.00155  5.94058E+04 0.00153  3.88931E+04 0.00167  2.76996E+04 0.00159  2.06569E+04 0.00141  2.43843E+04 0.00141  4.77754E+04 0.00115  6.74035E+04 0.00140  1.33859E+05 0.00138  2.11039E+05 0.00145  3.19590E+05 0.00123  2.06183E+05 0.00105  1.48150E+05 0.00122  1.06816E+05 0.00121  9.62402E+04 0.00147  9.49799E+04 0.00161  7.94259E+04 0.00146  5.37780E+04 0.00173  4.97277E+04 0.00125  4.42721E+04 0.00151  3.74962E+04 0.00154  2.95803E+04 0.00156  1.98571E+04 0.00170  7.04932E+03 0.00224 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.65632E-01 0.00064 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.40904E+18 0.00058  3.73594E+17 0.00111 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38638E-01 0.00012  1.55559E+00 0.00031 ];
INF_CAPT                  (idx, [1:   4]) = [  7.56277E-03 0.00055  3.50650E-02 0.00059 ];
INF_ABS                   (idx, [1:   4]) = [  9.39866E-03 0.00047  6.48402E-02 0.00085 ];
INF_FISS                  (idx, [1:   4]) = [  1.83589E-03 0.00043  2.97752E-02 0.00117 ];
INF_NSF                   (idx, [1:   4]) = [  4.88375E-03 0.00044  7.82482E-02 0.00118 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.66016E+00 7.1E-05  2.62796E+00 4.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05123E+02 5.9E-06  2.04861E+02 7.3E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.67921E-08 0.00048  2.58347E-06 0.00013 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29237E-01 0.00012  1.49078E+00 0.00036 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44321E-01 0.00023  3.94816E-01 0.00048 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63795E-02 0.00026  9.32702E-02 0.00109 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35046E-03 0.00301  2.80007E-02 0.00373 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03203E-02 0.00205 -8.72809E-03 0.00833 ];
INF_SCATT5                (idx, [1:   4]) = [  1.71675E-04 0.09117  6.70758E-03 0.00828 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10889E-03 0.00294 -1.70430E-02 0.00281 ];
INF_SCATT7                (idx, [1:   4]) = [  7.49034E-04 0.02036  4.46860E-04 0.09854 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29279E-01 0.00012  1.49078E+00 0.00036 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44322E-01 0.00023  3.94816E-01 0.00048 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63797E-02 0.00026  9.32702E-02 0.00109 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35054E-03 0.00301  2.80007E-02 0.00373 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03202E-02 0.00205 -8.72809E-03 0.00833 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.71662E-04 0.09123  6.70758E-03 0.00828 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10870E-03 0.00295 -1.70430E-02 0.00281 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.49037E-04 0.02036  4.46860E-04 0.09854 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12539E-01 0.00024  1.00899E+00 0.00028 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56834E+00 0.00024  3.30363E-01 0.00028 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.35666E-03 0.00048  6.48402E-02 0.00085 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69744E-02 0.00021  6.59576E-02 0.00088 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11664E-01 0.00012  1.75732E-02 0.00043  1.14874E-03 0.00424  1.48963E+00 0.00036 ];
INF_S1                    (idx, [1:   8]) = [  2.39221E-01 0.00023  5.09947E-03 0.00113  4.94403E-04 0.00826  3.94322E-01 0.00048 ];
INF_S2                    (idx, [1:   8]) = [  9.79433E-02 0.00025 -1.56379E-03 0.00292  2.71833E-04 0.00937  9.29983E-02 0.00109 ];
INF_S3                    (idx, [1:   8]) = [  9.16349E-03 0.00233 -1.81303E-03 0.00199  9.69574E-05 0.01702  2.79037E-02 0.00374 ];
INF_S4                    (idx, [1:   8]) = [ -9.74240E-03 0.00208 -5.77903E-04 0.00566 -1.86124E-07 1.00000 -8.72790E-03 0.00833 ];
INF_S5                    (idx, [1:   8]) = [  1.36642E-04 0.11227  3.50325E-05 0.08673 -4.07958E-05 0.04135  6.74838E-03 0.00823 ];
INF_S6                    (idx, [1:   8]) = [  5.24799E-03 0.00271 -1.39103E-04 0.02028 -5.12731E-05 0.02939 -1.69917E-02 0.00279 ];
INF_S7                    (idx, [1:   8]) = [  9.23957E-04 0.01530 -1.74922E-04 0.01578 -4.63627E-05 0.03243  4.93223E-04 0.09020 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11706E-01 0.00012  1.75732E-02 0.00043  1.14874E-03 0.00424  1.48963E+00 0.00036 ];
INF_SP1                   (idx, [1:   8]) = [  2.39222E-01 0.00023  5.09947E-03 0.00113  4.94403E-04 0.00826  3.94322E-01 0.00048 ];
INF_SP2                   (idx, [1:   8]) = [  9.79435E-02 0.00026 -1.56379E-03 0.00292  2.71833E-04 0.00937  9.29983E-02 0.00109 ];
INF_SP3                   (idx, [1:   8]) = [  9.16357E-03 0.00232 -1.81303E-03 0.00199  9.69574E-05 0.01702  2.79037E-02 0.00374 ];
INF_SP4                   (idx, [1:   8]) = [ -9.74232E-03 0.00208 -5.77903E-04 0.00566 -1.86124E-07 1.00000 -8.72790E-03 0.00833 ];
INF_SP5                   (idx, [1:   8]) = [  1.36629E-04 0.11238  3.50325E-05 0.08673 -4.07958E-05 0.04135  6.74838E-03 0.00823 ];
INF_SP6                   (idx, [1:   8]) = [  5.24780E-03 0.00271 -1.39103E-04 0.02028 -5.12731E-05 0.02939 -1.69917E-02 0.00279 ];
INF_SP7                   (idx, [1:   8]) = [  9.23960E-04 0.01531 -1.74922E-04 0.01578 -4.63627E-05 0.03243  4.93223E-04 0.09020 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32198E-01 0.00059  1.17616E+00 0.00672 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33586E-01 0.00090  1.27768E+00 0.00840 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33888E-01 0.00099  1.27282E+00 0.00950 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29191E-01 0.00106  1.01915E+00 0.00603 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43557E+00 0.00059  2.83715E-01 0.00674 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42705E+00 0.00090  2.61328E-01 0.00834 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42522E+00 0.00099  2.62460E-01 0.00963 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45443E+00 0.00106  3.27357E-01 0.00609 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.52224E-03 0.00892  1.52920E-04 0.05722  9.53222E-04 0.02214  8.44741E-04 0.02467  2.48425E-03 0.01362  8.29149E-04 0.02325  2.57963E-04 0.04136 ];
LAMBDA                    (idx, [1:  14]) = [  7.82180E-01 0.02214  1.25212E-02 0.00070  3.10769E-02 0.00061  1.10527E-01 0.00065  3.22261E-01 0.00044  1.31740E+00 0.00169  8.82487E+00 0.00755 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0003' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:41 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:10:42 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284841 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.97078E-01  9.99871E-01  1.00333E+00  1.00188E+00  9.97842E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14688E-02 0.00114  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88531E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06185E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06611E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67613E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.53830E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.53737E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.48035E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.20557E-01 0.00111  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000647 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00013E+04 0.00085 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00013E+04 0.00085 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.47970E+02 ;
RUNNING_TIME              (idx, 1)        =  9.00254E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.31417E-01  4.31417E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.47967E-01  1.01833E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  8.85636E+01  3.36552E+00  2.64558E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  7.75383E-01  2.82500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.99167E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.00253E+01  1.26555E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97604 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00102E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80714E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.50336E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.73418E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.17114E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.89337E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.32276E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.60997E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.60187E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.94276E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.64065E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.22392E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.11572E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.18844E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.82908E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.56168E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.36033E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.47405E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.52136E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.72869E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.01957E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.29264E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.32347E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.56985E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.28381E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.88404E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 14 ;
BURNUP                     (idx, [1:  2])  = [  1.50000E+01  1.50014E+01 ];
BURN_DAYS                 (idx, 1)        =  3.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.49924E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  6.76264E+15 0.00102  4.93852E-01 0.00078 ];
U238_FISS                 (idx, [1:   4]) = [  1.08020E+15 0.00270  7.88802E-02 0.00258 ];
PU239_FISS                (idx, [1:   4]) = [  5.08536E+15 0.00113  3.71373E-01 0.00096 ];
PU240_FISS                (idx, [1:   4]) = [  7.79672E+12 0.03044  5.69217E-04 0.03044 ];
PU241_FISS                (idx, [1:   4]) = [  7.44791E+14 0.00312  5.43914E-02 0.00307 ];
U235_CAPT                 (idx, [1:   4]) = [  1.49854E+15 0.00237  6.03135E-02 0.00234 ];
U238_CAPT                 (idx, [1:   4]) = [  9.39877E+15 0.00103  3.78242E-01 0.00071 ];
PU239_CAPT                (idx, [1:   4]) = [  2.80590E+15 0.00163  1.12933E-01 0.00161 ];
PU240_CAPT                (idx, [1:   4]) = [  1.69641E+15 0.00225  6.82702E-02 0.00213 ];
PU241_CAPT                (idx, [1:   4]) = [  2.65234E+14 0.00567  1.06741E-02 0.00562 ];
XE135_CAPT                (idx, [1:   4]) = [  7.61989E+14 0.00324  3.06714E-02 0.00327 ];
SM149_CAPT                (idx, [1:   4]) = [  2.15423E+14 0.00563  8.67094E-03 0.00564 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000647 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.85007E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000647 5.00785E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3193847 3.19864E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1760500 1.76289E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46300 4.63213E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000647 5.00785E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 2.79397E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50058E+05 5.4E-09  4.50058E+05 5.4E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.63462E+16 2.1E-05  3.63462E+16 2.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36882E+16 4.1E-06  1.36882E+16 4.1E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.48735E+16 0.00047  1.87213E+16 0.00047  6.15217E+15 0.00117 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.85617E+16 0.00030  3.24095E+16 0.00027  6.15217E+15 0.00117 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.88404E+16 0.00051  3.88404E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.83124E+18 0.00048  4.85025E+17 0.00046  1.34622E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.59902E+14 0.00487 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.89216E+16 0.00031 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.37590E+18 0.00064 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10771E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10771E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.53165E+00 0.00054 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.77062E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.42990E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23482E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94004E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96712E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.44908E-01 0.00058 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.36154E-01 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.65529E+00 2.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05216E+02 4.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.36249E-01 0.00059  9.31182E-01 0.00058  4.97169E-03 0.01048 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.35361E-01 0.00032 ];
COL_KEFF                  (idx, [1:   2]) = [  9.35904E-01 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.35361E-01 0.00032 ];
ABS_KINF                  (idx, [1:   2]) = [  9.44101E-01 0.00031 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71392E+01 0.00023 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71425E+01 9.0E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.23170E-07 0.00392 ];
IMP_EALF                  (idx, [1:   2]) = [  7.18446E-07 0.00154 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.67617E-01 0.00287 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.66951E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.84685E-03 0.00629  1.56854E-04 0.03726  1.00407E-03 0.01492  9.09604E-04 0.01602  2.57135E-03 0.00906  9.35880E-04 0.01508  2.69094E-04 0.02975 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.73744E-01 0.01525  9.52408E-03 0.02517  3.09651E-02 0.00044  1.10610E-01 0.00047  3.22344E-01 0.00032  1.30004E+00 0.00257  8.04874E+00 0.01457 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.33073E-03 0.00960  1.49256E-04 0.05381  9.27542E-04 0.02297  8.37614E-04 0.02463  2.31464E-03 0.01363  8.46778E-04 0.02304  2.54893E-04 0.04186 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.80082E-01 0.02156  1.25256E-02 0.00074  3.09620E-02 0.00065  1.10518E-01 0.00066  3.22548E-01 0.00046  1.30306E+00 0.00237  8.66375E+00 0.00854 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.77191E-05 0.00138  2.77075E-05 0.00138  2.97085E-05 0.01595 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.59477E-05 0.00127  2.59368E-05 0.00127  2.78078E-05 0.01590 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.30475E-03 0.01038  1.58155E-04 0.06375  9.29410E-04 0.02535  8.29032E-04 0.02668  2.32154E-03 0.01539  8.29668E-04 0.02626  2.36943E-04 0.04994 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.55418E-01 0.02518  1.25275E-02 0.00107  3.09664E-02 0.00085  1.10623E-01 0.00090  3.22447E-01 0.00058  1.30470E+00 0.00294  8.64019E+00 0.01113 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.73124E-05 0.00326  2.73100E-05 0.00327  2.65623E-05 0.04006 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.55669E-05 0.00321  2.55646E-05 0.00321  2.48670E-05 0.04010 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.42486E-03 0.03348  1.55700E-04 0.19195  1.09216E-03 0.07327  7.60499E-04 0.09155  2.38879E-03 0.05189  8.30816E-04 0.08632  1.96895E-04 0.19953 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  6.85332E-01 0.08085  1.25205E-02 0.00253  3.08470E-02 0.00185  1.10781E-01 0.00216  3.22770E-01 0.00158  1.30495E+00 0.00673  8.64074E+00 0.02849 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.41951E-03 0.03275  1.49379E-04 0.19317  1.07639E-03 0.07142  7.55973E-04 0.08976  2.41101E-03 0.05178  8.27837E-04 0.08330  1.98917E-04 0.19719 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  6.85386E-01 0.08034  1.25205E-02 0.00253  3.08503E-02 0.00184  1.10771E-01 0.00216  3.22754E-01 0.00157  1.30489E+00 0.00674  8.63629E+00 0.02859 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.99786E+02 0.03344 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.75187E-05 0.00085 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.57599E-05 0.00061 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.33047E-03 0.00653 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.93727E+02 0.00652 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.83646E-07 0.00074 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.70889E-06 0.00043  2.70862E-06 0.00043  2.75703E-06 0.00560 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.96812E-05 0.00093  3.96981E-05 0.00093  3.67232E-05 0.01078 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.40750E-01 0.00033  6.40913E-01 0.00033  6.25308E-01 0.00995 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06978E+01 0.01334 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.53737E+01 0.00049  3.37207E+01 0.00050 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.85431E+04 0.00296  3.02203E+05 0.00128  6.10592E+05 0.00112  6.51307E+05 0.00072  5.98304E+05 0.00075  6.41049E+05 0.00070  4.34830E+05 0.00064  3.84721E+05 0.00081  2.94601E+05 0.00085  2.40622E+05 0.00075  2.07251E+05 0.00064  1.87206E+05 0.00069  1.72921E+05 0.00084  1.64408E+05 0.00097  1.60377E+05 0.00089  1.38380E+05 0.00063  1.36470E+05 0.00085  1.35479E+05 0.00097  1.33073E+05 0.00125  2.59931E+05 0.00052  2.51247E+05 0.00083  1.81277E+05 0.00084  1.17637E+05 0.00088  1.35716E+05 0.00084  1.28983E+05 0.00076  1.15297E+05 0.00107  1.87537E+05 0.00085  4.33333E+04 0.00166  5.40764E+04 0.00143  4.90565E+04 0.00171  2.86715E+04 0.00235  4.95793E+04 0.00126  3.32809E+04 0.00163  2.74342E+04 0.00203  4.78845E+03 0.00272  4.20844E+03 0.00316  3.68381E+03 0.00327  3.53870E+03 0.00419  3.63192E+03 0.00316  3.98030E+03 0.00366  4.68246E+03 0.00388  4.63724E+03 0.00365  9.20523E+03 0.00269  1.51454E+04 0.00329  1.96198E+04 0.00175  5.22185E+04 0.00167  5.49218E+04 0.00133  5.88464E+04 0.00157  3.83944E+04 0.00138  2.72766E+04 0.00198  2.02816E+04 0.00189  2.39636E+04 0.00142  4.68793E+04 0.00133  6.64302E+04 0.00170  1.32602E+05 0.00146  2.09465E+05 0.00142  3.17804E+05 0.00144  2.04732E+05 0.00143  1.47264E+05 0.00147  1.06174E+05 0.00156  9.57575E+04 0.00181  9.44456E+04 0.00148  7.90393E+04 0.00176  5.35214E+04 0.00157  4.95678E+04 0.00164  4.40778E+04 0.00158  3.72986E+04 0.00169  2.94942E+04 0.00192  1.97984E+04 0.00177  7.00960E+03 0.00176 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.44655E-01 0.00058 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.44967E+18 0.00052  3.81616E+17 0.00137 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38805E-01 0.00011  1.55873E+00 0.00041 ];
INF_CAPT                  (idx, [1:   4]) = [  7.74491E-03 0.00071  3.57618E-02 0.00073 ];
INF_ABS                   (idx, [1:   4]) = [  9.52523E-03 0.00056  6.48736E-02 0.00100 ];
INF_FISS                  (idx, [1:   4]) = [  1.78032E-03 0.00052  2.91118E-02 0.00134 ];
INF_NSF                   (idx, [1:   4]) = [  4.76350E-03 0.00052  7.71626E-02 0.00137 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.67565E+00 6.3E-05  2.65056E+00 5.9E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05339E+02 9.1E-06  2.05187E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.65169E-08 0.00039  2.58623E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29286E-01 0.00011  1.49389E+00 0.00047 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44387E-01 0.00018  3.95535E-01 0.00058 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64407E-02 0.00030  9.34334E-02 0.00102 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34141E-03 0.00342  2.80702E-02 0.00260 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03267E-02 0.00251 -8.96795E-03 0.00712 ];
INF_SCATT5                (idx, [1:   4]) = [  1.75282E-04 0.12082  6.64618E-03 0.00675 ];
INF_SCATT6                (idx, [1:   4]) = [  5.12964E-03 0.00363 -1.70961E-02 0.00222 ];
INF_SCATT7                (idx, [1:   4]) = [  7.62152E-04 0.01780  4.94387E-04 0.10560 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29328E-01 0.00011  1.49389E+00 0.00047 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44388E-01 0.00018  3.95535E-01 0.00058 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64405E-02 0.00030  9.34334E-02 0.00102 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34169E-03 0.00341  2.80702E-02 0.00260 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03268E-02 0.00250 -8.96795E-03 0.00712 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.75714E-04 0.12074  6.64618E-03 0.00675 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.12987E-03 0.00362 -1.70961E-02 0.00222 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.62330E-04 0.01776  4.94387E-04 0.10560 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12376E-01 0.00024  1.01172E+00 0.00038 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56954E+00 0.00024  3.29474E-01 0.00038 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.48316E-03 0.00057  6.48736E-02 0.00100 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69761E-02 0.00021  6.59860E-02 0.00119 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11829E-01 0.00011  1.74574E-02 0.00040  1.14741E-03 0.00452  1.49274E+00 0.00047 ];
INF_S1                    (idx, [1:   8]) = [  2.39333E-01 0.00018  5.05468E-03 0.00098  4.94172E-04 0.00575  3.95041E-01 0.00058 ];
INF_S2                    (idx, [1:   8]) = [  9.80156E-02 0.00030 -1.57494E-03 0.00268  2.69950E-04 0.01056  9.31634E-02 0.00102 ];
INF_S3                    (idx, [1:   8]) = [  9.14478E-03 0.00267 -1.80337E-03 0.00167  9.72656E-05 0.01552  2.79729E-02 0.00262 ];
INF_S4                    (idx, [1:   8]) = [ -9.75716E-03 0.00250 -5.69558E-04 0.00618 -5.72539E-07 1.00000 -8.96738E-03 0.00716 ];
INF_S5                    (idx, [1:   8]) = [  1.40626E-04 0.14273  3.46559E-05 0.08484 -4.05038E-05 0.03543  6.68669E-03 0.00671 ];
INF_S6                    (idx, [1:   8]) = [  5.26482E-03 0.00357 -1.35187E-04 0.01742 -4.80204E-05 0.02393 -1.70481E-02 0.00223 ];
INF_S7                    (idx, [1:   8]) = [  9.28449E-04 0.01457 -1.66298E-04 0.01815 -4.32660E-05 0.02105  5.37653E-04 0.09782 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11871E-01 0.00011  1.74574E-02 0.00040  1.14741E-03 0.00452  1.49274E+00 0.00047 ];
INF_SP1                   (idx, [1:   8]) = [  2.39334E-01 0.00018  5.05468E-03 0.00098  4.94172E-04 0.00575  3.95041E-01 0.00058 ];
INF_SP2                   (idx, [1:   8]) = [  9.80154E-02 0.00030 -1.57494E-03 0.00268  2.69950E-04 0.01056  9.31634E-02 0.00102 ];
INF_SP3                   (idx, [1:   8]) = [  9.14506E-03 0.00267 -1.80337E-03 0.00167  9.72656E-05 0.01552  2.79729E-02 0.00262 ];
INF_SP4                   (idx, [1:   8]) = [ -9.75724E-03 0.00250 -5.69558E-04 0.00618 -5.72539E-07 1.00000 -8.96738E-03 0.00716 ];
INF_SP5                   (idx, [1:   8]) = [  1.41058E-04 0.14257  3.46559E-05 0.08484 -4.05038E-05 0.03543  6.68669E-03 0.00671 ];
INF_SP6                   (idx, [1:   8]) = [  5.26505E-03 0.00356 -1.35187E-04 0.01742 -4.80204E-05 0.02393 -1.70481E-02 0.00223 ];
INF_SP7                   (idx, [1:   8]) = [  9.28627E-04 0.01453 -1.66298E-04 0.01815 -4.32660E-05 0.02105  5.37653E-04 0.09782 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31986E-01 0.00090  1.18727E+00 0.00779 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33551E-01 0.00117  1.29196E+00 0.01078 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33690E-01 0.00099  1.28282E+00 0.00955 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28795E-01 0.00131  1.02893E+00 0.00715 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43689E+00 0.00090  2.81163E-01 0.00775 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42729E+00 0.00117  2.58719E-01 0.01067 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42642E+00 0.00099  2.60407E-01 0.00944 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45697E+00 0.00131  3.24363E-01 0.00720 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.33073E-03 0.00960  1.49256E-04 0.05381  9.27542E-04 0.02297  8.37614E-04 0.02463  2.31464E-03 0.01363  8.46778E-04 0.02304  2.54893E-04 0.04186 ];
LAMBDA                    (idx, [1:  14]) = [  7.80082E-01 0.02156  1.25256E-02 0.00074  3.09620E-02 0.00065  1.10518E-01 0.00066  3.22548E-01 0.00046  1.30306E+00 0.00237  8.66375E+00 0.00854 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0003' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:41 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:16:47 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284841 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.97562E-01  1.00002E+00  1.00286E+00  1.00187E+00  9.97691E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14922E-02 0.00112  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88508E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06563E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06989E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67599E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.53622E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.53529E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.45479E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.21061E-01 0.00111  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001179 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00024E+04 0.00087 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00024E+04 0.00087 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.78338E+02 ;
RUNNING_TIME              (idx, 1)        =  9.61057E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.31417E-01  4.31417E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.68250E-01  9.91667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.45673E+01  3.36495E+00  2.63868E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  8.31433E-01  2.72833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.17833E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.61055E+01  1.26499E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97720 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99934E+00 0.00021 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80996E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.55755E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.72516E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.79513E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.94427E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.35938E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61325E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.58919E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.33475E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.72203E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.57960E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.40134E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.55149E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.88189E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.77439E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.38296E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.50589E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.55316E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.31281E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.35369E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.34227E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.29805E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  4.08974E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.29868E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.99273E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 15 ;
BURNUP                     (idx, [1:  2])  = [  1.75000E+01  1.75016E+01 ];
BURN_DAYS                 (idx, 1)        =  4.37500E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.73908E-01 0.00110 ];
U235_FISS                 (idx, [1:   4]) = [  6.16756E+15 0.00109  4.51433E-01 0.00091 ];
U238_FISS                 (idx, [1:   4]) = [  1.10532E+15 0.00290  8.08917E-02 0.00273 ];
PU239_FISS                (idx, [1:   4]) = [  5.44000E+15 0.00114  3.98172E-01 0.00093 ];
PU240_FISS                (idx, [1:   4]) = [  9.01272E+12 0.03056  6.58842E-04 0.03047 ];
PU241_FISS                (idx, [1:   4]) = [  9.24905E+14 0.00302  6.76939E-02 0.00291 ];
U235_CAPT                 (idx, [1:   4]) = [  1.37300E+15 0.00247  5.29027E-02 0.00245 ];
U238_CAPT                 (idx, [1:   4]) = [  9.60302E+15 0.00104  3.69971E-01 0.00075 ];
PU239_CAPT                (idx, [1:   4]) = [  2.99343E+15 0.00158  1.15340E-01 0.00156 ];
PU240_CAPT                (idx, [1:   4]) = [  1.94673E+15 0.00218  7.49996E-02 0.00204 ];
PU241_CAPT                (idx, [1:   4]) = [  3.32856E+14 0.00475  1.28249E-02 0.00473 ];
XE135_CAPT                (idx, [1:   4]) = [  7.72150E+14 0.00310  2.97532E-02 0.00312 ];
SM149_CAPT                (idx, [1:   4]) = [  2.21898E+14 0.00626  8.55018E-03 0.00625 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001179 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.77918E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001179 5.00778E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3245882 3.25029E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1708821 1.71100E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46476 4.64808E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001179 5.00778E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.44589E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50058E+05 5.4E-09  4.50058E+05 5.4E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.65599E+16 2.4E-05  3.65599E+16 2.4E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36696E+16 4.9E-06  1.36696E+16 4.9E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.59823E+16 0.00046  1.96613E+16 0.00048  6.32099E+15 0.00119 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.96519E+16 0.00030  3.33309E+16 0.00028  6.32099E+15 0.00119 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.99273E+16 0.00050  3.99273E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.88039E+18 0.00049  4.97510E+17 0.00048  1.38288E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.71191E+14 0.00483 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.00231E+16 0.00031 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.41354E+18 0.00065 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10482E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10482E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.51065E+00 0.00057 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.75770E-01 0.00036 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.38753E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23418E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93904E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96780E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.23788E-01 0.00063 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.15201E-01 0.00063 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.67453E+00 2.8E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05495E+02 4.9E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.15090E-01 0.00064  9.10384E-01 0.00064  4.81660E-03 0.01046 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.14979E-01 0.00031 ];
COL_KEFF                  (idx, [1:   2]) = [  9.15776E-01 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.14979E-01 0.00031 ];
ABS_KINF                  (idx, [1:   2]) = [  9.23556E-01 0.00030 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71018E+01 0.00024 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71034E+01 9.3E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.51067E-07 0.00418 ];
IMP_EALF                  (idx, [1:   2]) = [  7.47132E-07 0.00159 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.74755E-01 0.00302 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.74631E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.83933E-03 0.00638  1.52360E-04 0.03940  1.02708E-03 0.01579  9.31230E-04 0.01565  2.53030E-03 0.00980  9.25437E-04 0.01568  2.72924E-04 0.02748 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.69181E-01 0.01438  9.27817E-03 0.02669  3.08955E-02 0.00045  1.10618E-01 0.00048  3.22776E-01 0.00035  1.29552E+00 0.00185  7.96443E+00 0.01446 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.22810E-03 0.00919  1.35171E-04 0.05613  9.05837E-04 0.02436  8.17204E-04 0.02393  2.24355E-03 0.01411  8.69996E-04 0.02371  2.56341E-04 0.04410 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.91269E-01 0.02382  1.25677E-02 0.00106  3.09003E-02 0.00063  1.10563E-01 0.00072  3.22858E-01 0.00049  1.29601E+00 0.00245  8.56103E+00 0.00901 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.82527E-05 0.00142  2.82448E-05 0.00142  2.98961E-05 0.01647 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.58487E-05 0.00128  2.58415E-05 0.00128  2.73518E-05 0.01643 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.26588E-03 0.01057  1.46397E-04 0.06324  9.07448E-04 0.02613  8.20268E-04 0.02720  2.30212E-03 0.01607  8.37808E-04 0.02619  2.51842E-04 0.04797 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.78402E-01 0.02527  1.25773E-02 0.00160  3.08703E-02 0.00085  1.10531E-01 0.00095  3.22906E-01 0.00062  1.30251E+00 0.00295  8.58732E+00 0.01244 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.77964E-05 0.00332  2.77921E-05 0.00331  2.70415E-05 0.04260 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.54313E-05 0.00326  2.54274E-05 0.00326  2.47085E-05 0.04245 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.15850E-03 0.03633  1.82111E-04 0.17438  8.45187E-04 0.09397  8.21223E-04 0.09017  2.21836E-03 0.05517  8.49544E-04 0.08776  2.42071E-04 0.17361 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.30513E-01 0.07792  1.25561E-02 0.00300  3.08282E-02 0.00210  1.10553E-01 0.00212  3.23027E-01 0.00171  1.30525E+00 0.00660  8.56364E+00 0.02891 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.15994E-03 0.03543  1.76858E-04 0.16738  7.96916E-04 0.09313  8.21081E-04 0.08854  2.24708E-03 0.05353  8.67069E-04 0.08541  2.50935E-04 0.16499 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.52563E-01 0.07793  1.25561E-02 0.00300  3.08272E-02 0.00210  1.10567E-01 0.00211  3.23006E-01 0.00169  1.30524E+00 0.00662  8.56217E+00 0.02890 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.86453E+02 0.03696 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.80617E-05 0.00084 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.56739E-05 0.00057 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.26597E-03 0.00648 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.87734E+02 0.00656 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.82846E-07 0.00076 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.69232E-06 0.00042  2.69216E-06 0.00042  2.72051E-06 0.00592 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.98607E-05 0.00090  3.98782E-05 0.00091  3.66439E-05 0.01121 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.36453E-01 0.00035  6.36680E-01 0.00035  6.09939E-01 0.00978 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06333E+01 0.01535 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.53529E+01 0.00050  3.36730E+01 0.00051 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.90634E+04 0.00272  3.03428E+05 0.00165  6.09855E+05 0.00098  6.51482E+05 0.00066  5.97407E+05 0.00070  6.41280E+05 0.00073  4.34776E+05 0.00057  3.85023E+05 0.00092  2.94052E+05 0.00081  2.40989E+05 0.00075  2.07657E+05 0.00082  1.87019E+05 0.00103  1.72714E+05 0.00078  1.64335E+05 0.00075  1.60149E+05 0.00097  1.38224E+05 0.00096  1.36794E+05 0.00092  1.35466E+05 0.00094  1.33106E+05 0.00111  2.60185E+05 0.00050  2.50915E+05 0.00071  1.81348E+05 0.00090  1.17616E+05 0.00117  1.35658E+05 0.00070  1.29118E+05 0.00075  1.15231E+05 0.00081  1.87234E+05 0.00064  4.32016E+04 0.00188  5.39752E+04 0.00113  4.90140E+04 0.00108  2.85819E+04 0.00207  4.93934E+04 0.00155  3.30006E+04 0.00171  2.69744E+04 0.00172  4.65031E+03 0.00337  4.03884E+03 0.00354  3.56541E+03 0.00400  3.42945E+03 0.00344  3.47925E+03 0.00300  3.84744E+03 0.00363  4.58145E+03 0.00356  4.62046E+03 0.00296  9.08144E+03 0.00282  1.49646E+04 0.00217  1.92767E+04 0.00201  5.17873E+04 0.00152  5.43570E+04 0.00129  5.83719E+04 0.00125  3.80499E+04 0.00153  2.69422E+04 0.00173  1.99680E+04 0.00192  2.36005E+04 0.00145  4.63047E+04 0.00168  6.57500E+04 0.00147  1.31423E+05 0.00147  2.08441E+05 0.00135  3.16706E+05 0.00162  2.04387E+05 0.00160  1.47181E+05 0.00155  1.06162E+05 0.00176  9.57804E+04 0.00170  9.43571E+04 0.00182  7.89379E+04 0.00190  5.34770E+04 0.00198  4.95964E+04 0.00180  4.41589E+04 0.00195  3.73523E+04 0.00209  2.95264E+04 0.00206  1.97834E+04 0.00220  7.03844E+03 0.00273 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.24369E-01 0.00050 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.48961E+18 0.00058  3.90820E+17 0.00158 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39017E-01 8.7E-05  1.56245E+00 0.00047 ];
INF_CAPT                  (idx, [1:   4]) = [  7.91102E-03 0.00064  3.63327E-02 0.00089 ];
INF_ABS                   (idx, [1:   4]) = [  9.64078E-03 0.00052  6.47224E-02 0.00121 ];
INF_FISS                  (idx, [1:   4]) = [  1.72976E-03 0.00045  2.83898E-02 0.00163 ];
INF_NSF                   (idx, [1:   4]) = [  4.65368E-03 0.00044  7.58251E-02 0.00167 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.69037E+00 5.2E-05  2.67086E+00 5.3E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05543E+02 6.4E-06  2.05484E+02 9.8E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.62194E-08 0.00045  2.59022E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29383E-01 9.3E-05  1.49779E+00 0.00054 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44408E-01 0.00022  3.96564E-01 0.00062 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64542E-02 0.00028  9.35694E-02 0.00096 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32960E-03 0.00330  2.81523E-02 0.00288 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03387E-02 0.00185 -8.88342E-03 0.00637 ];
INF_SCATT5                (idx, [1:   4]) = [  1.57999E-04 0.10238  6.67867E-03 0.00897 ];
INF_SCATT6                (idx, [1:   4]) = [  5.11411E-03 0.00295 -1.71105E-02 0.00330 ];
INF_SCATT7                (idx, [1:   4]) = [  7.67068E-04 0.02033  4.25272E-04 0.08214 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29424E-01 9.3E-05  1.49779E+00 0.00054 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44409E-01 0.00022  3.96564E-01 0.00062 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64543E-02 0.00028  9.35694E-02 0.00096 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32973E-03 0.00331  2.81523E-02 0.00288 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03389E-02 0.00185 -8.88342E-03 0.00637 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.57984E-04 0.10242  6.67867E-03 0.00897 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.11411E-03 0.00295 -1.71105E-02 0.00330 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.67070E-04 0.02034  4.25272E-04 0.08214 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12397E-01 0.00032  1.01471E+00 0.00037 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56940E+00 0.00032  3.28501E-01 0.00037 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.59907E-03 0.00052  6.47224E-02 0.00121 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69820E-02 0.00026  6.57953E-02 0.00123 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12035E-01 8.4E-05  1.73475E-02 0.00051  1.14247E-03 0.00476  1.49665E+00 0.00054 ];
INF_S1                    (idx, [1:   8]) = [  2.39389E-01 0.00022  5.01919E-03 0.00107  4.96553E-04 0.00739  3.96067E-01 0.00062 ];
INF_S2                    (idx, [1:   8]) = [  9.80181E-02 0.00028 -1.56388E-03 0.00301  2.67744E-04 0.01064  9.33017E-02 0.00096 ];
INF_S3                    (idx, [1:   8]) = [  9.12135E-03 0.00264 -1.79175E-03 0.00159  9.48167E-05 0.02214  2.80575E-02 0.00286 ];
INF_S4                    (idx, [1:   8]) = [ -9.77679E-03 0.00194 -5.61910E-04 0.00467 -9.25966E-07 1.00000 -8.88249E-03 0.00631 ];
INF_S5                    (idx, [1:   8]) = [  1.18776E-04 0.14060  3.92226E-05 0.07216 -4.11574E-05 0.03910  6.71982E-03 0.00895 ];
INF_S6                    (idx, [1:   8]) = [  5.25293E-03 0.00290 -1.38820E-04 0.01571 -4.81421E-05 0.03027 -1.70623E-02 0.00333 ];
INF_S7                    (idx, [1:   8]) = [  9.40429E-04 0.01622 -1.73361E-04 0.01165 -4.44163E-05 0.02698  4.69688E-04 0.07354 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12077E-01 8.4E-05  1.73475E-02 0.00051  1.14247E-03 0.00476  1.49665E+00 0.00054 ];
INF_SP1                   (idx, [1:   8]) = [  2.39390E-01 0.00022  5.01919E-03 0.00107  4.96553E-04 0.00739  3.96067E-01 0.00062 ];
INF_SP2                   (idx, [1:   8]) = [  9.80182E-02 0.00028 -1.56388E-03 0.00301  2.67744E-04 0.01064  9.33017E-02 0.00096 ];
INF_SP3                   (idx, [1:   8]) = [  9.12149E-03 0.00264 -1.79175E-03 0.00159  9.48167E-05 0.02214  2.80575E-02 0.00286 ];
INF_SP4                   (idx, [1:   8]) = [ -9.77696E-03 0.00194 -5.61910E-04 0.00467 -9.25966E-07 1.00000 -8.88249E-03 0.00631 ];
INF_SP5                   (idx, [1:   8]) = [  1.18762E-04 0.14071  3.92226E-05 0.07216 -4.11574E-05 0.03910  6.71982E-03 0.00895 ];
INF_SP6                   (idx, [1:   8]) = [  5.25293E-03 0.00290 -1.38820E-04 0.01571 -4.81421E-05 0.03027 -1.70623E-02 0.00333 ];
INF_SP7                   (idx, [1:   8]) = [  9.40431E-04 0.01623 -1.73361E-04 0.01165 -4.44163E-05 0.02698  4.69688E-04 0.07354 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32075E-01 0.00061  1.17907E+00 0.00539 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33805E-01 0.00087  1.27068E+00 0.00873 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33625E-01 0.00073  1.27975E+00 0.00685 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28868E-01 0.00094  1.02567E+00 0.00433 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43633E+00 0.00062  2.82905E-01 0.00539 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42571E+00 0.00087  2.62820E-01 0.00896 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42681E+00 0.00073  2.60758E-01 0.00677 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45647E+00 0.00094  3.25136E-01 0.00428 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.22810E-03 0.00919  1.35171E-04 0.05613  9.05837E-04 0.02436  8.17204E-04 0.02393  2.24355E-03 0.01411  8.69996E-04 0.02371  2.56341E-04 0.04410 ];
LAMBDA                    (idx, [1:  14]) = [  7.91269E-01 0.02382  1.25677E-02 0.00106  3.09003E-02 0.00063  1.10563E-01 0.00072  3.22858E-01 0.00049  1.29601E+00 0.00245  8.56103E+00 0.00901 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0003' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:41 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:22:53 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284841 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.97173E-01  9.99947E-01  1.00228E+00  1.00244E+00  9.98157E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15253E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88475E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06550E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06977E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67841E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.52788E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.52695E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.43552E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.21507E-01 0.00109  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001186 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00024E+04 0.00100 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00024E+04 0.00100 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.08753E+02 ;
RUNNING_TIME              (idx, 1)        =  1.02195E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.31417E-01  4.31417E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.88917E-01  1.00000E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.00580E+02  3.36913E+00  2.64378E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  8.87233E-01  2.70333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.36500E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.02195E+02  1.26526E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97824 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00010E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81246E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.61464E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.71924E+04 ;
TOT_SF_RATE               (idx, 1)        =  6.12894E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.99430E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.39578E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62031E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.57963E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.77376E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.80174E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.98563E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.69080E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.88128E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.93266E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.97730E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.40460E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.53448E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.58280E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.96506E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.68688E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.39395E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.27645E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  6.01932E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.31340E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.10494E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 16 ;
BURNUP                     (idx, [1:  2])  = [  2.00000E+01  2.00019E+01 ];
BURN_DAYS                 (idx, 1)        =  5.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.96980E-01 0.00109 ];
U235_FISS                 (idx, [1:   4]) = [  5.62224E+15 0.00116  4.11334E-01 0.00098 ];
U238_FISS                 (idx, [1:   4]) = [  1.13948E+15 0.00270  8.33580E-02 0.00254 ];
PU239_FISS                (idx, [1:   4]) = [  5.76576E+15 0.00116  4.21817E-01 0.00089 ];
PU240_FISS                (idx, [1:   4]) = [  1.05126E+13 0.02923  7.68758E-04 0.02922 ];
PU241_FISS                (idx, [1:   4]) = [  1.11187E+15 0.00265  8.13454E-02 0.00256 ];
U235_CAPT                 (idx, [1:   4]) = [  1.25660E+15 0.00237  4.64335E-02 0.00235 ];
U238_CAPT                 (idx, [1:   4]) = [  9.84025E+15 0.00103  3.63571E-01 0.00072 ];
PU239_CAPT                (idx, [1:   4]) = [  3.17034E+15 0.00169  1.17142E-01 0.00160 ];
PU240_CAPT                (idx, [1:   4]) = [  2.16882E+15 0.00193  8.01330E-02 0.00179 ];
PU241_CAPT                (idx, [1:   4]) = [  3.98052E+14 0.00452  1.47066E-02 0.00445 ];
XE135_CAPT                (idx, [1:   4]) = [  7.76442E+14 0.00325  2.86926E-02 0.00328 ];
SM149_CAPT                (idx, [1:   4]) = [  2.28375E+14 0.00601  8.43951E-03 0.00603 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001186 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.03589E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001186 5.00804E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3291984 3.29656E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1662720 1.66499E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46482 4.64843E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001186 5.00804E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.81842E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50058E+05 5.4E-09  4.50058E+05 5.4E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.67600E+16 2.3E-05  3.67600E+16 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36520E+16 4.6E-06  1.36520E+16 4.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.70666E+16 0.00046  2.05953E+16 0.00045  6.47129E+15 0.00121 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.07186E+16 0.00031  3.42473E+16 0.00027  6.47129E+15 0.00121 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.10494E+16 0.00049  4.10494E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.92997E+18 0.00048  5.10455E+17 0.00046  1.41951E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.81639E+14 0.00455 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.11002E+16 0.00031 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.44993E+18 0.00065 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10192E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10192E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.49110E+00 0.00060 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.74936E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.34740E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23413E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93935E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96749E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.05111E-01 0.00069 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.96698E-01 0.00070 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.69264E+00 2.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05760E+02 4.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.96660E-01 0.00070  8.92117E-01 0.00070  4.58089E-03 0.01078 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.95890E-01 0.00032 ];
COL_KEFF                  (idx, [1:   2]) = [  8.95614E-01 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.95890E-01 0.00032 ];
ABS_KINF                  (idx, [1:   2]) = [  9.04299E-01 0.00031 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70560E+01 0.00023 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70624E+01 9.4E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.86009E-07 0.00400 ];
IMP_EALF                  (idx, [1:   2]) = [  7.78391E-07 0.00160 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.83655E-01 0.00286 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.82491E-01 0.00110 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.86325E-03 0.00635  1.52134E-04 0.03796  1.04976E-03 0.01493  9.27184E-04 0.01582  2.52863E-03 0.00957  9.23464E-04 0.01595  2.82085E-04 0.02754 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.67779E-01 0.01455  9.38306E-03 0.02614  3.08150E-02 0.00045  1.10826E-01 0.00054  3.22549E-01 0.00035  1.29301E+00 0.00180  7.73217E+00 0.01571 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.06366E-03 0.00957  1.20815E-04 0.05641  8.96574E-04 0.02263  8.19893E-04 0.02415  2.17973E-03 0.01512  8.04932E-04 0.02438  2.41716E-04 0.04108 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.75493E-01 0.02122  1.25810E-02 0.00119  3.08166E-02 0.00067  1.10831E-01 0.00077  3.22356E-01 0.00053  1.29131E+00 0.00269  8.45843E+00 0.00972 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.88452E-05 0.00156  2.88372E-05 0.00156  3.03349E-05 0.01648 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.58570E-05 0.00133  2.58497E-05 0.00134  2.72041E-05 0.01652 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.11288E-03 0.01095  1.24595E-04 0.07391  9.16182E-04 0.02750  8.39413E-04 0.02537  2.18833E-03 0.01809  7.85937E-04 0.02705  2.58418E-04 0.04718 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.01085E-01 0.02706  1.25755E-02 0.00180  3.08025E-02 0.00088  1.10973E-01 0.00099  3.22261E-01 0.00064  1.29096E+00 0.00356  8.51102E+00 0.01274 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.83038E-05 0.00358  2.82856E-05 0.00359  2.80876E-05 0.04376 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.53712E-05 0.00347  2.53550E-05 0.00349  2.51571E-05 0.04370 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.78559E-03 0.03960  9.00629E-05 0.25819  1.00403E-03 0.08822  8.37521E-04 0.09601  1.93570E-03 0.06084  6.96613E-04 0.09098  2.21666E-04 0.15151 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.87336E-01 0.08271  1.25677E-02 0.00438  3.06832E-02 0.00196  1.11110E-01 0.00234  3.22042E-01 0.00169  1.29461E+00 0.00810  8.40841E+00 0.03570 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.76820E-03 0.03822  8.70652E-05 0.24972  9.78462E-04 0.08575  8.17467E-04 0.08996  1.95964E-03 0.05942  7.02008E-04 0.09122  2.23565E-04 0.15088 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.81322E-01 0.08146  1.25677E-02 0.00438  3.06743E-02 0.00194  1.11113E-01 0.00234  3.22065E-01 0.00168  1.29429E+00 0.00812  8.41843E+00 0.03544 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.69872E+02 0.03957 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.85913E-05 0.00100 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.56297E-05 0.00064 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.90168E-03 0.00682 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.71525E+02 0.00688 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.80738E-07 0.00078 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.67616E-06 0.00042  2.67597E-06 0.00043  2.70919E-06 0.00576 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.98853E-05 0.00093  3.99027E-05 0.00094  3.65149E-05 0.01141 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.32534E-01 0.00034  6.32892E-01 0.00035  5.83589E-01 0.00980 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07787E+01 0.01468 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.52695E+01 0.00051  3.35956E+01 0.00054 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.94340E+04 0.00314  3.04421E+05 0.00138  6.10129E+05 0.00072  6.50585E+05 0.00061  5.97980E+05 0.00061  6.40480E+05 0.00057  4.35192E+05 0.00065  3.84931E+05 0.00078  2.94698E+05 0.00080  2.40525E+05 0.00093  2.07408E+05 0.00085  1.87021E+05 0.00079  1.72900E+05 0.00081  1.64382E+05 0.00063  1.59982E+05 0.00090  1.38181E+05 0.00089  1.36354E+05 0.00085  1.35325E+05 0.00077  1.33054E+05 0.00103  2.59609E+05 0.00081  2.51397E+05 0.00083  1.81273E+05 0.00077  1.17624E+05 0.00085  1.35598E+05 0.00075  1.29150E+05 0.00089  1.14974E+05 0.00087  1.86787E+05 0.00085  4.32710E+04 0.00198  5.37052E+04 0.00159  4.87641E+04 0.00132  2.86310E+04 0.00131  4.91785E+04 0.00145  3.28250E+04 0.00151  2.68285E+04 0.00170  4.57971E+03 0.00306  3.94462E+03 0.00353  3.44547E+03 0.00352  3.30278E+03 0.00429  3.36860E+03 0.00384  3.74705E+03 0.00363  4.44256E+03 0.00372  4.53618E+03 0.00430  8.96662E+03 0.00255  1.46699E+04 0.00266  1.91043E+04 0.00171  5.11934E+04 0.00124  5.40067E+04 0.00134  5.78529E+04 0.00148  3.77027E+04 0.00145  2.66372E+04 0.00155  1.97506E+04 0.00169  2.33412E+04 0.00151  4.58960E+04 0.00138  6.50256E+04 0.00189  1.30169E+05 0.00156  2.06595E+05 0.00172  3.14741E+05 0.00185  2.03322E+05 0.00174  1.46478E+05 0.00153  1.05878E+05 0.00176  9.52731E+04 0.00189  9.40514E+04 0.00174  7.86248E+04 0.00158  5.32769E+04 0.00147  4.93542E+04 0.00195  4.40190E+04 0.00221  3.72331E+04 0.00224  2.94411E+04 0.00242  1.97431E+04 0.00221  7.02237E+03 0.00287 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.04018E-01 0.00045 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.53071E+18 0.00039  3.99294E+17 0.00156 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39198E-01 8.0E-05  1.56423E+00 0.00041 ];
INF_CAPT                  (idx, [1:   4]) = [  8.06307E-03 0.00051  3.68799E-02 0.00086 ];
INF_ABS                   (idx, [1:   4]) = [  9.74456E-03 0.00043  6.46297E-02 0.00114 ];
INF_FISS                  (idx, [1:   4]) = [  1.68148E-03 0.00042  2.77498E-02 0.00153 ];
INF_NSF                   (idx, [1:   4]) = [  4.54735E-03 0.00044  7.46449E-02 0.00158 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.70437E+00 8.7E-05  2.68992E+00 5.9E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05739E+02 8.7E-06  2.05765E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.59449E-08 0.00030  2.59271E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29451E-01 8.8E-05  1.49958E+00 0.00048 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44375E-01 0.00019  3.96971E-01 0.00053 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64117E-02 0.00027  9.36007E-02 0.00089 ];
INF_SCATT3                (idx, [1:   4]) = [  7.26181E-03 0.00278  2.80887E-02 0.00198 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03983E-02 0.00190 -8.95080E-03 0.00649 ];
INF_SCATT5                (idx, [1:   4]) = [  1.53008E-04 0.10866  6.66021E-03 0.00885 ];
INF_SCATT6                (idx, [1:   4]) = [  5.13501E-03 0.00303 -1.72148E-02 0.00234 ];
INF_SCATT7                (idx, [1:   4]) = [  7.62107E-04 0.02156  5.66565E-04 0.06228 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29494E-01 8.8E-05  1.49958E+00 0.00048 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44376E-01 0.00019  3.96971E-01 0.00053 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64119E-02 0.00027  9.36007E-02 0.00089 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.26180E-03 0.00279  2.80887E-02 0.00198 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03985E-02 0.00191 -8.95080E-03 0.00649 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.53012E-04 0.10854  6.66021E-03 0.00885 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.13489E-03 0.00303 -1.72148E-02 0.00234 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.62318E-04 0.02157  5.66565E-04 0.06228 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12357E-01 0.00023  1.01617E+00 0.00036 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56968E+00 0.00023  3.28031E-01 0.00036 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.70146E-03 0.00044  6.46297E-02 0.00114 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69943E-02 0.00022  6.57876E-02 0.00131 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12204E-01 8.2E-05  1.72470E-02 0.00046  1.13695E-03 0.00360  1.49844E+00 0.00048 ];
INF_S1                    (idx, [1:   8]) = [  2.39389E-01 0.00018  4.98629E-03 0.00101  4.88345E-04 0.00504  3.96483E-01 0.00053 ];
INF_S2                    (idx, [1:   8]) = [  9.79745E-02 0.00026 -1.56282E-03 0.00246  2.67020E-04 0.00720  9.33336E-02 0.00090 ];
INF_S3                    (idx, [1:   8]) = [  9.04406E-03 0.00213 -1.78225E-03 0.00173  9.63807E-05 0.01869  2.79923E-02 0.00200 ];
INF_S4                    (idx, [1:   8]) = [ -9.84226E-03 0.00198 -5.56064E-04 0.00470  1.69707E-06 1.00000 -8.95250E-03 0.00642 ];
INF_S5                    (idx, [1:   8]) = [  1.14634E-04 0.14711  3.83745E-05 0.07537 -3.79868E-05 0.04416  6.69820E-03 0.00876 ];
INF_S6                    (idx, [1:   8]) = [  5.27223E-03 0.00288 -1.37224E-04 0.01690 -4.92525E-05 0.03228 -1.71656E-02 0.00236 ];
INF_S7                    (idx, [1:   8]) = [  9.31975E-04 0.01772 -1.69868E-04 0.01595 -4.47833E-05 0.02880  6.11348E-04 0.05802 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12247E-01 8.2E-05  1.72470E-02 0.00046  1.13695E-03 0.00360  1.49844E+00 0.00048 ];
INF_SP1                   (idx, [1:   8]) = [  2.39390E-01 0.00018  4.98629E-03 0.00101  4.88345E-04 0.00504  3.96483E-01 0.00053 ];
INF_SP2                   (idx, [1:   8]) = [  9.79747E-02 0.00026 -1.56282E-03 0.00246  2.67020E-04 0.00720  9.33336E-02 0.00090 ];
INF_SP3                   (idx, [1:   8]) = [  9.04405E-03 0.00214 -1.78225E-03 0.00173  9.63807E-05 0.01869  2.79923E-02 0.00200 ];
INF_SP4                   (idx, [1:   8]) = [ -9.84242E-03 0.00198 -5.56064E-04 0.00470  1.69707E-06 1.00000 -8.95250E-03 0.00642 ];
INF_SP5                   (idx, [1:   8]) = [  1.14637E-04 0.14693  3.83745E-05 0.07537 -3.79868E-05 0.04416  6.69820E-03 0.00876 ];
INF_SP6                   (idx, [1:   8]) = [  5.27212E-03 0.00288 -1.37224E-04 0.01690 -4.92525E-05 0.03228 -1.71656E-02 0.00236 ];
INF_SP7                   (idx, [1:   8]) = [  9.32187E-04 0.01772 -1.69868E-04 0.01595 -4.47833E-05 0.02880  6.11348E-04 0.05802 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31703E-01 0.00067  1.21550E+00 0.00821 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33493E-01 0.00078  1.32589E+00 0.01033 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33409E-01 0.00102  1.32325E+00 0.00953 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28292E-01 0.00117  1.04480E+00 0.00801 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43864E+00 0.00067  2.74680E-01 0.00822 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42761E+00 0.00078  2.52034E-01 0.01012 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42815E+00 0.00103  2.52472E-01 0.00982 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.46016E+00 0.00117  3.19533E-01 0.00803 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.06366E-03 0.00957  1.20815E-04 0.05641  8.96574E-04 0.02263  8.19893E-04 0.02415  2.17973E-03 0.01512  8.04932E-04 0.02438  2.41716E-04 0.04108 ];
LAMBDA                    (idx, [1:  14]) = [  7.75493E-01 0.02122  1.25810E-02 0.00119  3.08166E-02 0.00067  1.10831E-01 0.00077  3.22356E-01 0.00053  1.29131E+00 0.00269  8.45843E+00 0.00972 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0003' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:41 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:28:57 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284841 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.95990E-01  9.99372E-01  1.00331E+00  1.00241E+00  9.98914E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15701E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88430E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06506E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06933E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68148E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.52251E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.52158E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.42504E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.22769E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000611 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00012E+04 0.00092 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00012E+04 0.00092 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.39118E+02 ;
RUNNING_TIME              (idx, 1)        =  1.08275E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.31417E-01  4.31417E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.10100E-01  1.03500E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.06582E+02  3.36422E+00  2.63807E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  9.42967E-01  2.69833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.62333E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.08275E+02  1.26520E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97916 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99966E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81471E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.63987E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68458E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.35191E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.07351E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.45501E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.56632E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.53904E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  3.80501E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.91998E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.96139E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.21236E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  8.43625E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.99875E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.35556E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.37983E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.54000E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.57043E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.44435E+13 ;
CS137_ACTIVITY            (idx, 1)        =  3.35006E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.41745E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.22553E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.10450E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.32981E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.30630E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 17 ;
BURNUP                     (idx, [1:  2])  = [  2.50000E+01  2.50023E+01 ];
BURN_DAYS                 (idx, 1)        =  6.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.41752E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  4.64033E+15 0.00130  3.40872E-01 0.00111 ];
U238_FISS                 (idx, [1:   4]) = [  1.18518E+15 0.00286  8.70491E-02 0.00267 ];
PU239_FISS                (idx, [1:   4]) = [  6.27285E+15 0.00108  4.60799E-01 0.00086 ];
PU240_FISS                (idx, [1:   4]) = [  1.32576E+13 0.02695  9.74457E-04 0.02698 ];
PU241_FISS                (idx, [1:   4]) = [  1.47570E+15 0.00231  1.08405E-01 0.00222 ];
U235_CAPT                 (idx, [1:   4]) = [  1.03617E+15 0.00281  3.55914E-02 0.00282 ];
U238_CAPT                 (idx, [1:   4]) = [  1.02615E+16 0.00101  3.52430E-01 0.00076 ];
PU239_CAPT                (idx, [1:   4]) = [  3.44660E+15 0.00155  1.18382E-01 0.00151 ];
PU240_CAPT                (idx, [1:   4]) = [  2.56436E+15 0.00186  8.80744E-02 0.00175 ];
PU241_CAPT                (idx, [1:   4]) = [  5.25507E+14 0.00408  1.80504E-02 0.00408 ];
XE135_CAPT                (idx, [1:   4]) = [  7.87793E+14 0.00335  2.70610E-02 0.00337 ];
SM149_CAPT                (idx, [1:   4]) = [  2.44132E+14 0.00592  8.38473E-03 0.00587 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000611 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.14117E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000611 5.00814E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3375473 3.38060E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1578296 1.58069E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46842 4.68522E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000611 5.00814E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -8.84756E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50058E+05 5.4E-09  4.50058E+05 5.4E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.71160E+16 2.3E-05  3.71160E+16 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36203E+16 4.9E-06  1.36203E+16 4.9E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.91314E+16 0.00043  2.23649E+16 0.00044  6.76653E+15 0.00123 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.27517E+16 0.00030  3.59852E+16 0.00027  6.76653E+15 0.00123 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.30630E+16 0.00052  4.30630E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.02202E+18 0.00050  5.34471E+17 0.00048  1.48755E+18 0.00056 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.03559E+14 0.00477 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.31552E+16 0.00031 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.51873E+18 0.00067 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.09615E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.09615E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.45250E+00 0.00060 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.73540E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.28140E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23240E+00 0.00039 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93898E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96711E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.69699E-01 0.00064 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.61550E-01 0.00065 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.72506E+00 2.8E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06240E+02 4.9E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.61532E-01 0.00065  8.57246E-01 0.00065  4.30359E-03 0.01085 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.61510E-01 0.00031 ];
COL_KEFF                  (idx, [1:   2]) = [  8.62016E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.61510E-01 0.00031 ];
ABS_KINF                  (idx, [1:   2]) = [  8.69653E-01 0.00030 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.69965E+01 0.00026 ];
IMP_ALF                   (idx, [1:   2]) = [  1.69952E+01 0.00010 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  8.35028E-07 0.00446 ];
IMP_EALF                  (idx, [1:   2]) = [  8.32632E-07 0.00173 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.96306E-01 0.00295 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.95937E-01 0.00118 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.86963E-03 0.00678  1.44255E-04 0.04214  1.07813E-03 0.01453  9.30754E-04 0.01549  2.47280E-03 0.00995  9.60709E-04 0.01627  2.82983E-04 0.02835 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.67965E-01 0.01591  8.71760E-03 0.02989  3.06558E-02 0.00044  1.11096E-01 0.00059  3.22822E-01 0.00039  1.26598E+00 0.00237  7.55899E+00 0.01716 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.92241E-03 0.00985  1.25226E-04 0.06580  9.00876E-04 0.02329  7.92360E-04 0.02488  2.08878E-03 0.01490  7.65385E-04 0.02547  2.49780E-04 0.04435 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.74580E-01 0.02306  1.25943E-02 0.00129  3.06730E-02 0.00064  1.11161E-01 0.00087  3.22629E-01 0.00057  1.26254E+00 0.00331  8.33979E+00 0.01191 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.00173E-05 0.00159  3.00103E-05 0.00158  3.13096E-05 0.01804 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.58549E-05 0.00142  2.58490E-05 0.00142  2.69691E-05 0.01804 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.99237E-03 0.01101  1.24686E-04 0.07327  9.07889E-04 0.02601  8.11875E-04 0.02736  2.10772E-03 0.01700  7.98068E-04 0.02825  2.42133E-04 0.05378 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.83164E-01 0.02950  1.25710E-02 0.00177  3.06519E-02 0.00088  1.11062E-01 0.00113  3.22850E-01 0.00070  1.26195E+00 0.00455  8.64537E+00 0.01376 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.95418E-05 0.00365  2.95314E-05 0.00367  2.76415E-05 0.05787 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.54477E-05 0.00364  2.54389E-05 0.00365  2.38135E-05 0.05798 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.99349E-03 0.03848  1.46052E-04 0.21634  9.68980E-04 0.09674  7.47104E-04 0.09912  2.07859E-03 0.05910  7.80828E-04 0.09820  2.71935E-04 0.16530 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.79476E-01 0.08530  1.25896E-02 0.00448  3.06853E-02 0.00203  1.11161E-01 0.00272  3.23509E-01 0.00194  1.26568E+00 0.01067  8.30257E+00 0.04152 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.99475E-03 0.03756  1.41357E-04 0.21397  9.50231E-04 0.09252  7.52832E-04 0.09938  2.08118E-03 0.05738  7.96007E-04 0.09776  2.73142E-04 0.16617 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.68482E-01 0.08375  1.25896E-02 0.00448  3.06751E-02 0.00202  1.11174E-01 0.00272  3.23447E-01 0.00193  1.26654E+00 0.01062  8.31998E+00 0.04117 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.70926E+02 0.03866 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.97524E-05 0.00091 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.56274E-05 0.00066 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.03087E-03 0.00796 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.69275E+02 0.00820 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.78900E-07 0.00077 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.64960E-06 0.00042  2.64953E-06 0.00042  2.66408E-06 0.00584 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.01506E-05 0.00095  4.01722E-05 0.00096  3.59197E-05 0.01206 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.25988E-01 0.00033  6.26392E-01 0.00034  5.70277E-01 0.01042 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07936E+01 0.01488 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.52158E+01 0.00050  3.35225E+01 0.00057 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.05789E+04 0.00401  3.04540E+05 0.00165  6.11117E+05 0.00110  6.50845E+05 0.00058  5.97447E+05 0.00074  6.41013E+05 0.00071  4.35842E+05 0.00056  3.85596E+05 0.00064  2.94653E+05 0.00058  2.40856E+05 0.00060  2.07582E+05 0.00084  1.87020E+05 0.00068  1.72681E+05 0.00087  1.64198E+05 0.00064  1.60133E+05 0.00101  1.38077E+05 0.00101  1.36419E+05 0.00097  1.35402E+05 0.00086  1.33318E+05 0.00093  2.59965E+05 0.00065  2.51377E+05 0.00072  1.81566E+05 0.00066  1.17726E+05 0.00089  1.35450E+05 0.00075  1.29168E+05 0.00091  1.15203E+05 0.00097  1.85943E+05 0.00077  4.29230E+04 0.00164  5.32838E+04 0.00150  4.84591E+04 0.00124  2.83778E+04 0.00191  4.91638E+04 0.00155  3.24173E+04 0.00186  2.62146E+04 0.00163  4.44809E+03 0.00301  3.76838E+03 0.00409  3.29160E+03 0.00398  3.12411E+03 0.00493  3.22813E+03 0.00375  3.51830E+03 0.00356  4.23225E+03 0.00416  4.34931E+03 0.00441  8.73413E+03 0.00296  1.44381E+04 0.00236  1.89264E+04 0.00273  5.05858E+04 0.00156  5.33991E+04 0.00149  5.70390E+04 0.00135  3.71617E+04 0.00160  2.62193E+04 0.00173  1.94441E+04 0.00210  2.29416E+04 0.00192  4.51593E+04 0.00177  6.41257E+04 0.00155  1.28805E+05 0.00149  2.05134E+05 0.00158  3.12992E+05 0.00155  2.02507E+05 0.00174  1.46102E+05 0.00188  1.05493E+05 0.00183  9.51243E+04 0.00174  9.38801E+04 0.00178  7.85079E+04 0.00176  5.32945E+04 0.00190  4.93102E+04 0.00206  4.40087E+04 0.00186  3.71808E+04 0.00183  2.93948E+04 0.00221  1.97464E+04 0.00205  7.01145E+03 0.00256 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.70170E-01 0.00047 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.60565E+18 0.00050  4.16414E+17 0.00149 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39541E-01 0.00013  1.56680E+00 0.00046 ];
INF_CAPT                  (idx, [1:   4]) = [  8.33351E-03 0.00067  3.78295E-02 0.00090 ];
INF_ABS                   (idx, [1:   4]) = [  9.93134E-03 0.00057  6.43825E-02 0.00114 ];
INF_FISS                  (idx, [1:   4]) = [  1.59783E-03 0.00043  2.65530E-02 0.00148 ];
INF_NSF                   (idx, [1:   4]) = [  4.36187E-03 0.00046  7.23290E-02 0.00154 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.72986E+00 7.8E-05  2.72394E+00 6.8E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06101E+02 9.2E-06  2.06272E+02 1.3E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.54869E-08 0.00042  2.59727E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29618E-01 0.00014  1.50245E+00 0.00053 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44383E-01 0.00022  3.97586E-01 0.00056 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64230E-02 0.00028  9.36444E-02 0.00112 ];
INF_SCATT3                (idx, [1:   4]) = [  7.29394E-03 0.00303  2.78872E-02 0.00274 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03639E-02 0.00227 -9.02607E-03 0.00614 ];
INF_SCATT5                (idx, [1:   4]) = [  1.90786E-04 0.10132  6.70571E-03 0.00657 ];
INF_SCATT6                (idx, [1:   4]) = [  5.16357E-03 0.00283 -1.72999E-02 0.00311 ];
INF_SCATT7                (idx, [1:   4]) = [  7.63737E-04 0.02134  4.48794E-04 0.12495 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29661E-01 0.00014  1.50245E+00 0.00053 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44383E-01 0.00022  3.97586E-01 0.00056 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64233E-02 0.00028  9.36444E-02 0.00112 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.29375E-03 0.00303  2.78872E-02 0.00274 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03639E-02 0.00228 -9.02607E-03 0.00614 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.90674E-04 0.10139  6.70571E-03 0.00657 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.16351E-03 0.00283 -1.72999E-02 0.00311 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.63638E-04 0.02131  4.48794E-04 0.12495 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12415E-01 0.00034  1.01892E+00 0.00042 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56926E+00 0.00034  3.27147E-01 0.00042 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.88768E-03 0.00059  6.43825E-02 0.00114 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69916E-02 0.00020  6.54774E-02 0.00117 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12549E-01 0.00013  1.70682E-02 0.00052  1.12543E-03 0.00350  1.50132E+00 0.00053 ];
INF_S1                    (idx, [1:   8]) = [  2.39451E-01 0.00021  4.93192E-03 0.00101  4.84160E-04 0.00772  3.97101E-01 0.00056 ];
INF_S2                    (idx, [1:   8]) = [  9.79803E-02 0.00027 -1.55728E-03 0.00173  2.67357E-04 0.00984  9.33770E-02 0.00112 ];
INF_S3                    (idx, [1:   8]) = [  9.06881E-03 0.00232 -1.77487E-03 0.00216  9.65033E-05 0.02559  2.77907E-02 0.00276 ];
INF_S4                    (idx, [1:   8]) = [ -9.80983E-03 0.00237 -5.54030E-04 0.00531  9.29825E-07 1.00000 -9.02700E-03 0.00616 ];
INF_S5                    (idx, [1:   8]) = [  1.46703E-04 0.13210  4.40833E-05 0.06959 -3.89154E-05 0.03297  6.74463E-03 0.00651 ];
INF_S6                    (idx, [1:   8]) = [  5.29280E-03 0.00276 -1.29221E-04 0.01896 -4.78275E-05 0.02452 -1.72521E-02 0.00312 ];
INF_S7                    (idx, [1:   8]) = [  9.32298E-04 0.01722 -1.68561E-04 0.01327 -4.29635E-05 0.02485  4.91758E-04 0.11459 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12593E-01 0.00013  1.70682E-02 0.00052  1.12543E-03 0.00350  1.50132E+00 0.00053 ];
INF_SP1                   (idx, [1:   8]) = [  2.39451E-01 0.00021  4.93192E-03 0.00101  4.84160E-04 0.00772  3.97101E-01 0.00056 ];
INF_SP2                   (idx, [1:   8]) = [  9.79806E-02 0.00027 -1.55728E-03 0.00173  2.67357E-04 0.00984  9.33770E-02 0.00112 ];
INF_SP3                   (idx, [1:   8]) = [  9.06862E-03 0.00232 -1.77487E-03 0.00216  9.65033E-05 0.02559  2.77907E-02 0.00276 ];
INF_SP4                   (idx, [1:   8]) = [ -9.80988E-03 0.00238 -5.54030E-04 0.00531  9.29825E-07 1.00000 -9.02700E-03 0.00616 ];
INF_SP5                   (idx, [1:   8]) = [  1.46590E-04 0.13222  4.40833E-05 0.06959 -3.89154E-05 0.03297  6.74463E-03 0.00651 ];
INF_SP6                   (idx, [1:   8]) = [  5.29273E-03 0.00275 -1.29221E-04 0.01896 -4.78275E-05 0.02452 -1.72521E-02 0.00312 ];
INF_SP7                   (idx, [1:   8]) = [  9.32200E-04 0.01719 -1.68561E-04 0.01327 -4.29635E-05 0.02485  4.91758E-04 0.11459 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31574E-01 0.00071  1.20646E+00 0.00796 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33254E-01 0.00112  1.31801E+00 0.00965 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33318E-01 0.00082  1.30656E+00 0.01064 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28233E-01 0.00106  1.04016E+00 0.00719 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43944E+00 0.00071  2.76710E-01 0.00795 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42910E+00 0.00112  2.53476E-01 0.00973 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42869E+00 0.00082  2.55798E-01 0.01035 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.46054E+00 0.00105  3.20856E-01 0.00711 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.92241E-03 0.00985  1.25226E-04 0.06580  9.00876E-04 0.02329  7.92360E-04 0.02488  2.08878E-03 0.01490  7.65385E-04 0.02547  2.49780E-04 0.04435 ];
LAMBDA                    (idx, [1:  14]) = [  7.74580E-01 0.02306  1.25943E-02 0.00129  3.06730E-02 0.00064  1.11161E-01 0.00087  3.22629E-01 0.00057  1.26254E+00 0.00331  8.33979E+00 0.01191 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0003' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:41 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:35:01 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284841 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.95933E-01  1.00173E+00  1.00244E+00  1.00197E+00  9.97924E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.6E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16168E-02 0.00104  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88383E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06043E-01 0.00013  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06471E-01 0.00013  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68535E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.51152E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.51059E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.42356E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.23645E-01 0.00105  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001138 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00023E+04 0.00099 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00023E+04 0.00099 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.69424E+02 ;
RUNNING_TIME              (idx, 1)        =  1.14343E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.31417E-01  4.31417E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.31533E-01  1.04667E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.12573E+02  3.35828E+00  2.63237E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  9.98033E-01  2.75833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.96167E-02  8.16667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.14343E+02  1.26488E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97998 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00065E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81667E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.76849E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68407E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.54106E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.17500E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.53120E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.59346E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.53091E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  5.05767E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.08020E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.16390E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.86609E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  8.93770E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.09359E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.70213E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.41713E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.58360E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.61943E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.12553E+13 ;
CS137_ACTIVITY            (idx, 1)        =  4.00926E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.53317E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.19694E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.73540E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.35968E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.50555E+12 0.00053  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 18 ;
BURNUP                     (idx, [1:  2])  = [  3.00000E+01  3.00028E+01 ];
BURN_DAYS                 (idx, 1)        =  7.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.83656E-01 0.00114 ];
U235_FISS                 (idx, [1:   4]) = [  3.81437E+15 0.00147  2.80545E-01 0.00130 ];
U238_FISS                 (idx, [1:   4]) = [  1.23871E+15 0.00267  9.10963E-02 0.00249 ];
PU239_FISS                (idx, [1:   4]) = [  6.69544E+15 0.00105  4.92462E-01 0.00087 ];
PU240_FISS                (idx, [1:   4]) = [  1.59109E+13 0.02308  1.17063E-03 0.02310 ];
PU241_FISS                (idx, [1:   4]) = [  1.79745E+15 0.00233  1.32195E-01 0.00218 ];
U235_CAPT                 (idx, [1:   4]) = [  8.51157E+14 0.00356  2.73578E-02 0.00353 ];
U238_CAPT                 (idx, [1:   4]) = [  1.06888E+16 0.00107  3.43527E-01 0.00075 ];
PU239_CAPT                (idx, [1:   4]) = [  3.67609E+15 0.00151  1.18159E-01 0.00147 ];
PU240_CAPT                (idx, [1:   4]) = [  2.91207E+15 0.00186  9.35885E-02 0.00167 ];
PU241_CAPT                (idx, [1:   4]) = [  6.41483E+14 0.00339  2.06195E-02 0.00339 ];
XE135_CAPT                (idx, [1:   4]) = [  7.99770E+14 0.00343  2.57057E-02 0.00339 ];
SM149_CAPT                (idx, [1:   4]) = [  2.53837E+14 0.00601  8.15977E-03 0.00603 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001138 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.11522E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001138 5.00812E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3447849 3.45278E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1506919 1.50895E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46370 4.63872E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001138 5.00812E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -9.59262E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50058E+05 5.4E-09  4.50058E+05 5.4E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.74217E+16 2.1E-05  3.74217E+16 2.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35930E+16 4.6E-06  1.35930E+16 4.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.10848E+16 0.00047  2.40500E+16 0.00048  7.03483E+15 0.00128 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.46778E+16 0.00033  3.76430E+16 0.00030  7.03483E+15 0.00128 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.50555E+16 0.00053  4.50555E+16 0.00053  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.11142E+18 0.00052  5.58142E+17 0.00049  1.55328E+18 0.00059 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.18030E+14 0.00478 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.50958E+16 0.00033 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.58402E+18 0.00070 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.09038E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.09038E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.41683E+00 0.00062 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.72445E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.21778E-01 0.00038 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23255E+00 0.00040 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93937E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96766E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.38663E-01 0.00069 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.30882E-01 0.00070 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.75301E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06653E+02 4.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.30982E-01 0.00070  8.26898E-01 0.00069  3.98432E-03 0.01173 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.31252E-01 0.00034 ];
COL_KEFF                  (idx, [1:   2]) = [  8.30682E-01 0.00053 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.31252E-01 0.00034 ];
ABS_KINF                  (idx, [1:   2]) = [  8.39041E-01 0.00033 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.69267E+01 0.00025 ];
IMP_ALF                   (idx, [1:   2]) = [  1.69297E+01 0.00010 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  8.94944E-07 0.00423 ];
IMP_EALF                  (idx, [1:   2]) = [  8.88977E-07 0.00174 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.10909E-01 0.00280 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.09625E-01 0.00114 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.02956E-03 0.00642  1.32483E-04 0.04212  1.12551E-03 0.01435  9.35622E-04 0.01641  2.57377E-03 0.00957  9.87331E-04 0.01576  2.74847E-04 0.02958 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.26768E-01 0.01524  8.54710E-03 0.03102  3.05368E-02 0.00045  1.11470E-01 0.00061  3.23052E-01 0.00039  1.24935E+00 0.00311  7.04601E+00 0.02017 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.79660E-03 0.01031  1.00814E-04 0.06628  9.22098E-04 0.02318  7.43518E-04 0.02715  2.05588E-03 0.01514  7.71099E-04 0.02420  2.03187E-04 0.04684 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.10612E-01 0.02352  1.26174E-02 0.00140  3.05348E-02 0.00064  1.11514E-01 0.00089  3.23248E-01 0.00060  1.25567E+00 0.00341  7.98387E+00 0.01449 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.10990E-05 0.00150  3.10873E-05 0.00150  3.34461E-05 0.01927 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.58367E-05 0.00135  2.58271E-05 0.00135  2.77824E-05 0.01920 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.79869E-03 0.01175  1.12135E-04 0.07594  8.57744E-04 0.02757  7.49733E-04 0.03008  2.06440E-03 0.01795  7.97873E-04 0.02925  2.16807E-04 0.05570 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.26607E-01 0.02882  1.26476E-02 0.00242  3.05080E-02 0.00087  1.11538E-01 0.00128  3.23159E-01 0.00080  1.25667E+00 0.00469  7.95612E+00 0.02053 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.05473E-05 0.00370  3.05364E-05 0.00372  2.88616E-05 0.04400 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.53784E-05 0.00364  2.53693E-05 0.00366  2.39719E-05 0.04396 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.71314E-03 0.04143  8.94435E-05 0.28830  9.22535E-04 0.09303  8.01224E-04 0.10322  1.95619E-03 0.06210  7.05224E-04 0.10213  2.38525E-04 0.19608 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  6.73629E-01 0.08359  1.26020E-02 0.00640  3.04996E-02 0.00199  1.11618E-01 0.00298  3.22834E-01 0.00204  1.26396E+00 0.01085  7.32650E+00 0.06044 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.68604E-03 0.04069  8.32093E-05 0.30462  9.12314E-04 0.09051  7.75339E-04 0.10115  1.97278E-03 0.06063  7.11664E-04 0.10190  2.30730E-04 0.19895 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  6.78098E-01 0.08340  1.26020E-02 0.00640  3.04954E-02 0.00197  1.11628E-01 0.00297  3.22881E-01 0.00202  1.26401E+00 0.01085  7.32714E+00 0.06044 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.56488E+02 0.04230 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.08405E-05 0.00092 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.56218E-05 0.00062 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.67675E-03 0.00722 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.51670E+02 0.00720 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.76227E-07 0.00079 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.62998E-06 0.00044  2.62969E-06 0.00044  2.69054E-06 0.00605 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.02834E-05 0.00095  4.03016E-05 0.00096  3.66286E-05 0.01155 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.19707E-01 0.00037  6.20286E-01 0.00037  5.36099E-01 0.01071 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06657E+01 0.01392 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.51059E+01 0.00052  3.34145E+01 0.00057 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.14269E+04 0.00313  3.06022E+05 0.00191  6.11059E+05 0.00084  6.49761E+05 0.00075  5.98370E+05 0.00089  6.41034E+05 0.00061  4.35134E+05 0.00089  3.84964E+05 0.00069  2.94566E+05 0.00067  2.40739E+05 0.00085  2.07652E+05 0.00066  1.87091E+05 0.00077  1.72684E+05 0.00075  1.64219E+05 0.00090  1.60301E+05 0.00086  1.38235E+05 0.00108  1.36772E+05 0.00097  1.35442E+05 0.00100  1.33444E+05 0.00097  2.60080E+05 0.00073  2.51398E+05 0.00071  1.81602E+05 0.00074  1.17671E+05 0.00084  1.35730E+05 0.00107  1.29212E+05 0.00095  1.14766E+05 0.00068  1.85414E+05 0.00065  4.31466E+04 0.00172  5.26686E+04 0.00120  4.81672E+04 0.00161  2.82669E+04 0.00184  4.87958E+04 0.00156  3.19815E+04 0.00153  2.57204E+04 0.00182  4.30706E+03 0.00355  3.63019E+03 0.00426  3.14961E+03 0.00385  3.05515E+03 0.00360  3.11536E+03 0.00414  3.39231E+03 0.00282  4.12439E+03 0.00331  4.22921E+03 0.00363  8.49278E+03 0.00254  1.42534E+04 0.00167  1.86219E+04 0.00159  4.99485E+04 0.00155  5.24869E+04 0.00139  5.63293E+04 0.00146  3.66717E+04 0.00139  2.58077E+04 0.00162  1.91102E+04 0.00136  2.26739E+04 0.00151  4.43535E+04 0.00162  6.34505E+04 0.00111  1.27425E+05 0.00160  2.03151E+05 0.00151  3.10726E+05 0.00136  2.01315E+05 0.00149  1.45152E+05 0.00143  1.04935E+05 0.00163  9.46428E+04 0.00179  9.34154E+04 0.00159  7.81663E+04 0.00178  5.30335E+04 0.00156  4.91365E+04 0.00172  4.37767E+04 0.00185  3.70765E+04 0.00169  2.93844E+04 0.00173  1.96333E+04 0.00179  6.97261E+03 0.00250 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.38461E-01 0.00053 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.67919E+18 0.00055  4.32271E+17 0.00155 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39801E-01 0.00011  1.56755E+00 0.00042 ];
INF_CAPT                  (idx, [1:   4]) = [  8.56356E-03 0.00063  3.86489E-02 0.00087 ];
INF_ABS                   (idx, [1:   4]) = [  1.00904E-02 0.00053  6.41691E-02 0.00114 ];
INF_FISS                  (idx, [1:   4]) = [  1.52682E-03 0.00048  2.55201E-02 0.00156 ];
INF_NSF                   (idx, [1:   4]) = [  4.20287E-03 0.00049  7.02590E-02 0.00159 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.75270E+00 6.4E-05  2.75308E+00 5.2E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06420E+02 7.8E-06  2.06708E+02 1.0E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.50852E-08 0.00033  2.60081E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29703E-01 0.00012  1.50334E+00 0.00048 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44329E-01 0.00023  3.97510E-01 0.00056 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64381E-02 0.00031  9.35905E-02 0.00092 ];
INF_SCATT3                (idx, [1:   4]) = [  7.28523E-03 0.00419  2.80550E-02 0.00259 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03611E-02 0.00213 -8.96396E-03 0.00592 ];
INF_SCATT5                (idx, [1:   4]) = [  1.70856E-04 0.09987  6.83194E-03 0.00720 ];
INF_SCATT6                (idx, [1:   4]) = [  5.15240E-03 0.00287 -1.73419E-02 0.00316 ];
INF_SCATT7                (idx, [1:   4]) = [  7.62551E-04 0.01702  4.62277E-04 0.09097 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29747E-01 0.00012  1.50334E+00 0.00048 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44329E-01 0.00023  3.97510E-01 0.00056 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64386E-02 0.00031  9.35905E-02 0.00092 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.28537E-03 0.00420  2.80550E-02 0.00259 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03611E-02 0.00214 -8.96396E-03 0.00592 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.70684E-04 0.10021  6.83194E-03 0.00720 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.15247E-03 0.00286 -1.73419E-02 0.00316 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.62488E-04 0.01697  4.62277E-04 0.09097 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12407E-01 0.00029  1.02058E+00 0.00034 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56932E+00 0.00029  3.26612E-01 0.00034 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.00468E-02 0.00053  6.41691E-02 0.00114 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70018E-02 0.00023  6.53320E-02 0.00104 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12799E-01 0.00011  1.69040E-02 0.00046  1.12312E-03 0.00484  1.50222E+00 0.00048 ];
INF_S1                    (idx, [1:   8]) = [  2.39453E-01 0.00022  4.87578E-03 0.00098  4.88724E-04 0.00751  3.97021E-01 0.00056 ];
INF_S2                    (idx, [1:   8]) = [  9.79875E-02 0.00031 -1.54940E-03 0.00247  2.69684E-04 0.00760  9.33208E-02 0.00092 ];
INF_S3                    (idx, [1:   8]) = [  9.03461E-03 0.00334 -1.74938E-03 0.00175  9.67959E-05 0.01610  2.79582E-02 0.00259 ];
INF_S4                    (idx, [1:   8]) = [ -9.82203E-03 0.00219 -5.39039E-04 0.00382  4.64674E-06 0.35330 -8.96861E-03 0.00591 ];
INF_S5                    (idx, [1:   8]) = [  1.29527E-04 0.12678  4.13295E-05 0.05892 -3.59965E-05 0.04160  6.86794E-03 0.00716 ];
INF_S6                    (idx, [1:   8]) = [  5.28549E-03 0.00269 -1.33094E-04 0.02002 -4.76336E-05 0.03257 -1.72943E-02 0.00318 ];
INF_S7                    (idx, [1:   8]) = [  9.29675E-04 0.01408 -1.67124E-04 0.01022 -4.53835E-05 0.02974  5.07660E-04 0.08187 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12843E-01 0.00011  1.69040E-02 0.00046  1.12312E-03 0.00484  1.50222E+00 0.00048 ];
INF_SP1                   (idx, [1:   8]) = [  2.39454E-01 0.00022  4.87578E-03 0.00098  4.88724E-04 0.00751  3.97021E-01 0.00056 ];
INF_SP2                   (idx, [1:   8]) = [  9.79880E-02 0.00031 -1.54940E-03 0.00247  2.69684E-04 0.00760  9.33208E-02 0.00092 ];
INF_SP3                   (idx, [1:   8]) = [  9.03475E-03 0.00334 -1.74938E-03 0.00175  9.67959E-05 0.01610  2.79582E-02 0.00259 ];
INF_SP4                   (idx, [1:   8]) = [ -9.82208E-03 0.00220 -5.39039E-04 0.00382  4.64674E-06 0.35330 -8.96861E-03 0.00591 ];
INF_SP5                   (idx, [1:   8]) = [  1.29355E-04 0.12727  4.13295E-05 0.05892 -3.59965E-05 0.04160  6.86794E-03 0.00716 ];
INF_SP6                   (idx, [1:   8]) = [  5.28557E-03 0.00268 -1.33094E-04 0.02002 -4.76336E-05 0.03257 -1.72943E-02 0.00318 ];
INF_SP7                   (idx, [1:   8]) = [  9.29612E-04 0.01404 -1.67124E-04 0.01022 -4.53835E-05 0.02974  5.07660E-04 0.08187 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31923E-01 0.00064  1.22057E+00 0.00692 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33540E-01 0.00086  1.32769E+00 0.00626 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33597E-01 0.00097  1.33263E+00 0.00874 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28710E-01 0.00097  1.04888E+00 0.00842 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43727E+00 0.00064  2.73406E-01 0.00682 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42733E+00 0.00086  2.51296E-01 0.00618 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42699E+00 0.00097  2.50590E-01 0.00874 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45748E+00 0.00096  3.18333E-01 0.00830 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.79660E-03 0.01031  1.00814E-04 0.06628  9.22098E-04 0.02318  7.43518E-04 0.02715  2.05588E-03 0.01514  7.71099E-04 0.02420  2.03187E-04 0.04684 ];
LAMBDA                    (idx, [1:  14]) = [  7.10612E-01 0.02352  1.26174E-02 0.00140  3.05348E-02 0.00064  1.11514E-01 0.00089  3.23248E-01 0.00060  1.25567E+00 0.00341  7.98387E+00 0.01449 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0003' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:41 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:41:04 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284841 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.94842E-01  1.00073E+00  1.00252E+00  1.00267E+00  9.99240E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16804E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88320E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05198E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05629E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68815E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.49793E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.49702E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.43647E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.25083E-01 0.00111  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001293 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00026E+04 0.00100 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00026E+04 0.00100 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.99642E+02 ;
RUNNING_TIME              (idx, 1)        =  1.20393E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.31417E-01  4.31417E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.52800E-01  1.04333E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.18546E+02  3.33797E+00  2.63522E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.05343E+00  2.74333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  5.22500E-02  9.16668E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.20393E+02  1.26462E+02 ];
CPU_USAGE                 (idx, 1)        = 4.98070 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99974E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81839E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.90322E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68850E+04 ;
TOT_SF_RATE               (idx, 1)        =  4.26437E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.27404E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.60666E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62914E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.52779E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  6.53987E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.24415E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.60178E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.05525E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  9.38083E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.18890E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.02068E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.45637E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.62560E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.66920E+14 ;
CS134_ACTIVITY            (idx, 1)        =  7.98049E+13 ;
CS137_ACTIVITY            (idx, 1)        =  4.66433E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.65341E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.17402E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.45278E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.38936E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.68432E+12 0.00056  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 19 ;
BURNUP                     (idx, [1:  2])  = [  3.50000E+01  3.50033E+01 ];
BURN_DAYS                 (idx, 1)        =  8.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  8.23797E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  3.08732E+15 0.00171  2.27680E-01 0.00151 ];
U238_FISS                 (idx, [1:   4]) = [  1.28478E+15 0.00286  9.47408E-02 0.00269 ];
PU239_FISS                (idx, [1:   4]) = [  7.03683E+15 0.00109  5.18952E-01 0.00080 ];
PU240_FISS                (idx, [1:   4]) = [  1.88528E+13 0.02237  1.38981E-03 0.02233 ];
PU241_FISS                (idx, [1:   4]) = [  2.08639E+15 0.00201  1.53874E-01 0.00192 ];
U235_CAPT                 (idx, [1:   4]) = [  6.88036E+14 0.00369  2.08945E-02 0.00368 ];
U238_CAPT                 (idx, [1:   4]) = [  1.10803E+16 0.00109  3.36447E-01 0.00075 ];
PU239_CAPT                (idx, [1:   4]) = [  3.87402E+15 0.00146  1.17647E-01 0.00142 ];
PU240_CAPT                (idx, [1:   4]) = [  3.23606E+15 0.00171  9.82666E-02 0.00158 ];
PU241_CAPT                (idx, [1:   4]) = [  7.44445E+14 0.00356  2.26083E-02 0.00356 ];
XE135_CAPT                (idx, [1:   4]) = [  8.04751E+14 0.00355  2.44398E-02 0.00356 ];
SM149_CAPT                (idx, [1:   4]) = [  2.61371E+14 0.00586  7.93909E-03 0.00592 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001293 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.30364E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001293 5.00830E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3510030 3.51505E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1445454 1.44744E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45809 4.58044E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001293 5.00830E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.93601E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50058E+05 5.4E-09  4.50058E+05 5.4E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.76935E+16 2.0E-05  3.76935E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35688E+16 4.4E-06  1.35688E+16 4.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.29494E+16 0.00044  2.57046E+16 0.00046  7.24474E+15 0.00118 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.65182E+16 0.00031  3.92734E+16 0.00030  7.24474E+15 0.00118 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.68432E+16 0.00056  4.68432E+16 0.00056  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.19206E+18 0.00051  5.81238E+17 0.00050  1.61083E+18 0.00057 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.29169E+14 0.00487 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.69473E+16 0.00032 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.64051E+18 0.00065 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.08462E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.08462E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.38401E+00 0.00067 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.73127E-01 0.00036 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.15387E-01 0.00039 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23256E+00 0.00042 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93989E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96831E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.11523E-01 0.00071 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.04090E-01 0.00071 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.77796E+00 2.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.07023E+02 4.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.04046E-01 0.00073  8.00273E-01 0.00071  3.81677E-03 0.01242 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.04262E-01 0.00032 ];
COL_KEFF                  (idx, [1:   2]) = [  8.04798E-01 0.00056 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.04262E-01 0.00032 ];
ABS_KINF                  (idx, [1:   2]) = [  8.11693E-01 0.00032 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.68677E+01 0.00029 ];
IMP_ALF                   (idx, [1:   2]) = [  1.68685E+01 0.00011 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  9.50645E-07 0.00492 ];
IMP_EALF                  (idx, [1:   2]) = [  9.45150E-07 0.00182 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.21724E-01 0.00293 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.21669E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.04885E-03 0.00658  1.43877E-04 0.04243  1.13320E-03 0.01394  9.22382E-04 0.01643  2.53701E-03 0.01024  1.00687E-03 0.01504  3.05508E-04 0.02966 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.56463E-01 0.01614  8.79540E-03 0.02977  3.04445E-02 0.00040  1.11604E-01 0.00066  3.23418E-01 0.00040  1.23346E+00 0.00268  6.93596E+00 0.01992 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.68943E-03 0.01012  1.22081E-04 0.06143  8.78813E-04 0.02197  6.95889E-04 0.02632  1.95404E-03 0.01512  7.83210E-04 0.02370  2.55396E-04 0.04392 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.90853E-01 0.02497  1.26791E-02 0.00167  3.04476E-02 0.00059  1.11601E-01 0.00095  3.23407E-01 0.00063  1.23822E+00 0.00391  7.74499E+00 0.01555 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.21638E-05 0.00155  3.21498E-05 0.00155  3.48938E-05 0.01912 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.58553E-05 0.00142  2.58440E-05 0.00142  2.80588E-05 0.01914 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.72983E-03 0.01256  1.24603E-04 0.07804  9.02142E-04 0.02581  7.27560E-04 0.03049  1.97475E-03 0.01970  7.63228E-04 0.02988  2.37543E-04 0.05531 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.50546E-01 0.03029  1.26688E-02 0.00256  3.04551E-02 0.00081  1.11428E-01 0.00131  3.23227E-01 0.00083  1.23794E+00 0.00541  7.79083E+00 0.02216 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.12695E-05 0.00382  3.12603E-05 0.00383  2.95858E-05 0.04823 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.51363E-05 0.00376  2.51289E-05 0.00378  2.37935E-05 0.04829 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.74537E-03 0.04361  1.53665E-04 0.24785  8.82380E-04 0.09182  8.05232E-04 0.10497  1.79764E-03 0.06834  8.85151E-04 0.09685  2.21294E-04 0.21041 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.73383E-01 0.09672  1.25927E-02 0.00462  3.05034E-02 0.00202  1.11895E-01 0.00308  3.22913E-01 0.00226  1.23537E+00 0.01233  8.34845E+00 0.04904 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.71597E-03 0.04287  1.53605E-04 0.24140  8.59359E-04 0.09024  8.02591E-04 0.10399  1.78327E-03 0.06651  8.92382E-04 0.09673  2.24770E-04 0.20517 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.79487E-01 0.09793  1.25928E-02 0.00462  3.05023E-02 0.00202  1.11873E-01 0.00308  3.22798E-01 0.00224  1.23494E+00 0.01230  8.35031E+00 0.04905 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.52536E+02 0.04375 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.17594E-05 0.00109 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.55286E-05 0.00074 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.69633E-03 0.00736 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.47951E+02 0.00741 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.72402E-07 0.00078 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.61422E-06 0.00043  2.61389E-06 0.00043  2.67874E-06 0.00657 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.03781E-05 0.00089  4.03963E-05 0.00090  3.68062E-05 0.01236 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.13402E-01 0.00038  6.14062E-01 0.00038  5.16774E-01 0.01048 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08638E+01 0.01502 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.49702E+01 0.00050  3.33942E+01 0.00060 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.18092E+04 0.00339  3.07031E+05 0.00161  6.10965E+05 0.00100  6.51444E+05 0.00079  5.99306E+05 0.00059  6.41593E+05 0.00071  4.35576E+05 0.00070  3.85277E+05 0.00061  2.95523E+05 0.00077  2.41288E+05 0.00073  2.07973E+05 0.00077  1.87344E+05 0.00091  1.72995E+05 0.00093  1.64652E+05 0.00073  1.60288E+05 0.00102  1.38697E+05 0.00072  1.36701E+05 0.00090  1.35746E+05 0.00091  1.33281E+05 0.00085  2.59859E+05 0.00062  2.51788E+05 0.00077  1.81738E+05 0.00069  1.17864E+05 0.00128  1.35476E+05 0.00082  1.29352E+05 0.00079  1.14626E+05 0.00108  1.84606E+05 0.00074  4.28054E+04 0.00149  5.22193E+04 0.00177  4.77862E+04 0.00170  2.82422E+04 0.00174  4.84988E+04 0.00182  3.16011E+04 0.00229  2.53093E+04 0.00196  4.20373E+03 0.00337  3.50430E+03 0.00367  3.09571E+03 0.00370  2.96236E+03 0.00442  3.00762E+03 0.00411  3.30200E+03 0.00397  3.94684E+03 0.00280  4.14434E+03 0.00412  8.34974E+03 0.00290  1.40071E+04 0.00255  1.83612E+04 0.00227  4.93541E+04 0.00137  5.19138E+04 0.00150  5.57172E+04 0.00176  3.63125E+04 0.00127  2.54779E+04 0.00187  1.89057E+04 0.00225  2.22798E+04 0.00182  4.39357E+04 0.00141  6.25407E+04 0.00145  1.25944E+05 0.00146  2.01253E+05 0.00135  3.08116E+05 0.00144  1.99622E+05 0.00158  1.44202E+05 0.00172  1.04150E+05 0.00159  9.37643E+04 0.00161  9.27501E+04 0.00185  7.76129E+04 0.00181  5.27047E+04 0.00142  4.87906E+04 0.00157  4.34989E+04 0.00182  3.67883E+04 0.00167  2.91271E+04 0.00188  1.95505E+04 0.00196  6.94737E+03 0.00242 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.12239E-01 0.00060 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.74666E+18 0.00058  4.45457E+17 0.00149 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39874E-01 0.00010  1.56599E+00 0.00042 ];
INF_CAPT                  (idx, [1:   4]) = [  8.79777E-03 0.00064  3.94768E-02 0.00098 ];
INF_ABS                   (idx, [1:   4]) = [  1.02656E-02 0.00057  6.41874E-02 0.00118 ];
INF_FISS                  (idx, [1:   4]) = [  1.46785E-03 0.00045  2.47106E-02 0.00152 ];
INF_NSF                   (idx, [1:   4]) = [  4.07011E-03 0.00047  6.86746E-02 0.00157 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.77284E+00 9.0E-05  2.77915E+00 6.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06705E+02 9.3E-06  2.07097E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.46523E-08 0.00037  2.60269E-06 0.00013 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29609E-01 0.00011  1.50186E+00 0.00048 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44055E-01 0.00020  3.96891E-01 0.00050 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63083E-02 0.00026  9.34322E-02 0.00083 ];
INF_SCATT3                (idx, [1:   4]) = [  7.28034E-03 0.00306  2.80317E-02 0.00230 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03176E-02 0.00239 -9.03774E-03 0.00625 ];
INF_SCATT5                (idx, [1:   4]) = [  2.19441E-04 0.08017  6.68713E-03 0.00971 ];
INF_SCATT6                (idx, [1:   4]) = [  5.15691E-03 0.00358 -1.72791E-02 0.00303 ];
INF_SCATT7                (idx, [1:   4]) = [  7.78778E-04 0.01620  5.52287E-04 0.08767 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29653E-01 0.00011  1.50186E+00 0.00048 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44056E-01 0.00020  3.96891E-01 0.00050 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63085E-02 0.00026  9.34322E-02 0.00083 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.28049E-03 0.00306  2.80317E-02 0.00230 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03179E-02 0.00239 -9.03774E-03 0.00625 ];
INF_SCATTP5               (idx, [1:   4]) = [  2.19399E-04 0.08017  6.68713E-03 0.00971 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.15685E-03 0.00357 -1.72791E-02 0.00303 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.78613E-04 0.01621  5.52287E-04 0.08767 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12515E-01 0.00025  1.02047E+00 0.00034 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56852E+00 0.00025  3.26648E-01 0.00034 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.02211E-02 0.00057  6.41874E-02 0.00118 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69867E-02 0.00026  6.52386E-02 0.00116 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12887E-01 9.9E-05  1.67219E-02 0.00047  1.11645E-03 0.00363  1.50075E+00 0.00048 ];
INF_S1                    (idx, [1:   8]) = [  2.39241E-01 0.00020  4.81416E-03 0.00095  4.83566E-04 0.00508  3.96408E-01 0.00050 ];
INF_S2                    (idx, [1:   8]) = [  9.78473E-02 0.00027 -1.53905E-03 0.00227  2.63405E-04 0.00734  9.31688E-02 0.00084 ];
INF_S3                    (idx, [1:   8]) = [  9.00835E-03 0.00236 -1.72802E-03 0.00194  9.25449E-05 0.02201  2.79392E-02 0.00230 ];
INF_S4                    (idx, [1:   8]) = [ -9.79137E-03 0.00246 -5.26249E-04 0.00494  1.50211E-06 1.00000 -9.03924E-03 0.00621 ];
INF_S5                    (idx, [1:   8]) = [  1.74469E-04 0.10383  4.49720E-05 0.04342 -3.77103E-05 0.04352  6.72484E-03 0.00962 ];
INF_S6                    (idx, [1:   8]) = [  5.28581E-03 0.00333 -1.28898E-04 0.01731 -4.67385E-05 0.02326 -1.72323E-02 0.00303 ];
INF_S7                    (idx, [1:   8]) = [  9.43591E-04 0.01331 -1.64814E-04 0.01202 -4.41203E-05 0.02913  5.96407E-04 0.08091 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12932E-01 9.9E-05  1.67219E-02 0.00047  1.11645E-03 0.00363  1.50075E+00 0.00048 ];
INF_SP1                   (idx, [1:   8]) = [  2.39242E-01 0.00020  4.81416E-03 0.00095  4.83566E-04 0.00508  3.96408E-01 0.00050 ];
INF_SP2                   (idx, [1:   8]) = [  9.78475E-02 0.00027 -1.53905E-03 0.00227  2.63405E-04 0.00734  9.31688E-02 0.00084 ];
INF_SP3                   (idx, [1:   8]) = [  9.00851E-03 0.00237 -1.72802E-03 0.00194  9.25449E-05 0.02201  2.79392E-02 0.00230 ];
INF_SP4                   (idx, [1:   8]) = [ -9.79164E-03 0.00246 -5.26249E-04 0.00494  1.50211E-06 1.00000 -9.03924E-03 0.00621 ];
INF_SP5                   (idx, [1:   8]) = [  1.74427E-04 0.10384  4.49720E-05 0.04342 -3.77103E-05 0.04352  6.72484E-03 0.00962 ];
INF_SP6                   (idx, [1:   8]) = [  5.28575E-03 0.00332 -1.28898E-04 0.01731 -4.67385E-05 0.02326 -1.72323E-02 0.00303 ];
INF_SP7                   (idx, [1:   8]) = [  9.43426E-04 0.01331 -1.64814E-04 0.01202 -4.41203E-05 0.02913  5.96407E-04 0.08091 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31723E-01 0.00065  1.19693E+00 0.00510 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33385E-01 0.00103  1.29184E+00 0.00696 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33463E-01 0.00116  1.31781E+00 0.00752 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28403E-01 0.00093  1.02821E+00 0.00472 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43852E+00 0.00065  2.78668E-01 0.00517 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42829E+00 0.00103  2.58333E-01 0.00700 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42782E+00 0.00116  2.53307E-01 0.00791 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45944E+00 0.00093  3.24363E-01 0.00474 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.68943E-03 0.01012  1.22081E-04 0.06143  8.78813E-04 0.02197  6.95889E-04 0.02632  1.95404E-03 0.01512  7.83210E-04 0.02370  2.55396E-04 0.04392 ];
LAMBDA                    (idx, [1:  14]) = [  7.90853E-01 0.02497  1.26791E-02 0.00167  3.04476E-02 0.00059  1.11601E-01 0.00095  3.23407E-01 0.00063  1.23822E+00 0.00391  7.74499E+00 0.01555 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0003' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:41 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:47:07 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284841 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.96199E-01  9.99693E-01  1.00351E+00  1.00186E+00  9.98736E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.17385E-02 0.00111  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88261E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04479E-01 0.00013  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04913E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69102E+00 0.00024  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.47808E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.47718E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.42682E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.25199E-01 0.00109  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001508 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00030E+04 0.00107 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00030E+04 0.00107 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  6.29794E+02 ;
RUNNING_TIME              (idx, 1)        =  1.26430E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.31417E-01  4.31417E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.75817E-01  1.21333E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.24503E+02  3.33467E+00  2.62170E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.11075E+00  2.94167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  5.49000E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.26430E+02  1.26430E+02 ];
CPU_USAGE                 (idx, 1)        = 4.98135 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00033E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81992E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  1.00410E+17 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.69716E+04 ;
TOT_SF_RATE               (idx, 1)        =  6.60137E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.36809E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.67914E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.67291E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.52920E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  8.26274E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.41124E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  7.28492E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.12628E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  9.77825E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.28496E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.31515E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.49731E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.66607E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.71938E+14 ;
CS134_ACTIVITY            (idx, 1)        =  9.94936E+13 ;
CS137_ACTIVITY            (idx, 1)        =  5.31539E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.77454E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.15596E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.21582E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.41816E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.85492E+12 0.00054  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 20 ;
BURNUP                     (idx, [1:  2])  = [  4.00000E+01  4.00039E+01 ];
BURN_DAYS                 (idx, 1)        =  1.00000E+03 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  8.59313E-01 0.00118 ];
U235_FISS                 (idx, [1:   4]) = [  2.46860E+15 0.00217  1.82198E-01 0.00200 ];
U238_FISS                 (idx, [1:   4]) = [  1.32223E+15 0.00293  9.75793E-02 0.00273 ];
PU239_FISS                (idx, [1:   4]) = [  7.34037E+15 0.00103  5.41800E-01 0.00080 ];
PU240_FISS                (idx, [1:   4]) = [  2.14865E+13 0.02181  1.58546E-03 0.02180 ];
PU241_FISS                (idx, [1:   4]) = [  2.33528E+15 0.00202  1.72368E-01 0.00190 ];
U235_CAPT                 (idx, [1:   4]) = [  5.49606E+14 0.00433  1.58634E-02 0.00427 ];
U238_CAPT                 (idx, [1:   4]) = [  1.14429E+16 0.00109  3.30275E-01 0.00078 ];
PU239_CAPT                (idx, [1:   4]) = [  4.01839E+15 0.00146  1.16001E-01 0.00148 ];
PU240_CAPT                (idx, [1:   4]) = [  3.49939E+15 0.00176  1.01005E-01 0.00161 ];
PU241_CAPT                (idx, [1:   4]) = [  8.36097E+14 0.00335  2.41374E-02 0.00339 ];
XE135_CAPT                (idx, [1:   4]) = [  8.11227E+14 0.00358  2.34184E-02 0.00359 ];
SM149_CAPT                (idx, [1:   4]) = [  2.70619E+14 0.00619  7.81138E-03 0.00617 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001508 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.35737E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001508 5.00836E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3563000 3.56800E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1393589 1.39544E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44919 4.49233E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001508 5.00836E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -9.49949E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50058E+05 5.4E-09  4.50058E+05 5.4E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.79241E+16 2.1E-05  3.79241E+16 2.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35484E+16 4.3E-06  1.35484E+16 4.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.46344E+16 0.00046  2.72222E+16 0.00047  7.41223E+15 0.00134 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.81829E+16 0.00033  4.07707E+16 0.00031  7.41223E+15 0.00134 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.85492E+16 0.00054  4.85492E+16 0.00054  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.26577E+18 0.00053  6.02339E+17 0.00050  1.66343E+18 0.00060 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.36237E+14 0.00512 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.86191E+16 0.00033 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.69070E+18 0.00072 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.07886E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.07886E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.35606E+00 0.00069 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.73585E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.09436E-01 0.00038 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23313E+00 0.00043 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94137E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96859E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  7.88281E-01 0.00074 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  7.81198E-01 0.00074 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.79915E+00 2.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.07333E+02 4.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  7.81177E-01 0.00074  7.77669E-01 0.00074  3.52902E-03 0.01223 ];
IMP_KEFF                  (idx, [1:   2]) = [  7.81374E-01 0.00034 ];
COL_KEFF                  (idx, [1:   2]) = [  7.81261E-01 0.00054 ];
ABS_KEFF                  (idx, [1:   2]) = [  7.81374E-01 0.00034 ];
ABS_KINF                  (idx, [1:   2]) = [  7.88458E-01 0.00033 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.68164E+01 0.00029 ];
IMP_ALF                   (idx, [1:   2]) = [  1.68117E+01 0.00011 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  1.00076E-06 0.00493 ];
IMP_EALF                  (idx, [1:   2]) = [  1.00035E-06 0.00181 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.32575E-01 0.00295 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.32821E-01 0.00111 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.06751E-03 0.00697  1.24290E-04 0.04462  1.16384E-03 0.01508  9.29720E-04 0.01715  2.51354E-03 0.01012  1.05007E-03 0.01576  2.86062E-04 0.02950 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.30342E-01 0.01518  8.25322E-03 0.03289  3.03570E-02 0.00036  1.11634E-01 0.00212  3.23510E-01 0.00042  1.21493E+00 0.00291  6.97352E+00 0.01999 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.58481E-03 0.01056  1.02929E-04 0.07404  8.56174E-04 0.02337  6.96020E-04 0.02761  1.91898E-03 0.01528  7.95972E-04 0.02534  2.14735E-04 0.04709 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.28489E-01 0.02333  1.27042E-02 0.00185  3.03500E-02 0.00057  1.12001E-01 0.00100  3.23589E-01 0.00067  1.22113E+00 0.00412  7.76423E+00 0.01594 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.29880E-05 0.00167  3.29696E-05 0.00167  3.66531E-05 0.01971 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.57625E-05 0.00150  2.57481E-05 0.00150  2.86347E-05 0.01977 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.52655E-03 0.01218  9.85869E-05 0.08476  8.80710E-04 0.02893  6.75492E-04 0.03298  1.83867E-03 0.01920  8.19063E-04 0.02981  2.14024E-04 0.05617 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.48986E-01 0.03049  1.27564E-02 0.00336  3.03559E-02 0.00075  1.11812E-01 0.00139  3.23592E-01 0.00092  1.21927E+00 0.00559  7.86012E+00 0.02421 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.21566E-05 0.00411  3.21364E-05 0.00412  2.96807E-05 0.05127 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.51133E-05 0.00405  2.50977E-05 0.00406  2.31694E-05 0.05118 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.52041E-03 0.04499  6.84260E-05 0.29358  1.05607E-03 0.09288  5.61206E-04 0.12444  1.82000E-03 0.06996  8.27797E-04 0.09884  1.86907E-04 0.22675 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.05881E-01 0.10123  1.26955E-02 0.00883  3.03768E-02 0.00188  1.11799E-01 0.00335  3.23651E-01 0.00241  1.23533E+00 0.01292  8.26982E+00 0.05719 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.50033E-03 0.04401  6.23582E-05 0.28633  1.05854E-03 0.09212  5.54440E-04 0.12362  1.82990E-03 0.06751  8.15547E-04 0.09843  1.79540E-04 0.21453 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.14022E-01 0.10120  1.26955E-02 0.00883  3.03808E-02 0.00188  1.11804E-01 0.00336  3.23633E-01 0.00240  1.23567E+00 0.01290  8.27214E+00 0.05721 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.41690E+02 0.04493 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.26484E-05 0.00109 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.54971E-05 0.00080 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.47773E-03 0.00855 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.37259E+02 0.00864 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.67145E-07 0.00084 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.59825E-06 0.00044  2.59807E-06 0.00044  2.63699E-06 0.00651 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.02476E-05 0.00098  4.02600E-05 0.00099  3.78601E-05 0.01180 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.07583E-01 0.00038  6.08316E-01 0.00038  5.00536E-01 0.01162 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06592E+01 0.01426 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.47718E+01 0.00054  3.32627E+01 0.00064 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.24826E+04 0.00343  3.07268E+05 0.00166  6.10598E+05 0.00097  6.50952E+05 0.00077  5.99118E+05 0.00059  6.41864E+05 0.00075  4.35585E+05 0.00077  3.86034E+05 0.00082  2.95302E+05 0.00071  2.41534E+05 0.00081  2.08129E+05 0.00066  1.87451E+05 0.00088  1.73496E+05 0.00077  1.64578E+05 0.00086  1.60440E+05 0.00084  1.38539E+05 0.00086  1.37134E+05 0.00107  1.35982E+05 0.00121  1.33528E+05 0.00117  2.60317E+05 0.00062  2.52248E+05 0.00070  1.81711E+05 0.00065  1.17897E+05 0.00092  1.35320E+05 0.00128  1.29352E+05 0.00070  1.14593E+05 0.00093  1.84022E+05 0.00067  4.27726E+04 0.00142  5.16679E+04 0.00130  4.74874E+04 0.00137  2.80550E+04 0.00197  4.82932E+04 0.00155  3.10466E+04 0.00170  2.49015E+04 0.00163  4.08877E+03 0.00356  3.42158E+03 0.00366  3.01559E+03 0.00471  2.94113E+03 0.00487  2.96069E+03 0.00326  3.21098E+03 0.00339  3.88778E+03 0.00384  4.07497E+03 0.00293  8.19717E+03 0.00256  1.38656E+04 0.00245  1.81434E+04 0.00228  4.87223E+04 0.00155  5.12975E+04 0.00136  5.51219E+04 0.00141  3.58705E+04 0.00115  2.51908E+04 0.00193  1.86208E+04 0.00180  2.20248E+04 0.00168  4.31834E+04 0.00177  6.16819E+04 0.00154  1.24225E+05 0.00155  1.98713E+05 0.00168  3.04122E+05 0.00175  1.97014E+05 0.00189  1.42160E+05 0.00208  1.02807E+05 0.00204  9.26857E+04 0.00176  9.16151E+04 0.00195  7.68040E+04 0.00222  5.20586E+04 0.00208  4.81520E+04 0.00184  4.30474E+04 0.00228  3.64054E+04 0.00154  2.88487E+04 0.00223  1.93104E+04 0.00217  6.90490E+03 0.00240 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  7.88344E-01 0.00067 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.80997E+18 0.00065  4.55847E+17 0.00189 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.40166E-01 0.00014  1.56420E+00 0.00049 ];
INF_CAPT                  (idx, [1:   4]) = [  9.00315E-03 0.00062  4.02365E-02 0.00114 ];
INF_ABS                   (idx, [1:   4]) = [  1.04237E-02 0.00055  6.43237E-02 0.00142 ];
INF_FISS                  (idx, [1:   4]) = [  1.42053E-03 0.00040  2.40872E-02 0.00192 ];
INF_NSF                   (idx, [1:   4]) = [  3.96384E-03 0.00041  6.74732E-02 0.00196 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.79039E+00 7.8E-05  2.80120E+00 6.0E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06946E+02 8.4E-06  2.07424E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.42986E-08 0.00034  2.60362E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29739E-01 0.00014  1.49987E+00 0.00056 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43979E-01 0.00025  3.96086E-01 0.00062 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62942E-02 0.00029  9.33497E-02 0.00102 ];
INF_SCATT3                (idx, [1:   4]) = [  7.23087E-03 0.00291  2.79992E-02 0.00235 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03666E-02 0.00193 -9.00557E-03 0.00612 ];
INF_SCATT5                (idx, [1:   4]) = [  1.77489E-04 0.10473  6.63584E-03 0.00598 ];
INF_SCATT6                (idx, [1:   4]) = [  5.14557E-03 0.00283 -1.73640E-02 0.00273 ];
INF_SCATT7                (idx, [1:   4]) = [  7.75852E-04 0.01780  4.74634E-04 0.10388 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29784E-01 0.00014  1.49987E+00 0.00056 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43979E-01 0.00025  3.96086E-01 0.00062 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62942E-02 0.00029  9.33497E-02 0.00102 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.23128E-03 0.00292  2.79992E-02 0.00235 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03667E-02 0.00193 -9.00557E-03 0.00612 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.77430E-04 0.10478  6.63584E-03 0.00598 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.14559E-03 0.00284 -1.73640E-02 0.00273 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.75737E-04 0.01779  4.74634E-04 0.10388 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12637E-01 0.00028  1.02018E+00 0.00041 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56762E+00 0.00028  3.26741E-01 0.00041 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.03788E-02 0.00055  6.43237E-02 0.00142 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69927E-02 0.00033  6.54468E-02 0.00141 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.13173E-01 0.00014  1.65658E-02 0.00051  1.11925E-03 0.00444  1.49875E+00 0.00057 ];
INF_S1                    (idx, [1:   8]) = [  2.39202E-01 0.00025  4.77655E-03 0.00108  4.83353E-04 0.00682  3.95603E-01 0.00062 ];
INF_S2                    (idx, [1:   8]) = [  9.78211E-02 0.00028 -1.52697E-03 0.00242  2.61795E-04 0.01059  9.30879E-02 0.00103 ];
INF_S3                    (idx, [1:   8]) = [  8.94688E-03 0.00225 -1.71601E-03 0.00208  9.53248E-05 0.02055  2.79039E-02 0.00237 ];
INF_S4                    (idx, [1:   8]) = [ -9.84357E-03 0.00185 -5.23083E-04 0.00661  2.09508E-07 1.00000 -9.00578E-03 0.00611 ];
INF_S5                    (idx, [1:   8]) = [  1.32826E-04 0.13166  4.46632E-05 0.06783 -3.75084E-05 0.04715  6.67335E-03 0.00589 ];
INF_S6                    (idx, [1:   8]) = [  5.27456E-03 0.00283 -1.28986E-04 0.02013 -4.70272E-05 0.03086 -1.73170E-02 0.00271 ];
INF_S7                    (idx, [1:   8]) = [  9.38835E-04 0.01463 -1.62983E-04 0.01165 -4.25165E-05 0.02864  5.17151E-04 0.09487 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.13218E-01 0.00014  1.65658E-02 0.00051  1.11925E-03 0.00444  1.49875E+00 0.00057 ];
INF_SP1                   (idx, [1:   8]) = [  2.39203E-01 0.00024  4.77655E-03 0.00108  4.83353E-04 0.00682  3.95603E-01 0.00062 ];
INF_SP2                   (idx, [1:   8]) = [  9.78212E-02 0.00028 -1.52697E-03 0.00242  2.61795E-04 0.01059  9.30879E-02 0.00103 ];
INF_SP3                   (idx, [1:   8]) = [  8.94730E-03 0.00226 -1.71601E-03 0.00208  9.53248E-05 0.02055  2.79039E-02 0.00237 ];
INF_SP4                   (idx, [1:   8]) = [ -9.84357E-03 0.00185 -5.23083E-04 0.00661  2.09508E-07 1.00000 -9.00578E-03 0.00611 ];
INF_SP5                   (idx, [1:   8]) = [  1.32767E-04 0.13174  4.46632E-05 0.06783 -3.75084E-05 0.04715  6.67335E-03 0.00589 ];
INF_SP6                   (idx, [1:   8]) = [  5.27457E-03 0.00284 -1.28986E-04 0.02013 -4.70272E-05 0.03086 -1.73170E-02 0.00271 ];
INF_SP7                   (idx, [1:   8]) = [  9.38720E-04 0.01463 -1.62983E-04 0.01165 -4.25165E-05 0.02864  5.17151E-04 0.09487 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31737E-01 0.00049  1.22059E+00 0.00763 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33494E-01 0.00076  1.32702E+00 0.01056 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33293E-01 0.00072  1.32939E+00 0.01055 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28500E-01 0.00075  1.05256E+00 0.00792 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43842E+00 0.00049  2.73472E-01 0.00760 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42761E+00 0.00076  2.51864E-01 0.01059 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42883E+00 0.00072  2.51393E-01 0.01026 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45881E+00 0.00075  3.17159E-01 0.00780 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.58481E-03 0.01056  1.02929E-04 0.07404  8.56174E-04 0.02337  6.96020E-04 0.02761  1.91898E-03 0.01528  7.95972E-04 0.02534  2.14735E-04 0.04709 ];
LAMBDA                    (idx, [1:  14]) = [  7.28489E-01 0.02333  1.27042E-02 0.00185  3.03500E-02 0.00057  1.12001E-01 0.00100  3.23589E-01 0.00067  1.22113E+00 0.00412  7.76423E+00 0.01594 ];

