
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0008' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:50:49 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:54:30 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299849 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00385E+00  9.86881E-01  1.00882E+00  9.98935E-01  1.00152E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 9.3E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15121E-02 0.00108  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88488E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.93540E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.93976E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.71860E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.55770E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.55700E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.22936E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.32971E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000427 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00009E+04 0.00067 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00009E+04 0.00067 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.65497E+01 ;
RUNNING_TIME              (idx, 1)        =  3.68033E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  4.62450E-01  4.62450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.33334E-03  1.33334E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.21625E+00  3.21625E+00  0.00000E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.67992E+00  0.00000E+00 ];
CPU_USAGE                 (idx, 1)        = 4.49680 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99891E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  8.61906E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.65648E+08 ;
TOT_DECAY_HEAT            (idx, 1)        =  6.59392E-04 ;
TOT_SF_RATE               (idx, 1)        =  7.40873E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  8.65648E+08 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  6.59392E-04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  7.92033E+03 ;
INGESTION_TOXICITY        (idx, 1)        =  4.18128E+01 ;
ACTINIDE_INH_TOX          (idx, 1)        =  7.92033E+03 ;
ACTINIDE_ING_TOX          (idx, 1)        =  4.18128E+01 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  1.12686E+08 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  8.65249E+08 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  3.57149E+08 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.90723E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 0 ;
BURNUP                     (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
BURN_DAYS                 (idx, 1)        =  0.00000E+00 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.78011E-01 0.00104 ];
U235_FISS                 (idx, [1:   4]) = [  1.30526E+16 0.00056  9.41315E-01 0.00017 ];
U238_FISS                 (idx, [1:   4]) = [  8.12272E+14 0.00284  5.85713E-02 0.00269 ];
U235_CAPT                 (idx, [1:   4]) = [  2.81038E+15 0.00143  1.86832E-01 0.00128 ];
U238_CAPT                 (idx, [1:   4]) = [  7.52940E+15 0.00100  5.00530E-01 0.00065 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000427 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.16289E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000427 5.00716E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2583537 2.58706E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2381726 2.38493E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 35164 3.51751E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000427 5.00716E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.68107E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 6.3E-09  4.50056E+05 6.3E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.41026E+16 1.0E-05  3.41026E+16 1.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38685E+16 1.2E-06  1.38685E+16 1.2E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.50467E+16 0.00048  1.04544E+16 0.00050  4.59237E+15 0.00103 ];
TOT_ABSRATE               (idx, [1:   6]) = [  2.89153E+16 0.00025  2.43229E+16 0.00021  4.59237E+15 0.00103 ];
TOT_SRCRATE               (idx, [1:   6]) = [  2.90723E+16 0.00052  2.90723E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.39186E+18 0.00047  3.81170E+17 0.00046  1.01069E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.04556E+14 0.00563 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  2.91198E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.03545E+18 0.00058 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12514E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.80783E+00 0.00035 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.87015E-01 0.00029 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.69819E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23958E+00 0.00032 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95435E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97519E-01 2.2E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.18127E+00 0.00046 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.17296E+00 0.00046 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.45899E+00 1.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02547E+02 1.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.17289E+00 0.00047  1.16477E+00 0.00047  8.19325E-03 0.00777 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.17287E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.17319E+00 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.17287E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.18118E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75872E+01 0.00017 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75908E+01 7.6E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.61332E-07 0.00307 ];
IMP_EALF                  (idx, [1:   2]) = [  4.58815E-07 0.00133 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.99375E-01 0.00288 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.98400E-01 0.00113 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.05343E-03 0.00560  1.74992E-04 0.03103  9.47504E-04 0.01377  9.44939E-04 0.01358  2.77461E-03 0.00755  9.01812E-04 0.01361  3.09575E-04 0.02374 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.23805E-01 0.01221  1.08170E-02 0.01761  3.16584E-02 0.00022  1.10241E-01 0.00029  3.20517E-01 0.00022  1.34571E+00 0.00016  8.70393E+00 0.00691 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.02100E-03 0.00773  2.11686E-04 0.04452  1.08833E-03 0.01879  1.08515E-03 0.02123  3.24440E-03 0.01118  1.03645E-03 0.02062  3.54991E-04 0.03444 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.19279E-01 0.01796  1.24907E-02 2.4E-06  3.16598E-02 0.00031  1.10220E-01 0.00041  3.20539E-01 0.00033  1.34577E+00 0.00023  8.90968E+00 0.00204 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.44361E-05 0.00105  2.44251E-05 0.00105  2.60038E-05 0.01059 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.86575E-05 0.00091  2.86446E-05 0.00092  3.04998E-05 0.01063 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.98850E-03 0.00774  2.06552E-04 0.04541  1.11412E-03 0.01857  1.09500E-03 0.02009  3.15141E-03 0.01068  1.05618E-03 0.02050  3.65231E-04 0.03442 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.33218E-01 0.01839  1.24908E-02 3.3E-06  3.16576E-02 0.00035  1.10245E-01 0.00050  3.20600E-01 0.00033  1.34539E+00 0.00027  8.90174E+00 0.00238 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.42274E-05 0.00223  2.42108E-05 0.00223  2.66439E-05 0.02671 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.84135E-05 0.00219  2.83940E-05 0.00219  3.12517E-05 0.02672 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.07885E-03 0.02369  2.50108E-04 0.14631  1.14517E-03 0.05928  1.15345E-03 0.05931  3.14308E-03 0.03474  1.02238E-03 0.06401  3.64647E-04 0.11054 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.18248E-01 0.05998  1.24907E-02 6.7E-06  3.16264E-02 0.00082  1.10230E-01 0.00105  3.20576E-01 0.00090  1.34551E+00 0.00059  8.95566E+00 0.00534 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.12719E-03 0.02262  2.59106E-04 0.13928  1.14055E-03 0.05836  1.14268E-03 0.05816  3.17094E-03 0.03338  1.04460E-03 0.06112  3.69316E-04 0.10348 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.38659E-01 0.05904  1.24907E-02 6.7E-06  3.16220E-02 0.00082  1.10229E-01 0.00104  3.20615E-01 0.00089  1.34546E+00 0.00059  8.95435E+00 0.00531 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.92998E+02 0.02388 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.43384E-05 0.00066 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.85431E-05 0.00043 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.08150E-03 0.00433 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.91001E+02 0.00435 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.98327E-07 0.00064 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.88713E-06 0.00043  2.88715E-06 0.00043  2.88468E-06 0.00481 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.96145E-05 0.00079  3.96343E-05 0.00079  3.68655E-05 0.00861 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.68011E-01 0.00032  6.67192E-01 0.00032  8.16781E-01 0.00908 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03247E+01 0.01303 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.55700E+01 0.00044  3.52860E+01 0.00040 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.16205E+04 0.00323  2.89862E+05 0.00184  6.03389E+05 0.00099  6.50932E+05 0.00087  5.99838E+05 0.00075  6.45048E+05 0.00055  4.37806E+05 0.00080  3.87965E+05 0.00073  2.96998E+05 0.00071  2.42406E+05 0.00102  2.09022E+05 0.00055  1.88436E+05 0.00080  1.74023E+05 0.00085  1.65353E+05 0.00070  1.61207E+05 0.00093  1.39258E+05 0.00086  1.37140E+05 0.00095  1.36217E+05 0.00086  1.33657E+05 0.00100  2.60784E+05 0.00053  2.51533E+05 0.00058  1.81205E+05 0.00062  1.17355E+05 0.00126  1.35321E+05 0.00101  1.27764E+05 0.00094  1.16203E+05 0.00096  1.91072E+05 0.00084  4.35777E+04 0.00146  5.46438E+04 0.00167  4.97415E+04 0.00147  2.87935E+04 0.00173  5.01343E+04 0.00189  3.41425E+04 0.00168  2.89310E+04 0.00209  5.51846E+03 0.00341  5.47640E+03 0.00307  5.62238E+03 0.00442  5.76859E+03 0.00408  5.73176E+03 0.00256  5.63487E+03 0.00432  5.84392E+03 0.00386  5.48939E+03 0.00392  1.03929E+04 0.00241  1.65852E+04 0.00192  2.11200E+04 0.00196  5.56914E+04 0.00157  5.87502E+04 0.00125  6.42409E+04 0.00098  4.38985E+04 0.00187  3.28919E+04 0.00137  2.54156E+04 0.00161  2.99939E+04 0.00187  5.70869E+04 0.00134  7.70007E+04 0.00103  1.47390E+05 0.00125  2.25824E+05 0.00116  3.35229E+05 0.00116  2.13034E+05 0.00110  1.51811E+05 0.00117  1.08930E+05 0.00137  9.76147E+04 0.00126  9.57986E+04 0.00140  7.98117E+04 0.00141  5.39190E+04 0.00147  4.95800E+04 0.00128  4.40759E+04 0.00130  3.71977E+04 0.00120  2.92231E+04 0.00148  1.95552E+04 0.00170  6.89664E+03 0.00237 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.18150E+00 0.00045 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.08879E+18 0.00045  3.03102E+17 0.00097 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37155E-01 9.1E-05  1.48670E+00 0.00036 ];
INF_CAPT                  (idx, [1:   4]) = [  6.42967E-03 0.00060  2.65486E-02 0.00028 ];
INF_ABS                   (idx, [1:   4]) = [  8.79613E-03 0.00044  6.38099E-02 0.00069 ];
INF_FISS                  (idx, [1:   4]) = [  2.36646E-03 0.00049  3.72613E-02 0.00099 ];
INF_NSF                   (idx, [1:   4]) = [  6.05033E-03 0.00049  9.07946E-02 0.00099 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.55670E+00 6.1E-05  2.43670E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03762E+02 5.7E-06  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  5.87756E-08 0.00043  2.53659E-06 0.00013 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28363E-01 9.6E-05  1.42291E+00 0.00041 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42984E-01 0.00018  3.76620E-01 0.00052 ];
INF_SCATT2                (idx, [1:   4]) = [  9.58361E-02 0.00025  8.97296E-02 0.00079 ];
INF_SCATT3                (idx, [1:   4]) = [  7.38681E-03 0.00250  2.70313E-02 0.00258 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02310E-02 0.00181 -8.02995E-03 0.00566 ];
INF_SCATT5                (idx, [1:   4]) = [  1.25334E-04 0.15570  6.21539E-03 0.00731 ];
INF_SCATT6                (idx, [1:   4]) = [  5.02802E-03 0.00312 -1.58429E-02 0.00381 ];
INF_SCATT7                (idx, [1:   4]) = [  7.14938E-04 0.01615  2.57164E-04 0.20459 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28401E-01 9.6E-05  1.42291E+00 0.00041 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42985E-01 0.00018  3.76620E-01 0.00052 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.58362E-02 0.00025  8.97296E-02 0.00079 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.38675E-03 0.00249  2.70313E-02 0.00258 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02314E-02 0.00181 -8.02995E-03 0.00566 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.25465E-04 0.15588  6.21539E-03 0.00731 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.02805E-03 0.00313 -1.58429E-02 0.00381 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.14846E-04 0.01617  2.57164E-04 0.20459 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.14217E-01 0.00027  9.59555E-01 0.00035 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55606E+00 0.00027  3.47384E-01 0.00035 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.75787E-03 0.00043  6.38099E-02 0.00069 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69350E-02 0.00018  6.49348E-02 0.00091 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10220E-01 9.3E-05  1.81424E-02 0.00037  1.13959E-03 0.00494  1.42177E+00 0.00042 ];
INF_S1                    (idx, [1:   8]) = [  2.37685E-01 0.00018  5.29954E-03 0.00091  4.81140E-04 0.00618  3.76139E-01 0.00053 ];
INF_S2                    (idx, [1:   8]) = [  9.73987E-02 0.00026 -1.56266E-03 0.00247  2.64535E-04 0.00858  8.94650E-02 0.00079 ];
INF_S3                    (idx, [1:   8]) = [  9.24900E-03 0.00212 -1.86219E-03 0.00127  9.46713E-05 0.01661  2.69366E-02 0.00258 ];
INF_S4                    (idx, [1:   8]) = [ -9.61039E-03 0.00192 -6.20614E-04 0.00432  2.37234E-06 0.78439 -8.03233E-03 0.00559 ];
INF_S5                    (idx, [1:   8]) = [  1.07740E-04 0.17426  1.75948E-05 0.14638 -3.89764E-05 0.03619  6.25437E-03 0.00724 ];
INF_S6                    (idx, [1:   8]) = [  5.17151E-03 0.00299 -1.43489E-04 0.00976 -4.76390E-05 0.03286 -1.57953E-02 0.00378 ];
INF_S7                    (idx, [1:   8]) = [  8.89227E-04 0.01308 -1.74289E-04 0.01342 -4.22879E-05 0.03748  2.99451E-04 0.17541 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10259E-01 9.3E-05  1.81424E-02 0.00037  1.13959E-03 0.00494  1.42177E+00 0.00042 ];
INF_SP1                   (idx, [1:   8]) = [  2.37685E-01 0.00018  5.29954E-03 0.00091  4.81140E-04 0.00618  3.76139E-01 0.00053 ];
INF_SP2                   (idx, [1:   8]) = [  9.73988E-02 0.00026 -1.56266E-03 0.00247  2.64535E-04 0.00858  8.94650E-02 0.00079 ];
INF_SP3                   (idx, [1:   8]) = [  9.24893E-03 0.00211 -1.86219E-03 0.00127  9.46713E-05 0.01661  2.69366E-02 0.00258 ];
INF_SP4                   (idx, [1:   8]) = [ -9.61080E-03 0.00192 -6.20614E-04 0.00432  2.37234E-06 0.78439 -8.03233E-03 0.00559 ];
INF_SP5                   (idx, [1:   8]) = [  1.07870E-04 0.17444  1.75948E-05 0.14638 -3.89764E-05 0.03619  6.25437E-03 0.00724 ];
INF_SP6                   (idx, [1:   8]) = [  5.17154E-03 0.00300 -1.43489E-04 0.00976 -4.76390E-05 0.03286 -1.57953E-02 0.00378 ];
INF_SP7                   (idx, [1:   8]) = [  8.89135E-04 0.01310 -1.74289E-04 0.01342 -4.22879E-05 0.03748  2.99451E-04 0.17541 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32285E-01 0.00058  1.04331E+00 0.00539 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33501E-01 0.00078  1.09916E+00 0.00791 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33756E-01 0.00088  1.11289E+00 0.00601 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29652E-01 0.00107  9.37880E-01 0.00534 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43503E+00 0.00058  3.19720E-01 0.00543 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42757E+00 0.00078  3.03721E-01 0.00798 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42601E+00 0.00087  2.99783E-01 0.00607 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45151E+00 0.00106  3.55656E-01 0.00538 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.02100E-03 0.00773  2.11686E-04 0.04452  1.08833E-03 0.01879  1.08515E-03 0.02123  3.24440E-03 0.01118  1.03645E-03 0.02062  3.54991E-04 0.03444 ];
LAMBDA                    (idx, [1:  14]) = [  8.19279E-01 0.01796  1.24907E-02 2.4E-06  3.16598E-02 0.00031  1.10220E-01 0.00041  3.20539E-01 0.00033  1.34577E+00 0.00023  8.90968E+00 0.00204 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0008' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:50:49 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:59:59 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299849 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99203E-01  9.91813E-01  1.00616E+00  1.00180E+00  1.00102E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14856E-02 0.00106  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88514E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.95642E-01 9.7E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.96078E-01 9.7E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.71190E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.53373E+01 0.00040  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.53302E+01 0.00040  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.04973E+00 0.00035  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.26993E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000770 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00015E+04 0.00074 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00015E+04 0.00074 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.38853E+01 ;
RUNNING_TIME              (idx, 1)        =  9.15245E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  4.62450E-01  4.62450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.41833E-02  6.18333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  8.62487E+00  3.06702E+00  2.34160E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.02000E-02  2.56333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.00001E-03  7.16666E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.15230E+00  1.15960E+02 ];
CPU_USAGE                 (idx, 1)        = 4.79493 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99901E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.35527E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  7.84659E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.75252E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.40859E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.16139E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  8.23553E+02 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  6.68518E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.67014E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.20541E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  1.74156E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.20846E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  3.36957E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.84561E+06 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  1.40461E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.28421E+11 ;
TE132_ACTIVITY            (idx, 1)        =  2.51404E+14 ;
I131_ACTIVITY             (idx, 1)        =  7.42281E+13 ;
I132_ACTIVITY             (idx, 1)        =  2.43571E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.69312E+07 ;
CS137_ACTIVITY            (idx, 1)        =  1.34693E+11 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  6.53345E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.64814E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.29912E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  8.96956E+16 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.01621E+12 0.00045  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 1 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E-01  1.00010E-01 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+00 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.90489E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  1.30057E+16 0.00057  9.38051E-01 0.00018 ];
U238_FISS                 (idx, [1:   4]) = [  8.35308E+14 0.00289  6.02395E-02 0.00274 ];
PU239_FISS                (idx, [1:   4]) = [  2.20493E+13 0.01587  1.59080E-03 0.01588 ];
U235_CAPT                 (idx, [1:   4]) = [  2.82296E+15 0.00146  1.75068E-01 0.00135 ];
U238_CAPT                 (idx, [1:   4]) = [  7.72494E+15 0.00097  4.79043E-01 0.00065 ];
PU239_CAPT                (idx, [1:   4]) = [  1.20988E+13 0.02250  7.50067E-04 0.02244 ];
PU240_CAPT                (idx, [1:   4]) = [  1.25778E+11 0.21383  7.82089E-06 0.21383 ];
XE135_CAPT                (idx, [1:   4]) = [  7.06311E+14 0.00293  4.38025E-02 0.00287 ];
SM149_CAPT                (idx, [1:   4]) = [  1.96869E+13 0.01782  1.22114E-03 0.01783 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000770 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.26079E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000770 5.00726E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2669626 2.67310E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2295437 2.29844E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 35707 3.57121E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000770 5.00726E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -2.32831E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 6.3E-09  4.50056E+05 6.3E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.41198E+16 1.1E-05  3.41198E+16 1.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38672E+16 1.3E-06  1.38672E+16 1.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.61233E+16 0.00043  1.14241E+16 0.00046  4.69916E+15 0.00094 ];
TOT_ABSRATE               (idx, [1:   6]) = [  2.99905E+16 0.00023  2.52913E+16 0.00021  4.69916E+15 0.00094 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.01621E+16 0.00045  3.01621E+16 0.00045  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.43710E+18 0.00042  3.92399E+17 0.00042  1.04470E+18 0.00046 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.15449E+14 0.00530 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.02059E+16 0.00024 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.06705E+18 0.00053 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12502E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12502E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.72130E+00 0.00040 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.90514E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.70081E-01 0.00030 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24947E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95363E-01 2.9E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97483E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.13919E+00 0.00051 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.13106E+00 0.00051 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46046E+00 1.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02566E+02 1.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.13088E+00 0.00054  1.12311E+00 0.00051  7.94376E-03 0.00763 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.13126E+00 0.00024 ];
COL_KEFF                  (idx, [1:   2]) = [  1.13133E+00 0.00044 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.13126E+00 0.00024 ];
ABS_KINF                  (idx, [1:   2]) = [  1.13940E+00 0.00023 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75186E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75172E+01 8.0E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.94326E-07 0.00337 ];
IMP_EALF                  (idx, [1:   2]) = [  4.93884E-07 0.00140 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.05357E-01 0.00291 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.05290E-01 0.00117 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.27744E-03 0.00579  1.78041E-04 0.03350  9.91144E-04 0.01403  9.85074E-04 0.01422  2.89600E-03 0.00825  9.25395E-04 0.01390  3.01787E-04 0.02411 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.96995E-01 0.01256  1.06921E-02 0.01836  3.16507E-02 0.00022  1.10128E-01 0.00027  3.20501E-01 0.00022  1.34598E+00 0.00017  8.68480E+00 0.00690 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.10573E-03 0.00811  2.06699E-04 0.04861  1.11395E-03 0.01999  1.11801E-03 0.01982  3.26837E-03 0.01165  1.05768E-03 0.02039  3.41029E-04 0.03588 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.99295E-01 0.01912  1.24908E-02 3.0E-06  3.16356E-02 0.00033  1.10215E-01 0.00041  3.20435E-01 0.00032  1.34617E+00 0.00023  8.88745E+00 0.00200 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.42864E-05 0.00119  2.42724E-05 0.00120  2.62306E-05 0.01053 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.74602E-05 0.00100  2.74444E-05 0.00101  2.96526E-05 0.01047 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.02569E-03 0.00783  1.98646E-04 0.04642  1.08312E-03 0.02046  1.10596E-03 0.01902  3.24459E-03 0.01209  1.06001E-03 0.02066  3.33363E-04 0.03576 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.96220E-01 0.01857  1.24908E-02 3.6E-06  3.16430E-02 0.00037  1.10161E-01 0.00043  3.20567E-01 0.00034  1.34607E+00 0.00025  8.87836E+00 0.00235 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.40496E-05 0.00247  2.40340E-05 0.00248  2.57874E-05 0.02522 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.71928E-05 0.00239  2.71751E-05 0.00240  2.91756E-05 0.02528 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.74431E-03 0.02359  1.65427E-04 0.14806  9.55360E-04 0.06960  9.70930E-04 0.06947  3.25692E-03 0.03506  1.07697E-03 0.06163  3.18703E-04 0.11462 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.28654E-01 0.05825  1.24907E-02 7.1E-06  3.16756E-02 0.00081  1.10247E-01 0.00114  3.20595E-01 0.00096  1.34718E+00 0.00055  8.90141E+00 0.00562 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.76087E-03 0.02292  1.64466E-04 0.14378  9.62861E-04 0.06681  9.65794E-04 0.06524  3.27249E-03 0.03435  1.07653E-03 0.06069  3.18730E-04 0.11092 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.15849E-01 0.05647  1.24908E-02 8.0E-06  3.16746E-02 0.00080  1.10244E-01 0.00114  3.20535E-01 0.00093  1.34718E+00 0.00055  8.90257E+00 0.00565 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.81975E+02 0.02381 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.41972E-05 0.00073 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.73601E-05 0.00046 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.83255E-03 0.00436 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.82453E+02 0.00444 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.89075E-07 0.00059 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.88497E-06 0.00041  2.88469E-06 0.00041  2.92059E-06 0.00516 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.87306E-05 0.00073  3.87493E-05 0.00074  3.62342E-05 0.00854 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.68232E-01 0.00030  6.67563E-01 0.00031  7.87164E-01 0.00875 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03063E+01 0.01255 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.53302E+01 0.00040  3.46770E+01 0.00043 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.14267E+04 0.00380  2.90375E+05 0.00140  6.02858E+05 0.00085  6.50411E+05 0.00068  5.99721E+05 0.00072  6.44963E+05 0.00072  4.38116E+05 0.00075  3.87955E+05 0.00055  2.96620E+05 0.00066  2.42160E+05 0.00070  2.08842E+05 0.00103  1.88528E+05 0.00077  1.73830E+05 0.00094  1.65156E+05 0.00069  1.60986E+05 0.00093  1.39232E+05 0.00108  1.37226E+05 0.00094  1.35866E+05 0.00090  1.33556E+05 0.00089  2.60323E+05 0.00081  2.51282E+05 0.00077  1.81075E+05 0.00062  1.17181E+05 0.00080  1.35093E+05 0.00089  1.27495E+05 0.00089  1.16121E+05 0.00091  1.90536E+05 0.00076  4.34880E+04 0.00147  5.45963E+04 0.00142  4.94753E+04 0.00144  2.87267E+04 0.00137  5.00857E+04 0.00161  3.40010E+04 0.00181  2.89965E+04 0.00176  5.52424E+03 0.00419  5.52807E+03 0.00402  5.64689E+03 0.00292  5.77659E+03 0.00321  5.79098E+03 0.00359  5.65018E+03 0.00375  5.84847E+03 0.00360  5.52617E+03 0.00353  1.04338E+04 0.00290  1.66614E+04 0.00249  2.12278E+04 0.00186  5.58943E+04 0.00126  5.87284E+04 0.00146  6.40718E+04 0.00115  4.37733E+04 0.00142  3.26839E+04 0.00226  2.51563E+04 0.00160  2.96879E+04 0.00132  5.64807E+04 0.00135  7.59858E+04 0.00066  1.45035E+05 0.00107  2.21629E+05 0.00105  3.27379E+05 0.00103  2.07546E+05 0.00123  1.48105E+05 0.00132  1.05979E+05 0.00124  9.50458E+04 0.00154  9.33684E+04 0.00158  7.78582E+04 0.00150  5.25997E+04 0.00184  4.84545E+04 0.00149  4.30977E+04 0.00186  3.64222E+04 0.00168  2.86108E+04 0.00148  1.91209E+04 0.00180  6.73887E+03 0.00170 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.13947E+00 0.00036 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.12894E+18 0.00035  3.08190E+17 0.00098 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37277E-01 0.00011  1.49427E+00 0.00038 ];
INF_CAPT                  (idx, [1:   4]) = [  6.43317E-03 0.00075  2.87526E-02 0.00032 ];
INF_ABS                   (idx, [1:   4]) = [  8.79157E-03 0.00058  6.51145E-02 0.00069 ];
INF_FISS                  (idx, [1:   4]) = [  2.35839E-03 0.00055  3.63619E-02 0.00099 ];
INF_NSF                   (idx, [1:   4]) = [  6.03061E-03 0.00055  8.86323E-02 0.00099 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.55709E+00 4.6E-05  2.43750E+00 4.0E-07 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03767E+02 4.8E-06  2.02280E+02 6.3E-08 ];
INF_INVV                  (idx, [1:   4]) = [  5.87980E-08 0.00046  2.53163E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28486E-01 0.00012  1.42916E+00 0.00044 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43159E-01 0.00017  3.78732E-01 0.00050 ];
INF_SCATT2                (idx, [1:   4]) = [  9.59115E-02 0.00033  9.04658E-02 0.00100 ];
INF_SCATT3                (idx, [1:   4]) = [  7.38272E-03 0.00371  2.72583E-02 0.00246 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02415E-02 0.00188 -8.20601E-03 0.00617 ];
INF_SCATT5                (idx, [1:   4]) = [  1.40924E-04 0.13923  6.16793E-03 0.00786 ];
INF_SCATT6                (idx, [1:   4]) = [  5.02591E-03 0.00345 -1.60287E-02 0.00273 ];
INF_SCATT7                (idx, [1:   4]) = [  7.21883E-04 0.02101  3.53979E-04 0.10756 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28525E-01 0.00012  1.42916E+00 0.00044 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43160E-01 0.00017  3.78732E-01 0.00050 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.59114E-02 0.00033  9.04658E-02 0.00100 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.38269E-03 0.00372  2.72583E-02 0.00246 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02415E-02 0.00187 -8.20601E-03 0.00617 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.40730E-04 0.13973  6.16793E-03 0.00786 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.02588E-03 0.00345 -1.60287E-02 0.00273 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.22111E-04 0.02098  3.53979E-04 0.10756 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.14049E-01 0.00033  9.62419E-01 0.00039 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55728E+00 0.00033  3.46351E-01 0.00039 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.75277E-03 0.00058  6.51145E-02 0.00069 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69525E-02 0.00017  6.62863E-02 0.00089 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10324E-01 0.00011  1.81622E-02 0.00035  1.17367E-03 0.00581  1.42798E+00 0.00044 ];
INF_S1                    (idx, [1:   8]) = [  2.37848E-01 0.00016  5.31082E-03 0.00100  4.93388E-04 0.00844  3.78239E-01 0.00050 ];
INF_S2                    (idx, [1:   8]) = [  9.74743E-02 0.00032 -1.56285E-03 0.00284  2.71004E-04 0.00771  9.01948E-02 0.00101 ];
INF_S3                    (idx, [1:   8]) = [  9.24609E-03 0.00295 -1.86337E-03 0.00201  9.30896E-05 0.02307  2.71652E-02 0.00247 ];
INF_S4                    (idx, [1:   8]) = [ -9.61581E-03 0.00194 -6.25724E-04 0.00422 -2.35081E-06 0.60420 -8.20366E-03 0.00619 ];
INF_S5                    (idx, [1:   8]) = [  1.30160E-04 0.14003  1.07635E-05 0.29146 -4.05631E-05 0.03492  6.20849E-03 0.00781 ];
INF_S6                    (idx, [1:   8]) = [  5.17349E-03 0.00324 -1.47576E-04 0.01246 -4.98382E-05 0.02891 -1.59788E-02 0.00274 ];
INF_S7                    (idx, [1:   8]) = [  8.94698E-04 0.01685 -1.72815E-04 0.01127 -4.38533E-05 0.04318  3.97832E-04 0.09675 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10363E-01 0.00011  1.81622E-02 0.00035  1.17367E-03 0.00581  1.42798E+00 0.00044 ];
INF_SP1                   (idx, [1:   8]) = [  2.37849E-01 0.00016  5.31082E-03 0.00100  4.93388E-04 0.00844  3.78239E-01 0.00050 ];
INF_SP2                   (idx, [1:   8]) = [  9.74743E-02 0.00032 -1.56285E-03 0.00284  2.71004E-04 0.00771  9.01948E-02 0.00101 ];
INF_SP3                   (idx, [1:   8]) = [  9.24606E-03 0.00295 -1.86337E-03 0.00201  9.30896E-05 0.02307  2.71652E-02 0.00247 ];
INF_SP4                   (idx, [1:   8]) = [ -9.61578E-03 0.00194 -6.25724E-04 0.00422 -2.35081E-06 0.60420 -8.20366E-03 0.00619 ];
INF_SP5                   (idx, [1:   8]) = [  1.29966E-04 0.14053  1.07635E-05 0.29146 -4.05631E-05 0.03492  6.20849E-03 0.00781 ];
INF_SP6                   (idx, [1:   8]) = [  5.17346E-03 0.00325 -1.47576E-04 0.01246 -4.98382E-05 0.02891 -1.59788E-02 0.00274 ];
INF_SP7                   (idx, [1:   8]) = [  8.94926E-04 0.01684 -1.72815E-04 0.01127 -4.38533E-05 0.04318  3.97832E-04 0.09675 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32305E-01 0.00050  1.05881E+00 0.00645 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33558E-01 0.00097  1.12096E+00 0.00926 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33645E-01 0.00081  1.12102E+00 0.00732 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29763E-01 0.00070  9.53798E-01 0.00515 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43490E+00 0.00050  3.15133E-01 0.00643 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42723E+00 0.00097  2.97969E-01 0.00915 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42669E+00 0.00081  2.97729E-01 0.00728 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45079E+00 0.00070  3.49703E-01 0.00516 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.10573E-03 0.00811  2.06699E-04 0.04861  1.11395E-03 0.01999  1.11801E-03 0.01982  3.26837E-03 0.01165  1.05768E-03 0.02039  3.41029E-04 0.03588 ];
LAMBDA                    (idx, [1:  14]) = [  7.99295E-01 0.01912  1.24908E-02 3.0E-06  3.16356E-02 0.00033  1.10215E-01 0.00041  3.20435E-01 0.00032  1.34617E+00 0.00023  8.88745E+00 0.00200 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0008' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:50:49 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:05:30 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299849 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00022E+00  9.92292E-01  1.00711E+00  1.00330E+00  9.97077E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.5E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14820E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88518E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.96333E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.96770E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.70723E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.52196E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.52124E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.98108E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.24729E-01 0.00113  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000684 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00014E+04 0.00073 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00014E+04 0.00073 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  7.15030E+01 ;
RUNNING_TIME              (idx, 1)        =  1.46812E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.62450E-01  4.62450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.93333E-02  7.40000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.40852E+01  3.07592E+00  2.38445E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.03117E-01  2.63500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  6.23334E-03  7.50001E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.46811E+01  1.14022E+02 ];
CPU_USAGE                 (idx, 1)        = 4.87037 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99881E+00 0.00021 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.54135E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.67643E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.83085E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.42125E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.53671E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.07389E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.13970E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.72344E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.64163E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  2.98701E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  8.49632E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.27114E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  1.79200E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  2.35989E+07 ;
SR90_ACTIVITY             (idx, 1)        =  6.38536E+11 ;
TE132_ACTIVITY            (idx, 1)        =  5.63103E+14 ;
I131_ACTIVITY             (idx, 1)        =  2.65256E+14 ;
I132_ACTIVITY             (idx, 1)        =  5.65937E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.87963E+09 ;
CS137_ACTIVITY            (idx, 1)        =  6.73936E+11 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.59614E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.63752E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  6.52777E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.10361E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.04878E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 2 ;
BURNUP                     (idx, [1:  2])  = [  5.00000E-01  5.00040E-01 ];
BURN_DAYS                 (idx, 1)        =  1.25000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.91031E-01 0.00112 ];
U235_FISS                 (idx, [1:   4]) = [  1.27378E+16 0.00060  9.18783E-01 0.00020 ];
U238_FISS                 (idx, [1:   4]) = [  8.50925E+14 0.00282  6.13691E-02 0.00266 ];
PU239_FISS                (idx, [1:   4]) = [  2.72799E+14 0.00506  1.96771E-02 0.00502 ];
PU240_FISS                (idx, [1:   4]) = [  1.24935E+10 0.70648  9.07341E-07 0.70640 ];
PU241_FISS                (idx, [1:   4]) = [  8.46886E+10 0.26380  6.15946E-06 0.26383 ];
U235_CAPT                 (idx, [1:   4]) = [  2.77279E+15 0.00149  1.68588E-01 0.00140 ];
U238_CAPT                 (idx, [1:   4]) = [  7.76397E+15 0.00103  4.72015E-01 0.00066 ];
PU239_CAPT                (idx, [1:   4]) = [  1.53163E+14 0.00631  9.31296E-03 0.00631 ];
PU240_CAPT                (idx, [1:   4]) = [  7.03066E+12 0.03009  4.27451E-04 0.03013 ];
PU241_CAPT                (idx, [1:   4]) = [  3.08356E+10 0.44544  1.86505E-06 0.44542 ];
XE135_CAPT                (idx, [1:   4]) = [  7.09206E+14 0.00288  4.31240E-02 0.00289 ];
SM149_CAPT                (idx, [1:   4]) = [  1.22672E+14 0.00686  7.45863E-03 0.00684 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000684 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.31350E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000684 5.00731E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2693801 2.69743E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2270750 2.27375E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 36133 3.61335E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000684 5.00731E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.84288E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 6.3E-09  4.50056E+05 6.3E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.42156E+16 1.1E-05  3.42156E+16 1.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38600E+16 1.3E-06  1.38600E+16 1.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.64528E+16 0.00047  1.17370E+16 0.00048  4.71585E+15 0.00104 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.03128E+16 0.00026  2.55969E+16 0.00022  4.71585E+15 0.00104 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.04878E+16 0.00049  3.04878E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.45007E+18 0.00045  3.95810E+17 0.00043  1.05426E+18 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.20355E+14 0.00538 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.05331E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.07499E+18 0.00057 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12456E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12456E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.70390E+00 0.00042 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.92251E-01 0.00030 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.69836E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25066E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95302E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97460E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.13081E+00 0.00049 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.12264E+00 0.00050 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46866E+00 1.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02672E+02 1.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.12250E+00 0.00051  1.11491E+00 0.00050  7.73216E-03 0.00771 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.12228E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.12241E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.12228E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.13045E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74867E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74884E+01 8.0E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.10259E-07 0.00325 ];
IMP_EALF                  (idx, [1:   2]) = [  5.08313E-07 0.00140 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.09090E-01 0.00289 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.08262E-01 0.00112 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.30109E-03 0.00553  1.95202E-04 0.03121  1.00857E-03 0.01382  9.77632E-04 0.01455  2.87289E-03 0.00767  9.31775E-04 0.01409  3.15014E-04 0.02425 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.12552E-01 0.01293  1.10667E-02 0.01606  3.16283E-02 0.00025  1.10155E-01 0.00029  3.20615E-01 0.00022  1.34554E+00 0.00016  8.60746E+00 0.00831 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.01269E-03 0.00816  2.24724E-04 0.04790  1.12681E-03 0.01968  1.09558E-03 0.02111  3.17889E-03 0.01087  1.03196E-03 0.02098  3.54734E-04 0.03604 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.13829E-01 0.01853  1.24906E-02 4.7E-06  3.16313E-02 0.00033  1.10138E-01 0.00040  3.20639E-01 0.00031  1.34558E+00 0.00023  8.89295E+00 0.00199 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.40990E-05 0.00112  2.40901E-05 0.00113  2.53760E-05 0.01140 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.70471E-05 0.00098  2.70371E-05 0.00098  2.84803E-05 0.01139 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.87367E-03 0.00782  2.04321E-04 0.04797  1.12552E-03 0.02023  1.06199E-03 0.02119  3.14214E-03 0.01167  9.97941E-04 0.02149  3.41765E-04 0.03665 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.07374E-01 0.01915  1.24908E-02 5.7E-06  3.16337E-02 0.00038  1.10153E-01 0.00046  3.20716E-01 0.00034  1.34513E+00 0.00028  8.86649E+00 0.00227 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.39245E-05 0.00246  2.39180E-05 0.00248  2.44586E-05 0.02661 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.68504E-05 0.00237  2.68431E-05 0.00239  2.74447E-05 0.02658 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.66183E-03 0.02506  1.89667E-04 0.15119  1.06979E-03 0.06281  1.04068E-03 0.06102  3.00833E-03 0.03889  9.89176E-04 0.06434  3.64183E-04 0.11900 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.19874E-01 0.05816  1.24908E-02 8.6E-06  3.16113E-02 0.00098  1.10038E-01 0.00099  3.20861E-01 0.00098  1.34333E+00 0.00066  8.88722E+00 0.00517 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.67051E-03 0.02455  1.88018E-04 0.14425  1.05591E-03 0.06079  1.05434E-03 0.05944  3.00981E-03 0.03788  1.00144E-03 0.06347  3.60997E-04 0.11279 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.30441E-01 0.05808  1.24908E-02 8.6E-06  3.16087E-02 0.00098  1.10055E-01 0.00099  3.20926E-01 0.00098  1.34341E+00 0.00066  8.88450E+00 0.00512 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.79811E+02 0.02546 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.40197E-05 0.00071 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.69585E-05 0.00049 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.78058E-03 0.00466 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.82416E+02 0.00479 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.84589E-07 0.00064 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.88257E-06 0.00041  2.88247E-06 0.00041  2.89550E-06 0.00512 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.83609E-05 0.00081  3.83802E-05 0.00081  3.57095E-05 0.00902 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.67966E-01 0.00031  6.67337E-01 0.00032  7.79646E-01 0.00874 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05028E+01 0.01314 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.52124E+01 0.00044  3.44682E+01 0.00043 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.18818E+04 0.00364  2.91311E+05 0.00135  6.04628E+05 0.00088  6.50922E+05 0.00063  5.99632E+05 0.00067  6.45091E+05 0.00067  4.37990E+05 0.00073  3.88172E+05 0.00079  2.96647E+05 0.00074  2.42295E+05 0.00072  2.08878E+05 0.00088  1.88588E+05 0.00070  1.73990E+05 0.00092  1.65190E+05 0.00081  1.61082E+05 0.00061  1.39112E+05 0.00082  1.37128E+05 0.00099  1.36146E+05 0.00116  1.33668E+05 0.00079  2.60632E+05 0.00059  2.51210E+05 0.00077  1.81382E+05 0.00109  1.17113E+05 0.00090  1.35172E+05 0.00101  1.27690E+05 0.00079  1.15834E+05 0.00111  1.90152E+05 0.00072  4.34935E+04 0.00186  5.46217E+04 0.00153  4.96150E+04 0.00128  2.87665E+04 0.00250  4.99557E+04 0.00113  3.39005E+04 0.00238  2.89621E+04 0.00177  5.53083E+03 0.00332  5.48972E+03 0.00410  5.66175E+03 0.00320  5.82918E+03 0.00370  5.75325E+03 0.00330  5.66525E+03 0.00371  5.83562E+03 0.00331  5.49965E+03 0.00376  1.03639E+04 0.00289  1.65416E+04 0.00177  2.10725E+04 0.00223  5.56706E+04 0.00134  5.86757E+04 0.00143  6.38415E+04 0.00150  4.33641E+04 0.00182  3.22196E+04 0.00169  2.47555E+04 0.00169  2.91727E+04 0.00140  5.55605E+04 0.00104  7.50223E+04 0.00094  1.43460E+05 0.00128  2.19129E+05 0.00103  3.23985E+05 0.00118  2.05526E+05 0.00136  1.46517E+05 0.00119  1.05021E+05 0.00146  9.42579E+04 0.00148  9.25878E+04 0.00159  7.71189E+04 0.00145  5.21045E+04 0.00159  4.80450E+04 0.00153  4.26046E+04 0.00121  3.60399E+04 0.00184  2.82862E+04 0.00191  1.89228E+04 0.00212  6.68744E+03 0.00212 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.13058E+00 0.00051 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.14173E+18 0.00054  3.08369E+17 0.00105 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37120E-01 7.3E-05  1.49746E+00 0.00039 ];
INF_CAPT                  (idx, [1:   4]) = [  6.44835E-03 0.00051  2.94819E-02 0.00036 ];
INF_ABS                   (idx, [1:   4]) = [  8.79270E-03 0.00041  6.57542E-02 0.00074 ];
INF_FISS                  (idx, [1:   4]) = [  2.34435E-03 0.00038  3.62722E-02 0.00105 ];
INF_NSF                   (idx, [1:   4]) = [  6.00302E-03 0.00038  8.87456E-02 0.00105 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.56063E+00 5.1E-05  2.44665E+00 4.1E-06 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03811E+02 5.3E-06  2.02400E+02 6.5E-07 ];
INF_INVV                  (idx, [1:   4]) = [  5.86808E-08 0.00037  2.53186E-06 0.00020 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28329E-01 7.5E-05  1.43171E+00 0.00044 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43092E-01 0.00016  3.79832E-01 0.00046 ];
INF_SCATT2                (idx, [1:   4]) = [  9.58962E-02 0.00028  9.07694E-02 0.00089 ];
INF_SCATT3                (idx, [1:   4]) = [  7.38666E-03 0.00262  2.73208E-02 0.00155 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02387E-02 0.00161 -8.24551E-03 0.00699 ];
INF_SCATT5                (idx, [1:   4]) = [  1.56942E-04 0.08129  6.17776E-03 0.01158 ];
INF_SCATT6                (idx, [1:   4]) = [  5.05101E-03 0.00272 -1.60285E-02 0.00255 ];
INF_SCATT7                (idx, [1:   4]) = [  7.38335E-04 0.02014  2.73446E-04 0.14110 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28368E-01 7.5E-05  1.43171E+00 0.00044 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43093E-01 0.00016  3.79832E-01 0.00046 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.58964E-02 0.00028  9.07694E-02 0.00089 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.38668E-03 0.00263  2.73208E-02 0.00155 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02386E-02 0.00160 -8.24551E-03 0.00699 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.57077E-04 0.08128  6.17776E-03 0.01158 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.05116E-03 0.00271 -1.60285E-02 0.00255 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.38524E-04 0.02011  2.73446E-04 0.14110 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13928E-01 0.00027  9.64353E-01 0.00039 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55816E+00 0.00027  3.45656E-01 0.00039 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.75364E-03 0.00042  6.57542E-02 0.00074 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69331E-02 0.00023  6.69194E-02 0.00090 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10187E-01 7.0E-05  1.81414E-02 0.00036  1.17147E-03 0.00379  1.43054E+00 0.00044 ];
INF_S1                    (idx, [1:   8]) = [  2.37795E-01 0.00016  5.29700E-03 0.00092  4.96278E-04 0.00592  3.79335E-01 0.00046 ];
INF_S2                    (idx, [1:   8]) = [  9.74619E-02 0.00027 -1.56562E-03 0.00233  2.73333E-04 0.00848  9.04960E-02 0.00090 ];
INF_S3                    (idx, [1:   8]) = [  9.25290E-03 0.00206 -1.86624E-03 0.00142  9.88011E-05 0.01551  2.72220E-02 0.00156 ];
INF_S4                    (idx, [1:   8]) = [ -9.61780E-03 0.00178 -6.20877E-04 0.00448  2.75993E-06 0.61320 -8.24827E-03 0.00698 ];
INF_S5                    (idx, [1:   8]) = [  1.44215E-04 0.09443  1.27272E-05 0.19364 -3.81589E-05 0.02958  6.21592E-03 0.01158 ];
INF_S6                    (idx, [1:   8]) = [  5.19783E-03 0.00255 -1.46819E-04 0.01469 -4.96676E-05 0.02792 -1.59788E-02 0.00259 ];
INF_S7                    (idx, [1:   8]) = [  9.08174E-04 0.01685 -1.69839E-04 0.01443 -4.45985E-05 0.02782  3.18045E-04 0.12226 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10226E-01 7.0E-05  1.81414E-02 0.00036  1.17147E-03 0.00379  1.43054E+00 0.00044 ];
INF_SP1                   (idx, [1:   8]) = [  2.37796E-01 0.00016  5.29700E-03 0.00092  4.96278E-04 0.00592  3.79335E-01 0.00046 ];
INF_SP2                   (idx, [1:   8]) = [  9.74620E-02 0.00027 -1.56562E-03 0.00233  2.73333E-04 0.00848  9.04960E-02 0.00090 ];
INF_SP3                   (idx, [1:   8]) = [  9.25292E-03 0.00207 -1.86624E-03 0.00142  9.88011E-05 0.01551  2.72220E-02 0.00156 ];
INF_SP4                   (idx, [1:   8]) = [ -9.61772E-03 0.00177 -6.20877E-04 0.00448  2.75993E-06 0.61320 -8.24827E-03 0.00698 ];
INF_SP5                   (idx, [1:   8]) = [  1.44350E-04 0.09438  1.27272E-05 0.19364 -3.81589E-05 0.02958  6.21592E-03 0.01158 ];
INF_SP6                   (idx, [1:   8]) = [  5.19798E-03 0.00254 -1.46819E-04 0.01469 -4.96676E-05 0.02792 -1.59788E-02 0.00259 ];
INF_SP7                   (idx, [1:   8]) = [  9.08363E-04 0.01682 -1.69839E-04 0.01443 -4.45985E-05 0.02782  3.18045E-04 0.12226 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32070E-01 0.00063  1.06422E+00 0.00753 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33792E-01 0.00076  1.13609E+00 0.00804 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33561E-01 0.00111  1.12546E+00 0.00977 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28933E-01 0.00102  9.53004E-01 0.00729 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43636E+00 0.00063  3.13644E-01 0.00749 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42579E+00 0.00076  2.93854E-01 0.00797 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42722E+00 0.00111  2.96867E-01 0.00996 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45607E+00 0.00102  3.50212E-01 0.00720 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.01269E-03 0.00816  2.24724E-04 0.04790  1.12681E-03 0.01968  1.09558E-03 0.02111  3.17889E-03 0.01087  1.03196E-03 0.02098  3.54734E-04 0.03604 ];
LAMBDA                    (idx, [1:  14]) = [  8.13829E-01 0.01853  1.24906E-02 4.7E-06  3.16313E-02 0.00033  1.10138E-01 0.00040  3.20639E-01 0.00031  1.34558E+00 0.00023  8.89295E+00 0.00199 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0008' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:50:49 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:11:00 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299849 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99482E-01  9.92355E-01  1.00630E+00  1.00378E+00  9.98080E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14770E-02 0.00107  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88523E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.96895E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.97332E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.70519E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.51055E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.50984E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.92092E+00 0.00036  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.22389E-01 0.00109  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000552 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00011E+04 0.00075 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00011E+04 0.00075 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  9.89824E+01 ;
RUNNING_TIME              (idx, 1)        =  2.01829E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.62450E-01  4.62450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  4.56167E-02  7.90000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.95180E+01  3.05862E+00  2.37418E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.55467E-01  2.57500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  9.35001E-03  7.50001E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  2.01822E+01  1.13978E+02 ];
CPU_USAGE                 (idx, 1)        = 4.90427 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99900E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.62482E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.88219E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.84758E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.47096E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.56414E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.09217E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.31803E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.73834E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  3.50630E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.46131E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  9.77421E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.46771E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  2.52888E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  2.81454E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.26629E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.03992E+14 ;
I131_ACTIVITY             (idx, 1)        =  3.59732E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.09052E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.69791E+10 ;
CS137_ACTIVITY            (idx, 1)        =  1.34866E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.79156E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.61960E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.44614E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.14183E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.07411E+12 0.00046  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 3 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E+00  1.00008E+00 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.91368E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  1.23743E+16 0.00059  8.93397E-01 0.00024 ];
U238_FISS                 (idx, [1:   4]) = [  8.54259E+14 0.00282  6.16695E-02 0.00270 ];
PU239_FISS                (idx, [1:   4]) = [  6.18717E+14 0.00314  4.46702E-02 0.00310 ];
PU240_FISS                (idx, [1:   4]) = [  9.24079E+10 0.27215  6.66439E-06 0.27199 ];
PU241_FISS                (idx, [1:   4]) = [  1.18635E+12 0.07063  8.57209E-05 0.07061 ];
U235_CAPT                 (idx, [1:   4]) = [  2.69377E+15 0.00148  1.61160E-01 0.00141 ];
U238_CAPT                 (idx, [1:   4]) = [  7.79286E+15 0.00101  4.66180E-01 0.00066 ];
PU239_CAPT                (idx, [1:   4]) = [  3.47845E+14 0.00424  2.08067E-02 0.00412 ];
PU240_CAPT                (idx, [1:   4]) = [  3.23339E+13 0.01341  1.93481E-03 0.01343 ];
PU241_CAPT                (idx, [1:   4]) = [  4.42730E+11 0.11912  2.64771E-05 0.11905 ];
XE135_CAPT                (idx, [1:   4]) = [  7.14232E+14 0.00285  4.27322E-02 0.00285 ];
SM149_CAPT                (idx, [1:   4]) = [  1.48777E+14 0.00608  8.90105E-03 0.00607 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000552 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.32067E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000552 5.00732E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2715080 2.71878E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2249840 2.25291E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 35632 3.56355E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000552 5.00732E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.72529E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 6.3E-09  4.50056E+05 6.3E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.43428E+16 1.1E-05  3.43428E+16 1.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38503E+16 1.4E-06  1.38503E+16 1.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.67307E+16 0.00044  1.20080E+16 0.00045  4.72272E+15 0.00102 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.05810E+16 0.00024  2.58583E+16 0.00021  4.72272E+15 0.00102 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.07411E+16 0.00046  3.07411E+16 0.00046  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.45942E+18 0.00043  3.98234E+17 0.00041  1.06119E+18 0.00049 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.19112E+14 0.00540 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.08001E+16 0.00025 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.08044E+18 0.00055 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12397E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12397E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.69493E+00 0.00042 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.93891E-01 0.00029 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.69090E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24997E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95382E-01 3.0E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97479E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.12531E+00 0.00050 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.11729E+00 0.00050 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.47958E+00 1.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02814E+02 1.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.11747E+00 0.00051  1.10974E+00 0.00051  7.54380E-03 0.00828 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.11669E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.11728E+00 0.00046 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.11669E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.12470E+00 0.00024 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74697E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74667E+01 7.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.19045E-07 0.00326 ];
IMP_EALF                  (idx, [1:   2]) = [  5.19422E-07 0.00135 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.10387E-01 0.00285 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.10455E-01 0.00111 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.15680E-03 0.00552  1.69391E-04 0.03400  9.89455E-04 0.01379  9.75460E-04 0.01313  2.80483E-03 0.00846  9.06423E-04 0.01379  3.11240E-04 0.02410 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.19130E-01 0.01312  1.04920E-02 0.01954  3.16006E-02 0.00026  1.10226E-01 0.00029  3.20774E-01 0.00022  1.34499E+00 0.00020  8.65763E+00 0.00780 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.79950E-03 0.00801  1.81777E-04 0.04934  1.10661E-03 0.01933  1.07298E-03 0.01881  3.08357E-03 0.01188  1.00292E-03 0.02167  3.51651E-04 0.03621 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.21342E-01 0.01911  1.24905E-02 5.7E-06  3.16065E-02 0.00034  1.10180E-01 0.00042  3.20688E-01 0.00034  1.34470E+00 0.00032  8.90570E+00 0.00218 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.39145E-05 0.00114  2.39034E-05 0.00114  2.56505E-05 0.01150 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.67201E-05 0.00100  2.67077E-05 0.00101  2.86597E-05 0.01147 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.74869E-03 0.00844  1.88297E-04 0.04711  1.08728E-03 0.02019  1.04460E-03 0.02048  3.09677E-03 0.01248  9.88581E-04 0.02214  3.43168E-04 0.03583 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.14285E-01 0.01907  1.24904E-02 6.8E-06  3.15952E-02 0.00043  1.10187E-01 0.00048  3.20630E-01 0.00034  1.34527E+00 0.00027  8.90482E+00 0.00255 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.36178E-05 0.00249  2.36049E-05 0.00250  2.57944E-05 0.02890 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.63896E-05 0.00246  2.63753E-05 0.00248  2.88173E-05 0.02882 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.83545E-03 0.02621  2.23466E-04 0.15644  1.03032E-03 0.06369  1.12055E-03 0.06336  3.21032E-03 0.03885  8.65452E-04 0.07027  3.85350E-04 0.10719 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.42876E-01 0.05870  1.24903E-02 2.2E-05  3.15948E-02 0.00096  1.10476E-01 0.00125  3.21345E-01 0.00107  1.34588E+00 0.00064  8.96856E+00 0.00606 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.79616E-03 0.02546  2.16425E-04 0.15434  1.03996E-03 0.06197  1.10415E-03 0.06353  3.20705E-03 0.03826  8.47983E-04 0.06759  3.80582E-04 0.10648 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.41940E-01 0.05924  1.24903E-02 2.1E-05  3.15928E-02 0.00096  1.10459E-01 0.00123  3.21321E-01 0.00105  1.34586E+00 0.00064  8.96789E+00 0.00608 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.89783E+02 0.02622 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.37728E-05 0.00072 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.65618E-05 0.00049 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.78519E-03 0.00498 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.85529E+02 0.00509 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.80539E-07 0.00065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.88058E-06 0.00042  2.88039E-06 0.00042  2.90719E-06 0.00515 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.80382E-05 0.00082  3.80606E-05 0.00082  3.49451E-05 0.00855 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.67260E-01 0.00032  6.66660E-01 0.00032  7.74362E-01 0.00807 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03062E+01 0.01332 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.50984E+01 0.00044  3.43344E+01 0.00044 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.21778E+04 0.00327  2.93357E+05 0.00193  6.03875E+05 0.00100  6.50828E+05 0.00084  6.00294E+05 0.00085  6.45948E+05 0.00063  4.37568E+05 0.00078  3.88057E+05 0.00077  2.96694E+05 0.00080  2.42536E+05 0.00084  2.09301E+05 0.00070  1.88090E+05 0.00092  1.73722E+05 0.00081  1.65412E+05 0.00091  1.61051E+05 0.00095  1.38886E+05 0.00124  1.37116E+05 0.00084  1.36255E+05 0.00079  1.33495E+05 0.00100  2.60968E+05 0.00062  2.51677E+05 0.00072  1.81183E+05 0.00072  1.17149E+05 0.00107  1.35185E+05 0.00078  1.27736E+05 0.00083  1.15882E+05 0.00084  1.90151E+05 0.00089  4.34465E+04 0.00161  5.45937E+04 0.00154  4.95140E+04 0.00133  2.86826E+04 0.00171  5.01404E+04 0.00133  3.41668E+04 0.00164  2.89772E+04 0.00180  5.51236E+03 0.00390  5.46736E+03 0.00387  5.59376E+03 0.00315  5.73842E+03 0.00293  5.68412E+03 0.00455  5.59538E+03 0.00399  5.84245E+03 0.00362  5.50011E+03 0.00355  1.03751E+04 0.00304  1.65459E+04 0.00250  2.11040E+04 0.00193  5.55851E+04 0.00137  5.85918E+04 0.00124  6.35631E+04 0.00133  4.29752E+04 0.00138  3.17855E+04 0.00145  2.42897E+04 0.00161  2.85886E+04 0.00139  5.44917E+04 0.00138  7.38158E+04 0.00145  1.41502E+05 0.00115  2.16661E+05 0.00105  3.20584E+05 0.00126  2.03666E+05 0.00133  1.45335E+05 0.00155  1.04100E+05 0.00138  9.33240E+04 0.00121  9.17917E+04 0.00163  7.65116E+04 0.00151  5.16137E+04 0.00173  4.77024E+04 0.00177  4.23399E+04 0.00176  3.57236E+04 0.00150  2.81224E+04 0.00155  1.87596E+04 0.00188  6.63354E+03 0.00208 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.12530E+00 0.00049 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.15166E+18 0.00046  3.07792E+17 0.00124 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37101E-01 0.00013  1.50069E+00 0.00039 ];
INF_CAPT                  (idx, [1:   4]) = [  6.48584E-03 0.00061  3.00920E-02 0.00048 ];
INF_ABS                   (idx, [1:   4]) = [  8.81155E-03 0.00053  6.63947E-02 0.00092 ];
INF_FISS                  (idx, [1:   4]) = [  2.32572E-03 0.00053  3.63026E-02 0.00128 ];
INF_NSF                   (idx, [1:   4]) = [  5.96612E-03 0.00052  8.92693E-02 0.00128 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.56528E+00 4.5E-05  2.45903E+00 9.1E-06 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03868E+02 4.8E-06  2.02561E+02 1.4E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.86475E-08 0.00045  2.53342E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28292E-01 0.00014  1.43434E+00 0.00045 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43060E-01 0.00021  3.80645E-01 0.00052 ];
INF_SCATT2                (idx, [1:   4]) = [  9.58759E-02 0.00026  9.08756E-02 0.00092 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35176E-03 0.00222  2.72516E-02 0.00257 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02444E-02 0.00178 -8.20738E-03 0.00681 ];
INF_SCATT5                (idx, [1:   4]) = [  1.48241E-04 0.13380  6.27470E-03 0.00705 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07397E-03 0.00358 -1.60375E-02 0.00344 ];
INF_SCATT7                (idx, [1:   4]) = [  7.41588E-04 0.02191  3.48952E-04 0.12943 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28331E-01 0.00014  1.43434E+00 0.00045 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43060E-01 0.00021  3.80645E-01 0.00052 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.58761E-02 0.00026  9.08756E-02 0.00092 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35138E-03 0.00223  2.72516E-02 0.00257 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02446E-02 0.00178 -8.20738E-03 0.00681 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.48312E-04 0.13388  6.27470E-03 0.00705 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07404E-03 0.00358 -1.60375E-02 0.00344 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.41439E-04 0.02190  3.48952E-04 0.12943 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13850E-01 0.00036  9.67167E-01 0.00035 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55873E+00 0.00036  3.44650E-01 0.00035 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.77247E-03 0.00051  6.63947E-02 0.00092 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69272E-02 0.00019  6.75435E-02 0.00095 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10174E-01 0.00013  1.81181E-02 0.00040  1.19185E-03 0.00477  1.43315E+00 0.00045 ];
INF_S1                    (idx, [1:   8]) = [  2.37763E-01 0.00021  5.29648E-03 0.00071  5.01496E-04 0.00781  3.80144E-01 0.00052 ];
INF_S2                    (idx, [1:   8]) = [  9.74379E-02 0.00026 -1.56201E-03 0.00239  2.73942E-04 0.01390  9.06017E-02 0.00091 ];
INF_S3                    (idx, [1:   8]) = [  9.21163E-03 0.00180 -1.85987E-03 0.00137  9.83035E-05 0.02302  2.71533E-02 0.00256 ];
INF_S4                    (idx, [1:   8]) = [ -9.62214E-03 0.00186 -6.22271E-04 0.00433 -3.92187E-06 0.40480 -8.20346E-03 0.00684 ];
INF_S5                    (idx, [1:   8]) = [  1.33707E-04 0.14186  1.45345E-05 0.15344 -4.36878E-05 0.04158  6.31839E-03 0.00700 ];
INF_S6                    (idx, [1:   8]) = [  5.21811E-03 0.00340 -1.44139E-04 0.01646 -5.11127E-05 0.02326 -1.59864E-02 0.00345 ];
INF_S7                    (idx, [1:   8]) = [  9.12622E-04 0.01780 -1.71034E-04 0.01069 -4.75448E-05 0.02455  3.96497E-04 0.11447 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10213E-01 0.00013  1.81181E-02 0.00040  1.19185E-03 0.00477  1.43315E+00 0.00045 ];
INF_SP1                   (idx, [1:   8]) = [  2.37764E-01 0.00021  5.29648E-03 0.00071  5.01496E-04 0.00781  3.80144E-01 0.00052 ];
INF_SP2                   (idx, [1:   8]) = [  9.74381E-02 0.00026 -1.56201E-03 0.00239  2.73942E-04 0.01390  9.06017E-02 0.00091 ];
INF_SP3                   (idx, [1:   8]) = [  9.21125E-03 0.00180 -1.85987E-03 0.00137  9.83035E-05 0.02302  2.71533E-02 0.00256 ];
INF_SP4                   (idx, [1:   8]) = [ -9.62238E-03 0.00186 -6.22271E-04 0.00433 -3.92187E-06 0.40480 -8.20346E-03 0.00684 ];
INF_SP5                   (idx, [1:   8]) = [  1.33777E-04 0.14196  1.45345E-05 0.15344 -4.36878E-05 0.04158  6.31839E-03 0.00700 ];
INF_SP6                   (idx, [1:   8]) = [  5.21818E-03 0.00339 -1.44139E-04 0.01646 -5.11127E-05 0.02326 -1.59864E-02 0.00345 ];
INF_SP7                   (idx, [1:   8]) = [  9.12473E-04 0.01780 -1.71034E-04 0.01069 -4.75448E-05 0.02455  3.96497E-04 0.11447 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31934E-01 0.00047  1.06092E+00 0.00650 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33364E-01 0.00080  1.13019E+00 0.00909 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33359E-01 0.00065  1.12021E+00 0.00824 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29138E-01 0.00091  9.53227E-01 0.00584 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43720E+00 0.00047  3.14507E-01 0.00642 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42841E+00 0.00080  2.95506E-01 0.00887 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42843E+00 0.00065  2.98043E-01 0.00816 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45475E+00 0.00092  3.49972E-01 0.00577 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.79950E-03 0.00801  1.81777E-04 0.04934  1.10661E-03 0.01933  1.07298E-03 0.01881  3.08357E-03 0.01188  1.00292E-03 0.02167  3.51651E-04 0.03621 ];
LAMBDA                    (idx, [1:  14]) = [  8.21342E-01 0.01911  1.24905E-02 5.7E-06  3.16065E-02 0.00034  1.10180E-01 0.00042  3.20688E-01 0.00034  1.34470E+00 0.00032  8.90570E+00 0.00218 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0008' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:50:49 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:16:31 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299849 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.97707E-01  9.99505E-01  1.00498E+00  1.00188E+00  9.95928E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 6.6E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14900E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88510E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.98247E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.98685E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69777E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.49057E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.48985E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.79601E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.19337E-01 0.00116  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000610 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00012E+04 0.00076 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00012E+04 0.00076 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.26540E+02 ;
RUNNING_TIME              (idx, 1)        =  2.56996E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.62450E-01  4.62450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  6.23500E-02  8.03334E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  2.49690E+01  3.05530E+00  2.39565E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  2.04167E-01  2.48667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.24667E-02  7.50001E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  2.56995E+01  1.14021E+02 ];
CPU_USAGE                 (idx, 1)        = 4.92379 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99991E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.67286E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.04568E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.84889E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.68314E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.57626E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.10043E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.46940E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.73883E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  4.69443E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.82230E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.23834E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.53310E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  3.45609E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.16899E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.49026E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.12262E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.07346E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.18943E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.58779E+11 ;
CS137_ACTIVITY            (idx, 1)        =  2.69912E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.91231E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.58462E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.16010E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.16874E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.12632E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 4 ;
BURNUP                     (idx, [1:  2])  = [  2.00000E+00  2.00016E+00 ];
BURN_DAYS                 (idx, 1)        =  5.00000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.96018E-01 0.00105 ];
U235_FISS                 (idx, [1:   4]) = [  1.17404E+16 0.00060  8.48949E-01 0.00028 ];
U238_FISS                 (idx, [1:   4]) = [  8.72452E+14 0.00273  6.30810E-02 0.00260 ];
PU239_FISS                (idx, [1:   4]) = [  1.20403E+15 0.00229  8.70607E-02 0.00220 ];
PU240_FISS                (idx, [1:   4]) = [  3.26394E+11 0.13142  2.36122E-05 0.13142 ];
PU241_FISS                (idx, [1:   4]) = [  9.00062E+12 0.02619  6.50496E-04 0.02616 ];
U235_CAPT                 (idx, [1:   4]) = [  2.57511E+15 0.00156  1.49267E-01 0.00146 ];
U238_CAPT                 (idx, [1:   4]) = [  7.87020E+15 0.00100  4.56171E-01 0.00068 ];
PU239_CAPT                (idx, [1:   4]) = [  6.77600E+14 0.00308  3.92780E-02 0.00303 ];
PU240_CAPT                (idx, [1:   4]) = [  1.17787E+14 0.00719  6.82615E-03 0.00712 ];
PU241_CAPT                (idx, [1:   4]) = [  3.31506E+12 0.04414  1.92147E-04 0.04413 ];
XE135_CAPT                (idx, [1:   4]) = [  7.15529E+14 0.00297  4.14776E-02 0.00294 ];
SM149_CAPT                (idx, [1:   4]) = [  1.56225E+14 0.00621  9.05560E-03 0.00620 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000610 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.32019E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000610 5.00732E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2755450 2.75918E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2208916 2.21188E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 36244 3.62633E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000610 5.00732E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -1.39698E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 6.3E-09  4.50056E+05 6.3E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.45648E+16 1.4E-05  3.45648E+16 1.4E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38333E+16 2.0E-06  1.38333E+16 2.0E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.72609E+16 0.00045  1.25093E+16 0.00046  4.75160E+15 0.00106 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.10942E+16 0.00025  2.63426E+16 0.00022  4.75160E+15 0.00106 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.12632E+16 0.00049  3.12632E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.47805E+18 0.00046  4.02506E+17 0.00046  1.07554E+18 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.26762E+14 0.00544 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.13209E+16 0.00025 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.09248E+18 0.00059 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12281E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12281E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.68117E+00 0.00042 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.95412E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.66313E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24970E+00 0.00033 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95298E-01 3.1E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97437E-01 2.2E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.11343E+00 0.00050 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.10535E+00 0.00051 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.49867E+00 1.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03063E+02 2.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.10527E+00 0.00052  1.09794E+00 0.00051  7.41441E-03 0.00762 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.10524E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.10574E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.10524E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.11331E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74250E+01 0.00018 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74261E+01 8.2E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.42731E-07 0.00324 ];
IMP_EALF                  (idx, [1:   2]) = [  5.40998E-07 0.00143 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.14834E-01 0.00279 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.14455E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.14738E-03 0.00551  1.82646E-04 0.03108  9.91016E-04 0.01372  9.50293E-04 0.01402  2.82131E-03 0.00763  8.98615E-04 0.01396  3.03498E-04 0.02434 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.03623E-01 0.01254  1.08917E-02 0.01715  3.15325E-02 0.00030  1.10127E-01 0.00029  3.20857E-01 0.00023  1.34463E+00 0.00022  8.63318E+00 0.00834 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.72471E-03 0.00757  2.06533E-04 0.04831  1.05503E-03 0.02033  1.02630E-03 0.02017  3.10725E-03 0.01142  1.00313E-03 0.02012  3.26459E-04 0.03578 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.99532E-01 0.01808  1.24905E-02 5.9E-06  3.15496E-02 0.00042  1.10138E-01 0.00040  3.20798E-01 0.00032  1.34441E+00 0.00026  8.94557E+00 0.00226 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.36609E-05 0.00117  2.36503E-05 0.00118  2.52963E-05 0.01204 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.61484E-05 0.00106  2.61365E-05 0.00106  2.79603E-05 0.01207 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.71325E-03 0.00776  2.08440E-04 0.04633  1.07619E-03 0.02019  1.00082E-03 0.02171  3.10169E-03 0.01228  9.99049E-04 0.02098  3.27068E-04 0.03645 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.04145E-01 0.01918  1.24903E-02 9.2E-06  3.15338E-02 0.00048  1.10136E-01 0.00047  3.20902E-01 0.00035  1.34493E+00 0.00032  8.95737E+00 0.00282 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.33101E-05 0.00260  2.33073E-05 0.00261  2.38157E-05 0.02629 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.57602E-05 0.00254  2.57571E-05 0.00255  2.63070E-05 0.02621 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.61968E-03 0.02657  1.73789E-04 0.15072  1.04401E-03 0.06581  8.76600E-04 0.07094  3.18401E-03 0.04022  1.01583E-03 0.06853  3.25429E-04 0.11889 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.99022E-01 0.06079  1.24903E-02 1.6E-05  3.14748E-02 0.00124  1.10226E-01 0.00126  3.20190E-01 0.00091  1.34259E+00 0.00150  8.89770E+00 0.00581 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.69153E-03 0.02632  1.78067E-04 0.15075  1.03512E-03 0.06368  8.94085E-04 0.06899  3.24016E-03 0.04008  1.02524E-03 0.06647  3.18864E-04 0.11525 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.99766E-01 0.05796  1.24903E-02 1.6E-05  3.14815E-02 0.00122  1.10246E-01 0.00126  3.20276E-01 0.00091  1.34266E+00 0.00149  8.89879E+00 0.00581 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.85375E+02 0.02678 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.34810E-05 0.00068 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.59495E-05 0.00045 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.67567E-03 0.00486 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.84293E+02 0.00481 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.74068E-07 0.00067 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.86824E-06 0.00042  2.86795E-06 0.00042  2.90849E-06 0.00516 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.75641E-05 0.00083  3.75875E-05 0.00084  3.42345E-05 0.00870 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.64482E-01 0.00031  6.63909E-01 0.00031  7.68589E-01 0.00835 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05568E+01 0.01327 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.48985E+01 0.00044  3.40209E+01 0.00045 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.29965E+04 0.00363  2.93416E+05 0.00165  6.05203E+05 0.00116  6.51811E+05 0.00087  5.99269E+05 0.00067  6.44605E+05 0.00068  4.37661E+05 0.00075  3.87299E+05 0.00070  2.96141E+05 0.00071  2.42248E+05 0.00067  2.09159E+05 0.00082  1.88486E+05 0.00076  1.73717E+05 0.00081  1.64874E+05 0.00085  1.60943E+05 0.00081  1.39013E+05 0.00092  1.37376E+05 0.00074  1.35960E+05 0.00093  1.33621E+05 0.00127  2.60664E+05 0.00072  2.51390E+05 0.00076  1.81105E+05 0.00101  1.16994E+05 0.00123  1.35239E+05 0.00093  1.27795E+05 0.00074  1.15970E+05 0.00114  1.89842E+05 0.00078  4.34416E+04 0.00167  5.44333E+04 0.00103  4.95403E+04 0.00125  2.86426E+04 0.00207  4.99077E+04 0.00145  3.39580E+04 0.00198  2.89066E+04 0.00157  5.47899E+03 0.00386  5.41289E+03 0.00275  5.43603E+03 0.00374  5.48293E+03 0.00292  5.49024E+03 0.00305  5.48934E+03 0.00381  5.77915E+03 0.00361  5.44476E+03 0.00437  1.02717E+04 0.00305  1.64636E+04 0.00240  2.09414E+04 0.00211  5.54116E+04 0.00159  5.80833E+04 0.00139  6.28826E+04 0.00109  4.21882E+04 0.00121  3.09083E+04 0.00141  2.34268E+04 0.00158  2.75311E+04 0.00168  5.27754E+04 0.00130  7.18754E+04 0.00124  1.38491E+05 0.00150  2.12246E+05 0.00122  3.14986E+05 0.00124  2.00085E+05 0.00141  1.42960E+05 0.00124  1.02501E+05 0.00148  9.21092E+04 0.00168  9.04942E+04 0.00170  7.54350E+04 0.00162  5.09031E+04 0.00193  4.71005E+04 0.00175  4.17813E+04 0.00152  3.52944E+04 0.00184  2.78074E+04 0.00188  1.85755E+04 0.00208  6.55697E+03 0.00268 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.11382E+00 0.00061 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.17061E+18 0.00064  3.07472E+17 0.00115 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37172E-01 0.00015  1.50805E+00 0.00040 ];
INF_CAPT                  (idx, [1:   4]) = [  6.59578E-03 0.00060  3.10294E-02 0.00047 ];
INF_ABS                   (idx, [1:   4]) = [  8.88803E-03 0.00046  6.72988E-02 0.00083 ];
INF_FISS                  (idx, [1:   4]) = [  2.29225E-03 0.00047  3.62694E-02 0.00114 ];
INF_NSF                   (idx, [1:   4]) = [  5.89973E-03 0.00048  8.99698E-02 0.00116 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.57377E+00 7.3E-05  2.48060E+00 2.0E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03975E+02 6.8E-06  2.02844E+02 3.3E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.84690E-08 0.00050  2.53716E-06 0.00020 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28286E-01 0.00015  1.44079E+00 0.00046 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43027E-01 0.00024  3.82355E-01 0.00049 ];
INF_SCATT2                (idx, [1:   4]) = [  9.58426E-02 0.00035  9.11713E-02 0.00086 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35019E-03 0.00349  2.74723E-02 0.00330 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02466E-02 0.00192 -8.28023E-03 0.00730 ];
INF_SCATT5                (idx, [1:   4]) = [  1.52183E-04 0.12039  6.20933E-03 0.00796 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07833E-03 0.00326 -1.62275E-02 0.00300 ];
INF_SCATT7                (idx, [1:   4]) = [  7.33462E-04 0.02217  3.13710E-04 0.13512 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28325E-01 0.00015  1.44079E+00 0.00046 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43027E-01 0.00024  3.82355E-01 0.00049 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.58423E-02 0.00035  9.11713E-02 0.00086 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35022E-03 0.00349  2.74723E-02 0.00330 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02469E-02 0.00192 -8.28023E-03 0.00730 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.52315E-04 0.12013  6.20933E-03 0.00796 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07839E-03 0.00325 -1.62275E-02 0.00300 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.33174E-04 0.02219  3.13710E-04 0.13512 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13794E-01 0.00033  9.72954E-01 0.00037 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55914E+00 0.00033  3.42601E-01 0.00037 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.84894E-03 0.00048  6.72988E-02 0.00083 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69383E-02 0.00022  6.84704E-02 0.00106 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10234E-01 0.00015  1.80521E-02 0.00040  1.21067E-03 0.00314  1.43958E+00 0.00047 ];
INF_S1                    (idx, [1:   8]) = [  2.37754E-01 0.00024  5.27294E-03 0.00074  5.03446E-04 0.00591  3.81852E-01 0.00049 ];
INF_S2                    (idx, [1:   8]) = [  9.74046E-02 0.00033 -1.56208E-03 0.00266  2.77479E-04 0.00800  9.08938E-02 0.00085 ];
INF_S3                    (idx, [1:   8]) = [  9.20810E-03 0.00268 -1.85791E-03 0.00176  9.72762E-05 0.01979  2.73750E-02 0.00330 ];
INF_S4                    (idx, [1:   8]) = [ -9.62867E-03 0.00205 -6.17958E-04 0.00474 -5.90966E-07 1.00000 -8.27964E-03 0.00727 ];
INF_S5                    (idx, [1:   8]) = [  1.32864E-04 0.14143  1.93193E-05 0.12584 -4.07150E-05 0.03993  6.25005E-03 0.00790 ];
INF_S6                    (idx, [1:   8]) = [  5.21987E-03 0.00311 -1.41540E-04 0.01888 -5.10066E-05 0.02898 -1.61764E-02 0.00299 ];
INF_S7                    (idx, [1:   8]) = [  9.04756E-04 0.01784 -1.71295E-04 0.00850 -4.66981E-05 0.03089  3.60408E-04 0.11648 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10273E-01 0.00015  1.80521E-02 0.00040  1.21067E-03 0.00314  1.43958E+00 0.00047 ];
INF_SP1                   (idx, [1:   8]) = [  2.37754E-01 0.00024  5.27294E-03 0.00074  5.03446E-04 0.00591  3.81852E-01 0.00049 ];
INF_SP2                   (idx, [1:   8]) = [  9.74044E-02 0.00032 -1.56208E-03 0.00266  2.77479E-04 0.00800  9.08938E-02 0.00085 ];
INF_SP3                   (idx, [1:   8]) = [  9.20814E-03 0.00268 -1.85791E-03 0.00176  9.72762E-05 0.01979  2.73750E-02 0.00330 ];
INF_SP4                   (idx, [1:   8]) = [ -9.62899E-03 0.00205 -6.17958E-04 0.00474 -5.90966E-07 1.00000 -8.27964E-03 0.00727 ];
INF_SP5                   (idx, [1:   8]) = [  1.32995E-04 0.14116  1.93193E-05 0.12584 -4.07150E-05 0.03993  6.25005E-03 0.00790 ];
INF_SP6                   (idx, [1:   8]) = [  5.21993E-03 0.00311 -1.41540E-04 0.01888 -5.10066E-05 0.02898 -1.61764E-02 0.00299 ];
INF_SP7                   (idx, [1:   8]) = [  9.04468E-04 0.01785 -1.71295E-04 0.00850 -4.66981E-05 0.03089  3.60408E-04 0.11648 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32071E-01 0.00053  1.07548E+00 0.00723 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33456E-01 0.00086  1.14140E+00 0.00819 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33306E-01 0.00085  1.14997E+00 0.00892 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29504E-01 0.00101  9.58754E-01 0.00680 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43635E+00 0.00053  3.10334E-01 0.00731 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42784E+00 0.00086  2.92513E-01 0.00826 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42876E+00 0.00085  2.90423E-01 0.00904 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45245E+00 0.00101  3.48065E-01 0.00690 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.72471E-03 0.00757  2.06533E-04 0.04831  1.05503E-03 0.02033  1.02630E-03 0.02017  3.10725E-03 0.01142  1.00313E-03 0.02012  3.26459E-04 0.03578 ];
LAMBDA                    (idx, [1:  14]) = [  7.99532E-01 0.01808  1.24905E-02 5.9E-06  3.15496E-02 0.00042  1.10138E-01 0.00040  3.20798E-01 0.00032  1.34441E+00 0.00026  8.94557E+00 0.00226 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0008' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:50:49 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:22:00 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299849 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.98101E-01  1.00009E+00  1.00474E+00  1.00132E+00  9.95748E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.1E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14564E-02 0.00112  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88544E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.99857E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.00293E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69020E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.48155E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.48080E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.68552E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.15714E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000781 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00016E+04 0.00074 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00016E+04 0.00074 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.53860E+02 ;
RUNNING_TIME              (idx, 1)        =  3.11690E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.62450E-01  4.62450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  7.88833E-02  8.25000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.03711E+01  3.05158E+00  2.35053E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  2.54617E-01  2.49667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.55667E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.11689E+01  1.13253E+02 ];
CPU_USAGE                 (idx, 1)        = 4.93630 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00115E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.70351E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.11998E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.83611E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.11542E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.59176E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.11113E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.52820E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.72497E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  5.65797E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.98187E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.57691E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.61043E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.08106E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.32083E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.67649E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.15257E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.16725E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.23103E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.46046E+11 ;
CS137_ACTIVITY            (idx, 1)        =  4.04926E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.95980E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.55176E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.17592E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.18240E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.18021E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 5 ;
BURNUP                     (idx, [1:  2])  = [  3.00000E+00  3.00025E+00 ];
BURN_DAYS                 (idx, 1)        =  7.50000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.02479E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  1.11968E+16 0.00061  8.10569E-01 0.00034 ];
U238_FISS                 (idx, [1:   4]) = [  8.90961E+14 0.00287  6.44898E-02 0.00272 ];
PU239_FISS                (idx, [1:   4]) = [  1.69652E+15 0.00194  1.22808E-01 0.00180 ];
PU240_FISS                (idx, [1:   4]) = [  6.17598E+11 0.10136  4.46420E-05 0.10143 ];
PU241_FISS                (idx, [1:   4]) = [  2.48504E+13 0.01574  1.79949E-03 0.01576 ];
U235_CAPT                 (idx, [1:   4]) = [  2.46219E+15 0.00150  1.38353E-01 0.00138 ];
U238_CAPT                 (idx, [1:   4]) = [  7.92835E+15 0.00106  4.45467E-01 0.00068 ];
PU239_CAPT                (idx, [1:   4]) = [  9.54563E+14 0.00242  5.36447E-02 0.00245 ];
PU240_CAPT                (idx, [1:   4]) = [  2.32736E+14 0.00559  1.30758E-02 0.00550 ];
PU241_CAPT                (idx, [1:   4]) = [  8.85686E+12 0.02675  4.97578E-04 0.02676 ];
XE135_CAPT                (idx, [1:   4]) = [  7.18282E+14 0.00302  4.03635E-02 0.00300 ];
SM149_CAPT                (idx, [1:   4]) = [  1.63240E+14 0.00617  9.17167E-03 0.00611 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000781 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.54351E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000781 5.00754E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2794164 2.79804E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2169053 2.17194E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 37564 3.75633E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000781 5.00754E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.49480E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 6.3E-09  4.50056E+05 6.3E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.47548E+16 1.4E-05  3.47548E+16 1.4E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38186E+16 2.1E-06  1.38186E+16 2.1E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.77954E+16 0.00046  1.29814E+16 0.00048  4.81402E+15 0.00113 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.16140E+16 0.00026  2.68000E+16 0.00023  4.81402E+15 0.00113 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.18021E+16 0.00050  3.18021E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.49966E+18 0.00048  4.07238E+17 0.00046  1.09242E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.38943E+14 0.00542 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.18530E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.10850E+18 0.00061 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12164E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12164E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.66856E+00 0.00040 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.95375E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.63896E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24953E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95130E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97344E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.10088E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.09261E+00 0.00052 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.51507E+00 1.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03278E+02 2.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.09237E+00 0.00054  1.08555E+00 0.00053  7.05669E-03 0.00809 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.09280E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.09298E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.09280E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.10107E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73868E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73906E+01 7.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.63979E-07 0.00337 ];
IMP_EALF                  (idx, [1:   2]) = [  5.60514E-07 0.00137 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.19773E-01 0.00303 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.18763E-01 0.00111 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.95997E-03 0.00582  1.62127E-04 0.03261  9.41391E-04 0.01460  9.68432E-04 0.01391  2.71491E-03 0.00865  8.78999E-04 0.01440  2.94115E-04 0.02479 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.07748E-01 0.01315  1.04418E-02 0.01983  3.14974E-02 0.00032  1.10142E-01 0.00031  3.21043E-01 0.00023  1.34431E+00 0.00028  8.59213E+00 0.00889 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.42677E-03 0.00816  1.77547E-04 0.05072  1.00823E-03 0.02206  1.02917E-03 0.02008  2.94080E-03 0.01292  9.57419E-04 0.02137  3.13605E-04 0.03626 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.06614E-01 0.01891  1.24902E-02 8.8E-06  3.15018E-02 0.00044  1.10138E-01 0.00045  3.20905E-01 0.00033  1.34469E+00 0.00039  8.91879E+00 0.00265 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.35898E-05 0.00116  2.35804E-05 0.00116  2.49757E-05 0.01229 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.57654E-05 0.00104  2.57551E-05 0.00104  2.72753E-05 0.01225 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.46194E-03 0.00830  1.85631E-04 0.04928  9.97672E-04 0.02238  1.06414E-03 0.02092  2.92436E-03 0.01280  9.80134E-04 0.02294  3.10006E-04 0.03809 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.03741E-01 0.02033  1.24902E-02 1.0E-05  3.14952E-02 0.00052  1.10146E-01 0.00055  3.21044E-01 0.00039  1.34401E+00 0.00040  8.93423E+00 0.00278 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.32232E-05 0.00257  2.32156E-05 0.00258  2.47561E-05 0.02798 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.53650E-05 0.00252  2.53568E-05 0.00253  2.70383E-05 0.02794 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.04223E-03 0.02723  2.20092E-04 0.15549  8.96907E-04 0.06789  9.81800E-04 0.06833  2.83019E-03 0.03956  8.34512E-04 0.07516  2.78721E-04 0.13411 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.53304E-01 0.06755  1.24903E-02 2.5E-05  3.15031E-02 0.00123  1.10159E-01 0.00128  3.20248E-01 0.00092  1.34397E+00 0.00071  8.83443E+00 0.00565 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.09862E-03 0.02657  2.14182E-04 0.15248  9.11305E-04 0.06750  9.88747E-04 0.06686  2.85808E-03 0.03906  8.51747E-04 0.07054  2.74564E-04 0.12908 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.52616E-01 0.06602  1.24903E-02 2.5E-05  3.15056E-02 0.00122  1.10156E-01 0.00127  3.20220E-01 0.00091  1.34388E+00 0.00070  8.82847E+00 0.00580 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.60896E+02 0.02721 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.33994E-05 0.00071 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.55573E-05 0.00047 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.25691E-03 0.00486 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.67460E+02 0.00490 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.70485E-07 0.00071 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.85080E-06 0.00041  2.85060E-06 0.00041  2.87821E-06 0.00511 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.73660E-05 0.00087  3.73841E-05 0.00087  3.46661E-05 0.00947 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.61992E-01 0.00034  6.61515E-01 0.00034  7.54544E-01 0.00928 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.01706E+01 0.01321 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.48080E+01 0.00046  3.38244E+01 0.00044 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.39186E+04 0.00342  2.94164E+05 0.00133  6.05439E+05 0.00083  6.51700E+05 0.00072  5.99075E+05 0.00073  6.43775E+05 0.00067  4.37599E+05 0.00069  3.87437E+05 0.00085  2.96175E+05 0.00078  2.41927E+05 0.00067  2.08766E+05 0.00046  1.87960E+05 0.00069  1.73838E+05 0.00083  1.64848E+05 0.00088  1.60750E+05 0.00076  1.38792E+05 0.00103  1.37170E+05 0.00078  1.35770E+05 0.00083  1.33695E+05 0.00080  2.60676E+05 0.00065  2.51139E+05 0.00079  1.81361E+05 0.00081  1.17316E+05 0.00088  1.35283E+05 0.00072  1.27901E+05 0.00076  1.15592E+05 0.00077  1.89293E+05 0.00081  4.34191E+04 0.00099  5.45476E+04 0.00144  4.94726E+04 0.00148  2.86395E+04 0.00191  5.00077E+04 0.00177  3.39288E+04 0.00169  2.87448E+04 0.00177  5.39246E+03 0.00322  5.29069E+03 0.00397  5.22195E+03 0.00351  5.25450E+03 0.00421  5.27629E+03 0.00321  5.35392E+03 0.00277  5.66989E+03 0.00359  5.34684E+03 0.00251  1.01874E+04 0.00236  1.62865E+04 0.00193  2.08611E+04 0.00199  5.50712E+04 0.00167  5.77009E+04 0.00143  6.22534E+04 0.00174  4.15196E+04 0.00139  3.01252E+04 0.00193  2.28727E+04 0.00149  2.68805E+04 0.00132  5.14383E+04 0.00158  7.04372E+04 0.00132  1.36277E+05 0.00151  2.09860E+05 0.00121  3.11808E+05 0.00152  1.98558E+05 0.00164  1.41896E+05 0.00177  1.01778E+05 0.00165  9.12969E+04 0.00184  8.97581E+04 0.00171  7.49433E+04 0.00182  5.07016E+04 0.00165  4.66950E+04 0.00183  4.15557E+04 0.00197  3.50738E+04 0.00199  2.75760E+04 0.00217  1.84638E+04 0.00238  6.54189E+03 0.00276 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.10126E+00 0.00059 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.19024E+18 0.00057  3.09434E+17 0.00123 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37291E-01 0.00010  1.51571E+00 0.00044 ];
INF_CAPT                  (idx, [1:   4]) = [  6.69741E-03 0.00055  3.17504E-02 0.00051 ];
INF_ABS                   (idx, [1:   4]) = [  8.95389E-03 0.00047  6.77352E-02 0.00089 ];
INF_FISS                  (idx, [1:   4]) = [  2.25647E-03 0.00051  3.59848E-02 0.00124 ];
INF_NSF                   (idx, [1:   4]) = [  5.82619E-03 0.00051  8.99232E-02 0.00125 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.58199E+00 6.2E-05  2.49892E+00 2.5E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04077E+02 6.5E-06  2.03086E+02 4.1E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.82719E-08 0.00034  2.54083E-06 0.00023 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28338E-01 0.00011  1.44798E+00 0.00051 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43137E-01 0.00020  3.84418E-01 0.00051 ];
INF_SCATT2                (idx, [1:   4]) = [  9.59360E-02 0.00032  9.15728E-02 0.00118 ];
INF_SCATT3                (idx, [1:   4]) = [  7.36641E-03 0.00266  2.75886E-02 0.00244 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02464E-02 0.00143 -8.34378E-03 0.00726 ];
INF_SCATT5                (idx, [1:   4]) = [  1.69200E-04 0.07659  6.32030E-03 0.00714 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07143E-03 0.00283 -1.62770E-02 0.00282 ];
INF_SCATT7                (idx, [1:   4]) = [  7.49880E-04 0.02258  3.89066E-04 0.08930 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28379E-01 0.00011  1.44798E+00 0.00051 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43138E-01 0.00020  3.84418E-01 0.00051 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.59362E-02 0.00032  9.15728E-02 0.00118 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.36676E-03 0.00266  2.75886E-02 0.00244 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02464E-02 0.00144 -8.34378E-03 0.00726 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.69260E-04 0.07644  6.32030E-03 0.00714 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07152E-03 0.00283 -1.62770E-02 0.00282 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.50076E-04 0.02255  3.89066E-04 0.08930 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13626E-01 0.00021  9.78586E-01 0.00046 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56036E+00 0.00021  3.40629E-01 0.00046 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.91357E-03 0.00048  6.77352E-02 0.00089 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69415E-02 0.00021  6.89225E-02 0.00110 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10350E-01 0.00010  1.79883E-02 0.00045  1.20152E-03 0.00478  1.44678E+00 0.00051 ];
INF_S1                    (idx, [1:   8]) = [  2.37892E-01 0.00019  5.24556E-03 0.00097  5.10190E-04 0.00732  3.83907E-01 0.00051 ];
INF_S2                    (idx, [1:   8]) = [  9.74967E-02 0.00031 -1.56068E-03 0.00200  2.79812E-04 0.01144  9.12930E-02 0.00118 ];
INF_S3                    (idx, [1:   8]) = [  9.21027E-03 0.00207 -1.84387E-03 0.00156  9.94827E-05 0.02208  2.74891E-02 0.00244 ];
INF_S4                    (idx, [1:   8]) = [ -9.63319E-03 0.00150 -6.13176E-04 0.00373  9.07361E-07 1.00000 -8.34468E-03 0.00733 ];
INF_S5                    (idx, [1:   8]) = [  1.52404E-04 0.09126  1.67961E-05 0.18268 -3.99630E-05 0.04797  6.36027E-03 0.00704 ];
INF_S6                    (idx, [1:   8]) = [  5.21301E-03 0.00262 -1.41580E-04 0.01364 -5.11541E-05 0.03565 -1.62259E-02 0.00287 ];
INF_S7                    (idx, [1:   8]) = [  9.16730E-04 0.01830 -1.66850E-04 0.00885 -4.66225E-05 0.02852  4.35689E-04 0.08033 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10390E-01 0.00010  1.79883E-02 0.00045  1.20152E-03 0.00478  1.44678E+00 0.00051 ];
INF_SP1                   (idx, [1:   8]) = [  2.37893E-01 0.00019  5.24556E-03 0.00097  5.10190E-04 0.00732  3.83907E-01 0.00051 ];
INF_SP2                   (idx, [1:   8]) = [  9.74969E-02 0.00031 -1.56068E-03 0.00200  2.79812E-04 0.01144  9.12930E-02 0.00118 ];
INF_SP3                   (idx, [1:   8]) = [  9.21063E-03 0.00208 -1.84387E-03 0.00156  9.94827E-05 0.02208  2.74891E-02 0.00244 ];
INF_SP4                   (idx, [1:   8]) = [ -9.63323E-03 0.00150 -6.13176E-04 0.00373  9.07361E-07 1.00000 -8.34468E-03 0.00733 ];
INF_SP5                   (idx, [1:   8]) = [  1.52464E-04 0.09108  1.67961E-05 0.18268 -3.99630E-05 0.04797  6.36027E-03 0.00704 ];
INF_SP6                   (idx, [1:   8]) = [  5.21310E-03 0.00263 -1.41580E-04 0.01364 -5.11541E-05 0.03565 -1.62259E-02 0.00287 ];
INF_SP7                   (idx, [1:   8]) = [  9.16926E-04 0.01828 -1.66850E-04 0.00885 -4.66225E-05 0.02852  4.35689E-04 0.08033 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31844E-01 0.00086  1.09854E+00 0.00672 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33104E-01 0.00090  1.17227E+00 0.00807 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33460E-01 0.00130  1.17954E+00 0.00943 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29028E-01 0.00089  9.71841E-01 0.00603 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43777E+00 0.00086  3.03763E-01 0.00674 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.43001E+00 0.00090  2.84785E-01 0.00792 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42786E+00 0.00130  2.83210E-01 0.00959 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45545E+00 0.00089  3.43294E-01 0.00609 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.42677E-03 0.00816  1.77547E-04 0.05072  1.00823E-03 0.02206  1.02917E-03 0.02008  2.94080E-03 0.01292  9.57419E-04 0.02137  3.13605E-04 0.03626 ];
LAMBDA                    (idx, [1:  14]) = [  8.06614E-01 0.01891  1.24902E-02 8.8E-06  3.15018E-02 0.00044  1.10138E-01 0.00045  3.20905E-01 0.00033  1.34469E+00 0.00039  8.91879E+00 0.00265 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0008' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:50:49 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:27:27 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299849 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.96720E-01  1.00148E+00  1.00378E+00  1.00233E+00  9.95693E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 6.6E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14429E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88557E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.00872E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.01307E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68491E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.46989E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.46914E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.60162E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.13029E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000591 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00012E+04 0.00072 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00012E+04 0.00072 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.81105E+02 ;
RUNNING_TIME              (idx, 1)        =  3.66243E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.62450E-01  4.62450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  9.59667E-02  8.23333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.57595E+01  3.03687E+00  2.35155E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  3.03750E-01  2.43000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.87167E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.66242E+01  1.13179E+02 ];
CPU_USAGE                 (idx, 1)        = 4.94493 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99878E+00 0.00026 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.72465E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.17367E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.82437E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.97362E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.60906E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.12312E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.56459E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.71204E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  6.56543E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.08822E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.99926E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.69659E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.56617E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.41856E+07 ;
SR90_ACTIVITY             (idx, 1)        =  4.82970E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.17944E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.21195E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.26780E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.20494E+12 ;
CS137_ACTIVITY            (idx, 1)        =  5.39888E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.99491E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.52423E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  7.82266E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.19291E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.23408E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 6 ;
BURNUP                     (idx, [1:  2])  = [  4.00000E+00  4.00034E+00 ];
BURN_DAYS                 (idx, 1)        =  1.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.11226E-01 0.00112 ];
U235_FISS                 (idx, [1:   4]) = [  1.07406E+16 0.00067  7.77511E-01 0.00036 ];
U238_FISS                 (idx, [1:   4]) = [  9.03133E+14 0.00271  6.53713E-02 0.00257 ];
PU239_FISS                (idx, [1:   4]) = [  2.11437E+15 0.00164  1.53065E-01 0.00159 ];
PU240_FISS                (idx, [1:   4]) = [  9.70203E+11 0.08242  7.01457E-05 0.08244 ];
PU241_FISS                (idx, [1:   4]) = [  5.03889E+13 0.01141  3.64753E-03 0.01138 ];
U235_CAPT                 (idx, [1:   4]) = [  2.36683E+15 0.00173  1.29123E-01 0.00164 ];
U238_CAPT                 (idx, [1:   4]) = [  8.01704E+15 0.00106  4.37335E-01 0.00070 ];
PU239_CAPT                (idx, [1:   4]) = [  1.18661E+15 0.00243  6.47351E-02 0.00237 ];
PU240_CAPT                (idx, [1:   4]) = [  3.54575E+14 0.00456  1.93428E-02 0.00450 ];
PU241_CAPT                (idx, [1:   4]) = [  1.76978E+13 0.01930  9.65151E-04 0.01929 ];
XE135_CAPT                (idx, [1:   4]) = [  7.18923E+14 0.00299  3.92263E-02 0.00303 ];
SM149_CAPT                (idx, [1:   4]) = [  1.68661E+14 0.00592  9.20260E-03 0.00594 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000591 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.34149E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000591 5.00734E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2829984 2.83395E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2133018 2.13580E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 37589 3.75934E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000591 5.00734E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.23986E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 6.3E-09  4.50056E+05 6.3E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.49223E+16 1.5E-05  3.49223E+16 1.5E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38055E+16 2.4E-06  1.38055E+16 2.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.83238E+16 0.00046  1.34591E+16 0.00046  4.86469E+15 0.00109 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.21294E+16 0.00026  2.72647E+16 0.00023  4.86469E+15 0.00109 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.23408E+16 0.00049  3.23408E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.52124E+18 0.00046  4.12484E+17 0.00046  1.10876E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.43206E+14 0.00557 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.23726E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.12345E+18 0.00059 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12048E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12048E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.65817E+00 0.00044 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.95621E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.60504E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24951E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95099E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97370E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.08874E+00 0.00050 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.08055E+00 0.00051 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.52959E+00 1.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03472E+02 2.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.08049E+00 0.00052  1.07379E+00 0.00050  6.76239E-03 0.00866 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.08043E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07995E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.08043E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.08862E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73544E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73576E+01 8.6E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.82667E-07 0.00354 ];
IMP_EALF                  (idx, [1:   2]) = [  5.79349E-07 0.00149 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.22975E-01 0.00282 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.22595E-01 0.00114 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.95717E-03 0.00593  1.73669E-04 0.03189  9.73679E-04 0.01341  9.39732E-04 0.01467  2.71575E-03 0.00897  8.73429E-04 0.01448  2.80914E-04 0.02658 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.85980E-01 0.01374  1.06689E-02 0.01851  3.14407E-02 0.00033  1.10188E-01 0.00034  3.21101E-01 0.00024  1.34217E+00 0.00039  8.40507E+00 0.01133 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.34187E-03 0.00873  1.77078E-04 0.04716  1.04037E-03 0.01962  9.76699E-04 0.02122  2.89351E-03 0.01297  9.45626E-04 0.02130  3.08600E-04 0.04010 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.00202E-01 0.02015  1.24921E-02 0.00013  3.14559E-02 0.00046  1.10249E-01 0.00049  3.21151E-01 0.00034  1.34207E+00 0.00068  8.91778E+00 0.00285 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.35889E-05 0.00124  2.35797E-05 0.00125  2.49382E-05 0.01206 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.54839E-05 0.00112  2.54740E-05 0.00113  2.69385E-05 0.01203 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.25178E-03 0.00876  1.82493E-04 0.04953  1.03246E-03 0.02130  9.60477E-04 0.02179  2.83884E-03 0.01340  9.36590E-04 0.02221  3.00917E-04 0.04062 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.95273E-01 0.02129  1.24920E-02 0.00016  3.14511E-02 0.00056  1.10277E-01 0.00058  3.21005E-01 0.00036  1.34138E+00 0.00067  8.94237E+00 0.00350 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.31675E-05 0.00270  2.31586E-05 0.00272  2.44327E-05 0.02903 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.50281E-05 0.00263  2.50186E-05 0.00265  2.64027E-05 0.02902 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.34039E-03 0.02876  1.26527E-04 0.17043  1.06110E-03 0.06526  9.43043E-04 0.07100  2.91863E-03 0.04183  9.21783E-04 0.07582  3.69314E-04 0.13581 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.72297E-01 0.07099  1.25117E-02 0.00174  3.14070E-02 0.00134  1.10226E-01 0.00139  3.21243E-01 0.00113  1.34539E+00 0.00069  8.92221E+00 0.00960 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.28818E-03 0.02767  1.31991E-04 0.17312  1.04829E-03 0.06447  9.33082E-04 0.07054  2.92371E-03 0.04018  9.06139E-04 0.07425  3.44964E-04 0.12904 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.64183E-01 0.06956  1.25117E-02 0.00174  3.14021E-02 0.00134  1.10213E-01 0.00137  3.21259E-01 0.00113  1.34534E+00 0.00069  8.91553E+00 0.00955 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.75453E+02 0.02906 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.33760E-05 0.00076 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.52536E-05 0.00050 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.30418E-03 0.00527 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.69662E+02 0.00518 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.66333E-07 0.00068 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.83516E-06 0.00042  2.83512E-06 0.00042  2.84181E-06 0.00518 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.71912E-05 0.00085  3.72103E-05 0.00085  3.43267E-05 0.00977 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.58606E-01 0.00034  6.58182E-01 0.00034  7.43844E-01 0.00971 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05779E+01 0.01229 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.46914E+01 0.00046  3.36634E+01 0.00044 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.42267E+04 0.00378  2.95281E+05 0.00136  6.07003E+05 0.00094  6.51313E+05 0.00097  6.00154E+05 0.00079  6.44684E+05 0.00068  4.37256E+05 0.00061  3.86848E+05 0.00086  2.95885E+05 0.00069  2.41772E+05 0.00084  2.08455E+05 0.00063  1.88310E+05 0.00079  1.73597E+05 0.00080  1.65085E+05 0.00086  1.60689E+05 0.00097  1.38914E+05 0.00082  1.37151E+05 0.00080  1.35876E+05 0.00102  1.33352E+05 0.00096  2.60391E+05 0.00063  2.51357E+05 0.00075  1.80898E+05 0.00101  1.17105E+05 0.00079  1.35095E+05 0.00086  1.27844E+05 0.00112  1.15503E+05 0.00099  1.88876E+05 0.00052  4.33519E+04 0.00165  5.44151E+04 0.00153  4.93553E+04 0.00131  2.87109E+04 0.00227  4.99110E+04 0.00134  3.38405E+04 0.00157  2.85160E+04 0.00188  5.40676E+03 0.00361  5.17125E+03 0.00445  5.04661E+03 0.00371  5.04131E+03 0.00375  5.05485E+03 0.00388  5.19247E+03 0.00336  5.57880E+03 0.00385  5.27765E+03 0.00350  1.00406E+04 0.00206  1.61780E+04 0.00264  2.06435E+04 0.00134  5.45615E+04 0.00148  5.73877E+04 0.00110  6.16346E+04 0.00149  4.09281E+04 0.00150  2.95639E+04 0.00147  2.22677E+04 0.00128  2.61879E+04 0.00133  5.03028E+04 0.00156  6.90275E+04 0.00106  1.34004E+05 0.00107  2.06916E+05 0.00114  3.08438E+05 0.00124  1.96395E+05 0.00105  1.40538E+05 0.00139  1.00813E+05 0.00140  9.05787E+04 0.00170  8.92113E+04 0.00168  7.44215E+04 0.00146  5.02378E+04 0.00158  4.64362E+04 0.00143  4.12524E+04 0.00154  3.48004E+04 0.00146  2.73589E+04 0.00167  1.83492E+04 0.00214  6.49421E+03 0.00211 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.08813E+00 0.00053 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.21023E+18 0.00053  3.11046E+17 0.00121 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37279E-01 0.00011  1.52145E+00 0.00039 ];
INF_CAPT                  (idx, [1:   4]) = [  6.81269E-03 0.00058  3.24062E-02 0.00055 ];
INF_ABS                   (idx, [1:   4]) = [  9.03403E-03 0.00047  6.81538E-02 0.00090 ];
INF_FISS                  (idx, [1:   4]) = [  2.22134E-03 0.00036  3.57476E-02 0.00122 ];
INF_NSF                   (idx, [1:   4]) = [  5.75222E-03 0.00037  8.99088E-02 0.00124 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.58952E+00 5.4E-05  2.51510E+00 2.7E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04177E+02 5.0E-06  2.03301E+02 4.5E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.79958E-08 0.00025  2.54432E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28236E-01 0.00011  1.45329E+00 0.00044 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43179E-01 0.00019  3.85980E-01 0.00046 ];
INF_SCATT2                (idx, [1:   4]) = [  9.59254E-02 0.00033  9.18898E-02 0.00096 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32440E-03 0.00276  2.76765E-02 0.00275 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02649E-02 0.00159 -8.37681E-03 0.00742 ];
INF_SCATT5                (idx, [1:   4]) = [  1.42985E-04 0.11036  6.43530E-03 0.00713 ];
INF_SCATT6                (idx, [1:   4]) = [  5.04591E-03 0.00271 -1.63401E-02 0.00379 ];
INF_SCATT7                (idx, [1:   4]) = [  7.24893E-04 0.02223  4.02835E-04 0.14086 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28275E-01 0.00011  1.45329E+00 0.00044 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43180E-01 0.00019  3.85980E-01 0.00046 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.59255E-02 0.00033  9.18898E-02 0.00096 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32432E-03 0.00277  2.76765E-02 0.00275 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02649E-02 0.00159 -8.37681E-03 0.00742 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.43189E-04 0.11019  6.43530E-03 0.00713 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.04581E-03 0.00271 -1.63401E-02 0.00379 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.24745E-04 0.02224  4.02835E-04 0.14086 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13475E-01 0.00026  9.82680E-01 0.00034 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56147E+00 0.00026  3.39209E-01 0.00034 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.99480E-03 0.00047  6.81538E-02 0.00090 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69441E-02 0.00016  6.93782E-02 0.00086 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10335E-01 0.00011  1.79007E-02 0.00036  1.21748E-03 0.00444  1.45208E+00 0.00044 ];
INF_S1                    (idx, [1:   8]) = [  2.37955E-01 0.00019  5.22424E-03 0.00086  5.15423E-04 0.00799  3.85465E-01 0.00047 ];
INF_S2                    (idx, [1:   8]) = [  9.74810E-02 0.00032 -1.55558E-03 0.00282  2.79215E-04 0.01026  9.16106E-02 0.00097 ];
INF_S3                    (idx, [1:   8]) = [  9.16931E-03 0.00228 -1.84491E-03 0.00171  1.00876E-04 0.01866  2.75756E-02 0.00276 ];
INF_S4                    (idx, [1:   8]) = [ -9.65196E-03 0.00169 -6.12953E-04 0.00512  5.34475E-07 1.00000 -8.37735E-03 0.00736 ];
INF_S5                    (idx, [1:   8]) = [  1.21773E-04 0.13039  2.12121E-05 0.10552 -4.05977E-05 0.03937  6.47590E-03 0.00710 ];
INF_S6                    (idx, [1:   8]) = [  5.18498E-03 0.00271 -1.39069E-04 0.01591 -5.09983E-05 0.03489 -1.62891E-02 0.00380 ];
INF_S7                    (idx, [1:   8]) = [  8.92653E-04 0.01668 -1.67761E-04 0.01517 -4.64756E-05 0.02688  4.49310E-04 0.12702 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10374E-01 0.00011  1.79007E-02 0.00036  1.21748E-03 0.00444  1.45208E+00 0.00044 ];
INF_SP1                   (idx, [1:   8]) = [  2.37956E-01 0.00019  5.22424E-03 0.00086  5.15423E-04 0.00799  3.85465E-01 0.00047 ];
INF_SP2                   (idx, [1:   8]) = [  9.74811E-02 0.00032 -1.55558E-03 0.00282  2.79215E-04 0.01026  9.16106E-02 0.00097 ];
INF_SP3                   (idx, [1:   8]) = [  9.16923E-03 0.00228 -1.84491E-03 0.00171  1.00876E-04 0.01866  2.75756E-02 0.00276 ];
INF_SP4                   (idx, [1:   8]) = [ -9.65196E-03 0.00169 -6.12953E-04 0.00512  5.34475E-07 1.00000 -8.37735E-03 0.00736 ];
INF_SP5                   (idx, [1:   8]) = [  1.21976E-04 0.13013  2.12121E-05 0.10552 -4.05977E-05 0.03937  6.47590E-03 0.00710 ];
INF_SP6                   (idx, [1:   8]) = [  5.18488E-03 0.00271 -1.39069E-04 0.01591 -5.09983E-05 0.03489 -1.62891E-02 0.00380 ];
INF_SP7                   (idx, [1:   8]) = [  8.92506E-04 0.01668 -1.67761E-04 0.01517 -4.64756E-05 0.02688  4.49310E-04 0.12702 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32022E-01 0.00054  1.10905E+00 0.00707 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33499E-01 0.00077  1.18496E+00 0.00889 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33557E-01 0.00081  1.17409E+00 0.00914 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29076E-01 0.00098  9.91663E-01 0.00619 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43665E+00 0.00054  3.00920E-01 0.00713 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42758E+00 0.00077  2.81830E-01 0.00879 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42723E+00 0.00081  2.84484E-01 0.00926 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45516E+00 0.00098  3.36447E-01 0.00624 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.34187E-03 0.00873  1.77078E-04 0.04716  1.04037E-03 0.01962  9.76699E-04 0.02122  2.89351E-03 0.01297  9.45626E-04 0.02130  3.08600E-04 0.04010 ];
LAMBDA                    (idx, [1:  14]) = [  8.00202E-01 0.02015  1.24921E-02 0.00013  3.14559E-02 0.00046  1.10249E-01 0.00049  3.21151E-01 0.00034  1.34207E+00 0.00068  8.91778E+00 0.00285 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0008' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:50:49 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:32:54 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299849 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.92374E-01  1.00108E+00  1.00622E+00  1.00409E+00  9.96231E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14167E-02 0.00103  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88583E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.02065E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.02498E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67915E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.46556E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.46479E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.52659E+00 0.00036  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.10883E-01 0.00108  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000727 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00015E+04 0.00073 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00015E+04 0.00073 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.08370E+02 ;
RUNNING_TIME              (idx, 1)        =  4.20833E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.62450E-01  4.62450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.14667E-01  9.60000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  4.11494E+01  3.04185E+00  2.34800E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  3.53500E-01  2.60833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.10667E-02  8.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.20831E+01  1.13014E+02 ];
CPU_USAGE                 (idx, 1)        = 4.95138 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99972E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.74033E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.21406E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.81390E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.05862E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.62469E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.13398E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.58935E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.70048E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  7.46731E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.16973E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.50139E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.77650E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.96592E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.49208E+07 ;
SR90_ACTIVITY             (idx, 1)        =  5.95355E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.20281E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.24619E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.29966E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.92397E+12 ;
CS137_ACTIVITY            (idx, 1)        =  6.74773E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.02184E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.50060E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.15634E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.20123E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.28443E+12 0.00048  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 7 ;
BURNUP                     (idx, [1:  2])  = [  5.00000E+00  5.00043E+00 ];
BURN_DAYS                 (idx, 1)        =  1.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.19694E-01 0.00109 ];
U235_FISS                 (idx, [1:   4]) = [  1.03154E+16 0.00066  7.47914E-01 0.00041 ];
U238_FISS                 (idx, [1:   4]) = [  9.19107E+14 0.00278  6.66322E-02 0.00265 ];
PU239_FISS                (idx, [1:   4]) = [  2.46601E+15 0.00157  1.78798E-01 0.00149 ];
PU240_FISS                (idx, [1:   4]) = [  1.28160E+12 0.07462  9.27816E-05 0.07464 ];
PU241_FISS                (idx, [1:   4]) = [  8.51641E+13 0.00891  6.17517E-03 0.00891 ];
U235_CAPT                 (idx, [1:   4]) = [  2.28187E+15 0.00168  1.21064E-01 0.00160 ];
U238_CAPT                 (idx, [1:   4]) = [  8.08282E+15 0.00103  4.28803E-01 0.00072 ];
PU239_CAPT                (idx, [1:   4]) = [  1.38622E+15 0.00217  7.35475E-02 0.00213 ];
PU240_CAPT                (idx, [1:   4]) = [  4.75318E+14 0.00372  2.52156E-02 0.00363 ];
PU241_CAPT                (idx, [1:   4]) = [  3.05870E+13 0.01463  1.62283E-03 0.01462 ];
XE135_CAPT                (idx, [1:   4]) = [  7.21194E+14 0.00309  3.82609E-02 0.00301 ];
SM149_CAPT                (idx, [1:   4]) = [  1.75063E+14 0.00602  9.28775E-03 0.00598 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000727 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.76893E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000727 5.00777E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2865258 2.86943E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2096900 2.09976E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 38569 3.85796E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000727 5.00777E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.93601E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 6.3E-09  4.50056E+05 6.3E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.50731E+16 1.7E-05  3.50731E+16 1.7E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37937E+16 2.8E-06  1.37937E+16 2.8E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.88357E+16 0.00044  1.39023E+16 0.00046  4.93341E+15 0.00111 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.26294E+16 0.00026  2.76960E+16 0.00023  4.93341E+15 0.00111 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.28443E+16 0.00048  3.28443E+16 0.00048  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.54275E+18 0.00044  4.17352E+17 0.00044  1.12540E+18 0.00049 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.53476E+14 0.00561 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.28829E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.13961E+18 0.00058 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11931E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11931E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.64816E+00 0.00044 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.94784E-01 0.00030 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.58225E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24804E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94982E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97288E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.07602E+00 0.00050 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06772E+00 0.00050 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.54269E+00 1.9E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03646E+02 2.8E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06804E+00 0.00053  1.06110E+00 0.00051  6.61825E-03 0.00863 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06826E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06798E+00 0.00048 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06826E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.07657E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73298E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73283E+01 8.5E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.97157E-07 0.00351 ];
IMP_EALF                  (idx, [1:   2]) = [  5.96624E-07 0.00147 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.27097E-01 0.00288 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.26873E-01 0.00111 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.94349E-03 0.00565  1.69131E-04 0.03251  9.77600E-04 0.01377  9.63561E-04 0.01398  2.68987E-03 0.00803  8.61720E-04 0.01448  2.81612E-04 0.02635 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.86956E-01 0.01378  1.06427E-02 0.01866  3.13764E-02 0.00033  1.10187E-01 0.00035  3.21231E-01 0.00027  1.34039E+00 0.00049  8.48698E+00 0.01070 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.23102E-03 0.00803  1.82797E-04 0.04747  1.00929E-03 0.02072  1.00703E-03 0.02082  2.85140E-03 0.01175  8.97174E-04 0.02182  2.83328E-04 0.03824 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.67748E-01 0.01955  1.24924E-02 0.00015  3.13883E-02 0.00048  1.10162E-01 0.00049  3.21222E-01 0.00038  1.34066E+00 0.00064  8.92726E+00 0.00363 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.36540E-05 0.00116  2.36442E-05 0.00116  2.52582E-05 0.01346 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.52603E-05 0.00105  2.52498E-05 0.00105  2.69702E-05 0.01341 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.18432E-03 0.00867  1.77663E-04 0.05420  9.93340E-04 0.02144  9.90768E-04 0.02197  2.82998E-03 0.01219  9.12001E-04 0.02276  2.80566E-04 0.04188 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.72155E-01 0.02143  1.24947E-02 0.00027  3.14040E-02 0.00058  1.10205E-01 0.00060  3.21321E-01 0.00041  1.33941E+00 0.00082  8.89945E+00 0.00505 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.32766E-05 0.00287  2.32641E-05 0.00288  2.38875E-05 0.03220 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.48562E-05 0.00280  2.48428E-05 0.00280  2.55143E-05 0.03217 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.96430E-03 0.02821  1.60470E-04 0.17303  9.49288E-04 0.07311  9.96276E-04 0.08021  2.62348E-03 0.04327  8.79289E-04 0.07670  3.55501E-04 0.13094 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.98304E-01 0.07273  1.25109E-02 0.00164  3.13478E-02 0.00148  1.10429E-01 0.00151  3.21281E-01 0.00118  1.33601E+00 0.00274  8.96695E+00 0.00950 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.94154E-03 0.02761  1.56744E-04 0.16990  9.78774E-04 0.07155  9.80456E-04 0.07643  2.62772E-03 0.04239  8.58393E-04 0.07391  3.39448E-04 0.12416 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.86261E-01 0.07217  1.25109E-02 0.00164  3.13466E-02 0.00146  1.10424E-01 0.00149  3.21265E-01 0.00115  1.33635E+00 0.00272  8.96462E+00 0.00950 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.57266E+02 0.02827 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.34562E-05 0.00073 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.50486E-05 0.00049 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.08121E-03 0.00515 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.59281E+02 0.00513 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.64315E-07 0.00074 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.81994E-06 0.00042  2.81992E-06 0.00042  2.82174E-06 0.00538 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.71069E-05 0.00090  3.71250E-05 0.00090  3.43646E-05 0.00988 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.56312E-01 0.00034  6.55932E-01 0.00034  7.31426E-01 0.00881 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04341E+01 0.01438 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.46479E+01 0.00048  3.35616E+01 0.00043 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.47320E+04 0.00327  2.97116E+05 0.00117  6.07736E+05 0.00115  6.51626E+05 0.00075  5.99960E+05 0.00062  6.43304E+05 0.00062  4.37152E+05 0.00096  3.87343E+05 0.00081  2.95923E+05 0.00064  2.41925E+05 0.00080  2.08677E+05 0.00086  1.88262E+05 0.00059  1.73567E+05 0.00088  1.64853E+05 0.00088  1.60703E+05 0.00114  1.38632E+05 0.00093  1.36979E+05 0.00088  1.35757E+05 0.00059  1.33257E+05 0.00088  2.60375E+05 0.00074  2.51138E+05 0.00063  1.81041E+05 0.00058  1.17237E+05 0.00118  1.35065E+05 0.00092  1.27807E+05 0.00086  1.15416E+05 0.00090  1.88619E+05 0.00072  4.32883E+04 0.00123  5.44312E+04 0.00190  4.94951E+04 0.00165  2.87232E+04 0.00178  4.98014E+04 0.00151  3.37819E+04 0.00238  2.85340E+04 0.00208  5.27126E+03 0.00378  5.05501E+03 0.00427  4.79508E+03 0.00282  4.78124E+03 0.00365  4.83628E+03 0.00347  5.03205E+03 0.00319  5.44433E+03 0.00366  5.20806E+03 0.00445  9.96889E+03 0.00249  1.60530E+04 0.00198  2.05160E+04 0.00256  5.43087E+04 0.00162  5.69181E+04 0.00140  6.10832E+04 0.00116  4.04132E+04 0.00132  2.90112E+04 0.00172  2.18729E+04 0.00196  2.56352E+04 0.00130  4.94332E+04 0.00159  6.83316E+04 0.00149  1.32806E+05 0.00127  2.05531E+05 0.00142  3.06559E+05 0.00138  1.95389E+05 0.00179  1.40016E+05 0.00200  1.00516E+05 0.00164  9.01939E+04 0.00155  8.88039E+04 0.00168  7.40099E+04 0.00183  5.00501E+04 0.00171  4.63110E+04 0.00171  4.11486E+04 0.00201  3.47535E+04 0.00175  2.74069E+04 0.00200  1.83564E+04 0.00224  6.48879E+03 0.00223 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.07628E+00 0.00053 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.22896E+18 0.00050  3.13834E+17 0.00143 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37316E-01 8.9E-05  1.52724E+00 0.00043 ];
INF_CAPT                  (idx, [1:   4]) = [  6.91699E-03 0.00079  3.29353E-02 0.00065 ];
INF_ABS                   (idx, [1:   4]) = [  9.10558E-03 0.00065  6.83235E-02 0.00105 ];
INF_FISS                  (idx, [1:   4]) = [  2.18859E-03 0.00042  3.53882E-02 0.00143 ];
INF_NSF                   (idx, [1:   4]) = [  5.68413E-03 0.00042  8.95145E-02 0.00145 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.59717E+00 5.8E-05  2.52950E+00 3.3E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04274E+02 5.3E-06  2.03494E+02 5.5E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.77914E-08 0.00039  2.54797E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28211E-01 9.4E-05  1.45888E+00 0.00050 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43233E-01 0.00020  3.87361E-01 0.00058 ];
INF_SCATT2                (idx, [1:   4]) = [  9.59771E-02 0.00034  9.20658E-02 0.00112 ];
INF_SCATT3                (idx, [1:   4]) = [  7.37602E-03 0.00373  2.75615E-02 0.00271 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02406E-02 0.00223 -8.58397E-03 0.00739 ];
INF_SCATT5                (idx, [1:   4]) = [  1.67764E-04 0.10612  6.43068E-03 0.00951 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07462E-03 0.00312 -1.64417E-02 0.00229 ];
INF_SCATT7                (idx, [1:   4]) = [  7.54641E-04 0.02123  3.27870E-04 0.11285 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28252E-01 9.4E-05  1.45888E+00 0.00050 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43233E-01 0.00020  3.87361E-01 0.00058 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.59772E-02 0.00034  9.20658E-02 0.00112 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.37618E-03 0.00373  2.75615E-02 0.00271 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02405E-02 0.00223 -8.58397E-03 0.00739 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.67752E-04 0.10627  6.43068E-03 0.00951 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07453E-03 0.00312 -1.64417E-02 0.00229 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.54595E-04 0.02125  3.27870E-04 0.11285 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13247E-01 0.00030  9.86999E-01 0.00039 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56314E+00 0.00030  3.37725E-01 0.00039 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.06406E-03 0.00066  6.83235E-02 0.00105 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69415E-02 0.00019  6.95665E-02 0.00114 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10375E-01 9.0E-05  1.78364E-02 0.00039  1.20902E-03 0.00403  1.45767E+00 0.00050 ];
INF_S1                    (idx, [1:   8]) = [  2.38041E-01 0.00020  5.19179E-03 0.00082  5.12336E-04 0.00720  3.86849E-01 0.00058 ];
INF_S2                    (idx, [1:   8]) = [  9.75377E-02 0.00033 -1.56063E-03 0.00265  2.78322E-04 0.00886  9.17875E-02 0.00113 ];
INF_S3                    (idx, [1:   8]) = [  9.20967E-03 0.00298 -1.83365E-03 0.00206  9.79642E-05 0.02314  2.74635E-02 0.00270 ];
INF_S4                    (idx, [1:   8]) = [ -9.63872E-03 0.00229 -6.01919E-04 0.00499 -3.81174E-06 0.43459 -8.58016E-03 0.00740 ];
INF_S5                    (idx, [1:   8]) = [  1.45755E-04 0.11814  2.20091E-05 0.08102 -4.14268E-05 0.03727  6.47210E-03 0.00952 ];
INF_S6                    (idx, [1:   8]) = [  5.21335E-03 0.00299 -1.38728E-04 0.02067 -5.46565E-05 0.03158 -1.63871E-02 0.00233 ];
INF_S7                    (idx, [1:   8]) = [  9.27151E-04 0.01732 -1.72510E-04 0.00981 -4.65991E-05 0.02235  3.74469E-04 0.09840 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10416E-01 9.0E-05  1.78364E-02 0.00039  1.20902E-03 0.00403  1.45767E+00 0.00050 ];
INF_SP1                   (idx, [1:   8]) = [  2.38041E-01 0.00020  5.19179E-03 0.00082  5.12336E-04 0.00720  3.86849E-01 0.00058 ];
INF_SP2                   (idx, [1:   8]) = [  9.75379E-02 0.00033 -1.56063E-03 0.00265  2.78322E-04 0.00886  9.17875E-02 0.00113 ];
INF_SP3                   (idx, [1:   8]) = [  9.20983E-03 0.00297 -1.83365E-03 0.00206  9.79642E-05 0.02314  2.74635E-02 0.00270 ];
INF_SP4                   (idx, [1:   8]) = [ -9.63855E-03 0.00229 -6.01919E-04 0.00499 -3.81174E-06 0.43459 -8.58016E-03 0.00740 ];
INF_SP5                   (idx, [1:   8]) = [  1.45743E-04 0.11834  2.20091E-05 0.08102 -4.14268E-05 0.03727  6.47210E-03 0.00952 ];
INF_SP6                   (idx, [1:   8]) = [  5.21326E-03 0.00299 -1.38728E-04 0.02067 -5.46565E-05 0.03158 -1.63871E-02 0.00233 ];
INF_SP7                   (idx, [1:   8]) = [  9.27106E-04 0.01733 -1.72510E-04 0.00981 -4.65991E-05 0.02235  3.74469E-04 0.09840 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31782E-01 0.00062  1.12918E+00 0.00830 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33312E-01 0.00096  1.21303E+00 0.01163 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33183E-01 0.00080  1.20734E+00 0.00924 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28913E-01 0.00094  9.97380E-01 0.00808 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43815E+00 0.00062  2.95683E-01 0.00822 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42874E+00 0.00096  2.75684E-01 0.01159 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42952E+00 0.00080  2.76640E-01 0.00898 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45619E+00 0.00094  3.34725E-01 0.00795 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.23102E-03 0.00803  1.82797E-04 0.04747  1.00929E-03 0.02072  1.00703E-03 0.02082  2.85140E-03 0.01175  8.97174E-04 0.02182  2.83328E-04 0.03824 ];
LAMBDA                    (idx, [1:  14]) = [  7.67748E-01 0.01955  1.24924E-02 0.00015  3.13883E-02 0.00048  1.10162E-01 0.00049  3.21222E-01 0.00038  1.34066E+00 0.00064  8.92726E+00 0.00363 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0008' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:50:49 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:38:24 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299849 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.93873E-01  9.98193E-01  1.00474E+00  9.99528E-01  1.00367E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14076E-02 0.00106  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88592E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.02834E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.03268E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67647E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.46399E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.46320E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.48128E+00 0.00034  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.09555E-01 0.00109  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000693 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00014E+04 0.00073 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00014E+04 0.00073 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.35791E+02 ;
RUNNING_TIME              (idx, 1)        =  4.75734E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.62450E-01  4.62450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.33717E-01  9.26667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  4.65684E+01  3.04820E+00  2.37083E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  4.05050E-01  2.50333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.34833E-02  7.83332E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.75733E+01  1.13349E+02 ];
CPU_USAGE                 (idx, 1)        = 4.95636 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99925E+00 0.00022 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75256E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.24957E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.80479E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.33953E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.64233E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.14623E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.60722E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.69014E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  8.38719E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.23798E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.08074E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.86636E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.30645E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.55134E+07 ;
SR90_ACTIVITY             (idx, 1)        =  7.05037E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.22440E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.27636E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.32891E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.80652E+12 ;
CS137_ACTIVITY            (idx, 1)        =  8.09572E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.04786E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.47966E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.69630E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.20911E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.33526E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 8 ;
BURNUP                     (idx, [1:  2])  = [  6.00000E+00  6.00052E+00 ];
BURN_DAYS                 (idx, 1)        =  1.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.30037E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  9.91471E+15 0.00076  7.19908E-01 0.00044 ];
U238_FISS                 (idx, [1:   4]) = [  9.27678E+14 0.00255  6.73580E-02 0.00246 ];
PU239_FISS                (idx, [1:   4]) = [  2.79716E+15 0.00148  2.03104E-01 0.00136 ];
PU240_FISS                (idx, [1:   4]) = [  1.78780E+12 0.06304  1.29865E-04 0.06312 ];
PU241_FISS                (idx, [1:   4]) = [  1.24479E+14 0.00711  9.03851E-03 0.00707 ];
U235_CAPT                 (idx, [1:   4]) = [  2.20596E+15 0.00166  1.13905E-01 0.00163 ];
U238_CAPT                 (idx, [1:   4]) = [  8.17706E+15 0.00104  4.22177E-01 0.00072 ];
PU239_CAPT                (idx, [1:   4]) = [  1.56394E+15 0.00204  8.07562E-02 0.00203 ];
PU240_CAPT                (idx, [1:   4]) = [  5.98164E+14 0.00350  3.08843E-02 0.00345 ];
PU241_CAPT                (idx, [1:   4]) = [  4.54905E+13 0.01214  2.34904E-03 0.01214 ];
XE135_CAPT                (idx, [1:   4]) = [  7.24171E+14 0.00320  3.73916E-02 0.00317 ];
SM149_CAPT                (idx, [1:   4]) = [  1.77493E+14 0.00626  9.16540E-03 0.00627 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000693 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.69479E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000693 5.00769E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2899310 2.90352E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2061911 2.06470E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 39472 3.94803E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000693 5.00769E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.84288E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 6.3E-09  4.50056E+05 6.3E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.52117E+16 1.7E-05  3.52117E+16 1.7E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37826E+16 2.9E-06  1.37826E+16 2.9E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.93682E+16 0.00046  1.43597E+16 0.00045  5.00845E+15 0.00117 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.31508E+16 0.00027  2.81423E+16 0.00023  5.00845E+15 0.00117 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.33526E+16 0.00050  3.33526E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.56477E+18 0.00047  4.22197E+17 0.00045  1.14257E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.63383E+14 0.00528 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.34141E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.15668E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11815E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11815E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.63717E+00 0.00046 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.94409E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.55331E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24773E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94868E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97221E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.06338E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.05499E+00 0.00052 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.55479E+00 1.9E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03810E+02 2.9E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.05489E+00 0.00052  1.04864E+00 0.00052  6.34643E-03 0.00924 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.05545E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.05587E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.05545E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.06385E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73066E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73035E+01 8.4E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.11071E-07 0.00339 ];
IMP_EALF                  (idx, [1:   2]) = [  6.11547E-07 0.00145 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.30286E-01 0.00270 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.30233E-01 0.00111 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.80174E-03 0.00620  1.57539E-04 0.03391  9.64036E-04 0.01376  9.15532E-04 0.01529  2.62645E-03 0.00862  8.72605E-04 0.01488  2.65575E-04 0.02660 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.77378E-01 0.01340  1.00452E-02 0.02210  3.13569E-02 0.00036  1.10169E-01 0.00036  3.21350E-01 0.00025  1.33721E+00 0.00079  8.43491E+00 0.01162 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.03244E-03 0.00862  1.60439E-04 0.05278  1.01917E-03 0.02204  9.43941E-04 0.02184  2.72314E-03 0.01278  9.09677E-04 0.02206  2.76071E-04 0.03972 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.80382E-01 0.01993  1.24931E-02 0.00021  3.13412E-02 0.00052  1.10103E-01 0.00047  3.21307E-01 0.00036  1.33579E+00 0.00126  8.95185E+00 0.00416 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.37350E-05 0.00124  2.37235E-05 0.00125  2.55290E-05 0.01430 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.50340E-05 0.00110  2.50219E-05 0.00110  2.69264E-05 0.01429 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.01503E-03 0.00939  1.50130E-04 0.05423  1.02854E-03 0.02241  9.43694E-04 0.02270  2.73280E-03 0.01376  8.81894E-04 0.02433  2.77970E-04 0.04332 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.72749E-01 0.02203  1.24986E-02 0.00051  3.13378E-02 0.00064  1.10136E-01 0.00062  3.21296E-01 0.00041  1.33569E+00 0.00150  8.95892E+00 0.00548 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.32644E-05 0.00291  2.32489E-05 0.00293  2.49728E-05 0.03819 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.45374E-05 0.00284  2.45210E-05 0.00286  2.63483E-05 0.03825 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.22572E-03 0.03074  1.58920E-04 0.18316  9.94191E-04 0.06993  9.54226E-04 0.07315  2.99146E-03 0.04397  8.09387E-04 0.07965  3.17536E-04 0.13423 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.38667E-01 0.06823  1.25131E-02 0.00189  3.13164E-02 0.00146  1.10077E-01 0.00138  3.20853E-01 0.00106  1.34186E+00 0.00109  8.90407E+00 0.01047 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.18682E-03 0.02987  1.51762E-04 0.17884  1.01506E-03 0.06887  9.60745E-04 0.07098  2.94957E-03 0.04368  8.00245E-04 0.07612  3.09437E-04 0.12893 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.37948E-01 0.06623  1.25131E-02 0.00189  3.13189E-02 0.00145  1.10107E-01 0.00139  3.20881E-01 0.00106  1.34170E+00 0.00106  8.90610E+00 0.01048 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.68120E+02 0.03067 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.35170E-05 0.00077 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.48042E-05 0.00053 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.17440E-03 0.00580 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.62548E+02 0.00575 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.62931E-07 0.00075 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.80490E-06 0.00042  2.80471E-06 0.00042  2.83553E-06 0.00552 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.71520E-05 0.00091  3.71689E-05 0.00091  3.44616E-05 0.00988 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.53362E-01 0.00032  6.53062E-01 0.00033  7.19002E-01 0.00988 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02918E+01 0.01326 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.46320E+01 0.00048  3.34476E+01 0.00045 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.51181E+04 0.00315  2.97394E+05 0.00155  6.08796E+05 0.00093  6.51487E+05 0.00094  5.99557E+05 0.00056  6.43517E+05 0.00067  4.37277E+05 0.00059  3.86638E+05 0.00081  2.95907E+05 0.00083  2.41603E+05 0.00082  2.08434E+05 0.00082  1.87923E+05 0.00083  1.73778E+05 0.00080  1.65020E+05 0.00077  1.60622E+05 0.00080  1.38789E+05 0.00083  1.37094E+05 0.00120  1.35984E+05 0.00101  1.33621E+05 0.00098  2.60376E+05 0.00090  2.51312E+05 0.00067  1.81173E+05 0.00079  1.17210E+05 0.00088  1.35108E+05 0.00086  1.27859E+05 0.00081  1.15292E+05 0.00104  1.88306E+05 0.00085  4.32104E+04 0.00136  5.43331E+04 0.00172  4.92770E+04 0.00147  2.85802E+04 0.00168  4.95457E+04 0.00135  3.36419E+04 0.00207  2.83602E+04 0.00215  5.21875E+03 0.00395  4.96139E+03 0.00371  4.67217E+03 0.00239  4.56473E+03 0.00361  4.62802E+03 0.00421  4.89064E+03 0.00374  5.35511E+03 0.00400  5.16631E+03 0.00363  9.89502E+03 0.00300  1.59168E+04 0.00246  2.04048E+04 0.00244  5.39623E+04 0.00124  5.66494E+04 0.00124  6.08061E+04 0.00146  3.99699E+04 0.00148  2.87083E+04 0.00160  2.15450E+04 0.00144  2.53258E+04 0.00194  4.87216E+04 0.00177  6.74860E+04 0.00146  1.31638E+05 0.00145  2.04053E+05 0.00142  3.04891E+05 0.00172  1.94778E+05 0.00167  1.39561E+05 0.00187  1.00198E+05 0.00185  9.00871E+04 0.00191  8.87480E+04 0.00193  7.40213E+04 0.00181  5.00590E+04 0.00182  4.62704E+04 0.00203  4.10881E+04 0.00191  3.47780E+04 0.00204  2.74063E+04 0.00199  1.83413E+04 0.00233  6.46612E+03 0.00264 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.06427E+00 0.00052 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.24768E+18 0.00049  3.17114E+17 0.00133 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37502E-01 0.00010  1.53279E+00 0.00039 ];
INF_CAPT                  (idx, [1:   4]) = [  7.03345E-03 0.00064  3.34066E-02 0.00066 ];
INF_ABS                   (idx, [1:   4]) = [  9.18727E-03 0.00052  6.84017E-02 0.00098 ];
INF_FISS                  (idx, [1:   4]) = [  2.15382E-03 0.00045  3.49951E-02 0.00129 ];
INF_NSF                   (idx, [1:   4]) = [  5.60891E-03 0.00046  8.89866E-02 0.00132 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.60416E+00 5.4E-05  2.54283E+00 3.9E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04366E+02 5.0E-06  2.03675E+02 6.5E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.75519E-08 0.00048  2.55147E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28320E-01 0.00011  1.46440E+00 0.00046 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43367E-01 0.00019  3.89059E-01 0.00051 ];
INF_SCATT2                (idx, [1:   4]) = [  9.60750E-02 0.00028  9.26327E-02 0.00089 ];
INF_SCATT3                (idx, [1:   4]) = [  7.38515E-03 0.00293  2.79084E-02 0.00270 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02578E-02 0.00214 -8.45956E-03 0.00641 ];
INF_SCATT5                (idx, [1:   4]) = [  1.32894E-04 0.13319  6.38076E-03 0.00788 ];
INF_SCATT6                (idx, [1:   4]) = [  5.06612E-03 0.00306 -1.65473E-02 0.00317 ];
INF_SCATT7                (idx, [1:   4]) = [  7.58935E-04 0.01482  3.40726E-04 0.10639 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28361E-01 0.00011  1.46440E+00 0.00046 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43367E-01 0.00019  3.89059E-01 0.00051 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.60752E-02 0.00028  9.26327E-02 0.00089 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.38487E-03 0.00293  2.79084E-02 0.00270 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02579E-02 0.00213 -8.45956E-03 0.00641 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.32910E-04 0.13332  6.38076E-03 0.00788 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.06612E-03 0.00306 -1.65473E-02 0.00317 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.58968E-04 0.01485  3.40726E-04 0.10639 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13158E-01 0.00028  9.90710E-01 0.00031 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56379E+00 0.00028  3.36460E-01 0.00031 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.14614E-03 0.00052  6.84017E-02 0.00098 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69451E-02 0.00015  6.96007E-02 0.00118 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10557E-01 0.00010  1.77632E-02 0.00048  1.21799E-03 0.00378  1.46318E+00 0.00046 ];
INF_S1                    (idx, [1:   8]) = [  2.38199E-01 0.00018  5.16815E-03 0.00113  5.15693E-04 0.00678  3.88543E-01 0.00052 ];
INF_S2                    (idx, [1:   8]) = [  9.76340E-02 0.00028 -1.55901E-03 0.00233  2.85830E-04 0.00838  9.23468E-02 0.00090 ];
INF_S3                    (idx, [1:   8]) = [  9.21157E-03 0.00233 -1.82642E-03 0.00150  1.03509E-04 0.01874  2.78048E-02 0.00271 ];
INF_S4                    (idx, [1:   8]) = [ -9.65854E-03 0.00220 -5.99300E-04 0.00504  3.01617E-06 0.55680 -8.46257E-03 0.00645 ];
INF_S5                    (idx, [1:   8]) = [  1.12970E-04 0.15611  1.99238E-05 0.14471 -4.06425E-05 0.04384  6.42140E-03 0.00788 ];
INF_S6                    (idx, [1:   8]) = [  5.20813E-03 0.00301 -1.42014E-04 0.01403 -5.01692E-05 0.03246 -1.64971E-02 0.00316 ];
INF_S7                    (idx, [1:   8]) = [  9.29147E-04 0.01109 -1.70212E-04 0.01485 -4.49361E-05 0.03060  3.85662E-04 0.09436 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10598E-01 0.00010  1.77632E-02 0.00048  1.21799E-03 0.00378  1.46318E+00 0.00046 ];
INF_SP1                   (idx, [1:   8]) = [  2.38199E-01 0.00018  5.16815E-03 0.00113  5.15693E-04 0.00678  3.88543E-01 0.00052 ];
INF_SP2                   (idx, [1:   8]) = [  9.76342E-02 0.00027 -1.55901E-03 0.00233  2.85830E-04 0.00838  9.23468E-02 0.00090 ];
INF_SP3                   (idx, [1:   8]) = [  9.21129E-03 0.00233 -1.82642E-03 0.00150  1.03509E-04 0.01874  2.78048E-02 0.00271 ];
INF_SP4                   (idx, [1:   8]) = [ -9.65865E-03 0.00220 -5.99300E-04 0.00504  3.01617E-06 0.55680 -8.46257E-03 0.00645 ];
INF_SP5                   (idx, [1:   8]) = [  1.12986E-04 0.15618  1.99238E-05 0.14471 -4.06425E-05 0.04384  6.42140E-03 0.00788 ];
INF_SP6                   (idx, [1:   8]) = [  5.20813E-03 0.00300 -1.42014E-04 0.01403 -5.01692E-05 0.03246 -1.64971E-02 0.00316 ];
INF_SP7                   (idx, [1:   8]) = [  9.29180E-04 0.01111 -1.70212E-04 0.01485 -4.49361E-05 0.03060  3.85662E-04 0.09436 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31843E-01 0.00044  1.13560E+00 0.00751 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33460E-01 0.00101  1.21185E+00 0.00919 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33459E-01 0.00089  1.21997E+00 0.00951 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28685E-01 0.00087  1.00424E+00 0.00719 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43776E+00 0.00044  2.93931E-01 0.00758 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42783E+00 0.00102  2.75624E-01 0.00926 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42783E+00 0.00089  2.73827E-01 0.00955 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45764E+00 0.00087  3.32342E-01 0.00725 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.03244E-03 0.00862  1.60439E-04 0.05278  1.01917E-03 0.02204  9.43941E-04 0.02184  2.72314E-03 0.01278  9.09677E-04 0.02206  2.76071E-04 0.03972 ];
LAMBDA                    (idx, [1:  14]) = [  7.80382E-01 0.01993  1.24931E-02 0.00021  3.13412E-02 0.00052  1.10103E-01 0.00047  3.21307E-01 0.00036  1.33579E+00 0.00126  8.95185E+00 0.00416 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0008' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:50:49 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:43:54 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299849 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.94493E-01  9.98748E-01  1.00418E+00  1.00026E+00  1.00232E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.0E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14121E-02 0.00107  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88588E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.03529E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.03962E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67302E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.45936E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.45857E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.43289E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.08636E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000928 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00019E+04 0.00085 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00019E+04 0.00085 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.63279E+02 ;
RUNNING_TIME              (idx, 1)        =  5.30770E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.62450E-01  4.62450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.51767E-01  8.71667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  5.20022E+01  3.05743E+00  2.37640E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  4.56117E-01  2.56167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.60333E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  5.30769E+01  1.13503E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96032 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99964E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76215E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.28276E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.79687E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.79107E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.66224E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.16008E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62050E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.68084E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  9.34277E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.29751E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.73953E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.96699E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.60323E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.60082E+07 ;
SR90_ACTIVITY             (idx, 1)        =  8.12213E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.24442E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.30362E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.35589E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.84113E+12 ;
CS137_ACTIVITY            (idx, 1)        =  9.44278E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.07436E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.46074E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.46171E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.21693E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.38563E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 9 ;
BURNUP                     (idx, [1:  2])  = [  7.00000E+00  7.00061E+00 ];
BURN_DAYS                 (idx, 1)        =  1.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.39261E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  9.56529E+15 0.00079  6.94610E-01 0.00051 ];
U238_FISS                 (idx, [1:   4]) = [  9.39948E+14 0.00282  6.82470E-02 0.00264 ];
PU239_FISS                (idx, [1:   4]) = [  3.08461E+15 0.00154  2.23998E-01 0.00143 ];
PU240_FISS                (idx, [1:   4]) = [  2.45546E+12 0.05353  1.78304E-04 0.05348 ];
PU241_FISS                (idx, [1:   4]) = [  1.71676E+14 0.00643  1.24666E-02 0.00640 ];
U235_CAPT                 (idx, [1:   4]) = [  2.12483E+15 0.00174  1.06949E-01 0.00166 ];
U238_CAPT                 (idx, [1:   4]) = [  8.25196E+15 0.00106  4.15303E-01 0.00066 ];
PU239_CAPT                (idx, [1:   4]) = [  1.72667E+15 0.00196  8.69084E-02 0.00188 ];
PU240_CAPT                (idx, [1:   4]) = [  7.21941E+14 0.00311  3.63318E-02 0.00297 ];
PU241_CAPT                (idx, [1:   4]) = [  6.16566E+13 0.01028  3.10431E-03 0.01032 ];
XE135_CAPT                (idx, [1:   4]) = [  7.25837E+14 0.00295  3.65358E-02 0.00295 ];
SM149_CAPT                (idx, [1:   4]) = [  1.83908E+14 0.00610  9.25584E-03 0.00605 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000928 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.84985E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000928 5.00785E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2930154 2.93423E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2030961 2.03381E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 39813 3.98179E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000928 5.00785E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -2.60770E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 6.3E-09  4.50056E+05 6.3E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.53409E+16 1.8E-05  3.53409E+16 1.8E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37722E+16 3.2E-06  1.37722E+16 3.2E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.98745E+16 0.00045  1.47994E+16 0.00046  5.07510E+15 0.00107 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.36467E+16 0.00027  2.85716E+16 0.00024  5.07510E+15 0.00107 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.38563E+16 0.00052  3.38563E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.58631E+18 0.00048  4.27375E+17 0.00046  1.15893E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.69646E+14 0.00522 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.39164E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.17259E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11699E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11699E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.62860E+00 0.00048 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.93548E-01 0.00029 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.52607E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24750E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94837E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97185E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.05209E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04371E+00 0.00056 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.56610E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03964E+02 3.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04376E+00 0.00058  1.03743E+00 0.00056  6.28228E-03 0.00878 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.04364E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04399E+00 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.04364E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.05202E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72841E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72776E+01 8.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.25145E-07 0.00352 ];
IMP_EALF                  (idx, [1:   2]) = [  6.27656E-07 0.00153 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.32917E-01 0.00293 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.34047E-01 0.00119 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.86582E-03 0.00619  1.67843E-04 0.03261  9.59213E-04 0.01451  9.29407E-04 0.01491  2.64974E-03 0.00893  8.83191E-04 0.01417  2.76425E-04 0.02646 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.90302E-01 0.01360  1.03995E-02 0.02012  3.12902E-02 0.00041  1.10175E-01 0.00036  3.21480E-01 0.00026  1.33575E+00 0.00073  8.40941E+00 0.01184 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.03534E-03 0.00863  1.75267E-04 0.05187  9.73325E-04 0.02246  9.34742E-04 0.02214  2.72452E-03 0.01264  9.38625E-04 0.02178  2.88856E-04 0.04235 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.96906E-01 0.02076  1.25003E-02 0.00039  3.12990E-02 0.00057  1.10222E-01 0.00051  3.21557E-01 0.00039  1.33370E+00 0.00120  8.95065E+00 0.00421 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.38758E-05 0.00125  2.38655E-05 0.00126  2.55929E-05 0.01371 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.49165E-05 0.00112  2.49058E-05 0.00113  2.67136E-05 0.01372 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.01600E-03 0.00892  1.70592E-04 0.05344  9.67331E-04 0.02385  9.66259E-04 0.02351  2.70085E-03 0.01361  9.34981E-04 0.02271  2.75986E-04 0.04602 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.81064E-01 0.02359  1.24965E-02 0.00041  3.12939E-02 0.00068  1.10149E-01 0.00061  3.21500E-01 0.00043  1.33325E+00 0.00137  8.89733E+00 0.00574 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.34552E-05 0.00297  2.34348E-05 0.00298  2.62873E-05 0.03777 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.44770E-05 0.00290  2.44558E-05 0.00291  2.74344E-05 0.03772 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.01049E-03 0.03045  1.43666E-04 0.19580  1.01793E-03 0.07067  8.74317E-04 0.07573  2.72178E-03 0.04259  9.95367E-04 0.07978  2.57430E-04 0.15327 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.43517E-01 0.07529  1.25150E-02 0.00201  3.13348E-02 0.00155  1.10051E-01 0.00131  3.20876E-01 0.00116  1.32265E+00 0.00451  8.98245E+00 0.01303 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.97127E-03 0.02974  1.44492E-04 0.19596  1.02262E-03 0.07048  8.77904E-04 0.07129  2.70460E-03 0.04177  9.64822E-04 0.07632  2.56835E-04 0.14567 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.43019E-01 0.07385  1.25150E-02 0.00201  3.13364E-02 0.00155  1.10046E-01 0.00130  3.20893E-01 0.00115  1.32253E+00 0.00452  8.99793E+00 0.01285 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.57037E+02 0.03049 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.36571E-05 0.00075 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.46882E-05 0.00050 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.90962E-03 0.00502 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.49837E+02 0.00502 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.61117E-07 0.00069 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.79156E-06 0.00044  2.79111E-06 0.00043  2.86125E-06 0.00539 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.71357E-05 0.00086  3.71575E-05 0.00086  3.36793E-05 0.01049 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.50657E-01 0.00035  6.50398E-01 0.00036  7.07065E-01 0.00925 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06267E+01 0.01394 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.45857E+01 0.00045  3.33565E+01 0.00048 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.57790E+04 0.00360  2.98500E+05 0.00132  6.09227E+05 0.00109  6.52091E+05 0.00074  5.99479E+05 0.00073  6.43286E+05 0.00066  4.36785E+05 0.00058  3.86606E+05 0.00058  2.95769E+05 0.00060  2.41910E+05 0.00093  2.08326E+05 0.00062  1.87925E+05 0.00097  1.73501E+05 0.00096  1.64803E+05 0.00092  1.60405E+05 0.00090  1.38601E+05 0.00090  1.37118E+05 0.00092  1.35854E+05 0.00097  1.33285E+05 0.00104  2.60220E+05 0.00063  2.51189E+05 0.00090  1.81176E+05 0.00072  1.17112E+05 0.00076  1.35304E+05 0.00068  1.28109E+05 0.00081  1.15216E+05 0.00095  1.88246E+05 0.00077  4.32116E+04 0.00153  5.42633E+04 0.00169  4.93207E+04 0.00135  2.86371E+04 0.00207  4.96617E+04 0.00159  3.35799E+04 0.00160  2.81396E+04 0.00189  5.15568E+03 0.00296  4.83882E+03 0.00353  4.52576E+03 0.00276  4.41607E+03 0.00367  4.47622E+03 0.00340  4.80469E+03 0.00375  5.26319E+03 0.00333  5.11824E+03 0.00409  9.80562E+03 0.00348  1.57894E+04 0.00212  2.02619E+04 0.00205  5.37039E+04 0.00120  5.62887E+04 0.00143  6.03710E+04 0.00136  3.94567E+04 0.00129  2.82186E+04 0.00147  2.11360E+04 0.00181  2.48155E+04 0.00130  4.80885E+04 0.00110  6.66954E+04 0.00121  1.30431E+05 0.00121  2.02617E+05 0.00137  3.03385E+05 0.00134  1.93948E+05 0.00126  1.38861E+05 0.00142  9.99425E+04 0.00135  8.98311E+04 0.00146  8.84201E+04 0.00141  7.38234E+04 0.00131  4.99525E+04 0.00147  4.61376E+04 0.00162  4.09960E+04 0.00130  3.46732E+04 0.00163  2.73562E+04 0.00175  1.83220E+04 0.00219  6.47082E+03 0.00220 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.05237E+00 0.00054 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.26636E+18 0.00050  3.19982E+17 0.00117 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37532E-01 9.1E-05  1.53730E+00 0.00036 ];
INF_CAPT                  (idx, [1:   4]) = [  7.14106E-03 0.00057  3.38534E-02 0.00061 ];
INF_ABS                   (idx, [1:   4]) = [  9.26503E-03 0.00046  6.84947E-02 0.00090 ];
INF_FISS                  (idx, [1:   4]) = [  2.12397E-03 0.00046  3.46413E-02 0.00120 ];
INF_NSF                   (idx, [1:   4]) = [  5.54600E-03 0.00044  8.85143E-02 0.00123 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.61115E+00 5.7E-05  2.55517E+00 3.7E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04459E+02 6.7E-06  2.03844E+02 6.3E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.73953E-08 0.00032  2.55477E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28277E-01 9.2E-05  1.46880E+00 0.00042 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43377E-01 0.00016  3.90046E-01 0.00050 ];
INF_SCATT2                (idx, [1:   4]) = [  9.60581E-02 0.00025  9.27909E-02 0.00130 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35734E-03 0.00371  2.78664E-02 0.00284 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02528E-02 0.00229 -8.56419E-03 0.00698 ];
INF_SCATT5                (idx, [1:   4]) = [  1.66005E-04 0.10190  6.51894E-03 0.00665 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08958E-03 0.00224 -1.65979E-02 0.00318 ];
INF_SCATT7                (idx, [1:   4]) = [  7.47078E-04 0.01459  3.61262E-04 0.14491 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28319E-01 9.3E-05  1.46880E+00 0.00042 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43378E-01 0.00016  3.90046E-01 0.00050 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.60581E-02 0.00025  9.27909E-02 0.00130 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35710E-03 0.00372  2.78664E-02 0.00284 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02528E-02 0.00229 -8.56419E-03 0.00698 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.66070E-04 0.10175  6.51894E-03 0.00665 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08933E-03 0.00225 -1.65979E-02 0.00318 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.47068E-04 0.01454  3.61262E-04 0.14491 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13002E-01 0.00031  9.94320E-01 0.00034 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56493E+00 0.00031  3.35238E-01 0.00034 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.22306E-03 0.00045  6.84947E-02 0.00090 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69444E-02 0.00021  6.97102E-02 0.00096 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10588E-01 9.0E-05  1.76888E-02 0.00044  1.20830E-03 0.00438  1.46759E+00 0.00042 ];
INF_S1                    (idx, [1:   8]) = [  2.38226E-01 0.00016  5.15102E-03 0.00092  5.20124E-04 0.00880  3.89526E-01 0.00051 ];
INF_S2                    (idx, [1:   8]) = [  9.76173E-02 0.00024 -1.55921E-03 0.00287  2.85313E-04 0.00901  9.25055E-02 0.00131 ];
INF_S3                    (idx, [1:   8]) = [  9.18072E-03 0.00297 -1.82338E-03 0.00229  1.02828E-04 0.01657  2.77635E-02 0.00283 ];
INF_S4                    (idx, [1:   8]) = [ -9.65918E-03 0.00247 -5.93597E-04 0.00357 -8.68837E-07 1.00000 -8.56332E-03 0.00705 ];
INF_S5                    (idx, [1:   8]) = [  1.43580E-04 0.11701  2.24246E-05 0.11009 -4.09561E-05 0.02922  6.55990E-03 0.00664 ];
INF_S6                    (idx, [1:   8]) = [  5.22860E-03 0.00242 -1.39022E-04 0.01953 -5.30793E-05 0.02425 -1.65449E-02 0.00319 ];
INF_S7                    (idx, [1:   8]) = [  9.15575E-04 0.01233 -1.68497E-04 0.01417 -4.79988E-05 0.03137  4.09261E-04 0.12880 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10630E-01 9.0E-05  1.76888E-02 0.00044  1.20830E-03 0.00438  1.46759E+00 0.00042 ];
INF_SP1                   (idx, [1:   8]) = [  2.38227E-01 0.00016  5.15102E-03 0.00092  5.20124E-04 0.00880  3.89526E-01 0.00051 ];
INF_SP2                   (idx, [1:   8]) = [  9.76173E-02 0.00024 -1.55921E-03 0.00287  2.85313E-04 0.00901  9.25055E-02 0.00131 ];
INF_SP3                   (idx, [1:   8]) = [  9.18048E-03 0.00297 -1.82338E-03 0.00229  1.02828E-04 0.01657  2.77635E-02 0.00283 ];
INF_SP4                   (idx, [1:   8]) = [ -9.65923E-03 0.00247 -5.93597E-04 0.00357 -8.68837E-07 1.00000 -8.56332E-03 0.00705 ];
INF_SP5                   (idx, [1:   8]) = [  1.43646E-04 0.11686  2.24246E-05 0.11009 -4.09561E-05 0.02922  6.55990E-03 0.00664 ];
INF_SP6                   (idx, [1:   8]) = [  5.22835E-03 0.00243 -1.39022E-04 0.01953 -5.30793E-05 0.02425 -1.65449E-02 0.00319 ];
INF_SP7                   (idx, [1:   8]) = [  9.15565E-04 0.01230 -1.68497E-04 0.01417 -4.79988E-05 0.03137  4.09261E-04 0.12880 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31733E-01 0.00063  1.14403E+00 0.00733 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33142E-01 0.00079  1.23059E+00 0.00947 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33309E-01 0.00078  1.22933E+00 0.00888 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28812E-01 0.00107  1.00479E+00 0.00685 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43845E+00 0.00063  2.91743E-01 0.00732 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42976E+00 0.00079  2.71450E-01 0.00938 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42874E+00 0.00078  2.71659E-01 0.00880 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45684E+00 0.00107  3.32122E-01 0.00692 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.03534E-03 0.00863  1.75267E-04 0.05187  9.73325E-04 0.02246  9.34742E-04 0.02214  2.72452E-03 0.01264  9.38625E-04 0.02178  2.88856E-04 0.04235 ];
LAMBDA                    (idx, [1:  14]) = [  7.96906E-01 0.02076  1.25003E-02 0.00039  3.12990E-02 0.00057  1.10222E-01 0.00051  3.21557E-01 0.00039  1.33370E+00 0.00120  8.95065E+00 0.00421 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0008' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:50:49 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:49:24 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299849 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.94198E-01  9.98120E-01  1.00251E+00  1.00139E+00  1.00378E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.6E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13711E-02 0.00108  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88629E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04017E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04449E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67036E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.45598E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.45518E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.39858E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.06214E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000772 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00015E+04 0.00083 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00015E+04 0.00083 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.90768E+02 ;
RUNNING_TIME              (idx, 1)        =  5.85804E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.62450E-01  4.62450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.70283E-01  9.26667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  5.74387E+01  3.02512E+00  2.41132E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.03950E-01  2.39500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.75667E-02  7.33336E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  5.85803E+01  1.13932E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96356 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99901E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76993E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.31036E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.78933E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.47330E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.68079E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.17305E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62954E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.67200E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.03360E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.34912E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.46959E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.06291E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.86638E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.64283E+07 ;
SR90_ACTIVITY             (idx, 1)        =  9.17020E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.26238E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.32822E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.38024E+14 ;
CS134_ACTIVITY            (idx, 1)        =  5.02259E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.07887E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.09689E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.44329E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.51530E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.22384E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.43194E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 10 ;
BURNUP                     (idx, [1:  2])  = [  8.00000E+00  8.00071E+00 ];
BURN_DAYS                 (idx, 1)        =  2.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.49196E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  9.22973E+15 0.00076  6.70983E-01 0.00048 ];
U238_FISS                 (idx, [1:   4]) = [  9.51099E+14 0.00276  6.91310E-02 0.00257 ];
PU239_FISS                (idx, [1:   4]) = [  3.34444E+15 0.00127  2.43139E-01 0.00116 ];
PU240_FISS                (idx, [1:   4]) = [  2.64335E+12 0.05184  1.92156E-04 0.05184 ];
PU241_FISS                (idx, [1:   4]) = [  2.19708E+14 0.00563  1.59719E-02 0.00559 ];
U235_CAPT                 (idx, [1:   4]) = [  2.05441E+15 0.00173  1.01000E-01 0.00164 ];
U238_CAPT                 (idx, [1:   4]) = [  8.33405E+15 0.00102  4.09691E-01 0.00066 ];
PU239_CAPT                (idx, [1:   4]) = [  1.86774E+15 0.00186  9.18288E-02 0.00185 ];
PU240_CAPT                (idx, [1:   4]) = [  8.38399E+14 0.00286  4.12162E-02 0.00278 ];
PU241_CAPT                (idx, [1:   4]) = [  7.86975E+13 0.00981  3.86911E-03 0.00978 ];
XE135_CAPT                (idx, [1:   4]) = [  7.30762E+14 0.00297  3.59268E-02 0.00294 ];
SM149_CAPT                (idx, [1:   4]) = [  1.89078E+14 0.00624  9.29638E-03 0.00624 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000772 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.81813E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000772 5.00782E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2959341 2.96351E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2001290 2.00416E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 40141 4.01458E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000772 5.00782E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -7.45058E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 6.3E-09  4.50056E+05 6.3E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.54601E+16 1.9E-05  3.54601E+16 1.9E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37625E+16 3.4E-06  1.37625E+16 3.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.03415E+16 0.00046  1.52035E+16 0.00045  5.13800E+15 0.00112 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.41040E+16 0.00028  2.89660E+16 0.00024  5.13800E+15 0.00112 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.43194E+16 0.00050  3.43194E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.60566E+18 0.00047  4.31848E+17 0.00045  1.17381E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.75593E+14 0.00519 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.43796E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.18747E+18 0.00061 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11583E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11583E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.61847E+00 0.00047 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.93254E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.50330E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24693E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94757E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97199E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.04104E+00 0.00056 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.03268E+00 0.00057 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.57658E+00 2.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04108E+02 3.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03291E+00 0.00058  1.02669E+00 0.00057  5.99214E-03 0.00949 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.03307E+00 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03337E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.03307E+00 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  1.04143E+00 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72620E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72596E+01 8.3E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.39097E-07 0.00350 ];
IMP_EALF                  (idx, [1:   2]) = [  6.38980E-07 0.00143 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.36339E-01 0.00289 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.36968E-01 0.00113 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.78277E-03 0.00594  1.61165E-04 0.03759  9.97198E-04 0.01388  9.01841E-04 0.01399  2.59176E-03 0.00915  8.56166E-04 0.01465  2.74639E-04 0.02710 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.90070E-01 0.01420  9.92919E-03 0.02281  3.12754E-02 0.00038  1.10328E-01 0.00043  3.21735E-01 0.00026  1.33351E+00 0.00077  8.35054E+00 0.01244 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.82168E-03 0.00902  1.62885E-04 0.05579  1.01776E-03 0.02185  9.20059E-04 0.02095  2.59507E-03 0.01370  8.49306E-04 0.02210  2.76598E-04 0.04301 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.87155E-01 0.02277  1.25079E-02 0.00054  3.12673E-02 0.00054  1.10375E-01 0.00060  3.21749E-01 0.00039  1.33252E+00 0.00122  8.92207E+00 0.00468 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.40221E-05 0.00137  2.40127E-05 0.00137  2.58530E-05 0.01384 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.48075E-05 0.00117  2.47978E-05 0.00117  2.66945E-05 0.01381 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.80612E-03 0.00958  1.77331E-04 0.05791  1.01171E-03 0.02227  8.96409E-04 0.02376  2.58360E-03 0.01525  8.70421E-04 0.02437  2.66660E-04 0.04674 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.75859E-01 0.02468  1.25069E-02 0.00066  3.13134E-02 0.00063  1.10322E-01 0.00070  3.21666E-01 0.00044  1.33369E+00 0.00124  8.91463E+00 0.00634 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.34910E-05 0.00298  2.34850E-05 0.00299  2.47265E-05 0.03667 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.42599E-05 0.00291  2.42538E-05 0.00293  2.55298E-05 0.03667 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.79527E-03 0.03126  1.63377E-04 0.16599  9.61706E-04 0.07565  1.01069E-03 0.07814  2.40605E-03 0.04667  9.38087E-04 0.07864  3.15367E-04 0.14944 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.57566E-01 0.07449  1.25092E-02 0.00150  3.13849E-02 0.00144  1.10521E-01 0.00163  3.21785E-01 0.00130  1.33610E+00 0.00276  9.00268E+00 0.01617 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.76606E-03 0.03075  1.70447E-04 0.16148  9.66567E-04 0.07401  9.99746E-04 0.07636  2.40751E-03 0.04565  9.13837E-04 0.07744  3.07954E-04 0.14730 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.51863E-01 0.07342  1.25092E-02 0.00150  3.13869E-02 0.00143  1.10507E-01 0.00162  3.21749E-01 0.00128  1.33612E+00 0.00272  9.00673E+00 0.01618 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.48049E+02 0.03188 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.37521E-05 0.00078 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.45299E-05 0.00055 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.83797E-03 0.00591 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.45917E+02 0.00604 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.60053E-07 0.00071 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.78229E-06 0.00041  2.78199E-06 0.00041  2.82765E-06 0.00556 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.71409E-05 0.00088  3.71591E-05 0.00088  3.42088E-05 0.01068 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.48326E-01 0.00032  6.48115E-01 0.00032  6.96525E-01 0.00933 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07915E+01 0.01408 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.45518E+01 0.00044  3.33006E+01 0.00046 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.60695E+04 0.00382  2.97897E+05 0.00126  6.07762E+05 0.00088  6.51289E+05 0.00074  5.98890E+05 0.00051  6.43414E+05 0.00056  4.36515E+05 0.00071  3.86525E+05 0.00076  2.95833E+05 0.00078  2.41492E+05 0.00060  2.08186E+05 0.00077  1.87819E+05 0.00091  1.73269E+05 0.00069  1.64791E+05 0.00080  1.60750E+05 0.00075  1.38780E+05 0.00090  1.37010E+05 0.00098  1.35608E+05 0.00089  1.33431E+05 0.00109  2.60554E+05 0.00073  2.51350E+05 0.00064  1.80920E+05 0.00099  1.17346E+05 0.00113  1.35289E+05 0.00116  1.28221E+05 0.00072  1.15196E+05 0.00090  1.87660E+05 0.00069  4.33063E+04 0.00098  5.41905E+04 0.00150  4.91926E+04 0.00165  2.86108E+04 0.00202  4.94993E+04 0.00122  3.35557E+04 0.00154  2.80946E+04 0.00203  5.13110E+03 0.00313  4.73149E+03 0.00311  4.40079E+03 0.00387  4.28376E+03 0.00259  4.30862E+03 0.00392  4.65391E+03 0.00410  5.19519E+03 0.00291  5.03676E+03 0.00388  9.71137E+03 0.00314  1.57386E+04 0.00234  2.01618E+04 0.00232  5.35260E+04 0.00122  5.60048E+04 0.00163  5.98876E+04 0.00161  3.92469E+04 0.00133  2.80304E+04 0.00153  2.09124E+04 0.00151  2.45375E+04 0.00239  4.75978E+04 0.00117  6.60984E+04 0.00145  1.29510E+05 0.00121  2.01561E+05 0.00130  3.02125E+05 0.00133  1.93344E+05 0.00155  1.38516E+05 0.00163  9.96725E+04 0.00160  8.96226E+04 0.00137  8.82686E+04 0.00141  7.37168E+04 0.00138  4.98601E+04 0.00189  4.60143E+04 0.00142  4.09427E+04 0.00170  3.46568E+04 0.00181  2.73066E+04 0.00139  1.82404E+04 0.00214  6.47967E+03 0.00226 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.04173E+00 0.00059 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.28272E+18 0.00059  3.22970E+17 0.00118 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37839E-01 0.00011  1.54067E+00 0.00033 ];
INF_CAPT                  (idx, [1:   4]) = [  7.23350E-03 0.00061  3.42569E-02 0.00061 ];
INF_ABS                   (idx, [1:   4]) = [  9.32530E-03 0.00050  6.85673E-02 0.00088 ];
INF_FISS                  (idx, [1:   4]) = [  2.09180E-03 0.00044  3.43104E-02 0.00116 ];
INF_NSF                   (idx, [1:   4]) = [  5.47594E-03 0.00043  8.80609E-02 0.00118 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.61781E+00 6.7E-05  2.56659E+00 4.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04547E+02 7.4E-06  2.04001E+02 7.0E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.72640E-08 0.00034  2.55705E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28517E-01 0.00011  1.47212E+00 0.00039 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43496E-01 0.00019  3.91047E-01 0.00054 ];
INF_SCATT2                (idx, [1:   4]) = [  9.60587E-02 0.00026  9.30042E-02 0.00075 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35235E-03 0.00283  2.79481E-02 0.00248 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02496E-02 0.00144 -8.67392E-03 0.00778 ];
INF_SCATT5                (idx, [1:   4]) = [  1.59458E-04 0.10728  6.46759E-03 0.00816 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08739E-03 0.00250 -1.66884E-02 0.00267 ];
INF_SCATT7                (idx, [1:   4]) = [  7.46115E-04 0.01778  3.95220E-04 0.12549 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28558E-01 0.00011  1.47212E+00 0.00039 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43496E-01 0.00019  3.91047E-01 0.00054 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.60590E-02 0.00026  9.30042E-02 0.00075 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35231E-03 0.00283  2.79481E-02 0.00248 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02494E-02 0.00144 -8.67392E-03 0.00778 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.59331E-04 0.10748  6.46759E-03 0.00816 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08742E-03 0.00250 -1.66884E-02 0.00267 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.46113E-04 0.01778  3.95220E-04 0.12549 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13186E-01 0.00034  9.96584E-01 0.00025 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56358E+00 0.00034  3.34477E-01 0.00025 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.28347E-03 0.00051  6.85673E-02 0.00088 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69618E-02 0.00023  6.97636E-02 0.00105 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10877E-01 0.00011  1.76396E-02 0.00045  1.21161E-03 0.00430  1.47091E+00 0.00039 ];
INF_S1                    (idx, [1:   8]) = [  2.38369E-01 0.00018  5.12685E-03 0.00103  5.17944E-04 0.00703  3.90529E-01 0.00054 ];
INF_S2                    (idx, [1:   8]) = [  9.76214E-02 0.00026 -1.56268E-03 0.00297  2.81518E-04 0.00923  9.27227E-02 0.00075 ];
INF_S3                    (idx, [1:   8]) = [  9.17535E-03 0.00231 -1.82300E-03 0.00180  1.00290E-04 0.02468  2.78478E-02 0.00250 ];
INF_S4                    (idx, [1:   8]) = [ -9.65955E-03 0.00146 -5.90028E-04 0.00448  1.74319E-06 1.00000 -8.67566E-03 0.00776 ];
INF_S5                    (idx, [1:   8]) = [  1.31570E-04 0.12372  2.78880E-05 0.09917 -3.77727E-05 0.04585  6.50536E-03 0.00821 ];
INF_S6                    (idx, [1:   8]) = [  5.22810E-03 0.00241 -1.40710E-04 0.01468 -5.22722E-05 0.02939 -1.66361E-02 0.00270 ];
INF_S7                    (idx, [1:   8]) = [  9.12839E-04 0.01498 -1.66724E-04 0.01448 -4.57792E-05 0.03387  4.41000E-04 0.11172 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10919E-01 0.00011  1.76396E-02 0.00045  1.21161E-03 0.00430  1.47091E+00 0.00039 ];
INF_SP1                   (idx, [1:   8]) = [  2.38370E-01 0.00018  5.12685E-03 0.00103  5.17944E-04 0.00703  3.90529E-01 0.00054 ];
INF_SP2                   (idx, [1:   8]) = [  9.76217E-02 0.00026 -1.56268E-03 0.00297  2.81518E-04 0.00923  9.27227E-02 0.00075 ];
INF_SP3                   (idx, [1:   8]) = [  9.17531E-03 0.00231 -1.82300E-03 0.00180  1.00290E-04 0.02468  2.78478E-02 0.00250 ];
INF_SP4                   (idx, [1:   8]) = [ -9.65937E-03 0.00145 -5.90028E-04 0.00448  1.74319E-06 1.00000 -8.67566E-03 0.00776 ];
INF_SP5                   (idx, [1:   8]) = [  1.31443E-04 0.12405  2.78880E-05 0.09917 -3.77727E-05 0.04585  6.50536E-03 0.00821 ];
INF_SP6                   (idx, [1:   8]) = [  5.22813E-03 0.00241 -1.40710E-04 0.01468 -5.22722E-05 0.02939 -1.66361E-02 0.00270 ];
INF_SP7                   (idx, [1:   8]) = [  9.12837E-04 0.01497 -1.66724E-04 0.01448 -4.57792E-05 0.03387  4.41000E-04 0.11172 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31762E-01 0.00058  1.14184E+00 0.00771 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33179E-01 0.00094  1.23178E+00 0.00970 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33372E-01 0.00096  1.22349E+00 0.00959 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28802E-01 0.00091  1.00300E+00 0.00740 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43827E+00 0.00058  2.92333E-01 0.00753 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42955E+00 0.00095  2.71201E-01 0.00937 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42837E+00 0.00096  2.73033E-01 0.00935 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45689E+00 0.00091  3.32767E-01 0.00730 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.82168E-03 0.00902  1.62885E-04 0.05579  1.01776E-03 0.02185  9.20059E-04 0.02095  2.59507E-03 0.01370  8.49306E-04 0.02210  2.76598E-04 0.04301 ];
LAMBDA                    (idx, [1:  14]) = [  7.87155E-01 0.02277  1.25079E-02 0.00054  3.12673E-02 0.00054  1.10375E-01 0.00060  3.21749E-01 0.00039  1.33252E+00 0.00122  8.92207E+00 0.00468 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0008' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:50:49 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:54:52 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299849 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.94363E-01  9.98003E-01  1.00254E+00  1.00080E+00  1.00430E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13833E-02 0.00113  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88617E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04256E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04690E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.66971E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.45115E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.45033E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.37391E+00 0.00035  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.05792E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000763 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00015E+04 0.00081 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00015E+04 0.00081 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.18076E+02 ;
RUNNING_TIME              (idx, 1)        =  6.40481E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.62450E-01  4.62450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.88833E-01  8.98333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.28374E+01  3.04247E+00  2.35627E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.53950E-01  2.45500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.92833E-02  8.49998E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  6.40477E+01  1.13084E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96620 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99887E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.77630E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.33667E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.78279E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.45006E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.69982E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.18636E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.63683E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.66413E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.13758E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.39591E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.27215E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.16188E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.10368E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.67972E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.01962E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.27931E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.35087E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.40302E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.34840E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.21337E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.11918E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.42743E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  4.91661E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.23057E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.47758E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 11 ;
BURNUP                     (idx, [1:  2])  = [  9.00000E+00  9.00081E+00 ];
BURN_DAYS                 (idx, 1)        =  2.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.58584E-01 0.00113 ];
U235_FISS                 (idx, [1:   4]) = [  8.89418E+15 0.00078  6.47080E-01 0.00053 ];
U238_FISS                 (idx, [1:   4]) = [  9.65217E+14 0.00271  7.02147E-02 0.00256 ];
PU239_FISS                (idx, [1:   4]) = [  3.59844E+15 0.00125  2.61806E-01 0.00116 ];
PU240_FISS                (idx, [1:   4]) = [  3.38735E+12 0.04598  2.46372E-04 0.04590 ];
PU241_FISS                (idx, [1:   4]) = [  2.75402E+14 0.00496  2.00349E-02 0.00491 ];
U235_CAPT                 (idx, [1:   4]) = [  1.98466E+15 0.00184  9.54253E-02 0.00180 ];
U238_CAPT                 (idx, [1:   4]) = [  8.41400E+15 0.00106  4.04513E-01 0.00073 ];
PU239_CAPT                (idx, [1:   4]) = [  1.99941E+15 0.00179  9.61320E-02 0.00172 ];
PU240_CAPT                (idx, [1:   4]) = [  9.48190E+14 0.00289  4.55851E-02 0.00278 ];
PU241_CAPT                (idx, [1:   4]) = [  9.97953E+13 0.00866  4.79810E-03 0.00864 ];
XE135_CAPT                (idx, [1:   4]) = [  7.31750E+14 0.00310  3.51881E-02 0.00316 ];
SM149_CAPT                (idx, [1:   4]) = [  1.91556E+14 0.00580  9.20950E-03 0.00576 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000763 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.68611E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000763 5.00769E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2986256 2.99047E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1973651 1.97636E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 40856 4.08634E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000763 5.00769E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.56348E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 6.3E-09  4.50056E+05 6.3E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.55746E+16 1.9E-05  3.55746E+16 1.9E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37530E+16 3.5E-06  1.37530E+16 3.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.08022E+16 0.00046  1.56100E+16 0.00048  5.19221E+15 0.00113 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.45552E+16 0.00027  2.93630E+16 0.00025  5.19221E+15 0.00113 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.47758E+16 0.00050  3.47758E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.62527E+18 0.00048  4.36811E+17 0.00047  1.18846E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.84239E+14 0.00530 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.48395E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.20155E+18 0.00061 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11466E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11466E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.60995E+00 0.00046 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.92760E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.48283E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24608E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94681E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97132E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.03095E+00 0.00056 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.02253E+00 0.00056 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.58667E+00 2.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04248E+02 3.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.02251E+00 0.00058  1.01661E+00 0.00056  5.91958E-03 0.00999 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.02271E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.02310E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.02271E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.03114E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72435E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72412E+01 9.1E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.51259E-07 0.00371 ];
IMP_EALF                  (idx, [1:   2]) = [  6.50906E-07 0.00156 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.39830E-01 0.00285 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.39932E-01 0.00114 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.82325E-03 0.00627  1.55734E-04 0.03599  9.86190E-04 0.01395  9.27822E-04 0.01520  2.61262E-03 0.00932  8.74052E-04 0.01556  2.66837E-04 0.02858 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.75455E-01 0.01464  9.84948E-03 0.02322  3.12512E-02 0.00039  1.10206E-01 0.00040  3.21624E-01 0.00027  1.32729E+00 0.00221  8.22035E+00 0.01350 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.82323E-03 0.00934  1.50895E-04 0.05493  9.99169E-04 0.02112  9.57056E-04 0.02326  2.57832E-03 0.01329  8.85225E-04 0.02307  2.52574E-04 0.04195 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.60930E-01 0.02198  1.24976E-02 0.00025  3.12604E-02 0.00056  1.10128E-01 0.00055  3.21674E-01 0.00042  1.33071E+00 0.00120  8.84222E+00 0.00595 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.41363E-05 0.00130  2.41278E-05 0.00131  2.56514E-05 0.01411 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.46754E-05 0.00116  2.46667E-05 0.00117  2.62280E-05 0.01412 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.79052E-03 0.01010  1.47908E-04 0.05755  1.00239E-03 0.02271  9.20571E-04 0.02457  2.56383E-03 0.01511  9.03825E-04 0.02492  2.51993E-04 0.04392 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.59918E-01 0.02232  1.25006E-02 0.00048  3.12254E-02 0.00070  1.10098E-01 0.00068  3.21684E-01 0.00048  1.32980E+00 0.00164  8.87950E+00 0.00662 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.36436E-05 0.00298  2.36324E-05 0.00299  2.41781E-05 0.03595 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.41709E-05 0.00291  2.41595E-05 0.00291  2.47118E-05 0.03592 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.72637E-03 0.03046  1.41631E-04 0.20183  1.06000E-03 0.07211  9.22136E-04 0.07757  2.43550E-03 0.04861  8.12632E-04 0.08288  3.54465E-04 0.13403 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.72218E-01 0.07551  1.25116E-02 0.00177  3.12523E-02 0.00158  1.09942E-01 0.00153  3.21846E-01 0.00132  1.33161E+00 0.00327  8.90028E+00 0.01534 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.69549E-03 0.03018  1.33169E-04 0.19316  1.06715E-03 0.07057  9.01597E-04 0.07707  2.42817E-03 0.04834  8.15746E-04 0.08135  3.49669E-04 0.13386 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.82056E-01 0.07463  1.25116E-02 0.00177  3.12563E-02 0.00157  1.09940E-01 0.00152  3.21753E-01 0.00130  1.33221E+00 0.00316  8.89945E+00 0.01545 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.43482E+02 0.03069 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.38836E-05 0.00081 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.44168E-05 0.00052 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.65344E-03 0.00652 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.36740E+02 0.00652 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.58441E-07 0.00072 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.77017E-06 0.00042  2.76998E-06 0.00042  2.80562E-06 0.00554 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.71098E-05 0.00089  3.71263E-05 0.00089  3.43740E-05 0.01027 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.46315E-01 0.00032  6.46145E-01 0.00032  6.88863E-01 0.00964 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06466E+01 0.01377 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.45033E+01 0.00044  3.32268E+01 0.00049 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.59811E+04 0.00355  2.99072E+05 0.00154  6.09122E+05 0.00100  6.51237E+05 0.00074  5.99182E+05 0.00061  6.43109E+05 0.00069  4.36434E+05 0.00063  3.86614E+05 0.00051  2.95509E+05 0.00090  2.41747E+05 0.00098  2.08146E+05 0.00068  1.87595E+05 0.00060  1.73432E+05 0.00088  1.64941E+05 0.00082  1.60341E+05 0.00079  1.38805E+05 0.00071  1.36847E+05 0.00100  1.35667E+05 0.00113  1.33342E+05 0.00084  2.60319E+05 0.00084  2.50815E+05 0.00070  1.80963E+05 0.00062  1.17146E+05 0.00084  1.35292E+05 0.00089  1.28344E+05 0.00073  1.15175E+05 0.00099  1.87630E+05 0.00103  4.31719E+04 0.00185  5.41687E+04 0.00140  4.92000E+04 0.00179  2.86636E+04 0.00162  4.96159E+04 0.00148  3.35209E+04 0.00180  2.79237E+04 0.00217  5.06644E+03 0.00330  4.60391E+03 0.00309  4.26429E+03 0.00397  4.09230E+03 0.00368  4.19236E+03 0.00445  4.53475E+03 0.00489  5.07568E+03 0.00267  5.01589E+03 0.00365  9.68402E+03 0.00331  1.56483E+04 0.00232  2.00706E+04 0.00228  5.33324E+04 0.00114  5.59053E+04 0.00127  5.95201E+04 0.00117  3.88363E+04 0.00129  2.76909E+04 0.00154  2.05992E+04 0.00161  2.41910E+04 0.00150  4.69412E+04 0.00141  6.53542E+04 0.00118  1.28596E+05 0.00122  2.00431E+05 0.00131  3.00866E+05 0.00149  1.92549E+05 0.00145  1.38001E+05 0.00153  9.92747E+04 0.00156  8.92642E+04 0.00159  8.80418E+04 0.00170  7.34851E+04 0.00187  4.97207E+04 0.00172  4.59540E+04 0.00180  4.08673E+04 0.00191  3.45454E+04 0.00170  2.73307E+04 0.00168  1.82548E+04 0.00206  6.45733E+03 0.00234 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.03153E+00 0.00052 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.29968E+18 0.00053  3.25619E+17 0.00122 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37828E-01 0.00013  1.54338E+00 0.00041 ];
INF_CAPT                  (idx, [1:   4]) = [  7.32278E-03 0.00092  3.46597E-02 0.00066 ];
INF_ABS                   (idx, [1:   4]) = [  9.38505E-03 0.00074  6.86712E-02 0.00093 ];
INF_FISS                  (idx, [1:   4]) = [  2.06227E-03 0.00040  3.40114E-02 0.00123 ];
INF_NSF                   (idx, [1:   4]) = [  5.41195E-03 0.00038  8.76669E-02 0.00126 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.62427E+00 5.4E-05  2.57757E+00 4.7E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04635E+02 5.8E-06  2.04154E+02 8.1E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.71205E-08 0.00036  2.55954E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28453E-01 0.00014  1.47471E+00 0.00047 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43491E-01 0.00024  3.92075E-01 0.00054 ];
INF_SCATT2                (idx, [1:   4]) = [  9.60598E-02 0.00030  9.31749E-02 0.00106 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34374E-03 0.00254  2.80139E-02 0.00263 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02758E-02 0.00186 -8.70671E-03 0.00643 ];
INF_SCATT5                (idx, [1:   4]) = [  2.04660E-04 0.09460  6.48740E-03 0.00830 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10659E-03 0.00357 -1.67037E-02 0.00261 ];
INF_SCATT7                (idx, [1:   4]) = [  7.54296E-04 0.02420  3.22737E-04 0.13605 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28494E-01 0.00014  1.47471E+00 0.00047 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43492E-01 0.00024  3.92075E-01 0.00054 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.60599E-02 0.00030  9.31749E-02 0.00106 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34380E-03 0.00253  2.80139E-02 0.00263 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02757E-02 0.00186 -8.70671E-03 0.00643 ];
INF_SCATTP5               (idx, [1:   4]) = [  2.04738E-04 0.09456  6.48740E-03 0.00830 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10655E-03 0.00357 -1.67037E-02 0.00261 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.54163E-04 0.02411  3.22737E-04 0.13605 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13033E-01 0.00034  9.98562E-01 0.00038 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56471E+00 0.00034  3.33814E-01 0.00038 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.34392E-03 0.00074  6.86712E-02 0.00093 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69636E-02 0.00020  6.98999E-02 0.00101 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10865E-01 0.00013  1.75884E-02 0.00041  1.22797E-03 0.00409  1.47348E+00 0.00047 ];
INF_S1                    (idx, [1:   8]) = [  2.38380E-01 0.00024  5.11106E-03 0.00102  5.27333E-04 0.00607  3.91548E-01 0.00055 ];
INF_S2                    (idx, [1:   8]) = [  9.76209E-02 0.00030 -1.56108E-03 0.00242  2.89059E-04 0.00681  9.28859E-02 0.00106 ];
INF_S3                    (idx, [1:   8]) = [  9.16078E-03 0.00203 -1.81703E-03 0.00193  1.04166E-04 0.01402  2.79098E-02 0.00264 ];
INF_S4                    (idx, [1:   8]) = [ -9.68801E-03 0.00198 -5.87814E-04 0.00487  5.77942E-07 1.00000 -8.70729E-03 0.00643 ];
INF_S5                    (idx, [1:   8]) = [  1.75920E-04 0.11606  2.87401E-05 0.08479 -4.07527E-05 0.03868  6.52815E-03 0.00821 ];
INF_S6                    (idx, [1:   8]) = [  5.24479E-03 0.00363 -1.38207E-04 0.01408 -5.23121E-05 0.02254 -1.66514E-02 0.00261 ];
INF_S7                    (idx, [1:   8]) = [  9.23116E-04 0.02037 -1.68820E-04 0.01348 -4.73127E-05 0.02834  3.70050E-04 0.11880 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10906E-01 0.00013  1.75884E-02 0.00041  1.22797E-03 0.00409  1.47348E+00 0.00047 ];
INF_SP1                   (idx, [1:   8]) = [  2.38381E-01 0.00024  5.11106E-03 0.00102  5.27333E-04 0.00607  3.91548E-01 0.00055 ];
INF_SP2                   (idx, [1:   8]) = [  9.76209E-02 0.00030 -1.56108E-03 0.00242  2.89059E-04 0.00681  9.28859E-02 0.00106 ];
INF_SP3                   (idx, [1:   8]) = [  9.16083E-03 0.00202 -1.81703E-03 0.00193  1.04166E-04 0.01402  2.79098E-02 0.00264 ];
INF_SP4                   (idx, [1:   8]) = [ -9.68790E-03 0.00198 -5.87814E-04 0.00487  5.77942E-07 1.00000 -8.70729E-03 0.00643 ];
INF_SP5                   (idx, [1:   8]) = [  1.75997E-04 0.11601  2.87401E-05 0.08479 -4.07527E-05 0.03868  6.52815E-03 0.00821 ];
INF_SP6                   (idx, [1:   8]) = [  5.24476E-03 0.00363 -1.38207E-04 0.01408 -5.23121E-05 0.02254 -1.66514E-02 0.00261 ];
INF_SP7                   (idx, [1:   8]) = [  9.22983E-04 0.02029 -1.68820E-04 0.01348 -4.73127E-05 0.02834  3.70050E-04 0.11880 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31909E-01 0.00063  1.14900E+00 0.00637 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33702E-01 0.00087  1.24916E+00 0.00792 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33144E-01 0.00092  1.23236E+00 0.00737 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28948E-01 0.00104  1.00211E+00 0.00748 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43736E+00 0.00063  2.90389E-01 0.00633 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42634E+00 0.00087  2.67243E-01 0.00781 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42976E+00 0.00092  2.70842E-01 0.00748 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45597E+00 0.00104  3.33081E-01 0.00753 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.82323E-03 0.00934  1.50895E-04 0.05493  9.99169E-04 0.02112  9.57056E-04 0.02326  2.57832E-03 0.01329  8.85225E-04 0.02307  2.52574E-04 0.04195 ];
LAMBDA                    (idx, [1:  14]) = [  7.60930E-01 0.02198  1.24976E-02 0.00025  3.12604E-02 0.00056  1.10128E-01 0.00055  3.21674E-01 0.00042  1.33071E+00 0.00120  8.84222E+00 0.00595 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0008' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:50:49 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:00:22 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299849 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.95388E-01  9.97652E-01  1.00255E+00  1.00149E+00  1.00291E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 9.3E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13359E-02 0.00115  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88664E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05016E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05447E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.66867E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.45730E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.45648E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.34847E+00 0.00035  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.04411E-01 0.00119  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001040 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00021E+04 0.00080 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00021E+04 0.00080 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.45504E+02 ;
RUNNING_TIME              (idx, 1)        =  6.95399E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.62450E-01  4.62450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.08667E-01  9.86667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.82581E+01  3.05372E+00  2.36695E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  6.04650E-01  2.49833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.09167E-02  8.49998E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  6.95397E+01  1.13373E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96843 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00053E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78179E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.36163E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.77691E+04 ;
TOT_SF_RATE               (idx, 1)        =  4.78962E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.71917E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.19994E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.64243E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.65688E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.24647E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.43884E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  6.14437E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.26322E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.32030E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.71252E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.12012E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.29504E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.37176E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.42416E+14 ;
CS134_ACTIVITY            (idx, 1)        =  7.80735E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.34775E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.14094E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.41285E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  6.72049E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.23711E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.52694E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 12 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E+01  1.00009E+01 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.67756E-01 0.00103 ];
U235_FISS                 (idx, [1:   4]) = [  8.59940E+15 0.00080  6.25864E-01 0.00056 ];
U238_FISS                 (idx, [1:   4]) = [  9.77627E+14 0.00267  7.11425E-02 0.00251 ];
PU239_FISS                (idx, [1:   4]) = [  3.81794E+15 0.00132  2.77865E-01 0.00117 ];
PU240_FISS                (idx, [1:   4]) = [  3.75661E+12 0.04353  2.73390E-04 0.04350 ];
PU241_FISS                (idx, [1:   4]) = [  3.32542E+14 0.00454  2.42034E-02 0.00453 ];
U235_CAPT                 (idx, [1:   4]) = [  1.92323E+15 0.00201  9.03158E-02 0.00189 ];
U238_CAPT                 (idx, [1:   4]) = [  8.49756E+15 0.00102  3.99043E-01 0.00069 ];
PU239_CAPT                (idx, [1:   4]) = [  2.13066E+15 0.00179  1.00066E-01 0.00176 ];
PU240_CAPT                (idx, [1:   4]) = [  1.05793E+15 0.00263  4.96806E-02 0.00253 ];
PU241_CAPT                (idx, [1:   4]) = [  1.18798E+14 0.00789  5.57961E-03 0.00788 ];
XE135_CAPT                (idx, [1:   4]) = [  7.34939E+14 0.00311  3.45178E-02 0.00312 ];
SM149_CAPT                (idx, [1:   4]) = [  1.94755E+14 0.00592  9.14794E-03 0.00596 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001040 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.97218E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001040 5.00797E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3014460 3.01874E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1945318 1.94797E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 41262 4.12658E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001040 5.00797E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.63216E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 6.3E-09  4.50056E+05 6.3E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.56839E+16 1.9E-05  3.56839E+16 1.9E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37440E+16 3.5E-06  1.37440E+16 3.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.13059E+16 0.00044  1.60122E+16 0.00046  5.29369E+15 0.00111 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.50499E+16 0.00026  2.97562E+16 0.00025  5.29369E+15 0.00111 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.52694E+16 0.00050  3.52694E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.64826E+18 0.00046  4.42019E+17 0.00046  1.20624E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.91097E+14 0.00509 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.53410E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.22088E+18 0.00059 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11350E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11350E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.59983E+00 0.00048 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.91800E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.46346E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24584E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94619E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97112E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.01996E+00 0.00054 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.01154E+00 0.00054 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.59632E+00 2.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04382E+02 3.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.01167E+00 0.00055  1.00578E+00 0.00054  5.76360E-03 0.00953 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.01132E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.01188E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.01132E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.01973E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72202E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72193E+01 9.0E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.66394E-07 0.00354 ];
IMP_EALF                  (idx, [1:   2]) = [  6.65362E-07 0.00155 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.43440E-01 0.00268 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.43669E-01 0.00117 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.71634E-03 0.00633  1.62869E-04 0.03513  9.86231E-04 0.01435  9.09098E-04 0.01485  2.51451E-03 0.00901  8.74983E-04 0.01637  2.68645E-04 0.02652 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.83619E-01 0.01396  1.00573E-02 0.02211  3.11915E-02 0.00040  1.10335E-01 0.00039  3.21592E-01 0.00029  1.33022E+00 0.00093  8.18999E+00 0.01370 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.67421E-03 0.00924  1.60558E-04 0.05268  1.00145E-03 0.02336  8.77932E-04 0.02174  2.49867E-03 0.01350  8.74980E-04 0.02269  2.60620E-04 0.04062 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.82100E-01 0.02068  1.25061E-02 0.00045  3.11711E-02 0.00059  1.10382E-01 0.00059  3.21682E-01 0.00042  1.32849E+00 0.00148  8.83075E+00 0.00606 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.43729E-05 0.00127  2.43615E-05 0.00128  2.65080E-05 0.01445 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.46532E-05 0.00112  2.46416E-05 0.00112  2.68149E-05 0.01446 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.70406E-03 0.00968  1.71248E-04 0.05677  9.83640E-04 0.02322  9.26638E-04 0.02340  2.47013E-03 0.01464  8.89497E-04 0.02583  2.62910E-04 0.04432 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.74602E-01 0.02330  1.25041E-02 0.00058  3.12271E-02 0.00071  1.10419E-01 0.00071  3.21718E-01 0.00050  1.32920E+00 0.00175  8.78912E+00 0.00783 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.38190E-05 0.00313  2.38146E-05 0.00315  2.41839E-05 0.03398 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.40904E-05 0.00300  2.40859E-05 0.00302  2.44720E-05 0.03409 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.58829E-03 0.03173  1.25971E-04 0.22617  9.62359E-04 0.07497  9.62152E-04 0.07932  2.52202E-03 0.04566  8.09988E-04 0.07929  2.05801E-04 0.17612 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  6.83761E-01 0.07319  1.24883E-02 5.3E-05  3.12265E-02 0.00164  1.10491E-01 0.00177  3.21575E-01 0.00128  1.33554E+00 0.00315  9.09659E+00 0.01361 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.58027E-03 0.03095  1.26798E-04 0.21437  9.65641E-04 0.07419  9.59040E-04 0.07610  2.49791E-03 0.04542  8.20089E-04 0.07744  2.10794E-04 0.16422 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  6.90765E-01 0.07172  1.24883E-02 5.3E-05  3.12336E-02 0.00162  1.10491E-01 0.00177  3.21588E-01 0.00126  1.33550E+00 0.00315  9.09621E+00 0.01361 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.36056E+02 0.03206 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.40946E-05 0.00081 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.43718E-05 0.00056 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.67900E-03 0.00637 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.35776E+02 0.00641 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.59748E-07 0.00073 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.75596E-06 0.00045  2.75579E-06 0.00045  2.78134E-06 0.00559 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.73281E-05 0.00088  3.73480E-05 0.00089  3.39449E-05 0.01061 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.44333E-01 0.00035  6.44198E-01 0.00036  6.83195E-01 0.01083 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07775E+01 0.01430 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.45648E+01 0.00046  3.31985E+01 0.00048 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.67051E+04 0.00317  2.99178E+05 0.00164  6.08471E+05 0.00063  6.51466E+05 0.00069  5.99205E+05 0.00071  6.42292E+05 0.00065  4.36319E+05 0.00066  3.86699E+05 0.00059  2.95789E+05 0.00088  2.41351E+05 0.00089  2.08402E+05 0.00078  1.87401E+05 0.00093  1.73336E+05 0.00068  1.64802E+05 0.00093  1.60408E+05 0.00094  1.38578E+05 0.00098  1.36859E+05 0.00096  1.35671E+05 0.00105  1.33449E+05 0.00109  2.60127E+05 0.00066  2.51104E+05 0.00077  1.80943E+05 0.00073  1.17167E+05 0.00066  1.35406E+05 0.00099  1.28285E+05 0.00091  1.14958E+05 0.00075  1.87309E+05 0.00080  4.32430E+04 0.00163  5.41153E+04 0.00134  4.91915E+04 0.00147  2.86482E+04 0.00199  4.93909E+04 0.00139  3.33439E+04 0.00153  2.77780E+04 0.00180  5.01033E+03 0.00408  4.58265E+03 0.00366  4.13471E+03 0.00272  3.98085E+03 0.00351  4.07311E+03 0.00349  4.40975E+03 0.00303  5.02906E+03 0.00294  4.91862E+03 0.00365  9.53237E+03 0.00317  1.54742E+04 0.00216  1.98660E+04 0.00222  5.27839E+04 0.00161  5.56154E+04 0.00158  5.94603E+04 0.00106  3.86542E+04 0.00172  2.74520E+04 0.00100  2.03962E+04 0.00188  2.40740E+04 0.00228  4.66396E+04 0.00145  6.51865E+04 0.00129  1.28542E+05 0.00146  2.00465E+05 0.00139  3.01423E+05 0.00147  1.93366E+05 0.00153  1.38524E+05 0.00154  9.96181E+04 0.00156  8.97413E+04 0.00190  8.84856E+04 0.00167  7.38218E+04 0.00196  5.00090E+04 0.00202  4.61866E+04 0.00184  4.10725E+04 0.00193  3.47454E+04 0.00210  2.73600E+04 0.00195  1.83710E+04 0.00185  6.50773E+03 0.00252 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.02030E+00 0.00050 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.31752E+18 0.00052  3.30776E+17 0.00132 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37991E-01 8.0E-05  1.54816E+00 0.00041 ];
INF_CAPT                  (idx, [1:   4]) = [  7.40631E-03 0.00069  3.49152E-02 0.00071 ];
INF_ABS                   (idx, [1:   4]) = [  9.43857E-03 0.00054  6.83775E-02 0.00101 ];
INF_FISS                  (idx, [1:   4]) = [  2.03226E-03 0.00043  3.34623E-02 0.00134 ];
INF_NSF                   (idx, [1:   4]) = [  5.34697E-03 0.00042  8.65979E-02 0.00137 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.63104E+00 4.9E-05  2.58792E+00 5.2E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04725E+02 5.3E-06  2.04299E+02 9.0E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.68990E-08 0.00038  2.56278E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28559E-01 8.2E-05  1.47981E+00 0.00048 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43561E-01 0.00016  3.93080E-01 0.00050 ];
INF_SCATT2                (idx, [1:   4]) = [  9.60732E-02 0.00024  9.33087E-02 0.00069 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32548E-03 0.00282  2.80787E-02 0.00272 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03207E-02 0.00190 -8.65165E-03 0.00754 ];
INF_SCATT5                (idx, [1:   4]) = [  1.44978E-04 0.12664  6.51861E-03 0.00926 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07557E-03 0.00382 -1.67949E-02 0.00295 ];
INF_SCATT7                (idx, [1:   4]) = [  7.29740E-04 0.02411  3.53333E-04 0.10148 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28602E-01 8.2E-05  1.47981E+00 0.00048 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43562E-01 0.00016  3.93080E-01 0.00050 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.60735E-02 0.00024  9.33087E-02 0.00069 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32564E-03 0.00283  2.80787E-02 0.00272 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03205E-02 0.00190 -8.65165E-03 0.00754 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.44977E-04 0.12656  6.51861E-03 0.00926 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07540E-03 0.00383 -1.67949E-02 0.00295 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.29817E-04 0.02408  3.53333E-04 0.10148 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12931E-01 0.00028  1.00179E+00 0.00037 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56546E+00 0.00028  3.32737E-01 0.00037 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.39589E-03 0.00053  6.83775E-02 0.00101 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69714E-02 0.00021  6.95586E-02 0.00113 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11020E-01 8.3E-05  1.75397E-02 0.00040  1.20785E-03 0.00356  1.47860E+00 0.00048 ];
INF_S1                    (idx, [1:   8]) = [  2.38470E-01 0.00016  5.09142E-03 0.00106  5.19841E-04 0.00776  3.92560E-01 0.00050 ];
INF_S2                    (idx, [1:   8]) = [  9.76305E-02 0.00023 -1.55736E-03 0.00247  2.84054E-04 0.00897  9.30247E-02 0.00069 ];
INF_S3                    (idx, [1:   8]) = [  9.13379E-03 0.00228 -1.80830E-03 0.00184  1.04576E-04 0.02201  2.79741E-02 0.00275 ];
INF_S4                    (idx, [1:   8]) = [ -9.73760E-03 0.00200 -5.83119E-04 0.00466  3.88297E-06 0.53858 -8.65554E-03 0.00759 ];
INF_S5                    (idx, [1:   8]) = [  1.15747E-04 0.16311  2.92310E-05 0.09366 -3.98501E-05 0.04610  6.55846E-03 0.00917 ];
INF_S6                    (idx, [1:   8]) = [  5.21515E-03 0.00372 -1.39575E-04 0.02115 -5.09506E-05 0.02765 -1.67439E-02 0.00295 ];
INF_S7                    (idx, [1:   8]) = [  8.97461E-04 0.01937 -1.67721E-04 0.01411 -4.67482E-05 0.02645  4.00081E-04 0.08905 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11062E-01 8.3E-05  1.75397E-02 0.00040  1.20785E-03 0.00356  1.47860E+00 0.00048 ];
INF_SP1                   (idx, [1:   8]) = [  2.38470E-01 0.00016  5.09142E-03 0.00106  5.19841E-04 0.00776  3.92560E-01 0.00050 ];
INF_SP2                   (idx, [1:   8]) = [  9.76309E-02 0.00023 -1.55736E-03 0.00247  2.84054E-04 0.00897  9.30247E-02 0.00069 ];
INF_SP3                   (idx, [1:   8]) = [  9.13394E-03 0.00228 -1.80830E-03 0.00184  1.04576E-04 0.02201  2.79741E-02 0.00275 ];
INF_SP4                   (idx, [1:   8]) = [ -9.73736E-03 0.00200 -5.83119E-04 0.00466  3.88297E-06 0.53858 -8.65554E-03 0.00759 ];
INF_SP5                   (idx, [1:   8]) = [  1.15746E-04 0.16305  2.92310E-05 0.09366 -3.98501E-05 0.04610  6.55846E-03 0.00917 ];
INF_SP6                   (idx, [1:   8]) = [  5.21498E-03 0.00373 -1.39575E-04 0.02115 -5.09506E-05 0.02765 -1.67439E-02 0.00295 ];
INF_SP7                   (idx, [1:   8]) = [  8.97538E-04 0.01935 -1.67721E-04 0.01411 -4.67482E-05 0.02645  4.00081E-04 0.08905 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31868E-01 0.00065  1.15330E+00 0.00646 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33862E-01 0.00116  1.24019E+00 0.00795 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33327E-01 0.00077  1.24430E+00 0.00884 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28499E-01 0.00085  1.01000E+00 0.00655 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43761E+00 0.00065  2.89319E-01 0.00653 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42539E+00 0.00116  2.69188E-01 0.00802 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42863E+00 0.00077  2.68401E-01 0.00903 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45882E+00 0.00085  3.30368E-01 0.00647 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.67421E-03 0.00924  1.60558E-04 0.05268  1.00145E-03 0.02336  8.77932E-04 0.02174  2.49867E-03 0.01350  8.74980E-04 0.02269  2.60620E-04 0.04062 ];
LAMBDA                    (idx, [1:  14]) = [  7.82100E-01 0.02068  1.25061E-02 0.00045  3.11711E-02 0.00059  1.10382E-01 0.00059  3.21682E-01 0.00042  1.32849E+00 0.00148  8.83075E+00 0.00606 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0008' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:50:49 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:05:51 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299849 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.96550E-01  9.98954E-01  1.00219E+00  9.99573E-01  1.00273E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 6.6E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15076E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88492E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05260E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05692E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67126E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.44611E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.44527E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.30831E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.10133E-01 0.00112  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000795 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00016E+04 0.00083 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00016E+04 0.00083 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.72936E+02 ;
RUNNING_TIME              (idx, 1)        =  7.50324E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.62450E-01  4.62450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.28050E-01  9.41666E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.36807E+01  3.05313E+00  2.36953E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  6.54700E-01  2.44167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.34000E-02  8.50002E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.50323E+01  1.13469E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97034 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00015E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78656E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.37116E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.75052E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.01941E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.75264E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.22360E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61849E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.62813E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.54270E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.51701E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  8.63898E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.46041E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.78798E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.77097E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.36280E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.30139E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.40195E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.44222E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.20415E+13 ;
CS137_ACTIVITY            (idx, 1)        =  1.68330E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.14809E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.37258E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.33020E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.24550E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.63812E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 13 ;
BURNUP                     (idx, [1:  2])  = [  1.25000E+01  1.25011E+01 ];
BURN_DAYS                 (idx, 1)        =  3.12500E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.92785E-01 0.00104 ];
U235_FISS                 (idx, [1:   4]) = [  7.89278E+15 0.00093  5.75411E-01 0.00068 ];
U238_FISS                 (idx, [1:   4]) = [  1.00892E+15 0.00273  7.35448E-02 0.00256 ];
PU239_FISS                (idx, [1:   4]) = [  4.31170E+15 0.00124  3.14345E-01 0.00111 ];
PU240_FISS                (idx, [1:   4]) = [  5.24413E+12 0.03745  3.82034E-04 0.03737 ];
PU241_FISS                (idx, [1:   4]) = [  4.87182E+14 0.00404  3.55175E-02 0.00399 ];
U235_CAPT                 (idx, [1:   4]) = [  1.76911E+15 0.00192  7.89239E-02 0.00191 ];
U238_CAPT                 (idx, [1:   4]) = [  8.72387E+15 0.00102  3.89143E-01 0.00071 ];
PU239_CAPT                (idx, [1:   4]) = [  2.39560E+15 0.00170  1.06864E-01 0.00158 ];
PU240_CAPT                (idx, [1:   4]) = [  1.31861E+15 0.00256  5.88182E-02 0.00244 ];
PU241_CAPT                (idx, [1:   4]) = [  1.75051E+14 0.00668  7.81036E-03 0.00671 ];
XE135_CAPT                (idx, [1:   4]) = [  7.40285E+14 0.00316  3.30234E-02 0.00311 ];
SM149_CAPT                (idx, [1:   4]) = [  2.05712E+14 0.00613  9.17732E-03 0.00613 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000795 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.97256E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000795 5.00797E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3076461 3.08091E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1882516 1.88524E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 41818 4.18291E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000795 5.00797E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -8.00937E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 6.3E-09  4.50056E+05 6.3E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.59381E+16 2.2E-05  3.59381E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37226E+16 4.0E-06  1.37226E+16 4.0E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.24206E+16 0.00046  1.69949E+16 0.00045  5.42571E+15 0.00118 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.61432E+16 0.00029  3.07175E+16 0.00025  5.42571E+15 0.00118 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.63812E+16 0.00051  3.63812E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.69649E+18 0.00050  4.54624E+17 0.00046  1.24187E+18 0.00056 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.04381E+14 0.00506 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.64476E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.25525E+18 0.00065 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11060E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11060E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.57791E+00 0.00050 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.91081E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.41068E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24444E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94542E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97076E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.95757E-01 0.00058 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.87427E-01 0.00058 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.61889E+00 2.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04701E+02 4.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.87403E-01 0.00059  9.81917E-01 0.00058  5.50943E-03 0.00954 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.87615E-01 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  9.87949E-01 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.87615E-01 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  9.95944E-01 0.00029 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71765E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71742E+01 9.1E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.96296E-07 0.00363 ];
IMP_EALF                  (idx, [1:   2]) = [  6.96020E-07 0.00156 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.50931E-01 0.00288 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.51839E-01 0.00113 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.76600E-03 0.00606  1.52032E-04 0.03692  1.00041E-03 0.01350  8.97290E-04 0.01474  2.57275E-03 0.00881  8.71488E-04 0.01499  2.72032E-04 0.02751 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.77456E-01 0.01417  9.79443E-03 0.02365  3.11290E-02 0.00041  1.10390E-01 0.00043  3.22060E-01 0.00030  1.31681E+00 0.00134  8.21630E+00 0.01288 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.59423E-03 0.00925  1.52603E-04 0.05509  9.78403E-04 0.02182  8.70860E-04 0.02155  2.48795E-03 0.01369  8.37523E-04 0.02197  2.66896E-04 0.04182 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.75901E-01 0.02227  1.25329E-02 0.00081  3.11405E-02 0.00060  1.10374E-01 0.00061  3.21984E-01 0.00045  1.31215E+00 0.00210  8.67232E+00 0.00767 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.48307E-05 0.00139  2.48193E-05 0.00139  2.67938E-05 0.01651 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.45133E-05 0.00124  2.45022E-05 0.00124  2.64512E-05 0.01646 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.58523E-03 0.00960  1.56993E-04 0.05792  9.63369E-04 0.02358  8.70728E-04 0.02371  2.46928E-03 0.01429  8.55903E-04 0.02471  2.68964E-04 0.04850 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.71374E-01 0.02418  1.25236E-02 0.00099  3.11373E-02 0.00080  1.10338E-01 0.00077  3.21825E-01 0.00050  1.31336E+00 0.00252  8.72417E+00 0.00980 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.43318E-05 0.00326  2.43230E-05 0.00327  2.59126E-05 0.05018 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.40204E-05 0.00319  2.40119E-05 0.00320  2.55607E-05 0.05004 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.61039E-03 0.03266  1.82568E-04 0.19089  9.54757E-04 0.08103  8.57468E-04 0.08910  2.41127E-03 0.04893  9.36643E-04 0.08561  2.67676E-04 0.14671 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.08553E-01 0.07756  1.25138E-02 0.00202  3.11602E-02 0.00186  1.10227E-01 0.00174  3.21754E-01 0.00142  1.31149E+00 0.00567  8.69793E+00 0.02414 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.61984E-03 0.03192  1.68935E-04 0.19548  9.67117E-04 0.08066  8.57765E-04 0.08779  2.43218E-03 0.04673  9.32901E-04 0.08512  2.60946E-04 0.14045 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.11211E-01 0.07675  1.25138E-02 0.00202  3.11669E-02 0.00185  1.10232E-01 0.00173  3.21768E-01 0.00140  1.31260E+00 0.00552  8.69998E+00 0.02432 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.31637E+02 0.03269 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.45746E-05 0.00087 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.42608E-05 0.00063 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.46138E-03 0.00659 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.22351E+02 0.00670 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.56168E-07 0.00075 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.73394E-06 0.00043  2.73378E-06 0.00042  2.76013E-06 0.00545 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.73065E-05 0.00092  3.73248E-05 0.00092  3.41396E-05 0.01093 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.39059E-01 0.00034  6.38988E-01 0.00034  6.63564E-01 0.00945 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06523E+01 0.01365 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.44527E+01 0.00048  3.30702E+01 0.00048 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.74190E+04 0.00270  3.00978E+05 0.00191  6.09840E+05 0.00087  6.51644E+05 0.00090  5.98712E+05 0.00060  6.42831E+05 0.00073  4.36217E+05 0.00063  3.85845E+05 0.00064  2.95336E+05 0.00106  2.41799E+05 0.00058  2.07952E+05 0.00081  1.87851E+05 0.00095  1.73425E+05 0.00073  1.64531E+05 0.00067  1.60461E+05 0.00087  1.38460E+05 0.00118  1.36971E+05 0.00103  1.35964E+05 0.00086  1.33115E+05 0.00080  2.60478E+05 0.00062  2.51085E+05 0.00060  1.81157E+05 0.00069  1.17070E+05 0.00094  1.35317E+05 0.00080  1.28225E+05 0.00064  1.14791E+05 0.00088  1.86785E+05 0.00064  4.30858E+04 0.00169  5.39391E+04 0.00147  4.92422E+04 0.00137  2.84684E+04 0.00141  4.94507E+04 0.00173  3.31866E+04 0.00196  2.74989E+04 0.00197  4.90589E+03 0.00305  4.37894E+03 0.00395  3.89305E+03 0.00287  3.72631E+03 0.00329  3.79531E+03 0.00366  4.17786E+03 0.00314  4.85063E+03 0.00371  4.80752E+03 0.00376  9.30627E+03 0.00251  1.52324E+04 0.00247  1.96750E+04 0.00247  5.23624E+04 0.00138  5.50178E+04 0.00167  5.86016E+04 0.00112  3.79600E+04 0.00146  2.68760E+04 0.00142  2.00211E+04 0.00190  2.35358E+04 0.00138  4.56213E+04 0.00158  6.39935E+04 0.00137  1.26542E+05 0.00098  1.98035E+05 0.00127  2.98471E+05 0.00153  1.91577E+05 0.00159  1.37533E+05 0.00178  9.89639E+04 0.00175  8.90076E+04 0.00172  8.78081E+04 0.00161  7.33737E+04 0.00152  4.96927E+04 0.00149  4.59890E+04 0.00153  4.08871E+04 0.00148  3.45729E+04 0.00160  2.73095E+04 0.00180  1.82382E+04 0.00213  6.47605E+03 0.00242 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.96283E-01 0.00053 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.35888E+18 0.00050  3.37635E+17 0.00121 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38082E-01 0.00011  1.55220E+00 0.00035 ];
INF_CAPT                  (idx, [1:   4]) = [  7.61344E-03 0.00057  3.57666E-02 0.00066 ];
INF_ABS                   (idx, [1:   4]) = [  9.57834E-03 0.00049  6.85080E-02 0.00094 ];
INF_FISS                  (idx, [1:   4]) = [  1.96490E-03 0.00051  3.27414E-02 0.00126 ];
INF_NSF                   (idx, [1:   4]) = [  5.20083E-03 0.00052  8.55250E-02 0.00128 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.64687E+00 5.7E-05  2.61213E+00 5.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04943E+02 5.7E-06  2.04642E+02 9.0E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.65479E-08 0.00042  2.56713E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28510E-01 0.00012  1.48370E+00 0.00041 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43550E-01 0.00018  3.94030E-01 0.00041 ];
INF_SCATT2                (idx, [1:   4]) = [  9.60817E-02 0.00029  9.33515E-02 0.00077 ];
INF_SCATT3                (idx, [1:   4]) = [  7.29641E-03 0.00312  2.80423E-02 0.00306 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02972E-02 0.00219 -8.65264E-03 0.00455 ];
INF_SCATT5                (idx, [1:   4]) = [  1.74605E-04 0.10054  6.62866E-03 0.00795 ];
INF_SCATT6                (idx, [1:   4]) = [  5.09218E-03 0.00412 -1.68109E-02 0.00289 ];
INF_SCATT7                (idx, [1:   4]) = [  7.56347E-04 0.02013  4.53679E-04 0.10111 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28552E-01 0.00012  1.48370E+00 0.00041 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43551E-01 0.00018  3.94030E-01 0.00041 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.60815E-02 0.00029  9.33515E-02 0.00077 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.29658E-03 0.00312  2.80423E-02 0.00306 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02973E-02 0.00219 -8.65264E-03 0.00455 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.74577E-04 0.10049  6.62866E-03 0.00795 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.09208E-03 0.00412 -1.68109E-02 0.00289 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.56214E-04 0.02016  4.53679E-04 0.10111 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12824E-01 0.00028  1.00558E+00 0.00037 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56624E+00 0.00028  3.31485E-01 0.00037 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.53565E-03 0.00049  6.85080E-02 0.00094 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69697E-02 0.00022  6.97094E-02 0.00109 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11112E-01 0.00011  1.73978E-02 0.00044  1.20851E-03 0.00567  1.48249E+00 0.00042 ];
INF_S1                    (idx, [1:   8]) = [  2.38499E-01 0.00017  5.05034E-03 0.00083  5.18978E-04 0.00852  3.93511E-01 0.00041 ];
INF_S2                    (idx, [1:   8]) = [  9.76366E-02 0.00029 -1.55488E-03 0.00227  2.82499E-04 0.01066  9.30690E-02 0.00078 ];
INF_S3                    (idx, [1:   8]) = [  9.09311E-03 0.00244 -1.79671E-03 0.00148  9.96186E-05 0.02284  2.79426E-02 0.00308 ];
INF_S4                    (idx, [1:   8]) = [ -9.72437E-03 0.00219 -5.72797E-04 0.00533 -2.19921E-07 1.00000 -8.65242E-03 0.00459 ];
INF_S5                    (idx, [1:   8]) = [  1.46190E-04 0.11913  2.84145E-05 0.08863 -4.19319E-05 0.03651  6.67060E-03 0.00794 ];
INF_S6                    (idx, [1:   8]) = [  5.23210E-03 0.00385 -1.39917E-04 0.01994 -5.16537E-05 0.03332 -1.67592E-02 0.00293 ];
INF_S7                    (idx, [1:   8]) = [  9.27652E-04 0.01615 -1.71305E-04 0.01266 -4.76869E-05 0.02229  5.01366E-04 0.09107 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11155E-01 0.00011  1.73978E-02 0.00044  1.20851E-03 0.00567  1.48249E+00 0.00042 ];
INF_SP1                   (idx, [1:   8]) = [  2.38501E-01 0.00017  5.05034E-03 0.00083  5.18978E-04 0.00852  3.93511E-01 0.00041 ];
INF_SP2                   (idx, [1:   8]) = [  9.76364E-02 0.00029 -1.55488E-03 0.00227  2.82499E-04 0.01066  9.30690E-02 0.00078 ];
INF_SP3                   (idx, [1:   8]) = [  9.09329E-03 0.00244 -1.79671E-03 0.00148  9.96186E-05 0.02284  2.79426E-02 0.00308 ];
INF_SP4                   (idx, [1:   8]) = [ -9.72452E-03 0.00219 -5.72797E-04 0.00533 -2.19921E-07 1.00000 -8.65242E-03 0.00459 ];
INF_SP5                   (idx, [1:   8]) = [  1.46163E-04 0.11912  2.84145E-05 0.08863 -4.19319E-05 0.03651  6.67060E-03 0.00794 ];
INF_SP6                   (idx, [1:   8]) = [  5.23199E-03 0.00386 -1.39917E-04 0.01994 -5.16537E-05 0.03332 -1.67592E-02 0.00293 ];
INF_SP7                   (idx, [1:   8]) = [  9.27518E-04 0.01618 -1.71305E-04 0.01266 -4.76869E-05 0.02229  5.01366E-04 0.09107 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31629E-01 0.00072  1.16724E+00 0.00609 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33156E-01 0.00084  1.26361E+00 0.00695 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33275E-01 0.00087  1.25823E+00 0.01001 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28524E-01 0.00108  1.01772E+00 0.00620 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43910E+00 0.00072  2.85829E-01 0.00613 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42968E+00 0.00084  2.64097E-01 0.00689 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42895E+00 0.00087  2.65558E-01 0.01000 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45868E+00 0.00109  3.27831E-01 0.00617 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.59423E-03 0.00925  1.52603E-04 0.05509  9.78403E-04 0.02182  8.70860E-04 0.02155  2.48795E-03 0.01369  8.37523E-04 0.02197  2.66896E-04 0.04182 ];
LAMBDA                    (idx, [1:  14]) = [  7.75901E-01 0.02227  1.25329E-02 0.00081  3.11405E-02 0.00060  1.10374E-01 0.00061  3.21984E-01 0.00045  1.31215E+00 0.00210  8.67232E+00 0.00767 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0008' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:50:49 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:11:24 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299849 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.94548E-01  9.98847E-01  1.00246E+00  9.99999E-01  1.00414E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15234E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88477E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05904E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06337E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67156E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.45117E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.45030E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.28619E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.10897E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000566 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00011E+04 0.00075 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00011E+04 0.00075 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.00660E+02 ;
RUNNING_TIME              (idx, 1)        =  8.05834E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.62450E-01  4.62450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.48283E-01  9.73334E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.91598E+01  3.07318E+00  2.40587E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  7.05833E-01  2.46167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.58000E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  8.05833E+01  1.13749E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97200 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00105E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79067E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.42146E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.73817E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.95432E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.80287E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.25931E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61856E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.61221E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.87748E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.60160E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.15811E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.73397E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.19361E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.82820E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.59390E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.32534E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.43730E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.47652E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.70652E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.01793E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.19479E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.34350E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.33595E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.26021E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.74911E+12 0.00053  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 14 ;
BURNUP                     (idx, [1:  2])  = [  1.50000E+01  1.50013E+01 ];
BURN_DAYS                 (idx, 1)        =  3.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.15093E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  7.27584E+15 0.00098  5.30886E-01 0.00071 ];
U238_FISS                 (idx, [1:   4]) = [  1.03138E+15 0.00266  7.52493E-02 0.00251 ];
PU239_FISS                (idx, [1:   4]) = [  4.72603E+15 0.00117  3.44849E-01 0.00103 ];
PU240_FISS                (idx, [1:   4]) = [  6.61454E+12 0.03336  4.82735E-04 0.03338 ];
PU241_FISS                (idx, [1:   4]) = [  6.52120E+14 0.00343  4.75851E-02 0.00341 ];
U235_CAPT                 (idx, [1:   4]) = [  1.62898E+15 0.00225  6.92572E-02 0.00218 ];
U238_CAPT                 (idx, [1:   4]) = [  8.92325E+15 0.00104  3.79356E-01 0.00074 ];
PU239_CAPT                (idx, [1:   4]) = [  2.61571E+15 0.00172  1.11217E-01 0.00171 ];
PU240_CAPT                (idx, [1:   4]) = [  1.55923E+15 0.00232  6.62860E-02 0.00218 ];
PU241_CAPT                (idx, [1:   4]) = [  2.33772E+14 0.00560  9.93942E-03 0.00559 ];
XE135_CAPT                (idx, [1:   4]) = [  7.46830E+14 0.00312  3.17577E-02 0.00319 ];
SM149_CAPT                (idx, [1:   4]) = [  2.13082E+14 0.00598  9.05939E-03 0.00596 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000566 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.87169E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000566 5.00787E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3132227 3.13690E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1825222 1.82784E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 43117 4.31300E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000566 5.00787E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -7.17118E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 6.3E-09  4.50056E+05 6.3E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.61627E+16 2.0E-05  3.61627E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37035E+16 3.9E-06  1.37035E+16 3.9E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.35322E+16 0.00045  1.79178E+16 0.00048  5.61443E+15 0.00121 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.72357E+16 0.00028  3.16213E+16 0.00027  5.61443E+15 0.00121 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.74911E+16 0.00053  3.74911E+16 0.00053  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.74762E+18 0.00050  4.66689E+17 0.00049  1.28093E+18 0.00056 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.23447E+14 0.00519 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.75591E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.29537E+18 0.00065 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10770E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10770E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.55884E+00 0.00053 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.88647E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.37325E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24215E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94393E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96964E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.73153E-01 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.64759E-01 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.63895E+00 2.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04987E+02 3.9E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.64663E-01 0.00055  9.59554E-01 0.00055  5.20533E-03 0.00998 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.64404E-01 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  9.64703E-01 0.00053 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.64404E-01 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  9.72793E-01 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71496E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71371E+01 9.3E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.15356E-07 0.00367 ];
IMP_EALF                  (idx, [1:   2]) = [  7.22379E-07 0.00159 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.57796E-01 0.00273 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.59224E-01 0.00117 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.75864E-03 0.00589  1.55263E-04 0.03627  9.75709E-04 0.01401  9.10384E-04 0.01504  2.54834E-03 0.00895  8.97822E-04 0.01515  2.71124E-04 0.02667 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.78934E-01 0.01378  9.72853E-03 0.02406  3.10475E-02 0.00046  1.10402E-01 0.00044  3.22403E-01 0.00030  1.30705E+00 0.00153  8.06306E+00 0.01390 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.44866E-03 0.00881  1.40833E-04 0.05309  8.95074E-04 0.02153  8.74914E-04 0.02227  2.42277E-03 0.01367  8.73004E-04 0.02222  2.42065E-04 0.04173 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.56447E-01 0.02017  1.25343E-02 0.00083  3.10416E-02 0.00064  1.10452E-01 0.00061  3.22346E-01 0.00045  1.30835E+00 0.00212  8.58661E+00 0.00887 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.54347E-05 0.00135  2.54211E-05 0.00135  2.81054E-05 0.01618 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.45322E-05 0.00124  2.45192E-05 0.00124  2.71045E-05 0.01615 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.40123E-03 0.01000  1.36699E-04 0.06199  9.16415E-04 0.02660  8.77362E-04 0.02446  2.35064E-03 0.01520  8.65173E-04 0.02588  2.54946E-04 0.04665 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.75583E-01 0.02446  1.25155E-02 0.00085  3.10207E-02 0.00083  1.10474E-01 0.00081  3.22300E-01 0.00057  1.30831E+00 0.00259  8.53025E+00 0.01239 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.48102E-05 0.00327  2.47938E-05 0.00326  2.67839E-05 0.04318 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.39281E-05 0.00318  2.39123E-05 0.00318  2.58242E-05 0.04327 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.46581E-03 0.03540  2.01175E-04 0.18075  1.01739E-03 0.08617  8.08756E-04 0.08966  2.32135E-03 0.05498  9.18151E-04 0.08245  1.98991E-04 0.15441 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.17877E-01 0.07315  1.25380E-02 0.00275  3.09149E-02 0.00186  1.10462E-01 0.00195  3.23147E-01 0.00161  1.31333E+00 0.00575  8.73680E+00 0.02799 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.48379E-03 0.03456  1.89946E-04 0.17635  1.03621E-03 0.08222  8.05890E-04 0.08760  2.32980E-03 0.05349  9.24350E-04 0.08277  1.97587E-04 0.15050 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.16070E-01 0.07222  1.25380E-02 0.00275  3.09188E-02 0.00186  1.10487E-01 0.00195  3.23167E-01 0.00159  1.31377E+00 0.00570  8.73760E+00 0.02799 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.21371E+02 0.03564 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.51424E-05 0.00081 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.42501E-05 0.00057 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.42395E-03 0.00639 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.15726E+02 0.00633 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.56833E-07 0.00079 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.71480E-06 0.00044  2.71461E-06 0.00044  2.74467E-06 0.00584 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.75756E-05 0.00094  3.75945E-05 0.00094  3.43691E-05 0.01104 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.35310E-01 0.00035  6.35356E-01 0.00035  6.38797E-01 0.00964 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06035E+01 0.01359 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.45030E+01 0.00049  3.30945E+01 0.00052 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.83215E+04 0.00271  3.01958E+05 0.00174  6.09004E+05 0.00092  6.51330E+05 0.00092  5.99978E+05 0.00081  6.42491E+05 0.00076  4.36031E+05 0.00075  3.86194E+05 0.00082  2.95502E+05 0.00056  2.41338E+05 0.00072  2.08089E+05 0.00096  1.87611E+05 0.00091  1.73162E+05 0.00078  1.64376E+05 0.00090  1.60323E+05 0.00073  1.38857E+05 0.00076  1.36795E+05 0.00100  1.35773E+05 0.00073  1.33350E+05 0.00078  2.60468E+05 0.00094  2.50944E+05 0.00065  1.81212E+05 0.00077  1.17212E+05 0.00103  1.35507E+05 0.00103  1.28453E+05 0.00063  1.14787E+05 0.00104  1.86422E+05 0.00068  4.31121E+04 0.00176  5.39170E+04 0.00209  4.88149E+04 0.00135  2.84961E+04 0.00190  4.93615E+04 0.00131  3.30300E+04 0.00174  2.71712E+04 0.00175  4.78291E+03 0.00397  4.18588E+03 0.00248  3.72456E+03 0.00222  3.56412E+03 0.00383  3.65287E+03 0.00363  4.04961E+03 0.00370  4.68925E+03 0.00234  4.71882E+03 0.00277  9.20407E+03 0.00295  1.49917E+04 0.00206  1.94303E+04 0.00256  5.19428E+04 0.00112  5.44955E+04 0.00136  5.80534E+04 0.00120  3.75065E+04 0.00177  2.65385E+04 0.00145  1.96470E+04 0.00237  2.31652E+04 0.00166  4.51474E+04 0.00136  6.33835E+04 0.00139  1.25728E+05 0.00127  1.97645E+05 0.00125  2.98508E+05 0.00114  1.92084E+05 0.00107  1.38037E+05 0.00109  9.92820E+04 0.00145  8.94065E+04 0.00116  8.82226E+04 0.00153  7.37295E+04 0.00137  4.98946E+04 0.00163  4.61342E+04 0.00166  4.10536E+04 0.00161  3.47323E+04 0.00145  2.74353E+04 0.00139  1.83582E+04 0.00192  6.53915E+03 0.00243 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.73097E-01 0.00047 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.39998E+18 0.00046  3.47678E+17 0.00105 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38398E-01 9.3E-05  1.55791E+00 0.00028 ];
INF_CAPT                  (idx, [1:   4]) = [  7.78200E-03 0.00063  3.63526E-02 0.00057 ];
INF_ABS                   (idx, [1:   4]) = [  9.68244E-03 0.00053  6.81206E-02 0.00079 ];
INF_FISS                  (idx, [1:   4]) = [  1.90044E-03 0.00054  3.17680E-02 0.00106 ];
INF_NSF                   (idx, [1:   4]) = [  5.05864E-03 0.00054  8.36590E-02 0.00108 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.66182E+00 5.1E-05  2.63344E+00 4.4E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05147E+02 6.5E-06  2.04948E+02 8.1E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.62604E-08 0.00044  2.57261E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28727E-01 9.6E-05  1.48977E+00 0.00033 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43754E-01 0.00014  3.95563E-01 0.00037 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61947E-02 0.00030  9.36291E-02 0.00082 ];
INF_SCATT3                (idx, [1:   4]) = [  7.30351E-03 0.00335  2.81138E-02 0.00221 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03386E-02 0.00209 -8.82095E-03 0.00593 ];
INF_SCATT5                (idx, [1:   4]) = [  1.53973E-04 0.10686  6.56739E-03 0.00968 ];
INF_SCATT6                (idx, [1:   4]) = [  5.12885E-03 0.00306 -1.69386E-02 0.00294 ];
INF_SCATT7                (idx, [1:   4]) = [  7.67941E-04 0.01540  4.33403E-04 0.09380 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28769E-01 9.7E-05  1.48977E+00 0.00033 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43754E-01 0.00014  3.95563E-01 0.00037 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61950E-02 0.00030  9.36291E-02 0.00082 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.30352E-03 0.00336  2.81138E-02 0.00221 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03383E-02 0.00209 -8.82095E-03 0.00593 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.53670E-04 0.10703  6.56739E-03 0.00968 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.12894E-03 0.00307 -1.69386E-02 0.00294 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.68014E-04 0.01538  4.33403E-04 0.09380 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12646E-01 0.00031  1.00981E+00 0.00026 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56755E+00 0.00031  3.30097E-01 0.00026 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.64028E-03 0.00052  6.81206E-02 0.00079 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69700E-02 0.00023  6.93338E-02 0.00093 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11428E-01 9.1E-05  1.72992E-02 0.00040  1.20074E-03 0.00513  1.48857E+00 0.00033 ];
INF_S1                    (idx, [1:   8]) = [  2.38740E-01 0.00013  5.01338E-03 0.00087  5.16861E-04 0.00746  3.95046E-01 0.00037 ];
INF_S2                    (idx, [1:   8]) = [  9.77466E-02 0.00030 -1.55184E-03 0.00212  2.85724E-04 0.00844  9.33434E-02 0.00082 ];
INF_S3                    (idx, [1:   8]) = [  9.08868E-03 0.00261 -1.78517E-03 0.00173  1.04390E-04 0.01747  2.80094E-02 0.00222 ];
INF_S4                    (idx, [1:   8]) = [ -9.77112E-03 0.00221 -5.67437E-04 0.00416  2.89880E-06 0.68506 -8.82385E-03 0.00596 ];
INF_S5                    (idx, [1:   8]) = [  1.19166E-04 0.13286  3.48072E-05 0.06260 -3.94468E-05 0.04591  6.60684E-03 0.00960 ];
INF_S6                    (idx, [1:   8]) = [  5.26095E-03 0.00288 -1.32097E-04 0.01940 -5.21266E-05 0.03290 -1.68865E-02 0.00296 ];
INF_S7                    (idx, [1:   8]) = [  9.34640E-04 0.01213 -1.66699E-04 0.00950 -4.65425E-05 0.03305  4.79945E-04 0.08523 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11470E-01 9.1E-05  1.72992E-02 0.00040  1.20074E-03 0.00513  1.48857E+00 0.00033 ];
INF_SP1                   (idx, [1:   8]) = [  2.38741E-01 0.00013  5.01338E-03 0.00087  5.16861E-04 0.00746  3.95046E-01 0.00037 ];
INF_SP2                   (idx, [1:   8]) = [  9.77469E-02 0.00030 -1.55184E-03 0.00212  2.85724E-04 0.00844  9.33434E-02 0.00082 ];
INF_SP3                   (idx, [1:   8]) = [  9.08869E-03 0.00261 -1.78517E-03 0.00173  1.04390E-04 0.01747  2.80094E-02 0.00222 ];
INF_SP4                   (idx, [1:   8]) = [ -9.77086E-03 0.00221 -5.67437E-04 0.00416  2.89880E-06 0.68506 -8.82385E-03 0.00596 ];
INF_SP5                   (idx, [1:   8]) = [  1.18863E-04 0.13318  3.48072E-05 0.06260 -3.94468E-05 0.04591  6.60684E-03 0.00960 ];
INF_SP6                   (idx, [1:   8]) = [  5.26103E-03 0.00288 -1.32097E-04 0.01940 -5.21266E-05 0.03290 -1.68865E-02 0.00296 ];
INF_SP7                   (idx, [1:   8]) = [  9.34713E-04 0.01212 -1.66699E-04 0.00950 -4.65425E-05 0.03305  4.79945E-04 0.08523 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31567E-01 0.00057  1.19309E+00 0.00762 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.32991E-01 0.00088  1.29380E+00 0.01092 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33141E-01 0.00070  1.30649E+00 0.01024 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28632E-01 0.00095  1.02592E+00 0.00607 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43948E+00 0.00057  2.79770E-01 0.00748 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.43070E+00 0.00088  2.58348E-01 0.01050 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42976E+00 0.00070  2.55766E-01 0.01003 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45798E+00 0.00096  3.25196E-01 0.00601 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.44866E-03 0.00881  1.40833E-04 0.05309  8.95074E-04 0.02153  8.74914E-04 0.02227  2.42277E-03 0.01367  8.73004E-04 0.02222  2.42065E-04 0.04173 ];
LAMBDA                    (idx, [1:  14]) = [  7.56447E-01 0.02017  1.25343E-02 0.00083  3.10416E-02 0.00064  1.10452E-01 0.00061  3.22346E-01 0.00045  1.30835E+00 0.00212  8.58661E+00 0.00887 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0008' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:50:49 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:16:56 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299849 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.95109E-01  9.98835E-01  1.00272E+00  9.99524E-01  1.00381E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.1E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15709E-02 0.00118  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88429E-01 1.4E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06247E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06680E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67440E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.45183E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.45095E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.26954E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.13013E-01 0.00119  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001208 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00024E+04 0.00087 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00024E+04 0.00087 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.28241E+02 ;
RUNNING_TIME              (idx, 1)        =  8.61054E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.62450E-01  4.62450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.68550E-01  9.81667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  8.46125E+01  3.07055E+00  2.38212E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  7.54483E-01  2.28500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.74500E-02  7.83336E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  8.61053E+01  1.13708E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97345 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00020E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79427E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.47180E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.72875E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.40081E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.85122E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.29406E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62055E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.59931E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.25393E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.68159E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.49856E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.00761E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.55373E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.88083E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.81453E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.34749E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.46865E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.50768E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.27931E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.35164E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.24128E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.31831E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.72073E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.27433E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.86161E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 15 ;
BURNUP                     (idx, [1:  2])  = [  1.75000E+01  1.75016E+01 ];
BURN_DAYS                 (idx, 1)        =  4.37500E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.39623E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  6.70200E+15 0.00099  4.89460E-01 0.00081 ];
U238_FISS                 (idx, [1:   4]) = [  1.07053E+15 0.00287  7.81685E-02 0.00268 ];
PU239_FISS                (idx, [1:   4]) = [  5.07731E+15 0.00116  3.70806E-01 0.00100 ];
PU240_FISS                (idx, [1:   4]) = [  8.44183E+12 0.03133  6.16650E-04 0.03136 ];
PU241_FISS                (idx, [1:   4]) = [  8.18926E+14 0.00315  5.98061E-02 0.00308 ];
U235_CAPT                 (idx, [1:   4]) = [  1.50808E+15 0.00222  6.11921E-02 0.00216 ];
U238_CAPT                 (idx, [1:   4]) = [  9.16808E+15 0.00109  3.71974E-01 0.00075 ];
PU239_CAPT                (idx, [1:   4]) = [  2.81265E+15 0.00170  1.14127E-01 0.00163 ];
PU240_CAPT                (idx, [1:   4]) = [  1.78235E+15 0.00223  7.23180E-02 0.00212 ];
PU241_CAPT                (idx, [1:   4]) = [  2.93548E+14 0.00515  1.19120E-02 0.00516 ];
XE135_CAPT                (idx, [1:   4]) = [  7.54805E+14 0.00314  3.06305E-02 0.00316 ];
SM149_CAPT                (idx, [1:   4]) = [  2.20653E+14 0.00592  8.95388E-03 0.00592 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001208 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.13240E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001208 5.00813E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3186565 3.19111E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1770683 1.77305E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 43960 4.39651E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001208 5.00813E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -1.02445E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 6.3E-09  4.50056E+05 6.3E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.63690E+16 2.1E-05  3.63690E+16 2.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36856E+16 4.2E-06  1.36856E+16 4.2E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.46351E+16 0.00044  1.88404E+16 0.00044  5.79471E+15 0.00122 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.83207E+16 0.00028  3.25260E+16 0.00025  5.79471E+15 0.00122 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.86161E+16 0.00052  3.86161E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.79842E+18 0.00051  4.79292E+17 0.00047  1.31913E+18 0.00057 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.39610E+14 0.00490 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.86603E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.33459E+18 0.00067 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10480E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10480E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.53609E+00 0.00055 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.86601E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.33077E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24303E+00 0.00038 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94273E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96916E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.50764E-01 0.00062 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.42405E-01 0.00062 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.65747E+00 2.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05255E+02 4.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.42384E-01 0.00063  9.37361E-01 0.00063  5.04402E-03 0.00968 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.42276E-01 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  9.41935E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.42276E-01 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  9.50637E-01 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70972E+01 0.00024 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70981E+01 9.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.54381E-07 0.00405 ];
IMP_EALF                  (idx, [1:   2]) = [  7.51192E-07 0.00170 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.66571E-01 0.00286 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.66790E-01 0.00121 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.78708E-03 0.00604  1.48622E-04 0.03940  1.00555E-03 0.01415  9.27614E-04 0.01479  2.53351E-03 0.00964  8.95818E-04 0.01584  2.75966E-04 0.02689 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.77717E-01 0.01375  9.21289E-03 0.02682  3.09698E-02 0.00046  1.10549E-01 0.00050  3.22401E-01 0.00030  1.29440E+00 0.00261  8.00042E+00 0.01467 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.36370E-03 0.00903  1.38586E-04 0.05488  9.40524E-04 0.02190  8.59919E-04 0.02421  2.36004E-03 0.01386  8.17858E-04 0.02357  2.46771E-04 0.04076 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.66784E-01 0.02174  1.25158E-02 0.00061  3.09510E-02 0.00067  1.10543E-01 0.00071  3.22603E-01 0.00045  1.29686E+00 0.00255  8.69768E+00 0.00821 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.59877E-05 0.00139  2.59735E-05 0.00140  2.86397E-05 0.01616 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.44859E-05 0.00126  2.44725E-05 0.00127  2.69916E-05 0.01622 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.34913E-03 0.00993  1.44648E-04 0.06415  9.20103E-04 0.02489  8.61611E-04 0.02452  2.32620E-03 0.01524  8.53314E-04 0.02582  2.43257E-04 0.04860 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.73558E-01 0.02522  1.25024E-02 0.00057  3.09315E-02 0.00086  1.10506E-01 0.00089  3.22392E-01 0.00057  1.30258E+00 0.00287  8.72297E+00 0.01181 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.54018E-05 0.00338  2.53914E-05 0.00340  2.62967E-05 0.03965 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.39331E-05 0.00331  2.39233E-05 0.00333  2.47656E-05 0.03958 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.37559E-03 0.03324  1.15839E-04 0.21407  1.00242E-03 0.08295  8.69130E-04 0.08503  2.21840E-03 0.05137  9.17258E-04 0.07875  2.52548E-04 0.17988 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.66843E-01 0.07875  1.24890E-02 5.8E-05  3.08774E-02 0.00191  1.10594E-01 0.00208  3.22129E-01 0.00156  1.29367E+00 0.00723  8.80889E+00 0.02337 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.39955E-03 0.03312  1.18179E-04 0.20796  9.98836E-04 0.08364  8.62370E-04 0.08285  2.23194E-03 0.05146  9.46430E-04 0.07847  2.41804E-04 0.17399 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.71198E-01 0.07797  1.24890E-02 5.7E-05  3.08703E-02 0.00190  1.10597E-01 0.00208  3.22075E-01 0.00154  1.29323E+00 0.00725  8.81050E+00 0.02338 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.12902E+02 0.03367 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.57130E-05 0.00090 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.42266E-05 0.00063 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.40952E-03 0.00670 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.10359E+02 0.00661 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.57175E-07 0.00076 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.69698E-06 0.00042  2.69671E-06 0.00042  2.75165E-06 0.00637 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.78409E-05 0.00092  3.78549E-05 0.00092  3.53584E-05 0.01130 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.31028E-01 0.00036  6.31172E-01 0.00036  6.16938E-01 0.00931 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07172E+01 0.01443 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.45095E+01 0.00048  3.30237E+01 0.00052 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.84287E+04 0.00280  3.02452E+05 0.00161  6.10199E+05 0.00086  6.51405E+05 0.00064  5.99049E+05 0.00065  6.42377E+05 0.00082  4.36105E+05 0.00066  3.85451E+05 0.00075  2.95296E+05 0.00084  2.41441E+05 0.00066  2.08006E+05 0.00069  1.87283E+05 0.00085  1.73150E+05 0.00090  1.64709E+05 0.00086  1.60309E+05 0.00079  1.38519E+05 0.00094  1.36701E+05 0.00112  1.35745E+05 0.00097  1.33266E+05 0.00084  2.60179E+05 0.00074  2.50991E+05 0.00063  1.80861E+05 0.00082  1.17172E+05 0.00111  1.35222E+05 0.00099  1.28597E+05 0.00082  1.14527E+05 0.00111  1.85875E+05 0.00072  4.29918E+04 0.00134  5.36180E+04 0.00149  4.86732E+04 0.00156  2.85145E+04 0.00195  4.90664E+04 0.00161  3.27781E+04 0.00154  2.68534E+04 0.00204  4.68239E+03 0.00406  4.07130E+03 0.00307  3.57725E+03 0.00365  3.44768E+03 0.00319  3.49958E+03 0.00339  3.88905E+03 0.00396  4.55439E+03 0.00375  4.61215E+03 0.00309  9.03524E+03 0.00299  1.48723E+04 0.00203  1.92445E+04 0.00197  5.13127E+04 0.00144  5.40490E+04 0.00131  5.75194E+04 0.00120  3.72318E+04 0.00161  2.62071E+04 0.00202  1.94010E+04 0.00228  2.28313E+04 0.00174  4.46017E+04 0.00156  6.28622E+04 0.00110  1.25108E+05 0.00110  1.96970E+05 0.00142  2.98348E+05 0.00159  1.92155E+05 0.00146  1.38187E+05 0.00179  9.95255E+04 0.00187  8.95502E+04 0.00193  8.83854E+04 0.00191  7.39309E+04 0.00184  5.01439E+04 0.00230  4.62434E+04 0.00181  4.12316E+04 0.00177  3.48857E+04 0.00202  2.75557E+04 0.00180  1.84430E+04 0.00209  6.53728E+03 0.00256 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.50291E-01 0.00052 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.44088E+18 0.00052  3.57567E+17 0.00152 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38595E-01 0.00010  1.56207E+00 0.00044 ];
INF_CAPT                  (idx, [1:   4]) = [  7.94741E-03 0.00051  3.68754E-02 0.00087 ];
INF_ABS                   (idx, [1:   4]) = [  9.78972E-03 0.00043  6.77323E-02 0.00118 ];
INF_FISS                  (idx, [1:   4]) = [  1.84230E-03 0.00042  3.08569E-02 0.00158 ];
INF_NSF                   (idx, [1:   4]) = [  4.92955E-03 0.00042  8.18655E-02 0.00161 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.67575E+00 4.7E-05  2.65306E+00 5.7E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05344E+02 6.7E-06  2.05233E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.59439E-08 0.00035  2.57712E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28805E-01 0.00011  1.49430E+00 0.00052 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43778E-01 0.00016  3.96595E-01 0.00061 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62139E-02 0.00033  9.38428E-02 0.00075 ];
INF_SCATT3                (idx, [1:   4]) = [  7.28390E-03 0.00259  2.82806E-02 0.00191 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03332E-02 0.00183 -8.78154E-03 0.00742 ];
INF_SCATT5                (idx, [1:   4]) = [  1.76417E-04 0.10855  6.57251E-03 0.00840 ];
INF_SCATT6                (idx, [1:   4]) = [  5.12033E-03 0.00363 -1.71782E-02 0.00254 ];
INF_SCATT7                (idx, [1:   4]) = [  7.66086E-04 0.01942  3.51259E-04 0.09474 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28848E-01 0.00011  1.49430E+00 0.00052 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43779E-01 0.00016  3.96595E-01 0.00061 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62143E-02 0.00033  9.38428E-02 0.00075 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.28394E-03 0.00259  2.82806E-02 0.00191 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03332E-02 0.00183 -8.78154E-03 0.00742 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.76571E-04 0.10862  6.57251E-03 0.00840 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.12047E-03 0.00363 -1.71782E-02 0.00254 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.66036E-04 0.01941  3.51259E-04 0.09474 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12624E-01 0.00026  1.01311E+00 0.00034 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56771E+00 0.00026  3.29022E-01 0.00034 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.74613E-03 0.00043  6.77323E-02 0.00118 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69851E-02 0.00021  6.89594E-02 0.00123 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11610E-01 0.00010  1.71943E-02 0.00038  1.18761E-03 0.00430  1.49311E+00 0.00052 ];
INF_S1                    (idx, [1:   8]) = [  2.38807E-01 0.00016  4.97128E-03 0.00070  5.13666E-04 0.00783  3.96081E-01 0.00061 ];
INF_S2                    (idx, [1:   8]) = [  9.77679E-02 0.00032 -1.55396E-03 0.00206  2.80339E-04 0.00821  9.35625E-02 0.00075 ];
INF_S3                    (idx, [1:   8]) = [  9.06333E-03 0.00208 -1.77943E-03 0.00162  1.01800E-04 0.02009  2.81788E-02 0.00191 ];
INF_S4                    (idx, [1:   8]) = [ -9.77383E-03 0.00189 -5.59384E-04 0.00462  2.69497E-06 0.69614 -8.78424E-03 0.00738 ];
INF_S5                    (idx, [1:   8]) = [  1.33376E-04 0.14507  4.30416E-05 0.04325 -4.13336E-05 0.04285  6.61385E-03 0.00828 ];
INF_S6                    (idx, [1:   8]) = [  5.24999E-03 0.00347 -1.29656E-04 0.01326 -5.28422E-05 0.02963 -1.71254E-02 0.00253 ];
INF_S7                    (idx, [1:   8]) = [  9.31437E-04 0.01669 -1.65351E-04 0.01229 -4.83081E-05 0.02570  3.99567E-04 0.08429 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11654E-01 0.00010  1.71943E-02 0.00038  1.18761E-03 0.00430  1.49311E+00 0.00052 ];
INF_SP1                   (idx, [1:   8]) = [  2.38807E-01 0.00016  4.97128E-03 0.00070  5.13666E-04 0.00783  3.96081E-01 0.00061 ];
INF_SP2                   (idx, [1:   8]) = [  9.77682E-02 0.00032 -1.55396E-03 0.00206  2.80339E-04 0.00821  9.35625E-02 0.00075 ];
INF_SP3                   (idx, [1:   8]) = [  9.06336E-03 0.00207 -1.77943E-03 0.00162  1.01800E-04 0.02009  2.81788E-02 0.00191 ];
INF_SP4                   (idx, [1:   8]) = [ -9.77379E-03 0.00189 -5.59384E-04 0.00462  2.69497E-06 0.69614 -8.78424E-03 0.00738 ];
INF_SP5                   (idx, [1:   8]) = [  1.33530E-04 0.14512  4.30416E-05 0.04325 -4.13336E-05 0.04285  6.61385E-03 0.00828 ];
INF_SP6                   (idx, [1:   8]) = [  5.25013E-03 0.00348 -1.29656E-04 0.01326 -5.28422E-05 0.02963 -1.71254E-02 0.00253 ];
INF_SP7                   (idx, [1:   8]) = [  9.31387E-04 0.01668 -1.65351E-04 0.01229 -4.83081E-05 0.02570  3.99567E-04 0.08429 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31701E-01 0.00064  1.20915E+00 0.00661 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33411E-01 0.00078  1.32165E+00 0.01051 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33117E-01 0.00102  1.30814E+00 0.00711 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28646E-01 0.00086  1.04293E+00 0.00619 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43865E+00 0.00064  2.75965E-01 0.00660 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42812E+00 0.00078  2.52863E-01 0.01024 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42994E+00 0.00102  2.55127E-01 0.00719 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45789E+00 0.00086  3.19904E-01 0.00614 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.36370E-03 0.00903  1.38586E-04 0.05488  9.40524E-04 0.02190  8.59919E-04 0.02421  2.36004E-03 0.01386  8.17858E-04 0.02357  2.46771E-04 0.04076 ];
LAMBDA                    (idx, [1:  14]) = [  7.66784E-01 0.02174  1.25158E-02 0.00061  3.09510E-02 0.00067  1.10543E-01 0.00071  3.22603E-01 0.00045  1.29686E+00 0.00255  8.69768E+00 0.00821 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0008' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:50:49 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:22:26 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299849 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.96128E-01  9.96988E-01  1.00276E+00  1.00117E+00  1.00296E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15967E-02 0.00117  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88403E-01 1.4E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06231E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06663E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67583E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.45043E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.44955E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.26701E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.14140E-01 0.00118  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001193 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00024E+04 0.00089 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00024E+04 0.00089 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.55753E+02 ;
RUNNING_TIME              (idx, 1)        =  9.16140E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.62450E-01  4.62450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.89450E-01  1.01167E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.00486E+01  3.04358E+00  2.39255E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  8.05600E-01  2.45167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.99000E-02  8.50002E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.16138E+01  1.13743E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97471 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00078E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79751E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.52988E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.72261E+04 ;
TOT_SF_RATE               (idx, 1)        =  5.48088E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.90379E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.33212E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62605E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.58936E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.67556E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.76176E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.88758E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.30606E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.87980E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.93115E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.02556E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.36919E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.49733E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.53740E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.91839E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.68438E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.29545E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.29686E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.49468E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.28960E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.96522E+12 0.00053  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 16 ;
BURNUP                     (idx, [1:  2])  = [  2.00000E+01  2.00018E+01 ];
BURN_DAYS                 (idx, 1)        =  5.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.62728E-01 0.00114 ];
U235_FISS                 (idx, [1:   4]) = [  6.17276E+15 0.00110  4.51431E-01 0.00086 ];
U238_FISS                 (idx, [1:   4]) = [  1.09304E+15 0.00279  7.99257E-02 0.00259 ];
PU239_FISS                (idx, [1:   4]) = [  5.38916E+15 0.00110  3.94140E-01 0.00096 ];
PU240_FISS                (idx, [1:   4]) = [  9.51907E+12 0.03039  6.96361E-04 0.03037 ];
PU241_FISS                (idx, [1:   4]) = [  9.90889E+14 0.00264  7.24711E-02 0.00260 ];
U235_CAPT                 (idx, [1:   4]) = [  1.38875E+15 0.00253  5.40482E-02 0.00248 ];
U238_CAPT                 (idx, [1:   4]) = [  9.39509E+15 0.00111  3.65609E-01 0.00078 ];
PU239_CAPT                (idx, [1:   4]) = [  2.98396E+15 0.00154  1.16144E-01 0.00160 ];
PU240_CAPT                (idx, [1:   4]) = [  1.99489E+15 0.00213  7.76279E-02 0.00194 ];
PU241_CAPT                (idx, [1:   4]) = [  3.55260E+14 0.00468  1.38285E-02 0.00472 ];
XE135_CAPT                (idx, [1:   4]) = [  7.60853E+14 0.00320  2.96103E-02 0.00314 ];
SM149_CAPT                (idx, [1:   4]) = [  2.28098E+14 0.00579  8.87843E-03 0.00582 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001193 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.20562E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001193 5.00821E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3235432 3.24012E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1721970 1.72429E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 43791 4.37957E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001193 5.00821E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.35276E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 6.3E-09  4.50056E+05 6.3E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.65669E+16 2.3E-05  3.65669E+16 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36683E+16 4.6E-06  1.36683E+16 4.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.56918E+16 0.00046  1.97460E+16 0.00048  5.94571E+15 0.00126 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.93601E+16 0.00030  3.34144E+16 0.00029  5.94571E+15 0.00126 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.96522E+16 0.00053  3.96522E+16 0.00053  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.84565E+18 0.00051  4.91389E+17 0.00050  1.35426E+18 0.00057 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.47342E+14 0.00523 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.97074E+16 0.00031 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.36984E+18 0.00067 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10191E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10191E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.51570E+00 0.00054 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.85784E-01 0.00035 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.29754E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24081E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94289E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96935E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.30593E-01 0.00060 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.22441E-01 0.00060 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.67530E+00 2.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05514E+02 4.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.22381E-01 0.00061  9.17617E-01 0.00061  4.82406E-03 0.01026 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.22444E-01 0.00031 ];
COL_KEFF                  (idx, [1:   2]) = [  9.22319E-01 0.00053 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.22444E-01 0.00031 ];
ABS_KINF                  (idx, [1:   2]) = [  9.30596E-01 0.00030 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70718E+01 0.00023 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70676E+01 9.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.73500E-07 0.00389 ];
IMP_EALF                  (idx, [1:   2]) = [  7.74410E-07 0.00168 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.72174E-01 0.00289 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.73558E-01 0.00119 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.79481E-03 0.00616  1.49258E-04 0.03753  1.00843E-03 0.01503  8.92001E-04 0.01504  2.55390E-03 0.00955  9.18282E-04 0.01480  2.72937E-04 0.02871 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.70648E-01 0.01486  9.45554E-03 0.02558  3.08704E-02 0.00048  1.10688E-01 0.00055  3.22642E-01 0.00034  1.29345E+00 0.00180  7.74543E+00 0.01610 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.21706E-03 0.00977  1.38635E-04 0.05478  9.19872E-04 0.02291  7.83723E-04 0.02313  2.30538E-03 0.01570  8.19719E-04 0.02216  2.49729E-04 0.04555 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.76018E-01 0.02318  1.25300E-02 0.00077  3.08428E-02 0.00068  1.10712E-01 0.00078  3.22797E-01 0.00052  1.29299E+00 0.00258  8.48615E+00 0.00998 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.65333E-05 0.00141  2.65225E-05 0.00142  2.84527E-05 0.01653 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.44688E-05 0.00125  2.44589E-05 0.00126  2.62394E-05 0.01652 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.24079E-03 0.01029  1.40235E-04 0.06248  9.33368E-04 0.02470  7.77054E-04 0.02723  2.32033E-03 0.01632  8.15688E-04 0.02487  2.54116E-04 0.04694 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.80852E-01 0.02624  1.25226E-02 0.00099  3.08685E-02 0.00085  1.10810E-01 0.00103  3.22538E-01 0.00061  1.29629E+00 0.00304  8.41263E+00 0.01388 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.58642E-05 0.00339  2.58578E-05 0.00340  2.50459E-05 0.03955 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.38499E-05 0.00328  2.38439E-05 0.00329  2.30993E-05 0.03949 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.41174E-03 0.03640  1.63972E-04 0.24048  8.93206E-04 0.09359  9.11284E-04 0.08525  2.23749E-03 0.05367  8.83160E-04 0.08259  3.22626E-04 0.16885 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.19304E-01 0.08143  1.24890E-02 5.6E-05  3.09185E-02 0.00209  1.10540E-01 0.00199  3.22890E-01 0.00164  1.29194E+00 0.00731  8.48052E+00 0.02934 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.39235E-03 0.03526  1.55907E-04 0.23484  8.73903E-04 0.09195  9.08588E-04 0.08268  2.24682E-03 0.05252  9.02771E-04 0.08164  3.04351E-04 0.16013 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.18342E-01 0.07985  1.24890E-02 5.6E-05  3.09115E-02 0.00209  1.10524E-01 0.00197  3.22956E-01 0.00165  1.29066E+00 0.00739  8.46101E+00 0.02956 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.10105E+02 0.03627 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.62084E-05 0.00084 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.41698E-05 0.00060 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.30403E-03 0.00684 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.02470E+02 0.00691 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.56678E-07 0.00083 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.68191E-06 0.00044  2.68174E-06 0.00044  2.71385E-06 0.00591 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.79871E-05 0.00099  3.80039E-05 0.00099  3.49093E-05 0.01110 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.27767E-01 0.00034  6.27971E-01 0.00035  6.05101E-01 0.01018 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07333E+01 0.01449 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.44955E+01 0.00052  3.29810E+01 0.00051 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.93382E+04 0.00282  3.03115E+05 0.00210  6.09949E+05 0.00087  6.51475E+05 0.00074  5.99196E+05 0.00054  6.41672E+05 0.00069  4.36190E+05 0.00045  3.85610E+05 0.00057  2.95245E+05 0.00057  2.41309E+05 0.00091  2.08047E+05 0.00080  1.87399E+05 0.00098  1.73214E+05 0.00083  1.64370E+05 0.00079  1.60486E+05 0.00106  1.38693E+05 0.00080  1.36702E+05 0.00103  1.35971E+05 0.00073  1.33451E+05 0.00071  2.60514E+05 0.00071  2.51164E+05 0.00065  1.81220E+05 0.00086  1.17259E+05 0.00084  1.35378E+05 0.00093  1.28902E+05 0.00089  1.14612E+05 0.00097  1.85347E+05 0.00063  4.29486E+04 0.00215  5.35477E+04 0.00146  4.85482E+04 0.00184  2.84196E+04 0.00195  4.90044E+04 0.00163  3.25612E+04 0.00163  2.65679E+04 0.00237  4.56899E+03 0.00271  3.95447E+03 0.00386  3.47162E+03 0.00413  3.30448E+03 0.00419  3.39065E+03 0.00368  3.74713E+03 0.00402  4.45783E+03 0.00359  4.52796E+03 0.00379  8.92886E+03 0.00237  1.47043E+04 0.00163  1.91704E+04 0.00212  5.10538E+04 0.00132  5.36027E+04 0.00159  5.70489E+04 0.00115  3.68511E+04 0.00125  2.59468E+04 0.00178  1.92738E+04 0.00149  2.25638E+04 0.00180  4.40865E+04 0.00165  6.22506E+04 0.00187  1.24367E+05 0.00181  1.96280E+05 0.00178  2.97619E+05 0.00195  1.91752E+05 0.00187  1.37928E+05 0.00198  9.95133E+04 0.00222  8.96256E+04 0.00224  8.84040E+04 0.00215  7.39996E+04 0.00250  5.01177E+04 0.00222  4.63895E+04 0.00235  4.13532E+04 0.00233  3.49714E+04 0.00213  2.76107E+04 0.00253  1.85267E+04 0.00258  6.57699E+03 0.00301 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.30469E-01 0.00037 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.47956E+18 0.00041  3.66120E+17 0.00171 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38827E-01 0.00014  1.56405E+00 0.00052 ];
INF_CAPT                  (idx, [1:   4]) = [  8.10008E-03 0.00064  3.74438E-02 0.00095 ];
INF_ABS                   (idx, [1:   4]) = [  9.88647E-03 0.00055  6.75647E-02 0.00126 ];
INF_FISS                  (idx, [1:   4]) = [  1.78639E-03 0.00043  3.01209E-02 0.00166 ];
INF_NSF                   (idx, [1:   4]) = [  4.80489E-03 0.00046  8.04785E-02 0.00171 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.68973E+00 7.3E-05  2.67184E+00 6.8E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05538E+02 8.7E-06  2.05508E+02 1.3E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.57230E-08 0.00043  2.58129E-06 0.00022 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28947E-01 0.00014  1.49644E+00 0.00060 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43770E-01 0.00026  3.96898E-01 0.00064 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61796E-02 0.00038  9.37437E-02 0.00074 ];
INF_SCATT3                (idx, [1:   4]) = [  7.28712E-03 0.00328  2.80842E-02 0.00221 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03352E-02 0.00207 -8.86727E-03 0.00639 ];
INF_SCATT5                (idx, [1:   4]) = [  1.65274E-04 0.08375  6.67009E-03 0.01135 ];
INF_SCATT6                (idx, [1:   4]) = [  5.11583E-03 0.00275 -1.71960E-02 0.00342 ];
INF_SCATT7                (idx, [1:   4]) = [  7.82143E-04 0.02065  4.74292E-04 0.10171 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28991E-01 0.00014  1.49644E+00 0.00060 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43770E-01 0.00026  3.96898E-01 0.00064 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61795E-02 0.00038  9.37437E-02 0.00074 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.28704E-03 0.00327  2.80842E-02 0.00221 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03353E-02 0.00207 -8.86727E-03 0.00639 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.65278E-04 0.08402  6.67009E-03 0.01135 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.11596E-03 0.00275 -1.71960E-02 0.00342 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.82175E-04 0.02068  4.74292E-04 0.10171 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12637E-01 0.00028  1.01506E+00 0.00046 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56762E+00 0.00028  3.28391E-01 0.00046 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.84249E-03 0.00056  6.75647E-02 0.00126 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69851E-02 0.00021  6.88005E-02 0.00147 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11842E-01 0.00014  1.71055E-02 0.00045  1.18916E-03 0.00447  1.49525E+00 0.00060 ];
INF_S1                    (idx, [1:   8]) = [  2.38826E-01 0.00025  4.94382E-03 0.00101  5.08031E-04 0.00682  3.96390E-01 0.00064 ];
INF_S2                    (idx, [1:   8]) = [  9.77249E-02 0.00037 -1.54528E-03 0.00248  2.80120E-04 0.00908  9.34635E-02 0.00075 ];
INF_S3                    (idx, [1:   8]) = [  9.04981E-03 0.00277 -1.76268E-03 0.00199  9.78517E-05 0.02572  2.79864E-02 0.00222 ];
INF_S4                    (idx, [1:   8]) = [ -9.78326E-03 0.00215 -5.51967E-04 0.00544  2.31392E-06 0.68533 -8.86959E-03 0.00639 ];
INF_S5                    (idx, [1:   8]) = [  1.28015E-04 0.10316  3.72587E-05 0.06660 -4.00338E-05 0.04742  6.71013E-03 0.01120 ];
INF_S6                    (idx, [1:   8]) = [  5.25113E-03 0.00278 -1.35297E-04 0.01434 -5.04078E-05 0.02282 -1.71456E-02 0.00343 ];
INF_S7                    (idx, [1:   8]) = [  9.49694E-04 0.01733 -1.67552E-04 0.01060 -4.48256E-05 0.02845  5.19118E-04 0.09307 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11886E-01 0.00014  1.71055E-02 0.00045  1.18916E-03 0.00447  1.49525E+00 0.00060 ];
INF_SP1                   (idx, [1:   8]) = [  2.38826E-01 0.00025  4.94382E-03 0.00101  5.08031E-04 0.00682  3.96390E-01 0.00064 ];
INF_SP2                   (idx, [1:   8]) = [  9.77248E-02 0.00037 -1.54528E-03 0.00248  2.80120E-04 0.00908  9.34635E-02 0.00075 ];
INF_SP3                   (idx, [1:   8]) = [  9.04973E-03 0.00277 -1.76268E-03 0.00199  9.78517E-05 0.02572  2.79864E-02 0.00222 ];
INF_SP4                   (idx, [1:   8]) = [ -9.78333E-03 0.00215 -5.51967E-04 0.00544  2.31392E-06 0.68533 -8.86959E-03 0.00639 ];
INF_SP5                   (idx, [1:   8]) = [  1.28019E-04 0.10351  3.72587E-05 0.06660 -4.00338E-05 0.04742  6.71013E-03 0.01120 ];
INF_SP6                   (idx, [1:   8]) = [  5.25126E-03 0.00278 -1.35297E-04 0.01434 -5.04078E-05 0.02282 -1.71456E-02 0.00343 ];
INF_SP7                   (idx, [1:   8]) = [  9.49727E-04 0.01736 -1.67552E-04 0.01060 -4.48256E-05 0.02845  5.19118E-04 0.09307 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31902E-01 0.00071  1.21815E+00 0.00613 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33769E-01 0.00087  1.32231E+00 0.00855 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33689E-01 0.00079  1.32569E+00 0.00840 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28340E-01 0.00122  1.05178E+00 0.00657 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43741E+00 0.00071  2.73889E-01 0.00621 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42594E+00 0.00087  2.52534E-01 0.00871 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42642E+00 0.00079  2.51880E-01 0.00867 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45986E+00 0.00122  3.17252E-01 0.00657 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.21706E-03 0.00977  1.38635E-04 0.05478  9.19872E-04 0.02291  7.83723E-04 0.02313  2.30538E-03 0.01570  8.19719E-04 0.02216  2.49729E-04 0.04555 ];
LAMBDA                    (idx, [1:  14]) = [  7.76018E-01 0.02318  1.25300E-02 0.00077  3.08428E-02 0.00068  1.10712E-01 0.00078  3.22797E-01 0.00052  1.29299E+00 0.00258  8.48615E+00 0.00998 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0008' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:50:49 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:27:55 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299849 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.95834E-01  9.96306E-01  1.00316E+00  9.99542E-01  1.00516E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16120E-02 0.00106  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88388E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06363E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06795E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68023E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.45542E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.45453E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.27186E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.15420E-01 0.00107  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000789 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00016E+04 0.00092 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00016E+04 0.00092 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.83091E+02 ;
RUNNING_TIME              (idx, 1)        =  9.70877E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.62450E-01  4.62450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.10117E-01  1.01167E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.54512E+01  3.04840E+00  2.35420E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  8.55600E-01  2.43500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.23000E-02  7.49997E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.70876E+01  1.13488E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97582 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00002E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80029E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.53839E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68271E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.20567E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.98025E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.38925E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.55811E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.54375E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  3.66529E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.87285E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.82301E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.81163E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  8.42282E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.99169E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.42038E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.33598E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.49764E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.51667E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.37215E+13 ;
CS137_ACTIVITY            (idx, 1)        =  3.34658E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.30607E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.24303E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.01694E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.30401E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.17046E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 17 ;
BURNUP                     (idx, [1:  2])  = [  2.50000E+01  2.50023E+01 ];
BURN_DAYS                 (idx, 1)        =  6.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.05398E-01 0.00105 ];
U235_FISS                 (idx, [1:   4]) = [  5.19701E+15 0.00121  3.80607E-01 0.00104 ];
U238_FISS                 (idx, [1:   4]) = [  1.15033E+15 0.00271  8.42377E-02 0.00257 ];
PU239_FISS                (idx, [1:   4]) = [  5.94173E+15 0.00110  4.35146E-01 0.00090 ];
PU240_FISS                (idx, [1:   4]) = [  1.24129E+13 0.02465  9.08541E-04 0.02463 ];
PU241_FISS                (idx, [1:   4]) = [  1.32829E+15 0.00250  9.72731E-02 0.00238 ];
U235_CAPT                 (idx, [1:   4]) = [  1.16426E+15 0.00266  4.19632E-02 0.00261 ];
U238_CAPT                 (idx, [1:   4]) = [  9.80587E+15 0.00103  3.53412E-01 0.00075 ];
PU239_CAPT                (idx, [1:   4]) = [  3.27578E+15 0.00155  1.18075E-01 0.00153 ];
PU240_CAPT                (idx, [1:   4]) = [  2.38210E+15 0.00190  8.58531E-02 0.00177 ];
PU241_CAPT                (idx, [1:   4]) = [  4.78406E+14 0.00411  1.72441E-02 0.00410 ];
XE135_CAPT                (idx, [1:   4]) = [  7.72772E+14 0.00334  2.78538E-02 0.00332 ];
SM149_CAPT                (idx, [1:   4]) = [  2.40101E+14 0.00579  8.65497E-03 0.00581 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000789 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.11899E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000789 5.00812E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3321429 3.32642E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1634849 1.63718E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44511 4.45247E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000789 5.00812E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.79865E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 6.3E-09  4.50056E+05 6.3E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.69205E+16 2.2E-05  3.69205E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36370E+16 4.6E-06  1.36370E+16 4.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.77541E+16 0.00047  2.14728E+16 0.00047  6.28132E+15 0.00132 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.13912E+16 0.00031  3.51098E+16 0.00029  6.28132E+15 0.00132 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.17046E+16 0.00052  4.17046E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.94012E+18 0.00051  5.15278E+17 0.00049  1.42484E+18 0.00058 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.71433E+14 0.00525 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.17626E+16 0.00032 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.44283E+18 0.00070 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.09613E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.09613E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.47824E+00 0.00060 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.82418E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.23718E-01 0.00037 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24001E+00 0.00039 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94178E-01 3.7E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96899E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.94451E-01 0.00064 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.86486E-01 0.00065 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.70737E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05986E+02 4.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.86441E-01 0.00065  8.81991E-01 0.00065  4.49523E-03 0.01118 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.85534E-01 0.00033 ];
COL_KEFF                  (idx, [1:   2]) = [  8.85404E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.85534E-01 0.00033 ];
ABS_KINF                  (idx, [1:   2]) = [  8.93491E-01 0.00031 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70002E+01 0.00024 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70039E+01 9.5E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  8.31358E-07 0.00416 ];
IMP_EALF                  (idx, [1:   2]) = [  8.25335E-07 0.00162 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.87829E-01 0.00282 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.87195E-01 0.00113 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.91260E-03 0.00653  1.54692E-04 0.03821  1.05033E-03 0.01522  9.40234E-04 0.01552  2.54554E-03 0.00952  9.43246E-04 0.01639  2.78565E-04 0.02942 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.62037E-01 0.01565  9.47867E-03 0.02559  3.07286E-02 0.00046  1.10957E-01 0.00056  3.22663E-01 0.00034  1.27217E+00 0.00221  7.69303E+00 0.01676 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.18192E-03 0.00967  1.38230E-04 0.05705  8.99465E-04 0.02346  8.22922E-04 0.02355  2.24150E-03 0.01462  8.43966E-04 0.02447  2.35842E-04 0.04326 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.47630E-01 0.02133  1.25732E-02 0.00113  3.07427E-02 0.00067  1.11045E-01 0.00077  3.22627E-01 0.00051  1.27649E+00 0.00292  8.44194E+00 0.01109 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.78236E-05 0.00143  2.78134E-05 0.00144  3.00219E-05 0.01755 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.46594E-05 0.00131  2.46504E-05 0.00132  2.66032E-05 0.01749 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.06334E-03 0.01122  1.27269E-04 0.07127  8.81857E-04 0.02815  8.02196E-04 0.02632  2.20027E-03 0.01692  8.02635E-04 0.02783  2.49108E-04 0.04888 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.83847E-01 0.02693  1.25666E-02 0.00168  3.07374E-02 0.00088  1.10976E-01 0.00105  3.22705E-01 0.00066  1.27957E+00 0.00374  8.50363E+00 0.01488 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.70395E-05 0.00368  2.70310E-05 0.00369  2.56827E-05 0.04132 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.39621E-05 0.00358  2.39544E-05 0.00359  2.27670E-05 0.04133 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.36101E-03 0.03985  1.55760E-04 0.21113  8.53273E-04 0.08981  8.65050E-04 0.08408  2.32628E-03 0.06083  8.47044E-04 0.09619  3.13595E-04 0.15509 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  9.04643E-01 0.08373  1.25172E-02 0.00234  3.05605E-02 0.00199  1.11407E-01 0.00246  3.24021E-01 0.00189  1.28086E+00 0.00896  8.48622E+00 0.03254 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.33855E-03 0.03912  1.49982E-04 0.20772  8.40364E-04 0.08713  8.61390E-04 0.08285  2.34155E-03 0.05963  8.37153E-04 0.09652  3.08118E-04 0.14962 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  9.06393E-01 0.08391  1.25172E-02 0.00234  3.05579E-02 0.00198  1.11430E-01 0.00246  3.23972E-01 0.00189  1.28064E+00 0.00895  8.48482E+00 0.03254 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.99626E+02 0.04006 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.74583E-05 0.00095 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.43349E-05 0.00069 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.16891E-03 0.00745 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.88395E+02 0.00760 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.58318E-07 0.00084 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.65291E-06 0.00046  2.65280E-06 0.00046  2.67277E-06 0.00600 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.84990E-05 0.00102  3.85137E-05 0.00102  3.57935E-05 0.01183 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.21730E-01 0.00037  6.22055E-01 0.00037  5.80201E-01 0.01050 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07873E+01 0.01449 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.45453E+01 0.00054  3.29834E+01 0.00054 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.00889E+04 0.00317  3.04916E+05 0.00187  6.10740E+05 0.00101  6.51157E+05 0.00058  5.98317E+05 0.00063  6.41228E+05 0.00083  4.35210E+05 0.00061  3.85586E+05 0.00070  2.95055E+05 0.00075  2.41092E+05 0.00087  2.07787E+05 0.00108  1.87326E+05 0.00075  1.73088E+05 0.00095  1.64646E+05 0.00090  1.60519E+05 0.00079  1.38647E+05 0.00095  1.36973E+05 0.00094  1.35469E+05 0.00097  1.33274E+05 0.00072  2.60040E+05 0.00064  2.51636E+05 0.00057  1.81003E+05 0.00093  1.17354E+05 0.00109  1.35182E+05 0.00102  1.28536E+05 0.00076  1.14208E+05 0.00095  1.84793E+05 0.00059  4.28368E+04 0.00150  5.29865E+04 0.00164  4.83454E+04 0.00162  2.82847E+04 0.00142  4.88006E+04 0.00144  3.22880E+04 0.00184  2.60924E+04 0.00200  4.44132E+03 0.00297  3.78575E+03 0.00313  3.31002E+03 0.00243  3.16766E+03 0.00426  3.20974E+03 0.00387  3.52876E+03 0.00393  4.23573E+03 0.00363  4.37948E+03 0.00292  8.69198E+03 0.00213  1.43937E+04 0.00236  1.87683E+04 0.00202  5.02846E+04 0.00142  5.27819E+04 0.00162  5.62466E+04 0.00130  3.64192E+04 0.00157  2.56037E+04 0.00152  1.89406E+04 0.00184  2.23481E+04 0.00134  4.37533E+04 0.00161  6.17836E+04 0.00171  1.23779E+05 0.00146  1.96175E+05 0.00169  2.98321E+05 0.00185  1.92672E+05 0.00198  1.38736E+05 0.00209  1.00116E+05 0.00205  9.01818E+04 0.00206  8.91029E+04 0.00209  7.45254E+04 0.00218  5.05363E+04 0.00218  4.66702E+04 0.00202  4.15952E+04 0.00201  3.52505E+04 0.00203  2.77716E+04 0.00221  1.86381E+04 0.00217  6.62346E+03 0.00264 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.93359E-01 0.00047 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.55468E+18 0.00044  3.85472E+17 0.00182 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39217E-01 0.00013  1.56831E+00 0.00051 ];
INF_CAPT                  (idx, [1:   4]) = [  8.35796E-03 0.00061  3.82962E-02 0.00105 ];
INF_ABS                   (idx, [1:   4]) = [  1.00469E-02 0.00051  6.68687E-02 0.00138 ];
INF_FISS                  (idx, [1:   4]) = [  1.68893E-03 0.00035  2.85725E-02 0.00181 ];
INF_NSF                   (idx, [1:   4]) = [  4.58583E-03 0.00036  7.73029E-02 0.00186 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.71523E+00 5.6E-05  2.70549E+00 6.2E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05900E+02 9.4E-06  2.06006E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.52674E-08 0.00040  2.58742E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29169E-01 0.00014  1.50142E+00 0.00060 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43946E-01 0.00025  3.98162E-01 0.00063 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62411E-02 0.00031  9.39940E-02 0.00072 ];
INF_SCATT3                (idx, [1:   4]) = [  7.24907E-03 0.00362  2.82149E-02 0.00227 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03569E-02 0.00194 -8.92189E-03 0.00664 ];
INF_SCATT5                (idx, [1:   4]) = [  1.45263E-04 0.10141  6.62725E-03 0.00643 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10681E-03 0.00213 -1.72598E-02 0.00284 ];
INF_SCATT7                (idx, [1:   4]) = [  7.22496E-04 0.01697  5.04282E-04 0.10123 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29213E-01 0.00014  1.50142E+00 0.00060 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43946E-01 0.00025  3.98162E-01 0.00063 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62415E-02 0.00031  9.39940E-02 0.00072 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.24904E-03 0.00362  2.82149E-02 0.00227 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03568E-02 0.00194 -8.92189E-03 0.00664 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.45226E-04 0.10168  6.62725E-03 0.00643 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10678E-03 0.00214 -1.72598E-02 0.00284 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.22477E-04 0.01708  5.04282E-04 0.10123 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12487E-01 0.00030  1.01886E+00 0.00049 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56872E+00 0.00030  3.27165E-01 0.00049 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.00033E-02 0.00052  6.68687E-02 0.00138 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70008E-02 0.00028  6.80462E-02 0.00146 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12216E-01 0.00013  1.69532E-02 0.00056  1.16252E-03 0.00461  1.50026E+00 0.00060 ];
INF_S1                    (idx, [1:   8]) = [  2.39045E-01 0.00024  4.90043E-03 0.00115  4.98735E-04 0.00966  3.97663E-01 0.00063 ];
INF_S2                    (idx, [1:   8]) = [  9.77851E-02 0.00029 -1.54393E-03 0.00280  2.73656E-04 0.01229  9.37203E-02 0.00071 ];
INF_S3                    (idx, [1:   8]) = [  9.00211E-03 0.00300 -1.75304E-03 0.00200  9.82767E-05 0.02823  2.81166E-02 0.00226 ];
INF_S4                    (idx, [1:   8]) = [ -9.81373E-03 0.00203 -5.43211E-04 0.00529  1.65370E-06 1.00000 -8.92354E-03 0.00660 ];
INF_S5                    (idx, [1:   8]) = [  1.06675E-04 0.12981  3.85877E-05 0.05862 -3.67376E-05 0.03920  6.66399E-03 0.00633 ];
INF_S6                    (idx, [1:   8]) = [  5.24020E-03 0.00200 -1.33390E-04 0.01989 -4.84570E-05 0.02706 -1.72114E-02 0.00286 ];
INF_S7                    (idx, [1:   8]) = [  8.88155E-04 0.01332 -1.65659E-04 0.01315 -4.49244E-05 0.02954  5.49207E-04 0.09262 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12260E-01 0.00013  1.69532E-02 0.00056  1.16252E-03 0.00461  1.50026E+00 0.00060 ];
INF_SP1                   (idx, [1:   8]) = [  2.39046E-01 0.00024  4.90043E-03 0.00115  4.98735E-04 0.00966  3.97663E-01 0.00063 ];
INF_SP2                   (idx, [1:   8]) = [  9.77855E-02 0.00029 -1.54393E-03 0.00280  2.73656E-04 0.01229  9.37203E-02 0.00071 ];
INF_SP3                   (idx, [1:   8]) = [  9.00208E-03 0.00300 -1.75304E-03 0.00200  9.82767E-05 0.02823  2.81166E-02 0.00226 ];
INF_SP4                   (idx, [1:   8]) = [ -9.81364E-03 0.00203 -5.43211E-04 0.00529  1.65370E-06 1.00000 -8.92354E-03 0.00660 ];
INF_SP5                   (idx, [1:   8]) = [  1.06638E-04 0.13015  3.85877E-05 0.05862 -3.67376E-05 0.03920  6.66399E-03 0.00633 ];
INF_SP6                   (idx, [1:   8]) = [  5.24017E-03 0.00201 -1.33390E-04 0.01989 -4.84570E-05 0.02706 -1.72114E-02 0.00286 ];
INF_SP7                   (idx, [1:   8]) = [  8.88137E-04 0.01341 -1.65659E-04 0.01315 -4.49244E-05 0.02954  5.49207E-04 0.09262 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31655E-01 0.00053  1.22235E+00 0.00666 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33450E-01 0.00072  1.32102E+00 0.00753 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33274E-01 0.00089  1.35225E+00 0.01101 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28320E-01 0.00087  1.04549E+00 0.00570 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43893E+00 0.00053  2.72985E-01 0.00653 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42788E+00 0.00072  2.52676E-01 0.00756 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42896E+00 0.00089  2.47204E-01 0.01077 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45996E+00 0.00087  3.19073E-01 0.00556 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.18192E-03 0.00967  1.38230E-04 0.05705  8.99465E-04 0.02346  8.22922E-04 0.02355  2.24150E-03 0.01462  8.43966E-04 0.02447  2.35842E-04 0.04326 ];
LAMBDA                    (idx, [1:  14]) = [  7.47630E-01 0.02133  1.25732E-02 0.00113  3.07427E-02 0.00067  1.11045E-01 0.00077  3.22627E-01 0.00051  1.27649E+00 0.00292  8.44194E+00 0.01109 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0008' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:50:49 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:33:25 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299849 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.94144E-01  9.97629E-01  1.00342E+00  9.99771E-01  1.00504E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16381E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88362E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06389E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06821E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68407E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.46153E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.46063E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.28525E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.17354E-01 0.00111  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001060 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00021E+04 0.00093 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00021E+04 0.00093 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.10589E+02 ;
RUNNING_TIME              (idx, 1)        =  1.02594E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.62450E-01  4.62450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.31100E-01  1.01500E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.00886E+02  3.06130E+00  2.37348E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  9.05383E-01  2.37333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.47833E-02  7.49997E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.02593E+02  1.13573E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97681 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99887E+00 0.00022 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80277E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.65955E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.67937E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.26659E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.08171E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.46523E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.57781E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.53281E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  4.86932E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.02924E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.97796E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.46017E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  8.91359E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.08322E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.78272E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.37110E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.54079E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.56396E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.02644E+13 ;
CS137_ACTIVITY            (idx, 1)        =  4.00462E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.41726E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.21248E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.61674E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.33330E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.36869E+12 0.00053  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 18 ;
BURNUP                     (idx, [1:  2])  = [  3.00000E+01  3.00028E+01 ];
BURN_DAYS                 (idx, 1)        =  7.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.47602E-01 0.00102 ];
U235_FISS                 (idx, [1:   4]) = [  4.33337E+15 0.00138  3.18354E-01 0.00120 ];
U238_FISS                 (idx, [1:   4]) = [  1.19425E+15 0.00265  8.77221E-02 0.00243 ];
PU239_FISS                (idx, [1:   4]) = [  6.38094E+15 0.00110  4.68785E-01 0.00088 ];
PU240_FISS                (idx, [1:   4]) = [  1.58296E+13 0.02328  1.16274E-03 0.02322 ];
PU241_FISS                (idx, [1:   4]) = [  1.65424E+15 0.00226  1.21524E-01 0.00212 ];
U235_CAPT                 (idx, [1:   4]) = [  9.68940E+14 0.00302  3.25676E-02 0.00298 ];
U238_CAPT                 (idx, [1:   4]) = [  1.02303E+16 0.00105  3.43832E-01 0.00076 ];
PU239_CAPT                (idx, [1:   4]) = [  3.50998E+15 0.00150  1.17982E-01 0.00149 ];
PU240_CAPT                (idx, [1:   4]) = [  2.71828E+15 0.00188  9.13605E-02 0.00175 ];
PU241_CAPT                (idx, [1:   4]) = [  5.94323E+14 0.00369  1.99759E-02 0.00365 ];
XE135_CAPT                (idx, [1:   4]) = [  7.83350E+14 0.00337  2.63295E-02 0.00333 ];
SM149_CAPT                (idx, [1:   4]) = [  2.49910E+14 0.00557  8.39898E-03 0.00550 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001060 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.25579E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001060 5.00826E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3400187 3.40521E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1555823 1.55798E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45050 4.50676E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001060 5.00826E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -7.72998E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 6.3E-09  4.50056E+05 6.3E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.72347E+16 2.2E-05  3.72347E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36091E+16 4.5E-06  1.36091E+16 4.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.97650E+16 0.00048  2.31568E+16 0.00048  6.60822E+15 0.00121 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.33741E+16 0.00033  3.67659E+16 0.00030  6.60822E+15 0.00121 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.36869E+16 0.00053  4.36869E+16 0.00053  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.03271E+18 0.00052  5.39013E+17 0.00051  1.49370E+18 0.00057 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.93805E+14 0.00485 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.37679E+16 0.00034 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.51409E+18 0.00066 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.09036E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.09036E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.44191E+00 0.00059 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.79315E-01 0.00035 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.18796E-01 0.00037 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23736E+00 0.00038 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94132E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96836E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.60322E-01 0.00065 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.52568E-01 0.00066 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.73602E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06408E+02 4.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.52473E-01 0.00066  8.48373E-01 0.00065  4.19525E-03 0.01222 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.52181E-01 0.00034 ];
COL_KEFF                  (idx, [1:   2]) = [  8.52429E-01 0.00053 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.52181E-01 0.00034 ];
ABS_KINF                  (idx, [1:   2]) = [  8.59929E-01 0.00033 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.69510E+01 0.00024 ];
IMP_ALF                   (idx, [1:   2]) = [  1.69465E+01 0.00011 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  8.73230E-07 0.00415 ];
IMP_EALF                  (idx, [1:   2]) = [  8.74227E-07 0.00185 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.00090E-01 0.00275 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.00238E-01 0.00122 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.00171E-03 0.00641  1.47067E-04 0.04126  1.12967E-03 0.01472  9.30787E-04 0.01620  2.52349E-03 0.00928  9.72556E-04 0.01614  2.98141E-04 0.02787 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.62638E-01 0.01465  8.99791E-03 0.02836  3.06351E-02 0.00041  1.11177E-01 0.00057  3.22816E-01 0.00037  1.25849E+00 0.00301  7.50851E+00 0.01673 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.92365E-03 0.00959  1.20889E-04 0.06380  9.13934E-04 0.02383  7.54060E-04 0.02334  2.07661E-03 0.01429  8.12439E-04 0.02397  2.45717E-04 0.04503 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.66250E-01 0.02359  1.26082E-02 0.00136  3.06320E-02 0.00062  1.11113E-01 0.00083  3.22764E-01 0.00058  1.26425E+00 0.00332  8.06998E+00 0.01298 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.91491E-05 0.00159  2.91350E-05 0.00159  3.18139E-05 0.01843 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.48423E-05 0.00139  2.48304E-05 0.00139  2.70961E-05 0.01828 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.92751E-03 0.01232  1.23661E-04 0.07647  9.73949E-04 0.02616  7.15938E-04 0.02975  2.06725E-03 0.01773  8.12010E-04 0.02996  2.34710E-04 0.05210 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.53831E-01 0.02752  1.25984E-02 0.00198  3.06564E-02 0.00084  1.11077E-01 0.00116  3.22602E-01 0.00073  1.25839E+00 0.00482  8.27838E+00 0.01769 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.85403E-05 0.00381  2.85204E-05 0.00381  2.75708E-05 0.04888 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.43253E-05 0.00377  2.43084E-05 0.00377  2.34925E-05 0.04889 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.96260E-03 0.04104  1.16288E-04 0.26961  9.73381E-04 0.09052  5.61301E-04 0.11061  2.13989E-03 0.06508  8.74892E-04 0.09421  2.96850E-04 0.17493 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.50644E-01 0.08848  1.26783E-02 0.00626  3.06293E-02 0.00203  1.11741E-01 0.00311  3.23253E-01 0.00201  1.22907E+00 0.01255  8.61212E+00 0.03781 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.94907E-03 0.04040  1.29283E-04 0.26266  9.66185E-04 0.08906  5.75683E-04 0.10817  2.11928E-03 0.06392  8.68388E-04 0.09110  2.90255E-04 0.17823 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.43781E-01 0.08663  1.26783E-02 0.00626  3.06280E-02 0.00203  1.11732E-01 0.00310  3.23203E-01 0.00200  1.23014E+00 0.01246  8.61383E+00 0.03782 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.74847E+02 0.04117 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.88713E-05 0.00098 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.46064E-05 0.00069 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.90239E-03 0.00794 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.69828E+02 0.00792 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.60202E-07 0.00080 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.63115E-06 0.00044  2.63114E-06 0.00044  2.63040E-06 0.00625 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.89765E-05 0.00096  3.89953E-05 0.00096  3.53290E-05 0.01189 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.16851E-01 0.00037  6.17328E-01 0.00037  5.49584E-01 0.01042 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.09706E+01 0.01412 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.46063E+01 0.00050  3.30262E+01 0.00055 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.03778E+04 0.00322  3.05360E+05 0.00160  6.10640E+05 0.00063  6.51497E+05 0.00086  5.98740E+05 0.00096  6.41837E+05 0.00054  4.35200E+05 0.00073  3.85381E+05 0.00067  2.95195E+05 0.00076  2.40942E+05 0.00096  2.07592E+05 0.00098  1.87413E+05 0.00068  1.73316E+05 0.00067  1.64711E+05 0.00075  1.60332E+05 0.00107  1.38671E+05 0.00094  1.36741E+05 0.00083  1.35607E+05 0.00095  1.33145E+05 0.00100  2.60292E+05 0.00058  2.51572E+05 0.00054  1.81322E+05 0.00062  1.17513E+05 0.00080  1.35023E+05 0.00092  1.28987E+05 0.00087  1.14438E+05 0.00088  1.84097E+05 0.00080  4.26576E+04 0.00170  5.26523E+04 0.00145  4.79644E+04 0.00179  2.82158E+04 0.00178  4.85962E+04 0.00180  3.18964E+04 0.00211  2.56364E+04 0.00154  4.31079E+03 0.00353  3.64550E+03 0.00337  3.17595E+03 0.00414  3.05504E+03 0.00297  3.11337E+03 0.00333  3.40317E+03 0.00450  4.10207E+03 0.00265  4.21519E+03 0.00473  8.49631E+03 0.00305  1.41606E+04 0.00256  1.85176E+04 0.00230  4.96409E+04 0.00120  5.22448E+04 0.00103  5.58427E+04 0.00151  3.60912E+04 0.00130  2.53649E+04 0.00166  1.87806E+04 0.00200  2.20441E+04 0.00148  4.33241E+04 0.00149  6.14735E+04 0.00184  1.23506E+05 0.00138  1.96220E+05 0.00131  2.99354E+05 0.00135  1.93611E+05 0.00148  1.39426E+05 0.00172  1.00633E+05 0.00180  9.07090E+04 0.00157  8.96286E+04 0.00170  7.50667E+04 0.00163  5.09501E+04 0.00171  4.71349E+04 0.00205  4.20464E+04 0.00212  3.55027E+04 0.00216  2.80749E+04 0.00196  1.88788E+04 0.00242  6.67639E+03 0.00272 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.60182E-01 0.00051 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.62808E+18 0.00054  4.04667E+17 0.00137 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39588E-01 0.00010  1.57073E+00 0.00041 ];
INF_CAPT                  (idx, [1:   4]) = [  8.58443E-03 0.00080  3.90215E-02 0.00086 ];
INF_ABS                   (idx, [1:   4]) = [  1.01884E-02 0.00069  6.62043E-02 0.00107 ];
INF_FISS                  (idx, [1:   4]) = [  1.60401E-03 0.00045  2.71828E-02 0.00138 ];
INF_NSF                   (idx, [1:   4]) = [  4.39306E-03 0.00047  7.43547E-02 0.00143 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.73880E+00 6.7E-05  2.73536E+00 7.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06230E+02 9.3E-06  2.06451E+02 1.4E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.48843E-08 0.00030  2.59337E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29407E-01 0.00011  1.50453E+00 0.00048 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43957E-01 0.00018  3.98657E-01 0.00060 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63037E-02 0.00031  9.41302E-02 0.00090 ];
INF_SCATT3                (idx, [1:   4]) = [  7.22802E-03 0.00347  2.82414E-02 0.00205 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03809E-02 0.00165 -9.00252E-03 0.00775 ];
INF_SCATT5                (idx, [1:   4]) = [  1.73074E-04 0.09407  6.69283E-03 0.00937 ];
INF_SCATT6                (idx, [1:   4]) = [  5.12812E-03 0.00341 -1.72106E-02 0.00372 ];
INF_SCATT7                (idx, [1:   4]) = [  7.43698E-04 0.01935  5.82219E-04 0.09391 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29451E-01 0.00011  1.50453E+00 0.00048 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43958E-01 0.00018  3.98657E-01 0.00060 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63043E-02 0.00031  9.41302E-02 0.00090 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.22793E-03 0.00346  2.82414E-02 0.00205 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03809E-02 0.00165 -9.00252E-03 0.00775 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.73148E-04 0.09414  6.69283E-03 0.00937 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.12771E-03 0.00342 -1.72106E-02 0.00372 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.43642E-04 0.01932  5.82219E-04 0.09391 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12575E-01 0.00028  1.02131E+00 0.00033 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56808E+00 0.00028  3.26379E-01 0.00033 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.01441E-02 0.00068  6.62043E-02 0.00107 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70064E-02 0.00021  6.73549E-02 0.00117 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12582E-01 0.00011  1.68253E-02 0.00054  1.15108E-03 0.00572  1.50338E+00 0.00048 ];
INF_S1                    (idx, [1:   8]) = [  2.39101E-01 0.00018  4.85664E-03 0.00066  4.98676E-04 0.00734  3.98158E-01 0.00061 ];
INF_S2                    (idx, [1:   8]) = [  9.78496E-02 0.00031 -1.54588E-03 0.00241  2.72425E-04 0.01110  9.38577E-02 0.00089 ];
INF_S3                    (idx, [1:   8]) = [  8.97304E-03 0.00273 -1.74502E-03 0.00144  9.98174E-05 0.02398  2.81416E-02 0.00204 ];
INF_S4                    (idx, [1:   8]) = [ -9.84688E-03 0.00169 -5.34055E-04 0.00488  3.08652E-06 0.60279 -9.00560E-03 0.00768 ];
INF_S5                    (idx, [1:   8]) = [  1.28750E-04 0.12144  4.43232E-05 0.05740 -3.87802E-05 0.03669  6.73161E-03 0.00933 ];
INF_S6                    (idx, [1:   8]) = [  5.25835E-03 0.00332 -1.30223E-04 0.01702 -5.25782E-05 0.02907 -1.71581E-02 0.00374 ];
INF_S7                    (idx, [1:   8]) = [  9.07018E-04 0.01524 -1.63319E-04 0.01690 -4.72106E-05 0.03564  6.29430E-04 0.08628 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12626E-01 0.00011  1.68253E-02 0.00054  1.15108E-03 0.00572  1.50338E+00 0.00048 ];
INF_SP1                   (idx, [1:   8]) = [  2.39102E-01 0.00018  4.85664E-03 0.00066  4.98676E-04 0.00734  3.98158E-01 0.00061 ];
INF_SP2                   (idx, [1:   8]) = [  9.78502E-02 0.00031 -1.54588E-03 0.00241  2.72425E-04 0.01110  9.38577E-02 0.00089 ];
INF_SP3                   (idx, [1:   8]) = [  8.97295E-03 0.00273 -1.74502E-03 0.00144  9.98174E-05 0.02398  2.81416E-02 0.00204 ];
INF_SP4                   (idx, [1:   8]) = [ -9.84688E-03 0.00169 -5.34055E-04 0.00488  3.08652E-06 0.60279 -9.00560E-03 0.00768 ];
INF_SP5                   (idx, [1:   8]) = [  1.28824E-04 0.12148  4.43232E-05 0.05740 -3.87802E-05 0.03669  6.73161E-03 0.00933 ];
INF_SP6                   (idx, [1:   8]) = [  5.25794E-03 0.00333 -1.30223E-04 0.01702 -5.25782E-05 0.02907 -1.71581E-02 0.00374 ];
INF_SP7                   (idx, [1:   8]) = [  9.06961E-04 0.01521 -1.63319E-04 0.01690 -4.72106E-05 0.03564  6.29430E-04 0.08628 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31818E-01 0.00061  1.22525E+00 0.00729 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33762E-01 0.00088  1.34327E+00 0.00946 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33291E-01 0.00077  1.34110E+00 0.01047 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28481E-01 0.00095  1.04513E+00 0.00699 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43792E+00 0.00061  2.72396E-01 0.00720 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42598E+00 0.00088  2.48675E-01 0.00931 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42885E+00 0.00077  2.49196E-01 0.01031 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45894E+00 0.00095  3.19318E-01 0.00707 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.92365E-03 0.00959  1.20889E-04 0.06380  9.13934E-04 0.02383  7.54060E-04 0.02334  2.07661E-03 0.01429  8.12439E-04 0.02397  2.45717E-04 0.04503 ];
LAMBDA                    (idx, [1:  14]) = [  7.66250E-01 0.02359  1.26082E-02 0.00136  3.06320E-02 0.00062  1.11113E-01 0.00083  3.22764E-01 0.00058  1.26425E+00 0.00332  8.06998E+00 0.01298 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0008' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:50:49 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:38:58 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299849 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.93159E-01  9.98706E-01  1.00272E+00  1.00012E+00  1.00530E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16817E-02 0.00114  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88318E-01 1.4E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05766E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06199E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68701E+00 0.00024  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.45499E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.45408E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.30255E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.18871E-01 0.00115  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001641 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00033E+04 0.00103 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00033E+04 0.00103 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.38280E+02 ;
RUNNING_TIME              (idx, 1)        =  1.08138E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.62450E-01  4.62450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.52717E-01  1.11167E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.06357E+02  3.06972E+00  2.40160E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  9.56400E-01  2.63500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.72000E-02  8.50002E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.08138E+02  1.13675E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97772 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00025E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80500E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.79403E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68203E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.81370E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.18539E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.54383E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.60860E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.52761E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  6.29315E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.19230E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.35825E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.01586E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  9.34901E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.17644E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.11640E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.41036E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.58394E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.61424E+14 ;
CS134_ACTIVITY            (idx, 1)        =  7.85342E+13 ;
CS137_ACTIVITY            (idx, 1)        =  4.65868E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.54028E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.18745E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.31214E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.36388E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.56374E+12 0.00055  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 19 ;
BURNUP                     (idx, [1:  2])  = [  3.50000E+01  3.50034E+01 ];
BURN_DAYS                 (idx, 1)        =  8.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.91034E-01 0.00113 ];
U235_FISS                 (idx, [1:   4]) = [  3.57021E+15 0.00172  2.62736E-01 0.00150 ];
U238_FISS                 (idx, [1:   4]) = [  1.24946E+15 0.00280  9.19402E-02 0.00260 ];
PU239_FISS                (idx, [1:   4]) = [  6.77049E+15 0.00109  4.98279E-01 0.00086 ];
PU240_FISS                (idx, [1:   4]) = [  1.79202E+13 0.02440  1.31967E-03 0.02448 ];
PU241_FISS                (idx, [1:   4]) = [  1.93679E+15 0.00212  1.42541E-01 0.00203 ];
U235_CAPT                 (idx, [1:   4]) = [  7.99718E+14 0.00347  2.52224E-02 0.00351 ];
U238_CAPT                 (idx, [1:   4]) = [  1.06711E+16 0.00110  3.36485E-01 0.00076 ];
PU239_CAPT                (idx, [1:   4]) = [  3.71905E+15 0.00153  1.17285E-01 0.00149 ];
PU240_CAPT                (idx, [1:   4]) = [  3.05439E+15 0.00180  9.63155E-02 0.00166 ];
PU241_CAPT                (idx, [1:   4]) = [  6.97253E+14 0.00343  2.19875E-02 0.00338 ];
XE135_CAPT                (idx, [1:   4]) = [  7.90851E+14 0.00335  2.49389E-02 0.00329 ];
SM149_CAPT                (idx, [1:   4]) = [  2.56172E+14 0.00592  8.07845E-03 0.00591 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001641 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.26988E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001641 5.00827E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3469559 3.47431E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1486912 1.48880E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45170 4.51596E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001641 5.00827E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -8.19564E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 6.3E-09  4.50056E+05 6.3E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.75185E+16 2.2E-05  3.75185E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35839E+16 4.6E-06  1.35839E+16 4.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.17096E+16 0.00045  2.48381E+16 0.00049  6.87146E+15 0.00126 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.52935E+16 0.00032  3.84220E+16 0.00031  6.87146E+15 0.00126 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.56374E+16 0.00055  4.56374E+16 0.00055  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.12148E+18 0.00052  5.63531E+17 0.00050  1.55795E+18 0.00058 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.12239E+14 0.00506 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.57057E+16 0.00033 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.57867E+18 0.00068 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.08460E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.08460E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.40581E+00 0.00064 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.78590E-01 0.00035 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.12704E-01 0.00038 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23752E+00 0.00041 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94121E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96828E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.29855E-01 0.00072 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.22359E-01 0.00071 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.76198E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06791E+02 4.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.22238E-01 0.00072  8.18528E-01 0.00071  3.83081E-03 0.01192 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.22272E-01 0.00033 ];
COL_KEFF                  (idx, [1:   2]) = [  8.22224E-01 0.00055 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.22272E-01 0.00033 ];
ABS_KINF                  (idx, [1:   2]) = [  8.29766E-01 0.00032 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.68821E+01 0.00027 ];
IMP_ALF                   (idx, [1:   2]) = [  1.68847E+01 0.00011 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  9.36367E-07 0.00450 ];
IMP_EALF                  (idx, [1:   2]) = [  9.29946E-07 0.00183 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.14177E-01 0.00287 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.13326E-01 0.00119 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.98817E-03 0.00659  1.36391E-04 0.04100  1.11886E-03 0.01445  9.25695E-04 0.01637  2.52988E-03 0.01030  9.82247E-04 0.01673  2.95097E-04 0.02934 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.58986E-01 0.01578  8.69292E-03 0.03018  3.05157E-02 0.00044  1.11366E-01 0.00062  3.23091E-01 0.00038  1.24326E+00 0.00248  7.27123E+00 0.01860 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.75711E-03 0.01007  1.16679E-04 0.06418  8.91007E-04 0.02232  7.20536E-04 0.02429  2.01924E-03 0.01591  7.68314E-04 0.02554  2.41328E-04 0.04665 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.65161E-01 0.02391  1.26335E-02 0.00152  3.05539E-02 0.00064  1.11394E-01 0.00089  3.23098E-01 0.00061  1.24511E+00 0.00365  8.01903E+00 0.01393 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.03263E-05 0.00154  3.03129E-05 0.00154  3.31873E-05 0.02067 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.49292E-05 0.00137  2.49182E-05 0.00138  2.72840E-05 0.02061 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.64095E-03 0.01194  1.02209E-04 0.08500  8.55888E-04 0.02831  7.20682E-04 0.03068  2.00316E-03 0.01942  7.33540E-04 0.02941  2.25466E-04 0.05475 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.50450E-01 0.02884  1.26296E-02 0.00251  3.05112E-02 0.00088  1.11451E-01 0.00126  3.23357E-01 0.00083  1.24959E+00 0.00509  8.09913E+00 0.02014 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.97029E-05 0.00368  2.96903E-05 0.00370  2.68400E-05 0.04348 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.44159E-05 0.00359  2.44055E-05 0.00361  2.20693E-05 0.04345 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.75049E-03 0.03978  9.67860E-05 0.25700  1.03214E-03 0.08993  6.88348E-04 0.10325  1.97107E-03 0.05972  6.94396E-04 0.09809  2.67750E-04 0.18373 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.93497E-01 0.09230  1.27071E-02 0.00716  3.05323E-02 0.00194  1.11856E-01 0.00298  3.23583E-01 0.00209  1.24035E+00 0.01263  7.70180E+00 0.05819 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.69815E-03 0.03915  9.61259E-05 0.24068  1.01878E-03 0.08990  6.74302E-04 0.10083  1.96805E-03 0.05889  6.87721E-04 0.09577  2.53172E-04 0.17408 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.79296E-01 0.09041  1.27071E-02 0.00716  3.05315E-02 0.00193  1.11846E-01 0.00298  3.23493E-01 0.00208  1.23904E+00 0.01265  7.69986E+00 0.05810 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.61683E+02 0.04037 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.99926E-05 0.00096 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.46546E-05 0.00062 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.81944E-03 0.00754 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.60666E+02 0.00747 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.58655E-07 0.00082 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.61569E-06 0.00045  2.61565E-06 0.00045  2.62370E-06 0.00597 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.92081E-05 0.00096  3.92252E-05 0.00096  3.57344E-05 0.01199 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.10828E-01 0.00038  6.11418E-01 0.00038  5.24518E-01 0.01073 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08792E+01 0.01528 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.45408E+01 0.00051  3.29870E+01 0.00059 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.11944E+04 0.00267  3.06091E+05 0.00149  6.11897E+05 0.00090  6.50129E+05 0.00061  5.98414E+05 0.00082  6.41745E+05 0.00082  4.35920E+05 0.00059  3.85701E+05 0.00083  2.95380E+05 0.00082  2.41371E+05 0.00087  2.07990E+05 0.00086  1.87465E+05 0.00078  1.73345E+05 0.00088  1.64846E+05 0.00106  1.60626E+05 0.00086  1.38703E+05 0.00120  1.36936E+05 0.00095  1.35914E+05 0.00064  1.33150E+05 0.00080  2.60197E+05 0.00069  2.51940E+05 0.00105  1.81271E+05 0.00083  1.17350E+05 0.00080  1.35123E+05 0.00105  1.28958E+05 0.00102  1.14186E+05 0.00081  1.83511E+05 0.00065  4.27441E+04 0.00209  5.20798E+04 0.00175  4.76955E+04 0.00193  2.80494E+04 0.00178  4.83366E+04 0.00160  3.14758E+04 0.00167  2.52912E+04 0.00169  4.20175E+03 0.00462  3.49387E+03 0.00466  3.07716E+03 0.00432  2.97885E+03 0.00365  3.01324E+03 0.00324  3.31120E+03 0.00341  3.97413E+03 0.00324  4.13083E+03 0.00387  8.37009E+03 0.00331  1.39773E+04 0.00290  1.82908E+04 0.00266  4.90257E+04 0.00136  5.16298E+04 0.00141  5.53707E+04 0.00152  3.57206E+04 0.00180  2.50773E+04 0.00192  1.85665E+04 0.00200  2.19066E+04 0.00198  4.29606E+04 0.00183  6.09161E+04 0.00163  1.22587E+05 0.00208  1.95257E+05 0.00187  2.98185E+05 0.00200  1.93048E+05 0.00229  1.39086E+05 0.00220  1.00339E+05 0.00220  9.05928E+04 0.00214  8.94566E+04 0.00213  7.49423E+04 0.00231  5.09064E+04 0.00233  4.70152E+04 0.00237  4.19328E+04 0.00216  3.55065E+04 0.00246  2.80169E+04 0.00200  1.87749E+04 0.00225  6.67740E+03 0.00295 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.29718E-01 0.00048 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.70065E+18 0.00055  4.20883E+17 0.00186 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39872E-01 0.00012  1.56946E+00 0.00053 ];
INF_CAPT                  (idx, [1:   4]) = [  8.80653E-03 0.00077  3.97623E-02 0.00108 ];
INF_ABS                   (idx, [1:   4]) = [  1.03388E-02 0.00067  6.58525E-02 0.00137 ];
INF_FISS                  (idx, [1:   4]) = [  1.53229E-03 0.00055  2.60902E-02 0.00182 ];
INF_NSF                   (idx, [1:   4]) = [  4.22946E-03 0.00059  7.20713E-02 0.00187 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.76023E+00 9.1E-05  2.76239E+00 6.3E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06530E+02 9.8E-06  2.06853E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.45105E-08 0.00049  2.59589E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29529E-01 0.00013  1.50362E+00 0.00061 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43893E-01 0.00021  3.97952E-01 0.00073 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62637E-02 0.00028  9.38118E-02 0.00090 ];
INF_SCATT3                (idx, [1:   4]) = [  7.25588E-03 0.00303  2.81763E-02 0.00252 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03359E-02 0.00194 -8.86771E-03 0.00640 ];
INF_SCATT5                (idx, [1:   4]) = [  1.88215E-04 0.11083  6.84849E-03 0.00849 ];
INF_SCATT6                (idx, [1:   4]) = [  5.17094E-03 0.00250 -1.72335E-02 0.00247 ];
INF_SCATT7                (idx, [1:   4]) = [  8.14912E-04 0.01929  4.56677E-04 0.11324 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29573E-01 0.00013  1.50362E+00 0.00061 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43893E-01 0.00021  3.97952E-01 0.00073 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62639E-02 0.00029  9.38118E-02 0.00090 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.25593E-03 0.00303  2.81763E-02 0.00252 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03356E-02 0.00195 -8.86771E-03 0.00640 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.88208E-04 0.11108  6.84849E-03 0.00849 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.17082E-03 0.00251 -1.72335E-02 0.00247 ];
INF_SCATTP7               (idx, [1:   4]) = [  8.15003E-04 0.01927  4.56677E-04 0.11324 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12597E-01 0.00031  1.02168E+00 0.00043 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56792E+00 0.00031  3.26262E-01 0.00043 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.02944E-02 0.00065  6.58525E-02 0.00137 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70034E-02 0.00027  6.69781E-02 0.00145 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12869E-01 0.00012  1.66597E-02 0.00062  1.13720E-03 0.00432  1.50249E+00 0.00061 ];
INF_S1                    (idx, [1:   8]) = [  2.39088E-01 0.00020  4.80498E-03 0.00112  4.95874E-04 0.00763  3.97456E-01 0.00073 ];
INF_S2                    (idx, [1:   8]) = [  9.77999E-02 0.00027 -1.53623E-03 0.00268  2.68281E-04 0.01069  9.35435E-02 0.00090 ];
INF_S3                    (idx, [1:   8]) = [  8.98571E-03 0.00236 -1.72983E-03 0.00187  9.57935E-05 0.02225  2.80805E-02 0.00252 ];
INF_S4                    (idx, [1:   8]) = [ -9.80857E-03 0.00211 -5.27281E-04 0.00568 -2.49847E-06 0.83409 -8.86521E-03 0.00638 ];
INF_S5                    (idx, [1:   8]) = [  1.40253E-04 0.15098  4.79616E-05 0.04584 -4.07800E-05 0.04119  6.88927E-03 0.00841 ];
INF_S6                    (idx, [1:   8]) = [  5.30219E-03 0.00262 -1.31248E-04 0.01609 -4.96819E-05 0.02868 -1.71838E-02 0.00249 ];
INF_S7                    (idx, [1:   8]) = [  9.78604E-04 0.01587 -1.63691E-04 0.01033 -4.69755E-05 0.02695  5.03652E-04 0.10227 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12913E-01 0.00012  1.66597E-02 0.00062  1.13720E-03 0.00432  1.50249E+00 0.00061 ];
INF_SP1                   (idx, [1:   8]) = [  2.39088E-01 0.00020  4.80498E-03 0.00112  4.95874E-04 0.00763  3.97456E-01 0.00073 ];
INF_SP2                   (idx, [1:   8]) = [  9.78001E-02 0.00027 -1.53623E-03 0.00268  2.68281E-04 0.01069  9.35435E-02 0.00090 ];
INF_SP3                   (idx, [1:   8]) = [  8.98575E-03 0.00236 -1.72983E-03 0.00187  9.57935E-05 0.02225  2.80805E-02 0.00252 ];
INF_SP4                   (idx, [1:   8]) = [ -9.80830E-03 0.00212 -5.27281E-04 0.00568 -2.49847E-06 0.83409 -8.86521E-03 0.00638 ];
INF_SP5                   (idx, [1:   8]) = [  1.40246E-04 0.15133  4.79616E-05 0.04584 -4.07800E-05 0.04119  6.88927E-03 0.00841 ];
INF_SP6                   (idx, [1:   8]) = [  5.30207E-03 0.00263 -1.31248E-04 0.01609 -4.96819E-05 0.02868 -1.71838E-02 0.00249 ];
INF_SP7                   (idx, [1:   8]) = [  9.78695E-04 0.01586 -1.63691E-04 0.01033 -4.69755E-05 0.02695  5.03652E-04 0.10227 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31911E-01 0.00066  1.22806E+00 0.00761 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33516E-01 0.00111  1.34940E+00 0.00980 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33592E-01 0.00093  1.32334E+00 0.00849 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28701E-01 0.00089  1.05797E+00 0.00767 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43735E+00 0.00066  2.71811E-01 0.00767 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42750E+00 0.00111  2.47599E-01 0.00988 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42702E+00 0.00093  2.52329E-01 0.00859 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45754E+00 0.00089  3.15504E-01 0.00751 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.75711E-03 0.01007  1.16679E-04 0.06418  8.91007E-04 0.02232  7.20536E-04 0.02429  2.01924E-03 0.01591  7.68314E-04 0.02554  2.41328E-04 0.04665 ];
LAMBDA                    (idx, [1:  14]) = [  7.65161E-01 0.02391  1.26335E-02 0.00152  3.05539E-02 0.00064  1.11394E-01 0.00089  3.23098E-01 0.00061  1.24511E+00 0.00365  8.01903E+00 0.01393 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0008' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:50:49 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:44:33 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595299849 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.94407E-01  9.99311E-01  1.00308E+00  9.99949E-01  1.00325E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.17159E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88284E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05611E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06045E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68828E+00 0.00024  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.45506E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.45417E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.31088E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.20267E-01 0.00112  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001028 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00021E+04 0.00106 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00021E+04 0.00106 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.66210E+02 ;
RUNNING_TIME              (idx, 1)        =  1.13731E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.62450E-01  4.62450E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.74167E-01  1.07500E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.11878E+02  3.09948E+00  2.42115E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.00687E+00  2.50167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.88333E-02  7.83332E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.13731E+02  1.13731E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97851 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99933E+00 0.00026 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80701E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.93002E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68837E+04 ;
TOT_SF_RATE               (idx, 1)        =  5.92415E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.28369E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.61931E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.64630E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.52639E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  7.95042E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.35781E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  6.97652E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.08791E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  9.73904E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.26990E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.42450E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.45027E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.62502E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.66405E+14 ;
CS134_ACTIVITY            (idx, 1)        =  9.80429E+13 ;
CS137_ACTIVITY            (idx, 1)        =  5.30861E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.66262E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.16700E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.06450E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.39339E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.74290E+12 0.00054  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 20 ;
BURNUP                     (idx, [1:  2])  = [  4.00000E+01  4.00040E+01 ];
BURN_DAYS                 (idx, 1)        =  1.00000E+03 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  8.26312E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  2.91343E+15 0.00188  2.14572E-01 0.00172 ];
U238_FISS                 (idx, [1:   4]) = [  1.28690E+15 0.00275  9.47664E-02 0.00253 ];
PU239_FISS                (idx, [1:   4]) = [  7.10631E+15 0.00108  5.23390E-01 0.00085 ];
PU240_FISS                (idx, [1:   4]) = [  2.05083E+13 0.02155  1.50976E-03 0.02150 ];
PU241_FISS                (idx, [1:   4]) = [  2.19215E+15 0.00214  1.61444E-01 0.00196 ];
U235_CAPT                 (idx, [1:   4]) = [  6.51776E+14 0.00401  1.94524E-02 0.00397 ];
U238_CAPT                 (idx, [1:   4]) = [  1.10262E+16 0.00107  3.29065E-01 0.00080 ];
PU239_CAPT                (idx, [1:   4]) = [  3.89577E+15 0.00157  1.16272E-01 0.00149 ];
PU240_CAPT                (idx, [1:   4]) = [  3.33309E+15 0.00191  9.94670E-02 0.00171 ];
PU241_CAPT                (idx, [1:   4]) = [  7.81901E+14 0.00346  2.33380E-02 0.00346 ];
XE135_CAPT                (idx, [1:   4]) = [  8.03143E+14 0.00333  2.39719E-02 0.00333 ];
SM149_CAPT                (idx, [1:   4]) = [  2.67851E+14 0.00609  7.99485E-03 0.00611 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001028 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.45713E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001028 5.00846E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3527002 3.53230E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1429344 1.43147E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44682 4.46872E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001028 5.00846E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.05360E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 6.3E-09  4.50056E+05 6.3E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.77614E+16 2.0E-05  3.77614E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35626E+16 4.0E-06  1.35626E+16 4.0E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.35126E+16 0.00046  2.63690E+16 0.00046  7.14365E+15 0.00126 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.70752E+16 0.00033  3.99315E+16 0.00031  7.14365E+15 0.00126 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.74290E+16 0.00054  4.74290E+16 0.00054  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.20383E+18 0.00052  5.84741E+17 0.00051  1.61909E+18 0.00057 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.23915E+14 0.00481 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.74991E+16 0.00033 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.64080E+18 0.00068 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.07884E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.07884E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.37611E+00 0.00069 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.76936E-01 0.00035 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.08283E-01 0.00037 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23695E+00 0.00041 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94195E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96849E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.04359E-01 0.00071 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  7.97170E-01 0.00071 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.78424E+00 2.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.07117E+02 4.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  7.97145E-01 0.00073  7.93405E-01 0.00071  3.76498E-03 0.01216 ];
IMP_KEFF                  (idx, [1:   2]) = [  7.96363E-01 0.00033 ];
COL_KEFF                  (idx, [1:   2]) = [  7.96282E-01 0.00054 ];
ABS_KEFF                  (idx, [1:   2]) = [  7.96363E-01 0.00033 ];
ABS_KINF                  (idx, [1:   2]) = [  8.03545E-01 0.00033 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.68345E+01 0.00027 ];
IMP_ALF                   (idx, [1:   2]) = [  1.68297E+01 0.00011 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  9.81926E-07 0.00448 ];
IMP_EALF                  (idx, [1:   2]) = [  9.82554E-07 0.00190 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.24606E-01 0.00289 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.25154E-01 0.00123 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.11407E-03 0.00696  1.48865E-04 0.03986  1.14734E-03 0.01501  9.02392E-04 0.01732  2.59752E-03 0.00997  1.02650E-03 0.01642  2.91464E-04 0.02912 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.30232E-01 0.01549  8.82981E-03 0.02948  3.04182E-02 0.00039  1.11468E-01 0.00211  3.23411E-01 0.00040  1.22240E+00 0.00285  6.92284E+00 0.02039 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.69248E-03 0.00984  1.25740E-04 0.06112  8.90161E-04 0.02251  7.02848E-04 0.02644  1.94590E-03 0.01492  7.93421E-04 0.02595  2.34412E-04 0.04741 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.43013E-01 0.02553  1.26538E-02 0.00156  3.04063E-02 0.00058  1.11706E-01 0.00097  3.23675E-01 0.00064  1.22559E+00 0.00399  7.72489E+00 0.01573 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.14413E-05 0.00165  3.14287E-05 0.00165  3.47918E-05 0.02031 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.50567E-05 0.00148  2.50467E-05 0.00149  2.77128E-05 0.02020 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.72716E-03 0.01220  1.17436E-04 0.07835  9.01554E-04 0.02830  6.81388E-04 0.03188  2.01573E-03 0.01843  7.91690E-04 0.03117  2.19361E-04 0.05654 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.19299E-01 0.03011  1.26535E-02 0.00252  3.04165E-02 0.00079  1.11740E-01 0.00139  3.23691E-01 0.00091  1.22864E+00 0.00561  7.65633E+00 0.02490 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.05317E-05 0.00387  3.05310E-05 0.00388  2.54737E-05 0.05075 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.43339E-05 0.00384  2.43334E-05 0.00385  2.02846E-05 0.05059 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.84361E-03 0.04366  1.37069E-04 0.30382  9.28115E-04 0.10526  6.30771E-04 0.10663  2.13820E-03 0.06438  7.46855E-04 0.10109  2.62603E-04 0.18547 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.10814E-01 0.09005  1.26160E-02 0.00595  3.03803E-02 0.00190  1.12143E-01 0.00324  3.23364E-01 0.00222  1.21067E+00 0.01399  7.24086E+00 0.06623 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.78115E-03 0.04318  1.35597E-04 0.29401  9.06148E-04 0.10513  6.35192E-04 0.10391  2.11764E-03 0.06413  7.29802E-04 0.10132  2.56769E-04 0.18221 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.10141E-01 0.08866  1.26105E-02 0.00586  3.03802E-02 0.00189  1.12164E-01 0.00324  3.23420E-01 0.00222  1.21111E+00 0.01394  7.23514E+00 0.06640 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.58574E+02 0.04326 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.10176E-05 0.00098 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.47190E-05 0.00065 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.68710E-03 0.00845 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.51241E+02 0.00858 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.58880E-07 0.00081 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.59684E-06 0.00044  2.59671E-06 0.00044  2.62324E-06 0.00607 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.95246E-05 0.00098  3.95412E-05 0.00098  3.61637E-05 0.01223 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.06487E-01 0.00036  6.07195E-01 0.00036  5.03305E-01 0.01077 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.09903E+01 0.01465 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.45417E+01 0.00053  3.29798E+01 0.00063 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.18298E+04 0.00309  3.07706E+05 0.00157  6.10788E+05 0.00058  6.50754E+05 0.00076  5.99091E+05 0.00075  6.41542E+05 0.00064  4.35781E+05 0.00075  3.85686E+05 0.00067  2.95305E+05 0.00067  2.41142E+05 0.00078  2.08197E+05 0.00078  1.87418E+05 0.00093  1.72982E+05 0.00102  1.64767E+05 0.00080  1.60526E+05 0.00092  1.38730E+05 0.00071  1.37157E+05 0.00095  1.35798E+05 0.00094  1.33592E+05 0.00103  2.59945E+05 0.00075  2.51891E+05 0.00080  1.81444E+05 0.00067  1.17573E+05 0.00088  1.34861E+05 0.00084  1.29269E+05 0.00080  1.14257E+05 0.00105  1.82990E+05 0.00087  4.24709E+04 0.00164  5.16637E+04 0.00126  4.73091E+04 0.00131  2.79443E+04 0.00188  4.80186E+04 0.00123  3.10956E+04 0.00214  2.48314E+04 0.00166  4.08968E+03 0.00334  3.42524E+03 0.00431  2.99980E+03 0.00426  2.93223E+03 0.00328  2.94133E+03 0.00388  3.22189E+03 0.00321  3.88127E+03 0.00388  4.04707E+03 0.00328  8.22173E+03 0.00266  1.38572E+04 0.00289  1.81509E+04 0.00257  4.86139E+04 0.00126  5.10330E+04 0.00153  5.46015E+04 0.00212  3.55092E+04 0.00179  2.49113E+04 0.00169  1.84249E+04 0.00219  2.17403E+04 0.00163  4.26602E+04 0.00162  6.06396E+04 0.00166  1.22168E+05 0.00146  1.94877E+05 0.00147  2.98085E+05 0.00162  1.93088E+05 0.00164  1.39285E+05 0.00164  1.00736E+05 0.00184  9.08092E+04 0.00165  8.97018E+04 0.00173  7.50348E+04 0.00190  5.08496E+04 0.00192  4.71358E+04 0.00204  4.20211E+04 0.00179  3.55552E+04 0.00188  2.81320E+04 0.00188  1.89233E+04 0.00187  6.70940E+03 0.00292 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.03462E-01 0.00052 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.76691E+18 0.00054  4.36968E+17 0.00126 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.40197E-01 0.00011  1.57072E+00 0.00042 ];
INF_CAPT                  (idx, [1:   4]) = [  8.99220E-03 0.00066  4.03381E-02 0.00079 ];
INF_ABS                   (idx, [1:   4]) = [  1.04665E-02 0.00058  6.54198E-02 0.00096 ];
INF_FISS                  (idx, [1:   4]) = [  1.47431E-03 0.00035  2.50817E-02 0.00127 ];
INF_NSF                   (idx, [1:   4]) = [  4.09677E-03 0.00038  6.98661E-02 0.00131 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.77877E+00 6.7E-05  2.78553E+00 5.6E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06784E+02 7.5E-06  2.07196E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.41950E-08 0.00036  2.59978E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29730E-01 0.00012  1.50528E+00 0.00048 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43966E-01 0.00020  3.98266E-01 0.00057 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63063E-02 0.00030  9.38653E-02 0.00082 ];
INF_SCATT3                (idx, [1:   4]) = [  7.26166E-03 0.00250  2.82083E-02 0.00245 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03574E-02 0.00164 -8.98099E-03 0.00741 ];
INF_SCATT5                (idx, [1:   4]) = [  1.78317E-04 0.09668  6.75506E-03 0.00946 ];
INF_SCATT6                (idx, [1:   4]) = [  5.14096E-03 0.00288 -1.73248E-02 0.00402 ];
INF_SCATT7                (idx, [1:   4]) = [  7.64169E-04 0.01879  4.93793E-04 0.11820 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29776E-01 0.00012  1.50528E+00 0.00048 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43967E-01 0.00020  3.98266E-01 0.00057 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63063E-02 0.00030  9.38653E-02 0.00082 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.26169E-03 0.00250  2.82083E-02 0.00245 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03574E-02 0.00164 -8.98099E-03 0.00741 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.78373E-04 0.09667  6.75506E-03 0.00946 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.14082E-03 0.00289 -1.73248E-02 0.00402 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.64060E-04 0.01879  4.93793E-04 0.11820 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12695E-01 0.00031  1.02313E+00 0.00038 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56720E+00 0.00031  3.25798E-01 0.00038 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.04211E-02 0.00058  6.54198E-02 0.00096 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70130E-02 0.00016  6.65691E-02 0.00111 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.13184E-01 0.00011  1.65466E-02 0.00047  1.13365E-03 0.00539  1.50415E+00 0.00048 ];
INF_S1                    (idx, [1:   8]) = [  2.39204E-01 0.00020  4.76216E-03 0.00109  4.92967E-04 0.00696  3.97773E-01 0.00057 ];
INF_S2                    (idx, [1:   8]) = [  9.78399E-02 0.00029 -1.53365E-03 0.00189  2.69700E-04 0.01108  9.35956E-02 0.00082 ];
INF_S3                    (idx, [1:   8]) = [  8.98050E-03 0.00208 -1.71884E-03 0.00184  9.85101E-05 0.02229  2.81098E-02 0.00246 ];
INF_S4                    (idx, [1:   8]) = [ -9.83474E-03 0.00167 -5.22620E-04 0.00455  2.67394E-06 0.55582 -8.98367E-03 0.00736 ];
INF_S5                    (idx, [1:   8]) = [  1.28010E-04 0.13540  5.03070E-05 0.04981 -3.91112E-05 0.04009  6.79417E-03 0.00927 ];
INF_S6                    (idx, [1:   8]) = [  5.26477E-03 0.00277 -1.23808E-04 0.01819 -4.90636E-05 0.02799 -1.72758E-02 0.00401 ];
INF_S7                    (idx, [1:   8]) = [  9.26712E-04 0.01490 -1.62543E-04 0.01607 -4.55809E-05 0.02711  5.39374E-04 0.10881 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.13229E-01 0.00011  1.65466E-02 0.00047  1.13365E-03 0.00539  1.50415E+00 0.00048 ];
INF_SP1                   (idx, [1:   8]) = [  2.39205E-01 0.00020  4.76216E-03 0.00109  4.92967E-04 0.00696  3.97773E-01 0.00057 ];
INF_SP2                   (idx, [1:   8]) = [  9.78399E-02 0.00029 -1.53365E-03 0.00189  2.69700E-04 0.01108  9.35956E-02 0.00082 ];
INF_SP3                   (idx, [1:   8]) = [  8.98053E-03 0.00209 -1.71884E-03 0.00184  9.85101E-05 0.02229  2.81098E-02 0.00246 ];
INF_SP4                   (idx, [1:   8]) = [ -9.83475E-03 0.00167 -5.22620E-04 0.00455  2.67394E-06 0.55582 -8.98367E-03 0.00736 ];
INF_SP5                   (idx, [1:   8]) = [  1.28066E-04 0.13531  5.03070E-05 0.04981 -3.91112E-05 0.04009  6.79417E-03 0.00927 ];
INF_SP6                   (idx, [1:   8]) = [  5.26463E-03 0.00278 -1.23808E-04 0.01819 -4.90636E-05 0.02799 -1.72758E-02 0.00401 ];
INF_SP7                   (idx, [1:   8]) = [  9.26604E-04 0.01490 -1.62543E-04 0.01607 -4.55809E-05 0.02711  5.39374E-04 0.10881 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31889E-01 0.00057  1.23642E+00 0.00682 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33617E-01 0.00068  1.35332E+00 0.00943 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33734E-01 0.00086  1.34300E+00 0.00835 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28404E-01 0.00107  1.06172E+00 0.00618 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43748E+00 0.00057  2.69900E-01 0.00689 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42685E+00 0.00068  2.46846E-01 0.00964 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42615E+00 0.00086  2.48608E-01 0.00821 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45944E+00 0.00107  3.14246E-01 0.00622 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.69248E-03 0.00984  1.25740E-04 0.06112  8.90161E-04 0.02251  7.02848E-04 0.02644  1.94590E-03 0.01492  7.93421E-04 0.02595  2.34412E-04 0.04741 ];
LAMBDA                    (idx, [1:  14]) = [  7.43013E-01 0.02553  1.26538E-02 0.00156  3.04063E-02 0.00058  1.11706E-01 0.00097  3.23675E-01 0.00064  1.22559E+00 0.00399  7.72489E+00 0.01573 ];

