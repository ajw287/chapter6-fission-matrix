
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0001' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:49:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:53:54 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292590 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00019E+00  1.00329E+00  1.00105E+00  9.93215E-01  1.00226E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12241E-02 0.00105  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88776E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.96839E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.97253E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.72281E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.81370E+01 0.00041  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.81280E+01 0.00041  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.69581E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.53136E-01 0.00108  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000836 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00017E+04 0.00072 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00017E+04 0.00072 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.83738E+01 ;
RUNNING_TIME              (idx, 1)        =  4.06410E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  4.86050E-01  4.86050E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.30000E-03  1.30000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.57647E+00  3.57647E+00  0.00000E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.06370E+00  0.00000E+00 ];
CPU_USAGE                 (idx, 1)        = 4.52100 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99920E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  8.69110E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.62954E+08 ;
TOT_DECAY_HEAT            (idx, 1)        =  6.57373E-04 ;
TOT_SF_RATE               (idx, 1)        =  7.43560E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  8.62954E+08 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  6.57373E-04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  7.89716E+03 ;
INGESTION_TOXICITY        (idx, 1)        =  4.16851E+01 ;
ACTINIDE_INH_TOX          (idx, 1)        =  7.89716E+03 ;
ACTINIDE_ING_TOX          (idx, 1)        =  4.16851E+01 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  1.08881E+08 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  8.62598E+08 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  3.52075E+08 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.08487E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 0 ;
BURNUP                     (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
BURN_DAYS                 (idx, 1)        =  0.00000E+00 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.23184E-01 0.00109 ];
U235_FISS                 (idx, [1:   4]) = [  1.30234E+16 0.00059  9.38958E-01 0.00018 ];
U238_FISS                 (idx, [1:   4]) = [  8.45253E+14 0.00285  6.09373E-02 0.00275 ];
U235_CAPT                 (idx, [1:   4]) = [  2.75897E+15 0.00148  1.64760E-01 0.00141 ];
U238_CAPT                 (idx, [1:   4]) = [  8.19934E+15 0.00099  4.89603E-01 0.00063 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000836 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.14838E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000836 5.00715E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2710735 2.71426E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2245402 2.24819E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44699 4.46928E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000836 5.00715E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -2.60770E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.41127E+16 1.1E-05  3.41127E+16 1.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38677E+16 1.3E-06  1.38677E+16 1.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.67336E+16 0.00048  1.10650E+16 0.00051  5.66854E+15 0.00090 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.06012E+16 0.00026  2.49327E+16 0.00023  5.66854E+15 0.00090 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.08487E+16 0.00050  3.08487E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.52220E+18 0.00047  4.05982E+17 0.00046  1.11622E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.75762E+14 0.00471 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.08770E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.17776E+18 0.00056 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12515E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.76386E+00 0.00041 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.53362E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.84630E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.22677E+00 0.00033 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94191E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96852E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.11599E+00 0.00051 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.10602E+00 0.00051 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.45987E+00 1.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02561E+02 1.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.10596E+00 0.00051  1.09814E+00 0.00051  7.88166E-03 0.00789 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.10642E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.10594E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.10642E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.11641E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.76434E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.76402E+01 8.0E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.36324E-07 0.00334 ];
IMP_EALF                  (idx, [1:   2]) = [  4.36720E-07 0.00142 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.05370E-01 0.00300 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.05966E-01 0.00117 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.51805E-03 0.00526  1.84803E-04 0.03274  1.02751E-03 0.01325  1.02020E-03 0.01392  3.00650E-03 0.00770  9.61129E-04 0.01362  3.17915E-04 0.02387 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.02936E-01 0.01273  1.05422E-02 0.01925  3.16413E-02 0.00022  1.10197E-01 0.00028  3.20625E-01 0.00020  1.34568E+00 0.00017  8.74829E+00 0.00595 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.20933E-03 0.00783  2.11219E-04 0.04723  1.12687E-03 0.02042  1.14518E-03 0.01971  3.29448E-03 0.01135  1.07192E-03 0.01942  3.59667E-04 0.03593 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.08495E-01 0.01834  1.24908E-02 2.9E-06  3.16344E-02 0.00033  1.10187E-01 0.00041  3.20571E-01 0.00030  1.34602E+00 0.00023  8.89658E+00 0.00212 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.92024E-05 0.00112  2.91884E-05 0.00112  3.12673E-05 0.01148 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.22927E-05 0.00100  3.22772E-05 0.00100  3.45778E-05 0.01147 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.13738E-03 0.00805  2.04045E-04 0.04843  1.12550E-03 0.01970  1.11514E-03 0.01892  3.27933E-03 0.01178  1.05840E-03 0.02104  3.54962E-04 0.03423 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.08910E-01 0.01785  1.24908E-02 3.8E-06  3.16312E-02 0.00038  1.10226E-01 0.00047  3.20641E-01 0.00033  1.34559E+00 0.00026  8.90433E+00 0.00244 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.92864E-05 0.00252  2.92800E-05 0.00253  3.04558E-05 0.02934 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.23864E-05 0.00249  3.23794E-05 0.00250  3.36728E-05 0.02928 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.09449E-03 0.02349  2.15465E-04 0.15045  1.16118E-03 0.06218  1.00415E-03 0.06154  3.25063E-03 0.03474  1.15837E-03 0.05895  3.04707E-04 0.11343 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.36086E-01 0.05161  1.24906E-02 5.0E-06  3.16048E-02 0.00092  1.10292E-01 0.00113  3.20765E-01 0.00099  1.34642E+00 0.00056  8.90584E+00 0.00537 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.08296E-03 0.02272  2.12679E-04 0.14437  1.17631E-03 0.05813  9.87773E-04 0.05936  3.24096E-03 0.03306  1.14841E-03 0.05749  3.16828E-04 0.10702 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.48480E-01 0.05041  1.24906E-02 5.0E-06  3.16043E-02 0.00092  1.10287E-01 0.00112  3.20705E-01 0.00095  1.34652E+00 0.00055  8.90753E+00 0.00538 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.42660E+02 0.02335 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.91650E-05 0.00071 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.22512E-05 0.00049 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.09688E-03 0.00454 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.43401E+02 0.00460 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.71071E-07 0.00053 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87251E-06 0.00042  2.87251E-06 0.00042  2.87298E-06 0.00462 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.53268E-05 0.00068  4.53457E-05 0.00068  4.27365E-05 0.00810 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.82047E-01 0.00031  6.81454E-01 0.00032  7.83363E-01 0.00818 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03529E+01 0.01350 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.81280E+01 0.00041  3.70890E+01 0.00044 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.18351E+04 0.00296  2.91282E+05 0.00142  6.01377E+05 0.00092  6.49272E+05 0.00066  5.97126E+05 0.00063  6.41807E+05 0.00065  4.35349E+05 0.00060  3.85232E+05 0.00079  2.94406E+05 0.00080  2.41011E+05 0.00091  2.07420E+05 0.00078  1.86612E+05 0.00087  1.72697E+05 0.00073  1.64173E+05 0.00092  1.59946E+05 0.00070  1.38168E+05 0.00064  1.36615E+05 0.00089  1.35160E+05 0.00109  1.33004E+05 0.00091  2.59283E+05 0.00069  2.50555E+05 0.00067  1.80799E+05 0.00077  1.17317E+05 0.00107  1.35610E+05 0.00083  1.28357E+05 0.00098  1.16672E+05 0.00104  1.92421E+05 0.00081  4.39011E+04 0.00122  5.49846E+04 0.00134  5.01216E+04 0.00167  2.90904E+04 0.00255  5.04832E+04 0.00160  3.43741E+04 0.00216  2.93022E+04 0.00165  5.62048E+03 0.00375  5.54025E+03 0.00224  5.68690E+03 0.00270  5.86655E+03 0.00325  5.82752E+03 0.00294  5.70934E+03 0.00449  5.92935E+03 0.00330  5.54669E+03 0.00376  1.04862E+04 0.00301  1.67761E+04 0.00239  2.13605E+04 0.00230  5.64587E+04 0.00132  5.97095E+04 0.00114  6.60670E+04 0.00136  4.61943E+04 0.00124  3.50697E+04 0.00126  2.73147E+04 0.00165  3.27191E+04 0.00133  6.28778E+04 0.00097  8.61122E+04 0.00119  1.67227E+05 0.00099  2.59454E+05 0.00109  3.90167E+05 0.00074  2.49949E+05 0.00093  1.79089E+05 0.00098  1.28834E+05 0.00103  1.15601E+05 0.00115  1.13758E+05 0.00096  9.49425E+04 0.00103  6.41287E+04 0.00118  5.91994E+04 0.00126  5.26635E+04 0.00126  4.45916E+04 0.00113  3.50286E+04 0.00119  2.34772E+04 0.00118  8.29316E+03 0.00199 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.11592E+00 0.00052 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.15167E+18 0.00056  3.70564E+17 0.00066 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38577E-01 0.00012  1.50454E+00 0.00033 ];
INF_CAPT                  (idx, [1:   4]) = [  6.27279E-03 0.00083  2.56638E-02 0.00022 ];
INF_ABS                   (idx, [1:   4]) = [  8.40738E-03 0.00062  5.64577E-02 0.00047 ];
INF_FISS                  (idx, [1:   4]) = [  2.13459E-03 0.00043  3.07939E-02 0.00069 ];
INF_NSF                   (idx, [1:   4]) = [  5.48035E-03 0.00042  7.50355E-02 0.00069 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.56740E+00 4.7E-05  2.43670E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03910E+02 5.3E-06  2.02270E+02 2.7E-09 ];
INF_INVV                  (idx, [1:   4]) = [  5.93765E-08 0.00042  2.57223E-06 0.00013 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.30167E-01 0.00012  1.44805E+00 0.00035 ];
INF_SCATT1                (idx, [1:   4]) = [  2.45006E-01 0.00018  3.81446E-01 0.00044 ];
INF_SCATT2                (idx, [1:   4]) = [  9.66040E-02 0.00027  9.01758E-02 0.00078 ];
INF_SCATT3                (idx, [1:   4]) = [  7.40316E-03 0.00245  2.70835E-02 0.00187 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03504E-02 0.00186 -8.46492E-03 0.00566 ];
INF_SCATT5                (idx, [1:   4]) = [  1.24855E-04 0.17768  6.36058E-03 0.00702 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07685E-03 0.00399 -1.63088E-02 0.00280 ];
INF_SCATT7                (idx, [1:   4]) = [  7.53261E-04 0.02409  4.72556E-04 0.07843 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.30205E-01 0.00012  1.44805E+00 0.00035 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.45006E-01 0.00018  3.81446E-01 0.00044 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.66042E-02 0.00027  9.01758E-02 0.00078 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.40326E-03 0.00245  2.70835E-02 0.00187 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03505E-02 0.00186 -8.46492E-03 0.00566 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.24864E-04 0.17794  6.36058E-03 0.00702 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07686E-03 0.00399 -1.63088E-02 0.00280 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.53102E-04 0.02409  4.72556E-04 0.07843 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13385E-01 0.00036  9.71768E-01 0.00030 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56213E+00 0.00036  3.43018E-01 0.00030 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.36908E-03 0.00063  5.64577E-02 0.00047 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69949E-02 0.00024  5.74984E-02 0.00066 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11582E-01 0.00012  1.85846E-02 0.00044  1.01347E-03 0.00397  1.44704E+00 0.00035 ];
INF_S1                    (idx, [1:   8]) = [  2.39561E-01 0.00018  5.44441E-03 0.00075  4.33994E-04 0.00727  3.81012E-01 0.00044 ];
INF_S2                    (idx, [1:   8]) = [  9.81999E-02 0.00026 -1.59595E-03 0.00194  2.37422E-04 0.01061  8.99383E-02 0.00078 ];
INF_S3                    (idx, [1:   8]) = [  9.30883E-03 0.00187 -1.90567E-03 0.00159  8.43507E-05 0.02217  2.69991E-02 0.00186 ];
INF_S4                    (idx, [1:   8]) = [ -9.71477E-03 0.00199 -6.35601E-04 0.00296  1.72760E-06 0.76532 -8.46665E-03 0.00565 ];
INF_S5                    (idx, [1:   8]) = [  1.11357E-04 0.20674  1.34975E-05 0.19481 -3.54451E-05 0.03319  6.39602E-03 0.00701 ];
INF_S6                    (idx, [1:   8]) = [  5.22806E-03 0.00395 -1.51206E-04 0.01739 -4.42084E-05 0.02674 -1.62646E-02 0.00281 ];
INF_S7                    (idx, [1:   8]) = [  9.31317E-04 0.01906 -1.78056E-04 0.00971 -4.05883E-05 0.03472  5.13144E-04 0.07203 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11620E-01 0.00012  1.85846E-02 0.00044  1.01347E-03 0.00397  1.44704E+00 0.00035 ];
INF_SP1                   (idx, [1:   8]) = [  2.39562E-01 0.00018  5.44441E-03 0.00075  4.33994E-04 0.00727  3.81012E-01 0.00044 ];
INF_SP2                   (idx, [1:   8]) = [  9.82001E-02 0.00026 -1.59595E-03 0.00194  2.37422E-04 0.01061  8.99383E-02 0.00078 ];
INF_SP3                   (idx, [1:   8]) = [  9.30892E-03 0.00187 -1.90567E-03 0.00159  8.43507E-05 0.02217  2.69991E-02 0.00186 ];
INF_SP4                   (idx, [1:   8]) = [ -9.71487E-03 0.00199 -6.35601E-04 0.00296  1.72760E-06 0.76532 -8.46665E-03 0.00565 ];
INF_SP5                   (idx, [1:   8]) = [  1.11366E-04 0.20700  1.34975E-05 0.19481 -3.54451E-05 0.03319  6.39602E-03 0.00701 ];
INF_SP6                   (idx, [1:   8]) = [  5.22807E-03 0.00396 -1.51206E-04 0.01739 -4.42084E-05 0.02674 -1.62646E-02 0.00281 ];
INF_SP7                   (idx, [1:   8]) = [  9.31158E-04 0.01906 -1.78056E-04 0.00971 -4.05883E-05 0.03472  5.13144E-04 0.07203 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.33012E-01 0.00044  1.08178E+00 0.00692 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34789E-01 0.00069  1.14634E+00 0.00812 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34652E-01 0.00074  1.15086E+00 0.00806 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29672E-01 0.00073  9.69750E-01 0.00691 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43055E+00 0.00044  3.08490E-01 0.00698 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.41973E+00 0.00069  2.91250E-01 0.00827 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42056E+00 0.00075  2.90098E-01 0.00819 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45136E+00 0.00073  3.44122E-01 0.00685 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.20933E-03 0.00783  2.11219E-04 0.04723  1.12687E-03 0.02042  1.14518E-03 0.01971  3.29448E-03 0.01135  1.07192E-03 0.01942  3.59667E-04 0.03593 ];
LAMBDA                    (idx, [1:  14]) = [  8.08495E-01 0.01834  1.24908E-02 2.9E-06  3.16344E-02 0.00033  1.10187E-01 0.00041  3.20571E-01 0.00030  1.34602E+00 0.00023  8.89658E+00 0.00212 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0001' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:49:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:00:14 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292590 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00249E+00  1.00391E+00  1.00040E+00  9.93371E-01  9.99827E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12024E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88798E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.98230E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.98644E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.71793E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.78121E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.78031E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.53048E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.46885E-01 0.00112  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000520 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00010E+04 0.00072 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00010E+04 0.00072 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.00059E+01 ;
RUNNING_TIME              (idx, 1)        =  1.03962E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.86050E-01  4.86050E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.45167E-02  6.58333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.83657E+00  3.54045E+00  2.71965E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.83167E-02  2.93500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.41667E-03  8.16667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.03961E+01  1.31388E+02 ];
CPU_USAGE                 (idx, 1)        = 4.81001 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00106E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.40421E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  7.94840E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.76128E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.43546E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.26190E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  8.94847E+02 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  6.68648E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.67177E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.24151E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  1.77044E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.57104E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  3.66078E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.84410E+06 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  1.40436E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.28236E+11 ;
TE132_ACTIVITY            (idx, 1)        =  2.51486E+14 ;
I131_ACTIVITY             (idx, 1)        =  7.42579E+13 ;
I132_ACTIVITY             (idx, 1)        =  2.43678E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.64382E+07 ;
CS137_ACTIVITY            (idx, 1)        =  1.34658E+11 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  6.63413E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.65217E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.33408E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  9.19284E+16 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.19385E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 1 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E-01  1.00009E-01 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+00 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.36081E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  1.29497E+16 0.00059  9.34707E-01 0.00018 ];
U238_FISS                 (idx, [1:   4]) = [  8.76538E+14 0.00274  6.32572E-02 0.00255 ];
PU239_FISS                (idx, [1:   4]) = [  2.62405E+13 0.01467  1.89439E-03 0.01469 ];
U235_CAPT                 (idx, [1:   4]) = [  2.77141E+15 0.00146  1.55335E-01 0.00137 ];
U238_CAPT                 (idx, [1:   4]) = [  8.38951E+15 0.00095  4.70198E-01 0.00065 ];
PU239_CAPT                (idx, [1:   4]) = [  1.47794E+13 0.02094  8.28087E-04 0.02092 ];
PU240_CAPT                (idx, [1:   4]) = [  2.42927E+11 0.16066  1.35563E-05 0.16059 ];
XE135_CAPT                (idx, [1:   4]) = [  7.24215E+14 0.00291  4.05911E-02 0.00285 ];
SM149_CAPT                (idx, [1:   4]) = [  2.17226E+13 0.01675  1.21749E-03 0.01675 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000520 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.30842E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000520 5.00731E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2789300 2.79321E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2166151 2.16902E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45069 4.50746E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000520 5.00731E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.89179E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.41320E+16 1.2E-05  3.41320E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38662E+16 1.4E-06  1.38662E+16 1.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.78332E+16 0.00045  1.20664E+16 0.00046  5.76681E+15 0.00096 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.16995E+16 0.00026  2.59326E+16 0.00022  5.76681E+15 0.00096 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.19385E+16 0.00052  3.19385E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.56789E+18 0.00047  4.17833E+17 0.00045  1.15005E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.87971E+14 0.00502 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.19874E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.20899E+18 0.00058 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12503E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12503E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.67935E+00 0.00039 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.57997E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.84497E-01 0.00030 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23669E+00 0.00033 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94169E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96797E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.07750E+00 0.00050 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06779E+00 0.00050 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46152E+00 1.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02582E+02 1.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06777E+00 0.00052  1.06027E+00 0.00051  7.51450E-03 0.00827 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06862E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06882E+00 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06862E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.07834E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75683E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75665E+01 8.3E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.70295E-07 0.00329 ];
IMP_EALF                  (idx, [1:   2]) = [  4.70126E-07 0.00146 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.12783E-01 0.00294 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.13350E-01 0.00118 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.70210E-03 0.00570  1.92405E-04 0.03320  1.07809E-03 0.01263  1.07120E-03 0.01329  3.04543E-03 0.00811  9.97120E-04 0.01416  3.17856E-04 0.02381 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.90351E-01 0.01223  1.06171E-02 0.01881  3.16407E-02 0.00023  1.10212E-01 0.00028  3.20685E-01 0.00021  1.34559E+00 0.00016  8.57650E+00 0.00881 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.14724E-03 0.00793  2.10218E-04 0.04908  1.13354E-03 0.01952  1.13667E-03 0.02024  3.24580E-03 0.01231  1.07573E-03 0.01991  3.45282E-04 0.03662 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.01363E-01 0.01851  1.24907E-02 2.6E-06  3.16499E-02 0.00030  1.10239E-01 0.00041  3.20715E-01 0.00033  1.34531E+00 0.00024  8.90523E+00 0.00209 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.89858E-05 0.00113  2.89720E-05 0.00113  3.09874E-05 0.01184 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.09461E-05 0.00100  3.09314E-05 0.00101  3.30850E-05 0.01184 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.03625E-03 0.00847  2.04327E-04 0.05005  1.12642E-03 0.02026  1.13450E-03 0.02158  3.15517E-03 0.01287  1.06712E-03 0.02136  3.48706E-04 0.03726 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.09397E-01 0.01994  1.24907E-02 3.3E-06  3.16374E-02 0.00038  1.10163E-01 0.00045  3.20798E-01 0.00035  1.34517E+00 0.00027  8.90320E+00 0.00241 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.90323E-05 0.00272  2.90167E-05 0.00273  3.05002E-05 0.02926 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.09954E-05 0.00266  3.09789E-05 0.00267  3.25457E-05 0.02926 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.85646E-03 0.02625  1.99152E-04 0.15201  1.13555E-03 0.06360  1.03782E-03 0.06751  3.20878E-03 0.04094  1.01365E-03 0.07242  2.61506E-04 0.11846 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.28800E-01 0.05875  1.24908E-02 7.9E-06  3.16807E-02 0.00079  1.10195E-01 0.00108  3.20724E-01 0.00100  1.34569E+00 0.00064  8.92715E+00 0.00608 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.90426E-03 0.02526  1.94901E-04 0.14580  1.11992E-03 0.06280  1.08047E-03 0.06602  3.23687E-03 0.03934  1.00530E-03 0.06930  2.66799E-04 0.11485 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.27153E-01 0.05605  1.24908E-02 7.9E-06  3.16777E-02 0.00079  1.10176E-01 0.00105  3.20785E-01 0.00099  1.34561E+00 0.00064  8.92550E+00 0.00606 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.37640E+02 0.02672 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.90168E-05 0.00070 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.09793E-05 0.00048 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.96880E-03 0.00500 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.40220E+02 0.00505 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.59894E-07 0.00059 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87323E-06 0.00044  2.87304E-06 0.00044  2.90102E-06 0.00498 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.42809E-05 0.00076  4.43045E-05 0.00076  4.10955E-05 0.00796 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.81912E-01 0.00030  6.81469E-01 0.00030  7.60572E-01 0.00908 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04591E+01 0.01285 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.78031E+01 0.00045  3.64339E+01 0.00048 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.17307E+04 0.00298  2.90964E+05 0.00164  6.01920E+05 0.00115  6.49375E+05 0.00101  5.97499E+05 0.00065  6.40897E+05 0.00073  4.35791E+05 0.00083  3.85222E+05 0.00071  2.95113E+05 0.00090  2.40693E+05 0.00078  2.07635E+05 0.00085  1.87054E+05 0.00073  1.72647E+05 0.00079  1.63967E+05 0.00090  1.59816E+05 0.00077  1.37912E+05 0.00087  1.36528E+05 0.00105  1.35017E+05 0.00101  1.32651E+05 0.00098  2.59321E+05 0.00050  2.50277E+05 0.00075  1.81069E+05 0.00075  1.17465E+05 0.00104  1.35813E+05 0.00109  1.28403E+05 0.00104  1.16962E+05 0.00092  1.92461E+05 0.00074  4.39536E+04 0.00153  5.51445E+04 0.00157  5.00465E+04 0.00157  2.90831E+04 0.00183  5.03945E+04 0.00116  3.42864E+04 0.00187  2.93235E+04 0.00144  5.57596E+03 0.00396  5.52384E+03 0.00417  5.67759E+03 0.00264  5.89108E+03 0.00302  5.81815E+03 0.00385  5.72032E+03 0.00335  5.94303E+03 0.00410  5.54924E+03 0.00426  1.04994E+04 0.00241  1.67567E+04 0.00229  2.13731E+04 0.00195  5.65524E+04 0.00129  5.98400E+04 0.00158  6.58726E+04 0.00129  4.58066E+04 0.00143  3.47707E+04 0.00153  2.71012E+04 0.00159  3.22783E+04 0.00131  6.20741E+04 0.00119  8.48123E+04 0.00105  1.64147E+05 0.00116  2.54367E+05 0.00104  3.80672E+05 0.00101  2.43422E+05 0.00107  1.74511E+05 0.00121  1.25379E+05 0.00127  1.12489E+05 0.00128  1.10794E+05 0.00116  9.26099E+04 0.00129  6.25659E+04 0.00113  5.77177E+04 0.00126  5.12939E+04 0.00149  4.34054E+04 0.00148  3.42132E+04 0.00126  2.28604E+04 0.00140  8.13067E+03 0.00172 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.07855E+00 0.00052 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.19247E+18 0.00051  3.75458E+17 0.00095 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38607E-01 0.00013  1.50950E+00 0.00037 ];
INF_CAPT                  (idx, [1:   4]) = [  6.28060E-03 0.00050  2.75526E-02 0.00032 ];
INF_ABS                   (idx, [1:   4]) = [  8.41294E-03 0.00042  5.77171E-02 0.00065 ];
INF_FISS                  (idx, [1:   4]) = [  2.13234E-03 0.00046  3.01645E-02 0.00097 ];
INF_NSF                   (idx, [1:   4]) = [  5.47550E-03 0.00046  7.35304E-02 0.00097 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.56783E+00 6.5E-05  2.43765E+00 4.4E-07 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03916E+02 6.7E-06  2.02282E+02 6.9E-08 ];
INF_INVV                  (idx, [1:   4]) = [  5.93865E-08 0.00034  2.56725E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.30194E-01 0.00012  1.45179E+00 0.00041 ];
INF_SCATT1                (idx, [1:   4]) = [  2.45016E-01 0.00020  3.82951E-01 0.00040 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65899E-02 0.00031  9.07700E-02 0.00076 ];
INF_SCATT3                (idx, [1:   4]) = [  7.37057E-03 0.00337  2.71568E-02 0.00217 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03762E-02 0.00165 -8.62219E-03 0.00573 ];
INF_SCATT5                (idx, [1:   4]) = [  1.13754E-04 0.15097  6.33285E-03 0.00786 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07391E-03 0.00242 -1.63875E-02 0.00313 ];
INF_SCATT7                (idx, [1:   4]) = [  7.47285E-04 0.01673  4.29827E-04 0.09320 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.30233E-01 0.00012  1.45179E+00 0.00041 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.45016E-01 0.00020  3.82951E-01 0.00040 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65896E-02 0.00031  9.07700E-02 0.00076 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.37065E-03 0.00337  2.71568E-02 0.00217 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03761E-02 0.00164 -8.62219E-03 0.00573 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.13617E-04 0.15115  6.33285E-03 0.00786 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07383E-03 0.00242 -1.63875E-02 0.00313 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.47278E-04 0.01675  4.29827E-04 0.09320 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13383E-01 0.00029  9.73252E-01 0.00034 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56214E+00 0.00029  3.42495E-01 0.00034 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.37380E-03 0.00041  5.77171E-02 0.00065 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69942E-02 0.00024  5.87442E-02 0.00085 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11613E-01 0.00012  1.85810E-02 0.00028  1.04115E-03 0.00386  1.45075E+00 0.00041 ];
INF_S1                    (idx, [1:   8]) = [  2.39578E-01 0.00020  5.43718E-03 0.00084  4.43800E-04 0.00665  3.82507E-01 0.00040 ];
INF_S2                    (idx, [1:   8]) = [  9.81808E-02 0.00030 -1.59094E-03 0.00289  2.42561E-04 0.00887  9.05275E-02 0.00076 ];
INF_S3                    (idx, [1:   8]) = [  9.27790E-03 0.00262 -1.90733E-03 0.00203  8.85122E-05 0.02073  2.70683E-02 0.00219 ];
INF_S4                    (idx, [1:   8]) = [ -9.73525E-03 0.00164 -6.40984E-04 0.00540  2.63111E-06 0.55914 -8.62482E-03 0.00571 ];
INF_S5                    (idx, [1:   8]) = [  1.02824E-04 0.16609  1.09296E-05 0.29773 -3.48117E-05 0.03175  6.36766E-03 0.00780 ];
INF_S6                    (idx, [1:   8]) = [  5.22436E-03 0.00248 -1.50452E-04 0.01616 -4.41207E-05 0.02913 -1.63434E-02 0.00313 ];
INF_S7                    (idx, [1:   8]) = [  9.19339E-04 0.01413 -1.72054E-04 0.01691 -4.07580E-05 0.02306  4.70585E-04 0.08494 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11652E-01 0.00012  1.85810E-02 0.00028  1.04115E-03 0.00386  1.45075E+00 0.00041 ];
INF_SP1                   (idx, [1:   8]) = [  2.39579E-01 0.00020  5.43718E-03 0.00084  4.43800E-04 0.00665  3.82507E-01 0.00040 ];
INF_SP2                   (idx, [1:   8]) = [  9.81805E-02 0.00030 -1.59094E-03 0.00289  2.42561E-04 0.00887  9.05275E-02 0.00076 ];
INF_SP3                   (idx, [1:   8]) = [  9.27799E-03 0.00261 -1.90733E-03 0.00203  8.85122E-05 0.02073  2.70683E-02 0.00219 ];
INF_SP4                   (idx, [1:   8]) = [ -9.73516E-03 0.00163 -6.40984E-04 0.00540  2.63111E-06 0.55914 -8.62482E-03 0.00571 ];
INF_SP5                   (idx, [1:   8]) = [  1.02688E-04 0.16632  1.09296E-05 0.29773 -3.48117E-05 0.03175  6.36766E-03 0.00780 ];
INF_SP6                   (idx, [1:   8]) = [  5.22428E-03 0.00248 -1.50452E-04 0.01616 -4.41207E-05 0.02913 -1.63434E-02 0.00313 ];
INF_SP7                   (idx, [1:   8]) = [  9.19332E-04 0.01414 -1.72054E-04 0.01691 -4.07580E-05 0.02306  4.70585E-04 0.08494 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32939E-01 0.00071  1.08400E+00 0.00659 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34588E-01 0.00113  1.15910E+00 0.00736 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34677E-01 0.00098  1.15013E+00 0.00898 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29633E-01 0.00100  9.66754E-01 0.00637 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43101E+00 0.00071  3.07816E-01 0.00641 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42098E+00 0.00112  2.87947E-01 0.00724 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42042E+00 0.00098  2.90368E-01 0.00872 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45163E+00 0.00100  3.45132E-01 0.00635 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.14724E-03 0.00793  2.10218E-04 0.04908  1.13354E-03 0.01952  1.13667E-03 0.02024  3.24580E-03 0.01231  1.07573E-03 0.01991  3.45282E-04 0.03662 ];
LAMBDA                    (idx, [1:  14]) = [  8.01363E-01 0.01851  1.24907E-02 2.6E-06  3.16499E-02 0.00030  1.10239E-01 0.00041  3.20715E-01 0.00033  1.34531E+00 0.00024  8.90523E+00 0.00209 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0001' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:49:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:06:32 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292590 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00280E+00  1.00288E+00  9.99500E-01  9.94021E-01  1.00080E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.6E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.11941E-02 0.00104  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88806E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.99261E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.99676E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.71340E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.76707E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.76616E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.43402E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.43889E-01 0.00109  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000776 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00016E+04 0.00076 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00016E+04 0.00076 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  8.14556E+01 ;
RUNNING_TIME              (idx, 1)        =  1.66913E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.86050E-01  4.86050E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.94000E-02  7.31666E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.60617E+01  3.49942E+00  2.72570E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.13117E-01  2.79333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  6.68333E-03  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.66912E+01  1.30751E+02 ];
CPU_USAGE                 (idx, 1)        = 4.88011 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99890E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.57661E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.81309E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.84271E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.45042E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.66752E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.16538E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.14555E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.72615E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.71359E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.04112E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  9.21662E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.80382E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  1.79193E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  2.36074E+07 ;
SR90_ACTIVITY             (idx, 1)        =  6.36962E+11 ;
TE132_ACTIVITY            (idx, 1)        =  5.63683E+14 ;
I131_ACTIVITY             (idx, 1)        =  2.65460E+14 ;
I132_ACTIVITY             (idx, 1)        =  5.66679E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.83213E+09 ;
CS137_ACTIVITY            (idx, 1)        =  6.73782E+11 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.74959E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.64045E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  7.01347E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.13756E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.23106E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 2 ;
BURNUP                     (idx, [1:  2])  = [  5.00000E-01  5.00047E-01 ];
BURN_DAYS                 (idx, 1)        =  1.25000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.35686E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  1.26409E+16 0.00062  9.12154E-01 0.00022 ];
U238_FISS                 (idx, [1:   4]) = [  8.88688E+14 0.00277  6.41194E-02 0.00262 ];
PU239_FISS                (idx, [1:   4]) = [  3.26010E+14 0.00449  2.35227E-02 0.00443 ];
PU240_FISS                (idx, [1:   4]) = [  6.55513E+09 1.00000  4.76872E-07 1.00000 ];
PU241_FISS                (idx, [1:   4]) = [  2.07956E+11 0.18806  1.49284E-05 0.18785 ];
U235_CAPT                 (idx, [1:   4]) = [  2.71029E+15 0.00157  1.48885E-01 0.00149 ];
U238_CAPT                 (idx, [1:   4]) = [  8.42843E+15 0.00100  4.62961E-01 0.00065 ];
PU239_CAPT                (idx, [1:   4]) = [  1.82822E+14 0.00608  1.00419E-02 0.00603 ];
PU240_CAPT                (idx, [1:   4]) = [  8.49192E+12 0.02693  4.66661E-04 0.02695 ];
PU241_CAPT                (idx, [1:   4]) = [  5.78402E+10 0.36581  3.22317E-06 0.36749 ];
XE135_CAPT                (idx, [1:   4]) = [  7.32521E+14 0.00292  4.02441E-02 0.00295 ];
SM149_CAPT                (idx, [1:   4]) = [  1.27012E+14 0.00689  6.97724E-03 0.00687 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000776 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.15624E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000776 5.00716E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2813584 2.81714E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2141812 2.14463E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45380 4.53881E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000776 5.00716E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -7.35745E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.42446E+16 1.2E-05  3.42446E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38577E+16 1.4E-06  1.38577E+16 1.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.81900E+16 0.00046  1.23977E+16 0.00048  5.79230E+15 0.00096 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.20476E+16 0.00026  2.62553E+16 0.00023  5.79230E+15 0.00096 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.23106E+16 0.00050  3.23106E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.58226E+18 0.00046  4.21118E+17 0.00044  1.16114E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.93344E+14 0.00483 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.23410E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.21842E+18 0.00057 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12456E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12456E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.66237E+00 0.00044 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.59875E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.84490E-01 0.00029 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23717E+00 0.00033 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94117E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96786E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.06965E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.05994E+00 0.00052 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.47116E+00 1.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02707E+02 1.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.05986E+00 0.00053  1.05254E+00 0.00053  7.39614E-03 0.00833 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06042E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.05999E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06042E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.07014E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75379E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75382E+01 7.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.84773E-07 0.00325 ];
IMP_EALF                  (idx, [1:   2]) = [  4.83630E-07 0.00139 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.16041E-01 0.00278 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.16193E-01 0.00114 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.61753E-03 0.00568  1.88871E-04 0.03248  1.07360E-03 0.01390  1.01369E-03 0.01364  3.02648E-03 0.00736  9.85041E-04 0.01446  3.29848E-04 0.02461 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.13431E-01 0.01261  1.05921E-02 0.01895  3.16136E-02 0.00025  1.10207E-01 0.00030  3.20837E-01 0.00023  1.34500E+00 0.00017  8.60669E+00 0.00885 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.00871E-03 0.00762  1.93108E-04 0.04693  1.10877E-03 0.01980  1.08226E-03 0.02084  3.20544E-03 0.01116  1.05861E-03 0.02146  3.60522E-04 0.03513 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.28035E-01 0.01851  1.24907E-02 4.0E-06  3.16083E-02 0.00035  1.10274E-01 0.00044  3.20765E-01 0.00034  1.34516E+00 0.00025  8.91665E+00 0.00220 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.87777E-05 0.00116  2.87655E-05 0.00116  3.05368E-05 0.01145 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.04961E-05 0.00103  3.04830E-05 0.00103  3.23630E-05 0.01145 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.98439E-03 0.00851  2.00972E-04 0.04854  1.11091E-03 0.01955  1.09573E-03 0.02101  3.17643E-03 0.01189  1.05795E-03 0.02061  3.42395E-04 0.03968 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.05617E-01 0.02018  1.24907E-02 4.3E-06  3.16195E-02 0.00040  1.10233E-01 0.00047  3.20834E-01 0.00036  1.34549E+00 0.00028  8.91158E+00 0.00264 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.87192E-05 0.00260  2.86949E-05 0.00262  3.17940E-05 0.02965 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.04345E-05 0.00255  3.04087E-05 0.00257  3.36971E-05 0.02966 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.32447E-03 0.02482  2.06818E-04 0.16039  1.13765E-03 0.06425  1.04577E-03 0.06590  3.33510E-03 0.03899  1.20644E-03 0.06765  3.92692E-04 0.10699 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.82332E-01 0.06184  1.24908E-02 9.1E-06  3.16147E-02 0.00093  1.09979E-01 0.00100  3.21341E-01 0.00109  1.34595E+00 0.00062  8.91700E+00 0.00540 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.31704E-03 0.02436  1.92244E-04 0.15182  1.14953E-03 0.06317  1.05406E-03 0.06349  3.33972E-03 0.03844  1.18334E-03 0.06498  3.98148E-04 0.10094 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.77263E-01 0.05877  1.24908E-02 9.1E-06  3.16129E-02 0.00093  1.09986E-01 0.00100  3.21408E-01 0.00108  1.34582E+00 0.00062  8.91787E+00 0.00541 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.56428E+02 0.02497 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.87406E-05 0.00069 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.04569E-05 0.00045 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.11703E-03 0.00469 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.47736E+02 0.00483 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.54750E-07 0.00058 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87048E-06 0.00044  2.87023E-06 0.00044  2.90732E-06 0.00475 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.38050E-05 0.00073  4.38317E-05 0.00074  4.01794E-05 0.00814 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.81877E-01 0.00029  6.81455E-01 0.00029  7.58239E-01 0.00890 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.01285E+01 0.01254 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.76616E+01 0.00043  3.62181E+01 0.00045 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.21760E+04 0.00285  2.92114E+05 0.00151  6.03026E+05 0.00104  6.48977E+05 0.00059  5.97477E+05 0.00057  6.41665E+05 0.00069  4.35564E+05 0.00060  3.85000E+05 0.00060  2.94553E+05 0.00084  2.40542E+05 0.00083  2.07432E+05 0.00074  1.87519E+05 0.00084  1.72743E+05 0.00064  1.63964E+05 0.00087  1.59957E+05 0.00081  1.38236E+05 0.00064  1.36324E+05 0.00110  1.35185E+05 0.00089  1.32522E+05 0.00119  2.59481E+05 0.00059  2.50749E+05 0.00077  1.81004E+05 0.00072  1.17171E+05 0.00104  1.35610E+05 0.00079  1.28344E+05 0.00101  1.16687E+05 0.00110  1.92124E+05 0.00079  4.38514E+04 0.00162  5.50401E+04 0.00141  5.00413E+04 0.00117  2.90288E+04 0.00156  5.04940E+04 0.00109  3.43076E+04 0.00131  2.92581E+04 0.00189  5.59581E+03 0.00284  5.55451E+03 0.00291  5.65614E+03 0.00366  5.84812E+03 0.00417  5.78870E+03 0.00321  5.72998E+03 0.00358  5.91787E+03 0.00364  5.56210E+03 0.00379  1.04822E+04 0.00271  1.68289E+04 0.00226  2.14633E+04 0.00225  5.65215E+04 0.00148  5.95970E+04 0.00148  6.56622E+04 0.00123  4.53911E+04 0.00155  3.43557E+04 0.00118  2.66001E+04 0.00195  3.17046E+04 0.00136  6.11675E+04 0.00118  8.36080E+04 0.00102  1.62386E+05 0.00093  2.51340E+05 0.00098  3.76425E+05 0.00094  2.40507E+05 0.00104  1.72546E+05 0.00101  1.24030E+05 0.00114  1.11191E+05 0.00129  1.09683E+05 0.00141  9.15175E+04 0.00122  6.19035E+04 0.00114  5.70758E+04 0.00108  5.08603E+04 0.00122  4.30816E+04 0.00162  3.38400E+04 0.00141  2.26535E+04 0.00138  8.01893E+03 0.00181 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.06970E+00 0.00046 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.20666E+18 0.00052  3.75633E+17 0.00080 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38530E-01 0.00010  1.51379E+00 0.00028 ];
INF_CAPT                  (idx, [1:   4]) = [  6.29472E-03 0.00064  2.82063E-02 0.00029 ];
INF_ABS                   (idx, [1:   4]) = [  8.41022E-03 0.00051  5.83069E-02 0.00056 ];
INF_FISS                  (idx, [1:   4]) = [  2.11549E-03 0.00044  3.01006E-02 0.00081 ];
INF_NSF                   (idx, [1:   4]) = [  5.44059E-03 0.00044  7.36998E-02 0.00081 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.57178E+00 5.5E-05  2.44845E+00 4.6E-06 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03964E+02 5.7E-06  2.02423E+02 7.2E-07 ];
INF_INVV                  (idx, [1:   4]) = [  5.93442E-08 0.00032  2.56749E-06 0.00012 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.30120E-01 0.00010  1.45545E+00 0.00031 ];
INF_SCATT1                (idx, [1:   4]) = [  2.45029E-01 0.00016  3.84133E-01 0.00044 ];
INF_SCATT2                (idx, [1:   4]) = [  9.66252E-02 0.00025  9.09006E-02 0.00094 ];
INF_SCATT3                (idx, [1:   4]) = [  7.36686E-03 0.00294  2.73664E-02 0.00213 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03971E-02 0.00161 -8.54870E-03 0.00505 ];
INF_SCATT5                (idx, [1:   4]) = [  9.45795E-05 0.23728  6.33112E-03 0.00796 ];
INF_SCATT6                (idx, [1:   4]) = [  5.03710E-03 0.00336 -1.65365E-02 0.00267 ];
INF_SCATT7                (idx, [1:   4]) = [  7.27392E-04 0.01827  4.14523E-04 0.09534 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.30158E-01 0.00010  1.45545E+00 0.00031 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.45029E-01 0.00016  3.84133E-01 0.00044 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.66251E-02 0.00025  9.09006E-02 0.00094 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.36682E-03 0.00293  2.73664E-02 0.00213 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03972E-02 0.00160 -8.54870E-03 0.00505 ];
INF_SCATTP5               (idx, [1:   4]) = [  9.46099E-05 0.23761  6.33112E-03 0.00796 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.03702E-03 0.00336 -1.65365E-02 0.00267 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.27617E-04 0.01823  4.14523E-04 0.09534 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13269E-01 0.00027  9.76149E-01 0.00026 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56298E+00 0.00027  3.41478E-01 0.00026 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.37190E-03 0.00051  5.83069E-02 0.00056 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69835E-02 0.00020  5.93900E-02 0.00070 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11546E-01 0.00010  1.85735E-02 0.00037  1.04647E-03 0.00496  1.45440E+00 0.00031 ];
INF_S1                    (idx, [1:   8]) = [  2.39587E-01 0.00016  5.44210E-03 0.00096  4.48621E-04 0.00856  3.83685E-01 0.00043 ];
INF_S2                    (idx, [1:   8]) = [  9.82184E-02 0.00025 -1.59320E-03 0.00238  2.47995E-04 0.01033  9.06526E-02 0.00094 ];
INF_S3                    (idx, [1:   8]) = [  9.27672E-03 0.00221 -1.90986E-03 0.00196  9.05721E-05 0.01850  2.72758E-02 0.00215 ];
INF_S4                    (idx, [1:   8]) = [ -9.75784E-03 0.00161 -6.39280E-04 0.00401  1.40067E-06 1.00000 -8.55010E-03 0.00506 ];
INF_S5                    (idx, [1:   8]) = [  8.41377E-05 0.26180  1.04419E-05 0.31837 -3.49222E-05 0.04463  6.36604E-03 0.00786 ];
INF_S6                    (idx, [1:   8]) = [  5.18709E-03 0.00323 -1.49986E-04 0.01362 -4.48521E-05 0.02861 -1.64917E-02 0.00265 ];
INF_S7                    (idx, [1:   8]) = [  8.99604E-04 0.01442 -1.72212E-04 0.00998 -3.79572E-05 0.02565  4.52481E-04 0.08705 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11584E-01 1.0E-04  1.85735E-02 0.00037  1.04647E-03 0.00496  1.45440E+00 0.00031 ];
INF_SP1                   (idx, [1:   8]) = [  2.39587E-01 0.00016  5.44210E-03 0.00096  4.48621E-04 0.00856  3.83685E-01 0.00043 ];
INF_SP2                   (idx, [1:   8]) = [  9.82183E-02 0.00025 -1.59320E-03 0.00238  2.47995E-04 0.01033  9.06526E-02 0.00094 ];
INF_SP3                   (idx, [1:   8]) = [  9.27667E-03 0.00220 -1.90986E-03 0.00196  9.05721E-05 0.01850  2.72758E-02 0.00215 ];
INF_SP4                   (idx, [1:   8]) = [ -9.75790E-03 0.00160 -6.39280E-04 0.00401  1.40067E-06 1.00000 -8.55010E-03 0.00506 ];
INF_SP5                   (idx, [1:   8]) = [  8.41680E-05 0.26216  1.04419E-05 0.31837 -3.49222E-05 0.04463  6.36604E-03 0.00786 ];
INF_SP6                   (idx, [1:   8]) = [  5.18701E-03 0.00323 -1.49986E-04 0.01362 -4.48521E-05 0.02861 -1.64917E-02 0.00265 ];
INF_SP7                   (idx, [1:   8]) = [  8.99829E-04 0.01439 -1.72212E-04 0.00998 -3.79572E-05 0.02565  4.52481E-04 0.08705 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32649E-01 0.00047  1.10883E+00 0.00589 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34839E-01 0.00082  1.19090E+00 0.00708 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34151E-01 0.00075  1.18177E+00 0.00839 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29052E-01 0.00092  9.81629E-01 0.00566 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43278E+00 0.00047  3.00869E-01 0.00591 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.41943E+00 0.00082  2.80239E-01 0.00714 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42360E+00 0.00075  2.82531E-01 0.00824 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45530E+00 0.00092  3.39835E-01 0.00572 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.00871E-03 0.00762  1.93108E-04 0.04693  1.10877E-03 0.01980  1.08226E-03 0.02084  3.20544E-03 0.01116  1.05861E-03 0.02146  3.60522E-04 0.03513 ];
LAMBDA                    (idx, [1:  14]) = [  8.28035E-01 0.01851  1.24907E-02 4.0E-06  3.16083E-02 0.00035  1.10274E-01 0.00044  3.20765E-01 0.00034  1.34516E+00 0.00025  8.91665E+00 0.00220 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0001' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:49:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:12:48 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292590 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00131E+00  1.00245E+00  1.00081E+00  9.93218E-01  1.00221E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12333E-02 0.00107  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88767E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.99369E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.99787E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.71128E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.73927E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.73838E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.35788E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.41503E-01 0.00112  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000743 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00015E+04 0.00076 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00015E+04 0.00076 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.12797E+02 ;
RUNNING_TIME              (idx, 1)        =  2.29653E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.86050E-01  4.86050E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  4.52333E-02  7.53333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  2.22635E+01  3.48342E+00  2.71842E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.68883E-01  2.66000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  9.96666E-03  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  2.29652E+01  1.29829E+02 ];
CPU_USAGE                 (idx, 1)        = 4.91160 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99923E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.65428E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.01958E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.85823E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.50893E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.69589E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.18428E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.32367E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.73977E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  3.58838E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.51760E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.05980E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.01094E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  2.52858E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  2.81651E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.26132E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.05259E+14 ;
I131_ACTIVITY             (idx, 1)        =  3.60403E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.10646E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.68258E+10 ;
CS137_ACTIVITY            (idx, 1)        =  1.34857E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.94731E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.61928E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.56292E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.17641E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.25339E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 3 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E+00  1.00009E+00 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.35334E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  1.22209E+16 0.00059  8.82744E-01 0.00025 ];
U238_FISS                 (idx, [1:   4]) = [  8.93338E+14 0.00287  6.45200E-02 0.00273 ];
PU239_FISS                (idx, [1:   4]) = [  7.25933E+14 0.00312  5.24336E-02 0.00304 ];
PU240_FISS                (idx, [1:   4]) = [  6.44273E+10 0.31339  4.64483E-06 0.31338 ];
PU241_FISS                (idx, [1:   4]) = [  1.33661E+12 0.07377  9.65938E-05 0.07381 ];
U235_CAPT                 (idx, [1:   4]) = [  2.62186E+15 0.00157  1.42141E-01 0.00149 ];
U238_CAPT                 (idx, [1:   4]) = [  8.45822E+15 0.00101  4.58516E-01 0.00065 ];
PU239_CAPT                (idx, [1:   4]) = [  4.07448E+14 0.00415  2.20908E-02 0.00416 ];
PU240_CAPT                (idx, [1:   4]) = [  3.65910E+13 0.01330  1.98436E-03 0.01334 ];
PU241_CAPT                (idx, [1:   4]) = [  4.23365E+11 0.12567  2.28963E-05 0.12574 ];
XE135_CAPT                (idx, [1:   4]) = [  7.36565E+14 0.00306  3.99325E-02 0.00302 ];
SM149_CAPT                (idx, [1:   4]) = [  1.48873E+14 0.00676  8.07079E-03 0.00674 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000743 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.27761E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000743 5.00728E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2831129 2.83489E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2125005 2.12778E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44609 4.46133E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000743 5.00728E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -7.17118E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.43934E+16 1.2E-05  3.43934E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38463E+16 1.5E-06  1.38463E+16 1.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.84325E+16 0.00047  1.26885E+16 0.00048  5.74395E+15 0.00103 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.22788E+16 0.00027  2.65349E+16 0.00023  5.74395E+15 0.00103 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.25339E+16 0.00049  3.25339E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.58723E+18 0.00046  4.23160E+17 0.00044  1.16407E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.90320E+14 0.00464 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.25691E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.21786E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12398E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12398E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.65332E+00 0.00046 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.63414E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.83297E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23672E+00 0.00033 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94215E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96844E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.06652E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.05700E+00 0.00052 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.48394E+00 1.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02873E+02 1.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.05695E+00 0.00054  1.04981E+00 0.00053  7.19102E-03 0.00832 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.05759E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.05728E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.05759E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.06711E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75168E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75142E+01 8.3E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.95229E-07 0.00340 ];
IMP_EALF                  (idx, [1:   2]) = [  4.95375E-07 0.00145 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.17567E-01 0.00297 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.17997E-01 0.00110 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.49709E-03 0.00571  1.86078E-04 0.03232  1.03434E-03 0.01389  1.01543E-03 0.01357  2.98247E-03 0.00870  9.53857E-04 0.01417  3.24918E-04 0.02495 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.14139E-01 0.01293  1.07169E-02 0.01821  3.15842E-02 0.00026  1.10201E-01 0.00029  3.20897E-01 0.00023  1.34548E+00 0.00018  8.62801E+00 0.00885 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.79934E-03 0.00856  1.98774E-04 0.04674  1.06825E-03 0.02043  1.02977E-03 0.02061  3.17782E-03 0.01298  9.91189E-04 0.02115  3.33536E-04 0.03600 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.09961E-01 0.01908  1.24905E-02 6.1E-06  3.15751E-02 0.00038  1.10209E-01 0.00043  3.20898E-01 0.00035  1.34504E+00 0.00025  8.95743E+00 0.00231 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.83182E-05 0.00114  2.83081E-05 0.00114  2.97903E-05 0.01289 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.99272E-05 0.00104  2.99165E-05 0.00104  3.14782E-05 0.01286 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.79327E-03 0.00836  1.92772E-04 0.04997  1.09013E-03 0.02143  1.02504E-03 0.02174  3.14930E-03 0.01258  9.89490E-04 0.02158  3.46542E-04 0.03772 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.17212E-01 0.02017  1.24906E-02 7.0E-06  3.15851E-02 0.00044  1.10125E-01 0.00047  3.20835E-01 0.00036  1.34514E+00 0.00028  8.93181E+00 0.00266 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.83404E-05 0.00262  2.83279E-05 0.00262  2.98824E-05 0.03022 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.99519E-05 0.00261  2.99386E-05 0.00261  3.15804E-05 0.03018 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.63562E-03 0.02741  1.53084E-04 0.15761  1.20353E-03 0.06278  8.34161E-04 0.06944  3.17463E-03 0.04056  9.55998E-04 0.06680  3.14209E-04 0.13830 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.48796E-01 0.06366  1.24908E-02 9.6E-06  3.16261E-02 0.00092  1.10217E-01 0.00121  3.20624E-01 0.00102  1.34466E+00 0.00067  8.88198E+00 0.00596 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.63598E-03 0.02698  1.58279E-04 0.16022  1.17114E-03 0.06080  8.56013E-04 0.06889  3.17785E-03 0.03904  9.60317E-04 0.06580  3.12385E-04 0.13210 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.54626E-01 0.06118  1.24908E-02 9.6E-06  3.16263E-02 0.00091  1.10247E-01 0.00123  3.20639E-01 0.00102  1.34466E+00 0.00067  8.88349E+00 0.00597 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.34544E+02 0.02740 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.83113E-05 0.00071 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.99195E-05 0.00050 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.77865E-03 0.00484 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.39452E+02 0.00482 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.46899E-07 0.00062 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87010E-06 0.00043  2.86991E-06 0.00043  2.89638E-06 0.00501 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.31237E-05 0.00078  4.31474E-05 0.00078  3.97683E-05 0.00876 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.80751E-01 0.00031  6.80373E-01 0.00031  7.49710E-01 0.00857 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02112E+01 0.01372 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.73838E+01 0.00045  3.59283E+01 0.00045 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.24703E+04 0.00404  2.91692E+05 0.00180  6.02784E+05 0.00074  6.48595E+05 0.00087  5.98216E+05 0.00070  6.42130E+05 0.00068  4.36359E+05 0.00060  3.85350E+05 0.00070  2.94474E+05 0.00063  2.40821E+05 0.00077  2.07411E+05 0.00075  1.86865E+05 0.00082  1.72766E+05 0.00075  1.64063E+05 0.00088  1.59825E+05 0.00070  1.38331E+05 0.00115  1.36507E+05 0.00083  1.35323E+05 0.00089  1.33087E+05 0.00100  2.59688E+05 0.00067  2.50724E+05 0.00053  1.80933E+05 0.00066  1.17549E+05 0.00109  1.35521E+05 0.00089  1.28378E+05 0.00067  1.16739E+05 0.00085  1.91923E+05 0.00071  4.38507E+04 0.00137  5.50733E+04 0.00140  4.98882E+04 0.00183  2.90112E+04 0.00178  5.05695E+04 0.00185  3.42321E+04 0.00215  2.93568E+04 0.00192  5.62730E+03 0.00285  5.54847E+03 0.00271  5.62520E+03 0.00372  5.77998E+03 0.00410  5.76749E+03 0.00300  5.68734E+03 0.00312  5.92025E+03 0.00390  5.56428E+03 0.00423  1.05382E+04 0.00246  1.68451E+04 0.00225  2.14272E+04 0.00217  5.65049E+04 0.00152  5.95372E+04 0.00128  6.55217E+04 0.00164  4.48308E+04 0.00135  3.37638E+04 0.00168  2.60325E+04 0.00194  3.09889E+04 0.00165  5.96885E+04 0.00115  8.19282E+04 0.00141  1.59303E+05 0.00106  2.46877E+05 0.00115  3.69540E+05 0.00095  2.36695E+05 0.00124  1.69445E+05 0.00093  1.21729E+05 0.00116  1.09473E+05 0.00129  1.07804E+05 0.00115  9.00683E+04 0.00103  6.09272E+04 0.00130  5.62339E+04 0.00136  5.00070E+04 0.00131  4.22687E+04 0.00166  3.33592E+04 0.00169  2.23061E+04 0.00165  7.89260E+03 0.00167 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.06680E+00 0.00052 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.21536E+18 0.00053  3.71897E+17 0.00083 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38567E-01 0.00011  1.51473E+00 0.00030 ];
INF_CAPT                  (idx, [1:   4]) = [  6.34358E-03 0.00081  2.88346E-02 0.00031 ];
INF_ABS                   (idx, [1:   4]) = [  8.44621E-03 0.00062  5.91996E-02 0.00059 ];
INF_FISS                  (idx, [1:   4]) = [  2.10263E-03 0.00051  3.03649E-02 0.00087 ];
INF_NSF                   (idx, [1:   4]) = [  5.41746E-03 0.00051  7.47883E-02 0.00087 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.57652E+00 5.7E-05  2.46298E+00 7.6E-06 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04023E+02 4.2E-06  2.02613E+02 1.2E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.93123E-08 0.00044  2.56723E-06 0.00012 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.30126E-01 0.00012  1.45548E+00 0.00034 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44895E-01 0.00018  3.84325E-01 0.00038 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65677E-02 0.00028  9.10088E-02 0.00097 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34355E-03 0.00362  2.73611E-02 0.00294 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.04121E-02 0.00147 -8.49245E-03 0.00589 ];
INF_SCATT5                (idx, [1:   4]) = [  8.28059E-05 0.20370  6.47837E-03 0.00839 ];
INF_SCATT6                (idx, [1:   4]) = [  5.05249E-03 0.00303 -1.64058E-02 0.00278 ];
INF_SCATT7                (idx, [1:   4]) = [  7.28379E-04 0.01854  4.29490E-04 0.08922 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.30165E-01 0.00012  1.45548E+00 0.00034 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44896E-01 0.00018  3.84325E-01 0.00038 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65679E-02 0.00027  9.10088E-02 0.00097 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34336E-03 0.00362  2.73611E-02 0.00294 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.04123E-02 0.00147 -8.49245E-03 0.00589 ];
INF_SCATTP5               (idx, [1:   4]) = [  8.28833E-05 0.20332  6.47837E-03 0.00839 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.05252E-03 0.00304 -1.64058E-02 0.00278 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.28418E-04 0.01856  4.29490E-04 0.08922 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13426E-01 0.00022  9.77542E-01 0.00028 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56183E+00 0.00022  3.40992E-01 0.00028 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.40725E-03 0.00063  5.91996E-02 0.00059 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69775E-02 0.00021  6.03030E-02 0.00070 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11590E-01 0.00011  1.85364E-02 0.00042  1.06035E-03 0.00423  1.45442E+00 0.00034 ];
INF_S1                    (idx, [1:   8]) = [  2.39471E-01 0.00018  5.42416E-03 0.00085  4.50487E-04 0.00661  3.83875E-01 0.00038 ];
INF_S2                    (idx, [1:   8]) = [  9.81556E-02 0.00027 -1.58789E-03 0.00264  2.44680E-04 0.00769  9.07641E-02 0.00097 ];
INF_S3                    (idx, [1:   8]) = [  9.24878E-03 0.00285 -1.90523E-03 0.00138  8.68665E-05 0.01858  2.72743E-02 0.00296 ];
INF_S4                    (idx, [1:   8]) = [ -9.76933E-03 0.00154 -6.42776E-04 0.00437 -1.48781E-06 0.80932 -8.49096E-03 0.00598 ];
INF_S5                    (idx, [1:   8]) = [  6.95498E-05 0.24901  1.32561E-05 0.16332 -3.69851E-05 0.03507  6.51536E-03 0.00830 ];
INF_S6                    (idx, [1:   8]) = [  5.20047E-03 0.00289 -1.47985E-04 0.01534 -4.68103E-05 0.02794 -1.63590E-02 0.00280 ];
INF_S7                    (idx, [1:   8]) = [  9.02451E-04 0.01514 -1.74072E-04 0.01228 -3.98624E-05 0.02863  4.69352E-04 0.08156 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11629E-01 0.00011  1.85364E-02 0.00042  1.06035E-03 0.00423  1.45442E+00 0.00034 ];
INF_SP1                   (idx, [1:   8]) = [  2.39472E-01 0.00018  5.42416E-03 0.00085  4.50487E-04 0.00661  3.83875E-01 0.00038 ];
INF_SP2                   (idx, [1:   8]) = [  9.81558E-02 0.00027 -1.58789E-03 0.00264  2.44680E-04 0.00769  9.07641E-02 0.00097 ];
INF_SP3                   (idx, [1:   8]) = [  9.24859E-03 0.00285 -1.90523E-03 0.00138  8.68665E-05 0.01858  2.72743E-02 0.00296 ];
INF_SP4                   (idx, [1:   8]) = [ -9.76951E-03 0.00154 -6.42776E-04 0.00437 -1.48781E-06 0.80932 -8.49096E-03 0.00598 ];
INF_SP5                   (idx, [1:   8]) = [  6.96272E-05 0.24852  1.32561E-05 0.16332 -3.69851E-05 0.03507  6.51536E-03 0.00830 ];
INF_SP6                   (idx, [1:   8]) = [  5.20051E-03 0.00289 -1.47985E-04 0.01534 -4.68103E-05 0.02794 -1.63590E-02 0.00280 ];
INF_SP7                   (idx, [1:   8]) = [  9.02491E-04 0.01516 -1.74072E-04 0.01228 -3.98624E-05 0.02863  4.69352E-04 0.08156 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32968E-01 0.00059  1.10109E+00 0.00430 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34516E-01 0.00094  1.16934E+00 0.00549 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34899E-01 0.00070  1.17795E+00 0.00582 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29573E-01 0.00094  9.80496E-01 0.00434 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43082E+00 0.00059  3.02861E-01 0.00421 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42140E+00 0.00095  2.85265E-01 0.00542 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.41906E+00 0.00070  2.83205E-01 0.00574 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45200E+00 0.00094  3.40115E-01 0.00426 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.79934E-03 0.00856  1.98774E-04 0.04674  1.06825E-03 0.02043  1.02977E-03 0.02061  3.17782E-03 0.01298  9.91189E-04 0.02115  3.33536E-04 0.03600 ];
LAMBDA                    (idx, [1:  14]) = [  8.09961E-01 0.01908  1.24905E-02 6.1E-06  3.15751E-02 0.00038  1.10209E-01 0.00043  3.20898E-01 0.00035  1.34504E+00 0.00025  8.95743E+00 0.00231 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0001' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:49:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:19:05 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292590 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99821E-01  1.00450E+00  1.00142E+00  9.93652E-01  1.00061E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12187E-02 0.00103  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88781E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.00704E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.01122E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.70521E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.70921E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.70832E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.20530E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.36007E-01 0.00109  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000902 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00018E+04 0.00076 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00018E+04 0.00076 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.44110E+02 ;
RUNNING_TIME              (idx, 1)        =  2.92362E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.86050E-01  4.86050E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  6.24333E-02  8.56667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  2.84615E+01  3.49448E+00  2.70347E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  2.24017E-01  2.72000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.33833E-02  7.83332E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  2.92361E+01  1.29371E+02 ];
CPU_USAGE                 (idx, 1)        = 4.92916 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99723E+00 0.00061 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.69753E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.18315E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.85829E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.75681E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.70586E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.19106E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.47726E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.73916E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  4.79825E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.88084E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.34283E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.06789E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  3.45543E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.17405E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.47354E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.15004E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.09219E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.22263E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.57984E+11 ;
CS137_ACTIVITY            (idx, 1)        =  2.69974E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.06847E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.57968E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.42161E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.20337E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.30330E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 4 ;
BURNUP                     (idx, [1:  2])  = [  2.00000E+00  2.00018E+00 ];
BURN_DAYS                 (idx, 1)        =  5.00000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.38101E-01 0.00100 ];
U235_FISS                 (idx, [1:   4]) = [  1.14819E+16 0.00062  8.30704E-01 0.00031 ];
U238_FISS                 (idx, [1:   4]) = [  9.11702E+14 0.00296  6.59434E-02 0.00272 ];
PU239_FISS                (idx, [1:   4]) = [  1.41402E+15 0.00215  1.02302E-01 0.00208 ];
PU240_FISS                (idx, [1:   4]) = [  3.89711E+11 0.12707  2.82289E-05 0.12696 ];
PU241_FISS                (idx, [1:   4]) = [  1.07543E+13 0.02448  7.78019E-04 0.02448 ];
U235_CAPT                 (idx, [1:   4]) = [  2.47780E+15 0.00154  1.30661E-01 0.00142 ];
U238_CAPT                 (idx, [1:   4]) = [  8.51218E+15 0.00096  4.48857E-01 0.00065 ];
PU239_CAPT                (idx, [1:   4]) = [  7.90381E+14 0.00285  4.16817E-02 0.00283 ];
PU240_CAPT                (idx, [1:   4]) = [  1.35823E+14 0.00691  7.16224E-03 0.00689 ];
PU241_CAPT                (idx, [1:   4]) = [  3.59911E+12 0.04202  1.89713E-04 0.04202 ];
XE135_CAPT                (idx, [1:   4]) = [  7.33637E+14 0.00295  3.86918E-02 0.00297 ];
SM149_CAPT                (idx, [1:   4]) = [  1.56370E+14 0.00662  8.24667E-03 0.00663 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000902 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.42940E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000902 5.00743E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2866708 2.87041E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2089443 2.09226E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44751 4.47612E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000902 5.00743E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -9.31323E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.46529E+16 1.4E-05  3.46529E+16 1.4E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38265E+16 2.1E-06  1.38265E+16 2.1E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.89597E+16 0.00046  1.32175E+16 0.00045  5.74219E+15 0.00106 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.27861E+16 0.00026  2.70440E+16 0.00022  5.74219E+15 0.00106 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.30330E+16 0.00051  3.30330E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.60376E+18 0.00047  4.27273E+17 0.00046  1.17649E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.95782E+14 0.00515 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.30819E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.22664E+18 0.00061 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12281E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12281E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.64196E+00 0.00043 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.66696E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.80124E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23600E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94177E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96852E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.05819E+00 0.00051 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04872E+00 0.00051 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.50627E+00 1.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03164E+02 2.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04859E+00 0.00054  1.04173E+00 0.00052  6.99338E-03 0.00841 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.04907E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04917E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.04907E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.05854E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74702E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74655E+01 8.4E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.19095E-07 0.00365 ];
IMP_EALF                  (idx, [1:   2]) = [  5.20100E-07 0.00147 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.22163E-01 0.00295 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.22934E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.42121E-03 0.00546  1.84275E-04 0.03256  1.05530E-03 0.01366  1.01887E-03 0.01465  2.90956E-03 0.00799  9.47842E-04 0.01344  3.05354E-04 0.02486 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.92968E-01 0.01277  1.05919E-02 0.01895  3.15041E-02 0.00028  1.10197E-01 0.00033  3.21219E-01 0.00024  1.34433E+00 0.00023  8.64806E+00 0.00887 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.69845E-03 0.00858  1.86394E-04 0.05074  1.10963E-03 0.02072  1.06684E-03 0.02161  3.00694E-03 0.01184  1.00935E-03 0.02094  3.19300E-04 0.03643 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.92864E-01 0.01860  1.24904E-02 7.4E-06  3.15046E-02 0.00042  1.10206E-01 0.00048  3.21242E-01 0.00034  1.34444E+00 0.00029  8.97062E+00 0.00237 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.78955E-05 0.00118  2.78863E-05 0.00118  2.90916E-05 0.01186 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.92465E-05 0.00102  2.92368E-05 0.00102  3.05034E-05 0.01186 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.66875E-03 0.00839  1.94892E-04 0.05172  1.09271E-03 0.02127  1.05122E-03 0.02214  3.01977E-03 0.01268  9.90073E-04 0.02166  3.20087E-04 0.04028 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.00179E-01 0.02177  1.24904E-02 9.4E-06  3.15017E-02 0.00049  1.10235E-01 0.00059  3.21397E-01 0.00040  1.34480E+00 0.00030  8.99031E+00 0.00295 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.78306E-05 0.00266  2.78263E-05 0.00268  2.77510E-05 0.02647 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.91790E-05 0.00262  2.91745E-05 0.00263  2.90895E-05 0.02648 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.62187E-03 0.02543  1.69762E-04 0.16655  1.01708E-03 0.07005  1.04401E-03 0.06494  3.12543E-03 0.03770  9.84604E-04 0.06645  2.80983E-04 0.14998 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.57249E-01 0.06834  1.24901E-02 2.8E-05  3.15596E-02 0.00114  1.10235E-01 0.00124  3.21349E-01 0.00113  1.34423E+00 0.00068  9.03362E+00 0.00728 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.59844E-03 0.02455  1.73797E-04 0.16598  1.00182E-03 0.06925  1.07641E-03 0.06369  3.07907E-03 0.03676  9.87892E-04 0.06599  2.79446E-04 0.14775 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.60438E-01 0.06845  1.24901E-02 2.8E-05  3.15575E-02 0.00113  1.10234E-01 0.00123  3.21298E-01 0.00110  1.34422E+00 0.00068  9.03050E+00 0.00725 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.39010E+02 0.02570 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.78537E-05 0.00081 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.92030E-05 0.00061 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.64142E-03 0.00475 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.38500E+02 0.00479 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.37292E-07 0.00064 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.85342E-06 0.00043  2.85316E-06 0.00043  2.89299E-06 0.00498 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.24322E-05 0.00081  4.24541E-05 0.00081  3.91935E-05 0.00815 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.77553E-01 0.00031  6.77253E-01 0.00031  7.35687E-01 0.00882 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05523E+01 0.01306 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.70832E+01 0.00047  3.55617E+01 0.00048 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.34732E+04 0.00414  2.93346E+05 0.00124  6.05990E+05 0.00081  6.49890E+05 0.00068  5.97683E+05 0.00066  6.41340E+05 0.00074  4.35725E+05 0.00058  3.85322E+05 0.00076  2.94811E+05 0.00076  2.40351E+05 0.00087  2.07591E+05 0.00083  1.86851E+05 0.00110  1.72914E+05 0.00087  1.63992E+05 0.00076  1.60266E+05 0.00112  1.38174E+05 0.00115  1.36643E+05 0.00102  1.35142E+05 0.00100  1.33042E+05 0.00111  2.59597E+05 0.00080  2.50484E+05 0.00071  1.80993E+05 0.00073  1.17523E+05 0.00093  1.35686E+05 0.00086  1.28452E+05 0.00099  1.16409E+05 0.00119  1.91741E+05 0.00085  4.38054E+04 0.00178  5.50428E+04 0.00154  5.00009E+04 0.00142  2.89834E+04 0.00185  5.04058E+04 0.00196  3.42674E+04 0.00199  2.91782E+04 0.00153  5.57204E+03 0.00376  5.47399E+03 0.00289  5.45282E+03 0.00346  5.55532E+03 0.00276  5.53511E+03 0.00304  5.53104E+03 0.00371  5.85222E+03 0.00396  5.52519E+03 0.00405  1.04394E+04 0.00279  1.66489E+04 0.00237  2.11889E+04 0.00166  5.59244E+04 0.00140  5.90336E+04 0.00090  6.44711E+04 0.00141  4.40303E+04 0.00139  3.26829E+04 0.00115  2.50389E+04 0.00162  2.97640E+04 0.00183  5.76521E+04 0.00163  7.94867E+04 0.00150  1.55081E+05 0.00109  2.41053E+05 0.00121  3.61533E+05 0.00124  2.31612E+05 0.00134  1.66057E+05 0.00126  1.19526E+05 0.00157  1.07507E+05 0.00158  1.05841E+05 0.00168  8.82809E+04 0.00159  5.96806E+04 0.00148  5.51731E+04 0.00151  4.90982E+04 0.00174  4.15292E+04 0.00156  3.27092E+04 0.00136  2.18819E+04 0.00152  7.77069E+03 0.00202 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.05865E+00 0.00051 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.23441E+18 0.00049  3.69391E+17 0.00132 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38355E-01 0.00014  1.52174E+00 0.00048 ];
INF_CAPT                  (idx, [1:   4]) = [  6.45135E-03 0.00057  2.97712E-02 0.00051 ];
INF_ABS                   (idx, [1:   4]) = [  8.52539E-03 0.00050  6.02765E-02 0.00092 ];
INF_FISS                  (idx, [1:   4]) = [  2.07404E-03 0.00050  3.05053E-02 0.00133 ];
INF_NSF                   (idx, [1:   4]) = [  5.36399E-03 0.00051  7.59001E-02 0.00134 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.58625E+00 6.7E-05  2.48810E+00 2.5E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04146E+02 6.6E-06  2.02941E+02 3.9E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.90357E-08 0.00034  2.56980E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29832E-01 0.00015  1.46147E+00 0.00053 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44711E-01 0.00025  3.86175E-01 0.00068 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64861E-02 0.00029  9.14465E-02 0.00081 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34736E-03 0.00287  2.74849E-02 0.00226 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03644E-02 0.00203 -8.59150E-03 0.00614 ];
INF_SCATT5                (idx, [1:   4]) = [  1.17060E-04 0.13869  6.41464E-03 0.00760 ];
INF_SCATT6                (idx, [1:   4]) = [  5.05579E-03 0.00398 -1.65671E-02 0.00224 ];
INF_SCATT7                (idx, [1:   4]) = [  7.17168E-04 0.02477  4.35488E-04 0.10378 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29872E-01 0.00015  1.46147E+00 0.00053 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44711E-01 0.00025  3.86175E-01 0.00068 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64860E-02 0.00029  9.14465E-02 0.00081 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34770E-03 0.00288  2.74849E-02 0.00226 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03645E-02 0.00203 -8.59150E-03 0.00614 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.17082E-04 0.13882  6.41464E-03 0.00760 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.05588E-03 0.00397 -1.65671E-02 0.00224 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.17252E-04 0.02472  4.35488E-04 0.10378 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13175E-01 0.00042  9.82951E-01 0.00041 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56367E+00 0.00042  3.39116E-01 0.00041 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.48563E-03 0.00051  6.02765E-02 0.00092 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69657E-02 0.00028  6.13523E-02 0.00104 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11390E-01 0.00014  1.84426E-02 0.00044  1.07465E-03 0.00471  1.46039E+00 0.00053 ];
INF_S1                    (idx, [1:   8]) = [  2.39320E-01 0.00024  5.39054E-03 0.00089  4.61164E-04 0.00647  3.85714E-01 0.00068 ];
INF_S2                    (idx, [1:   8]) = [  9.80875E-02 0.00029 -1.60134E-03 0.00240  2.50671E-04 0.01009  9.11958E-02 0.00082 ];
INF_S3                    (idx, [1:   8]) = [  9.24707E-03 0.00219 -1.89970E-03 0.00160  8.95645E-05 0.01728  2.73954E-02 0.00226 ];
INF_S4                    (idx, [1:   8]) = [ -9.73526E-03 0.00216 -6.29187E-04 0.00382 -3.23240E-06 0.40939 -8.58826E-03 0.00611 ];
INF_S5                    (idx, [1:   8]) = [  9.46076E-05 0.18144  2.24522E-05 0.13767 -3.78028E-05 0.04405  6.45244E-03 0.00762 ];
INF_S6                    (idx, [1:   8]) = [  5.20184E-03 0.00400 -1.46052E-04 0.01463 -4.72428E-05 0.02249 -1.65198E-02 0.00223 ];
INF_S7                    (idx, [1:   8]) = [  8.93863E-04 0.01976 -1.76695E-04 0.01026 -4.17915E-05 0.02294  4.77279E-04 0.09399 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11429E-01 0.00014  1.84426E-02 0.00044  1.07465E-03 0.00471  1.46039E+00 0.00053 ];
INF_SP1                   (idx, [1:   8]) = [  2.39321E-01 0.00024  5.39054E-03 0.00089  4.61164E-04 0.00647  3.85714E-01 0.00068 ];
INF_SP2                   (idx, [1:   8]) = [  9.80874E-02 0.00029 -1.60134E-03 0.00240  2.50671E-04 0.01009  9.11958E-02 0.00082 ];
INF_SP3                   (idx, [1:   8]) = [  9.24740E-03 0.00220 -1.89970E-03 0.00160  8.95645E-05 0.01728  2.73954E-02 0.00226 ];
INF_SP4                   (idx, [1:   8]) = [ -9.73531E-03 0.00216 -6.29187E-04 0.00382 -3.23240E-06 0.40939 -8.58826E-03 0.00611 ];
INF_SP5                   (idx, [1:   8]) = [  9.46299E-05 0.18152  2.24522E-05 0.13767 -3.78028E-05 0.04405  6.45244E-03 0.00762 ];
INF_SP6                   (idx, [1:   8]) = [  5.20193E-03 0.00399 -1.46052E-04 0.01463 -4.72428E-05 0.02249 -1.65198E-02 0.00223 ];
INF_SP7                   (idx, [1:   8]) = [  8.93947E-04 0.01973 -1.76695E-04 0.01026 -4.17915E-05 0.02294  4.77279E-04 0.09399 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32713E-01 0.00058  1.10592E+00 0.00627 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34465E-01 0.00089  1.18290E+00 0.00718 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34497E-01 0.00085  1.18430E+00 0.00868 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29264E-01 0.00092  9.78680E-01 0.00666 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43239E+00 0.00058  3.01685E-01 0.00614 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42170E+00 0.00089  2.82137E-01 0.00709 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42151E+00 0.00085  2.81969E-01 0.00869 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45396E+00 0.00092  3.40950E-01 0.00651 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.69845E-03 0.00858  1.86394E-04 0.05074  1.10963E-03 0.02072  1.06684E-03 0.02161  3.00694E-03 0.01184  1.00935E-03 0.02094  3.19300E-04 0.03643 ];
LAMBDA                    (idx, [1:  14]) = [  7.92864E-01 0.01860  1.24904E-02 7.4E-06  3.15046E-02 0.00042  1.10206E-01 0.00048  3.21242E-01 0.00034  1.34444E+00 0.00029  8.97062E+00 0.00237 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0001' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:49:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:25:19 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292590 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99110E-01  1.00469E+00  1.00129E+00  9.93386E-01  1.00153E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12362E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88764E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.01899E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.02317E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69838E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.68370E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.68281E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.07355E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.32736E-01 0.00108  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000968 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00019E+04 0.00078 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00019E+04 0.00078 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.75259E+02 ;
RUNNING_TIME              (idx, 1)        =  3.54719E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.86050E-01  4.86050E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  8.12500E-02  9.30000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.46211E+01  3.46177E+00  2.69792E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  2.80617E-01  2.86500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.60333E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.54718E+01  1.29496E+02 ];
CPU_USAGE                 (idx, 1)        = 4.94078 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99915E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.72549E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.24790E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.84101E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.25680E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.71930E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.20035E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.52858E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.72096E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  5.79070E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.03893E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.71295E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.13710E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.07774E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.32522E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.64221E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.18375E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.19325E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.26986E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.46676E+11 ;
CS137_ACTIVITY            (idx, 1)        =  4.05084E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.10816E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.54152E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.60950E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.21603E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.35796E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 5 ;
BURNUP                     (idx, [1:  2])  = [  3.00000E+00  3.00027E+00 ];
BURN_DAYS                 (idx, 1)        =  7.50000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.44007E-01 0.00102 ];
U235_FISS                 (idx, [1:   4]) = [  1.08790E+16 0.00068  7.86964E-01 0.00036 ];
U238_FISS                 (idx, [1:   4]) = [  9.29177E+14 0.00268  6.72089E-02 0.00254 ];
PU239_FISS                (idx, [1:   4]) = [  1.97955E+15 0.00180  1.43198E-01 0.00171 ];
PU240_FISS                (idx, [1:   4]) = [  8.25830E+11 0.08556  5.97306E-05 0.08553 ];
PU241_FISS                (idx, [1:   4]) = [  3.11011E+13 0.01440  2.24958E-03 0.01437 ];
U235_CAPT                 (idx, [1:   4]) = [  2.35614E+15 0.00166  1.20802E-01 0.00155 ];
U238_CAPT                 (idx, [1:   4]) = [  8.57067E+15 0.00096  4.39413E-01 0.00066 ];
PU239_CAPT                (idx, [1:   4]) = [  1.09959E+15 0.00251  5.63800E-02 0.00248 ];
PU240_CAPT                (idx, [1:   4]) = [  2.70076E+14 0.00490  1.38472E-02 0.00487 ];
PU241_CAPT                (idx, [1:   4]) = [  1.07815E+13 0.02478  5.53231E-04 0.02485 ];
XE135_CAPT                (idx, [1:   4]) = [  7.37807E+14 0.00324  3.78292E-02 0.00320 ];
SM149_CAPT                (idx, [1:   4]) = [  1.63673E+14 0.00611  8.39251E-03 0.00612 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000968 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.51176E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000968 5.00751E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2900294 2.90418E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2055836 2.05848E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44838 4.48468E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000968 5.00751E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.30854E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.48722E+16 1.5E-05  3.48722E+16 1.5E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38096E+16 2.4E-06  1.38096E+16 2.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.94996E+16 0.00044  1.37384E+16 0.00046  5.76114E+15 0.00102 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.33091E+16 0.00026  2.75480E+16 0.00023  5.76114E+15 0.00102 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.35796E+16 0.00049  3.35796E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.62347E+18 0.00046  4.31945E+17 0.00045  1.19153E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.01222E+14 0.00514 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.36104E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.23833E+18 0.00057 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12165E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12165E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.63141E+00 0.00047 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.69049E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.76664E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23572E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94158E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96854E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.04902E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.03961E+00 0.00052 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.52522E+00 1.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03413E+02 2.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03952E+00 0.00054  1.03297E+00 0.00053  6.63901E-03 0.00902 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.03913E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03861E+00 0.00048 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.03913E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.04854E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74259E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74261E+01 8.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.42403E-07 0.00344 ];
IMP_EALF                  (idx, [1:   2]) = [  5.41046E-07 0.00151 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.27208E-01 0.00276 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.27445E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.21397E-03 0.00607  1.85171E-04 0.03249  1.00803E-03 0.01325  9.97564E-04 0.01478  2.83368E-03 0.00879  9.08196E-04 0.01464  2.81323E-04 0.02666 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.72158E-01 0.01369  1.05447E-02 0.01925  3.14627E-02 0.00032  1.10183E-01 0.00033  3.21226E-01 0.00023  1.34244E+00 0.00040  8.47983E+00 0.01070 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.42865E-03 0.00865  1.93293E-04 0.04898  1.03273E-03 0.02012  1.05392E-03 0.02123  2.93829E-03 0.01196  9.26455E-04 0.02225  2.83973E-04 0.03822 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.57777E-01 0.01916  1.24938E-02 0.00022  3.14447E-02 0.00047  1.10136E-01 0.00045  3.21336E-01 0.00037  1.34254E+00 0.00061  8.94612E+00 0.00259 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.76106E-05 0.00124  2.75991E-05 0.00124  2.93817E-05 0.01294 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.86974E-05 0.00110  2.86854E-05 0.00111  3.05389E-05 0.01295 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.37234E-03 0.00924  1.92882E-04 0.04950  1.03578E-03 0.02167  1.02128E-03 0.02271  2.90414E-03 0.01332  9.34340E-04 0.02367  2.83920E-04 0.04254 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.68279E-01 0.02216  1.24919E-02 9.6E-05  3.14407E-02 0.00056  1.10153E-01 0.00050  3.21219E-01 0.00039  1.34254E+00 0.00067  8.96376E+00 0.00371 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.75174E-05 0.00292  2.75183E-05 0.00292  2.72158E-05 0.03003 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.86012E-05 0.00288  2.86022E-05 0.00288  2.82660E-05 0.02989 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.28900E-03 0.02914  2.40896E-04 0.16040  1.05365E-03 0.07290  1.04778E-03 0.07024  2.83266E-03 0.04215  8.01549E-04 0.08697  3.12468E-04 0.13122 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.44565E-01 0.07367  1.24902E-02 2.4E-05  3.15011E-02 0.00128  1.10139E-01 0.00122  3.21607E-01 0.00114  1.34437E+00 0.00076  9.01544E+00 0.01022 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.21756E-03 0.02847  2.29805E-04 0.15748  1.01068E-03 0.07041  1.03557E-03 0.06839  2.82779E-03 0.04131  8.00074E-04 0.08308  3.13656E-04 0.12908 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.47020E-01 0.07267  1.24902E-02 2.5E-05  3.14991E-02 0.00127  1.10138E-01 0.00120  3.21686E-01 0.00113  1.34431E+00 0.00076  9.01222E+00 0.01021 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.29537E+02 0.02931 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.75521E-05 0.00077 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.86366E-05 0.00053 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.31195E-03 0.00545 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.29183E+02 0.00555 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.29307E-07 0.00066 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.84054E-06 0.00041  2.84038E-06 0.00042  2.86707E-06 0.00516 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.18998E-05 0.00081  4.19194E-05 0.00081  3.88568E-05 0.00914 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.74139E-01 0.00032  6.73874E-01 0.00032  7.30469E-01 0.00932 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03762E+01 0.01352 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.68281E+01 0.00045  3.52790E+01 0.00047 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.41376E+04 0.00394  2.95698E+05 0.00158  6.06358E+05 0.00112  6.50071E+05 0.00075  5.97779E+05 0.00068  6.41402E+05 0.00083  4.35733E+05 0.00090  3.85521E+05 0.00072  2.94468E+05 0.00055  2.40816E+05 0.00074  2.07585E+05 0.00076  1.86841E+05 0.00094  1.72511E+05 0.00079  1.63704E+05 0.00080  1.59878E+05 0.00096  1.38299E+05 0.00098  1.36480E+05 0.00076  1.35306E+05 0.00092  1.32996E+05 0.00077  2.59722E+05 0.00085  2.50590E+05 0.00071  1.81133E+05 0.00081  1.17541E+05 0.00076  1.35686E+05 0.00075  1.28501E+05 0.00098  1.16492E+05 0.00108  1.91112E+05 0.00054  4.37252E+04 0.00128  5.50745E+04 0.00136  4.99430E+04 0.00164  2.90067E+04 0.00179  5.02173E+04 0.00164  3.41627E+04 0.00179  2.91304E+04 0.00151  5.48639E+03 0.00371  5.34582E+03 0.00388  5.28048E+03 0.00371  5.30586E+03 0.00360  5.33295E+03 0.00367  5.38080E+03 0.00320  5.70663E+03 0.00347  5.41355E+03 0.00454  1.03182E+04 0.00282  1.64751E+04 0.00217  2.09953E+04 0.00223  5.56083E+04 0.00166  5.88142E+04 0.00145  6.39493E+04 0.00118  4.33178E+04 0.00125  3.18265E+04 0.00152  2.43217E+04 0.00173  2.87971E+04 0.00134  5.57934E+04 0.00126  7.73589E+04 0.00139  1.51511E+05 0.00083  2.36174E+05 0.00090  3.54812E+05 0.00086  2.27534E+05 0.00113  1.63188E+05 0.00121  1.17473E+05 0.00107  1.05591E+05 0.00116  1.04043E+05 0.00131  8.68740E+04 0.00130  5.88455E+04 0.00141  5.43924E+04 0.00141  4.83622E+04 0.00168  4.08857E+04 0.00145  3.21973E+04 0.00176  2.15903E+04 0.00175  7.65414E+03 0.00183 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.04801E+00 0.00043 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.25492E+18 0.00043  3.68588E+17 0.00091 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38263E-01 0.00012  1.52714E+00 0.00040 ];
INF_CAPT                  (idx, [1:   4]) = [  6.56696E-03 0.00082  3.05480E-02 0.00041 ];
INF_ABS                   (idx, [1:   4]) = [  8.60816E-03 0.00064  6.10695E-02 0.00068 ];
INF_FISS                  (idx, [1:   4]) = [  2.04120E-03 0.00050  3.05215E-02 0.00097 ];
INF_NSF                   (idx, [1:   4]) = [  5.29782E-03 0.00049  7.65853E-02 0.00098 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.59544E+00 5.2E-05  2.50923E+00 2.8E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04259E+02 4.9E-06  2.03220E+02 4.6E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.87737E-08 0.00034  2.57189E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29649E-01 0.00012  1.46604E+00 0.00044 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44672E-01 0.00019  3.87733E-01 0.00045 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64777E-02 0.00032  9.16588E-02 0.00086 ];
INF_SCATT3                (idx, [1:   4]) = [  7.38397E-03 0.00333  2.75481E-02 0.00263 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03320E-02 0.00247 -8.47290E-03 0.00785 ];
INF_SCATT5                (idx, [1:   4]) = [  1.46931E-04 0.14266  6.52173E-03 0.00808 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08723E-03 0.00272 -1.65708E-02 0.00274 ];
INF_SCATT7                (idx, [1:   4]) = [  7.36758E-04 0.01634  4.82650E-04 0.09259 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29689E-01 0.00012  1.46604E+00 0.00044 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44673E-01 0.00019  3.87733E-01 0.00045 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64779E-02 0.00032  9.16588E-02 0.00086 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.38414E-03 0.00334  2.75481E-02 0.00263 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03321E-02 0.00248 -8.47290E-03 0.00785 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.47167E-04 0.14264  6.52173E-03 0.00808 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08716E-03 0.00273 -1.65708E-02 0.00274 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.36817E-04 0.01634  4.82650E-04 0.09259 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12917E-01 0.00029  9.86970E-01 0.00038 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56556E+00 0.00029  3.37735E-01 0.00038 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.56796E-03 0.00065  6.10695E-02 0.00068 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69615E-02 0.00020  6.21810E-02 0.00077 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11302E-01 0.00012  1.83469E-02 0.00035  1.08666E-03 0.00427  1.46496E+00 0.00044 ];
INF_S1                    (idx, [1:   8]) = [  2.39314E-01 0.00019  5.35829E-03 0.00075  4.72895E-04 0.00749  3.87260E-01 0.00045 ];
INF_S2                    (idx, [1:   8]) = [  9.80671E-02 0.00032 -1.58947E-03 0.00219  2.58394E-04 0.00896  9.14004E-02 0.00085 ];
INF_S3                    (idx, [1:   8]) = [  9.26865E-03 0.00269 -1.88468E-03 0.00185  9.30153E-05 0.01896  2.74551E-02 0.00264 ];
INF_S4                    (idx, [1:   8]) = [ -9.70718E-03 0.00267 -6.24785E-04 0.00408 -1.09501E-06 1.00000 -8.47181E-03 0.00787 ];
INF_S5                    (idx, [1:   8]) = [  1.27800E-04 0.16162  1.91303E-05 0.15759 -3.92294E-05 0.04548  6.56096E-03 0.00805 ];
INF_S6                    (idx, [1:   8]) = [  5.23178E-03 0.00254 -1.44558E-04 0.01593 -4.80996E-05 0.02856 -1.65227E-02 0.00272 ];
INF_S7                    (idx, [1:   8]) = [  9.13168E-04 0.01245 -1.76411E-04 0.01319 -4.28617E-05 0.02466  5.25512E-04 0.08540 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11342E-01 0.00012  1.83469E-02 0.00035  1.08666E-03 0.00427  1.46496E+00 0.00044 ];
INF_SP1                   (idx, [1:   8]) = [  2.39315E-01 0.00019  5.35829E-03 0.00075  4.72895E-04 0.00749  3.87260E-01 0.00045 ];
INF_SP2                   (idx, [1:   8]) = [  9.80673E-02 0.00032 -1.58947E-03 0.00219  2.58394E-04 0.00896  9.14004E-02 0.00085 ];
INF_SP3                   (idx, [1:   8]) = [  9.26882E-03 0.00269 -1.88468E-03 0.00185  9.30153E-05 0.01896  2.74551E-02 0.00264 ];
INF_SP4                   (idx, [1:   8]) = [ -9.70735E-03 0.00267 -6.24785E-04 0.00408 -1.09501E-06 1.00000 -8.47181E-03 0.00787 ];
INF_SP5                   (idx, [1:   8]) = [  1.28037E-04 0.16156  1.91303E-05 0.15759 -3.92294E-05 0.04548  6.56096E-03 0.00805 ];
INF_SP6                   (idx, [1:   8]) = [  5.23171E-03 0.00254 -1.44558E-04 0.01593 -4.80996E-05 0.02856 -1.65227E-02 0.00272 ];
INF_SP7                   (idx, [1:   8]) = [  9.13228E-04 0.01245 -1.76411E-04 0.01319 -4.28617E-05 0.02466  5.25512E-04 0.08540 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32579E-01 0.00052  1.12461E+00 0.00597 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34213E-01 0.00080  1.20341E+00 0.00741 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34139E-01 0.00094  1.19733E+00 0.00726 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29457E-01 0.00082  9.99272E-01 0.00583 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43321E+00 0.00052  2.96650E-01 0.00592 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42323E+00 0.00080  2.77353E-01 0.00736 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42369E+00 0.00094  2.78749E-01 0.00725 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45273E+00 0.00082  3.33848E-01 0.00582 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.42865E-03 0.00865  1.93293E-04 0.04898  1.03273E-03 0.02012  1.05392E-03 0.02123  2.93829E-03 0.01196  9.26455E-04 0.02225  2.83973E-04 0.03822 ];
LAMBDA                    (idx, [1:  14]) = [  7.57777E-01 0.01916  1.24938E-02 0.00022  3.14447E-02 0.00047  1.10136E-01 0.00045  3.21336E-01 0.00037  1.34254E+00 0.00061  8.94612E+00 0.00259 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0001' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:49:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:31:33 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292590 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.98957E-01  1.00376E+00  1.00275E+00  9.93493E-01  1.00104E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.1E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12473E-02 0.00103  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88753E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.02734E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.03154E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69199E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.66402E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.66311E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.97738E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.29869E-01 0.00107  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000840 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00017E+04 0.00079 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00017E+04 0.00079 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.06388E+02 ;
RUNNING_TIME              (idx, 1)        =  4.17035E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.86050E-01  4.86050E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  9.88333E-02  8.45000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  4.07784E+01  3.46367E+00  2.69360E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  3.37067E-01  2.72500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.79000E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.17033E+01  1.28878E+02 ];
CPU_USAGE                 (idx, 1)        = 4.94893 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99965E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.74537E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.29249E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.82560E+04 ;
TOT_SF_RATE               (idx, 1)        =  9.24747E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.73359E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.21026E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.55887E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.70455E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  6.73547E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.14309E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.17532E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.21136E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.56015E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.42196E+07 ;
SR90_ACTIVITY             (idx, 1)        =  4.77339E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.21290E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.24285E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.31027E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.21000E+12 ;
CS137_ACTIVITY            (idx, 1)        =  5.40160E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.13533E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.50967E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  8.50917E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.22537E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.40431E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 6 ;
BURNUP                     (idx, [1:  2])  = [  4.00000E+00  4.00035E+00 ];
BURN_DAYS                 (idx, 1)        =  1.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.50856E-01 0.00103 ];
U235_FISS                 (idx, [1:   4]) = [  1.03221E+16 0.00069  7.48612E-01 0.00041 ];
U238_FISS                 (idx, [1:   4]) = [  9.46376E+14 0.00266  6.86291E-02 0.00253 ];
PU239_FISS                (idx, [1:   4]) = [  2.45080E+15 0.00162  1.77742E-01 0.00150 ];
PU240_FISS                (idx, [1:   4]) = [  1.16306E+12 0.07471  8.45309E-05 0.07480 ];
PU241_FISS                (idx, [1:   4]) = [  6.34275E+13 0.01041  4.59971E-03 0.01038 ];
U235_CAPT                 (idx, [1:   4]) = [  2.25030E+15 0.00171  1.12546E-01 0.00163 ];
U238_CAPT                 (idx, [1:   4]) = [  8.61086E+15 0.00096  4.30635E-01 0.00063 ];
PU239_CAPT                (idx, [1:   4]) = [  1.36154E+15 0.00218  6.80974E-02 0.00214 ];
PU240_CAPT                (idx, [1:   4]) = [  4.06794E+14 0.00412  2.03434E-02 0.00403 ];
PU241_CAPT                (idx, [1:   4]) = [  2.26699E+13 0.01838  1.13412E-03 0.01839 ];
XE135_CAPT                (idx, [1:   4]) = [  7.42010E+14 0.00310  3.71095E-02 0.00304 ];
SM149_CAPT                (idx, [1:   4]) = [  1.68716E+14 0.00630  8.43860E-03 0.00629 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000840 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.47039E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000840 5.00747E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2932827 2.93673E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2022510 2.02524E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45503 4.55058E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000840 5.00747E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -8.94070E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.50623E+16 1.7E-05  3.50623E+16 1.7E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37947E+16 2.8E-06  1.37947E+16 2.8E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.00088E+16 0.00044  1.42292E+16 0.00046  5.77960E+15 0.00106 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.38034E+16 0.00026  2.80238E+16 0.00023  5.77960E+15 0.00106 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.40431E+16 0.00050  3.40431E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.63998E+18 0.00047  4.36038E+17 0.00046  1.20394E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.09889E+14 0.00496 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.41133E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.24870E+18 0.00059 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12049E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12049E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.62097E+00 0.00046 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.70750E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.73393E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23504E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94078E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96802E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.03899E+00 0.00053 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.02953E+00 0.00053 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.54173E+00 2.0E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03633E+02 2.8E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.02950E+00 0.00055  1.02311E+00 0.00053  6.42218E-03 0.00872 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.02939E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03006E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.02939E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.03884E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73894E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73945E+01 8.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.62683E-07 0.00356 ];
IMP_EALF                  (idx, [1:   2]) = [  5.58391E-07 0.00152 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.31604E-01 0.00275 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.30843E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.10323E-03 0.00567  1.71102E-04 0.03432  9.99792E-04 0.01418  9.85439E-04 0.01411  2.75331E-03 0.00850  9.12088E-04 0.01409  2.81507E-04 0.02688 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.82068E-01 0.01368  1.03514E-02 0.02041  3.14002E-02 0.00035  1.10149E-01 0.00033  3.21392E-01 0.00025  1.34095E+00 0.00050  8.44909E+00 0.01158 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.27769E-03 0.00860  1.73965E-04 0.05270  1.01621E-03 0.02126  1.00530E-03 0.02136  2.84624E-03 0.01251  9.37208E-04 0.02203  2.98760E-04 0.03701 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.95489E-01 0.01887  1.25013E-02 0.00039  3.14021E-02 0.00051  1.10152E-01 0.00045  3.21379E-01 0.00038  1.34079E+00 0.00073  8.99321E+00 0.00262 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.74441E-05 0.00127  2.74331E-05 0.00127  2.90763E-05 0.01268 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.82494E-05 0.00114  2.82380E-05 0.00114  2.99343E-05 0.01271 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.25655E-03 0.00894  1.61956E-04 0.05705  1.01316E-03 0.02263  1.00808E-03 0.02272  2.88879E-03 0.01324  9.18259E-04 0.02271  2.66304E-04 0.04201 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.50706E-01 0.02133  1.25033E-02 0.00057  3.14273E-02 0.00058  1.10112E-01 0.00051  3.21280E-01 0.00038  1.34082E+00 0.00080  8.97363E+00 0.00339 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.73293E-05 0.00301  2.73209E-05 0.00302  2.76644E-05 0.03408 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.81304E-05 0.00293  2.81218E-05 0.00295  2.84749E-05 0.03402 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.19090E-03 0.03017  1.36697E-04 0.15089  1.06807E-03 0.06936  9.62784E-04 0.07281  2.84108E-03 0.04532  9.18006E-04 0.07579  2.64266E-04 0.13343 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.76750E-01 0.06631  1.24898E-02 3.1E-05  3.13115E-02 0.00149  1.10171E-01 0.00138  3.21026E-01 0.00116  1.33980E+00 0.00233  9.04465E+00 0.00754 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.19136E-03 0.02948  1.44778E-04 0.15130  1.08194E-03 0.06831  9.59311E-04 0.07030  2.84611E-03 0.04431  8.91777E-04 0.07387  2.67450E-04 0.12632 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.76458E-01 0.06563  1.24898E-02 3.1E-05  3.13122E-02 0.00147  1.10188E-01 0.00138  3.21044E-01 0.00114  1.33983E+00 0.00225  9.04450E+00 0.00752 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.27931E+02 0.03062 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.73892E-05 0.00075 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.81931E-05 0.00055 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.21802E-03 0.00533 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.27092E+02 0.00538 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.23235E-07 0.00069 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.82651E-06 0.00041  2.82618E-06 0.00041  2.87420E-06 0.00524 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.15170E-05 0.00083  4.15396E-05 0.00083  3.80046E-05 0.00957 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.70852E-01 0.00032  6.70608E-01 0.00033  7.23330E-01 0.00930 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05159E+01 0.01354 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.66311E+01 0.00047  3.50364E+01 0.00049 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.44475E+04 0.00341  2.95099E+05 0.00135  6.06759E+05 0.00082  6.50315E+05 0.00070  5.98258E+05 0.00070  6.40671E+05 0.00044  4.35020E+05 0.00074  3.85019E+05 0.00059  2.94901E+05 0.00083  2.40589E+05 0.00079  2.07318E+05 0.00092  1.87098E+05 0.00093  1.72911E+05 0.00075  1.64195E+05 0.00111  1.60182E+05 0.00078  1.38243E+05 0.00091  1.36392E+05 0.00093  1.35252E+05 0.00089  1.32847E+05 0.00127  2.59478E+05 0.00063  2.50693E+05 0.00076  1.80909E+05 0.00085  1.17391E+05 0.00106  1.35787E+05 0.00124  1.28862E+05 0.00105  1.16123E+05 0.00101  1.90686E+05 0.00071  4.39245E+04 0.00158  5.51690E+04 0.00140  4.98182E+04 0.00150  2.89459E+04 0.00180  5.03820E+04 0.00120  3.41283E+04 0.00156  2.89946E+04 0.00185  5.44259E+03 0.00392  5.21362E+03 0.00402  5.02584E+03 0.00381  5.05783E+03 0.00285  5.06404E+03 0.00403  5.22969E+03 0.00385  5.58142E+03 0.00302  5.36057E+03 0.00386  1.02241E+04 0.00344  1.63701E+04 0.00232  2.08909E+04 0.00233  5.52614E+04 0.00146  5.82876E+04 0.00180  6.32169E+04 0.00119  4.24285E+04 0.00158  3.11489E+04 0.00129  2.36298E+04 0.00167  2.80811E+04 0.00193  5.44871E+04 0.00134  7.59550E+04 0.00137  1.49119E+05 0.00130  2.32541E+05 0.00120  3.49935E+05 0.00098  2.24441E+05 0.00117  1.60946E+05 0.00136  1.15965E+05 0.00104  1.04200E+05 0.00118  1.02702E+05 0.00148  8.58441E+04 0.00149  5.80287E+04 0.00118  5.36128E+04 0.00154  4.77918E+04 0.00150  4.04105E+04 0.00129  3.18063E+04 0.00180  2.12866E+04 0.00136  7.54427E+03 0.00217 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.03952E+00 0.00039 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.27177E+18 0.00038  3.68245E+17 0.00097 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38433E-01 8.9E-05  1.53151E+00 0.00038 ];
INF_CAPT                  (idx, [1:   4]) = [  6.68824E-03 0.00044  3.12402E-02 0.00044 ];
INF_ABS                   (idx, [1:   4]) = [  8.70069E-03 0.00036  6.17557E-02 0.00070 ];
INF_FISS                  (idx, [1:   4]) = [  2.01245E-03 0.00048  3.05155E-02 0.00097 ];
INF_NSF                   (idx, [1:   4]) = [  5.23952E-03 0.00048  7.71324E-02 0.00099 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.60355E+00 6.9E-05  2.52764E+00 3.5E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04365E+02 6.5E-06  2.03466E+02 5.7E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.85819E-08 0.00036  2.57344E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29730E-01 9.1E-05  1.46982E+00 0.00043 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44753E-01 0.00016  3.88773E-01 0.00048 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65341E-02 0.00024  9.18281E-02 0.00114 ];
INF_SCATT3                (idx, [1:   4]) = [  7.37774E-03 0.00319  2.76002E-02 0.00293 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03769E-02 0.00186 -8.67400E-03 0.00703 ];
INF_SCATT5                (idx, [1:   4]) = [  1.05398E-04 0.17251  6.45209E-03 0.00810 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07500E-03 0.00300 -1.66470E-02 0.00300 ];
INF_SCATT7                (idx, [1:   4]) = [  7.48790E-04 0.02307  4.10600E-04 0.09698 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29770E-01 9.1E-05  1.46982E+00 0.00043 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44754E-01 0.00016  3.88773E-01 0.00048 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65343E-02 0.00024  9.18281E-02 0.00114 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.37807E-03 0.00319  2.76002E-02 0.00293 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03768E-02 0.00186 -8.67400E-03 0.00703 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.05547E-04 0.17207  6.45209E-03 0.00810 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07502E-03 0.00301 -1.66470E-02 0.00300 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.48629E-04 0.02306  4.10600E-04 0.09698 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12908E-01 0.00023  9.90783E-01 0.00035 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56562E+00 0.00023  3.36435E-01 0.00035 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.66070E-03 0.00036  6.17557E-02 0.00070 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69714E-02 0.00024  6.27984E-02 0.00088 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11462E-01 8.8E-05  1.82683E-02 0.00038  1.11074E-03 0.00479  1.46871E+00 0.00043 ];
INF_S1                    (idx, [1:   8]) = [  2.39426E-01 0.00016  5.32672E-03 0.00085  4.75187E-04 0.00594  3.88297E-01 0.00048 ];
INF_S2                    (idx, [1:   8]) = [  9.81203E-02 0.00025 -1.58623E-03 0.00237  2.61835E-04 0.00894  9.15663E-02 0.00114 ];
INF_S3                    (idx, [1:   8]) = [  9.25023E-03 0.00252 -1.87249E-03 0.00199  9.18019E-05 0.01914  2.75084E-02 0.00294 ];
INF_S4                    (idx, [1:   8]) = [ -9.75913E-03 0.00189 -6.17781E-04 0.00424 -7.52585E-07 1.00000 -8.67324E-03 0.00704 ];
INF_S5                    (idx, [1:   8]) = [  8.84143E-05 0.20886  1.69840E-05 0.16400 -4.05631E-05 0.04038  6.49265E-03 0.00800 ];
INF_S6                    (idx, [1:   8]) = [  5.22455E-03 0.00288 -1.49544E-04 0.01392 -4.85993E-05 0.02242 -1.65984E-02 0.00301 ];
INF_S7                    (idx, [1:   8]) = [  9.22693E-04 0.01811 -1.73903E-04 0.01509 -4.38640E-05 0.02587  4.54464E-04 0.08793 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11502E-01 8.8E-05  1.82683E-02 0.00038  1.11074E-03 0.00479  1.46871E+00 0.00043 ];
INF_SP1                   (idx, [1:   8]) = [  2.39427E-01 0.00016  5.32672E-03 0.00085  4.75187E-04 0.00594  3.88297E-01 0.00048 ];
INF_SP2                   (idx, [1:   8]) = [  9.81205E-02 0.00025 -1.58623E-03 0.00237  2.61835E-04 0.00894  9.15663E-02 0.00114 ];
INF_SP3                   (idx, [1:   8]) = [  9.25057E-03 0.00253 -1.87249E-03 0.00199  9.18019E-05 0.01914  2.75084E-02 0.00294 ];
INF_SP4                   (idx, [1:   8]) = [ -9.75899E-03 0.00189 -6.17781E-04 0.00424 -7.52585E-07 1.00000 -8.67324E-03 0.00704 ];
INF_SP5                   (idx, [1:   8]) = [  8.85632E-05 0.20821  1.69840E-05 0.16400 -4.05631E-05 0.04038  6.49265E-03 0.00800 ];
INF_SP6                   (idx, [1:   8]) = [  5.22457E-03 0.00289 -1.49544E-04 0.01392 -4.85993E-05 0.02242 -1.65984E-02 0.00301 ];
INF_SP7                   (idx, [1:   8]) = [  9.22532E-04 0.01810 -1.73903E-04 0.01509 -4.38640E-05 0.02587  4.54464E-04 0.08793 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32372E-01 0.00053  1.11333E+00 0.00651 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34014E-01 0.00091  1.19872E+00 0.00728 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33940E-01 0.00093  1.19503E+00 0.00860 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29236E-01 0.00083  9.77898E-01 0.00711 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43449E+00 0.00053  2.99698E-01 0.00631 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42444E+00 0.00091  2.78428E-01 0.00730 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42490E+00 0.00093  2.79404E-01 0.00819 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45413E+00 0.00083  3.41264E-01 0.00681 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.27769E-03 0.00860  1.73965E-04 0.05270  1.01621E-03 0.02126  1.00530E-03 0.02136  2.84624E-03 0.01251  9.37208E-04 0.02203  2.98760E-04 0.03701 ];
LAMBDA                    (idx, [1:  14]) = [  7.95489E-01 0.01887  1.25013E-02 0.00039  3.14021E-02 0.00051  1.10152E-01 0.00045  3.21379E-01 0.00038  1.34079E+00 0.00073  8.99321E+00 0.00262 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0001' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:49:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:37:44 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292590 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00111E+00  1.00489E+00  9.99557E-01  9.94215E-01  1.00023E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.6E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12392E-02 0.00106  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88761E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.03872E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04291E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68551E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.64632E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.64540E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.86994E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.26553E-01 0.00111  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000847 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00017E+04 0.00080 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00017E+04 0.00080 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.37335E+02 ;
RUNNING_TIME              (idx, 1)        =  4.78996E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.86050E-01  4.86050E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.17050E-01  8.73334E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  4.68993E+01  3.42990E+00  2.69095E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  3.93550E-01  2.75167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.06000E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.78993E+01  1.28852E+02 ];
CPU_USAGE                 (idx, 1)        = 4.95484 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99926E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75976E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.32639E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.81243E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.11159E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.74682E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.21942E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.57954E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.69047E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  7.68228E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.22307E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.72451E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.28255E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.95777E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.49482E+07 ;
SR90_ACTIVITY             (idx, 1)        =  5.87163E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.23867E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.28128E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.34563E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.93767E+12 ;
CS137_ACTIVITY            (idx, 1)        =  6.75187E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.15656E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.48257E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.26690E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.23285E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.46208E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 7 ;
BURNUP                     (idx, [1:  2])  = [  5.00000E+00  5.00045E+00 ];
BURN_DAYS                 (idx, 1)        =  1.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.61261E-01 0.00111 ];
U235_FISS                 (idx, [1:   4]) = [  9.85768E+15 0.00073  7.14882E-01 0.00046 ];
U238_FISS                 (idx, [1:   4]) = [  9.59696E+14 0.00274  6.95887E-02 0.00259 ];
PU239_FISS                (idx, [1:   4]) = [  2.86302E+15 0.00148  2.07628E-01 0.00138 ];
PU240_FISS                (idx, [1:   4]) = [  1.42064E+12 0.06685  1.02958E-04 0.06683 ];
PU241_FISS                (idx, [1:   4]) = [  1.02424E+14 0.00805  7.42815E-03 0.00804 ];
U235_CAPT                 (idx, [1:   4]) = [  2.14920E+15 0.00177  1.04512E-01 0.00177 ];
U238_CAPT                 (idx, [1:   4]) = [  8.71188E+15 0.00103  4.23576E-01 0.00067 ];
PU239_CAPT                (idx, [1:   4]) = [  1.58953E+15 0.00206  7.72908E-02 0.00200 ];
PU240_CAPT                (idx, [1:   4]) = [  5.47967E+14 0.00361  2.66410E-02 0.00349 ];
PU241_CAPT                (idx, [1:   4]) = [  3.69521E+13 0.01400  1.79654E-03 0.01397 ];
XE135_CAPT                (idx, [1:   4]) = [  7.44077E+14 0.00295  3.61820E-02 0.00294 ];
SM149_CAPT                (idx, [1:   4]) = [  1.74988E+14 0.00644  8.50938E-03 0.00645 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000847 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.54907E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000847 5.00755E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2966168 2.97022E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1988949 1.99159E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45730 4.57335E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000847 5.00755E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -8.47504E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.52315E+16 1.8E-05  3.52315E+16 1.8E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37813E+16 3.2E-06  1.37813E+16 3.2E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.05588E+16 0.00045  1.47327E+16 0.00047  5.82604E+15 0.00102 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.43401E+16 0.00027  2.85140E+16 0.00024  5.82604E+15 0.00102 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.46208E+16 0.00051  3.46208E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.66248E+18 0.00047  4.41451E+17 0.00046  1.22103E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.16705E+14 0.00469 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.46568E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.26382E+18 0.00059 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11932E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11932E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.61168E+00 0.00047 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.71173E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.69488E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23518E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94071E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96764E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.02773E+00 0.00056 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.01833E+00 0.00056 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.55647E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03830E+02 3.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.01836E+00 0.00057  1.01227E+00 0.00056  6.05751E-03 0.00887 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.01817E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.01777E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.01817E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.02757E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73568E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73567E+01 8.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.81499E-07 0.00370 ];
IMP_EALF                  (idx, [1:   2]) = [  5.79891E-07 0.00153 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.35428E-01 0.00281 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.35557E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.08321E-03 0.00569  1.71585E-04 0.03443  1.00813E-03 0.01368  9.46476E-04 0.01400  2.73708E-03 0.00847  9.34059E-04 0.01478  2.85885E-04 0.02640 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.96050E-01 0.01369  1.01264E-02 0.02168  3.13293E-02 0.00035  1.10188E-01 0.00038  3.21619E-01 0.00025  1.33851E+00 0.00058  8.49083E+00 0.01124 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.05246E-03 0.00844  1.81930E-04 0.05034  9.99839E-04 0.02089  9.58389E-04 0.02273  2.70163E-03 0.01229  9.29776E-04 0.02244  2.80891E-04 0.03785 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.90539E-01 0.01896  1.24987E-02 0.00035  3.13175E-02 0.00055  1.10191E-01 0.00053  3.21768E-01 0.00037  1.33863E+00 0.00078  9.01248E+00 0.00279 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.73627E-05 0.00125  2.73516E-05 0.00126  2.93252E-05 0.01408 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.78610E-05 0.00114  2.78497E-05 0.00115  2.98627E-05 0.01408 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.94408E-03 0.00901  1.72956E-04 0.05429  9.66597E-04 0.02100  9.37332E-04 0.02500  2.68932E-03 0.01355  8.90664E-04 0.02435  2.87205E-04 0.04182 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.04022E-01 0.02215  1.25009E-02 0.00053  3.13443E-02 0.00063  1.10164E-01 0.00065  3.21887E-01 0.00045  1.33927E+00 0.00090  9.03208E+00 0.00374 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.71844E-05 0.00304  2.71715E-05 0.00305  2.80569E-05 0.03443 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.76772E-05 0.00295  2.76641E-05 0.00296  2.85586E-05 0.03440 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.07854E-03 0.02897  2.09849E-04 0.18358  1.00368E-03 0.07189  8.65383E-04 0.07691  2.76104E-03 0.04654  9.33010E-04 0.07850  3.05588E-04 0.12708 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.05676E-01 0.06671  1.24897E-02 3.0E-05  3.12480E-02 0.00157  1.10492E-01 0.00163  3.21778E-01 0.00123  1.34386E+00 0.00075  8.81202E+00 0.00883 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.09948E-03 0.02847  2.11432E-04 0.17379  1.00487E-03 0.07065  8.63960E-04 0.07673  2.78682E-03 0.04518  9.30535E-04 0.07664  3.01866E-04 0.12489 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.01266E-01 0.06654  1.24897E-02 3.1E-05  3.12474E-02 0.00156  1.10485E-01 0.00162  3.21708E-01 0.00122  1.34385E+00 0.00075  8.80935E+00 0.00882 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.24410E+02 0.02906 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.72653E-05 0.00081 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.77614E-05 0.00057 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.03581E-03 0.00563 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.21460E+02 0.00570 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.17502E-07 0.00062 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.80914E-06 0.00042  2.80883E-06 0.00042  2.85716E-06 0.00561 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.12225E-05 0.00077  4.12454E-05 0.00077  3.75114E-05 0.00945 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.66984E-01 0.00033  6.66816E-01 0.00033  7.05369E-01 0.00857 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03759E+01 0.01391 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.64540E+01 0.00043  3.48342E+01 0.00047 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.53523E+04 0.00326  2.96906E+05 0.00154  6.07808E+05 0.00088  6.50787E+05 0.00084  5.97517E+05 0.00088  6.40707E+05 0.00046  4.35060E+05 0.00072  3.84962E+05 0.00066  2.94363E+05 0.00077  2.40834E+05 0.00078  2.07717E+05 0.00100  1.86926E+05 0.00095  1.72359E+05 0.00093  1.64127E+05 0.00098  1.59814E+05 0.00098  1.38308E+05 0.00094  1.36443E+05 0.00083  1.35355E+05 0.00074  1.32888E+05 0.00075  2.59782E+05 0.00079  2.50790E+05 0.00086  1.80926E+05 0.00100  1.17372E+05 0.00112  1.35617E+05 0.00108  1.28518E+05 0.00063  1.15871E+05 0.00116  1.90671E+05 0.00065  4.36258E+04 0.00162  5.48596E+04 0.00132  4.96783E+04 0.00145  2.89440E+04 0.00211  5.02246E+04 0.00143  3.40652E+04 0.00149  2.87603E+04 0.00167  5.32885E+03 0.00356  5.04489E+03 0.00306  4.81228E+03 0.00334  4.81415E+03 0.00324  4.83645E+03 0.00353  5.08178E+03 0.00352  5.51937E+03 0.00312  5.26013E+03 0.00290  1.00504E+04 0.00260  1.61527E+04 0.00195  2.07347E+04 0.00214  5.50164E+04 0.00107  5.79774E+04 0.00165  6.26036E+04 0.00142  4.19603E+04 0.00199  3.05604E+04 0.00148  2.31116E+04 0.00119  2.74236E+04 0.00154  5.32083E+04 0.00143  7.42310E+04 0.00132  1.46481E+05 0.00115  2.28745E+05 0.00134  3.45045E+05 0.00114  2.21646E+05 0.00129  1.58955E+05 0.00108  1.14481E+05 0.00114  1.03046E+05 0.00127  1.01465E+05 0.00129  8.48269E+04 0.00145  5.74789E+04 0.00145  5.30738E+04 0.00153  4.72339E+04 0.00118  3.99807E+04 0.00173  3.14568E+04 0.00136  2.10799E+04 0.00179  7.47324E+03 0.00186 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.02716E+00 0.00058 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.29319E+18 0.00063  3.69324E+17 0.00106 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38335E-01 0.00012  1.53706E+00 0.00028 ];
INF_CAPT                  (idx, [1:   4]) = [  6.81398E-03 0.00085  3.18097E-02 0.00054 ];
INF_ABS                   (idx, [1:   4]) = [  8.79972E-03 0.00069  6.21768E-02 0.00080 ];
INF_FISS                  (idx, [1:   4]) = [  1.98574E-03 0.00047  3.03672E-02 0.00109 ];
INF_NSF                   (idx, [1:   4]) = [  5.18612E-03 0.00046  7.72488E-02 0.00110 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.61167E+00 6.7E-05  2.54382E+00 3.6E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04469E+02 6.8E-06  2.03684E+02 6.0E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.82875E-08 0.00037  2.57569E-06 0.00013 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29528E-01 0.00013  1.47487E+00 0.00032 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44660E-01 0.00018  3.90203E-01 0.00041 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64686E-02 0.00023  9.22716E-02 0.00098 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34052E-03 0.00391  2.77010E-02 0.00213 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03567E-02 0.00214 -8.60319E-03 0.00516 ];
INF_SCATT5                (idx, [1:   4]) = [  1.51755E-04 0.13824  6.61464E-03 0.00820 ];
INF_SCATT6                (idx, [1:   4]) = [  5.12822E-03 0.00299 -1.66871E-02 0.00217 ];
INF_SCATT7                (idx, [1:   4]) = [  7.63829E-04 0.02224  4.54918E-04 0.06421 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29568E-01 0.00013  1.47487E+00 0.00032 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44661E-01 0.00018  3.90203E-01 0.00041 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64690E-02 0.00023  9.22716E-02 0.00098 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34063E-03 0.00390  2.77010E-02 0.00213 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03568E-02 0.00214 -8.60319E-03 0.00516 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.51852E-04 0.13824  6.61464E-03 0.00820 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.12830E-03 0.00297 -1.66871E-02 0.00217 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.63742E-04 0.02224  4.54918E-04 0.06421 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12737E-01 0.00028  9.94371E-01 0.00024 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56689E+00 0.00028  3.35221E-01 0.00024 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.75930E-03 0.00070  6.21768E-02 0.00080 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69741E-02 0.00020  6.33084E-02 0.00079 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11361E-01 0.00012  1.81662E-02 0.00053  1.12305E-03 0.00365  1.47375E+00 0.00032 ];
INF_S1                    (idx, [1:   8]) = [  2.39360E-01 0.00018  5.30082E-03 0.00094  4.85822E-04 0.00696  3.89717E-01 0.00041 ];
INF_S2                    (idx, [1:   8]) = [  9.80491E-02 0.00022 -1.58051E-03 0.00229  2.63828E-04 0.00683  9.20078E-02 0.00098 ];
INF_S3                    (idx, [1:   8]) = [  9.20854E-03 0.00309 -1.86802E-03 0.00150  9.54238E-05 0.01584  2.76056E-02 0.00214 ];
INF_S4                    (idx, [1:   8]) = [ -9.74110E-03 0.00226 -6.15622E-04 0.00439  1.95237E-06 0.95372 -8.60515E-03 0.00518 ];
INF_S5                    (idx, [1:   8]) = [  1.32581E-04 0.15426  1.91744E-05 0.12458 -3.66455E-05 0.03698  6.65128E-03 0.00821 ];
INF_S6                    (idx, [1:   8]) = [  5.27174E-03 0.00300 -1.43519E-04 0.01566 -4.78107E-05 0.02658 -1.66393E-02 0.00219 ];
INF_S7                    (idx, [1:   8]) = [  9.37446E-04 0.01857 -1.73617E-04 0.01193 -4.34235E-05 0.03116  4.98342E-04 0.05814 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11402E-01 0.00012  1.81662E-02 0.00053  1.12305E-03 0.00365  1.47375E+00 0.00032 ];
INF_SP1                   (idx, [1:   8]) = [  2.39360E-01 0.00018  5.30082E-03 0.00094  4.85822E-04 0.00696  3.89717E-01 0.00041 ];
INF_SP2                   (idx, [1:   8]) = [  9.80495E-02 0.00022 -1.58051E-03 0.00229  2.63828E-04 0.00683  9.20078E-02 0.00098 ];
INF_SP3                   (idx, [1:   8]) = [  9.20866E-03 0.00309 -1.86802E-03 0.00150  9.54238E-05 0.01584  2.76056E-02 0.00214 ];
INF_SP4                   (idx, [1:   8]) = [ -9.74122E-03 0.00226 -6.15622E-04 0.00439  1.95237E-06 0.95372 -8.60515E-03 0.00518 ];
INF_SP5                   (idx, [1:   8]) = [  1.32678E-04 0.15424  1.91744E-05 0.12458 -3.66455E-05 0.03698  6.65128E-03 0.00821 ];
INF_SP6                   (idx, [1:   8]) = [  5.27182E-03 0.00299 -1.43519E-04 0.01566 -4.78107E-05 0.02658 -1.66393E-02 0.00219 ];
INF_SP7                   (idx, [1:   8]) = [  9.37359E-04 0.01857 -1.73617E-04 0.01193 -4.34235E-05 0.03116  4.98342E-04 0.05814 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32340E-01 0.00078  1.14378E+00 0.00550 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34066E-01 0.00119  1.22317E+00 0.00632 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33814E-01 0.00100  1.23083E+00 0.00682 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29214E-01 0.00110  1.00792E+00 0.00631 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43470E+00 0.00077  2.91643E-01 0.00547 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42415E+00 0.00119  2.72779E-01 0.00636 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42567E+00 0.00100  2.71121E-01 0.00680 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45428E+00 0.00110  3.31028E-01 0.00629 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.05246E-03 0.00844  1.81930E-04 0.05034  9.99839E-04 0.02089  9.58389E-04 0.02273  2.70163E-03 0.01229  9.29776E-04 0.02244  2.80891E-04 0.03785 ];
LAMBDA                    (idx, [1:  14]) = [  7.90539E-01 0.01896  1.24987E-02 0.00035  3.13175E-02 0.00055  1.10191E-01 0.00053  3.21768E-01 0.00037  1.33863E+00 0.00078  9.01248E+00 0.00279 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0001' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:49:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:43:52 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292590 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00280E+00  1.00341E+00  1.00087E+00  9.92637E-01  1.00029E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.5E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12315E-02 0.00112  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88768E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04337E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04756E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68158E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.63345E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.63255E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.81240E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.24050E-01 0.00115  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000880 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00018E+04 0.00083 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00018E+04 0.00083 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.67978E+02 ;
RUNNING_TIME              (idx, 1)        =  5.40343E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.86050E-01  4.86050E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.35500E-01  9.15000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  5.29602E+01  3.40225E+00  2.65873E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  4.48150E-01  2.65000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.31167E-02  8.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  5.40341E+01  1.27953E+02 ];
CPU_USAGE                 (idx, 1)        = 4.95940 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00003E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.77080E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.36015E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.80163E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.43693E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.76560E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.23248E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.59453E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.67835E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  8.65727E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.29107E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.36084E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.37824E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.29643E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.55325E+07 ;
SR90_ACTIVITY             (idx, 1)        =  6.93989E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.26174E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.31431E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.37713E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.82498E+12 ;
CS137_ACTIVITY            (idx, 1)        =  8.10133E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.18175E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.45927E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.87496E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.24086E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.51115E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 8 ;
BURNUP                     (idx, [1:  2])  = [  6.00000E+00  6.00053E+00 ];
BURN_DAYS                 (idx, 1)        =  1.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.71187E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  9.42890E+15 0.00083  6.84641E-01 0.00050 ];
U238_FISS                 (idx, [1:   4]) = [  9.70977E+14 0.00270  7.04944E-02 0.00252 ];
PU239_FISS                (idx, [1:   4]) = [  3.21023E+15 0.00141  2.33109E-01 0.00133 ];
PU240_FISS                (idx, [1:   4]) = [  1.95247E+12 0.05893  1.41660E-04 0.05894 ];
PU241_FISS                (idx, [1:   4]) = [  1.53676E+14 0.00662  1.11587E-02 0.00660 ];
U235_CAPT                 (idx, [1:   4]) = [  2.05809E+15 0.00178  9.76662E-02 0.00172 ];
U238_CAPT                 (idx, [1:   4]) = [  8.77945E+15 0.00106  4.16586E-01 0.00070 ];
PU239_CAPT                (idx, [1:   4]) = [  1.77767E+15 0.00197  8.43578E-02 0.00190 ];
PU240_CAPT                (idx, [1:   4]) = [  6.94105E+14 0.00316  3.29357E-02 0.00306 ];
PU241_CAPT                (idx, [1:   4]) = [  5.49144E+13 0.01105  2.60598E-03 0.01103 ];
XE135_CAPT                (idx, [1:   4]) = [  7.50264E+14 0.00308  3.56054E-02 0.00307 ];
SM149_CAPT                (idx, [1:   4]) = [  1.79329E+14 0.00609  8.50962E-03 0.00605 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000880 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.69196E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000880 5.00769E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2996781 3.00096E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1958646 1.96127E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45453 4.54653E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000880 5.00769E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.02914E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.53865E+16 2.0E-05  3.53865E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37689E+16 3.6E-06  1.37689E+16 3.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.10756E+16 0.00047  1.52096E+16 0.00047  5.86604E+15 0.00112 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.48445E+16 0.00028  2.89785E+16 0.00025  5.86604E+15 0.00112 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.51115E+16 0.00051  3.51115E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.68274E+18 0.00049  4.46529E+17 0.00046  1.23621E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.19326E+14 0.00491 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.51639E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.27725E+18 0.00064 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11816E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11816E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.60244E+00 0.00050 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.71871E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.66506E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23406E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94148E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96740E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.01728E+00 0.00057 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.00803E+00 0.00058 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.57003E+00 2.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04014E+02 3.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.00811E+00 0.00059  1.00207E+00 0.00058  5.95879E-03 0.00909 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.00792E+00 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  1.00797E+00 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.00792E+00 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  1.01716E+00 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73341E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73283E+01 8.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.94606E-07 0.00346 ];
IMP_EALF                  (idx, [1:   2]) = [  5.96653E-07 0.00154 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.38536E-01 0.00272 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.39565E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.02424E-03 0.00600  1.59106E-04 0.03537  1.00774E-03 0.01432  9.29964E-04 0.01488  2.72507E-03 0.00894  9.19095E-04 0.01546  2.83263E-04 0.02576 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.97461E-01 0.01367  9.86960E-03 0.02308  3.12984E-02 0.00041  1.10296E-01 0.00037  3.21492E-01 0.00026  1.33752E+00 0.00061  8.51686E+00 0.01117 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.99727E-03 0.00889  1.58682E-04 0.05502  1.01117E-03 0.02256  9.27721E-04 0.02226  2.70162E-03 0.01293  9.14411E-04 0.02309  2.83658E-04 0.03900 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.07011E-01 0.02149  1.24925E-02 0.00023  3.13153E-02 0.00055  1.10250E-01 0.00052  3.21361E-01 0.00038  1.33764E+00 0.00081  9.02401E+00 0.00429 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.73677E-05 0.00127  2.73552E-05 0.00127  2.93449E-05 0.01385 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.75850E-05 0.00114  2.75725E-05 0.00114  2.95756E-05 0.01381 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.92811E-03 0.00913  1.55389E-04 0.05833  1.01997E-03 0.02199  9.22760E-04 0.02479  2.63618E-03 0.01369  9.02538E-04 0.02516  2.91276E-04 0.04277 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.13453E-01 0.02301  1.24894E-02 1.9E-05  3.13229E-02 0.00066  1.10142E-01 0.00067  3.21503E-01 0.00043  1.33763E+00 0.00093  9.05876E+00 0.00440 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.70858E-05 0.00299  2.70749E-05 0.00301  2.83035E-05 0.03634 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.73032E-05 0.00300  2.72922E-05 0.00302  2.85243E-05 0.03631 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.65419E-03 0.03105  1.52198E-04 0.22021  9.66999E-04 0.07751  9.92930E-04 0.07084  2.43997E-03 0.04713  8.77788E-04 0.08263  2.24306E-04 0.13729 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.63534E-01 0.07383  1.24886E-02 5.5E-05  3.13126E-02 0.00153  1.10358E-01 0.00155  3.22023E-01 0.00132  1.33418E+00 0.00287  9.02012E+00 0.01181 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.70381E-03 0.03019  1.49727E-04 0.22355  9.85737E-04 0.07601  9.75832E-04 0.06909  2.49418E-03 0.04589  8.68759E-04 0.08045  2.29578E-04 0.13518 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.68664E-01 0.07286  1.24886E-02 5.5E-05  3.13063E-02 0.00154  1.10335E-01 0.00153  3.21994E-01 0.00131  1.33409E+00 0.00289  9.02012E+00 0.01181 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.09699E+02 0.03115 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.72127E-05 0.00077 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.74286E-05 0.00051 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.83898E-03 0.00560 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.14667E+02 0.00571 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.12985E-07 0.00068 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.79643E-06 0.00044  2.79630E-06 0.00044  2.81693E-06 0.00561 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.09976E-05 0.00082  4.10163E-05 0.00083  3.79883E-05 0.00997 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.64094E-01 0.00033  6.63958E-01 0.00033  6.98912E-01 0.00917 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03396E+01 0.01391 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.63255E+01 0.00048  3.46965E+01 0.00047 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.59399E+04 0.00345  2.98233E+05 0.00137  6.08387E+05 0.00089  6.51205E+05 0.00097  5.98075E+05 0.00061  6.41024E+05 0.00079  4.35515E+05 0.00074  3.85030E+05 0.00052  2.94847E+05 0.00057  2.40756E+05 0.00065  2.07414E+05 0.00070  1.87168E+05 0.00076  1.72329E+05 0.00089  1.64094E+05 0.00097  1.60165E+05 0.00084  1.38263E+05 0.00098  1.36488E+05 0.00092  1.35382E+05 0.00103  1.33037E+05 0.00110  2.59439E+05 0.00090  2.50496E+05 0.00064  1.81377E+05 0.00088  1.17392E+05 0.00102  1.35870E+05 0.00071  1.28678E+05 0.00089  1.16036E+05 0.00094  1.90237E+05 0.00087  4.36701E+04 0.00166  5.48936E+04 0.00142  4.96823E+04 0.00157  2.89302E+04 0.00190  5.01923E+04 0.00170  3.40808E+04 0.00161  2.87003E+04 0.00143  5.29087E+03 0.00320  4.98308E+03 0.00341  4.63763E+03 0.00357  4.56010E+03 0.00282  4.63912E+03 0.00353  4.91740E+03 0.00362  5.41963E+03 0.00390  5.22443E+03 0.00395  1.00016E+04 0.00259  1.60751E+04 0.00274  2.06020E+04 0.00270  5.47146E+04 0.00156  5.75075E+04 0.00148  6.22880E+04 0.00117  4.14263E+04 0.00091  3.00966E+04 0.00145  2.26725E+04 0.00172  2.68339E+04 0.00133  5.23199E+04 0.00147  7.31512E+04 0.00141  1.44444E+05 0.00118  2.26454E+05 0.00114  3.41355E+05 0.00111  2.19329E+05 0.00115  1.57359E+05 0.00135  1.13449E+05 0.00131  1.02082E+05 0.00153  1.00596E+05 0.00144  8.41327E+04 0.00150  5.70452E+04 0.00170  5.25725E+04 0.00164  4.68459E+04 0.00150  3.96177E+04 0.00196  3.12502E+04 0.00170  2.09598E+04 0.00190  7.41880E+03 0.00228 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.01722E+00 0.00040 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.31213E+18 0.00043  3.70637E+17 0.00110 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38338E-01 9.1E-05  1.54032E+00 0.00045 ];
INF_CAPT                  (idx, [1:   4]) = [  6.92836E-03 0.00076  3.23385E-02 0.00058 ];
INF_ABS                   (idx, [1:   4]) = [  8.88711E-03 0.00059  6.25590E-02 0.00085 ];
INF_FISS                  (idx, [1:   4]) = [  1.95875E-03 0.00060  3.02206E-02 0.00114 ];
INF_NSF                   (idx, [1:   4]) = [  5.13077E-03 0.00060  7.73254E-02 0.00118 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.61941E+00 6.4E-05  2.55870E+00 5.4E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04570E+02 7.6E-06  2.03886E+02 9.1E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.80956E-08 0.00037  2.57754E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29454E-01 9.6E-05  1.47776E+00 0.00049 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44642E-01 0.00016  3.91036E-01 0.00062 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64846E-02 0.00026  9.24813E-02 0.00105 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35769E-03 0.00291  2.78547E-02 0.00216 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03403E-02 0.00165 -8.74015E-03 0.00570 ];
INF_SCATT5                (idx, [1:   4]) = [  1.28178E-04 0.11981  6.51332E-03 0.00726 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07921E-03 0.00234 -1.68933E-02 0.00216 ];
INF_SCATT7                (idx, [1:   4]) = [  7.12531E-04 0.01934  4.34018E-04 0.11043 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29495E-01 9.6E-05  1.47776E+00 0.00049 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44642E-01 0.00016  3.91036E-01 0.00062 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64849E-02 0.00026  9.24813E-02 0.00105 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35788E-03 0.00291  2.78547E-02 0.00216 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03400E-02 0.00165 -8.74015E-03 0.00570 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.28456E-04 0.11967  6.51332E-03 0.00726 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07915E-03 0.00233 -1.68933E-02 0.00216 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.12336E-04 0.01930  4.34018E-04 0.11043 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12687E-01 0.00024  9.97079E-01 0.00036 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56725E+00 0.00024  3.34311E-01 0.00036 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.84594E-03 0.00061  6.25590E-02 0.00085 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69580E-02 0.00020  6.36798E-02 0.00086 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11380E-01 9.1E-05  1.80740E-02 0.00042  1.11275E-03 0.00426  1.47664E+00 0.00049 ];
INF_S1                    (idx, [1:   8]) = [  2.39374E-01 0.00016  5.26815E-03 0.00103  4.77905E-04 0.00530  3.90558E-01 0.00062 ];
INF_S2                    (idx, [1:   8]) = [  9.80759E-02 0.00026 -1.59132E-03 0.00207  2.60832E-04 0.00800  9.22205E-02 0.00106 ];
INF_S3                    (idx, [1:   8]) = [  9.22423E-03 0.00231 -1.86653E-03 0.00149  9.66917E-05 0.01565  2.77580E-02 0.00217 ];
INF_S4                    (idx, [1:   8]) = [ -9.72721E-03 0.00173 -6.13038E-04 0.00396  9.77910E-07 1.00000 -8.74113E-03 0.00572 ];
INF_S5                    (idx, [1:   8]) = [  1.05658E-04 0.13502  2.25199E-05 0.13681 -3.93406E-05 0.03709  6.55266E-03 0.00725 ];
INF_S6                    (idx, [1:   8]) = [  5.21882E-03 0.00215 -1.39609E-04 0.01647 -4.83008E-05 0.02469 -1.68450E-02 0.00214 ];
INF_S7                    (idx, [1:   8]) = [  8.80299E-04 0.01524 -1.67768E-04 0.01435 -4.53430E-05 0.02210  4.79361E-04 0.09904 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11421E-01 9.1E-05  1.80740E-02 0.00042  1.11275E-03 0.00426  1.47664E+00 0.00049 ];
INF_SP1                   (idx, [1:   8]) = [  2.39374E-01 0.00016  5.26815E-03 0.00103  4.77905E-04 0.00530  3.90558E-01 0.00062 ];
INF_SP2                   (idx, [1:   8]) = [  9.80762E-02 0.00026 -1.59132E-03 0.00207  2.60832E-04 0.00800  9.22205E-02 0.00106 ];
INF_SP3                   (idx, [1:   8]) = [  9.22441E-03 0.00230 -1.86653E-03 0.00149  9.66917E-05 0.01565  2.77580E-02 0.00217 ];
INF_SP4                   (idx, [1:   8]) = [ -9.72691E-03 0.00173 -6.13038E-04 0.00396  9.77910E-07 1.00000 -8.74113E-03 0.00572 ];
INF_SP5                   (idx, [1:   8]) = [  1.05936E-04 0.13484  2.25199E-05 0.13681 -3.93406E-05 0.03709  6.55266E-03 0.00725 ];
INF_SP6                   (idx, [1:   8]) = [  5.21876E-03 0.00214 -1.39609E-04 0.01647 -4.83008E-05 0.02469 -1.68450E-02 0.00214 ];
INF_SP7                   (idx, [1:   8]) = [  8.80104E-04 0.01521 -1.67768E-04 0.01435 -4.53430E-05 0.02210  4.79361E-04 0.09904 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32253E-01 0.00046  1.14994E+00 0.00617 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34029E-01 0.00096  1.23101E+00 0.00846 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34095E-01 0.00069  1.24351E+00 0.00818 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28726E-01 0.00080  1.00867E+00 0.00515 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43522E+00 0.00046  2.90132E-01 0.00612 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42435E+00 0.00096  2.71248E-01 0.00851 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42394E+00 0.00069  2.68472E-01 0.00785 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45737E+00 0.00081  3.30677E-01 0.00512 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.99727E-03 0.00889  1.58682E-04 0.05502  1.01117E-03 0.02256  9.27721E-04 0.02226  2.70162E-03 0.01293  9.14411E-04 0.02309  2.83658E-04 0.03900 ];
LAMBDA                    (idx, [1:  14]) = [  8.07011E-01 0.02149  1.24925E-02 0.00023  3.13153E-02 0.00055  1.10250E-01 0.00052  3.21361E-01 0.00038  1.33764E+00 0.00081  9.02401E+00 0.00429 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0001' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:49:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:50:03 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292590 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00146E+00  1.00256E+00  1.00177E+00  9.92716E-01  1.00150E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12138E-02 0.00108  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88786E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05104E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05524E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67818E+00 0.00020  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.62613E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.62520E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.75164E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.21656E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000721 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00014E+04 0.00080 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00014E+04 0.00080 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.98789E+02 ;
RUNNING_TIME              (idx, 1)        =  6.02025E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.86050E-01  4.86050E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.53900E-01  9.03333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  5.90564E+01  3.42242E+00  2.67377E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.01433E-01  2.67000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.64333E-02  7.83336E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  6.02024E+01  1.27825E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96306 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00022E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.77970E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.38780E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.79190E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.96264E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.78271E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.24441E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.60506E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.66743E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  9.66846E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.34861E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.07678E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.46781E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.59169E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.60182E+07 ;
SR90_ACTIVITY             (idx, 1)        =  7.98051E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.28267E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.34371E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.40567E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.87430E+12 ;
CS137_ACTIVITY            (idx, 1)        =  9.44995E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.20279E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.43822E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.74683E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.24777E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.56239E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 9 ;
BURNUP                     (idx, [1:  2])  = [  7.00000E+00  7.00063E+00 ];
BURN_DAYS                 (idx, 1)        =  1.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.81352E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  9.03628E+15 0.00079  6.56925E-01 0.00055 ];
U238_FISS                 (idx, [1:   4]) = [  9.83267E+14 0.00279  7.14749E-02 0.00265 ];
PU239_FISS                (idx, [1:   4]) = [  3.52049E+15 0.00145  2.55926E-01 0.00128 ];
PU240_FISS                (idx, [1:   4]) = [  2.76120E+12 0.05116  2.00701E-04 0.05120 ];
PU241_FISS                (idx, [1:   4]) = [  2.05980E+14 0.00585  1.49726E-02 0.00577 ];
U235_CAPT                 (idx, [1:   4]) = [  1.97777E+15 0.00199  9.15950E-02 0.00191 ];
U238_CAPT                 (idx, [1:   4]) = [  8.85727E+15 0.00104  4.10175E-01 0.00074 ];
PU239_CAPT                (idx, [1:   4]) = [  1.95506E+15 0.00194  9.05459E-02 0.00189 ];
PU240_CAPT                (idx, [1:   4]) = [  8.35613E+14 0.00298  3.86958E-02 0.00287 ];
PU241_CAPT                (idx, [1:   4]) = [  7.39427E+13 0.00976  3.42450E-03 0.00973 ];
XE135_CAPT                (idx, [1:   4]) = [  7.45355E+14 0.00304  3.45202E-02 0.00302 ];
SM149_CAPT                (idx, [1:   4]) = [  1.85885E+14 0.00638  8.60990E-03 0.00640 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000721 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.67430E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000721 5.00767E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3026455 3.03069E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1928047 1.93076E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46219 4.62244E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000721 5.00767E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.89179E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.55272E+16 1.9E-05  3.55272E+16 1.9E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37575E+16 3.5E-06  1.37575E+16 3.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.15973E+16 0.00045  1.56634E+16 0.00044  5.93387E+15 0.00110 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.53548E+16 0.00028  2.94209E+16 0.00024  5.93387E+15 0.00110 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.56239E+16 0.00052  3.56239E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.70352E+18 0.00049  4.51172E+17 0.00049  1.25235E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.29368E+14 0.00442 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.56842E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.29320E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11700E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11700E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.59201E+00 0.00050 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.72114E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.63546E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23395E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93998E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96737E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.00639E+00 0.00054 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.97084E-01 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.58239E+00 2.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04183E+02 3.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.97139E-01 0.00056  9.91192E-01 0.00055  5.89235E-03 0.00930 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.97164E-01 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  9.97417E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.97164E-01 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.00647E+00 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73084E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73035E+01 9.2E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.10269E-07 0.00367 ];
IMP_EALF                  (idx, [1:   2]) = [  6.11635E-07 0.00159 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.42311E-01 0.00280 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.42813E-01 0.00119 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.03271E-03 0.00582  1.60300E-04 0.03324  1.01057E-03 0.01430  9.36389E-04 0.01470  2.72930E-03 0.00885  9.02514E-04 0.01529  2.93640E-04 0.02490 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.02580E-01 0.01334  1.01982E-02 0.02126  3.12553E-02 0.00040  1.10229E-01 0.00038  3.21739E-01 0.00026  1.33368E+00 0.00078  8.61352E+00 0.00925 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.96434E-03 0.00872  1.63039E-04 0.05361  1.01117E-03 0.02084  9.27772E-04 0.02174  2.70421E-03 0.01225  8.82044E-04 0.02267  2.76104E-04 0.04033 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.81796E-01 0.02134  1.24983E-02 0.00038  3.12607E-02 0.00057  1.10229E-01 0.00053  3.21798E-01 0.00040  1.33498E+00 0.00098  8.91759E+00 0.00479 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.73978E-05 0.00130  2.73838E-05 0.00131  2.99113E-05 0.01385 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.73154E-05 0.00118  2.73014E-05 0.00119  2.98243E-05 0.01385 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.92715E-03 0.00944  1.61481E-04 0.05934  1.01447E-03 0.02353  9.18259E-04 0.02417  2.69217E-03 0.01374  8.62029E-04 0.02453  2.78751E-04 0.04564 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.86776E-01 0.02424  1.25039E-02 0.00066  3.12685E-02 0.00070  1.10214E-01 0.00069  3.21717E-01 0.00042  1.33517E+00 0.00121  8.88833E+00 0.00659 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.72739E-05 0.00307  2.72641E-05 0.00308  2.73250E-05 0.03423 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.71909E-05 0.00300  2.71811E-05 0.00302  2.72491E-05 0.03423 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.00281E-03 0.03026  1.76319E-04 0.17708  1.03704E-03 0.08119  9.01008E-04 0.07814  2.71004E-03 0.04443  9.01324E-04 0.07936  2.77076E-04 0.13507 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.49803E-01 0.06931  1.24894E-02 4.3E-05  3.11603E-02 0.00169  1.10220E-01 0.00167  3.22268E-01 0.00130  1.33142E+00 0.00378  8.90291E+00 0.01060 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.93409E-03 0.02973  1.75542E-04 0.17288  1.03182E-03 0.08072  8.75240E-04 0.07732  2.67387E-03 0.04315  8.99205E-04 0.07772  2.78418E-04 0.13103 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.54816E-01 0.06745  1.24894E-02 4.2E-05  3.11596E-02 0.00169  1.10211E-01 0.00167  3.22300E-01 0.00129  1.33145E+00 0.00372  8.90414E+00 0.01060 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.21929E+02 0.03075 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.73062E-05 0.00078 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.72239E-05 0.00056 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.02410E-03 0.00542 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.20656E+02 0.00543 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.10715E-07 0.00070 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.78101E-06 0.00041  2.78072E-06 0.00041  2.82490E-06 0.00529 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.09251E-05 0.00086  4.09459E-05 0.00086  3.75538E-05 0.00955 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.61063E-01 0.00032  6.60991E-01 0.00033  6.83694E-01 0.00898 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05547E+01 0.01376 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.62520E+01 0.00048  3.45545E+01 0.00049 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.59339E+04 0.00358  2.98869E+05 0.00154  6.08691E+05 0.00101  6.50252E+05 0.00079  5.98472E+05 0.00079  6.40645E+05 0.00073  4.34733E+05 0.00064  3.84511E+05 0.00070  2.94567E+05 0.00062  2.40455E+05 0.00075  2.07408E+05 0.00080  1.86676E+05 0.00071  1.72508E+05 0.00100  1.64186E+05 0.00087  1.59913E+05 0.00078  1.38094E+05 0.00087  1.36440E+05 0.00094  1.35389E+05 0.00085  1.32881E+05 0.00101  2.59441E+05 0.00071  2.50695E+05 0.00053  1.81120E+05 0.00079  1.17376E+05 0.00094  1.35579E+05 0.00092  1.28814E+05 0.00070  1.16101E+05 0.00101  1.89883E+05 0.00076  4.36725E+04 0.00170  5.46859E+04 0.00140  4.96416E+04 0.00153  2.88544E+04 0.00187  5.01191E+04 0.00098  3.39290E+04 0.00195  2.85094E+04 0.00169  5.21702E+03 0.00341  4.86962E+03 0.00389  4.52616E+03 0.00326  4.37259E+03 0.00335  4.49580E+03 0.00266  4.79474E+03 0.00384  5.28908E+03 0.00393  5.15192E+03 0.00356  9.82879E+03 0.00342  1.59011E+04 0.00280  2.04307E+04 0.00216  5.42390E+04 0.00170  5.71757E+04 0.00189  6.16486E+04 0.00132  4.09847E+04 0.00152  2.96164E+04 0.00165  2.23202E+04 0.00140  2.64703E+04 0.00128  5.16240E+04 0.00141  7.22890E+04 0.00109  1.42807E+05 0.00106  2.24446E+05 0.00098  3.39054E+05 0.00100  2.17886E+05 0.00126  1.56602E+05 0.00096  1.12841E+05 0.00110  1.01452E+05 0.00126  1.00149E+05 0.00130  8.36789E+04 0.00136  5.66904E+04 0.00133  5.24398E+04 0.00121  4.66602E+04 0.00137  3.95444E+04 0.00113  3.12038E+04 0.00174  2.09325E+04 0.00118  7.42221E+03 0.00180 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.00672E+00 0.00067 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.33023E+18 0.00066  3.73327E+17 0.00097 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38523E-01 0.00015  1.54523E+00 0.00033 ];
INF_CAPT                  (idx, [1:   4]) = [  7.03612E-03 0.00050  3.27834E-02 0.00045 ];
INF_ABS                   (idx, [1:   4]) = [  8.96731E-03 0.00042  6.27585E-02 0.00072 ];
INF_FISS                  (idx, [1:   4]) = [  1.93120E-03 0.00042  2.99751E-02 0.00102 ];
INF_NSF                   (idx, [1:   4]) = [  5.07269E-03 0.00044  7.71024E-02 0.00103 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.62671E+00 5.9E-05  2.57221E+00 3.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04668E+02 6.8E-06  2.04072E+02 5.3E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.78709E-08 0.00042  2.58072E-06 0.00012 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29555E-01 0.00015  1.48249E+00 0.00037 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44707E-01 0.00021  3.92282E-01 0.00048 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65341E-02 0.00025  9.26407E-02 0.00087 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35773E-03 0.00297  2.77786E-02 0.00233 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03608E-02 0.00147 -8.73915E-03 0.00821 ];
INF_SCATT5                (idx, [1:   4]) = [  1.42813E-04 0.09987  6.60210E-03 0.00788 ];
INF_SCATT6                (idx, [1:   4]) = [  5.11975E-03 0.00327 -1.69421E-02 0.00232 ];
INF_SCATT7                (idx, [1:   4]) = [  7.73151E-04 0.02117  4.32553E-04 0.09923 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29596E-01 0.00015  1.48249E+00 0.00037 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44708E-01 0.00021  3.92282E-01 0.00048 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65341E-02 0.00025  9.26407E-02 0.00087 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35765E-03 0.00297  2.77786E-02 0.00233 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03605E-02 0.00148 -8.73915E-03 0.00821 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.42694E-04 0.09993  6.60210E-03 0.00788 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.11966E-03 0.00327 -1.69421E-02 0.00232 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.73126E-04 0.02115  4.32553E-04 0.09923 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12643E-01 0.00030  1.00064E+00 0.00029 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56758E+00 0.00030  3.33121E-01 0.00029 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.92621E-03 0.00042  6.27585E-02 0.00072 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69729E-02 0.00024  6.38449E-02 0.00076 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11550E-01 0.00015  1.80047E-02 0.00039  1.11215E-03 0.00464  1.48138E+00 0.00037 ];
INF_S1                    (idx, [1:   8]) = [  2.39456E-01 0.00020  5.25171E-03 0.00110  4.78126E-04 0.00751  3.91804E-01 0.00049 ];
INF_S2                    (idx, [1:   8]) = [  9.81178E-02 0.00024 -1.58370E-03 0.00209  2.59046E-04 0.00961  9.23816E-02 0.00088 ];
INF_S3                    (idx, [1:   8]) = [  9.21269E-03 0.00233 -1.85496E-03 0.00132  9.41059E-05 0.01852  2.76845E-02 0.00236 ];
INF_S4                    (idx, [1:   8]) = [ -9.75760E-03 0.00157 -6.03163E-04 0.00475  3.63636E-06 0.45539 -8.74279E-03 0.00826 ];
INF_S5                    (idx, [1:   8]) = [  1.19609E-04 0.11921  2.32044E-05 0.10770 -3.68516E-05 0.04615  6.63895E-03 0.00784 ];
INF_S6                    (idx, [1:   8]) = [  5.26355E-03 0.00308 -1.43806E-04 0.02038 -4.82744E-05 0.02707 -1.68938E-02 0.00235 ];
INF_S7                    (idx, [1:   8]) = [  9.45753E-04 0.01679 -1.72602E-04 0.01627 -4.10948E-05 0.03104  4.73648E-04 0.09138 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11591E-01 0.00015  1.80047E-02 0.00039  1.11215E-03 0.00464  1.48138E+00 0.00037 ];
INF_SP1                   (idx, [1:   8]) = [  2.39456E-01 0.00021  5.25171E-03 0.00110  4.78126E-04 0.00751  3.91804E-01 0.00049 ];
INF_SP2                   (idx, [1:   8]) = [  9.81178E-02 0.00024 -1.58370E-03 0.00209  2.59046E-04 0.00961  9.23816E-02 0.00088 ];
INF_SP3                   (idx, [1:   8]) = [  9.21260E-03 0.00232 -1.85496E-03 0.00132  9.41059E-05 0.01852  2.76845E-02 0.00236 ];
INF_SP4                   (idx, [1:   8]) = [ -9.75736E-03 0.00158 -6.03163E-04 0.00475  3.63636E-06 0.45539 -8.74279E-03 0.00826 ];
INF_SP5                   (idx, [1:   8]) = [  1.19489E-04 0.11926  2.32044E-05 0.10770 -3.68516E-05 0.04615  6.63895E-03 0.00784 ];
INF_SP6                   (idx, [1:   8]) = [  5.26346E-03 0.00308 -1.43806E-04 0.02038 -4.82744E-05 0.02707 -1.68938E-02 0.00235 ];
INF_SP7                   (idx, [1:   8]) = [  9.45728E-04 0.01679 -1.72602E-04 0.01627 -4.10948E-05 0.03104  4.73648E-04 0.09138 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32127E-01 0.00070  1.15589E+00 0.00632 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33718E-01 0.00079  1.24295E+00 0.00822 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33954E-01 0.00158  1.25336E+00 0.00766 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28795E-01 0.00081  1.00783E+00 0.00594 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43601E+00 0.00070  2.88652E-01 0.00624 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42624E+00 0.00079  2.68612E-01 0.00818 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42487E+00 0.00158  2.66322E-01 0.00757 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45693E+00 0.00081  3.31020E-01 0.00588 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.96434E-03 0.00872  1.63039E-04 0.05361  1.01117E-03 0.02084  9.27772E-04 0.02174  2.70421E-03 0.01225  8.82044E-04 0.02267  2.76104E-04 0.04033 ];
LAMBDA                    (idx, [1:  14]) = [  7.81796E-01 0.02134  1.24983E-02 0.00038  3.12607E-02 0.00057  1.10229E-01 0.00053  3.21798E-01 0.00040  1.33498E+00 0.00098  8.91759E+00 0.00479 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0001' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:49:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:56:13 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292590 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99811E-01  1.00383E+00  1.00161E+00  9.92360E-01  1.00239E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 6.6E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.11895E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88810E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05465E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05884E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67477E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.61564E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.61471E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.70631E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.19370E-01 0.00113  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001064 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00021E+04 0.00082 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00021E+04 0.00082 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.29604E+02 ;
RUNNING_TIME              (idx, 1)        =  6.63722E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.86050E-01  4.86050E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.72983E-01  9.25000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.51495E+01  3.41570E+00  2.67740E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.58283E-01  2.78833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.91333E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  6.63720E+01  1.28113E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96599 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00040E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78673E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.41417E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.78378E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.75380E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.80051E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.25684E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61363E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.65807E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.07254E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.39967E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.87115E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.56179E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.85422E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.64349E+07 ;
SR90_ACTIVITY             (idx, 1)        =  8.99592E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.30180E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.36999E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.43150E+14 ;
CS134_ACTIVITY            (idx, 1)        =  5.06846E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.07977E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.22408E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.41971E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.94924E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.25449E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.61378E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 10 ;
BURNUP                     (idx, [1:  2])  = [  8.00000E+00  8.00073E+00 ];
BURN_DAYS                 (idx, 1)        =  2.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.92552E-01 0.00109 ];
U233_FISS                 (idx, [1:   4]) = [  7.07097E+09 1.00000  5.20156E-07 1.00000 ];
U235_FISS                 (idx, [1:   4]) = [  8.64628E+15 0.00082  6.29282E-01 0.00055 ];
U238_FISS                 (idx, [1:   4]) = [  1.00286E+15 0.00276  7.29766E-02 0.00257 ];
PU239_FISS                (idx, [1:   4]) = [  3.81102E+15 0.00130  2.77370E-01 0.00116 ];
PU240_FISS                (idx, [1:   4]) = [  3.11819E+12 0.04870  2.26825E-04 0.04868 ];
PU241_FISS                (idx, [1:   4]) = [  2.69633E+14 0.00498  1.96238E-02 0.00494 ];
U235_CAPT                 (idx, [1:   4]) = [  1.89961E+15 0.00190  8.58905E-02 0.00184 ];
U238_CAPT                 (idx, [1:   4]) = [  8.96011E+15 0.00101  4.05107E-01 0.00073 ];
PU239_CAPT                (idx, [1:   4]) = [  2.11061E+15 0.00173  9.54307E-02 0.00165 ];
PU240_CAPT                (idx, [1:   4]) = [  9.59152E+14 0.00281  4.33663E-02 0.00274 ];
PU241_CAPT                (idx, [1:   4]) = [  9.76049E+13 0.00916  4.41265E-03 0.00912 ];
XE135_CAPT                (idx, [1:   4]) = [  7.52542E+14 0.00309  3.40305E-02 0.00313 ];
SM149_CAPT                (idx, [1:   4]) = [  1.90011E+14 0.00627  8.59045E-03 0.00623 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001064 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.84699E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001064 5.00785E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3055912 3.06014E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1898599 1.90115E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46553 4.65488E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001064 5.00785E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.81842E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.56613E+16 1.9E-05  3.56613E+16 1.9E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37465E+16 3.6E-06  1.37465E+16 3.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.21079E+16 0.00047  1.61219E+16 0.00045  5.98605E+15 0.00115 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.58544E+16 0.00029  2.98683E+16 0.00024  5.98605E+15 0.00115 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.61378E+16 0.00051  3.61378E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.72512E+18 0.00049  4.56809E+17 0.00045  1.26832E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.36494E+14 0.00506 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.61909E+16 0.00030 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.30814E+18 0.00065 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11584E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11584E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.58211E+00 0.00052 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.71965E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.60669E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23406E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93930E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96741E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.95690E-01 0.00058 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.86421E-01 0.00058 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.59421E+00 2.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04347E+02 3.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.86525E-01 0.00059  9.80724E-01 0.00058  5.69667E-03 0.00945 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.86931E-01 0.00030 ];
COL_KEFF                  (idx, [1:   2]) = [  9.86943E-01 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.86931E-01 0.00030 ];
ABS_KINF                  (idx, [1:   2]) = [  9.96205E-01 0.00029 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72764E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72764E+01 8.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.30148E-07 0.00368 ];
IMP_EALF                  (idx, [1:   2]) = [  6.28396E-07 0.00150 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.47015E-01 0.00271 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.46813E-01 0.00112 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.96497E-03 0.00602  1.66618E-04 0.03433  1.00974E-03 0.01422  9.21267E-04 0.01495  2.67951E-03 0.00914  9.18159E-04 0.01480  2.69681E-04 0.02813 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.73983E-01 0.01473  9.95065E-03 0.02266  3.11933E-02 0.00041  1.10328E-01 0.00042  3.22059E-01 0.00027  1.32903E+00 0.00091  8.05519E+00 0.01482 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.74007E-03 0.00873  1.62660E-04 0.04977  9.69921E-04 0.02077  8.88370E-04 0.02256  2.58302E-03 0.01306  8.80640E-04 0.02242  2.55461E-04 0.04371 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.64419E-01 0.02207  1.25021E-02 0.00040  3.12024E-02 0.00060  1.10308E-01 0.00059  3.21895E-01 0.00040  1.32900E+00 0.00126  8.87104E+00 0.00575 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.75131E-05 0.00133  2.74999E-05 0.00134  2.98742E-05 0.01496 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.71378E-05 0.00120  2.71247E-05 0.00120  2.94687E-05 0.01499 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.76312E-03 0.00959  1.70829E-04 0.05721  9.64292E-04 0.02355  9.19421E-04 0.02459  2.58669E-03 0.01394  8.65898E-04 0.02434  2.55994E-04 0.04707 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.65065E-01 0.02390  1.25084E-02 0.00071  3.11928E-02 0.00074  1.10229E-01 0.00070  3.22107E-01 0.00047  1.33442E+00 0.00118  8.94343E+00 0.00652 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.73337E-05 0.00311  2.73246E-05 0.00313  2.77883E-05 0.03794 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.69598E-05 0.00303  2.69507E-05 0.00304  2.74524E-05 0.03817 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.40053E-03 0.03054  1.30816E-04 0.20948  8.24656E-04 0.07995  9.85168E-04 0.08006  2.43397E-03 0.04403  7.63749E-04 0.08186  2.62165E-04 0.15582 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.71409E-01 0.08036  1.25123E-02 0.00181  3.12966E-02 0.00169  1.10526E-01 0.00180  3.22075E-01 0.00136  1.32573E+00 0.00442  8.59102E+00 0.02620 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.43049E-03 0.03008  1.38778E-04 0.19555  8.23293E-04 0.08004  9.61187E-04 0.07740  2.46182E-03 0.04326  7.85052E-04 0.08260  2.60358E-04 0.15122 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.77685E-01 0.07973  1.25123E-02 0.00181  3.12943E-02 0.00169  1.10505E-01 0.00179  3.22127E-01 0.00135  1.32610E+00 0.00430  8.58881E+00 0.02619 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.98639E+02 0.03098 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.74264E-05 0.00086 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.70520E-05 0.00060 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.73486E-03 0.00661 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.09169E+02 0.00667 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.07351E-07 0.00068 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.77142E-06 0.00044  2.77120E-06 0.00044  2.80820E-06 0.00560 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.08071E-05 0.00087  4.08278E-05 0.00087  3.74195E-05 0.01009 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.58174E-01 0.00032  6.58169E-01 0.00032  6.70865E-01 0.00937 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05281E+01 0.01385 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.61471E+01 0.00047  3.44291E+01 0.00049 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.60357E+04 0.00299  3.00013E+05 0.00116  6.08820E+05 0.00079  6.51335E+05 0.00086  5.96955E+05 0.00078  6.41054E+05 0.00057  4.34599E+05 0.00064  3.85187E+05 0.00067  2.94497E+05 0.00089  2.40611E+05 0.00064  2.07482E+05 0.00086  1.86919E+05 0.00091  1.72238E+05 0.00080  1.64154E+05 0.00073  1.59963E+05 0.00067  1.38109E+05 0.00076  1.36407E+05 0.00095  1.35259E+05 0.00091  1.33007E+05 0.00089  2.59902E+05 0.00076  2.50784E+05 0.00080  1.80916E+05 0.00082  1.17457E+05 0.00076  1.35813E+05 0.00097  1.28812E+05 0.00080  1.15651E+05 0.00088  1.89659E+05 0.00067  4.36444E+04 0.00140  5.47391E+04 0.00163  4.96858E+04 0.00152  2.88833E+04 0.00204  5.01808E+04 0.00165  3.38345E+04 0.00222  2.83285E+04 0.00233  5.15991E+03 0.00355  4.75486E+03 0.00413  4.35208E+03 0.00376  4.21146E+03 0.00309  4.32787E+03 0.00460  4.60346E+03 0.00396  5.20684E+03 0.00356  5.05303E+03 0.00322  9.77598E+03 0.00248  1.57794E+04 0.00246  2.02744E+04 0.00185  5.40155E+04 0.00140  5.70194E+04 0.00107  6.13457E+04 0.00160  4.05962E+04 0.00118  2.92319E+04 0.00190  2.19867E+04 0.00187  2.59971E+04 0.00085  5.07987E+04 0.00107  7.14524E+04 0.00114  1.41469E+05 0.00125  2.22415E+05 0.00122  3.36037E+05 0.00127  2.16521E+05 0.00122  1.55588E+05 0.00138  1.12022E+05 0.00139  1.00967E+05 0.00154  9.95287E+04 0.00113  8.31481E+04 0.00108  5.63609E+04 0.00151  5.20420E+04 0.00144  4.63704E+04 0.00150  3.92494E+04 0.00182  3.09363E+04 0.00163  2.07621E+04 0.00173  7.37775E+03 0.00279 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.96218E-01 0.00048 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.34947E+18 0.00053  3.75693E+17 0.00112 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38509E-01 9.2E-05  1.54768E+00 0.00035 ];
INF_CAPT                  (idx, [1:   4]) = [  7.13733E-03 0.00049  3.32121E-02 0.00058 ];
INF_ABS                   (idx, [1:   4]) = [  9.04044E-03 0.00039  6.29714E-02 0.00084 ];
INF_FISS                  (idx, [1:   4]) = [  1.90311E-03 0.00046  2.97593E-02 0.00115 ];
INF_NSF                   (idx, [1:   4]) = [  5.01315E-03 0.00047  7.69288E-02 0.00117 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.63419E+00 5.2E-05  2.58503E+00 4.7E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04769E+02 5.4E-06  2.04250E+02 8.1E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.76809E-08 0.00037  2.58186E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29465E-01 9.8E-05  1.48472E+00 0.00040 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44643E-01 0.00020  3.92733E-01 0.00044 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65187E-02 0.00031  9.27334E-02 0.00109 ];
INF_SCATT3                (idx, [1:   4]) = [  7.40371E-03 0.00308  2.77961E-02 0.00289 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03166E-02 0.00187 -8.75750E-03 0.00817 ];
INF_SCATT5                (idx, [1:   4]) = [  1.70973E-04 0.10463  6.61442E-03 0.00578 ];
INF_SCATT6                (idx, [1:   4]) = [  5.09609E-03 0.00281 -1.70006E-02 0.00245 ];
INF_SCATT7                (idx, [1:   4]) = [  7.44230E-04 0.02354  4.16193E-04 0.09233 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29507E-01 9.7E-05  1.48472E+00 0.00040 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44644E-01 0.00020  3.92733E-01 0.00044 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65189E-02 0.00031  9.27334E-02 0.00109 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.40384E-03 0.00308  2.77961E-02 0.00289 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03165E-02 0.00187 -8.75750E-03 0.00817 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.71144E-04 0.10444  6.61442E-03 0.00578 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.09615E-03 0.00282 -1.70006E-02 0.00245 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.44113E-04 0.02351  4.16193E-04 0.09233 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12555E-01 0.00033  1.00259E+00 0.00031 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56822E+00 0.00033  3.32473E-01 0.00031 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.99841E-03 0.00040  6.29714E-02 0.00084 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69710E-02 0.00021  6.40808E-02 0.00091 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11538E-01 9.1E-05  1.79266E-02 0.00042  1.12146E-03 0.00319  1.48360E+00 0.00040 ];
INF_S1                    (idx, [1:   8]) = [  2.39422E-01 0.00019  5.22037E-03 0.00121  4.85385E-04 0.00594  3.92248E-01 0.00043 ];
INF_S2                    (idx, [1:   8]) = [  9.80960E-02 0.00029 -1.57725E-03 0.00235  2.63339E-04 0.00883  9.24701E-02 0.00109 ];
INF_S3                    (idx, [1:   8]) = [  9.25164E-03 0.00238 -1.84793E-03 0.00166  9.61788E-05 0.01645  2.76999E-02 0.00290 ];
INF_S4                    (idx, [1:   8]) = [ -9.71609E-03 0.00189 -6.00518E-04 0.00441  2.94460E-06 0.37480 -8.76044E-03 0.00816 ];
INF_S5                    (idx, [1:   8]) = [  1.44666E-04 0.12560  2.63070E-05 0.09307 -3.72645E-05 0.04250  6.65168E-03 0.00572 ];
INF_S6                    (idx, [1:   8]) = [  5.23868E-03 0.00287 -1.42583E-04 0.01690 -4.77629E-05 0.02241 -1.69529E-02 0.00247 ];
INF_S7                    (idx, [1:   8]) = [  9.18305E-04 0.01889 -1.74075E-04 0.01271 -4.46216E-05 0.02933  4.60815E-04 0.08308 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11580E-01 9.1E-05  1.79266E-02 0.00042  1.12146E-03 0.00319  1.48360E+00 0.00040 ];
INF_SP1                   (idx, [1:   8]) = [  2.39423E-01 0.00019  5.22037E-03 0.00121  4.85385E-04 0.00594  3.92248E-01 0.00043 ];
INF_SP2                   (idx, [1:   8]) = [  9.80962E-02 0.00029 -1.57725E-03 0.00235  2.63339E-04 0.00883  9.24701E-02 0.00109 ];
INF_SP3                   (idx, [1:   8]) = [  9.25177E-03 0.00239 -1.84793E-03 0.00166  9.61788E-05 0.01645  2.76999E-02 0.00290 ];
INF_SP4                   (idx, [1:   8]) = [ -9.71597E-03 0.00189 -6.00518E-04 0.00441  2.94460E-06 0.37480 -8.76044E-03 0.00816 ];
INF_SP5                   (idx, [1:   8]) = [  1.44837E-04 0.12535  2.63070E-05 0.09307 -3.72645E-05 0.04250  6.65168E-03 0.00572 ];
INF_SP6                   (idx, [1:   8]) = [  5.23873E-03 0.00288 -1.42583E-04 0.01690 -4.77629E-05 0.02241 -1.69529E-02 0.00247 ];
INF_SP7                   (idx, [1:   8]) = [  9.18188E-04 0.01886 -1.74075E-04 0.01271 -4.46216E-05 0.02933  4.60815E-04 0.08308 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32272E-01 0.00069  1.16220E+00 0.00562 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34017E-01 0.00089  1.24507E+00 0.00807 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33901E-01 0.00109  1.26341E+00 0.00749 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28979E-01 0.00106  1.01466E+00 0.00553 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43512E+00 0.00069  2.87024E-01 0.00549 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42443E+00 0.00089  2.68129E-01 0.00783 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42514E+00 0.00110  2.64191E-01 0.00750 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45578E+00 0.00106  3.28752E-01 0.00537 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.74007E-03 0.00873  1.62660E-04 0.04977  9.69921E-04 0.02077  8.88370E-04 0.02256  2.58302E-03 0.01306  8.80640E-04 0.02242  2.55461E-04 0.04371 ];
LAMBDA                    (idx, [1:  14]) = [  7.64419E-01 0.02207  1.25021E-02 0.00040  3.12024E-02 0.00060  1.10308E-01 0.00059  3.21895E-01 0.00040  1.32900E+00 0.00126  8.87104E+00 0.00575 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0001' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:49:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:02:23 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292590 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.97756E-01  1.00541E+00  1.00190E+00  9.93548E-01  1.00138E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.6E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12340E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88766E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05740E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06161E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67472E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.60913E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.60820E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.67525E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.19898E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000348 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00007E+04 0.00084 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00007E+04 0.00084 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.60466E+02 ;
RUNNING_TIME              (idx, 1)        =  7.25511E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.86050E-01  4.86050E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.92550E-01  9.71667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.12521E+01  3.43270E+00  2.66988E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  6.14517E-01  2.76333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.17333E-02  8.49998E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.25510E+01  1.27994E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96844 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00050E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79268E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.44016E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.77644E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.89127E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.82046E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.27081E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61968E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.64933E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.18304E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.44628E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.73949E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.66546E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.09092E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.67973E+07 ;
SR90_ACTIVITY             (idx, 1)        =  9.98698E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.31954E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.39412E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.45556E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.41809E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.21443E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.24629E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.40269E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.55650E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.26139E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.65684E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 11 ;
BURNUP                     (idx, [1:  2])  = [  9.00000E+00  9.00082E+00 ];
BURN_DAYS                 (idx, 1)        =  2.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.02070E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  8.30369E+15 0.00089  6.05403E-01 0.00061 ];
U238_FISS                 (idx, [1:   4]) = [  1.01453E+15 0.00281  7.39575E-02 0.00263 ];
PU239_FISS                (idx, [1:   4]) = [  4.05358E+15 0.00131  2.95533E-01 0.00111 ];
PU240_FISS                (idx, [1:   4]) = [  3.74920E+12 0.04625  2.73099E-04 0.04622 ];
PU241_FISS                (idx, [1:   4]) = [  3.32414E+14 0.00484  2.42366E-02 0.00481 ];
U235_CAPT                 (idx, [1:   4]) = [  1.82329E+15 0.00200  8.07885E-02 0.00187 ];
U238_CAPT                 (idx, [1:   4]) = [  9.01888E+15 0.00103  3.99613E-01 0.00070 ];
PU239_CAPT                (idx, [1:   4]) = [  2.24659E+15 0.00181  9.95516E-02 0.00174 ];
PU240_CAPT                (idx, [1:   4]) = [  1.08832E+15 0.00269  4.82210E-02 0.00258 ];
PU241_CAPT                (idx, [1:   4]) = [  1.20654E+14 0.00801  5.34769E-03 0.00805 ];
XE135_CAPT                (idx, [1:   4]) = [  7.56199E+14 0.00306  3.35118E-02 0.00308 ];
SM149_CAPT                (idx, [1:   4]) = [  1.93747E+14 0.00615  8.58629E-03 0.00617 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000348 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.83237E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000348 5.00783E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3080926 3.08575E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1872834 1.87549E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46588 4.65908E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000348 5.00783E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.68107E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.57835E+16 2.2E-05  3.57835E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37364E+16 4.0E-06  1.37364E+16 4.0E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.25804E+16 0.00044  1.65454E+16 0.00048  6.03497E+15 0.00110 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.63168E+16 0.00028  3.02818E+16 0.00026  6.03497E+15 0.00110 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.65684E+16 0.00052  3.65684E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.74358E+18 0.00050  4.61168E+17 0.00051  1.28241E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.40763E+14 0.00478 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.66575E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.32133E+18 0.00063 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11467E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11467E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.57273E+00 0.00053 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.72183E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.57987E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23435E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93954E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96708E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.86292E-01 0.00061 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.77102E-01 0.00062 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.60502E+00 2.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04497E+02 4.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.77132E-01 0.00062  9.71544E-01 0.00062  5.55856E-03 0.01007 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.77719E-01 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  9.78669E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.77719E-01 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  9.86905E-01 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72542E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72564E+01 9.6E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.44198E-07 0.00362 ];
IMP_EALF                  (idx, [1:   2]) = [  6.41183E-07 0.00166 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.50793E-01 0.00283 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.50213E-01 0.00123 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.90491E-03 0.00589  1.52244E-04 0.03678  9.98329E-04 0.01457  9.40880E-04 0.01488  2.64968E-03 0.00884  8.88015E-04 0.01580  2.75762E-04 0.02560 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.83808E-01 0.01340  9.58507E-03 0.02475  3.11628E-02 0.00043  1.10293E-01 0.00042  3.22244E-01 0.00027  1.32586E+00 0.00104  8.47007E+00 0.01142 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.65658E-03 0.00905  1.39270E-04 0.05496  9.71117E-04 0.02117  8.96604E-04 0.02201  2.54593E-03 0.01301  8.43628E-04 0.02368  2.60028E-04 0.03805 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.77752E-01 0.01977  1.25134E-02 0.00059  3.11486E-02 0.00063  1.10353E-01 0.00061  3.22167E-01 0.00041  1.32478E+00 0.00161  8.95713E+00 0.00558 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.76600E-05 0.00133  2.76488E-05 0.00134  2.94426E-05 0.01521 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.70221E-05 0.00117  2.70111E-05 0.00117  2.87658E-05 0.01522 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.68657E-03 0.01014  1.47367E-04 0.06242  9.49060E-04 0.02379  9.11516E-04 0.02503  2.56640E-03 0.01427  8.34435E-04 0.02615  2.77794E-04 0.04472 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.93299E-01 0.02323  1.25154E-02 0.00088  3.11347E-02 0.00075  1.10391E-01 0.00083  3.22367E-01 0.00049  1.32400E+00 0.00214  8.93238E+00 0.00709 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.73550E-05 0.00323  2.73380E-05 0.00321  2.91357E-05 0.03698 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.67237E-05 0.00315  2.67073E-05 0.00313  2.84447E-05 0.03692 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.51852E-03 0.03431  9.88488E-05 0.23635  8.74531E-04 0.07861  8.57667E-04 0.08030  2.61659E-03 0.05101  8.49923E-04 0.09200  2.20955E-04 0.14825 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.87320E-01 0.07687  1.24887E-02 5.6E-05  3.11287E-02 0.00180  1.10226E-01 0.00168  3.22112E-01 0.00138  1.31931E+00 0.00546  8.89216E+00 0.02101 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.56649E-03 0.03372  9.91918E-05 0.22569  8.69609E-04 0.07813  8.56140E-04 0.08115  2.64422E-03 0.04981  8.62556E-04 0.09099  2.34769E-04 0.14083 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.09104E-01 0.07736  1.24887E-02 5.6E-05  3.11237E-02 0.00180  1.10238E-01 0.00168  3.22079E-01 0.00137  1.31937E+00 0.00545  8.90017E+00 0.02091 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.02891E+02 0.03429 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.75198E-05 0.00083 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.68853E-05 0.00057 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.61984E-03 0.00566 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.04287E+02 0.00572 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.04913E-07 0.00070 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.75862E-06 0.00043  2.75845E-06 0.00043  2.78714E-06 0.00555 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.07512E-05 0.00085  4.07709E-05 0.00085  3.74002E-05 0.00969 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.55541E-01 0.00033  6.55536E-01 0.00033  6.68598E-01 0.00970 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.01638E+01 0.01360 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.60820E+01 0.00048  3.43750E+01 0.00050 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.68890E+04 0.00312  3.00558E+05 0.00150  6.09538E+05 0.00068  6.51214E+05 0.00081  5.98277E+05 0.00076  6.40601E+05 0.00088  4.35083E+05 0.00061  3.84748E+05 0.00088  2.94729E+05 0.00072  2.40432E+05 0.00083  2.07278E+05 0.00104  1.86815E+05 0.00087  1.72706E+05 0.00079  1.64197E+05 0.00067  1.60124E+05 0.00067  1.38029E+05 0.00104  1.36681E+05 0.00087  1.35294E+05 0.00099  1.32951E+05 0.00084  2.59679E+05 0.00062  2.50622E+05 0.00066  1.81062E+05 0.00085  1.17498E+05 0.00101  1.35876E+05 0.00089  1.28938E+05 0.00082  1.15976E+05 0.00092  1.89286E+05 0.00066  4.34723E+04 0.00163  5.47820E+04 0.00150  4.94779E+04 0.00136  2.88349E+04 0.00200  5.00449E+04 0.00135  3.37112E+04 0.00211  2.81518E+04 0.00197  5.12161E+03 0.00305  4.65290E+03 0.00315  4.24317E+03 0.00316  4.10526E+03 0.00449  4.20420E+03 0.00272  4.51151E+03 0.00376  5.13478E+03 0.00296  4.99578E+03 0.00403  9.64527E+03 0.00278  1.56967E+04 0.00192  2.01610E+04 0.00163  5.36415E+04 0.00141  5.65753E+04 0.00111  6.09263E+04 0.00130  4.01673E+04 0.00125  2.89189E+04 0.00154  2.17275E+04 0.00176  2.56988E+04 0.00122  5.02825E+04 0.00141  7.06625E+04 0.00164  1.40275E+05 0.00113  2.20984E+05 0.00100  3.34433E+05 0.00131  2.15199E+05 0.00128  1.54775E+05 0.00142  1.11505E+05 0.00167  1.00334E+05 0.00126  9.89688E+04 0.00130  8.27626E+04 0.00158  5.61579E+04 0.00158  5.18982E+04 0.00189  4.62462E+04 0.00161  3.91255E+04 0.00154  3.08864E+04 0.00145  2.06903E+04 0.00204  7.33160E+03 0.00250 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.87874E-01 0.00048 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.36577E+18 0.00048  3.77846E+17 0.00092 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38530E-01 9.9E-05  1.55050E+00 0.00037 ];
INF_CAPT                  (idx, [1:   4]) = [  7.23533E-03 0.00056  3.36111E-02 0.00053 ];
INF_ABS                   (idx, [1:   4]) = [  9.11504E-03 0.00043  6.31764E-02 0.00071 ];
INF_FISS                  (idx, [1:   4]) = [  1.87971E-03 0.00058  2.95653E-02 0.00093 ];
INF_NSF                   (idx, [1:   4]) = [  4.96469E-03 0.00056  7.67724E-02 0.00096 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.64120E+00 7.0E-05  2.59670E+00 5.5E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04861E+02 8.3E-06  2.04413E+02 9.6E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.74654E-08 0.00033  2.58384E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29416E-01 0.00011  1.48741E+00 0.00042 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44626E-01 0.00017  3.93536E-01 0.00054 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64918E-02 0.00025  9.30076E-02 0.00113 ];
INF_SCATT3                (idx, [1:   4]) = [  7.36366E-03 0.00388  2.78635E-02 0.00303 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03512E-02 0.00219 -8.80112E-03 0.00654 ];
INF_SCATT5                (idx, [1:   4]) = [  1.53902E-04 0.11609  6.66270E-03 0.00772 ];
INF_SCATT6                (idx, [1:   4]) = [  5.09938E-03 0.00249 -1.70264E-02 0.00314 ];
INF_SCATT7                (idx, [1:   4]) = [  7.55503E-04 0.01760  5.18904E-04 0.06964 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29458E-01 0.00010  1.48741E+00 0.00042 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44626E-01 0.00017  3.93536E-01 0.00054 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64918E-02 0.00025  9.30076E-02 0.00113 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.36393E-03 0.00388  2.78635E-02 0.00303 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03514E-02 0.00219 -8.80112E-03 0.00654 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.53749E-04 0.11597  6.66270E-03 0.00772 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.09936E-03 0.00249 -1.70264E-02 0.00314 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.55552E-04 0.01756  5.18904E-04 0.06964 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12425E-01 0.00026  1.00497E+00 0.00031 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56919E+00 0.00026  3.31686E-01 0.00031 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.07310E-03 0.00043  6.31764E-02 0.00071 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69647E-02 0.00021  6.42080E-02 0.00096 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11565E-01 9.7E-05  1.78508E-02 0.00043  1.11950E-03 0.00336  1.48629E+00 0.00042 ];
INF_S1                    (idx, [1:   8]) = [  2.39431E-01 0.00017  5.19492E-03 0.00081  4.84720E-04 0.00568  3.93051E-01 0.00054 ];
INF_S2                    (idx, [1:   8]) = [  9.80724E-02 0.00025 -1.58064E-03 0.00222  2.59574E-04 0.00985  9.27480E-02 0.00113 ];
INF_S3                    (idx, [1:   8]) = [  9.20404E-03 0.00311 -1.84038E-03 0.00148  9.25902E-05 0.02217  2.77709E-02 0.00302 ];
INF_S4                    (idx, [1:   8]) = [ -9.75961E-03 0.00232 -5.91631E-04 0.00571 -1.50730E-06 0.79613 -8.79961E-03 0.00653 ];
INF_S5                    (idx, [1:   8]) = [  1.24340E-04 0.14049  2.95617E-05 0.09375 -3.94949E-05 0.03786  6.70219E-03 0.00768 ];
INF_S6                    (idx, [1:   8]) = [  5.24042E-03 0.00243 -1.41042E-04 0.01750 -5.01258E-05 0.02212 -1.69763E-02 0.00315 ];
INF_S7                    (idx, [1:   8]) = [  9.26812E-04 0.01333 -1.71308E-04 0.01407 -4.29608E-05 0.02908  5.61864E-04 0.06504 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11607E-01 9.7E-05  1.78508E-02 0.00043  1.11950E-03 0.00336  1.48629E+00 0.00042 ];
INF_SP1                   (idx, [1:   8]) = [  2.39431E-01 0.00017  5.19492E-03 0.00081  4.84720E-04 0.00568  3.93051E-01 0.00054 ];
INF_SP2                   (idx, [1:   8]) = [  9.80724E-02 0.00025 -1.58064E-03 0.00222  2.59574E-04 0.00985  9.27480E-02 0.00113 ];
INF_SP3                   (idx, [1:   8]) = [  9.20430E-03 0.00312 -1.84038E-03 0.00148  9.25902E-05 0.02217  2.77709E-02 0.00302 ];
INF_SP4                   (idx, [1:   8]) = [ -9.75974E-03 0.00232 -5.91631E-04 0.00571 -1.50730E-06 0.79613 -8.79961E-03 0.00653 ];
INF_SP5                   (idx, [1:   8]) = [  1.24188E-04 0.14043  2.95617E-05 0.09375 -3.94949E-05 0.03786  6.70219E-03 0.00768 ];
INF_SP6                   (idx, [1:   8]) = [  5.24040E-03 0.00243 -1.41042E-04 0.01750 -5.01258E-05 0.02212 -1.69763E-02 0.00315 ];
INF_SP7                   (idx, [1:   8]) = [  9.26861E-04 0.01331 -1.71308E-04 0.01407 -4.29608E-05 0.02908  5.61864E-04 0.06504 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32071E-01 0.00073  1.15164E+00 0.00888 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33575E-01 0.00082  1.23827E+00 0.01103 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33857E-01 0.00104  1.24124E+00 0.01132 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28856E-01 0.00102  1.00946E+00 0.00727 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43636E+00 0.00073  2.89980E-01 0.00871 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42712E+00 0.00082  2.69971E-01 0.01093 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42541E+00 0.00104  2.69349E-01 0.01097 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45656E+00 0.00102  3.30621E-01 0.00714 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.65658E-03 0.00905  1.39270E-04 0.05496  9.71117E-04 0.02117  8.96604E-04 0.02201  2.54593E-03 0.01301  8.43628E-04 0.02368  2.60028E-04 0.03805 ];
LAMBDA                    (idx, [1:  14]) = [  7.77752E-01 0.01977  1.25134E-02 0.00059  3.11486E-02 0.00063  1.10353E-01 0.00061  3.22167E-01 0.00041  1.32478E+00 0.00161  8.95713E+00 0.00558 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0001' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:49:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:08:32 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292590 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.98929E-01  1.00434E+00  1.00070E+00  9.95049E-01  1.00098E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12246E-02 0.00108  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88775E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05833E-01 0.00013  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06254E-01 0.00013  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67309E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.59648E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.59555E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.63959E+00 0.00044  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.17949E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000697 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00014E+04 0.00084 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00014E+04 0.00084 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.91116E+02 ;
RUNNING_TIME              (idx, 1)        =  7.86875E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.86050E-01  4.86050E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.12000E-01  9.36667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.73135E+01  3.39853E+00  2.66283E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  6.69583E-01  2.60167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.51333E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.86873E+01  1.28018E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97050 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99843E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79759E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.46352E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.76969E+04 ;
TOT_SF_RATE               (idx, 1)        =  5.45265E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.83942E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.28412E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62407E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.64124E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.29864E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.48872E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  6.67902E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.76640E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.30739E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.71208E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.09553E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.33569E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.41610E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.47750E+14 ;
CS134_ACTIVITY            (idx, 1)        =  7.91071E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.34899E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.26651E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.38678E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  7.63011E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.26777E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.70999E+12 0.00054  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 12 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E+01  1.00009E+01 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.12344E-01 0.00114 ];
U235_FISS                 (idx, [1:   4]) = [  7.97503E+15 0.00092  5.81153E-01 0.00064 ];
U238_FISS                 (idx, [1:   4]) = [  1.02828E+15 0.00270  7.49237E-02 0.00253 ];
PU239_FISS                (idx, [1:   4]) = [  4.30494E+15 0.00123  3.13721E-01 0.00111 ];
PU240_FISS                (idx, [1:   4]) = [  4.62728E+12 0.04015  3.37270E-04 0.04014 ];
PU241_FISS                (idx, [1:   4]) = [  4.01138E+14 0.00428  2.92309E-02 0.00422 ];
U235_CAPT                 (idx, [1:   4]) = [  1.76744E+15 0.00200  7.65560E-02 0.00193 ];
U238_CAPT                 (idx, [1:   4]) = [  9.12238E+15 0.00110  3.95099E-01 0.00075 ];
PU239_CAPT                (idx, [1:   4]) = [  2.37975E+15 0.00171  1.03080E-01 0.00165 ];
PU240_CAPT                (idx, [1:   4]) = [  1.21610E+15 0.00258  5.26715E-02 0.00247 ];
PU241_CAPT                (idx, [1:   4]) = [  1.46149E+14 0.00677  6.33135E-03 0.00678 ];
XE135_CAPT                (idx, [1:   4]) = [  7.57300E+14 0.00306  3.28029E-02 0.00303 ];
SM149_CAPT                (idx, [1:   4]) = [  1.97557E+14 0.00608  8.55920E-03 0.00613 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000697 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.78541E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000697 5.00779E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3107070 3.11154E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1846912 1.84953E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46715 4.67167E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000697 5.00779E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -2.23517E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.59036E+16 2.2E-05  3.59036E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37263E+16 4.3E-06  1.37263E+16 4.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.30700E+16 0.00048  1.69872E+16 0.00047  6.08277E+15 0.00122 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.67963E+16 0.00030  3.07135E+16 0.00026  6.08277E+15 0.00122 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.70999E+16 0.00054  3.70999E+16 0.00054  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.76508E+18 0.00052  4.66803E+17 0.00047  1.29827E+18 0.00059 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.46701E+14 0.00519 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.71430E+16 0.00031 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.33584E+18 0.00069 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11351E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11351E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.56359E+00 0.00050 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.72627E-01 0.00035 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.55408E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23362E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93963E-01 3.8E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96673E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.76692E-01 0.00059 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.67567E-01 0.00060 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.61568E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04647E+02 4.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.67588E-01 0.00060  9.62177E-01 0.00060  5.39001E-03 0.00973 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.68185E-01 0.00031 ];
COL_KEFF                  (idx, [1:   2]) = [  9.67895E-01 0.00054 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.68185E-01 0.00031 ];
ABS_KINF                  (idx, [1:   2]) = [  9.77318E-01 0.00030 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72330E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72305E+01 9.4E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.58195E-07 0.00378 ];
IMP_EALF                  (idx, [1:   2]) = [  6.57974E-07 0.00162 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.53941E-01 0.00273 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.53910E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.89734E-03 0.00603  1.70751E-04 0.03684  1.02682E-03 0.01483  9.11736E-04 0.01588  2.60825E-03 0.00899  9.00744E-04 0.01560  2.79045E-04 0.02646 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.84238E-01 0.01395  9.91348E-03 0.02295  3.11074E-02 0.00042  1.10300E-01 0.00045  3.22104E-01 0.00030  1.32156E+00 0.00228  8.29653E+00 0.01246 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.63667E-03 0.00871  1.78737E-04 0.05642  9.97040E-04 0.02255  8.75764E-04 0.02409  2.44671E-03 0.01362  8.74617E-04 0.02353  2.63799E-04 0.04342 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.80991E-01 0.02162  1.25180E-02 0.00064  3.11063E-02 0.00062  1.10330E-01 0.00065  3.22063E-01 0.00043  1.32415E+00 0.00163  8.85503E+00 0.00603 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.77533E-05 0.00133  2.77413E-05 0.00133  2.99655E-05 0.01498 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.68495E-05 0.00122  2.68378E-05 0.00122  2.89933E-05 0.01496 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.57170E-03 0.00985  1.77319E-04 0.05777  9.93642E-04 0.02419  8.85587E-04 0.02695  2.39973E-03 0.01502  8.56818E-04 0.02565  2.58609E-04 0.04714 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.79509E-01 0.02512  1.25135E-02 0.00079  3.11088E-02 0.00076  1.10235E-01 0.00079  3.21938E-01 0.00054  1.32584E+00 0.00194  8.82574E+00 0.00905 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.74564E-05 0.00314  2.74424E-05 0.00316  2.73214E-05 0.03613 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.65606E-05 0.00305  2.65470E-05 0.00307  2.64353E-05 0.03614 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.27352E-03 0.03426  1.78129E-04 0.18763  9.78586E-04 0.08089  8.08787E-04 0.09274  2.20072E-03 0.04892  8.30610E-04 0.08609  2.76689E-04 0.15603 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.49171E-01 0.08278  1.25394E-02 0.00275  3.11804E-02 0.00179  1.10498E-01 0.00199  3.21502E-01 0.00139  1.32738E+00 0.00450  8.80553E+00 0.01989 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.29029E-03 0.03392  1.78914E-04 0.18029  9.79900E-04 0.07769  7.99359E-04 0.09126  2.23067E-03 0.04927  8.29514E-04 0.08262  2.71936E-04 0.14725 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.57707E-01 0.08184  1.25394E-02 0.00275  3.11855E-02 0.00179  1.10488E-01 0.00198  3.21327E-01 0.00136  1.32763E+00 0.00443  8.81297E+00 0.01963 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.93191E+02 0.03424 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.76187E-05 0.00089 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.67186E-05 0.00065 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.43215E-03 0.00578 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.96774E+02 0.00589 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.01496E-07 0.00072 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.74946E-06 0.00042  2.74942E-06 0.00042  2.75539E-06 0.00596 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.05712E-05 0.00089  4.05918E-05 0.00090  3.71452E-05 0.01022 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.53030E-01 0.00033  6.53072E-01 0.00034  6.58565E-01 0.00935 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.09377E+01 0.01393 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.59555E+01 0.00049  3.42463E+01 0.00050 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.73418E+04 0.00404  3.00326E+05 0.00139  6.10282E+05 0.00081  6.51745E+05 0.00071  5.97595E+05 0.00076  6.39942E+05 0.00059  4.35044E+05 0.00054  3.84482E+05 0.00084  2.94507E+05 0.00085  2.40552E+05 0.00064  2.07409E+05 0.00105  1.86768E+05 0.00085  1.72517E+05 0.00076  1.64239E+05 0.00089  1.59958E+05 0.00084  1.38360E+05 0.00101  1.36276E+05 0.00072  1.35000E+05 0.00106  1.32933E+05 0.00099  2.59521E+05 0.00066  2.50819E+05 0.00062  1.80907E+05 0.00080  1.17426E+05 0.00076  1.35951E+05 0.00101  1.28648E+05 0.00087  1.15928E+05 0.00098  1.88956E+05 0.00071  4.35154E+04 0.00156  5.44119E+04 0.00162  4.94856E+04 0.00207  2.88400E+04 0.00242  4.99730E+04 0.00140  3.36799E+04 0.00168  2.82063E+04 0.00178  5.04408E+03 0.00308  4.56725E+03 0.00331  4.13565E+03 0.00248  3.97565E+03 0.00360  4.04354E+03 0.00290  4.41141E+03 0.00407  5.03610E+03 0.00329  4.92078E+03 0.00361  9.57059E+03 0.00279  1.55888E+04 0.00259  2.00441E+04 0.00242  5.34385E+04 0.00158  5.63542E+04 0.00117  6.04748E+04 0.00146  3.99285E+04 0.00157  2.85854E+04 0.00151  2.14255E+04 0.00193  2.53060E+04 0.00175  4.96032E+04 0.00153  6.99151E+04 0.00132  1.38943E+05 0.00138  2.19040E+05 0.00137  3.31486E+05 0.00137  2.13418E+05 0.00133  1.53432E+05 0.00157  1.10641E+05 0.00175  9.96477E+04 0.00157  9.83251E+04 0.00188  8.23408E+04 0.00160  5.57309E+04 0.00194  5.14832E+04 0.00185  4.58218E+04 0.00196  3.87615E+04 0.00223  3.06388E+04 0.00210  2.05339E+04 0.00223  7.28465E+03 0.00278 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.77023E-01 0.00058 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.38500E+18 0.00053  3.80114E+17 0.00162 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38626E-01 0.00012  1.55177E+00 0.00053 ];
INF_CAPT                  (idx, [1:   4]) = [  7.32860E-03 0.00067  3.39938E-02 0.00089 ];
INF_ABS                   (idx, [1:   4]) = [  9.18356E-03 0.00056  6.33529E-02 0.00125 ];
INF_FISS                  (idx, [1:   4]) = [  1.85495E-03 0.00051  2.93591E-02 0.00169 ];
INF_NSF                   (idx, [1:   4]) = [  4.91207E-03 0.00051  7.65751E-02 0.00174 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.64808E+00 4.5E-05  2.60822E+00 6.0E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04958E+02 6.3E-06  2.04576E+02 1.0E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.73140E-08 0.00053  2.58445E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29440E-01 0.00012  1.48840E+00 0.00060 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44700E-01 0.00023  3.93889E-01 0.00066 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65309E-02 0.00031  9.28855E-02 0.00108 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34939E-03 0.00322  2.78143E-02 0.00235 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03269E-02 0.00190 -8.90215E-03 0.00609 ];
INF_SCATT5                (idx, [1:   4]) = [  1.70351E-04 0.10816  6.64980E-03 0.00720 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10786E-03 0.00258 -1.69965E-02 0.00263 ];
INF_SCATT7                (idx, [1:   4]) = [  7.55166E-04 0.02054  5.25748E-04 0.11038 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29482E-01 0.00012  1.48840E+00 0.00060 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44701E-01 0.00023  3.93889E-01 0.00066 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65310E-02 0.00031  9.28855E-02 0.00108 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34941E-03 0.00321  2.78143E-02 0.00235 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03270E-02 0.00189 -8.90215E-03 0.00609 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.70367E-04 0.10810  6.64980E-03 0.00720 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10806E-03 0.00257 -1.69965E-02 0.00263 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.55179E-04 0.02053  5.25748E-04 0.11038 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12315E-01 0.00040  1.00605E+00 0.00044 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.57000E+00 0.00040  3.31331E-01 0.00043 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.14185E-03 0.00057  6.33529E-02 0.00125 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69761E-02 0.00021  6.45007E-02 0.00121 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11650E-01 0.00012  1.77901E-02 0.00039  1.12651E-03 0.00415  1.48727E+00 0.00061 ];
INF_S1                    (idx, [1:   8]) = [  2.39530E-01 0.00023  5.16982E-03 0.00080  4.87370E-04 0.00730  3.93402E-01 0.00067 ];
INF_S2                    (idx, [1:   8]) = [  9.81106E-02 0.00030 -1.57969E-03 0.00256  2.65785E-04 0.00914  9.26197E-02 0.00109 ];
INF_S3                    (idx, [1:   8]) = [  9.18116E-03 0.00255 -1.83177E-03 0.00177  9.52371E-05 0.01968  2.77190E-02 0.00237 ];
INF_S4                    (idx, [1:   8]) = [ -9.74141E-03 0.00201 -5.85468E-04 0.00512  2.03521E-06 0.77243 -8.90418E-03 0.00609 ];
INF_S5                    (idx, [1:   8]) = [  1.37445E-04 0.13895  3.29061E-05 0.07387 -3.96487E-05 0.03216  6.68945E-03 0.00723 ];
INF_S6                    (idx, [1:   8]) = [  5.24850E-03 0.00254 -1.40644E-04 0.02003 -5.02827E-05 0.02221 -1.69462E-02 0.00266 ];
INF_S7                    (idx, [1:   8]) = [  9.29465E-04 0.01669 -1.74299E-04 0.01380 -4.50390E-05 0.02376  5.70787E-04 0.10182 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11692E-01 0.00012  1.77901E-02 0.00039  1.12651E-03 0.00415  1.48727E+00 0.00061 ];
INF_SP1                   (idx, [1:   8]) = [  2.39531E-01 0.00023  5.16982E-03 0.00080  4.87370E-04 0.00730  3.93402E-01 0.00067 ];
INF_SP2                   (idx, [1:   8]) = [  9.81107E-02 0.00030 -1.57969E-03 0.00256  2.65785E-04 0.00914  9.26197E-02 0.00109 ];
INF_SP3                   (idx, [1:   8]) = [  9.18119E-03 0.00255 -1.83177E-03 0.00177  9.52371E-05 0.01968  2.77190E-02 0.00237 ];
INF_SP4                   (idx, [1:   8]) = [ -9.74153E-03 0.00201 -5.85468E-04 0.00512  2.03521E-06 0.77243 -8.90418E-03 0.00609 ];
INF_SP5                   (idx, [1:   8]) = [  1.37461E-04 0.13885  3.29061E-05 0.07387 -3.96487E-05 0.03216  6.68945E-03 0.00723 ];
INF_SP6                   (idx, [1:   8]) = [  5.24870E-03 0.00253 -1.40644E-04 0.02003 -5.02827E-05 0.02221 -1.69462E-02 0.00266 ];
INF_SP7                   (idx, [1:   8]) = [  9.29478E-04 0.01668 -1.74299E-04 0.01380 -4.50390E-05 0.02376  5.70787E-04 0.10182 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31885E-01 0.00044  1.18349E+00 0.00709 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33703E-01 0.00086  1.27117E+00 0.01005 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33604E-01 0.00075  1.28176E+00 0.00875 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28436E-01 0.00091  1.03446E+00 0.00682 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43750E+00 0.00044  2.81988E-01 0.00697 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42634E+00 0.00086  2.62851E-01 0.00989 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42694E+00 0.00075  2.60525E-01 0.00853 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45923E+00 0.00091  3.22589E-01 0.00681 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.63667E-03 0.00871  1.78737E-04 0.05642  9.97040E-04 0.02255  8.75764E-04 0.02409  2.44671E-03 0.01362  8.74617E-04 0.02353  2.63799E-04 0.04342 ];
LAMBDA                    (idx, [1:  14]) = [  7.80991E-01 0.02162  1.25180E-02 0.00064  3.11063E-02 0.00062  1.10330E-01 0.00065  3.22063E-01 0.00043  1.32415E+00 0.00163  8.85503E+00 0.00603 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0001' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:49:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:14:39 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292590 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99125E-01  1.00362E+00  1.00152E+00  9.93991E-01  1.00174E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13616E-02 0.00114  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88638E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06327E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06750E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67302E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.58201E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.58107E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.57752E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.21707E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001303 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00026E+04 0.00095 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00026E+04 0.00095 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.21729E+02 ;
RUNNING_TIME              (idx, 1)        =  8.48167E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.86050E-01  4.86050E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.31950E-01  9.70000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  8.33658E+01  3.39727E+00  2.65505E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  7.25950E-01  2.75500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.78500E-02  8.33337E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  8.48166E+01  1.27713E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97224 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99967E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80175E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.48513E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.74618E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.17555E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.87617E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.31014E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.60893E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.61514E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.61311E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.57129E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  9.35208E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.97926E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.77898E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.77337E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.32855E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.35102E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.45278E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.50512E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.22173E+13 ;
CS137_ACTIVITY            (idx, 1)        =  1.68510E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.28412E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.34667E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.51840E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.27795E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.82371E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 13 ;
BURNUP                     (idx, [1:  2])  = [  1.25000E+01  1.25012E+01 ];
BURN_DAYS                 (idx, 1)        =  3.12500E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.36773E-01 0.00107 ];
U233_FISS                 (idx, [1:   4]) = [  1.53058E+10 0.70645  1.09945E-06 0.70653 ];
U235_FISS                 (idx, [1:   4]) = [  7.23276E+15 0.00095  5.27560E-01 0.00071 ];
U238_FISS                 (idx, [1:   4]) = [  1.05966E+15 0.00282  7.72794E-02 0.00262 ];
PU239_FISS                (idx, [1:   4]) = [  4.81463E+15 0.00122  3.51180E-01 0.00104 ];
PU240_FISS                (idx, [1:   4]) = [  5.88546E+12 0.03643  4.28949E-04 0.03641 ];
PU241_FISS                (idx, [1:   4]) = [  5.86221E+14 0.00357  4.27591E-02 0.00352 ];
U235_CAPT                 (idx, [1:   4]) = [  1.60267E+15 0.00219  6.61503E-02 0.00215 ];
U238_CAPT                 (idx, [1:   4]) = [  9.32479E+15 0.00104  3.84848E-01 0.00072 ];
PU239_CAPT                (idx, [1:   4]) = [  2.64361E+15 0.00166  1.09117E-01 0.00161 ];
PU240_CAPT                (idx, [1:   4]) = [  1.50149E+15 0.00229  6.19707E-02 0.00220 ];
PU241_CAPT                (idx, [1:   4]) = [  2.11713E+14 0.00616  8.73843E-03 0.00614 ];
XE135_CAPT                (idx, [1:   4]) = [  7.64913E+14 0.00302  3.15730E-02 0.00301 ];
SM149_CAPT                (idx, [1:   4]) = [  2.06070E+14 0.00634  8.50587E-03 0.00633 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001303 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.04266E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001303 5.00804E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3163812 3.16822E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1790515 1.79285E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46976 4.69761E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001303 5.00804E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.44589E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.61700E+16 2.3E-05  3.61700E+16 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37037E+16 4.6E-06  1.37037E+16 4.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.42325E+16 0.00046  1.80128E+16 0.00047  6.21968E+15 0.00117 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.79362E+16 0.00029  3.17165E+16 0.00026  6.21968E+15 0.00117 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.82371E+16 0.00051  3.82371E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.81387E+18 0.00049  4.79211E+17 0.00048  1.33466E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.59308E+14 0.00475 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.82955E+16 0.00030 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.37128E+18 0.00065 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11061E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11061E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.54387E+00 0.00053 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.72614E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.49805E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23285E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93871E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96714E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.55522E-01 0.00062 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.46545E-01 0.00062 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.63943E+00 2.8E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04984E+02 4.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.46639E-01 0.00064  9.41412E-01 0.00062  5.13293E-03 0.01012 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.46047E-01 0.00030 ];
COL_KEFF                  (idx, [1:   2]) = [  9.46063E-01 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.46047E-01 0.00030 ];
ABS_KINF                  (idx, [1:   2]) = [  9.55019E-01 0.00029 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71854E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71840E+01 9.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.90370E-07 0.00382 ];
IMP_EALF                  (idx, [1:   2]) = [  6.89301E-07 0.00169 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.61963E-01 0.00285 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.62140E-01 0.00119 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.85390E-03 0.00624  1.54768E-04 0.03518  1.00369E-03 0.01480  9.26150E-04 0.01453  2.55612E-03 0.00884  9.30916E-04 0.01491  2.82259E-04 0.02660 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.95562E-01 0.01362  9.90131E-03 0.02294  3.10393E-02 0.00046  1.10471E-01 0.00047  3.22292E-01 0.00031  1.31462E+00 0.00127  8.28064E+00 0.01268 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.40142E-03 0.00944  1.45693E-04 0.05575  9.34014E-04 0.02272  8.52735E-04 0.02346  2.34126E-03 0.01347  8.55511E-04 0.02170  2.72206E-04 0.04305 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.13344E-01 0.02274  1.25019E-02 0.00037  3.10506E-02 0.00064  1.10460E-01 0.00067  3.22457E-01 0.00046  1.31378E+00 0.00189  8.82349E+00 0.00649 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.80761E-05 0.00142  2.80668E-05 0.00143  2.96543E-05 0.01440 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.65720E-05 0.00124  2.65632E-05 0.00124  2.80763E-05 0.01445 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.41786E-03 0.01016  1.39991E-04 0.06569  9.30953E-04 0.02424  8.62193E-04 0.02566  2.37230E-03 0.01511  8.44830E-04 0.02562  2.67590E-04 0.04474 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.99207E-01 0.02445  1.25091E-02 0.00076  3.10667E-02 0.00078  1.10441E-01 0.00088  3.22379E-01 0.00052  1.31390E+00 0.00254  8.76233E+00 0.01022 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.77800E-05 0.00324  2.77753E-05 0.00325  2.74520E-05 0.03786 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.62924E-05 0.00318  2.62880E-05 0.00319  2.60099E-05 0.03794 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.22753E-03 0.03342  1.17541E-04 0.20617  7.74004E-04 0.07858  7.99893E-04 0.08251  2.51147E-03 0.04894  8.25213E-04 0.08553  1.99409E-04 0.18085 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.16699E-01 0.08014  1.25171E-02 0.00240  3.10158E-02 0.00190  1.10216E-01 0.00193  3.22274E-01 0.00150  1.31962E+00 0.00523  8.65159E+00 0.03071 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.22753E-03 0.03241  1.15253E-04 0.20698  7.71121E-04 0.07625  7.94181E-04 0.07834  2.48849E-03 0.04782  8.47563E-04 0.08478  2.10915E-04 0.17661 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.27916E-01 0.07938  1.25171E-02 0.00240  3.10170E-02 0.00190  1.10214E-01 0.00192  3.22285E-01 0.00149  1.31985E+00 0.00515  8.65159E+00 0.03071 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.89340E+02 0.03393 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.78608E-05 0.00090 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.63686E-05 0.00061 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.41186E-03 0.00634 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.94308E+02 0.00637 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.97018E-07 0.00074 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.72466E-06 0.00043  2.72444E-06 0.00043  2.76338E-06 0.00584 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.04983E-05 0.00089  4.05188E-05 0.00089  3.69152E-05 0.01073 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.47396E-01 0.00034  6.47548E-01 0.00034  6.34644E-01 0.00977 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06086E+01 0.01415 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.58107E+01 0.00049  3.40769E+01 0.00050 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.79950E+04 0.00381  3.01764E+05 0.00150  6.10030E+05 0.00108  6.50844E+05 0.00066  5.98274E+05 0.00065  6.40368E+05 0.00062  4.34230E+05 0.00086  3.84805E+05 0.00067  2.94345E+05 0.00088  2.40642E+05 0.00089  2.07388E+05 0.00080  1.87113E+05 0.00090  1.72441E+05 0.00093  1.64009E+05 0.00073  1.60092E+05 0.00087  1.38041E+05 0.00093  1.36309E+05 0.00098  1.35330E+05 0.00095  1.33093E+05 0.00099  2.59969E+05 0.00066  2.50770E+05 0.00080  1.80962E+05 0.00073  1.17107E+05 0.00110  1.35842E+05 0.00089  1.28888E+05 0.00086  1.15628E+05 0.00089  1.88534E+05 0.00072  4.33679E+04 0.00145  5.43754E+04 0.00141  4.92801E+04 0.00123  2.86773E+04 0.00233  4.98209E+04 0.00172  3.35198E+04 0.00161  2.77724E+04 0.00209  4.90791E+03 0.00416  4.33959E+03 0.00303  3.89501E+03 0.00342  3.71233E+03 0.00396  3.82033E+03 0.00290  4.18379E+03 0.00302  4.87111E+03 0.00368  4.80256E+03 0.00272  9.44436E+03 0.00250  1.53486E+04 0.00210  1.97618E+04 0.00211  5.27065E+04 0.00159  5.56078E+04 0.00151  5.96765E+04 0.00134  3.92188E+04 0.00136  2.80892E+04 0.00172  2.09457E+04 0.00171  2.47240E+04 0.00160  4.84774E+04 0.00131  6.85592E+04 0.00156  1.36523E+05 0.00137  2.15692E+05 0.00129  3.27616E+05 0.00133  2.11192E+05 0.00142  1.51954E+05 0.00156  1.09596E+05 0.00150  9.88206E+04 0.00136  9.74798E+04 0.00151  8.15951E+04 0.00135  5.52960E+04 0.00155  5.10554E+04 0.00154  4.55070E+04 0.00173  3.85166E+04 0.00168  3.04334E+04 0.00179  2.04228E+04 0.00220  7.23261E+03 0.00265 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.55036E-01 0.00047 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.42690E+18 0.00047  3.87003E+17 0.00120 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38820E-01 0.00014  1.55672E+00 0.00043 ];
INF_CAPT                  (idx, [1:   4]) = [  7.53971E-03 0.00079  3.48200E-02 0.00071 ];
INF_ABS                   (idx, [1:   4]) = [  9.33640E-03 0.00063  6.36107E-02 0.00093 ];
INF_FISS                  (idx, [1:   4]) = [  1.79670E-03 0.00064  2.87907E-02 0.00121 ];
INF_NSF                   (idx, [1:   4]) = [  4.78769E-03 0.00065  7.58237E-02 0.00126 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.66472E+00 6.0E-05  2.63361E+00 6.7E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05183E+02 6.6E-06  2.04939E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.69090E-08 0.00039  2.58856E-06 0.00013 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29480E-01 0.00015  1.49312E+00 0.00048 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44655E-01 0.00025  3.95271E-01 0.00058 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65073E-02 0.00036  9.31911E-02 0.00101 ];
INF_SCATT3                (idx, [1:   4]) = [  7.31386E-03 0.00372  2.78344E-02 0.00250 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03637E-02 0.00194 -8.96323E-03 0.00475 ];
INF_SCATT5                (idx, [1:   4]) = [  1.36738E-04 0.13618  6.68476E-03 0.00881 ];
INF_SCATT6                (idx, [1:   4]) = [  5.12842E-03 0.00339 -1.70032E-02 0.00289 ];
INF_SCATT7                (idx, [1:   4]) = [  7.55222E-04 0.02143  5.32843E-04 0.08712 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29524E-01 0.00015  1.49312E+00 0.00048 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44656E-01 0.00025  3.95271E-01 0.00058 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65075E-02 0.00036  9.31911E-02 0.00101 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.31399E-03 0.00371  2.78344E-02 0.00250 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03638E-02 0.00194 -8.96323E-03 0.00475 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.36591E-04 0.13632  6.68476E-03 0.00881 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.12852E-03 0.00339 -1.70032E-02 0.00289 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.55237E-04 0.02137  5.32843E-04 0.08712 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12300E-01 0.00029  1.00980E+00 0.00038 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.57011E+00 0.00029  3.30100E-01 0.00038 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.29330E-03 0.00065  6.36107E-02 0.00093 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69811E-02 0.00027  6.47252E-02 0.00103 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11839E-01 0.00014  1.76419E-02 0.00051  1.12405E-03 0.00448  1.49200E+00 0.00048 ];
INF_S1                    (idx, [1:   8]) = [  2.39531E-01 0.00025  5.12342E-03 0.00080  4.87868E-04 0.00756  3.94784E-01 0.00058 ];
INF_S2                    (idx, [1:   8]) = [  9.80793E-02 0.00035 -1.57204E-03 0.00172  2.65677E-04 0.00969  9.29254E-02 0.00101 ];
INF_S3                    (idx, [1:   8]) = [  9.13949E-03 0.00304 -1.82562E-03 0.00168  9.59630E-05 0.02270  2.77384E-02 0.00249 ];
INF_S4                    (idx, [1:   8]) = [ -9.77800E-03 0.00200 -5.85684E-04 0.00503  2.29342E-06 1.00000 -8.96552E-03 0.00469 ];
INF_S5                    (idx, [1:   8]) = [  9.98225E-05 0.18379  3.69158E-05 0.07227 -3.94103E-05 0.04057  6.72417E-03 0.00873 ];
INF_S6                    (idx, [1:   8]) = [  5.26231E-03 0.00333 -1.33888E-04 0.01624 -4.83496E-05 0.02284 -1.69549E-02 0.00291 ];
INF_S7                    (idx, [1:   8]) = [  9.25288E-04 0.01753 -1.70066E-04 0.01160 -4.20808E-05 0.03603  5.74924E-04 0.08034 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11882E-01 0.00014  1.76419E-02 0.00051  1.12405E-03 0.00448  1.49200E+00 0.00048 ];
INF_SP1                   (idx, [1:   8]) = [  2.39532E-01 0.00025  5.12342E-03 0.00080  4.87868E-04 0.00756  3.94784E-01 0.00058 ];
INF_SP2                   (idx, [1:   8]) = [  9.80796E-02 0.00035 -1.57204E-03 0.00172  2.65677E-04 0.00969  9.29254E-02 0.00101 ];
INF_SP3                   (idx, [1:   8]) = [  9.13961E-03 0.00304 -1.82562E-03 0.00168  9.59630E-05 0.02270  2.77384E-02 0.00249 ];
INF_SP4                   (idx, [1:   8]) = [ -9.77811E-03 0.00200 -5.85684E-04 0.00503  2.29342E-06 1.00000 -8.96552E-03 0.00469 ];
INF_SP5                   (idx, [1:   8]) = [  9.96748E-05 0.18406  3.69158E-05 0.07227 -3.94103E-05 0.04057  6.72417E-03 0.00873 ];
INF_SP6                   (idx, [1:   8]) = [  5.26241E-03 0.00333 -1.33888E-04 0.01624 -4.83496E-05 0.02284 -1.69549E-02 0.00291 ];
INF_SP7                   (idx, [1:   8]) = [  9.25303E-04 0.01748 -1.70066E-04 0.01160 -4.20808E-05 0.03603  5.74924E-04 0.08034 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32227E-01 0.00058  1.18120E+00 0.00647 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33870E-01 0.00064  1.28723E+00 0.00753 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33890E-01 0.00091  1.27780E+00 0.00827 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28996E-01 0.00082  1.02105E+00 0.00712 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43539E+00 0.00058  2.82481E-01 0.00641 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42531E+00 0.00064  2.59307E-01 0.00755 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42520E+00 0.00091  2.61281E-01 0.00804 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45566E+00 0.00083  3.26853E-01 0.00701 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.40142E-03 0.00944  1.45693E-04 0.05575  9.34014E-04 0.02272  8.52735E-04 0.02346  2.34126E-03 0.01347  8.55511E-04 0.02170  2.72206E-04 0.04305 ];
LAMBDA                    (idx, [1:  14]) = [  8.13344E-01 0.02274  1.25019E-02 0.00037  3.10506E-02 0.00064  1.10460E-01 0.00067  3.22457E-01 0.00046  1.31378E+00 0.00189  8.82349E+00 0.00649 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0001' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:49:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:20:48 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292590 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.97007E-01  1.00493E+00  1.00179E+00  9.94704E-01  1.00157E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14205E-02 0.00107  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88579E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06810E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.07232E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67510E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.57313E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.57218E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.52982E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.22804E-01 0.00107  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000687 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00014E+04 0.00089 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00014E+04 0.00089 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.52395E+02 ;
RUNNING_TIME              (idx, 1)        =  9.09564E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.86050E-01  4.86050E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.52633E-01  1.03833E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  8.94275E+01  3.40030E+00  2.66142E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  7.82750E-01  2.80500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.12667E-02  8.16667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.09562E+01  1.27770E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97375 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00130E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80534E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.53386E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.73316E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.26304E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.92566E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.34542E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.60817E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.59858E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.96720E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.65491E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.24848E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.25193E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.18720E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.82971E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.54940E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.37375E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.48774E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.53833E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.73439E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.02026E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.32852E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.31648E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.66224E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.29236E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.93803E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 14 ;
BURNUP                     (idx, [1:  2])  = [  1.50000E+01  1.50014E+01 ];
BURN_DAYS                 (idx, 1)        =  3.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.62431E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  6.59357E+15 0.00102  4.81987E-01 0.00080 ];
U238_FISS                 (idx, [1:   4]) = [  1.09279E+15 0.00282  7.98686E-02 0.00262 ];
PU239_FISS                (idx, [1:   4]) = [  5.20076E+15 0.00114  3.80172E-01 0.00094 ];
PU240_FISS                (idx, [1:   4]) = [  7.48778E+12 0.03111  5.47466E-04 0.03113 ];
PU241_FISS                (idx, [1:   4]) = [  7.72183E+14 0.00313  5.64485E-02 0.00309 ];
U235_CAPT                 (idx, [1:   4]) = [  1.46298E+15 0.00223  5.76255E-02 0.00221 ];
U238_CAPT                 (idx, [1:   4]) = [  9.56514E+15 0.00103  3.76720E-01 0.00074 ];
PU239_CAPT                (idx, [1:   4]) = [  2.87447E+15 0.00166  1.13219E-01 0.00159 ];
PU240_CAPT                (idx, [1:   4]) = [  1.75675E+15 0.00222  6.91830E-02 0.00201 ];
PU241_CAPT                (idx, [1:   4]) = [  2.77450E+14 0.00536  1.09266E-02 0.00528 ];
XE135_CAPT                (idx, [1:   4]) = [  7.72225E+14 0.00322  3.04190E-02 0.00325 ];
SM149_CAPT                (idx, [1:   4]) = [  2.17215E+14 0.00610  8.55605E-03 0.00610 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000687 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.90178E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000687 5.00790E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3218939 3.22365E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1734510 1.73701E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 47238 4.72483E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000687 5.00790E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.63216E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.64068E+16 2.3E-05  3.64068E+16 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36832E+16 4.6E-06  1.36832E+16 4.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.53772E+16 0.00045  1.89972E+16 0.00046  6.38001E+15 0.00112 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.90604E+16 0.00029  3.26804E+16 0.00027  6.38001E+15 0.00112 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.93803E+16 0.00052  3.93803E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.86368E+18 0.00050  4.91769E+17 0.00049  1.37191E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.72196E+14 0.00472 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.94326E+16 0.00030 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.40872E+18 0.00064 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10772E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10772E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.52022E+00 0.00055 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.71805E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.44675E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23362E+00 0.00038 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93843E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96687E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.33041E-01 0.00059 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.24224E-01 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.66070E+00 2.8E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05292E+02 4.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.24315E-01 0.00061  9.19392E-01 0.00059  4.83156E-03 0.01068 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.24767E-01 0.00031 ];
COL_KEFF                  (idx, [1:   2]) = [  9.24620E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.24767E-01 0.00031 ];
ABS_KINF                  (idx, [1:   2]) = [  9.33590E-01 0.00030 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71324E+01 0.00023 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71391E+01 9.5E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.28248E-07 0.00400 ];
IMP_EALF                  (idx, [1:   2]) = [  7.20919E-07 0.00163 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.70781E-01 0.00285 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.69625E-01 0.00118 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.90202E-03 0.00620  1.63518E-04 0.03833  1.02576E-03 0.01495  9.28269E-04 0.01511  2.56350E-03 0.00967  9.44188E-04 0.01453  2.76796E-04 0.02844 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.79441E-01 0.01479  9.45003E-03 0.02558  3.09418E-02 0.00044  1.10619E-01 0.00050  3.22571E-01 0.00032  1.30270E+00 0.00161  7.94490E+00 0.01572 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.26665E-03 0.00948  1.42077E-04 0.05815  9.05493E-04 0.02385  8.35574E-04 0.02275  2.31463E-03 0.01402  8.32671E-04 0.02342  2.36205E-04 0.04253 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.63085E-01 0.02146  1.25288E-02 0.00073  3.09461E-02 0.00064  1.10537E-01 0.00068  3.22488E-01 0.00046  1.30185E+00 0.00231  8.71445E+00 0.00903 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.85512E-05 0.00141  2.85407E-05 0.00142  3.04694E-05 0.01638 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.63849E-05 0.00125  2.63752E-05 0.00126  2.81590E-05 0.01635 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.23685E-03 0.01065  1.32593E-04 0.06414  9.03886E-04 0.02636  8.46697E-04 0.02756  2.28253E-03 0.01640  8.33165E-04 0.02527  2.37979E-04 0.04851 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.70944E-01 0.02536  1.25330E-02 0.00117  3.09534E-02 0.00084  1.10812E-01 0.00097  3.22532E-01 0.00058  1.30668E+00 0.00283  8.67366E+00 0.01249 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.82211E-05 0.00332  2.82134E-05 0.00334  2.73799E-05 0.03926 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.60793E-05 0.00324  2.60722E-05 0.00326  2.53143E-05 0.03925 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.12002E-03 0.03568  1.45408E-04 0.24014  8.34791E-04 0.08482  8.86501E-04 0.08870  2.22983E-03 0.05302  7.74373E-04 0.09659  2.49121E-04 0.16804 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.08537E-01 0.07960  1.25919E-02 0.00462  3.09426E-02 0.00203  1.10523E-01 0.00213  3.21784E-01 0.00152  1.31219E+00 0.00602  8.84778E+00 0.02608 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.12572E-03 0.03454  1.35172E-04 0.23822  8.24961E-04 0.08178  8.96496E-04 0.08798  2.23156E-03 0.05185  7.75200E-04 0.08973  2.62332E-04 0.16419 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.07705E-01 0.07709  1.25919E-02 0.00462  3.09458E-02 0.00202  1.10533E-01 0.00213  3.21784E-01 0.00152  1.31129E+00 0.00606  8.85523E+00 0.02606 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.83343E+02 0.03637 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.83911E-05 0.00089 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.62370E-05 0.00060 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.17329E-03 0.00664 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.82322E+02 0.00677 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.94266E-07 0.00071 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.70649E-06 0.00043  2.70635E-06 0.00043  2.73335E-06 0.00582 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.05283E-05 0.00086  4.05465E-05 0.00086  3.73116E-05 0.01120 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.42300E-01 0.00035  6.42532E-01 0.00035  6.14330E-01 0.00946 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05952E+01 0.01446 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.57218E+01 0.00048  3.39349E+01 0.00056 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.84212E+04 0.00304  3.01749E+05 0.00176  6.10502E+05 0.00090  6.50295E+05 0.00068  5.97183E+05 0.00070  6.40596E+05 0.00064  4.34553E+05 0.00056  3.84201E+05 0.00066  2.94040E+05 0.00073  2.40529E+05 0.00064  2.07268E+05 0.00074  1.86758E+05 0.00092  1.72658E+05 0.00105  1.63868E+05 0.00077  1.59773E+05 0.00053  1.38167E+05 0.00094  1.36344E+05 0.00097  1.35263E+05 0.00093  1.33165E+05 0.00080  2.59752E+05 0.00070  2.51179E+05 0.00084  1.81162E+05 0.00091  1.17453E+05 0.00096  1.35725E+05 0.00102  1.29156E+05 0.00100  1.15450E+05 0.00058  1.87789E+05 0.00079  4.32523E+04 0.00241  5.41116E+04 0.00191  4.92636E+04 0.00139  2.87333E+04 0.00222  4.96350E+04 0.00147  3.31700E+04 0.00160  2.73797E+04 0.00185  4.74990E+03 0.00316  4.20471E+03 0.00367  3.68990E+03 0.00305  3.53317E+03 0.00317  3.59055E+03 0.00312  4.01272E+03 0.00371  4.69129E+03 0.00313  4.69058E+03 0.00329  9.26132E+03 0.00372  1.51306E+04 0.00201  1.95710E+04 0.00204  5.22979E+04 0.00149  5.50542E+04 0.00149  5.92627E+04 0.00128  3.87007E+04 0.00138  2.75683E+04 0.00163  2.05070E+04 0.00123  2.42666E+04 0.00167  4.77066E+04 0.00153  6.74462E+04 0.00125  1.34817E+05 0.00115  2.13630E+05 0.00130  3.24829E+05 0.00157  2.09670E+05 0.00138  1.50978E+05 0.00136  1.09048E+05 0.00174  9.82705E+04 0.00160  9.69887E+04 0.00152  8.10756E+04 0.00154  5.49611E+04 0.00155  5.08565E+04 0.00176  4.53312E+04 0.00191  3.83805E+04 0.00193  3.03442E+04 0.00198  2.03334E+04 0.00226  7.23559E+03 0.00263 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.33440E-01 0.00055 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.46848E+18 0.00059  3.95231E+17 0.00122 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39088E-01 0.00011  1.56137E+00 0.00043 ];
INF_CAPT                  (idx, [1:   4]) = [  7.72965E-03 0.00074  3.54927E-02 0.00067 ];
INF_ABS                   (idx, [1:   4]) = [  9.47472E-03 0.00062  6.36351E-02 0.00092 ];
INF_FISS                  (idx, [1:   4]) = [  1.74508E-03 0.00045  2.81424E-02 0.00124 ];
INF_NSF                   (idx, [1:   4]) = [  4.67650E-03 0.00045  7.47544E-02 0.00129 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.67982E+00 7.8E-05  2.65629E+00 6.6E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05397E+02 8.7E-06  2.05267E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.65930E-08 0.00035  2.59211E-06 0.00020 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29607E-01 0.00012  1.49774E+00 0.00049 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44738E-01 0.00018  3.96371E-01 0.00058 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65300E-02 0.00028  9.33224E-02 0.00090 ];
INF_SCATT3                (idx, [1:   4]) = [  7.30556E-03 0.00364  2.78739E-02 0.00244 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03673E-02 0.00181 -9.01577E-03 0.00666 ];
INF_SCATT5                (idx, [1:   4]) = [  1.34378E-04 0.12304  6.62227E-03 0.00794 ];
INF_SCATT6                (idx, [1:   4]) = [  5.11493E-03 0.00330 -1.71481E-02 0.00210 ];
INF_SCATT7                (idx, [1:   4]) = [  7.43470E-04 0.02582  4.14059E-04 0.10862 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29650E-01 0.00012  1.49774E+00 0.00049 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44739E-01 0.00018  3.96371E-01 0.00058 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65301E-02 0.00028  9.33224E-02 0.00090 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.30569E-03 0.00364  2.78739E-02 0.00244 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03672E-02 0.00181 -9.01577E-03 0.00666 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.34447E-04 0.12295  6.62227E-03 0.00794 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.11492E-03 0.00331 -1.71481E-02 0.00210 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.43483E-04 0.02583  4.14059E-04 0.10862 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12300E-01 0.00038  1.01344E+00 0.00039 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.57011E+00 0.00038  3.28915E-01 0.00039 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.43234E-03 0.00062  6.36351E-02 0.00092 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69965E-02 0.00024  6.47521E-02 0.00107 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12092E-01 0.00011  1.75154E-02 0.00044  1.12295E-03 0.00453  1.49662E+00 0.00049 ];
INF_S1                    (idx, [1:   8]) = [  2.39654E-01 0.00018  5.08465E-03 0.00118  4.91463E-04 0.00532  3.95879E-01 0.00058 ];
INF_S2                    (idx, [1:   8]) = [  9.81016E-02 0.00028 -1.57162E-03 0.00198  2.69464E-04 0.00685  9.30530E-02 0.00091 ];
INF_S3                    (idx, [1:   8]) = [  9.11757E-03 0.00289 -1.81200E-03 0.00195  9.83538E-05 0.02220  2.77756E-02 0.00246 ];
INF_S4                    (idx, [1:   8]) = [ -9.79755E-03 0.00190 -5.69713E-04 0.00395  2.86722E-06 0.71286 -9.01863E-03 0.00663 ];
INF_S5                    (idx, [1:   8]) = [  9.64175E-05 0.17002  3.79606E-05 0.06028 -3.79785E-05 0.03929  6.66025E-03 0.00783 ];
INF_S6                    (idx, [1:   8]) = [  5.25445E-03 0.00303 -1.39515E-04 0.01792 -4.97144E-05 0.03012 -1.70984E-02 0.00209 ];
INF_S7                    (idx, [1:   8]) = [  9.13689E-04 0.02035 -1.70219E-04 0.01312 -4.62154E-05 0.02826  4.60275E-04 0.09871 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12134E-01 0.00011  1.75154E-02 0.00044  1.12295E-03 0.00453  1.49662E+00 0.00049 ];
INF_SP1                   (idx, [1:   8]) = [  2.39654E-01 0.00018  5.08465E-03 0.00118  4.91463E-04 0.00532  3.95879E-01 0.00058 ];
INF_SP2                   (idx, [1:   8]) = [  9.81018E-02 0.00028 -1.57162E-03 0.00198  2.69464E-04 0.00685  9.30530E-02 0.00091 ];
INF_SP3                   (idx, [1:   8]) = [  9.11769E-03 0.00288 -1.81200E-03 0.00195  9.83538E-05 0.02220  2.77756E-02 0.00246 ];
INF_SP4                   (idx, [1:   8]) = [ -9.79748E-03 0.00191 -5.69713E-04 0.00395  2.86722E-06 0.71286 -9.01863E-03 0.00663 ];
INF_SP5                   (idx, [1:   8]) = [  9.64864E-05 0.16986  3.79606E-05 0.06028 -3.79785E-05 0.03929  6.66025E-03 0.00783 ];
INF_SP6                   (idx, [1:   8]) = [  5.25444E-03 0.00304 -1.39515E-04 0.01792 -4.97144E-05 0.03012 -1.70984E-02 0.00209 ];
INF_SP7                   (idx, [1:   8]) = [  9.13703E-04 0.02035 -1.70219E-04 0.01312 -4.62154E-05 0.02826  4.60275E-04 0.09871 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31897E-01 0.00059  1.18840E+00 0.00657 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33628E-01 0.00094  1.29413E+00 0.00864 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33725E-01 0.00095  1.28149E+00 0.00884 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28428E-01 0.00097  1.03069E+00 0.00607 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43743E+00 0.00059  2.80779E-01 0.00658 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42680E+00 0.00094  2.58049E-01 0.00888 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42621E+00 0.00096  2.60598E-01 0.00877 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45928E+00 0.00097  3.23690E-01 0.00594 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.26665E-03 0.00948  1.42077E-04 0.05815  9.05493E-04 0.02385  8.35574E-04 0.02275  2.31463E-03 0.01402  8.32671E-04 0.02342  2.36205E-04 0.04253 ];
LAMBDA                    (idx, [1:  14]) = [  7.63085E-01 0.02146  1.25288E-02 0.00073  3.09461E-02 0.00064  1.10537E-01 0.00068  3.22488E-01 0.00046  1.30185E+00 0.00231  8.71445E+00 0.00903 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0001' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:49:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:26:55 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292590 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.98297E-01  1.00435E+00  1.00166E+00  9.93977E-01  1.00172E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14681E-02 0.00102  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88532E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06602E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.07026E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67723E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.56194E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.56099E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.51430E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.23596E-01 0.00103  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000673 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00013E+04 0.00087 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00013E+04 0.00087 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.82950E+02 ;
RUNNING_TIME              (idx, 1)        =  9.70740E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.86050E-01  4.86050E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.72767E-01  9.95000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.54689E+01  3.39763E+00  2.64378E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  8.38433E-01  2.71667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.39667E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.70739E+01  1.27665E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97507 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00006E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80848E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.59042E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.72468E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.95019E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.97763E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.38277E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61275E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.58637E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.36570E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.73718E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.61053E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.54276E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.55173E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.88291E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.75936E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.39677E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.51965E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.57046E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.32173E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.35453E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.38016E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.29124E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  4.22808E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.30761E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.04793E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 15 ;
BURNUP                     (idx, [1:  2])  = [  1.75000E+01  1.75016E+01 ];
BURN_DAYS                 (idx, 1)        =  4.37500E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.87135E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  5.98592E+15 0.00116  4.37824E-01 0.00093 ];
U238_FISS                 (idx, [1:   4]) = [  1.12070E+15 0.00257  8.19639E-02 0.00241 ];
PU239_FISS                (idx, [1:   4]) = [  5.56999E+15 0.00113  4.07413E-01 0.00095 ];
PU240_FISS                (idx, [1:   4]) = [  9.25275E+12 0.02882  6.76738E-04 0.02881 ];
PU241_FISS                (idx, [1:   4]) = [  9.70430E+14 0.00281  7.09803E-02 0.00273 ];
U235_CAPT                 (idx, [1:   4]) = [  1.33143E+15 0.00244  5.02675E-02 0.00242 ];
U238_CAPT                 (idx, [1:   4]) = [  9.79337E+15 0.00102  3.69703E-01 0.00072 ];
PU239_CAPT                (idx, [1:   4]) = [  3.06636E+15 0.00147  1.15771E-01 0.00147 ];
PU240_CAPT                (idx, [1:   4]) = [  2.01215E+15 0.00203  7.59599E-02 0.00190 ];
PU241_CAPT                (idx, [1:   4]) = [  3.47830E+14 0.00472  1.31319E-02 0.00470 ];
XE135_CAPT                (idx, [1:   4]) = [  7.74183E+14 0.00318  2.92277E-02 0.00315 ];
SM149_CAPT                (idx, [1:   4]) = [  2.23943E+14 0.00597  8.45419E-03 0.00593 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000673 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.96642E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000673 5.00797E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3266983 3.27188E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1686423 1.68882E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 47267 4.72728E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000673 5.00797E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.49480E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.66289E+16 2.2E-05  3.66289E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36638E+16 4.6E-06  1.36638E+16 4.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.64816E+16 0.00043  1.99678E+16 0.00045  6.51379E+15 0.00114 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.01455E+16 0.00029  3.36317E+16 0.00027  6.51379E+15 0.00114 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.04793E+16 0.00050  4.04793E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.91261E+18 0.00048  5.04993E+17 0.00047  1.40761E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.82768E+14 0.00503 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.05282E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.44353E+18 0.00061 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10482E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10482E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.50099E+00 0.00057 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.72042E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.39808E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23291E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93827E-01 3.9E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96698E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.14037E-01 0.00061 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.05395E-01 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.68072E+00 2.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05582E+02 4.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.05380E-01 0.00061  9.00710E-01 0.00061  4.68429E-03 0.01126 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.05279E-01 0.00030 ];
COL_KEFF                  (idx, [1:   2]) = [  9.04990E-01 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.05279E-01 0.00030 ];
ABS_KINF                  (idx, [1:   2]) = [  9.13922E-01 0.00029 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70922E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70957E+01 9.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.57649E-07 0.00369 ];
IMP_EALF                  (idx, [1:   2]) = [  7.52974E-07 0.00166 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.78281E-01 0.00258 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.77998E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.94488E-03 0.00670  1.46421E-04 0.03749  1.03659E-03 0.01612  9.24591E-04 0.01487  2.61815E-03 0.00931  9.34430E-04 0.01454  2.84702E-04 0.02849 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.79406E-01 0.01456  9.29231E-03 0.02655  3.08518E-02 0.00047  1.10711E-01 0.00052  3.22591E-01 0.00032  1.28936E+00 0.00193  7.94772E+00 0.01515 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.29299E-03 0.00996  1.30028E-04 0.05993  8.94432E-04 0.02346  8.14532E-04 0.02321  2.36683E-03 0.01354  8.31255E-04 0.02200  2.55905E-04 0.04477 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.82027E-01 0.02194  1.25594E-02 0.00106  3.08536E-02 0.00068  1.10646E-01 0.00074  3.22491E-01 0.00051  1.28507E+00 0.00289  8.68724E+00 0.00852 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.90798E-05 0.00144  2.90698E-05 0.00145  3.10417E-05 0.01590 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.63234E-05 0.00130  2.63143E-05 0.00131  2.81051E-05 0.01592 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.17866E-03 0.01133  1.21059E-04 0.07171  8.88141E-04 0.02809  7.98570E-04 0.02811  2.28351E-03 0.01622  8.28227E-04 0.02496  2.59154E-04 0.05007 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.06065E-01 0.02734  1.25602E-02 0.00160  3.08487E-02 0.00089  1.10597E-01 0.00098  3.22393E-01 0.00063  1.28568E+00 0.00372  8.73737E+00 0.01117 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.85528E-05 0.00320  2.85422E-05 0.00320  2.82627E-05 0.03648 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.58468E-05 0.00315  2.58372E-05 0.00316  2.55958E-05 0.03650 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.51301E-03 0.03541  1.17209E-04 0.21480  8.80898E-04 0.08547  9.44070E-04 0.08996  2.43156E-03 0.05465  8.71912E-04 0.08582  2.67367E-04 0.16082 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.94365E-01 0.08504  1.25513E-02 0.00352  3.08055E-02 0.00202  1.10618E-01 0.00229  3.23235E-01 0.00167  1.29541E+00 0.00751  8.34136E+00 0.03565 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.49417E-03 0.03506  1.12996E-04 0.21505  8.82707E-04 0.08372  9.42472E-04 0.08970  2.41971E-03 0.05329  8.67178E-04 0.08445  2.69109E-04 0.15080 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.01730E-01 0.08541  1.25513E-02 0.00352  3.08079E-02 0.00202  1.10605E-01 0.00227  3.23185E-01 0.00165  1.29679E+00 0.00741  8.33394E+00 0.03570 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.92652E+02 0.03515 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.88849E-05 0.00085 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.61469E-05 0.00059 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.24705E-03 0.00624 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.81700E+02 0.00627 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.90796E-07 0.00075 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.68945E-06 0.00041  2.68921E-06 0.00041  2.73508E-06 0.00610 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.05260E-05 0.00092  4.05477E-05 0.00092  3.66767E-05 0.01159 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.37489E-01 0.00034  6.37754E-01 0.00035  6.06128E-01 0.01025 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05991E+01 0.01478 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.56099E+01 0.00049  3.38557E+01 0.00054 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.92638E+04 0.00364  3.03829E+05 0.00134  6.09328E+05 0.00074  6.50511E+05 0.00056  5.98225E+05 0.00076  6.40300E+05 0.00068  4.34877E+05 0.00058  3.84783E+05 0.00080  2.94686E+05 0.00067  2.40335E+05 0.00075  2.07376E+05 0.00081  1.87011E+05 0.00089  1.72828E+05 0.00092  1.64008E+05 0.00090  1.59959E+05 0.00088  1.38325E+05 0.00116  1.36276E+05 0.00097  1.35447E+05 0.00107  1.32945E+05 0.00096  2.59888E+05 0.00063  2.51346E+05 0.00060  1.81320E+05 0.00068  1.17512E+05 0.00082  1.35754E+05 0.00076  1.28877E+05 0.00113  1.15316E+05 0.00097  1.87488E+05 0.00072  4.33932E+04 0.00176  5.40796E+04 0.00138  4.89791E+04 0.00156  2.86658E+04 0.00210  4.95085E+04 0.00119  3.30605E+04 0.00172  2.70773E+04 0.00196  4.68351E+03 0.00350  4.05304E+03 0.00354  3.56059E+03 0.00412  3.38141E+03 0.00375  3.48557E+03 0.00370  3.82916E+03 0.00349  4.54618E+03 0.00342  4.58865E+03 0.00390  9.09679E+03 0.00214  1.49616E+04 0.00257  1.93882E+04 0.00250  5.17666E+04 0.00137  5.44215E+04 0.00099  5.84582E+04 0.00127  3.82390E+04 0.00145  2.71201E+04 0.00163  2.01777E+04 0.00177  2.38744E+04 0.00173  4.69358E+04 0.00135  6.65995E+04 0.00122  1.33399E+05 0.00149  2.11706E+05 0.00120  3.22326E+05 0.00125  2.08269E+05 0.00125  1.49924E+05 0.00129  1.08152E+05 0.00161  9.74828E+04 0.00167  9.63040E+04 0.00154  8.06364E+04 0.00167  5.46408E+04 0.00144  5.05704E+04 0.00167  4.50860E+04 0.00209  3.81328E+04 0.00216  3.01768E+04 0.00194  2.02451E+04 0.00198  7.18746E+03 0.00225 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.13628E-01 0.00045 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.50986E+18 0.00040  4.02784E+17 0.00133 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39137E-01 0.00013  1.56299E+00 0.00043 ];
INF_CAPT                  (idx, [1:   4]) = [  7.89801E-03 0.00059  3.61444E-02 0.00073 ];
INF_ABS                   (idx, [1:   4]) = [  9.59340E-03 0.00053  6.37179E-02 0.00101 ];
INF_FISS                  (idx, [1:   4]) = [  1.69539E-03 0.00048  2.75735E-02 0.00139 ];
INF_NSF                   (idx, [1:   4]) = [  4.56915E-03 0.00047  7.38256E-02 0.00143 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.69504E+00 7.4E-05  2.67741E+00 5.9E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05607E+02 8.7E-06  2.05577E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.62854E-08 0.00043  2.59460E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29529E-01 0.00013  1.49930E+00 0.00049 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44573E-01 0.00022  3.96675E-01 0.00054 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64854E-02 0.00034  9.34865E-02 0.00094 ];
INF_SCATT3                (idx, [1:   4]) = [  7.30560E-03 0.00336  2.81054E-02 0.00214 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03396E-02 0.00205 -8.88199E-03 0.00565 ];
INF_SCATT5                (idx, [1:   4]) = [  1.69092E-04 0.10888  6.70034E-03 0.00803 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10759E-03 0.00264 -1.71897E-02 0.00280 ];
INF_SCATT7                (idx, [1:   4]) = [  7.60336E-04 0.02109  5.20456E-04 0.08989 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29571E-01 0.00013  1.49930E+00 0.00049 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44573E-01 0.00022  3.96675E-01 0.00054 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64857E-02 0.00034  9.34865E-02 0.00094 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.30560E-03 0.00337  2.81054E-02 0.00214 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03394E-02 0.00205 -8.88199E-03 0.00565 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.69159E-04 0.10878  6.70034E-03 0.00803 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10764E-03 0.00264 -1.71897E-02 0.00280 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.60269E-04 0.02113  5.20456E-04 0.08989 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12306E-01 0.00023  1.01527E+00 0.00037 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.57006E+00 0.00023  3.28321E-01 0.00037 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.55069E-03 0.00054  6.37179E-02 0.00101 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69870E-02 0.00024  6.48177E-02 0.00105 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12150E-01 0.00013  1.73788E-02 0.00039  1.12370E-03 0.00420  1.49817E+00 0.00049 ];
INF_S1                    (idx, [1:   8]) = [  2.39538E-01 0.00022  5.03448E-03 0.00081  4.84463E-04 0.00791  3.96190E-01 0.00054 ];
INF_S2                    (idx, [1:   8]) = [  9.80490E-02 0.00034 -1.56358E-03 0.00149  2.64888E-04 0.00845  9.32216E-02 0.00094 ];
INF_S3                    (idx, [1:   8]) = [  9.10432E-03 0.00275 -1.79871E-03 0.00161  9.50292E-05 0.02422  2.80104E-02 0.00212 ];
INF_S4                    (idx, [1:   8]) = [ -9.77025E-03 0.00215 -5.69392E-04 0.00442 -8.73941E-07 1.00000 -8.88112E-03 0.00566 ];
INF_S5                    (idx, [1:   8]) = [  1.33161E-04 0.13486  3.59312E-05 0.06506 -4.17478E-05 0.03790  6.74209E-03 0.00797 ];
INF_S6                    (idx, [1:   8]) = [  5.24357E-03 0.00246 -1.35981E-04 0.02014 -5.15101E-05 0.02852 -1.71382E-02 0.00280 ];
INF_S7                    (idx, [1:   8]) = [  9.30243E-04 0.01583 -1.69907E-04 0.01905 -4.43656E-05 0.02747  5.64821E-04 0.08278 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12192E-01 0.00013  1.73788E-02 0.00039  1.12370E-03 0.00420  1.49817E+00 0.00049 ];
INF_SP1                   (idx, [1:   8]) = [  2.39538E-01 0.00022  5.03448E-03 0.00081  4.84463E-04 0.00791  3.96190E-01 0.00054 ];
INF_SP2                   (idx, [1:   8]) = [  9.80493E-02 0.00034 -1.56358E-03 0.00149  2.64888E-04 0.00845  9.32216E-02 0.00094 ];
INF_SP3                   (idx, [1:   8]) = [  9.10431E-03 0.00276 -1.79871E-03 0.00161  9.50292E-05 0.02422  2.80104E-02 0.00212 ];
INF_SP4                   (idx, [1:   8]) = [ -9.77005E-03 0.00215 -5.69392E-04 0.00442 -8.73941E-07 1.00000 -8.88112E-03 0.00566 ];
INF_SP5                   (idx, [1:   8]) = [  1.33228E-04 0.13471  3.59312E-05 0.06506 -4.17478E-05 0.03790  6.74209E-03 0.00797 ];
INF_SP6                   (idx, [1:   8]) = [  5.24362E-03 0.00246 -1.35981E-04 0.02014 -5.15101E-05 0.02852 -1.71382E-02 0.00280 ];
INF_SP7                   (idx, [1:   8]) = [  9.30176E-04 0.01587 -1.69907E-04 0.01905 -4.43656E-05 0.02747  5.64821E-04 0.08278 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32144E-01 0.00045  1.20258E+00 0.00536 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33959E-01 0.00076  1.30304E+00 0.00579 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33682E-01 0.00093  1.31205E+00 0.00841 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28870E-01 0.00071  1.03739E+00 0.00614 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43589E+00 0.00045  2.77378E-01 0.00548 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42477E+00 0.00076  2.56025E-01 0.00603 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42647E+00 0.00093  2.54496E-01 0.00857 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45645E+00 0.00070  3.21612E-01 0.00615 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.29299E-03 0.00996  1.30028E-04 0.05993  8.94432E-04 0.02346  8.14532E-04 0.02321  2.36683E-03 0.01354  8.31255E-04 0.02200  2.55905E-04 0.04477 ];
LAMBDA                    (idx, [1:  14]) = [  7.82027E-01 0.02194  1.25594E-02 0.00106  3.08536E-02 0.00068  1.10646E-01 0.00074  3.22491E-01 0.00051  1.28507E+00 0.00289  8.68724E+00 0.00852 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0001' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:49:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:33:00 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292590 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99393E-01  1.00353E+00  1.00058E+00  9.93567E-01  1.00293E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.1E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15055E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88494E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06670E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.07095E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67899E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.55495E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.55399E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.49378E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.24153E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001262 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00025E+04 0.00092 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00025E+04 0.00092 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.13334E+02 ;
RUNNING_TIME              (idx, 1)        =  1.03158E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.86050E-01  4.86050E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.93800E-01  9.81667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.01477E+02  3.39687E+00  2.61108E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  8.92700E-01  2.71833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.66167E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.03157E+02  1.27488E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97621 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99960E+00 0.00022 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81114E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.64841E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.71841E+04 ;
TOT_SF_RATE               (idx, 1)        =  6.39267E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.02941E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.42044E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61896E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.57633E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.81122E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.81731E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.02301E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.84035E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.88212E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.93328E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.95923E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.41804E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.54829E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.59994E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.97897E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.68786E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.43285E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.26915E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  6.21983E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.32271E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.15419E+12 0.00048  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 16 ;
BURNUP                     (idx, [1:  2])  = [  2.00000E+01  2.00019E+01 ];
BURN_DAYS                 (idx, 1)        =  5.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.09007E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  5.44338E+15 0.00121  3.98763E-01 0.00105 ];
U238_FISS                 (idx, [1:   4]) = [  1.15112E+15 0.00270  8.43178E-02 0.00254 ];
PU239_FISS                (idx, [1:   4]) = [  5.87269E+15 0.00112  4.30207E-01 0.00090 ];
PU240_FISS                (idx, [1:   4]) = [  1.01943E+13 0.02792  7.47089E-04 0.02794 ];
PU241_FISS                (idx, [1:   4]) = [  1.15413E+15 0.00267  8.45479E-02 0.00260 ];
U235_CAPT                 (idx, [1:   4]) = [  1.21120E+15 0.00269  4.39492E-02 0.00267 ];
U238_CAPT                 (idx, [1:   4]) = [  9.99654E+15 0.00104  3.62692E-01 0.00074 ];
PU239_CAPT                (idx, [1:   4]) = [  3.23844E+15 0.00150  1.17510E-01 0.00148 ];
PU240_CAPT                (idx, [1:   4]) = [  2.22727E+15 0.00205  8.08074E-02 0.00189 ];
PU241_CAPT                (idx, [1:   4]) = [  4.15396E+14 0.00473  1.50724E-02 0.00471 ];
XE135_CAPT                (idx, [1:   4]) = [  7.84347E+14 0.00324  2.84603E-02 0.00322 ];
SM149_CAPT                (idx, [1:   4]) = [  2.31742E+14 0.00572  8.40931E-03 0.00573 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001262 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.10072E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001262 5.00810E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3312555 3.31722E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1640954 1.64311E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 47753 4.77635E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001262 5.00810E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -1.76951E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.68269E+16 2.4E-05  3.68269E+16 2.4E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36464E+16 5.0E-06  1.36464E+16 5.0E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.75433E+16 0.00046  2.08829E+16 0.00046  6.66037E+15 0.00117 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.11897E+16 0.00031  3.45293E+16 0.00028  6.66037E+15 0.00117 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.15419E+16 0.00048  4.15419E+16 0.00048  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.95987E+18 0.00046  5.17049E+17 0.00048  1.44282E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.96860E+14 0.00511 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.15865E+16 0.00031 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.47852E+18 0.00063 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10193E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10193E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.48109E+00 0.00054 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.70994E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.36590E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23171E+00 0.00038 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93771E-01 4.0E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96655E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.95304E-01 0.00064 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.86753E-01 0.00065 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.69866E+00 2.8E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05846E+02 5.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.86790E-01 0.00065  8.82235E-01 0.00065  4.51775E-03 0.01091 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.87033E-01 0.00032 ];
COL_KEFF                  (idx, [1:   2]) = [  8.86603E-01 0.00048 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.87033E-01 0.00032 ];
ABS_KINF                  (idx, [1:   2]) = [  8.95592E-01 0.00031 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70534E+01 0.00024 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70570E+01 0.00010 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.88191E-07 0.00413 ];
IMP_EALF                  (idx, [1:   2]) = [  7.82716E-07 0.00179 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.86649E-01 0.00294 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.85275E-01 0.00120 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.95195E-03 0.00627  1.54125E-04 0.03949  1.05531E-03 0.01406  9.02780E-04 0.01608  2.58322E-03 0.00942  9.84009E-04 0.01513  2.72506E-04 0.02946 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.63809E-01 0.01518  9.10544E-03 0.02766  3.08029E-02 0.00049  1.10858E-01 0.00056  3.22569E-01 0.00033  1.28211E+00 0.00191  7.66043E+00 0.01735 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.12397E-03 0.00958  1.31542E-04 0.06595  9.10640E-04 0.02278  7.70814E-04 0.02456  2.23782E-03 0.01396  8.39798E-04 0.02341  2.33350E-04 0.04583 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.56118E-01 0.02300  1.25851E-02 0.00122  3.07898E-02 0.00068  1.10857E-01 0.00081  3.22491E-01 0.00053  1.28158E+00 0.00293  8.54809E+00 0.01010 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.96149E-05 0.00150  2.95959E-05 0.00150  3.32395E-05 0.01688 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.62564E-05 0.00134  2.62397E-05 0.00135  2.94453E-05 0.01672 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.09414E-03 0.01102  1.32381E-04 0.07546  8.79565E-04 0.02718  7.63819E-04 0.02804  2.22167E-03 0.01640  8.56350E-04 0.02804  2.40357E-04 0.04863 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.72500E-01 0.02551  1.25561E-02 0.00158  3.08113E-02 0.00095  1.10599E-01 0.00104  3.22614E-01 0.00066  1.27991E+00 0.00377  8.39984E+00 0.01509 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.91190E-05 0.00343  2.90918E-05 0.00344  3.05542E-05 0.04197 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.58167E-05 0.00337  2.57927E-05 0.00338  2.70729E-05 0.04177 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.95827E-03 0.03689  1.17620E-04 0.26164  9.06273E-04 0.08731  7.49145E-04 0.08933  2.20631E-03 0.05909  7.93586E-04 0.09387  1.85336E-04 0.19476 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  6.72816E-01 0.08178  1.25264E-02 0.00306  3.07990E-02 0.00203  1.10719E-01 0.00236  3.22970E-01 0.00170  1.27367E+00 0.00886  8.09257E+00 0.04521 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.93758E-03 0.03612  1.16800E-04 0.25385  8.87893E-04 0.08374  7.55362E-04 0.08909  2.19449E-03 0.05815  7.97590E-04 0.09342  1.85453E-04 0.18901 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  6.78968E-01 0.08156  1.25264E-02 0.00306  3.08000E-02 0.00203  1.10699E-01 0.00236  3.22919E-01 0.00169  1.27283E+00 0.00894  8.09323E+00 0.04522 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.71739E+02 0.03703 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.93696E-05 0.00092 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.60391E-05 0.00064 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.09006E-03 0.00625 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.73414E+02 0.00637 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.88756E-07 0.00077 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.67623E-06 0.00044  2.67618E-06 0.00044  2.68234E-06 0.00589 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.05350E-05 0.00092  4.05523E-05 0.00092  3.74246E-05 0.01168 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.34289E-01 0.00036  6.34650E-01 0.00036  5.85599E-01 0.00987 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08590E+01 0.01475 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.55399E+01 0.00050  3.37815E+01 0.00054 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.94641E+04 0.00277  3.04284E+05 0.00116  6.10034E+05 0.00101  6.50949E+05 0.00082  5.98375E+05 0.00068  6.40057E+05 0.00062  4.34189E+05 0.00091  3.84366E+05 0.00052  2.94409E+05 0.00080  2.40759E+05 0.00079  2.07184E+05 0.00080  1.86805E+05 0.00099  1.72685E+05 0.00089  1.64204E+05 0.00078  1.59956E+05 0.00093  1.38162E+05 0.00116  1.36478E+05 0.00102  1.35072E+05 0.00071  1.33020E+05 0.00099  2.59951E+05 0.00078  2.51100E+05 0.00079  1.81071E+05 0.00087  1.17421E+05 0.00089  1.35890E+05 0.00086  1.29400E+05 0.00076  1.15313E+05 0.00080  1.87182E+05 0.00058  4.32107E+04 0.00148  5.37597E+04 0.00167  4.89452E+04 0.00145  2.86342E+04 0.00165  4.94331E+04 0.00145  3.28241E+04 0.00151  2.68414E+04 0.00164  4.57394E+03 0.00270  3.95028E+03 0.00319  3.45355E+03 0.00444  3.30492E+03 0.00259  3.37952E+03 0.00369  3.72918E+03 0.00370  4.42859E+03 0.00422  4.50426E+03 0.00457  8.94954E+03 0.00255  1.47445E+04 0.00221  1.91780E+04 0.00250  5.14373E+04 0.00162  5.42592E+04 0.00118  5.80006E+04 0.00132  3.79216E+04 0.00162  2.68640E+04 0.00163  1.99784E+04 0.00169  2.35999E+04 0.00175  4.63738E+04 0.00175  6.59282E+04 0.00127  1.31990E+05 0.00136  2.09995E+05 0.00132  3.20553E+05 0.00143  2.07347E+05 0.00160  1.49582E+05 0.00141  1.07774E+05 0.00153  9.72790E+04 0.00164  9.59222E+04 0.00170  8.02687E+04 0.00167  5.44957E+04 0.00156  5.03967E+04 0.00189  4.48938E+04 0.00177  3.80071E+04 0.00171  3.00432E+04 0.00144  2.01353E+04 0.00206  7.13972E+03 0.00268 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.95154E-01 0.00050 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.54890E+18 0.00049  4.11004E+17 0.00134 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39338E-01 9.1E-05  1.56487E+00 0.00040 ];
INF_CAPT                  (idx, [1:   4]) = [  8.04683E-03 0.00064  3.66929E-02 0.00074 ];
INF_ABS                   (idx, [1:   4]) = [  9.69681E-03 0.00057  6.36821E-02 0.00100 ];
INF_FISS                  (idx, [1:   4]) = [  1.64998E-03 0.00052  2.69892E-02 0.00135 ];
INF_NSF                   (idx, [1:   4]) = [  4.46961E-03 0.00051  7.27712E-02 0.00138 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.70888E+00 5.5E-05  2.69631E+00 5.7E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05802E+02 8.3E-06  2.05856E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.60485E-08 0.00043  2.59644E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29641E-01 9.4E-05  1.50114E+00 0.00046 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44651E-01 0.00020  3.97282E-01 0.00054 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65113E-02 0.00029  9.35483E-02 0.00077 ];
INF_SCATT3                (idx, [1:   4]) = [  7.23862E-03 0.00335  2.80338E-02 0.00246 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.04143E-02 0.00181 -8.89791E-03 0.00617 ];
INF_SCATT5                (idx, [1:   4]) = [  1.46820E-04 0.12106  6.82289E-03 0.00949 ];
INF_SCATT6                (idx, [1:   4]) = [  5.12753E-03 0.00332 -1.72204E-02 0.00315 ];
INF_SCATT7                (idx, [1:   4]) = [  7.59960E-04 0.01897  5.04348E-04 0.08063 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29684E-01 9.5E-05  1.50114E+00 0.00046 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44652E-01 0.00020  3.97282E-01 0.00054 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65115E-02 0.00029  9.35483E-02 0.00077 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.23869E-03 0.00336  2.80338E-02 0.00246 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.04141E-02 0.00181 -8.89791E-03 0.00617 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.46811E-04 0.12130  6.82289E-03 0.00949 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.12762E-03 0.00331 -1.72204E-02 0.00315 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.59940E-04 0.01899  5.04348E-04 0.08063 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12266E-01 0.00027  1.01692E+00 0.00032 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.57036E+00 0.00027  3.27787E-01 0.00032 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.65336E-03 0.00057  6.36821E-02 0.00100 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69921E-02 0.00025  6.48437E-02 0.00113 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12346E-01 9.0E-05  1.72950E-02 0.00041  1.11364E-03 0.00528  1.50002E+00 0.00046 ];
INF_S1                    (idx, [1:   8]) = [  2.39643E-01 0.00020  5.00822E-03 0.00098  4.83006E-04 0.00691  3.96799E-01 0.00055 ];
INF_S2                    (idx, [1:   8]) = [  9.80753E-02 0.00030 -1.56400E-03 0.00258  2.62286E-04 0.01040  9.32860E-02 0.00078 ];
INF_S3                    (idx, [1:   8]) = [  9.03029E-03 0.00267 -1.79167E-03 0.00199  9.61117E-05 0.01809  2.79377E-02 0.00247 ];
INF_S4                    (idx, [1:   8]) = [ -9.85159E-03 0.00196 -5.62675E-04 0.00525  2.50702E-06 0.56140 -8.90042E-03 0.00612 ];
INF_S5                    (idx, [1:   8]) = [  1.08127E-04 0.17086  3.86930E-05 0.06705 -3.60568E-05 0.04718  6.85895E-03 0.00943 ];
INF_S6                    (idx, [1:   8]) = [  5.26065E-03 0.00322 -1.33121E-04 0.01871 -4.66102E-05 0.02883 -1.71738E-02 0.00315 ];
INF_S7                    (idx, [1:   8]) = [  9.28698E-04 0.01525 -1.68739E-04 0.00930 -4.21787E-05 0.02444  5.46527E-04 0.07468 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12389E-01 9.0E-05  1.72950E-02 0.00041  1.11364E-03 0.00528  1.50002E+00 0.00046 ];
INF_SP1                   (idx, [1:   8]) = [  2.39644E-01 0.00020  5.00822E-03 0.00098  4.83006E-04 0.00691  3.96799E-01 0.00055 ];
INF_SP2                   (idx, [1:   8]) = [  9.80755E-02 0.00029 -1.56400E-03 0.00258  2.62286E-04 0.01040  9.32860E-02 0.00078 ];
INF_SP3                   (idx, [1:   8]) = [  9.03036E-03 0.00267 -1.79167E-03 0.00199  9.61117E-05 0.01809  2.79377E-02 0.00247 ];
INF_SP4                   (idx, [1:   8]) = [ -9.85143E-03 0.00196 -5.62675E-04 0.00525  2.50702E-06 0.56140 -8.90042E-03 0.00612 ];
INF_SP5                   (idx, [1:   8]) = [  1.08118E-04 0.17120  3.86930E-05 0.06705 -3.60568E-05 0.04718  6.85895E-03 0.00943 ];
INF_SP6                   (idx, [1:   8]) = [  5.26074E-03 0.00322 -1.33121E-04 0.01871 -4.66102E-05 0.02883 -1.71738E-02 0.00315 ];
INF_SP7                   (idx, [1:   8]) = [  9.28679E-04 0.01527 -1.68739E-04 0.00930 -4.21787E-05 0.02444  5.46527E-04 0.07468 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31940E-01 0.00063  1.22156E+00 0.00539 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33456E-01 0.00096  1.33084E+00 0.00641 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33701E-01 0.00086  1.32981E+00 0.00750 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28739E-01 0.00105  1.05121E+00 0.00710 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43717E+00 0.00063  2.73066E-01 0.00542 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42785E+00 0.00096  2.50715E-01 0.00640 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42635E+00 0.00086  2.51002E-01 0.00755 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45731E+00 0.00105  3.17482E-01 0.00716 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.12397E-03 0.00958  1.31542E-04 0.06595  9.10640E-04 0.02278  7.70814E-04 0.02456  2.23782E-03 0.01396  8.39798E-04 0.02341  2.33350E-04 0.04583 ];
LAMBDA                    (idx, [1:  14]) = [  7.56118E-01 0.02300  1.25851E-02 0.00122  3.07898E-02 0.00068  1.10857E-01 0.00081  3.22491E-01 0.00053  1.28158E+00 0.00293  8.54809E+00 0.01010 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0001' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:49:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:39:05 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292590 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99232E-01  1.00346E+00  1.00221E+00  9.94209E-01  1.00090E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15744E-02 0.00105  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88426E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06043E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06469E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68280E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.54073E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.53980E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.49385E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.25694E-01 0.00102  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001091 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00022E+04 0.00094 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00022E+04 0.00094 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.43753E+02 ;
RUNNING_TIME              (idx, 1)        =  1.09249E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.86050E-01  4.86050E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.14517E-01  1.05000E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.07492E+02  3.38660E+00  2.62830E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  9.47600E-01  2.85500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.98833E-02  7.83336E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.09249E+02  1.27558E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97720 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99884E+00 0.00022 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81344E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.67367E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68581E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.40547E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.10275E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.47561E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.57089E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.53822E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  3.85564E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.93572E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.01138E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.33873E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  8.44258E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.00184E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.33223E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.39553E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.55469E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.58930E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.46843E+13 ;
CS137_ACTIVITY            (idx, 1)        =  3.35145E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.45401E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.22024E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.13503E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.33811E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.35302E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 17 ;
BURNUP                     (idx, [1:  2])  = [  2.50000E+01  2.50023E+01 ];
BURN_DAYS                 (idx, 1)        =  6.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.53838E-01 0.00105 ];
U235_FISS                 (idx, [1:   4]) = [  4.44814E+15 0.00135  3.26856E-01 0.00112 ];
U238_FISS                 (idx, [1:   4]) = [  1.19837E+15 0.00300  8.80382E-02 0.00274 ];
PU239_FISS                (idx, [1:   4]) = [  6.39070E+15 0.00103  4.69624E-01 0.00085 ];
PU240_FISS                (idx, [1:   4]) = [  1.36704E+13 0.02593  1.00429E-03 0.02596 ];
PU241_FISS                (idx, [1:   4]) = [  1.53184E+15 0.00242  1.12561E-01 0.00229 ];
U235_CAPT                 (idx, [1:   4]) = [  9.89768E+14 0.00277  3.34556E-02 0.00273 ];
U238_CAPT                 (idx, [1:   4]) = [  1.04233E+16 0.00102  3.52294E-01 0.00073 ];
PU239_CAPT                (idx, [1:   4]) = [  3.51189E+15 0.00148  1.18710E-01 0.00145 ];
PU240_CAPT                (idx, [1:   4]) = [  2.62920E+15 0.00189  8.88621E-02 0.00173 ];
PU241_CAPT                (idx, [1:   4]) = [  5.48526E+14 0.00390  1.85398E-02 0.00384 ];
XE135_CAPT                (idx, [1:   4]) = [  7.90459E+14 0.00325  2.67194E-02 0.00324 ];
SM149_CAPT                (idx, [1:   4]) = [  2.46386E+14 0.00593  8.32964E-03 0.00597 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001091 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.11821E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001091 5.00812E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3393447 3.39831E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1561063 1.56322E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46581 4.65935E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001091 5.00812E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -2.98023E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.71857E+16 2.4E-05  3.71857E+16 2.4E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36142E+16 4.9E-06  1.36142E+16 4.9E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.96005E+16 0.00045  2.26858E+16 0.00047  6.91477E+15 0.00121 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.32148E+16 0.00030  3.63000E+16 0.00029  6.91477E+15 0.00121 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.35302E+16 0.00052  4.35302E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.04982E+18 0.00050  5.41472E+17 0.00051  1.50835E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.05679E+14 0.00474 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.36205E+16 0.00031 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.54316E+18 0.00065 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.09615E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.09615E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.44287E+00 0.00060 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.71142E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.29073E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23164E+00 0.00039 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93909E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96752E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.62012E-01 0.00067 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.53979E-01 0.00067 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.73138E+00 2.8E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06332E+02 4.9E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.53989E-01 0.00069  8.49804E-01 0.00068  4.17509E-03 0.01155 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.53909E-01 0.00031 ];
COL_KEFF                  (idx, [1:   2]) = [  8.54366E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.53909E-01 0.00031 ];
ABS_KINF                  (idx, [1:   2]) = [  8.61937E-01 0.00030 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.69858E+01 0.00026 ];
IMP_ALF                   (idx, [1:   2]) = [  1.69894E+01 0.00011 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  8.43869E-07 0.00440 ];
IMP_EALF                  (idx, [1:   2]) = [  8.37454E-07 0.00181 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.98913E-01 0.00296 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.98733E-01 0.00121 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.93967E-03 0.00662  1.50414E-04 0.03874  1.08867E-03 0.01514  9.23021E-04 0.01646  2.54342E-03 0.01016  9.54748E-04 0.01628  2.79391E-04 0.02843 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.50961E-01 0.01478  9.42440E-03 0.02601  3.06515E-02 0.00046  1.11153E-01 0.00057  3.22766E-01 0.00038  1.26073E+00 0.00238  7.30485E+00 0.01933 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.96326E-03 0.01030  1.23515E-04 0.06123  9.22052E-04 0.02397  7.56829E-04 0.02423  2.12147E-03 0.01589  8.04801E-04 0.02558  2.34596E-04 0.04401 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.59505E-01 0.02248  1.25984E-02 0.00130  3.06513E-02 0.00068  1.11173E-01 0.00082  3.22790E-01 0.00059  1.25499E+00 0.00368  8.39140E+00 0.01161 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.07446E-05 0.00164  3.07346E-05 0.00164  3.30705E-05 0.01941 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.62492E-05 0.00147  2.62407E-05 0.00148  2.82258E-05 0.01934 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.88061E-03 0.01161  1.32226E-04 0.07001  8.65986E-04 0.02705  7.63156E-04 0.02850  2.09014E-03 0.01807  7.85774E-04 0.03010  2.43327E-04 0.05232 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.74482E-01 0.02938  1.25717E-02 0.00171  3.06483E-02 0.00093  1.11262E-01 0.00116  3.23263E-01 0.00077  1.26159E+00 0.00472  8.41020E+00 0.01542 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.02670E-05 0.00361  3.02529E-05 0.00362  3.00274E-05 0.04489 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.58429E-05 0.00358  2.58310E-05 0.00359  2.56334E-05 0.04499 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.16866E-03 0.03980  2.01112E-04 0.21205  1.04180E-03 0.09417  8.55901E-04 0.09403  2.04352E-03 0.05753  8.08566E-04 0.09876  2.17764E-04 0.16063 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.09872E-01 0.08420  1.26105E-02 0.00469  3.05570E-02 0.00191  1.11644E-01 0.00266  3.23848E-01 0.00201  1.26427E+00 0.01049  8.32570E+00 0.03645 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.10143E-03 0.03816  1.90276E-04 0.21035  1.02672E-03 0.09046  8.36644E-04 0.09338  2.03048E-03 0.05567  7.91304E-04 0.09591  2.26010E-04 0.15687 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.14089E-01 0.08366  1.26105E-02 0.00468  3.05521E-02 0.00190  1.11645E-01 0.00265  3.23941E-01 0.00200  1.26312E+00 0.01053  8.32777E+00 0.03641 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.71498E+02 0.03987 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.04931E-05 0.00097 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.60344E-05 0.00066 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.95991E-03 0.00685 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.62714E+02 0.00689 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.84572E-07 0.00080 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.65177E-06 0.00043  2.65162E-06 0.00043  2.67886E-06 0.00584 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.06327E-05 0.00098  4.06497E-05 0.00098  3.72813E-05 0.01169 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.26889E-01 0.00036  6.27376E-01 0.00036  5.57125E-01 0.01062 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.11934E+01 0.01500 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.53980E+01 0.00052  3.37044E+01 0.00059 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.02960E+04 0.00329  3.05445E+05 0.00162  6.10435E+05 0.00096  6.50766E+05 0.00060  5.98952E+05 0.00093  6.40990E+05 0.00071  4.34993E+05 0.00057  3.85086E+05 0.00062  2.94515E+05 0.00078  2.40687E+05 0.00091  2.07416E+05 0.00085  1.87256E+05 0.00078  1.72714E+05 0.00091  1.64337E+05 0.00081  1.59887E+05 0.00087  1.38306E+05 0.00098  1.36615E+05 0.00091  1.35597E+05 0.00108  1.33117E+05 0.00114  2.60156E+05 0.00068  2.51745E+05 0.00069  1.81596E+05 0.00091  1.17803E+05 0.00125  1.35857E+05 0.00090  1.29532E+05 0.00103  1.15245E+05 0.00107  1.86526E+05 0.00078  4.31060E+04 0.00162  5.32054E+04 0.00165  4.86286E+04 0.00144  2.85489E+04 0.00237  4.90507E+04 0.00183  3.24774E+04 0.00168  2.62652E+04 0.00129  4.39512E+03 0.00354  3.75121E+03 0.00313  3.25493E+03 0.00399  3.17988E+03 0.00380  3.23162E+03 0.00333  3.54049E+03 0.00350  4.23708E+03 0.00317  4.36551E+03 0.00392  8.74516E+03 0.00309  1.44817E+04 0.00233  1.88754E+04 0.00202  5.07181E+04 0.00142  5.33905E+04 0.00157  5.73661E+04 0.00118  3.73873E+04 0.00137  2.64155E+04 0.00170  1.96453E+04 0.00187  2.31241E+04 0.00193  4.54614E+04 0.00129  6.48152E+04 0.00169  1.30232E+05 0.00137  2.07636E+05 0.00143  3.17179E+05 0.00136  2.05374E+05 0.00157  1.47991E+05 0.00165  1.06977E+05 0.00152  9.64803E+04 0.00166  9.52627E+04 0.00187  7.97861E+04 0.00187  5.41152E+04 0.00197  5.00415E+04 0.00166  4.46710E+04 0.00216  3.78537E+04 0.00195  2.98656E+04 0.00189  2.00345E+04 0.00221  7.10478E+03 0.00280 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.62403E-01 0.00059 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.62366E+18 0.00066  4.26205E+17 0.00133 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39575E-01 0.00012  1.56524E+00 0.00046 ];
INF_CAPT                  (idx, [1:   4]) = [  8.32848E-03 0.00068  3.77278E-02 0.00080 ];
INF_ABS                   (idx, [1:   4]) = [  9.89852E-03 0.00055  6.36948E-02 0.00104 ];
INF_FISS                  (idx, [1:   4]) = [  1.57004E-03 0.00049  2.59670E-02 0.00140 ];
INF_NSF                   (idx, [1:   4]) = [  4.29337E-03 0.00047  7.09068E-02 0.00144 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.73456E+00 7.1E-05  2.73065E+00 6.8E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06169E+02 1.0E-05  2.06369E+02 1.3E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.55516E-08 0.00036  2.60007E-06 0.00020 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29680E-01 0.00012  1.50159E+00 0.00053 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44481E-01 0.00020  3.96903E-01 0.00061 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64763E-02 0.00031  9.34061E-02 0.00100 ];
INF_SCATT3                (idx, [1:   4]) = [  7.28432E-03 0.00235  2.80765E-02 0.00254 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03712E-02 0.00183 -8.94825E-03 0.00586 ];
INF_SCATT5                (idx, [1:   4]) = [  1.70103E-04 0.09154  6.67731E-03 0.00927 ];
INF_SCATT6                (idx, [1:   4]) = [  5.13072E-03 0.00318 -1.72942E-02 0.00276 ];
INF_SCATT7                (idx, [1:   4]) = [  7.70975E-04 0.01961  5.04395E-04 0.09351 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29724E-01 0.00012  1.50159E+00 0.00053 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44481E-01 0.00020  3.96903E-01 0.00061 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64765E-02 0.00031  9.34061E-02 0.00100 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.28431E-03 0.00235  2.80765E-02 0.00254 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03711E-02 0.00183 -8.94825E-03 0.00586 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.70037E-04 0.09164  6.67731E-03 0.00927 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.13069E-03 0.00318 -1.72942E-02 0.00276 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.70999E-04 0.01967  5.04395E-04 0.09351 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12359E-01 0.00030  1.01851E+00 0.00039 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56967E+00 0.00030  3.27278E-01 0.00039 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.85499E-03 0.00055  6.36948E-02 0.00104 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69802E-02 0.00024  6.47582E-02 0.00119 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12595E-01 0.00012  1.70848E-02 0.00042  1.10469E-03 0.00460  1.50048E+00 0.00053 ];
INF_S1                    (idx, [1:   8]) = [  2.39547E-01 0.00020  4.93404E-03 0.00101  4.76539E-04 0.00791  3.96426E-01 0.00062 ];
INF_S2                    (idx, [1:   8]) = [  9.80363E-02 0.00031 -1.55999E-03 0.00210  2.59971E-04 0.00757  9.31461E-02 0.00101 ];
INF_S3                    (idx, [1:   8]) = [  9.05440E-03 0.00183 -1.77009E-03 0.00169  9.13302E-05 0.01736  2.79851E-02 0.00257 ];
INF_S4                    (idx, [1:   8]) = [ -9.82441E-03 0.00188 -5.46822E-04 0.00461 -7.16848E-07 1.00000 -8.94754E-03 0.00580 ];
INF_S5                    (idx, [1:   8]) = [  1.26640E-04 0.12066  4.34630E-05 0.03776 -3.80612E-05 0.03803  6.71537E-03 0.00920 ];
INF_S6                    (idx, [1:   8]) = [  5.26258E-03 0.00305 -1.31862E-04 0.01324 -4.79541E-05 0.02414 -1.72463E-02 0.00278 ];
INF_S7                    (idx, [1:   8]) = [  9.37943E-04 0.01527 -1.66968E-04 0.01327 -4.42665E-05 0.02642  5.48662E-04 0.08533 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12639E-01 0.00012  1.70848E-02 0.00042  1.10469E-03 0.00460  1.50048E+00 0.00053 ];
INF_SP1                   (idx, [1:   8]) = [  2.39547E-01 0.00020  4.93404E-03 0.00101  4.76539E-04 0.00791  3.96426E-01 0.00062 ];
INF_SP2                   (idx, [1:   8]) = [  9.80365E-02 0.00031 -1.55999E-03 0.00210  2.59971E-04 0.00757  9.31461E-02 0.00101 ];
INF_SP3                   (idx, [1:   8]) = [  9.05439E-03 0.00184 -1.77009E-03 0.00169  9.13302E-05 0.01736  2.79851E-02 0.00257 ];
INF_SP4                   (idx, [1:   8]) = [ -9.82427E-03 0.00189 -5.46822E-04 0.00461 -7.16848E-07 1.00000 -8.94754E-03 0.00580 ];
INF_SP5                   (idx, [1:   8]) = [  1.26574E-04 0.12073  4.34630E-05 0.03776 -3.80612E-05 0.03803  6.71537E-03 0.00920 ];
INF_SP6                   (idx, [1:   8]) = [  5.26255E-03 0.00304 -1.31862E-04 0.01324 -4.79541E-05 0.02414 -1.72463E-02 0.00278 ];
INF_SP7                   (idx, [1:   8]) = [  9.37968E-04 0.01532 -1.66968E-04 0.01327 -4.42665E-05 0.02642  5.48662E-04 0.08533 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32027E-01 0.00058  1.19842E+00 0.00801 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33776E-01 0.00094  1.30624E+00 0.00944 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34046E-01 0.00077  1.29999E+00 0.01077 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28354E-01 0.00079  1.03375E+00 0.00739 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43663E+00 0.00058  2.78565E-01 0.00786 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42590E+00 0.00093  2.55719E-01 0.00922 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42424E+00 0.00077  2.57116E-01 0.01059 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45974E+00 0.00079  3.22859E-01 0.00714 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.96326E-03 0.01030  1.23515E-04 0.06123  9.22052E-04 0.02397  7.56829E-04 0.02423  2.12147E-03 0.01589  8.04801E-04 0.02558  2.34596E-04 0.04401 ];
LAMBDA                    (idx, [1:  14]) = [  7.59505E-01 0.02248  1.25984E-02 0.00130  3.06513E-02 0.00068  1.11173E-01 0.00082  3.22790E-01 0.00059  1.25499E+00 0.00368  8.39140E+00 0.01161 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0001' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:49:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:44:56 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292590 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.97751E-01  1.00470E+00  1.00133E+00  9.93849E-01  1.00237E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.1E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16339E-02 0.00112  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88366E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05450E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05879E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68584E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.52454E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.52361E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.48705E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.26505E-01 0.00115  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001194 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00024E+04 0.00097 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00024E+04 0.00097 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.72950E+02 ;
RUNNING_TIME              (idx, 1)        =  1.15095E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.86050E-01  4.86050E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.35833E-01  1.07833E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.13262E+02  3.13425E+00  2.63568E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.00175E+00  2.78167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  5.32833E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.15094E+02  1.27293E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97808 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99927E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81539E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.81404E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68727E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.64946E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.21093E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.55656E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.60308E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.53158E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  5.13006E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.10101E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.23524E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.00212E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  8.94813E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.09890E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.67289E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.43726E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.60137E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.64320E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.16423E+13 ;
CS137_ACTIVITY            (idx, 1)        =  4.01118E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.58059E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.19190E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.77885E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.37016E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.54903E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 18 ;
BURNUP                     (idx, [1:  2])  = [  3.00000E+01  3.00029E+01 ];
BURN_DAYS                 (idx, 1)        =  7.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.96792E-01 0.00104 ];
U233_FISS                 (idx, [1:   4]) = [  9.10411E+09 1.00000  6.71366E-07 1.00000 ];
U235_FISS                 (idx, [1:   4]) = [  3.61393E+15 0.00153  2.65874E-01 0.00131 ];
U238_FISS                 (idx, [1:   4]) = [  1.25186E+15 0.00285  9.20851E-02 0.00264 ];
PU239_FISS                (idx, [1:   4]) = [  6.82135E+15 0.00099  5.01875E-01 0.00080 ];
PU240_FISS                (idx, [1:   4]) = [  1.69447E+13 0.02338  1.24635E-03 0.02338 ];
PU241_FISS                (idx, [1:   4]) = [  1.85327E+15 0.00222  1.36349E-01 0.00212 ];
U235_CAPT                 (idx, [1:   4]) = [  8.12708E+14 0.00329  2.57622E-02 0.00329 ];
U238_CAPT                 (idx, [1:   4]) = [  1.08503E+16 0.00098  3.43910E-01 0.00073 ];
PU239_CAPT                (idx, [1:   4]) = [  3.72655E+15 0.00151  1.18124E-01 0.00145 ];
PU240_CAPT                (idx, [1:   4]) = [  2.99132E+15 0.00186  9.48094E-02 0.00170 ];
PU241_CAPT                (idx, [1:   4]) = [  6.64014E+14 0.00380  2.10491E-02 0.00381 ];
XE135_CAPT                (idx, [1:   4]) = [  7.99984E+14 0.00318  2.53590E-02 0.00319 ];
SM149_CAPT                (idx, [1:   4]) = [  2.54186E+14 0.00613  8.05685E-03 0.00611 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001194 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.24783E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001194 5.00825E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3462700 3.46769E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1491973 1.49404E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46521 4.65184E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001194 5.00825E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.47035E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.74928E+16 2.2E-05  3.74928E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35868E+16 4.7E-06  1.35868E+16 4.7E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.15714E+16 0.00043  2.44176E+16 0.00044  7.15384E+15 0.00119 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.51582E+16 0.00030  3.80044E+16 0.00028  7.15384E+15 0.00119 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.54903E+16 0.00051  4.54903E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.13669E+18 0.00048  5.65479E+17 0.00046  1.57121E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.23263E+14 0.00478 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.55815E+16 0.00031 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.60533E+18 0.00064 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.09038E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.09038E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.40894E+00 0.00063 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.71297E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.22348E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23081E+00 0.00042 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93935E-01 3.7E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96741E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.32331E-01 0.00065 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.24588E-01 0.00066 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.75950E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06748E+02 4.7E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.24690E-01 0.00066  8.20613E-01 0.00066  3.97474E-03 0.01205 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.23944E-01 0.00031 ];
COL_KEFF                  (idx, [1:   2]) = [  8.24299E-01 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.23944E-01 0.00031 ];
ABS_KINF                  (idx, [1:   2]) = [  8.31678E-01 0.00030 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.69214E+01 0.00027 ];
IMP_ALF                   (idx, [1:   2]) = [  1.69237E+01 0.00010 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  9.00463E-07 0.00462 ];
IMP_EALF                  (idx, [1:   2]) = [  8.94313E-07 0.00177 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.12255E-01 0.00297 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.12095E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.04174E-03 0.00680  1.45088E-04 0.04081  1.12735E-03 0.01439  9.43678E-04 0.01619  2.53391E-03 0.01003  1.00426E-03 0.01534  2.87445E-04 0.02873 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.48827E-01 0.01466  8.81412E-03 0.02948  3.05436E-02 0.00042  1.11414E-01 0.00061  3.23285E-01 0.00040  1.24589E+00 0.00235  7.36632E+00 0.01811 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.84047E-03 0.01003  1.12723E-04 0.06901  9.14024E-04 0.02202  7.59392E-04 0.02579  2.00359E-03 0.01535  8.10409E-04 0.02433  2.40333E-04 0.04509 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.65111E-01 0.02391  1.26372E-02 0.00145  3.05476E-02 0.00063  1.11474E-01 0.00088  3.23023E-01 0.00059  1.24692E+00 0.00352  8.09330E+00 0.01367 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.17727E-05 0.00151  3.17552E-05 0.00152  3.52514E-05 0.01885 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.61975E-05 0.00140  2.61831E-05 0.00140  2.90701E-05 0.01888 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.83579E-03 0.01214  1.16673E-04 0.07683  8.84693E-04 0.02702  7.50882E-04 0.02986  2.03165E-03 0.01930  8.05267E-04 0.02915  2.46633E-04 0.05285 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.77798E-01 0.02983  1.26214E-02 0.00225  3.05215E-02 0.00086  1.11641E-01 0.00119  3.23243E-01 0.00084  1.24228E+00 0.00506  7.97837E+00 0.02012 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.11732E-05 0.00390  3.11571E-05 0.00393  2.96284E-05 0.04541 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.57014E-05 0.00382  2.56881E-05 0.00385  2.44061E-05 0.04527 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.88962E-03 0.04080  1.33038E-04 0.33544  1.00740E-03 0.10155  7.77694E-04 0.10621  1.99651E-03 0.06146  7.55066E-04 0.09877  2.19913E-04 0.19148 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.42855E-01 0.09056  1.26785E-02 0.00655  3.04931E-02 0.00195  1.11738E-01 0.00291  3.23295E-01 0.00208  1.23637E+00 0.01228  8.24160E+00 0.04578 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.86224E-03 0.04024  1.26946E-04 0.32101  9.77432E-04 0.09928  7.84535E-04 0.10460  1.98304E-03 0.06109  7.57226E-04 0.09920  2.33061E-04 0.18340 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.54928E-01 0.09006  1.26778E-02 0.00654  3.04900E-02 0.00195  1.11723E-01 0.00290  3.23294E-01 0.00207  1.23804E+00 0.01216  8.23525E+00 0.04590 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.57946E+02 0.04134 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.14936E-05 0.00101 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.59663E-05 0.00073 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.84648E-03 0.00840 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.53984E+02 0.00846 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.80344E-07 0.00075 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.63069E-06 0.00043  2.63068E-06 0.00044  2.63151E-06 0.00618 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.06678E-05 0.00092  4.06834E-05 0.00092  3.76543E-05 0.01174 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.20283E-01 0.00035  6.20872E-01 0.00035  5.35628E-01 0.01060 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.09318E+01 0.01439 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.52361E+01 0.00048  3.35674E+01 0.00058 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.08262E+04 0.00244  3.05492E+05 0.00121  6.11479E+05 0.00087  6.49307E+05 0.00075  5.98348E+05 0.00082  6.40997E+05 0.00070  4.35486E+05 0.00071  3.85100E+05 0.00081  2.95149E+05 0.00065  2.40972E+05 0.00078  2.07832E+05 0.00071  1.87426E+05 0.00087  1.72809E+05 0.00085  1.64417E+05 0.00077  1.60333E+05 0.00100  1.38463E+05 0.00102  1.36803E+05 0.00105  1.35598E+05 0.00105  1.33425E+05 0.00081  2.59776E+05 0.00060  2.51846E+05 0.00061  1.81600E+05 0.00079  1.18157E+05 0.00097  1.35742E+05 0.00097  1.29638E+05 0.00089  1.14944E+05 0.00108  1.85663E+05 0.00075  4.30748E+04 0.00191  5.28531E+04 0.00127  4.82449E+04 0.00108  2.83845E+04 0.00241  4.88890E+04 0.00204  3.20260E+04 0.00183  2.57764E+04 0.00209  4.28379E+03 0.00452  3.62537E+03 0.00388  3.15561E+03 0.00404  3.05073E+03 0.00453  3.11221E+03 0.00312  3.37436E+03 0.00363  4.10275E+03 0.00358  4.26893E+03 0.00370  8.52262E+03 0.00288  1.42681E+04 0.00200  1.86719E+04 0.00204  5.00414E+04 0.00179  5.27081E+04 0.00149  5.65153E+04 0.00152  3.68454E+04 0.00159  2.60223E+04 0.00150  1.92334E+04 0.00142  2.28103E+04 0.00245  4.46987E+04 0.00150  6.37897E+04 0.00139  1.28464E+05 0.00123  2.05166E+05 0.00119  3.13865E+05 0.00128  2.03312E+05 0.00127  1.46648E+05 0.00141  1.05910E+05 0.00147  9.56251E+04 0.00147  9.44584E+04 0.00158  7.90674E+04 0.00150  5.36380E+04 0.00195  4.96480E+04 0.00183  4.43351E+04 0.00178  3.75465E+04 0.00176  2.96864E+04 0.00192  1.98419E+04 0.00224  7.05557E+03 0.00241 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.32040E-01 0.00055 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.69619E+18 0.00048  4.40550E+17 0.00135 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39886E-01 0.00011  1.56534E+00 0.00047 ];
INF_CAPT                  (idx, [1:   4]) = [  8.58454E-03 0.00054  3.86165E-02 0.00083 ];
INF_ABS                   (idx, [1:   4]) = [  1.00874E-02 0.00047  6.36758E-02 0.00103 ];
INF_FISS                  (idx, [1:   4]) = [  1.50283E-03 0.00048  2.50593E-02 0.00135 ];
INF_NSF                   (idx, [1:   4]) = [  4.14392E-03 0.00049  6.91632E-02 0.00140 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.75741E+00 6.4E-05  2.75998E+00 6.5E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06488E+02 9.5E-06  2.06808E+02 1.3E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.51461E-08 0.00042  2.60258E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29808E-01 0.00012  1.50168E+00 0.00053 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44304E-01 0.00024  3.96700E-01 0.00065 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64249E-02 0.00029  9.34014E-02 0.00098 ];
INF_SCATT3                (idx, [1:   4]) = [  7.29949E-03 0.00314  2.79178E-02 0.00275 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03545E-02 0.00241 -9.06584E-03 0.00828 ];
INF_SCATT5                (idx, [1:   4]) = [  1.95168E-04 0.09358  6.66833E-03 0.01020 ];
INF_SCATT6                (idx, [1:   4]) = [  5.13510E-03 0.00283 -1.73187E-02 0.00259 ];
INF_SCATT7                (idx, [1:   4]) = [  7.64260E-04 0.01870  4.51935E-04 0.08251 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29852E-01 0.00012  1.50168E+00 0.00053 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44305E-01 0.00024  3.96700E-01 0.00065 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64250E-02 0.00029  9.34014E-02 0.00098 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.29946E-03 0.00314  2.79178E-02 0.00275 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03548E-02 0.00241 -9.06584E-03 0.00828 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.94905E-04 0.09363  6.66833E-03 0.01020 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.13517E-03 0.00282 -1.73187E-02 0.00259 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.63975E-04 0.01872  4.51935E-04 0.08251 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12513E-01 0.00030  1.01958E+00 0.00037 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56854E+00 0.00030  3.26934E-01 0.00037 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.00431E-02 0.00046  6.36758E-02 0.00103 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69928E-02 0.00026  6.47907E-02 0.00103 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12893E-01 0.00011  1.69144E-02 0.00047  1.12423E-03 0.00337  1.50055E+00 0.00053 ];
INF_S1                    (idx, [1:   8]) = [  2.39431E-01 0.00023  4.87335E-03 0.00106  4.85604E-04 0.00739  3.96215E-01 0.00066 ];
INF_S2                    (idx, [1:   8]) = [  9.79797E-02 0.00029 -1.55487E-03 0.00228  2.66283E-04 0.00861  9.31351E-02 0.00098 ];
INF_S3                    (idx, [1:   8]) = [  9.05466E-03 0.00260 -1.75517E-03 0.00166  9.55246E-05 0.02255  2.78223E-02 0.00277 ];
INF_S4                    (idx, [1:   8]) = [ -9.81410E-03 0.00253 -5.40403E-04 0.00420  2.38331E-06 0.99152 -9.06823E-03 0.00833 ];
INF_S5                    (idx, [1:   8]) = [  1.45743E-04 0.12700  4.94250E-05 0.04641 -3.71834E-05 0.04115  6.70551E-03 0.01017 ];
INF_S6                    (idx, [1:   8]) = [  5.26161E-03 0.00264 -1.26515E-04 0.02062 -4.75900E-05 0.02903 -1.72711E-02 0.00259 ];
INF_S7                    (idx, [1:   8]) = [  9.26890E-04 0.01597 -1.62630E-04 0.01458 -4.44264E-05 0.03329  4.96362E-04 0.07492 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12938E-01 0.00011  1.69144E-02 0.00047  1.12423E-03 0.00337  1.50055E+00 0.00053 ];
INF_SP1                   (idx, [1:   8]) = [  2.39432E-01 0.00022  4.87335E-03 0.00106  4.85604E-04 0.00739  3.96215E-01 0.00066 ];
INF_SP2                   (idx, [1:   8]) = [  9.79798E-02 0.00029 -1.55487E-03 0.00228  2.66283E-04 0.00861  9.31351E-02 0.00098 ];
INF_SP3                   (idx, [1:   8]) = [  9.05463E-03 0.00260 -1.75517E-03 0.00166  9.55246E-05 0.02255  2.78223E-02 0.00277 ];
INF_SP4                   (idx, [1:   8]) = [ -9.81443E-03 0.00253 -5.40403E-04 0.00420  2.38331E-06 0.99152 -9.06823E-03 0.00833 ];
INF_SP5                   (idx, [1:   8]) = [  1.45480E-04 0.12712  4.94250E-05 0.04641 -3.71834E-05 0.04115  6.70551E-03 0.01017 ];
INF_SP6                   (idx, [1:   8]) = [  5.26169E-03 0.00263 -1.26515E-04 0.02062 -4.75900E-05 0.02903 -1.72711E-02 0.00259 ];
INF_SP7                   (idx, [1:   8]) = [  9.26604E-04 0.01599 -1.62630E-04 0.01458 -4.44264E-05 0.03329  4.96362E-04 0.07492 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32122E-01 0.00064  1.21624E+00 0.00686 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33995E-01 0.00078  1.32537E+00 0.00952 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33842E-01 0.00089  1.31938E+00 0.00716 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28617E-01 0.00101  1.04967E+00 0.00869 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43604E+00 0.00064  2.74377E-01 0.00683 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42455E+00 0.00078  2.52051E-01 0.00952 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42549E+00 0.00089  2.52957E-01 0.00720 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45808E+00 0.00101  3.18124E-01 0.00851 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.84047E-03 0.01003  1.12723E-04 0.06901  9.14024E-04 0.02202  7.59392E-04 0.02579  2.00359E-03 0.01535  8.10409E-04 0.02433  2.40333E-04 0.04509 ];
LAMBDA                    (idx, [1:  14]) = [  7.65111E-01 0.02391  1.26372E-02 0.00145  3.05476E-02 0.00063  1.11474E-01 0.00088  3.23023E-01 0.00059  1.24692E+00 0.00352  8.09330E+00 0.01367 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0001' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:49:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:50:29 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292590 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.98281E-01  1.00405E+00  1.00128E+00  9.94986E-01  1.00140E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16906E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88309E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04945E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05375E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68986E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.50802E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.50711E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.47445E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.27179E-01 0.00111  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001195 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00024E+04 0.00111 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00024E+04 0.00111 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  6.00707E+02 ;
RUNNING_TIME              (idx, 1)        =  1.20652E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.86050E-01  4.86050E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.57200E-01  1.04167E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.18745E+02  3.06517E+00  2.41803E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.05420E+00  2.56167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  5.57833E-02  8.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.20652E+02  1.26276E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97883 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00109E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81694E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.94729E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.69202E+04 ;
TOT_SF_RATE               (idx, 1)        =  4.45272E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.30750E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.63038E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.63975E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.52894E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  6.64088E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.26497E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.70145E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.07021E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  9.39421E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.19475E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.98609E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.47532E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.64202E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.69143E+14 ;
CS134_ACTIVITY            (idx, 1)        =  8.02446E+13 ;
CS137_ACTIVITY            (idx, 1)        =  4.66670E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.69835E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.16948E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.50538E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.39924E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.72922E+12 0.00055  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 19 ;
BURNUP                     (idx, [1:  2])  = [  3.50000E+01  3.50034E+01 ];
BURN_DAYS                 (idx, 1)        =  8.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  8.34352E-01 0.00111 ];
U235_FISS                 (idx, [1:   4]) = [  2.92130E+15 0.00185  2.15154E-01 0.00170 ];
U238_FISS                 (idx, [1:   4]) = [  1.29357E+15 0.00281  9.52593E-02 0.00262 ];
PU239_FISS                (idx, [1:   4]) = [  7.15926E+15 0.00115  5.27258E-01 0.00079 ];
PU240_FISS                (idx, [1:   4]) = [  1.90263E+13 0.02226  1.40048E-03 0.02217 ];
PU241_FISS                (idx, [1:   4]) = [  2.13833E+15 0.00200  1.57486E-01 0.00186 ];
U235_CAPT                 (idx, [1:   4]) = [  6.52234E+14 0.00389  1.95514E-02 0.00386 ];
U238_CAPT                 (idx, [1:   4]) = [  1.12325E+16 0.00110  3.36679E-01 0.00083 ];
PU239_CAPT                (idx, [1:   4]) = [  3.91181E+15 0.00159  1.17264E-01 0.00155 ];
PU240_CAPT                (idx, [1:   4]) = [  3.29495E+15 0.00173  9.87632E-02 0.00158 ];
PU241_CAPT                (idx, [1:   4]) = [  7.65907E+14 0.00349  2.29590E-02 0.00346 ];
XE135_CAPT                (idx, [1:   4]) = [  8.09603E+14 0.00348  2.42700E-02 0.00348 ];
SM149_CAPT                (idx, [1:   4]) = [  2.61299E+14 0.00585  7.83260E-03 0.00583 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001195 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.24690E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001195 5.00825E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3522097 3.52715E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1433625 1.43562E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45473 4.54800E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001195 5.00825E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -9.31323E-09 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.77514E+16 2.4E-05  3.77514E+16 2.4E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35637E+16 5.2E-06  1.35637E+16 5.2E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.33676E+16 0.00045  2.60095E+16 0.00047  7.35819E+15 0.00126 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.69314E+16 0.00032  3.95732E+16 0.00031  7.35819E+15 0.00126 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.72922E+16 0.00055  4.72922E+16 0.00055  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.21630E+18 0.00051  5.87641E+17 0.00050  1.62866E+18 0.00057 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.30252E+14 0.00513 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.73616E+16 0.00033 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.66104E+18 0.00068 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.08462E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.08462E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.37794E+00 0.00068 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.71623E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.16067E-01 0.00039 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23134E+00 0.00041 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94076E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96810E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.06493E-01 0.00073 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  7.99158E-01 0.00073 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.78325E+00 2.9E-05 ];
FISSE                     (idx, [1:   2]) = [  2.07099E+02 5.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  7.99101E-01 0.00075  7.95459E-01 0.00074  3.69882E-03 0.01224 ];
IMP_KEFF                  (idx, [1:   2]) = [  7.98448E-01 0.00033 ];
COL_KEFF                  (idx, [1:   2]) = [  7.98376E-01 0.00055 ];
ABS_KEFF                  (idx, [1:   2]) = [  7.98448E-01 0.00033 ];
ABS_KINF                  (idx, [1:   2]) = [  8.05778E-01 0.00032 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.68632E+01 0.00028 ];
IMP_ALF                   (idx, [1:   2]) = [  1.68611E+01 0.00011 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  9.54514E-07 0.00464 ];
IMP_EALF                  (idx, [1:   2]) = [  9.52192E-07 0.00186 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.23536E-01 0.00288 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.24402E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.07042E-03 0.00678  1.38944E-04 0.04407  1.12420E-03 0.01499  9.30788E-04 0.01722  2.56963E-03 0.00973  1.02622E-03 0.01636  2.80652E-04 0.03094 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.33967E-01 0.01539  8.27273E-03 0.03260  3.04242E-02 0.00039  1.11644E-01 0.00067  3.23350E-01 0.00042  1.23656E+00 0.00260  6.93579E+00 0.02111 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.68487E-03 0.01039  9.74850E-05 0.06828  8.83739E-04 0.02378  7.08853E-04 0.02576  1.98491E-03 0.01555  7.95685E-04 0.02598  2.14203E-04 0.04740 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.34061E-01 0.02410  1.26524E-02 0.00164  3.04089E-02 0.00057  1.11570E-01 0.00097  3.23194E-01 0.00064  1.23502E+00 0.00395  7.98983E+00 0.01488 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.27126E-05 0.00166  3.27025E-05 0.00166  3.46816E-05 0.01938 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.61327E-05 0.00144  2.61247E-05 0.00145  2.77067E-05 0.01937 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.63229E-03 0.01222  1.01490E-04 0.08418  8.83226E-04 0.02861  6.92944E-04 0.03019  1.94124E-03 0.01841  7.93998E-04 0.02997  2.19389E-04 0.05804 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.48769E-01 0.03139  1.25915E-02 0.00224  3.04426E-02 0.00083  1.11613E-01 0.00136  3.23584E-01 0.00084  1.23898E+00 0.00526  8.03025E+00 0.02177 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.20296E-05 0.00379  3.20235E-05 0.00381  2.84515E-05 0.05157 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.55900E-05 0.00377  2.55852E-05 0.00379  2.27482E-05 0.05159 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.68446E-03 0.04111  1.37400E-04 0.26506  9.23889E-04 0.09283  6.40665E-04 0.10499  1.90281E-03 0.06620  8.41314E-04 0.09915  2.38379E-04 0.17713 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.15876E-01 0.09948  1.25325E-02 0.00370  3.04448E-02 0.00199  1.12055E-01 0.00318  3.23816E-01 0.00228  1.24513E+00 0.01211  8.31543E+00 0.05220 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.69851E-03 0.04051  1.36212E-04 0.26506  9.18785E-04 0.09104  6.29014E-04 0.10201  1.92521E-03 0.06568  8.49780E-04 0.09669  2.39510E-04 0.17564 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.18634E-01 0.09843  1.25325E-02 0.00370  3.04423E-02 0.00199  1.12038E-01 0.00317  3.23769E-01 0.00228  1.24443E+00 0.01214  8.31311E+00 0.05218 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.47099E+02 0.04150 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.24111E-05 0.00099 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.58926E-05 0.00064 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.57714E-03 0.00817 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.41317E+02 0.00826 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.75642E-07 0.00080 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.61407E-06 0.00046  2.61378E-06 0.00046  2.66749E-06 0.00599 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.06315E-05 0.00096  4.06491E-05 0.00096  3.71051E-05 0.01153 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.14133E-01 0.00039  6.14828E-01 0.00039  5.11935E-01 0.01062 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.10682E+01 0.01481 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.50711E+01 0.00053  3.34636E+01 0.00061 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.16408E+04 0.00245  3.06839E+05 0.00129  6.10657E+05 0.00085  6.50285E+05 0.00092  5.97619E+05 0.00065  6.42076E+05 0.00071  4.35571E+05 0.00078  3.86038E+05 0.00068  2.95110E+05 0.00050  2.40991E+05 0.00081  2.07835E+05 0.00060  1.87569E+05 0.00090  1.73066E+05 0.00089  1.64632E+05 0.00079  1.60344E+05 0.00095  1.38689E+05 0.00076  1.36977E+05 0.00095  1.35698E+05 0.00089  1.33454E+05 0.00081  2.60562E+05 0.00061  2.52141E+05 0.00076  1.81551E+05 0.00081  1.17980E+05 0.00104  1.35615E+05 0.00079  1.29727E+05 0.00094  1.14860E+05 0.00100  1.84801E+05 0.00067  4.29075E+04 0.00204  5.23572E+04 0.00174  4.78836E+04 0.00179  2.82852E+04 0.00177  4.85667E+04 0.00140  3.15363E+04 0.00164  2.53031E+04 0.00205  4.18550E+03 0.00243  3.50595E+03 0.00414  3.07599E+03 0.00481  2.97849E+03 0.00362  3.01654E+03 0.00304  3.29851E+03 0.00382  3.97014E+03 0.00314  4.15492E+03 0.00222  8.40494E+03 0.00282  1.40153E+04 0.00172  1.83555E+04 0.00208  4.94776E+04 0.00164  5.19421E+04 0.00136  5.59845E+04 0.00102  3.63448E+04 0.00121  2.56700E+04 0.00166  1.89715E+04 0.00180  2.23531E+04 0.00139  4.40446E+04 0.00175  6.29450E+04 0.00197  1.26977E+05 0.00185  2.02797E+05 0.00190  3.10249E+05 0.00197  2.01211E+05 0.00217  1.45160E+05 0.00205  1.04923E+05 0.00196  9.47010E+04 0.00218  9.34338E+04 0.00236  7.82986E+04 0.00231  5.30736E+04 0.00237  4.92572E+04 0.00234  4.39059E+04 0.00210  3.71009E+04 0.00230  2.94224E+04 0.00255  1.96557E+04 0.00296  7.00891E+03 0.00304 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.05705E-01 0.00061 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.76343E+18 0.00054  4.52928E+17 0.00202 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.40079E-01 0.00013  1.56467E+00 0.00060 ];
INF_CAPT                  (idx, [1:   4]) = [  8.80327E-03 0.00062  3.94027E-02 0.00122 ];
INF_ABS                   (idx, [1:   4]) = [  1.02522E-02 0.00055  6.37150E-02 0.00154 ];
INF_FISS                  (idx, [1:   4]) = [  1.44891E-03 0.00043  2.43123E-02 0.00206 ];
INF_NSF                   (idx, [1:   4]) = [  4.02355E-03 0.00045  6.77029E-02 0.00212 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.77695E+00 8.5E-05  2.78472E+00 7.6E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06761E+02 9.7E-06  2.07178E+02 1.5E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.47240E-08 0.00035  2.60397E-06 0.00021 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29832E-01 0.00014  1.50093E+00 0.00069 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44114E-01 0.00028  3.96277E-01 0.00078 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63649E-02 0.00031  9.31442E-02 0.00103 ];
INF_SCATT3                (idx, [1:   4]) = [  7.27288E-03 0.00276  2.79002E-02 0.00214 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03400E-02 0.00192 -9.09040E-03 0.00712 ];
INF_SCATT5                (idx, [1:   4]) = [  1.99432E-04 0.07368  6.72105E-03 0.00796 ];
INF_SCATT6                (idx, [1:   4]) = [  5.15540E-03 0.00220 -1.72343E-02 0.00274 ];
INF_SCATT7                (idx, [1:   4]) = [  7.79915E-04 0.01911  5.09435E-04 0.08866 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29876E-01 0.00014  1.50093E+00 0.00069 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44114E-01 0.00028  3.96277E-01 0.00078 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63649E-02 0.00031  9.31442E-02 0.00103 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.27287E-03 0.00276  2.79002E-02 0.00214 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03400E-02 0.00193 -9.09040E-03 0.00712 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.99138E-04 0.07379  6.72105E-03 0.00796 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.15543E-03 0.00220 -1.72343E-02 0.00274 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.79914E-04 0.01914  5.09435E-04 0.08866 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12720E-01 0.00029  1.02010E+00 0.00050 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56701E+00 0.00028  3.26766E-01 0.00050 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.02079E-02 0.00055  6.37150E-02 0.00154 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69880E-02 0.00019  6.48529E-02 0.00163 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.13091E-01 0.00014  1.67409E-02 0.00050  1.10706E-03 0.00477  1.49982E+00 0.00069 ];
INF_S1                    (idx, [1:   8]) = [  2.39292E-01 0.00027  4.82183E-03 0.00087  4.79772E-04 0.00652  3.95797E-01 0.00078 ];
INF_S2                    (idx, [1:   8]) = [  9.79115E-02 0.00030 -1.54660E-03 0.00208  2.60948E-04 0.00828  9.28832E-02 0.00103 ];
INF_S3                    (idx, [1:   8]) = [  9.01113E-03 0.00219 -1.73825E-03 0.00147  9.30658E-05 0.02334  2.78071E-02 0.00214 ];
INF_S4                    (idx, [1:   8]) = [ -9.81103E-03 0.00204 -5.28944E-04 0.00507  1.36612E-06 0.95983 -9.09176E-03 0.00718 ];
INF_S5                    (idx, [1:   8]) = [  1.49552E-04 0.10298  4.98807E-05 0.04494 -3.71993E-05 0.03368  6.75825E-03 0.00790 ];
INF_S6                    (idx, [1:   8]) = [  5.28297E-03 0.00209 -1.27573E-04 0.01469 -4.76750E-05 0.02900 -1.71866E-02 0.00274 ];
INF_S7                    (idx, [1:   8]) = [  9.47120E-04 0.01476 -1.67205E-04 0.01306 -4.35980E-05 0.03271  5.53033E-04 0.08239 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.13136E-01 0.00014  1.67409E-02 0.00050  1.10706E-03 0.00477  1.49982E+00 0.00069 ];
INF_SP1                   (idx, [1:   8]) = [  2.39293E-01 0.00027  4.82183E-03 0.00087  4.79772E-04 0.00652  3.95797E-01 0.00078 ];
INF_SP2                   (idx, [1:   8]) = [  9.79115E-02 0.00030 -1.54660E-03 0.00208  2.60948E-04 0.00828  9.28832E-02 0.00103 ];
INF_SP3                   (idx, [1:   8]) = [  9.01112E-03 0.00219 -1.73825E-03 0.00147  9.30658E-05 0.02334  2.78071E-02 0.00214 ];
INF_SP4                   (idx, [1:   8]) = [ -9.81103E-03 0.00204 -5.28944E-04 0.00507  1.36612E-06 0.95983 -9.09176E-03 0.00718 ];
INF_SP5                   (idx, [1:   8]) = [  1.49257E-04 0.10322  4.98807E-05 0.04494 -3.71993E-05 0.03368  6.75825E-03 0.00790 ];
INF_SP6                   (idx, [1:   8]) = [  5.28300E-03 0.00209 -1.27573E-04 0.01469 -4.76750E-05 0.02900 -1.71866E-02 0.00274 ];
INF_SP7                   (idx, [1:   8]) = [  9.47119E-04 0.01480 -1.67205E-04 0.01306 -4.35980E-05 0.03271  5.53033E-04 0.08239 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32111E-01 0.00046  1.22151E+00 0.00837 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33637E-01 0.00101  1.33588E+00 0.01049 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33863E-01 0.00081  1.32000E+00 0.01107 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28912E-01 0.00086  1.05439E+00 0.00782 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43610E+00 0.00046  2.73337E-01 0.00820 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42675E+00 0.00101  2.50149E-01 0.00996 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42536E+00 0.00081  2.53267E-01 0.01106 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45619E+00 0.00086  3.16594E-01 0.00766 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.68487E-03 0.01039  9.74850E-05 0.06828  8.83739E-04 0.02378  7.08853E-04 0.02576  1.98491E-03 0.01555  7.95685E-04 0.02598  2.14203E-04 0.04740 ];
LAMBDA                    (idx, [1:  14]) = [  7.34061E-01 0.02410  1.26524E-02 0.00164  3.04089E-02 0.00057  1.11570E-01 0.00097  3.23194E-01 0.00064  1.23502E+00 0.00395  7.98983E+00 0.01488 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0001' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:49:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:56:06 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292590 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.98357E-01  1.00444E+00  1.00110E+00  9.94264E-01  1.00183E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.17652E-02 0.00106  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88235E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.03913E-01 0.00013  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04346E-01 0.00013  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69223E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.48426E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.48337E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.47230E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.27603E-01 0.00109  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001450 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00029E+04 0.00100 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00029E+04 0.00100 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  6.28678E+02 ;
RUNNING_TIME              (idx, 1)        =  1.26253E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.86050E-01  4.86050E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.77500E-01  1.00833E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.24276E+02  3.17358E+00  2.35722E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.10292E+00  2.35000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  5.80667E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.26252E+02  1.26252E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97953 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99832E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81835E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  1.00785E+17 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.70033E+04 ;
TOT_SF_RATE               (idx, 1)        =  6.87028E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.39583E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.69888E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.68259E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.53040E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  8.38436E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.42935E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  7.40509E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.13922E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  9.79272E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.29013E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.27601E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.51324E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.67980E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.73808E+14 ;
CS134_ACTIVITY            (idx, 1)        =  9.99302E+13 ;
CS137_ACTIVITY            (idx, 1)        =  5.31804E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.81194E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.15234E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.27005E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.42641E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.88351E+12 0.00055  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 20 ;
BURNUP                     (idx, [1:  2])  = [  4.00000E+01  4.00040E+01 ];
BURN_DAYS                 (idx, 1)        =  1.00000E+03 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  8.67474E-01 0.00112 ];
U233_FISS                 (idx, [1:   4]) = [  9.60853E+09 1.00000  7.08466E-07 1.00000 ];
U235_FISS                 (idx, [1:   4]) = [  2.33501E+15 0.00221  1.72303E-01 0.00207 ];
U238_FISS                 (idx, [1:   4]) = [  1.33008E+15 0.00279  9.81405E-02 0.00262 ];
PU239_FISS                (idx, [1:   4]) = [  7.41685E+15 0.00105  5.47325E-01 0.00081 ];
PU240_FISS                (idx, [1:   4]) = [  2.10820E+13 0.02133  1.55539E-03 0.02129 ];
PU241_FISS                (idx, [1:   4]) = [  2.38669E+15 0.00206  1.76122E-01 0.00193 ];
U235_CAPT                 (idx, [1:   4]) = [  5.20994E+14 0.00451  1.49137E-02 0.00443 ];
U238_CAPT                 (idx, [1:   4]) = [  1.15516E+16 0.00108  3.30679E-01 0.00078 ];
PU239_CAPT                (idx, [1:   4]) = [  4.05574E+15 0.00148  1.16119E-01 0.00152 ];
PU240_CAPT                (idx, [1:   4]) = [  3.55192E+15 0.00172  1.01677E-01 0.00154 ];
PU241_CAPT                (idx, [1:   4]) = [  8.53088E+14 0.00335  2.44253E-02 0.00338 ];
XE135_CAPT                (idx, [1:   4]) = [  8.15666E+14 0.00324  2.33530E-02 0.00325 ];
SM149_CAPT                (idx, [1:   4]) = [  2.71034E+14 0.00609  7.76044E-03 0.00613 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001450 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.48552E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001450 5.00849E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3571370 3.57647E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1385614 1.38755E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44466 4.44612E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001450 5.00849E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -8.38190E-09 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.79746E+16 2.2E-05  3.79746E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35441E+16 4.6E-06  1.35441E+16 4.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.49322E+16 0.00046  2.74539E+16 0.00046  7.47828E+15 0.00134 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.84763E+16 0.00033  4.09980E+16 0.00031  7.47828E+15 0.00134 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.88351E+16 0.00055  4.88351E+16 0.00055  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.28236E+18 0.00055  6.07052E+17 0.00050  1.67531E+18 0.00062 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.34371E+14 0.00525 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.89106E+16 0.00034 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.70376E+18 0.00073 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.07886E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.07886E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.35125E+00 0.00066 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.72936E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.10207E-01 0.00037 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23192E+00 0.00041 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94142E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96948E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  7.85046E-01 0.00070 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  7.78066E-01 0.00070 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.80377E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.07400E+02 4.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  7.78131E-01 0.00070  7.74474E-01 0.00071  3.59132E-03 0.01263 ];
IMP_KEFF                  (idx, [1:   2]) = [  7.77760E-01 0.00035 ];
COL_KEFF                  (idx, [1:   2]) = [  7.77727E-01 0.00055 ];
ABS_KEFF                  (idx, [1:   2]) = [  7.77760E-01 0.00035 ];
ABS_KINF                  (idx, [1:   2]) = [  7.84738E-01 0.00034 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.68106E+01 0.00028 ];
IMP_ALF                   (idx, [1:   2]) = [  1.68073E+01 0.00011 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  1.00609E-06 0.00466 ];
IMP_EALF                  (idx, [1:   2]) = [  1.00475E-06 0.00183 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.34340E-01 0.00282 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.34877E-01 0.00118 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.14086E-03 0.00676  1.48604E-04 0.04043  1.19532E-03 0.01498  9.14621E-04 0.01616  2.52434E-03 0.01008  1.07652E-03 0.01491  2.81458E-04 0.02909 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.28098E-01 0.01530  8.95366E-03 0.02893  3.03614E-02 0.00037  1.12007E-01 0.00066  3.23614E-01 0.00042  1.22186E+00 0.00279  6.99457E+00 0.02028 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.60520E-03 0.01043  1.08703E-04 0.06740  8.98861E-04 0.02383  6.97499E-04 0.02675  1.87754E-03 0.01624  8.08906E-04 0.02459  2.13690E-04 0.04613 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.34378E-01 0.02393  1.26869E-02 0.00168  3.03364E-02 0.00052  1.11921E-01 0.00096  3.23584E-01 0.00063  1.22084E+00 0.00395  7.74581E+00 0.01627 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.34396E-05 0.00162  3.34210E-05 0.00163  3.72637E-05 0.02133 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.60144E-05 0.00148  2.60000E-05 0.00149  2.89781E-05 0.02124 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.62151E-03 0.01264  1.08266E-04 0.08204  8.81524E-04 0.02851  6.68200E-04 0.03275  1.90364E-03 0.02001  8.42370E-04 0.02835  2.17506E-04 0.05645 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.46124E-01 0.03057  1.26318E-02 0.00243  3.03317E-02 0.00074  1.11905E-01 0.00141  3.23958E-01 0.00092  1.21542E+00 0.00586  7.76238E+00 0.02478 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.27337E-05 0.00388  3.27244E-05 0.00390  2.95370E-05 0.04881 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.54637E-05 0.00380  2.54564E-05 0.00381  2.29876E-05 0.04892 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.45077E-03 0.04212  1.22174E-04 0.29936  7.30113E-04 0.10138  8.15535E-04 0.10416  1.75801E-03 0.06564  7.31982E-04 0.10683  2.92957E-04 0.17452 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.83436E-01 0.09340  1.25488E-02 0.00492  3.02875E-02 0.00175  1.11462E-01 0.00308  3.24301E-01 0.00243  1.22149E+00 0.01321  7.80325E+00 0.05554 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.45992E-03 0.04237  1.19761E-04 0.30839  7.36819E-04 0.09923  8.25423E-04 0.10596  1.74146E-03 0.06565  7.37181E-04 0.10580  2.99278E-04 0.17066 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.88629E-01 0.09306  1.25488E-02 0.00492  3.02909E-02 0.00176  1.11442E-01 0.00307  3.24267E-01 0.00243  1.22110E+00 0.01321  7.78332E+00 0.05579 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.37264E+02 0.04245 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.31141E-05 0.00104 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.57605E-05 0.00074 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.66020E-03 0.00800 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.40812E+02 0.00809 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.69164E-07 0.00082 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.59864E-06 0.00044  2.59859E-06 0.00044  2.60217E-06 0.00643 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.04195E-05 0.00099  4.04331E-05 0.00100  3.77271E-05 0.01253 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.08316E-01 0.00037  6.09078E-01 0.00037  4.96911E-01 0.01163 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.11964E+01 0.01567 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.48337E+01 0.00054  3.33582E+01 0.00059 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.20634E+04 0.00338  3.07097E+05 0.00152  6.11159E+05 0.00075  6.50169E+05 0.00083  5.98557E+05 0.00060  6.42164E+05 0.00084  4.37093E+05 0.00069  3.86789E+05 0.00075  2.95522E+05 0.00072  2.41262E+05 0.00058  2.08135E+05 0.00074  1.87800E+05 0.00077  1.73142E+05 0.00071  1.64875E+05 0.00097  1.60563E+05 0.00079  1.38844E+05 0.00107  1.37219E+05 0.00096  1.36050E+05 0.00090  1.33361E+05 0.00086  2.60575E+05 0.00065  2.52373E+05 0.00059  1.81920E+05 0.00076  1.17951E+05 0.00119  1.35472E+05 0.00088  1.29520E+05 0.00084  1.14535E+05 0.00071  1.84166E+05 0.00064  4.26933E+04 0.00134  5.17145E+04 0.00146  4.74295E+04 0.00116  2.81694E+04 0.00193  4.82967E+04 0.00153  3.11530E+04 0.00157  2.49746E+04 0.00164  4.09457E+03 0.00348  3.42011E+03 0.00337  3.00098E+03 0.00320  2.93185E+03 0.00352  2.94258E+03 0.00415  3.20458E+03 0.00520  3.87814E+03 0.00282  4.06151E+03 0.00410  8.24847E+03 0.00250  1.38467E+04 0.00201  1.81432E+04 0.00188  4.88833E+04 0.00131  5.14929E+04 0.00154  5.51074E+04 0.00109  3.60258E+04 0.00130  2.53477E+04 0.00127  1.87314E+04 0.00165  2.20883E+04 0.00192  4.34675E+04 0.00165  6.20011E+04 0.00188  1.24955E+05 0.00154  1.99708E+05 0.00148  3.05613E+05 0.00149  1.98376E+05 0.00184  1.43134E+05 0.00171  1.03497E+05 0.00171  9.32525E+04 0.00181  9.21109E+04 0.00210  7.71969E+04 0.00216  5.23535E+04 0.00257  4.84966E+04 0.00247  4.32056E+04 0.00244  3.65532E+04 0.00244  2.89278E+04 0.00240  1.93782E+04 0.00234  6.91411E+03 0.00258 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  7.84705E-01 0.00034 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.82142E+18 0.00032  4.60989E+17 0.00166 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.40171E-01 0.00013  1.56164E+00 0.00051 ];
INF_CAPT                  (idx, [1:   4]) = [  9.00164E-03 0.00069  4.02158E-02 0.00096 ];
INF_ABS                   (idx, [1:   4]) = [  1.04065E-02 0.00060  6.40516E-02 0.00121 ];
INF_FISS                  (idx, [1:   4]) = [  1.40484E-03 0.00059  2.38358E-02 0.00163 ];
INF_NSF                   (idx, [1:   4]) = [  3.92509E-03 0.00062  6.68848E-02 0.00168 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.79398E+00 8.6E-05  2.80606E+00 5.8E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06994E+02 8.6E-06  2.07494E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.43242E-08 0.00035  2.60353E-06 0.00022 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29767E-01 0.00013  1.49757E+00 0.00059 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43907E-01 0.00025  3.95490E-01 0.00072 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63002E-02 0.00031  9.31840E-02 0.00099 ];
INF_SCATT3                (idx, [1:   4]) = [  7.24254E-03 0.00298  2.79965E-02 0.00176 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03697E-02 0.00183 -8.94441E-03 0.00714 ];
INF_SCATT5                (idx, [1:   4]) = [  1.76220E-04 0.11432  6.64747E-03 0.01066 ];
INF_SCATT6                (idx, [1:   4]) = [  5.13154E-03 0.00317 -1.72443E-02 0.00339 ];
INF_SCATT7                (idx, [1:   4]) = [  7.79336E-04 0.02113  5.13112E-04 0.08019 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29813E-01 0.00013  1.49757E+00 0.00059 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43907E-01 0.00025  3.95490E-01 0.00072 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63005E-02 0.00031  9.31840E-02 0.00099 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.24219E-03 0.00298  2.79965E-02 0.00176 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03699E-02 0.00183 -8.94441E-03 0.00714 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.76155E-04 0.11470  6.64747E-03 0.01066 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.13158E-03 0.00317 -1.72443E-02 0.00339 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.79271E-04 0.02112  5.13112E-04 0.08019 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12786E-01 0.00027  1.01889E+00 0.00042 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56652E+00 0.00027  3.27155E-01 0.00042 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.03610E-02 0.00060  6.40516E-02 0.00121 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69830E-02 0.00025  6.51893E-02 0.00137 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.13188E-01 0.00013  1.65795E-02 0.00051  1.11809E-03 0.00415  1.49645E+00 0.00059 ];
INF_S1                    (idx, [1:   8]) = [  2.39136E-01 0.00025  4.77110E-03 0.00076  4.83334E-04 0.00663  3.95007E-01 0.00072 ];
INF_S2                    (idx, [1:   8]) = [  9.78371E-02 0.00031 -1.53690E-03 0.00212  2.63002E-04 0.00904  9.29210E-02 0.00098 ];
INF_S3                    (idx, [1:   8]) = [  8.95757E-03 0.00244 -1.71504E-03 0.00219  9.60072E-05 0.02029  2.79005E-02 0.00173 ];
INF_S4                    (idx, [1:   8]) = [ -9.84955E-03 0.00187 -5.20166E-04 0.00484  2.01258E-06 0.74890 -8.94642E-03 0.00709 ];
INF_S5                    (idx, [1:   8]) = [  1.30372E-04 0.15165  4.58477E-05 0.05319 -3.84074E-05 0.03892  6.68587E-03 0.01055 ];
INF_S6                    (idx, [1:   8]) = [  5.25966E-03 0.00306 -1.28115E-04 0.02046 -4.73510E-05 0.02816 -1.71969E-02 0.00336 ];
INF_S7                    (idx, [1:   8]) = [  9.39680E-04 0.01742 -1.60344E-04 0.01385 -4.36371E-05 0.03194  5.56749E-04 0.07409 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.13233E-01 0.00013  1.65795E-02 0.00051  1.11809E-03 0.00415  1.49645E+00 0.00059 ];
INF_SP1                   (idx, [1:   8]) = [  2.39136E-01 0.00025  4.77110E-03 0.00076  4.83334E-04 0.00663  3.95007E-01 0.00072 ];
INF_SP2                   (idx, [1:   8]) = [  9.78374E-02 0.00031 -1.53690E-03 0.00212  2.63002E-04 0.00904  9.29210E-02 0.00098 ];
INF_SP3                   (idx, [1:   8]) = [  8.95723E-03 0.00244 -1.71504E-03 0.00219  9.60072E-05 0.02029  2.79005E-02 0.00173 ];
INF_SP4                   (idx, [1:   8]) = [ -9.84976E-03 0.00187 -5.20166E-04 0.00484  2.01258E-06 0.74890 -8.94642E-03 0.00709 ];
INF_SP5                   (idx, [1:   8]) = [  1.30307E-04 0.15218  4.58477E-05 0.05319 -3.84074E-05 0.03892  6.68587E-03 0.01055 ];
INF_SP6                   (idx, [1:   8]) = [  5.25969E-03 0.00306 -1.28115E-04 0.02046 -4.73510E-05 0.02816 -1.71969E-02 0.00336 ];
INF_SP7                   (idx, [1:   8]) = [  9.39615E-04 0.01741 -1.60344E-04 0.01385 -4.36371E-05 0.03194  5.56749E-04 0.07409 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31818E-01 0.00054  1.21677E+00 0.00705 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33627E-01 0.00079  1.31728E+00 0.00955 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33064E-01 0.00092  1.32812E+00 0.00878 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28831E-01 0.00103  1.05041E+00 0.00780 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43792E+00 0.00054  2.74279E-01 0.00713 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42680E+00 0.00079  2.53594E-01 0.00943 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.43025E+00 0.00092  2.51445E-01 0.00877 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45672E+00 0.00103  3.17798E-01 0.00778 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.60520E-03 0.01043  1.08703E-04 0.06740  8.98861E-04 0.02383  6.97499E-04 0.02675  1.87754E-03 0.01624  8.08906E-04 0.02459  2.13690E-04 0.04613 ];
LAMBDA                    (idx, [1:  14]) = [  7.34378E-01 0.02393  1.26869E-02 0.00168  3.03364E-02 0.00052  1.11921E-01 0.00096  3.23584E-01 0.00063  1.22084E+00 0.00395  7.74581E+00 0.01627 ];

