
import sys, os
import re
import random
import pickle
import data_store as ds

def main():
    filename = sys.argv[1]
    try:
        f = open(filename+".pickle", "rb")  # you should be creating state.pickle here...
        var = pickle.load(f)
    except Exception as e:
        # everything else, possibly fatal
        print(e)
        print ("Error opening output file")
        exit()
    else:
        f.close()
        print (str(var.enrichments))
        input()
        for item in var.detector_data:
            print(item)

if __name__ == "__main__":
    main()
