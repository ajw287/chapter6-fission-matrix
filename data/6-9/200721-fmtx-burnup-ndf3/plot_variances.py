#! /usr/bin/env python3
import pickle
#from pygmo import *
import pygmo as pg
from pygmo import population, problem, algorithm, plot_non_dominated_fronts, moead, nsga2, hypervolume, fast_non_dominated_sorting, plot_non_dominated_fronts, bfe, member_bfe, hypervolume
import sys, os
import copy
import problem_dir_micro as pdm
import matplotlib.pyplot as plt
import numpy as np
from pygmo_micro import save_data
import scipy.interpolate as inter
import re

import matplotlib.font_manager as font_manager

dark2 =["#1b9e77", "#d95f02", "#7570b3", "#e7298a", "#66a61e", "#e6ab02", "#a6761d", "#666666"]

fig_font = {'fontname':'Liberation Serif'}
font = font_manager.FontProperties(family='Liberation Serif')

class sphere_function:
    def __init__(self, dim):
        self.dim = dim
    def fitness(self, x):
        return [0,0]
    def get_bounds(self):
        return ([-100] * self.dim, [100] * self.dim)
    def get_name(self):
        return "Sphere Function"
    # Return number of objectives
    def get_nobj(self):
        return 2
    def get_extra_info(self):
        return "\tDimensions: " + str(self.dim)



hyps =[]
dir_list = sys.argv[1:]
f, ax = plt.subplots()
mean_above = []
mean_below = []
max_above = []
min_below = []

populations = [20,32,40, 60, 80, 100]#,52,60,80,100,120]
hypervolumes = []
standard_devs = []
variances = []
variances_xvals = []
ref_point = [50,50]

for index, dir in enumerate(dir_list):
    hyper = []
    pickle_dir_list = [x for x in os.listdir(dir) if x.startswith("results-")]
    print(pickle_dir_list)
    for index2, pickle_dir in enumerate (pickle_dir_list):
    #run through the files in order.
        print (str(pickle_dir))
        try:
            pickle_file = open(dir+"/"+pickle_dir+"/pop_0040.pickle", "rb")
            print("opened file")
            save = pickle.load(pickle_file)
        except:
            print("error opening "+pickle_dir+"/" + pickle_file)
            exit()
        else:
            pop = save.pop
#            prob = problem(pdm.problem_dir_micro())
            prob = pg.problem(sphere_function(dim = 9))
            pop = population(prob)#prob, b=bfe(member_bfe()))
            for ind in range(len(save.pop)):
                #print(ind)
                if np.isnan(save.pop.get_f()[int(ind)]).any():
                    pass
                else:
                    pop.push_back(save.pop.get_x()[int(ind)], save.pop.get_f()[int(ind)])
            hyper.append(hypervolume(pop))
#            if index2 >= 4:
#                break
            #print(save.outputs)
            #i = input()
    #ref_point = hyper[0].refpoint(offset = 10.)
    #print("candidate ref point: "+ str(ref_point))
    ref_point = [50,50]
    print("but we'll just use: "+ str(ref_point))
    hyp_list = [x.compute(ref_point) for x in hyper]
    temp_vals = []
    for val in hyp_list:
        temp_vals.append(val)
        if len(temp_vals) >1:
            variances.append(np.var(temp_vals))
            variances_xvals.append(len(temp_vals))
print(hypervolumes)
print(standard_devs)
print(variances)
print(variances_xvals)
ax.plot(variances_xvals, variances, c="#e7298a",  alpha=1.0, dashes=(5., 5.), dash_capstyle='round', label="variance convergence") # , marker='x'
#from matplotlib.ticker import MaxNLocator
#ax.xaxis.set_major_locator(MaxNLocator(integer=True))
ax.set_title('Hypervolume variance over subsequent runs', **fig_font)
ax.set_ylabel('variance of hypervolumes' + str(ref_point), **fig_font)
#plt.xticks(populations, **fig_font)
#locs, labels = plt.yticks()
#plt.yticks(locs, **fig_font)
ax.set_xlabel('number of runs', **fig_font)
ax.legend(loc='lower right', prop=font) #loc="best" doesn't work
plt.savefig("populations.svg")
#plt.show()

#for results_dir in [x[0].startswith("gen") for x in os.walk(directory)]
