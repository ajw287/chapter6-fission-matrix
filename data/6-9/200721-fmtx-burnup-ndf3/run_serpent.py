import sys, os
import re
import numpy as np
import pickle
import fcntl
import time
import socket

from data_store import data_store

def main():
    if 3 != len(sys.argv):
        exit()
    index = sys.argv[1]
    serpent_dir = sys.argv[2]
    # host specific settings
    host = socket.gethostname()
    if host == 'lux' or host == 'lise' or host == 'ray':
        serpent = 'sss2'
    else:
        serpent = '/home/ajw287/rds/hpc-work/software/Serpent/sss2' #
    working_file  = "./hpc_{:04d}".format(int(index))
    os.chdir(serpent_dir)
    os.system(serpent+" -omp 5 "+working_file)
    os.system("python3 parse_detectors.py " + working_file)
    os.chdir("..")
    exit()

if __name__ == "__main__":
		main()
