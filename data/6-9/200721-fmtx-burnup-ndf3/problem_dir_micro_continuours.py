import numpy as np
import time
import data_store as ds
import sys
import os
import pickle
import shutil
import copy
import re

class problem_dir_micro:
    bu_steps_to_bu = [0.1, 0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12.5, 15, 17.5, 20, 25, 30, 35, 40]
    generation = 0
    task_list = []
    simID = "burn_cypp"

    nobj = 2
    dim  = 9
    individual =0
    mc_seed = 312078600
    hpc_dir="/home/ajw287/rds/hpc-work/ex4_"+simID+"/"
    hpc_uri="hpc:"+hpc_dir
    # very simple perturbed straight line MO problem
    # perturbed with this random array generated from
    # np.random.uniform(low=0, high=0.6,size=100)
    def __deepcopy__(self, memo):
        return problem_dir_micro()

    def aw_round(self,x, base=0.2):
        return base * round(x/base)

    def __init__(self, seed=-1):
        #self.dim = 9
        #self.nobj = 3 # average enrichment , lppf , k infinity error
        #self.generation = 0
        if seed == -1:
            print("seed not provided")
            print("keeping static value:"+str(problem_dir_micro.mc_seed))
        else:
            problem_dir_micro.mc_seed = 312078600 + (seed%100)
            print("completed init function"+str(seed))

    def get_nobj(self):
        return problem_dir_micro.nobj

    def start_fit(self, x):
        return []

    def fitness(self, x):
        #not implemented
        print("called the 'fitness' function - not implemented")
        exit()
    def get_bounds(self):
        return ([0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8],[5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0])
    def has_batch_fitness(self):
        return True

    def ssh_the_serpent(self, action_numbers, s_dir="./runs/"):
        template_file = "slurm_template"
        slurm_out = "slurm_serpent_{:04d}".format(int(action_numbers[0]))
        options = ""
        for a in action_numbers:
            options += " "+str(a)
            options += " ./ "      # saving slurm files in generation dir, so extension is current directory
            #options += " "+"/hpc_{:04d}".format(int(a)) #"+str(a).zfill(4)
        try:
            tempfile = open(template_file, 'r')
        except:
            print("failed to open template")
        else:
            filedata = tempfile.read()
            # Replace the target strings
            filedata = filedata.replace('#options#', '"'+options+'"')
            task_name = "B"+str(action_numbers[0]).zfill(3) + "g"+str(problem_dir_micro.generation).zfill(3)
            problem_dir_micro.task_list.append(task_name)
            filedata = filedata.replace('#title#', task_name)
            filedata = filedata.replace('#application#', problem_dir_micro.hpc_dir+s_dir+"runSerpent.sh")
            # Write the file out again
            try:
                f = open(s_dir+slurm_out, 'w')
            except Exception as e:
                print (e)
                print("failed to open slurm file")
                exit()
            else:
                f.write(filedata)
                f.close()
        #os.system("sbatch " + slurm_out)
        print("sbatch " + slurm_out)

    def set_assembly_u235(self, filename, enrichments, mc_seed=1574980501):
        inputs = ds.data_store()
        num_pins = 9 # number of assemblies (with single pin conc)
        use_random = False

        inputs.enrichments = []
        try:
            file = open("./super_cell", 'r')
        except in_file_error:
            usage_error("input file error: (check path and that it exists!)")
            quit()
        else:
            filedata = file.read()
            filedata = filedata.replace('%mcseed%', str(problem_dir_micro.mc_seed))
#            for pn in range(1, num_pins+1):
            for pn, enrichment in enumerate(enrichments):
                #print ("pin number "+str(pn+1))
                filedata = filedata.replace('%pin'+str(pn+1)+'%', "UO2_{:02d}".format(int(self.aw_round(enrichment) * 10)))
                inputs.enrichments.append(enrichment)
            # Write the file out again
            try:
                file = open(filename, 'w')
            except out_file_error:
                usage_error("file output error")
                quit()
            else:
                file.write(filedata)
                try:
                    f = open(filename+".pickle", "wb+")  # you should be creating state.pickle here...
                except:
                    print ("Error opening output file")
                    exit()
                else:
                    f.write(pickle.dumps(inputs))
                    f.close()

    def check_queue(self, old_val):
        print(problem_dir_micro.task_list)
        # assumes that no other simulation is on the same generation...
        ### returns number of slurm jobs with the strings in the task list running
        temp_file = "num_jobs"+problem_dir_micro.simID+".txt"
        num_jobs = 0
        # we do this then copy it across because of the horrible banner at startup
        returncode = os.system("ssh hpc 'squeue -u ajw287 > "+temp_file+"'") #  |wc -l
        if returncode == 0:
            returncode += os.system('scp hpc:~/'+temp_file+' .')
        try:
            f = open(temp_file)
        except:
            print("file error")
            return(old_val)
        else:
            lines = f.readlines()
            temp_task_list = []
            for line in lines:
                for task in problem_dir_micro.task_list:
                    if task in line:
                        print("task: " +task+" still running")
                        temp_task_list.append(task)
                        num_jobs += 1
            problem_dir_micro.task_list = temp_task_list
            return num_jobs
#  this code just counts the number of lines
# and returns this.
#            jobs = [int(x) for x in f]
#            os.system('rm ./'+temp_file)
#            return jobs[0]

    def num_4chars(self, x):
        return(x[4:8])

    def get_hot_pin_ppf(self, d):
        # takes a list of assembly powers and finds
        # the ppf and the position of the hot pin7
        full = np.block([[d[0], d[1], d[2]],
                         [d[3], d[4], d[5]],
                         [d[6], d[7], d[8]] ])
        #print(np.shape(full))
        hot_pin = np.unravel_index(full.argmax(), full.shape)
        max = np.max(full)
        full[full == 0.0] = np.nan
        mean_no_zero = np.nanmean(full.ravel())
        ppf = max / mean_no_zero
        #print(str(mean_no_zero))
        #print(str(ppf))
        return (list(hot_pin) + [ppf])


    def dist(self, a,b):
        #returns the euclidean distance.
        return np.sqrt(np.sum((a-b)**2, axis=0))

    def extract_remote_fitnesses(self, dat_dir, num_to_add=0):
        outs = []
###  N.B. SPEED UP simualtion by commenting these lines
### TODO: set up a flag that avoids doing this automatically...
### FIXME: bad code. bad andrew.
        os.system("ssh hpc  'cd "+problem_dir_micro.hpc_dir+dat_dir+"; python3 get_detectors_after.py .'")
        os.system("scp -r "+problem_dir_micro.hpc_uri+dat_dir+"*.pickle "+dat_dir)
        # list the files and sort by the number inside the pickle files.
        full_dir_list = os.listdir(dat_dir)
        file_list = [f for f in full_dir_list if f.endswith(".pickle")]
        # extract the data from the files.
        for i,filename in enumerate(sorted(file_list, key = self.num_4chars) ):
            with open(dat_dir+filename,'rb') as pickle_file:
                var = pickle.load(pickle_file, encoding='latin1')
                # assert that:  var.enrichments = input_data[i]
#TODO: Puth this back in and fix.
#                for truth_val in np.isclose(var.enrichments, input_data[i]).tolist():
#                    if truth_val != True:
#                        print("input data not same as expected:")
#                        print("from pickle: "+str(var.enrichments))
#                        print("from input : "+str(input_data[i]))
#                        input("enter to proceed, ctrl+C to exit")
                results = self.get_hot_pin_ppf(var.detector_data)
#                radial_dist = self.dist(results[0], results[1])
                cyc = problem_dir_micro.bu_steps_to_bu[-1] - var.cycle_length_bu
                outs.append([cyc, results[2]]) # cycle length (in burnup) vs ppf
        return outs

    def generation_completed(self, remote_files, local_files):
        # check if the remote files were all created.
        completed = True
        for file_name in local_files:
            if ".pickle" in file_name:
                base, ext = os.path.splitext(os.path.basename(file_name))
                for s in remote_files:
                    print (s.decode("utf-8"))
                if any(os.path.basename(base)+"_det0.m" in s.decode("utf-8") for s in remote_files):
                    print("OK "+base +"  "+ext+ " has a det0.m file ---")
                else:
                    print("NO "+base +"  "+ext+ " has no det0.m file XXX")
                    completed = False
                    exit()
        return completed

    def batch_fitness(self, input_data):
        # turn the array into an iterable list of inputs.
        individuals=[]
        output_data = []
        action_list = []
        num_to_add = 0
        #generate serpent input files
#        if self.generation > 0 and self.generation < 10:
#            num_inputs = input_data.size
#            num_to_add = input_data.size - 540
#            print(str(num_inputs))
#            print(str(input_data))
#            temp_input_split = np.take(input_data, range(0,540))
#            print(str(temp_input_split))
#            input_data = temp_input_split
#            num_inputs = 540
        num_inputs = input_data.size
        print(str(num_inputs))
        input_data = input_data.reshape((-1,self.dim))
        print (self.dim)
        #generate serpent input files
        num_inputs = input_data.size
        print(str(num_inputs))
#        if (self.generation == 51):
#            num_inputs = input_data.size
#            print(str(num_inputs))
#            print(str(input_data))
#            temp_input_split = np.take(input_data, range(0,540))
#            print(str(temp_input_split))
#            exit()
        data_dir = "./gen_{:04d}".format(int(problem_dir_micro.generation))+"/"
        try:
            os.stat(data_dir)
        except:
            os.mkdir(data_dir)
            shutil.copy("./run_serpent.py", data_dir)
            shutil.copy("./runSerpent.sh", data_dir)
            shutil.copy("./data_store.py", data_dir)
            shutil.copy("./get_detectors_after.py", data_dir)
            shutil.copy("./parse_detectors.py", data_dir)
        else:
            # file already exists!
            # This means that maybe you don't have to repeat the simulations.
            # Test if the files are already processed:
            # locally
            local_files = os.listdir(data_dir)
            import subprocess
            ls = subprocess.Popen(['ssh','hpc', 'ls', problem_dir_micro.hpc_dir+data_dir], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            remote_files, err =  ls.communicate()
            print (remote_files.splitlines())
            print(local_files)
            if self.generation_completed(remote_files.splitlines(), local_files):
                # extract fitneeses and continue
                print("successfully got fitnesses from a previous run... returning fitnesses for generation "+str(problem_dir_micro.generation))
                output_data = self.extract_remote_fitnesses(data_dir, num_to_add)
                output_data = np.array([item for sublist in output_data for item in sublist])
                problem_dir_micro.generation +=1
                problem_dir_micro.individual =0
                return output_data
            else:
                print("apparently unable to assume that generation " + str(problem_dir_micro.generation)+ " completed " )
                print("if a generation completed uncleanly, maybe delete this generation and re-run...")
                exit()
            # on the hpc
#        for row in input_data:
#            print(row)
        for row in input_data:
            problem_dir_micro.individual+=1
            individuals.append(problem_dir_micro.individual)

            file = "/hpc_{:04d}".format(int(problem_dir_micro.individual))
            self.set_assembly_u235(data_dir+file, row)
            #os.system("python3 set_assembly_u235.py ./runs/hpc_{:04d}".format(int(i)))
            action_list.append(problem_dir_micro.individual)
            if len(action_list) == 6:
                self.ssh_the_serpent(action_list, data_dir)
                action_list = []
                #print(Y[i])
        # catch any that don't divide by 6
        if len(action_list) > 0:
            self.ssh_the_serpent(action_list, data_dir)
            action_list = []
        # TODO:
        #copy to server
        os.system("scp -r "+data_dir+" "+problem_dir_micro.hpc_uri+data_dir)
        # run on server.
        cmd_string = "ssh hpc 'cd "+problem_dir_micro.hpc_dir+data_dir+"; for i in slurm_serpent_*; do sbatch $i; done'"
        #print(cmd_string)
        os.system (cmd_string)
        print("submitted jobs on hpc")
        num_jobs = self.check_queue(1000)
        print("jobs on the hpc: " + str(num_jobs))
        time.sleep(300) # 6 minute wait.
        num_jobs = self.check_queue(num_jobs)
        while num_jobs >= 1:
            print("jobs on the hpc: " + str(num_jobs))
            time.sleep(60) # 1.5 mins
            num_jobs = self.check_queue(num_jobs)
        print("found no further jobs")
        # post process the data for our problem
        print("generation: "+str(problem_dir_micro.generation))
        output_data = self.extract_remote_fitnesses(data_dir,num_to_add)
        output_data = np.array([item for sublist in output_data for item in sublist])
        problem_dir_micro.generation +=1
        problem_dir_micro.individual =0
        return output_data
