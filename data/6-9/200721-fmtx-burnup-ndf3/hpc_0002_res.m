
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0002' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:56:06 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:00:06 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595300166 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.96912E-01  1.00757E+00  9.96062E-01  9.99406E-01  1.00004E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.0E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13279E-02 0.00099  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88672E-01 1.1E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.94835E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.95254E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.72735E+00 0.00020  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.79339E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.79253E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.76382E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.56447E-01 0.00102  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000850 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00017E+04 0.00076 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00017E+04 0.00076 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.82007E+01 ;
RUNNING_TIME              (idx, 1)        =  4.00535E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  4.55900E-01  4.55900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.33334E-03  1.33334E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.54765E+00  3.54765E+00  0.00000E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.00475E+00  0.00000E+00 ];
CPU_USAGE                 (idx, 1)        = 4.54410 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99830E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  8.73453E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.62954E+08 ;
TOT_DECAY_HEAT            (idx, 1)        =  6.57373E-04 ;
TOT_SF_RATE               (idx, 1)        =  7.43560E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  8.62954E+08 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  6.57373E-04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  7.89716E+03 ;
INGESTION_TOXICITY        (idx, 1)        =  4.16851E+01 ;
ACTINIDE_INH_TOX          (idx, 1)        =  7.89716E+03 ;
ACTINIDE_ING_TOX          (idx, 1)        =  4.16851E+01 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  1.08881E+08 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  8.62598E+08 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  3.52075E+08 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.07715E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 0 ;
BURNUP                     (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
BURN_DAYS                 (idx, 1)        =  0.00000E+00 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.25587E-01 0.00109 ];
U235_FISS                 (idx, [1:   4]) = [  1.30197E+16 0.00059  9.38581E-01 0.00017 ];
U238_FISS                 (idx, [1:   4]) = [  8.50340E+14 0.00273  6.12933E-02 0.00258 ];
U235_CAPT                 (idx, [1:   4]) = [  2.75588E+15 0.00150  1.65221E-01 0.00138 ];
U238_CAPT                 (idx, [1:   4]) = [  8.23157E+15 0.00103  4.93465E-01 0.00065 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000850 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.15807E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000850 5.00716E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2706901 2.71035E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2251229 2.25410E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 42720 4.27157E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000850 5.00716E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.02914E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.41130E+16 1.2E-05  3.41130E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38676E+16 1.4E-06  1.38676E+16 1.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.66748E+16 0.00049  1.10935E+16 0.00051  5.58131E+15 0.00094 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.05424E+16 0.00026  2.49611E+16 0.00023  5.58131E+15 0.00094 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.07715E+16 0.00051  3.07715E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.51751E+18 0.00046  4.07235E+17 0.00048  1.11028E+18 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.62927E+14 0.00514 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.08053E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.16855E+18 0.00056 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12515E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.76213E+00 0.00038 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.57249E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.83511E-01 0.00030 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.22645E+00 0.00032 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94498E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96942E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.11854E+00 0.00050 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.10898E+00 0.00051 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.45990E+00 1.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02562E+02 1.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.10894E+00 0.00052  1.10109E+00 0.00051  7.89071E-03 0.00819 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.10902E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.10873E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.10902E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.11858E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.76430E+01 0.00018 ];
IMP_ALF                   (idx, [1:   2]) = [  1.76432E+01 8.2E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.36357E-07 0.00318 ];
IMP_EALF                  (idx, [1:   2]) = [  4.35414E-07 0.00144 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.06327E-01 0.00278 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.06285E-01 0.00121 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.48430E-03 0.00543  1.84440E-04 0.03051  1.02405E-03 0.01349  1.01422E-03 0.01401  2.95417E-03 0.00813  9.87349E-04 0.01322  3.20075E-04 0.02415 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.13334E-01 0.01223  1.08420E-02 0.01746  3.16469E-02 0.00022  1.10173E-01 0.00027  3.20653E-01 0.00022  1.34578E+00 0.00016  8.64859E+00 0.00778 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.10640E-03 0.00781  2.00239E-04 0.04833  1.12398E-03 0.02110  1.12267E-03 0.02097  3.22690E-03 0.01150  1.07693E-03 0.02000  3.55695E-04 0.03438 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.18261E-01 0.01776  1.24908E-02 2.7E-06  3.16462E-02 0.00032  1.10170E-01 0.00040  3.20777E-01 0.00034  1.34600E+00 0.00022  8.89676E+00 0.00206 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.92286E-05 0.00113  2.92165E-05 0.00114  3.08153E-05 0.01063 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.24085E-05 0.00101  3.23951E-05 0.00102  3.41771E-05 0.01067 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.11420E-03 0.00821  2.03809E-04 0.04629  1.10833E-03 0.02031  1.12452E-03 0.02034  3.25146E-03 0.01232  1.07052E-03 0.02034  3.55575E-04 0.03533 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.15660E-01 0.01861  1.24908E-02 3.9E-06  3.16471E-02 0.00037  1.10223E-01 0.00047  3.20688E-01 0.00036  1.34571E+00 0.00024  8.89040E+00 0.00243 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.92775E-05 0.00245  2.92642E-05 0.00245  3.00865E-05 0.03319 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.24640E-05 0.00243  3.24494E-05 0.00243  3.33351E-05 0.03304 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.99510E-03 0.02645  2.05915E-04 0.14055  1.01059E-03 0.06860  1.14109E-03 0.05910  3.10992E-03 0.03972  1.14557E-03 0.06247  3.82021E-04 0.10708 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.57748E-01 0.05861  1.24908E-02 8.2E-06  3.16564E-02 0.00085  1.10290E-01 0.00108  3.20846E-01 0.00102  1.34410E+00 0.00064  8.93697E+00 0.00543 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.99695E-03 0.02556  1.95520E-04 0.13812  1.03926E-03 0.06446  1.12101E-03 0.05777  3.11286E-03 0.03854  1.13339E-03 0.06071  3.94910E-04 0.10029 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.70180E-01 0.05673  1.24908E-02 8.2E-06  3.16561E-02 0.00085  1.10299E-01 0.00107  3.20948E-01 0.00101  1.34414E+00 0.00064  8.93697E+00 0.00543 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.39283E+02 0.02650 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.92424E-05 0.00071 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.24237E-05 0.00048 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.14497E-03 0.00462 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.44404E+02 0.00468 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.66220E-07 0.00058 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87706E-06 0.00041  2.87679E-06 0.00041  2.91473E-06 0.00507 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.50099E-05 0.00074  4.50346E-05 0.00074  4.16472E-05 0.00773 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.81127E-01 0.00029  6.80534E-01 0.00030  7.83216E-01 0.00824 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.01260E+01 0.01304 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.79253E+01 0.00044  3.71297E+01 0.00044 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.16896E+04 0.00330  2.89995E+05 0.00147  6.03508E+05 0.00096  6.51206E+05 0.00073  5.97573E+05 0.00070  6.42439E+05 0.00075  4.36218E+05 0.00055  3.85876E+05 0.00069  2.95066E+05 0.00074  2.40950E+05 0.00082  2.08222E+05 0.00090  1.87142E+05 0.00091  1.72811E+05 0.00076  1.64648E+05 0.00083  1.60450E+05 0.00081  1.38282E+05 0.00081  1.36770E+05 0.00067  1.35435E+05 0.00106  1.33297E+05 0.00088  2.59870E+05 0.00071  2.51284E+05 0.00068  1.81323E+05 0.00089  1.17686E+05 0.00081  1.35618E+05 0.00093  1.28537E+05 0.00114  1.16749E+05 0.00086  1.92391E+05 0.00077  4.39586E+04 0.00127  5.51986E+04 0.00122  4.99498E+04 0.00187  2.91202E+04 0.00212  5.05531E+04 0.00166  3.43989E+04 0.00178  2.93707E+04 0.00197  5.59056E+03 0.00329  5.56697E+03 0.00333  5.68909E+03 0.00388  5.86941E+03 0.00338  5.83890E+03 0.00310  5.70843E+03 0.00248  5.94006E+03 0.00320  5.53071E+03 0.00302  1.05243E+04 0.00271  1.67348E+04 0.00210  2.13099E+04 0.00187  5.64999E+04 0.00138  5.98295E+04 0.00117  6.61916E+04 0.00170  4.61225E+04 0.00153  3.50138E+04 0.00199  2.72723E+04 0.00144  3.25298E+04 0.00154  6.26875E+04 0.00111  8.57655E+04 0.00103  1.66229E+05 0.00117  2.57869E+05 0.00118  3.86704E+05 0.00099  2.48000E+05 0.00109  1.77521E+05 0.00106  1.27451E+05 0.00121  1.14536E+05 0.00136  1.12664E+05 0.00112  9.41239E+04 0.00130  6.35058E+04 0.00152  5.86554E+04 0.00133  5.22057E+04 0.00150  4.40562E+04 0.00126  3.46629E+04 0.00142  2.31666E+04 0.00152  8.19846E+03 0.00222 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.11829E+00 0.00045 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.15061E+18 0.00052  3.66944E+17 0.00091 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38270E-01 0.00011  1.49683E+00 0.00041 ];
INF_CAPT                  (idx, [1:   4]) = [  6.28518E-03 0.00062  2.57364E-02 0.00024 ];
INF_ABS                   (idx, [1:   4]) = [  8.41485E-03 0.00055  5.68561E-02 0.00060 ];
INF_FISS                  (idx, [1:   4]) = [  2.12967E-03 0.00059  3.11197E-02 0.00091 ];
INF_NSF                   (idx, [1:   4]) = [  5.46900E-03 0.00059  7.58293E-02 0.00091 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.56801E+00 6.1E-05  2.43670E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03920E+02 6.1E-06  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  5.93262E-08 0.00040  2.56933E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29840E-01 0.00011  1.44000E+00 0.00045 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44577E-01 0.00019  3.78998E-01 0.00052 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64194E-02 0.00032  8.97416E-02 0.00091 ];
INF_SCATT3                (idx, [1:   4]) = [  7.38625E-03 0.00326  2.70059E-02 0.00213 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03633E-02 0.00167 -8.39531E-03 0.00614 ];
INF_SCATT5                (idx, [1:   4]) = [  1.26157E-04 0.13266  6.36209E-03 0.00716 ];
INF_SCATT6                (idx, [1:   4]) = [  5.05912E-03 0.00317 -1.62627E-02 0.00353 ];
INF_SCATT7                (idx, [1:   4]) = [  7.34365E-04 0.02124  4.41467E-04 0.10231 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29878E-01 0.00011  1.44000E+00 0.00045 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44578E-01 0.00019  3.78998E-01 0.00052 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64196E-02 0.00032  8.97416E-02 0.00091 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.38625E-03 0.00326  2.70059E-02 0.00213 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03633E-02 0.00167 -8.39531E-03 0.00614 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.26407E-04 0.13215  6.36209E-03 0.00716 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.05908E-03 0.00317 -1.62627E-02 0.00353 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.34146E-04 0.02122  4.41467E-04 0.10231 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13534E-01 0.00024  9.67691E-01 0.00038 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56104E+00 0.00024  3.44464E-01 0.00038 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.37656E-03 0.00054  5.68561E-02 0.00060 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69634E-02 0.00017  5.78596E-02 0.00075 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11307E-01 0.00011  1.85333E-02 0.00045  1.03006E-03 0.00284  1.43897E+00 0.00045 ];
INF_S1                    (idx, [1:   8]) = [  2.39155E-01 0.00019  5.42184E-03 0.00086  4.39731E-04 0.00504  3.78558E-01 0.00052 ];
INF_S2                    (idx, [1:   8]) = [  9.80189E-02 0.00030 -1.59944E-03 0.00259  2.37957E-04 0.00668  8.95036E-02 0.00091 ];
INF_S3                    (idx, [1:   8]) = [  9.29457E-03 0.00250 -1.90832E-03 0.00191  8.43098E-05 0.02420  2.69216E-02 0.00213 ];
INF_S4                    (idx, [1:   8]) = [ -9.72339E-03 0.00172 -6.39954E-04 0.00428 -7.45311E-07 1.00000 -8.39456E-03 0.00608 ];
INF_S5                    (idx, [1:   8]) = [  1.09258E-04 0.15150  1.68986E-05 0.12495 -3.55508E-05 0.03864  6.39765E-03 0.00716 ];
INF_S6                    (idx, [1:   8]) = [  5.20043E-03 0.00301 -1.41306E-04 0.01694 -4.52475E-05 0.03188 -1.62175E-02 0.00355 ];
INF_S7                    (idx, [1:   8]) = [  9.07889E-04 0.01619 -1.73525E-04 0.01334 -3.87980E-05 0.03164  4.80265E-04 0.09404 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11345E-01 0.00011  1.85333E-02 0.00045  1.03006E-03 0.00284  1.43897E+00 0.00045 ];
INF_SP1                   (idx, [1:   8]) = [  2.39156E-01 0.00019  5.42184E-03 0.00086  4.39731E-04 0.00504  3.78558E-01 0.00052 ];
INF_SP2                   (idx, [1:   8]) = [  9.80190E-02 0.00030 -1.59944E-03 0.00259  2.37957E-04 0.00668  8.95036E-02 0.00091 ];
INF_SP3                   (idx, [1:   8]) = [  9.29457E-03 0.00250 -1.90832E-03 0.00191  8.43098E-05 0.02420  2.69216E-02 0.00213 ];
INF_SP4                   (idx, [1:   8]) = [ -9.72333E-03 0.00171 -6.39954E-04 0.00428 -7.45311E-07 1.00000 -8.39456E-03 0.00608 ];
INF_SP5                   (idx, [1:   8]) = [  1.09509E-04 0.15087  1.68986E-05 0.12495 -3.55508E-05 0.03864  6.39765E-03 0.00716 ];
INF_SP6                   (idx, [1:   8]) = [  5.20039E-03 0.00301 -1.41306E-04 0.01694 -4.52475E-05 0.03188 -1.62175E-02 0.00355 ];
INF_SP7                   (idx, [1:   8]) = [  9.07671E-04 0.01617 -1.73525E-04 0.01334 -3.87980E-05 0.03164  4.80265E-04 0.09404 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32725E-01 0.00045  1.06343E+00 0.00502 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34103E-01 0.00092  1.12840E+00 0.00648 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34068E-01 0.00087  1.12639E+00 0.00719 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.30063E-01 0.00092  9.55984E-01 0.00545 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43231E+00 0.00045  3.13641E-01 0.00502 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42391E+00 0.00092  2.95702E-01 0.00651 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42411E+00 0.00087  2.96294E-01 0.00712 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.44891E+00 0.00092  3.48928E-01 0.00540 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.10640E-03 0.00781  2.00239E-04 0.04833  1.12398E-03 0.02110  1.12267E-03 0.02097  3.22690E-03 0.01150  1.07693E-03 0.02000  3.55695E-04 0.03438 ];
LAMBDA                    (idx, [1:  14]) = [  8.18261E-01 0.01776  1.24908E-02 2.7E-06  3.16462E-02 0.00032  1.10170E-01 0.00040  3.20777E-01 0.00034  1.34600E+00 0.00022  8.89676E+00 0.00206 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0002' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:56:06 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:06:25 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595300166 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00424E+00  1.00762E+00  9.97198E-01  9.86791E-01  1.00415E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.3E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12672E-02 0.00100  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88733E-01 1.1E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.96571E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.96989E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.72065E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.76071E+01 0.00042  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.75986E+01 0.00042  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.57677E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.48167E-01 0.00106  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000606 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00012E+04 0.00072 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00012E+04 0.00072 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.96967E+01 ;
RUNNING_TIME              (idx, 1)        =  1.03117E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.55900E-01  4.55900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.79000E-02  8.48333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.77492E+00  3.53220E+00  2.69507E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  6.21500E-02  3.32500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.05000E-03  7.33332E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.03116E+01  1.30509E+02 ];
CPU_USAGE                 (idx, 1)        = 4.81943 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00073E+00 0.00016 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.41258E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  7.95343E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.76161E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.43547E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.26717E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  8.98619E+02 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  6.68624E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.67172E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.24337E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  1.77190E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.58987E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  3.67591E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.84383E+06 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  1.40431E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.28226E+11 ;
TE132_ACTIVITY            (idx, 1)        =  2.51487E+14 ;
I131_ACTIVITY             (idx, 1)        =  7.42576E+13 ;
I132_ACTIVITY             (idx, 1)        =  2.43688E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.71224E+07 ;
CS137_ACTIVITY            (idx, 1)        =  1.34656E+11 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  6.63913E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.65222E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.33615E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  9.20419E+16 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.19113E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 1 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E-01  1.00009E-01 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+00 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.37824E-01 0.00105 ];
U235_FISS                 (idx, [1:   4]) = [  1.29719E+16 0.00062  9.34503E-01 0.00019 ];
U238_FISS                 (idx, [1:   4]) = [  8.80014E+14 0.00296  6.33884E-02 0.00281 ];
PU239_FISS                (idx, [1:   4]) = [  2.72390E+13 0.01572  1.96175E-03 0.01569 ];
U235_CAPT                 (idx, [1:   4]) = [  2.76856E+15 0.00151  1.55519E-01 0.00144 ];
U238_CAPT                 (idx, [1:   4]) = [  8.42773E+15 0.00098  4.73371E-01 0.00063 ];
PU239_CAPT                (idx, [1:   4]) = [  1.55005E+13 0.02039  8.70621E-04 0.02040 ];
PU240_CAPT                (idx, [1:   4]) = [  1.91351E+11 0.19560  1.07535E-05 0.19566 ];
XE135_CAPT                (idx, [1:   4]) = [  7.28656E+14 0.00287  4.09369E-02 0.00293 ];
SM149_CAPT                (idx, [1:   4]) = [  2.21171E+13 0.01835  1.24232E-03 0.01835 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000606 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.28430E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000606 5.00728E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2785647 2.78944E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2172148 2.17503E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 42811 4.28162E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000606 5.00728E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.72529E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.41325E+16 1.2E-05  3.41325E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38662E+16 1.4E-06  1.38662E+16 1.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.77965E+16 0.00047  1.21093E+16 0.00047  5.68716E+15 0.00096 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.16627E+16 0.00027  2.59755E+16 0.00022  5.68716E+15 0.00096 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.19113E+16 0.00050  3.19113E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.56483E+18 0.00046  4.19288E+17 0.00044  1.14554E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.73295E+14 0.00526 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.19360E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.20141E+18 0.00059 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12503E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12503E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.67852E+00 0.00042 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.61547E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.83736E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23582E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94480E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96940E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.08004E+00 0.00050 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07079E+00 0.00050 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46157E+00 1.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02583E+02 1.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07071E+00 0.00051  1.06321E+00 0.00050  7.58624E-03 0.00847 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07035E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06974E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07035E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.07961E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75696E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75697E+01 8.1E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.69856E-07 0.00347 ];
IMP_EALF                  (idx, [1:   2]) = [  4.68608E-07 0.00142 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.13391E-01 0.00310 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.13487E-01 0.00117 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.68521E-03 0.00531  1.87856E-04 0.03134  1.07595E-03 0.01282  1.05077E-03 0.01382  3.04585E-03 0.00827  9.94200E-04 0.01378  3.30591E-04 0.02386 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.09517E-01 0.01270  1.04922E-02 0.01954  3.16498E-02 0.00022  1.10180E-01 0.00027  3.20627E-01 0.00022  1.34559E+00 0.00015  8.63042E+00 0.00831 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.19126E-03 0.00801  2.07398E-04 0.04797  1.11969E-03 0.01974  1.13246E-03 0.02031  3.30402E-03 0.01217  1.06965E-03 0.02163  3.58029E-04 0.03515 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.12508E-01 0.01851  1.24907E-02 2.6E-06  3.16551E-02 0.00030  1.10164E-01 0.00037  3.20566E-01 0.00031  1.34519E+00 0.00023  8.92310E+00 0.00210 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.90194E-05 0.00116  2.90084E-05 0.00117  3.06149E-05 0.01142 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.10673E-05 0.00105  3.10556E-05 0.00106  3.27745E-05 0.01139 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.09639E-03 0.00871  2.07892E-04 0.04894  1.09313E-03 0.02096  1.13856E-03 0.02148  3.25066E-03 0.01260  1.04820E-03 0.02091  3.57948E-04 0.03740 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.22061E-01 0.02039  1.24908E-02 3.9E-06  3.16610E-02 0.00036  1.10174E-01 0.00043  3.20608E-01 0.00035  1.34538E+00 0.00028  8.95230E+00 0.00260 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.90343E-05 0.00263  2.90135E-05 0.00262  3.08604E-05 0.02796 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.10831E-05 0.00257  3.10608E-05 0.00257  3.30419E-05 0.02797 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.06164E-03 0.02487  2.20702E-04 0.17188  1.08368E-03 0.06455  1.14475E-03 0.06485  3.25656E-03 0.03767  1.03547E-03 0.06843  3.20475E-04 0.11271 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.36532E-01 0.06566  1.24908E-02 8.3E-06  3.16714E-02 0.00083  1.10078E-01 0.00100  3.20597E-01 0.00098  1.34409E+00 0.00068  8.93031E+00 0.00568 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.97747E-03 0.02436  2.15406E-04 0.17185  1.05846E-03 0.06415  1.12865E-03 0.06360  3.20443E-03 0.03646  1.03702E-03 0.06692  3.33508E-04 0.10468 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.44064E-01 0.06264  1.24908E-02 8.1E-06  3.16729E-02 0.00082  1.10080E-01 0.00100  3.20608E-01 0.00097  1.34417E+00 0.00067  8.93097E+00 0.00567 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.44376E+02 0.02495 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.90361E-05 0.00074 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.10848E-05 0.00050 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.08651E-03 0.00508 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.44097E+02 0.00509 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.54928E-07 0.00055 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87806E-06 0.00043  2.87794E-06 0.00043  2.89347E-06 0.00487 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.39081E-05 0.00070  4.39297E-05 0.00071  4.10449E-05 0.00822 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.81327E-01 0.00031  6.80855E-01 0.00031  7.62174E-01 0.00835 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02446E+01 0.01271 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.75986E+01 0.00042  3.64472E+01 0.00045 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.17708E+04 0.00305  2.90769E+05 0.00132  6.02320E+05 0.00093  6.49247E+05 0.00083  5.97827E+05 0.00074  6.41512E+05 0.00070  4.36199E+05 0.00051  3.85814E+05 0.00063  2.95240E+05 0.00088  2.41194E+05 0.00084  2.07744E+05 0.00080  1.87368E+05 0.00102  1.73156E+05 0.00092  1.64398E+05 0.00083  1.60105E+05 0.00091  1.38263E+05 0.00114  1.36562E+05 0.00102  1.35305E+05 0.00096  1.33056E+05 0.00107  2.60267E+05 0.00075  2.50813E+05 0.00078  1.81275E+05 0.00095  1.17659E+05 0.00106  1.35820E+05 0.00085  1.28377E+05 0.00075  1.16977E+05 0.00091  1.92590E+05 0.00079  4.39746E+04 0.00159  5.51644E+04 0.00138  5.00091E+04 0.00181  2.91044E+04 0.00195  5.04981E+04 0.00169  3.43640E+04 0.00167  2.93183E+04 0.00172  5.64803E+03 0.00394  5.59800E+03 0.00264  5.70977E+03 0.00368  5.93885E+03 0.00357  5.87206E+03 0.00375  5.73774E+03 0.00393  5.97232E+03 0.00405  5.59031E+03 0.00363  1.05418E+04 0.00247  1.68213E+04 0.00264  2.13799E+04 0.00215  5.65147E+04 0.00182  5.97934E+04 0.00107  6.58482E+04 0.00147  4.57983E+04 0.00158  3.47518E+04 0.00160  2.69942E+04 0.00165  3.22661E+04 0.00198  6.18669E+04 0.00103  8.44546E+04 0.00147  1.63424E+05 0.00117  2.52385E+05 0.00103  3.77264E+05 0.00095  2.41108E+05 0.00099  1.72607E+05 0.00068  1.24094E+05 0.00096  1.11292E+05 0.00093  1.09702E+05 0.00089  9.15947E+04 0.00109  6.19025E+04 0.00118  5.70014E+04 0.00106  5.07910E+04 0.00104  4.29149E+04 0.00118  3.37685E+04 0.00107  2.25782E+04 0.00112  7.99346E+03 0.00195 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.07898E+00 0.00034 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.19275E+18 0.00029  3.72117E+17 0.00071 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38437E-01 0.00010  1.50279E+00 0.00027 ];
INF_CAPT                  (idx, [1:   4]) = [  6.29945E-03 0.00053  2.76357E-02 0.00026 ];
INF_ABS                   (idx, [1:   4]) = [  8.42578E-03 0.00040  5.80881E-02 0.00049 ];
INF_FISS                  (idx, [1:   4]) = [  2.12634E-03 0.00060  3.04524E-02 0.00071 ];
INF_NSF                   (idx, [1:   4]) = [  5.46101E-03 0.00061  7.42332E-02 0.00071 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.56827E+00 6.2E-05  2.43768E+00 3.9E-07 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03922E+02 5.7E-06  2.02283E+02 6.2E-08 ];
INF_INVV                  (idx, [1:   4]) = [  5.94012E-08 0.00041  2.56386E-06 0.00011 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.30009E-01 0.00010  1.44466E+00 0.00029 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44707E-01 0.00015  3.81132E-01 0.00040 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64663E-02 0.00024  9.03144E-02 0.00088 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35343E-03 0.00280  2.71393E-02 0.00168 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03878E-02 0.00138 -8.48128E-03 0.00716 ];
INF_SCATT5                (idx, [1:   4]) = [  8.56508E-05 0.16608  6.32491E-03 0.00874 ];
INF_SCATT6                (idx, [1:   4]) = [  5.04567E-03 0.00332 -1.62497E-02 0.00262 ];
INF_SCATT7                (idx, [1:   4]) = [  7.36269E-04 0.02085  4.49867E-04 0.08139 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.30048E-01 0.00010  1.44466E+00 0.00029 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44708E-01 0.00015  3.81132E-01 0.00040 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64665E-02 0.00024  9.03144E-02 0.00088 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35353E-03 0.00280  2.71393E-02 0.00168 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03879E-02 0.00139 -8.48128E-03 0.00716 ];
INF_SCATTP5               (idx, [1:   4]) = [  8.56556E-05 0.16600  6.32491E-03 0.00874 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.04589E-03 0.00332 -1.62497E-02 0.00262 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.36605E-04 0.02081  4.49867E-04 0.08139 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13576E-01 0.00028  9.69586E-01 0.00025 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56073E+00 0.00028  3.43790E-01 0.00025 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.38681E-03 0.00039  5.80881E-02 0.00049 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69695E-02 0.00022  5.91706E-02 0.00065 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11468E-01 1.0E-04  1.85415E-02 0.00032  1.03753E-03 0.00390  1.44362E+00 0.00029 ];
INF_S1                    (idx, [1:   8]) = [  2.39284E-01 0.00015  5.42275E-03 0.00102  4.47450E-04 0.00632  3.80685E-01 0.00040 ];
INF_S2                    (idx, [1:   8]) = [  9.80571E-02 0.00024 -1.59078E-03 0.00241  2.43168E-04 0.00944  9.00713E-02 0.00089 ];
INF_S3                    (idx, [1:   8]) = [  9.25843E-03 0.00208 -1.90500E-03 0.00165  8.67187E-05 0.01955  2.70526E-02 0.00168 ];
INF_S4                    (idx, [1:   8]) = [ -9.74724E-03 0.00141 -6.40560E-04 0.00496 -5.02128E-07 1.00000 -8.48078E-03 0.00716 ];
INF_S5                    (idx, [1:   8]) = [  7.22518E-05 0.19303  1.33990E-05 0.19084 -3.85496E-05 0.03392  6.36346E-03 0.00862 ];
INF_S6                    (idx, [1:   8]) = [  5.19410E-03 0.00320 -1.48427E-04 0.01831 -4.55333E-05 0.02660 -1.62041E-02 0.00260 ];
INF_S7                    (idx, [1:   8]) = [  9.09349E-04 0.01733 -1.73080E-04 0.01248 -4.10481E-05 0.03220  4.90915E-04 0.07428 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11507E-01 1.0E-04  1.85415E-02 0.00032  1.03753E-03 0.00390  1.44362E+00 0.00029 ];
INF_SP1                   (idx, [1:   8]) = [  2.39285E-01 0.00015  5.42275E-03 0.00102  4.47450E-04 0.00632  3.80685E-01 0.00040 ];
INF_SP2                   (idx, [1:   8]) = [  9.80572E-02 0.00024 -1.59078E-03 0.00241  2.43168E-04 0.00944  9.00713E-02 0.00089 ];
INF_SP3                   (idx, [1:   8]) = [  9.25853E-03 0.00208 -1.90500E-03 0.00165  8.67187E-05 0.01955  2.70526E-02 0.00168 ];
INF_SP4                   (idx, [1:   8]) = [ -9.74734E-03 0.00142 -6.40560E-04 0.00496 -5.02128E-07 1.00000 -8.48078E-03 0.00716 ];
INF_SP5                   (idx, [1:   8]) = [  7.22566E-05 0.19294  1.33990E-05 0.19084 -3.85496E-05 0.03392  6.36346E-03 0.00862 ];
INF_SP6                   (idx, [1:   8]) = [  5.19432E-03 0.00320 -1.48427E-04 0.01831 -4.55333E-05 0.02660 -1.62041E-02 0.00260 ];
INF_SP7                   (idx, [1:   8]) = [  9.09684E-04 0.01729 -1.73080E-04 0.01248 -4.10481E-05 0.03220  4.90915E-04 0.07428 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32859E-01 0.00079  1.08217E+00 0.00346 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34374E-01 0.00100  1.14983E+00 0.00486 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34389E-01 0.00086  1.14871E+00 0.00596 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29879E-01 0.00114  9.69883E-01 0.00445 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43150E+00 0.00079  3.08113E-01 0.00350 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42226E+00 0.00100  2.90064E-01 0.00493 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42216E+00 0.00086  2.90428E-01 0.00599 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45008E+00 0.00115  3.43847E-01 0.00442 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.19126E-03 0.00801  2.07398E-04 0.04797  1.11969E-03 0.01974  1.13246E-03 0.02031  3.30402E-03 0.01217  1.06965E-03 0.02163  3.58029E-04 0.03515 ];
LAMBDA                    (idx, [1:  14]) = [  8.12508E-01 0.01851  1.24907E-02 2.6E-06  3.16551E-02 0.00030  1.10164E-01 0.00037  3.20566E-01 0.00031  1.34519E+00 0.00023  8.92310E+00 0.00210 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0002' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:56:06 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:12:43 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595300166 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00108E+00  1.00062E+00  9.99416E-01  9.94534E-01  1.00435E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12792E-02 0.00097  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88721E-01 1.1E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.97294E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.97713E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.71790E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.74562E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.74475E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.49557E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.46018E-01 0.00102  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000773 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00015E+04 0.00074 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00015E+04 0.00074 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  8.11801E+01 ;
RUNNING_TIME              (idx, 1)        =  1.66155E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.55900E-01  4.55900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.75667E-02  9.61667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.59965E+01  3.52577E+00  2.69583E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.23933E-01  3.12333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  6.08333E-03  7.00001E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.66153E+01  1.30191E+02 ];
CPU_USAGE                 (idx, 1)        = 4.88582 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99965E+00 0.00016 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.57696E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.81985E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.84307E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.45097E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.67428E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.17019E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.14555E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.72602E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.71732E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.04387E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  9.25458E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.83115E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  1.79186E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  2.36075E+07 ;
SR90_ACTIVITY             (idx, 1)        =  6.36780E+11 ;
TE132_ACTIVITY            (idx, 1)        =  5.63736E+14 ;
I131_ACTIVITY             (idx, 1)        =  2.65472E+14 ;
I132_ACTIVITY             (idx, 1)        =  5.66777E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.98099E+09 ;
CS137_ACTIVITY            (idx, 1)        =  6.73767E+11 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.75733E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.64002E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  7.04134E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.13931E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.22207E+12 0.00047  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 2 ;
BURNUP                     (idx, [1:  2])  = [  5.00000E-01  5.00043E-01 ];
BURN_DAYS                 (idx, 1)        =  1.25000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.38961E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  1.26240E+16 0.00061  9.11326E-01 0.00020 ];
U238_FISS                 (idx, [1:   4]) = [  8.90020E+14 0.00263  6.42451E-02 0.00250 ];
PU239_FISS                (idx, [1:   4]) = [  3.35823E+14 0.00414  2.42447E-02 0.00414 ];
PU240_FISS                (idx, [1:   4]) = [  2.60505E+10 0.49862  1.86961E-06 0.49859 ];
PU241_FISS                (idx, [1:   4]) = [  1.93437E+11 0.18987  1.39939E-05 0.19009 ];
U235_CAPT                 (idx, [1:   4]) = [  2.69485E+15 0.00156  1.48594E-01 0.00144 ];
U238_CAPT                 (idx, [1:   4]) = [  8.47093E+15 0.00097  4.67071E-01 0.00065 ];
PU239_CAPT                (idx, [1:   4]) = [  1.89705E+14 0.00552  1.04606E-02 0.00550 ];
PU240_CAPT                (idx, [1:   4]) = [  9.00097E+12 0.02629  4.96318E-04 0.02632 ];
PU241_CAPT                (idx, [1:   4]) = [  5.81257E+10 0.33069  3.19590E-06 0.33067 ];
XE135_CAPT                (idx, [1:   4]) = [  7.33122E+14 0.00285  4.04276E-02 0.00284 ];
SM149_CAPT                (idx, [1:   4]) = [  1.25866E+14 0.00683  6.93959E-03 0.00677 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000773 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.29163E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000773 5.00729E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2810553 2.81430E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2146935 2.14970E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 43285 4.32971E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000773 5.00729E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.23986E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.42483E+16 1.2E-05  3.42483E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38574E+16 1.4E-06  1.38574E+16 1.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.81393E+16 0.00046  1.24380E+16 0.00046  5.70122E+15 0.00101 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.19966E+16 0.00026  2.62954E+16 0.00022  5.70122E+15 0.00101 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.22207E+16 0.00047  3.22207E+16 0.00047  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.57568E+18 0.00046  4.21987E+17 0.00045  1.15369E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.79050E+14 0.00485 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.22757E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.20824E+18 0.00059 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12456E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12456E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.66023E+00 0.00042 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.63931E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.83659E-01 0.00029 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23624E+00 0.00032 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94430E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96894E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.07187E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06259E+00 0.00052 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.47148E+00 1.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02711E+02 1.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06235E+00 0.00054  1.05524E+00 0.00052  7.35032E-03 0.00836 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06270E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06305E+00 0.00047 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06270E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.07198E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75429E+01 0.00018 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75447E+01 8.0E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.82308E-07 0.00314 ];
IMP_EALF                  (idx, [1:   2]) = [  4.80466E-07 0.00140 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.16113E-01 0.00277 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.16033E-01 0.00118 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.61035E-03 0.00561  1.91572E-04 0.03203  1.04804E-03 0.01325  1.03140E-03 0.01437  3.03167E-03 0.00789  9.84378E-04 0.01440  3.23283E-04 0.02341 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.05854E-01 0.01227  1.09168E-02 0.01700  3.16188E-02 0.00023  1.10244E-01 0.00030  3.20718E-01 0.00021  1.34510E+00 0.00017  8.59639E+00 0.00882 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.02036E-03 0.00786  2.05359E-04 0.04747  1.08794E-03 0.01967  1.10550E-03 0.02089  3.23247E-03 0.01168  1.03696E-03 0.02142  3.52141E-04 0.03483 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.17154E-01 0.01854  1.24906E-02 4.6E-06  3.16123E-02 0.00034  1.10282E-01 0.00043  3.20783E-01 0.00032  1.34495E+00 0.00025  8.92021E+00 0.00212 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.88219E-05 0.00117  2.88111E-05 0.00116  3.03253E-05 0.01150 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.06136E-05 0.00098  3.06022E-05 0.00097  3.22030E-05 0.01143 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.89769E-03 0.00852  1.91884E-04 0.05133  1.10437E-03 0.01923  1.07171E-03 0.02198  3.15962E-03 0.01259  1.03914E-03 0.02048  3.30969E-04 0.03785 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.01387E-01 0.02001  1.24906E-02 6.7E-06  3.16186E-02 0.00039  1.10217E-01 0.00048  3.20645E-01 0.00034  1.34510E+00 0.00028  8.90813E+00 0.00256 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.88392E-05 0.00271  2.88259E-05 0.00272  2.95689E-05 0.02944 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.06325E-05 0.00265  3.06181E-05 0.00266  3.14423E-05 0.02964 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.03781E-03 0.02834  1.71160E-04 0.15492  1.17311E-03 0.07029  1.01911E-03 0.06672  3.18824E-03 0.04063  1.12113E-03 0.06801  3.65052E-04 0.12227 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.05496E-01 0.05878  1.24905E-02 1.9E-05  3.15603E-02 0.00108  1.10316E-01 0.00128  3.20306E-01 0.00098  1.34427E+00 0.00065  8.85132E+00 0.00500 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.00719E-03 0.02780  1.72532E-04 0.16013  1.16245E-03 0.06693  1.01298E-03 0.06473  3.19186E-03 0.03968  1.10671E-03 0.06640  3.60646E-04 0.11875 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.09150E-01 0.05770  1.24905E-02 1.9E-05  3.15626E-02 0.00106  1.10306E-01 0.00126  3.20252E-01 0.00096  1.34422E+00 0.00065  8.84845E+00 0.00494 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.44832E+02 0.02853 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.88285E-05 0.00077 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.06211E-05 0.00050 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.06543E-03 0.00531 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.45212E+02 0.00545 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.50392E-07 0.00059 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87600E-06 0.00042  2.87585E-06 0.00042  2.89391E-06 0.00523 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.34638E-05 0.00075  4.34903E-05 0.00075  3.98233E-05 0.00837 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.81218E-01 0.00029  6.80805E-01 0.00029  7.55169E-01 0.00879 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02468E+01 0.01310 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.74475E+01 0.00043  3.62276E+01 0.00044 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.23405E+04 0.00368  2.90235E+05 0.00156  6.02772E+05 0.00096  6.48938E+05 0.00076  5.98473E+05 0.00058  6.41291E+05 0.00064  4.35399E+05 0.00064  3.85706E+05 0.00068  2.94970E+05 0.00089  2.40900E+05 0.00083  2.07573E+05 0.00073  1.87202E+05 0.00082  1.73152E+05 0.00096  1.64590E+05 0.00078  1.60152E+05 0.00081  1.38425E+05 0.00089  1.36723E+05 0.00082  1.35482E+05 0.00112  1.33075E+05 0.00087  2.60016E+05 0.00062  2.50484E+05 0.00073  1.81187E+05 0.00082  1.17455E+05 0.00100  1.35791E+05 0.00090  1.28302E+05 0.00086  1.16671E+05 0.00103  1.91930E+05 0.00074  4.39450E+04 0.00130  5.51708E+04 0.00108  4.99978E+04 0.00166  2.91172E+04 0.00181  5.06564E+04 0.00160  3.44219E+04 0.00191  2.92969E+04 0.00176  5.58641E+03 0.00359  5.56048E+03 0.00271  5.66222E+03 0.00427  5.86585E+03 0.00348  5.80852E+03 0.00328  5.76840E+03 0.00390  5.93027E+03 0.00323  5.57818E+03 0.00381  1.05089E+04 0.00285  1.68331E+04 0.00215  2.13872E+04 0.00161  5.65179E+04 0.00128  5.97589E+04 0.00095  6.57361E+04 0.00133  4.53802E+04 0.00149  3.42734E+04 0.00149  2.65071E+04 0.00175  3.16039E+04 0.00149  6.08704E+04 0.00145  8.32585E+04 0.00113  1.61553E+05 0.00100  2.49573E+05 0.00107  3.73300E+05 0.00097  2.38569E+05 0.00110  1.70921E+05 0.00106  1.22778E+05 0.00097  1.10147E+05 0.00115  1.08578E+05 0.00120  9.06358E+04 0.00124  6.12278E+04 0.00132  5.65766E+04 0.00131  5.02895E+04 0.00109  4.25256E+04 0.00109  3.34947E+04 0.00128  2.23868E+04 0.00174  7.89766E+03 0.00238 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.07233E+00 0.00056 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.20390E+18 0.00059  3.71799E+17 0.00076 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38438E-01 9.1E-05  1.50625E+00 0.00032 ];
INF_CAPT                  (idx, [1:   4]) = [  6.31962E-03 0.00076  2.83262E-02 0.00030 ];
INF_ABS                   (idx, [1:   4]) = [  8.42996E-03 0.00059  5.87683E-02 0.00057 ];
INF_FISS                  (idx, [1:   4]) = [  2.11034E-03 0.00049  3.04421E-02 0.00082 ];
INF_NSF                   (idx, [1:   4]) = [  5.42870E-03 0.00049  7.45472E-02 0.00083 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.57243E+00 6.5E-05  2.44882E+00 6.3E-06 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03972E+02 6.1E-06  2.02428E+02 9.9E-07 ];
INF_INVV                  (idx, [1:   4]) = [  5.93627E-08 0.00034  2.56425E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.30006E-01 9.0E-05  1.44751E+00 0.00035 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44701E-01 0.00020  3.81967E-01 0.00046 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64210E-02 0.00038  9.05595E-02 0.00090 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34739E-03 0.00395  2.71898E-02 0.00204 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03859E-02 0.00158 -8.44174E-03 0.00712 ];
INF_SCATT5                (idx, [1:   4]) = [  1.26709E-04 0.11046  6.40234E-03 0.00823 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08228E-03 0.00352 -1.63057E-02 0.00295 ];
INF_SCATT7                (idx, [1:   4]) = [  7.47056E-04 0.02089  4.08272E-04 0.09885 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.30045E-01 9.0E-05  1.44751E+00 0.00035 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44702E-01 0.00020  3.81967E-01 0.00046 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64210E-02 0.00038  9.05595E-02 0.00090 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34726E-03 0.00395  2.71898E-02 0.00204 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03859E-02 0.00158 -8.44174E-03 0.00712 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.26787E-04 0.11010  6.40234E-03 0.00823 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08242E-03 0.00351 -1.63057E-02 0.00295 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.47245E-04 0.02088  4.08272E-04 0.09885 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13594E-01 0.00027  9.72205E-01 0.00032 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56060E+00 0.00027  3.42864E-01 0.00032 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.39093E-03 0.00059  5.87683E-02 0.00057 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69805E-02 0.00024  5.97931E-02 0.00071 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11457E-01 8.9E-05  1.85484E-02 0.00035  1.05880E-03 0.00266  1.44645E+00 0.00035 ];
INF_S1                    (idx, [1:   8]) = [  2.39276E-01 0.00020  5.42542E-03 0.00109  4.52531E-04 0.00637  3.81514E-01 0.00046 ];
INF_S2                    (idx, [1:   8]) = [  9.80177E-02 0.00037 -1.59666E-03 0.00250  2.50263E-04 0.01032  9.03093E-02 0.00090 ];
INF_S3                    (idx, [1:   8]) = [  9.25137E-03 0.00296 -1.90398E-03 0.00201  8.89374E-05 0.02356  2.71009E-02 0.00205 ];
INF_S4                    (idx, [1:   8]) = [ -9.74967E-03 0.00167 -6.36218E-04 0.00508  1.14339E-06 1.00000 -8.44288E-03 0.00720 ];
INF_S5                    (idx, [1:   8]) = [  1.09418E-04 0.12668  1.72910E-05 0.16630 -3.65413E-05 0.03612  6.43888E-03 0.00823 ];
INF_S6                    (idx, [1:   8]) = [  5.22892E-03 0.00334 -1.46642E-04 0.01670 -4.57206E-05 0.02570 -1.62600E-02 0.00297 ];
INF_S7                    (idx, [1:   8]) = [  9.21440E-04 0.01675 -1.74384E-04 0.01339 -4.18501E-05 0.03223  4.50122E-04 0.08911 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11496E-01 9.0E-05  1.85484E-02 0.00035  1.05880E-03 0.00266  1.44645E+00 0.00035 ];
INF_SP1                   (idx, [1:   8]) = [  2.39276E-01 0.00020  5.42542E-03 0.00109  4.52531E-04 0.00637  3.81514E-01 0.00046 ];
INF_SP2                   (idx, [1:   8]) = [  9.80176E-02 0.00037 -1.59666E-03 0.00250  2.50263E-04 0.01032  9.03093E-02 0.00090 ];
INF_SP3                   (idx, [1:   8]) = [  9.25125E-03 0.00296 -1.90398E-03 0.00201  8.89374E-05 0.02356  2.71009E-02 0.00205 ];
INF_SP4                   (idx, [1:   8]) = [ -9.74970E-03 0.00167 -6.36218E-04 0.00508  1.14339E-06 1.00000 -8.44288E-03 0.00720 ];
INF_SP5                   (idx, [1:   8]) = [  1.09496E-04 0.12623  1.72910E-05 0.16630 -3.65413E-05 0.03612  6.43888E-03 0.00823 ];
INF_SP6                   (idx, [1:   8]) = [  5.22906E-03 0.00333 -1.46642E-04 0.01670 -4.57206E-05 0.02570 -1.62600E-02 0.00297 ];
INF_SP7                   (idx, [1:   8]) = [  9.21629E-04 0.01674 -1.74384E-04 0.01339 -4.18501E-05 0.03223  4.50122E-04 0.08911 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32913E-01 0.00068  1.07183E+00 0.00650 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34233E-01 0.00078  1.14662E+00 0.00831 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34646E-01 0.00095  1.13194E+00 0.00743 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29925E-01 0.00097  9.59084E-01 0.00605 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43116E+00 0.00068  3.11307E-01 0.00643 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42311E+00 0.00078  2.91190E-01 0.00830 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42061E+00 0.00095  2.94873E-01 0.00748 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.44978E+00 0.00097  3.47857E-01 0.00600 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.02036E-03 0.00786  2.05359E-04 0.04747  1.08794E-03 0.01967  1.10550E-03 0.02089  3.23247E-03 0.01168  1.03696E-03 0.02142  3.52141E-04 0.03483 ];
LAMBDA                    (idx, [1:  14]) = [  8.17154E-01 0.01854  1.24906E-02 4.6E-06  3.16123E-02 0.00034  1.10282E-01 0.00043  3.20783E-01 0.00032  1.34495E+00 0.00025  8.92021E+00 0.00212 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0002' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:56:06 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:19:00 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595300166 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.84716E-01  1.02537E+00  9.86289E-01  9.96265E-01  1.00736E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13066E-02 0.00102  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88693E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.97707E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.98128E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.71390E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.71858E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.71773E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.40274E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.43340E-01 0.00109  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000516 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00010E+04 0.00077 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00010E+04 0.00077 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.12618E+02 ;
RUNNING_TIME              (idx, 1)        =  2.29103E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.55900E-01  4.55900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  5.84667E-02  1.06167E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  2.22072E+01  3.54982E+00  2.66087E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.86567E-01  3.22833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  9.30000E-03  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  2.29101E+01  1.29542E+02 ];
CPU_USAGE                 (idx, 1)        = 4.91559 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99974E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.65000E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.02634E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.85836E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.51119E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.70269E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.18911E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.32363E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.73943E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  3.59318E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.52060E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.06468E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.03879E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  2.52850E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  2.81673E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.26063E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.05438E+14 ;
I131_ACTIVITY             (idx, 1)        =  3.60495E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.10902E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.77717E+10 ;
CS137_ACTIVITY            (idx, 1)        =  1.34860E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.95518E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.61833E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.56992E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.17821E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.24856E+12 0.00048  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 3 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E+00  1.00008E+00 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.37484E-01 0.00113 ];
U235_FISS                 (idx, [1:   4]) = [  1.22027E+16 0.00060  8.80866E-01 0.00024 ];
U238_FISS                 (idx, [1:   4]) = [  9.00412E+14 0.00279  6.49884E-02 0.00264 ];
PU239_FISS                (idx, [1:   4]) = [  7.45694E+14 0.00287  5.38237E-02 0.00275 ];
PU240_FISS                (idx, [1:   4]) = [  8.37629E+10 0.27404  6.06537E-06 0.27404 ];
PU241_FISS                (idx, [1:   4]) = [  1.63153E+12 0.06209  1.17836E-04 0.06213 ];
U235_CAPT                 (idx, [1:   4]) = [  2.61243E+15 0.00160  1.41977E-01 0.00153 ];
U238_CAPT                 (idx, [1:   4]) = [  8.49017E+15 0.00102  4.61370E-01 0.00067 ];
PU239_CAPT                (idx, [1:   4]) = [  4.18617E+14 0.00411  2.27538E-02 0.00416 ];
PU240_CAPT                (idx, [1:   4]) = [  3.93549E+13 0.01322  2.13880E-03 0.01321 ];
PU241_CAPT                (idx, [1:   4]) = [  5.77232E+11 0.11512  3.14220E-05 0.11525 ];
XE135_CAPT                (idx, [1:   4]) = [  7.34981E+14 0.00299  3.99446E-02 0.00296 ];
SM149_CAPT                (idx, [1:   4]) = [  1.50271E+14 0.00688  8.16710E-03 0.00688 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000516 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.35827E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000516 5.00736E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2828332 2.83220E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2129333 2.13230E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 42851 4.28520E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000516 5.00736E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.72529E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.44020E+16 1.2E-05  3.44020E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38457E+16 1.6E-06  1.38457E+16 1.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.83924E+16 0.00045  1.27291E+16 0.00045  5.66337E+15 0.00099 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.22381E+16 0.00026  2.65748E+16 0.00022  5.66337E+15 0.00099 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.24856E+16 0.00048  3.24856E+16 0.00048  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.58286E+18 0.00045  4.24169E+17 0.00044  1.15869E+18 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.78420E+14 0.00484 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.25166E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.20935E+18 0.00057 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12398E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12398E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.65392E+00 0.00042 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.66564E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.82043E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23597E+00 0.00033 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94436E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96977E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.06871E+00 0.00053 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.05955E+00 0.00053 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.48467E+00 1.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02882E+02 1.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.05963E+00 0.00054  1.05238E+00 0.00054  7.16772E-03 0.00830 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.05958E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.05912E+00 0.00048 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.05958E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.06874E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75151E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75150E+01 8.0E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.95954E-07 0.00322 ];
IMP_EALF                  (idx, [1:   2]) = [  4.94972E-07 0.00139 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.18348E-01 0.00284 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.18845E-01 0.00111 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.49886E-03 0.00527  1.86362E-04 0.03347  1.03226E-03 0.01295  1.01074E-03 0.01375  2.99695E-03 0.00790  9.60689E-04 0.01466  3.11862E-04 0.02451 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.98736E-01 0.01294  1.04172E-02 0.01997  3.15864E-02 0.00026  1.10266E-01 0.00032  3.20861E-01 0.00024  1.34487E+00 0.00017  8.56828E+00 0.00934 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.84734E-03 0.00801  2.04171E-04 0.04514  1.09792E-03 0.02057  1.06282E-03 0.02158  3.15073E-03 0.01216  9.94325E-04 0.02068  3.37367E-04 0.03669 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.09440E-01 0.01882  1.24906E-02 4.9E-06  3.15887E-02 0.00036  1.10336E-01 0.00044  3.20883E-01 0.00035  1.34457E+00 0.00024  8.94272E+00 0.00233 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.83834E-05 0.00124  2.83705E-05 0.00125  3.02667E-05 0.01171 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.00707E-05 0.00107  3.00570E-05 0.00108  3.20669E-05 0.01169 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.78986E-03 0.00852  1.94590E-04 0.05164  1.07299E-03 0.02047  1.05218E-03 0.02160  3.14834E-03 0.01298  9.95169E-04 0.02263  3.26589E-04 0.03823 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.01487E-01 0.02050  1.24905E-02 7.7E-06  3.15828E-02 0.00040  1.10313E-01 0.00052  3.20817E-01 0.00035  1.34510E+00 0.00029  8.94810E+00 0.00282 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.84036E-05 0.00268  2.83938E-05 0.00267  3.00663E-05 0.02848 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.00923E-05 0.00261  3.00821E-05 0.00260  3.18336E-05 0.02841 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.82599E-03 0.02722  1.48618E-04 0.16258  1.12124E-03 0.06864  1.12299E-03 0.07077  3.10291E-03 0.03922  1.01766E-03 0.07196  3.12576E-04 0.12161 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.58987E-01 0.06108  1.24905E-02 1.4E-05  3.15143E-02 0.00114  1.10285E-01 0.00123  3.21428E-01 0.00108  1.34514E+00 0.00064  8.91715E+00 0.00602 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.83750E-03 0.02667  1.49938E-04 0.15774  1.11805E-03 0.06530  1.12525E-03 0.06825  3.10643E-03 0.03868  9.98997E-04 0.06849  3.38830E-04 0.12080 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.84009E-01 0.06205  1.24905E-02 1.4E-05  3.15140E-02 0.00114  1.10282E-01 0.00123  3.21368E-01 0.00105  1.34521E+00 0.00064  8.91674E+00 0.00602 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.41368E+02 0.02740 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.83746E-05 0.00076 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.00618E-05 0.00048 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.82765E-03 0.00447 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.40693E+02 0.00453 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.42250E-07 0.00061 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87366E-06 0.00045  2.87343E-06 0.00045  2.90535E-06 0.00480 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.28223E-05 0.00077  4.28412E-05 0.00078  4.01405E-05 0.00864 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.79610E-01 0.00031  6.79236E-01 0.00032  7.47944E-01 0.00876 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03977E+01 0.01396 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.71773E+01 0.00046  3.59402E+01 0.00048 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.27492E+04 0.00274  2.91937E+05 0.00160  6.04016E+05 0.00083  6.50067E+05 0.00069  5.98337E+05 0.00070  6.41771E+05 0.00080  4.35913E+05 0.00072  3.85494E+05 0.00076  2.95436E+05 0.00068  2.40758E+05 0.00067  2.07737E+05 0.00086  1.87348E+05 0.00081  1.72956E+05 0.00089  1.64265E+05 0.00079  1.60122E+05 0.00105  1.38465E+05 0.00083  1.36626E+05 0.00097  1.35400E+05 0.00095  1.33224E+05 0.00112  2.59645E+05 0.00051  2.50674E+05 0.00062  1.81229E+05 0.00068  1.17559E+05 0.00065  1.35671E+05 0.00101  1.28626E+05 0.00109  1.16631E+05 0.00094  1.92082E+05 0.00074  4.38989E+04 0.00146  5.50808E+04 0.00132  4.99148E+04 0.00159  2.91071E+04 0.00188  5.05138E+04 0.00152  3.43635E+04 0.00176  2.93451E+04 0.00218  5.59413E+03 0.00379  5.50648E+03 0.00378  5.63665E+03 0.00361  5.81904E+03 0.00384  5.74732E+03 0.00407  5.66952E+03 0.00380  5.90125E+03 0.00359  5.55897E+03 0.00366  1.05120E+04 0.00288  1.67384E+04 0.00229  2.13106E+04 0.00191  5.64800E+04 0.00155  5.95184E+04 0.00150  6.53681E+04 0.00150  4.49501E+04 0.00184  3.36640E+04 0.00158  2.59762E+04 0.00116  3.07118E+04 0.00139  5.94164E+04 0.00115  8.14802E+04 0.00092  1.58188E+05 0.00108  2.44949E+05 0.00093  3.66309E+05 0.00088  2.34415E+05 0.00097  1.67776E+05 0.00096  1.20754E+05 0.00096  1.08505E+05 0.00110  1.06745E+05 0.00114  8.91318E+04 0.00091  6.02803E+04 0.00129  5.56074E+04 0.00106  4.94197E+04 0.00133  4.18594E+04 0.00113  3.29140E+04 0.00111  2.20619E+04 0.00156  7.78997E+03 0.00186 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.06827E+00 0.00047 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.21454E+18 0.00051  3.68352E+17 0.00085 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38227E-01 0.00012  1.50855E+00 0.00038 ];
INF_CAPT                  (idx, [1:   4]) = [  6.36083E-03 0.00057  2.89610E-02 0.00034 ];
INF_ABS                   (idx, [1:   4]) = [  8.45912E-03 0.00046  5.96354E-02 0.00062 ];
INF_FISS                  (idx, [1:   4]) = [  2.09829E-03 0.00054  3.06744E-02 0.00089 ];
INF_NSF                   (idx, [1:   4]) = [  5.40860E-03 0.00051  7.55726E-02 0.00090 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.57763E+00 5.7E-05  2.46370E+00 1.6E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04036E+02 6.2E-06  2.02622E+02 2.5E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.92589E-08 0.00041  2.56463E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29756E-01 0.00012  1.44892E+00 0.00042 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44529E-01 0.00022  3.82386E-01 0.00047 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64267E-02 0.00029  9.05355E-02 0.00103 ];
INF_SCATT3                (idx, [1:   4]) = [  7.37573E-03 0.00301  2.72297E-02 0.00248 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03205E-02 0.00157 -8.40721E-03 0.00594 ];
INF_SCATT5                (idx, [1:   4]) = [  1.40204E-04 0.10937  6.38955E-03 0.00855 ];
INF_SCATT6                (idx, [1:   4]) = [  5.06844E-03 0.00303 -1.63781E-02 0.00321 ];
INF_SCATT7                (idx, [1:   4]) = [  7.16644E-04 0.01686  3.25400E-04 0.14065 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29795E-01 0.00012  1.44892E+00 0.00042 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44530E-01 0.00022  3.82386E-01 0.00047 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64270E-02 0.00029  9.05355E-02 0.00103 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.37583E-03 0.00301  2.72297E-02 0.00248 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03205E-02 0.00157 -8.40721E-03 0.00594 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.40260E-04 0.10928  6.38955E-03 0.00855 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.06830E-03 0.00304 -1.63781E-02 0.00321 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.16774E-04 0.01684  3.25400E-04 0.14065 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13404E-01 0.00034  9.74384E-01 0.00038 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56199E+00 0.00034  3.42098E-01 0.00038 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.41975E-03 0.00046  5.96354E-02 0.00062 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69598E-02 0.00026  6.06974E-02 0.00069 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11267E-01 0.00012  1.84888E-02 0.00043  1.06197E-03 0.00425  1.44785E+00 0.00042 ];
INF_S1                    (idx, [1:   8]) = [  2.39118E-01 0.00021  5.41087E-03 0.00097  4.50986E-04 0.00651  3.81935E-01 0.00047 ];
INF_S2                    (idx, [1:   8]) = [  9.80171E-02 0.00028 -1.59038E-03 0.00241  2.50579E-04 0.00828  9.02850E-02 0.00103 ];
INF_S3                    (idx, [1:   8]) = [  9.27598E-03 0.00225 -1.90025E-03 0.00188  9.30964E-05 0.02256  2.71366E-02 0.00251 ];
INF_S4                    (idx, [1:   8]) = [ -9.68599E-03 0.00161 -6.34516E-04 0.00472  2.76922E-06 0.55635 -8.40998E-03 0.00594 ];
INF_S5                    (idx, [1:   8]) = [  1.23454E-04 0.11429  1.67496E-05 0.15426 -3.54391E-05 0.04324  6.42499E-03 0.00859 ];
INF_S6                    (idx, [1:   8]) = [  5.21255E-03 0.00307 -1.44113E-04 0.01613 -4.55153E-05 0.02951 -1.63326E-02 0.00323 ];
INF_S7                    (idx, [1:   8]) = [  8.93726E-04 0.01349 -1.77082E-04 0.00976 -4.26319E-05 0.02874  3.68032E-04 0.12542 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11306E-01 0.00012  1.84888E-02 0.00043  1.06197E-03 0.00425  1.44785E+00 0.00042 ];
INF_SP1                   (idx, [1:   8]) = [  2.39119E-01 0.00021  5.41087E-03 0.00097  4.50986E-04 0.00651  3.81935E-01 0.00047 ];
INF_SP2                   (idx, [1:   8]) = [  9.80174E-02 0.00028 -1.59038E-03 0.00241  2.50579E-04 0.00828  9.02850E-02 0.00103 ];
INF_SP3                   (idx, [1:   8]) = [  9.27608E-03 0.00225 -1.90025E-03 0.00188  9.30964E-05 0.02256  2.71366E-02 0.00251 ];
INF_SP4                   (idx, [1:   8]) = [ -9.68595E-03 0.00162 -6.34516E-04 0.00472  2.76922E-06 0.55635 -8.40998E-03 0.00594 ];
INF_SP5                   (idx, [1:   8]) = [  1.23511E-04 0.11414  1.67496E-05 0.15426 -3.54391E-05 0.04324  6.42499E-03 0.00859 ];
INF_SP6                   (idx, [1:   8]) = [  5.21242E-03 0.00308 -1.44113E-04 0.01613 -4.55153E-05 0.02951 -1.63326E-02 0.00323 ];
INF_SP7                   (idx, [1:   8]) = [  8.93856E-04 0.01347 -1.77082E-04 0.00976 -4.26319E-05 0.02874  3.68032E-04 0.12542 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32851E-01 0.00065  1.08330E+00 0.00410 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34483E-01 0.00104  1.14912E+00 0.00638 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34309E-01 0.00090  1.16081E+00 0.00538 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29828E-01 0.00093  9.64470E-01 0.00421 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43155E+00 0.00065  3.07825E-01 0.00410 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42161E+00 0.00104  2.90359E-01 0.00633 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42265E+00 0.00091  2.87355E-01 0.00539 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45039E+00 0.00093  3.45761E-01 0.00423 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.84734E-03 0.00801  2.04171E-04 0.04514  1.09792E-03 0.02057  1.06282E-03 0.02158  3.15073E-03 0.01216  9.94325E-04 0.02068  3.37367E-04 0.03669 ];
LAMBDA                    (idx, [1:  14]) = [  8.09440E-01 0.01882  1.24906E-02 4.9E-06  3.15887E-02 0.00036  1.10336E-01 0.00044  3.20883E-01 0.00035  1.34457E+00 0.00024  8.94272E+00 0.00233 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0002' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:56:06 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:25:13 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595300166 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00588E+00  9.93045E-01  1.00035E+00  9.94938E-01  1.00580E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12905E-02 0.00111  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88709E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.99220E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.99641E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.70654E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.68943E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.68856E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.24154E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.37646E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000992 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00020E+04 0.00084 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00020E+04 0.00084 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.43599E+02 ;
RUNNING_TIME              (idx, 1)        =  2.91145E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.55900E-01  4.55900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  8.00667E-02  1.12833E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  2.83272E+01  3.43488E+00  2.68508E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  2.48633E-01  3.26333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.26000E-02  8.33337E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  2.91144E+01  1.30278E+02 ];
CPU_USAGE                 (idx, 1)        = 4.93222 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00055E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.69080E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.18777E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.85786E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.76636E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.71108E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.19476E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.47667E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.73837E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  4.80616E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.88342E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.35097E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.08948E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  3.45519E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.17448E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.47117E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.15302E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.09457E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.22666E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.64315E+11 ;
CS137_ACTIVITY            (idx, 1)        =  2.69994E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.07416E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.57807E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.44033E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.20475E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.29768E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 4 ;
BURNUP                     (idx, [1:  2])  = [  2.00000E+00  2.00017E+00 ];
BURN_DAYS                 (idx, 1)        =  5.00000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.38526E-01 0.00114 ];
U235_FISS                 (idx, [1:   4]) = [  1.14499E+16 0.00066  8.27883E-01 0.00032 ];
U238_FISS                 (idx, [1:   4]) = [  9.16842E+14 0.00285  6.62801E-02 0.00266 ];
PU239_FISS                (idx, [1:   4]) = [  1.44796E+15 0.00211  1.04692E-01 0.00201 ];
PU240_FISS                (idx, [1:   4]) = [  3.29830E+11 0.14302  2.38087E-05 0.14300 ];
PU241_FISS                (idx, [1:   4]) = [  1.18598E+13 0.02408  8.57624E-04 0.02410 ];
U235_CAPT                 (idx, [1:   4]) = [  2.46824E+15 0.00168  1.30541E-01 0.00161 ];
U238_CAPT                 (idx, [1:   4]) = [  8.51625E+15 0.00101  4.50371E-01 0.00068 ];
PU239_CAPT                (idx, [1:   4]) = [  8.04657E+14 0.00288  4.25579E-02 0.00285 ];
PU240_CAPT                (idx, [1:   4]) = [  1.42908E+14 0.00650  7.55688E-03 0.00643 ];
PU241_CAPT                (idx, [1:   4]) = [  4.15935E+12 0.03641  2.20113E-04 0.03645 ];
XE135_CAPT                (idx, [1:   4]) = [  7.37524E+14 0.00283  3.90069E-02 0.00280 ];
SM149_CAPT                (idx, [1:   4]) = [  1.60342E+14 0.00607  8.48132E-03 0.00610 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000992 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.36088E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000992 5.00736E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2863213 2.86698E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2094514 2.09711E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 43265 4.32738E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000992 5.00736E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 1.39698E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.46652E+16 1.4E-05  3.46652E+16 1.4E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38256E+16 2.1E-06  1.38256E+16 2.1E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.89099E+16 0.00047  1.32505E+16 0.00047  5.65945E+15 0.00106 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.27355E+16 0.00027  2.70760E+16 0.00023  5.65945E+15 0.00106 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.29768E+16 0.00050  3.29768E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.59942E+18 0.00047  4.28040E+17 0.00044  1.17138E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.85433E+14 0.00510 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.30209E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.21802E+18 0.00061 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12281E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12281E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.64127E+00 0.00045 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.70048E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.79491E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23540E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94397E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96931E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.06088E+00 0.00057 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.05170E+00 0.00057 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.50732E+00 1.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03178E+02 2.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.05181E+00 0.00058  1.04481E+00 0.00058  6.88924E-03 0.00888 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.05140E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.05133E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.05140E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.06058E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74656E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74668E+01 8.6E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.21443E-07 0.00359 ];
IMP_EALF                  (idx, [1:   2]) = [  5.19449E-07 0.00150 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.24075E-01 0.00284 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.23559E-01 0.00119 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.34119E-03 0.00601  1.81460E-04 0.03373  1.00938E-03 0.01467  9.97457E-04 0.01408  2.90155E-03 0.00857  9.58802E-04 0.01379  2.92539E-04 0.02555 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.85780E-01 0.01291  1.05669E-02 0.01910  3.15056E-02 0.00032  1.10183E-01 0.00031  3.21197E-01 0.00024  1.34400E+00 0.00020  8.48605E+00 0.01067 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.59485E-03 0.00849  1.86239E-04 0.04849  1.05791E-03 0.02060  1.05441E-03 0.02110  2.99577E-03 0.01275  9.70351E-04 0.02022  3.30174E-04 0.03648 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.11199E-01 0.01910  1.24904E-02 6.6E-06  3.15112E-02 0.00043  1.10150E-01 0.00043  3.21356E-01 0.00038  1.34385E+00 0.00027  8.93071E+00 0.00259 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.78570E-05 0.00132  2.78500E-05 0.00132  2.90615E-05 0.01216 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.92940E-05 0.00110  2.92866E-05 0.00110  3.05661E-05 0.01218 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.54951E-03 0.00914  1.88841E-04 0.05172  1.04752E-03 0.02205  1.02385E-03 0.02156  3.01328E-03 0.01294  9.67369E-04 0.02270  3.08647E-04 0.03845 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.91673E-01 0.02058  1.24905E-02 9.3E-06  3.15181E-02 0.00049  1.10099E-01 0.00049  3.21144E-01 0.00039  1.34395E+00 0.00033  8.93590E+00 0.00285 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.77940E-05 0.00273  2.77919E-05 0.00274  2.77554E-05 0.02939 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.92289E-05 0.00267  2.92267E-05 0.00268  2.91936E-05 0.02940 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.41262E-03 0.02884  1.90015E-04 0.17101  1.00975E-03 0.06850  1.01823E-03 0.06717  2.82595E-03 0.04221  1.00892E-03 0.07567  3.59755E-04 0.13276 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.25293E-01 0.06532  1.24903E-02 2.3E-05  3.14598E-02 0.00129  1.10114E-01 0.00122  3.21647E-01 0.00122  1.34445E+00 0.00072  8.97145E+00 0.00674 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.39515E-03 0.02814  1.77134E-04 0.16200  1.01942E-03 0.06912  1.00328E-03 0.06588  2.82303E-03 0.04075  1.01896E-03 0.07348  3.53316E-04 0.12666 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.30926E-01 0.06380  1.24903E-02 2.3E-05  3.14561E-02 0.00128  1.10104E-01 0.00121  3.21564E-01 0.00120  1.34444E+00 0.00071  8.96875E+00 0.00672 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.31090E+02 0.02871 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.77963E-05 0.00077 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.92312E-05 0.00047 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.43944E-03 0.00517 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.31726E+02 0.00521 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.32290E-07 0.00062 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.86113E-06 0.00044  2.86091E-06 0.00044  2.89292E-06 0.00547 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.20579E-05 0.00081  4.20857E-05 0.00081  3.80508E-05 0.00878 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.77061E-01 0.00032  6.76737E-01 0.00032  7.40401E-01 0.00891 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.01797E+01 0.01356 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.68856E+01 0.00045  3.55522E+01 0.00046 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.40114E+04 0.00334  2.94391E+05 0.00158  6.05620E+05 0.00114  6.50872E+05 0.00071  5.98392E+05 0.00083  6.42165E+05 0.00059  4.36135E+05 0.00073  3.85635E+05 0.00062  2.94980E+05 0.00067  2.41366E+05 0.00084  2.07685E+05 0.00071  1.87346E+05 0.00084  1.73251E+05 0.00084  1.64360E+05 0.00073  1.60062E+05 0.00089  1.38526E+05 0.00087  1.36793E+05 0.00079  1.35399E+05 0.00090  1.33037E+05 0.00101  2.59860E+05 0.00056  2.50995E+05 0.00069  1.81351E+05 0.00060  1.17470E+05 0.00083  1.35884E+05 0.00076  1.28478E+05 0.00072  1.16495E+05 0.00107  1.91919E+05 0.00053  4.37938E+04 0.00164  5.50702E+04 0.00169  4.99541E+04 0.00152  2.90362E+04 0.00191  5.04368E+04 0.00127  3.42802E+04 0.00160  2.92178E+04 0.00212  5.56008E+03 0.00322  5.47377E+03 0.00351  5.50448E+03 0.00291  5.57423E+03 0.00250  5.52657E+03 0.00279  5.53907E+03 0.00367  5.84785E+03 0.00420  5.47921E+03 0.00295  1.04317E+04 0.00319  1.66811E+04 0.00216  2.11888E+04 0.00213  5.61075E+04 0.00144  5.89059E+04 0.00131  6.46166E+04 0.00104  4.39239E+04 0.00154  3.26762E+04 0.00147  2.50407E+04 0.00148  2.96834E+04 0.00172  5.72176E+04 0.00105  7.89552E+04 0.00121  1.53829E+05 0.00099  2.39025E+05 0.00114  3.58478E+05 0.00106  2.29232E+05 0.00129  1.64407E+05 0.00105  1.18219E+05 0.00111  1.06258E+05 0.00110  1.04550E+05 0.00135  8.73844E+04 0.00120  5.90868E+04 0.00155  5.45457E+04 0.00134  4.85428E+04 0.00143  4.10167E+04 0.00144  3.23604E+04 0.00142  2.16774E+04 0.00144  7.67368E+03 0.00244 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.06051E+00 0.00054 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.23386E+18 0.00052  3.65598E+17 0.00097 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38119E-01 0.00012  1.51554E+00 0.00040 ];
INF_CAPT                  (idx, [1:   4]) = [  6.46103E-03 0.00065  2.99204E-02 0.00041 ];
INF_ABS                   (idx, [1:   4]) = [  8.53001E-03 0.00052  6.07593E-02 0.00069 ];
INF_FISS                  (idx, [1:   4]) = [  2.06898E-03 0.00054  3.08389E-02 0.00097 ];
INF_NSF                   (idx, [1:   4]) = [  5.35334E-03 0.00053  7.67638E-02 0.00099 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.58743E+00 6.2E-05  2.48918E+00 2.8E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04158E+02 7.8E-06  2.02956E+02 4.5E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.90186E-08 0.00040  2.56714E-06 0.00013 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29588E-01 0.00013  1.45478E+00 0.00044 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44517E-01 0.00021  3.84051E-01 0.00052 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64439E-02 0.00029  9.08973E-02 0.00090 ];
INF_SCATT3                (idx, [1:   4]) = [  7.42316E-03 0.00215  2.73006E-02 0.00168 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03350E-02 0.00137 -8.48027E-03 0.00514 ];
INF_SCATT5                (idx, [1:   4]) = [  1.32789E-04 0.15818  6.45402E-03 0.00856 ];
INF_SCATT6                (idx, [1:   4]) = [  5.06286E-03 0.00385 -1.64163E-02 0.00308 ];
INF_SCATT7                (idx, [1:   4]) = [  7.45451E-04 0.01865  4.00927E-04 0.11402 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29627E-01 0.00013  1.45478E+00 0.00044 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44517E-01 0.00021  3.84051E-01 0.00052 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64440E-02 0.00029  9.08973E-02 0.00090 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.42326E-03 0.00216  2.73006E-02 0.00168 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03350E-02 0.00138 -8.48027E-03 0.00514 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.32743E-04 0.15806  6.45402E-03 0.00856 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.06271E-03 0.00386 -1.64163E-02 0.00308 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.45374E-04 0.01868  4.00927E-04 0.11402 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13110E-01 0.00029  9.79980E-01 0.00034 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56414E+00 0.00029  3.40144E-01 0.00034 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.49067E-03 0.00051  6.07593E-02 0.00069 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69385E-02 0.00016  6.18481E-02 0.00082 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11180E-01 0.00012  1.84072E-02 0.00041  1.08781E-03 0.00343  1.45370E+00 0.00045 ];
INF_S1                    (idx, [1:   8]) = [  2.39137E-01 0.00022  5.37926E-03 0.00114  4.65042E-04 0.00628  3.83586E-01 0.00052 ];
INF_S2                    (idx, [1:   8]) = [  9.80300E-02 0.00029 -1.58612E-03 0.00254  2.55086E-04 0.00942  9.06422E-02 0.00090 ];
INF_S3                    (idx, [1:   8]) = [  9.30975E-03 0.00169 -1.88659E-03 0.00209  9.20580E-05 0.01667  2.72086E-02 0.00169 ];
INF_S4                    (idx, [1:   8]) = [ -9.71000E-03 0.00142 -6.25025E-04 0.00451  1.87361E-06 0.80652 -8.48214E-03 0.00513 ];
INF_S5                    (idx, [1:   8]) = [  1.17892E-04 0.17442  1.48967E-05 0.21051 -3.66218E-05 0.03759  6.49064E-03 0.00849 ];
INF_S6                    (idx, [1:   8]) = [  5.21223E-03 0.00352 -1.49373E-04 0.01805 -4.50168E-05 0.02134 -1.63713E-02 0.00309 ];
INF_S7                    (idx, [1:   8]) = [  9.19183E-04 0.01547 -1.73733E-04 0.01436 -4.15195E-05 0.02467  4.42447E-04 0.10337 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11220E-01 0.00012  1.84072E-02 0.00041  1.08781E-03 0.00343  1.45370E+00 0.00045 ];
INF_SP1                   (idx, [1:   8]) = [  2.39138E-01 0.00022  5.37926E-03 0.00114  4.65042E-04 0.00628  3.83586E-01 0.00052 ];
INF_SP2                   (idx, [1:   8]) = [  9.80301E-02 0.00029 -1.58612E-03 0.00254  2.55086E-04 0.00942  9.06422E-02 0.00090 ];
INF_SP3                   (idx, [1:   8]) = [  9.30985E-03 0.00169 -1.88659E-03 0.00209  9.20580E-05 0.01667  2.72086E-02 0.00169 ];
INF_SP4                   (idx, [1:   8]) = [ -9.70996E-03 0.00142 -6.25025E-04 0.00451  1.87361E-06 0.80652 -8.48214E-03 0.00513 ];
INF_SP5                   (idx, [1:   8]) = [  1.17847E-04 0.17431  1.48967E-05 0.21051 -3.66218E-05 0.03759  6.49064E-03 0.00849 ];
INF_SP6                   (idx, [1:   8]) = [  5.21209E-03 0.00353 -1.49373E-04 0.01805 -4.50168E-05 0.02134 -1.63713E-02 0.00309 ];
INF_SP7                   (idx, [1:   8]) = [  9.19107E-04 0.01549 -1.73733E-04 0.01436 -4.15195E-05 0.02467  4.42447E-04 0.10337 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32441E-01 0.00057  1.10632E+00 0.00768 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33955E-01 0.00092  1.18039E+00 0.00986 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34059E-01 0.00079  1.18253E+00 0.00887 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29379E-01 0.00105  9.82354E-01 0.00694 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43407E+00 0.00057  3.01728E-01 0.00772 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42481E+00 0.00092  2.83062E-01 0.01001 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42416E+00 0.00079  2.82404E-01 0.00871 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45324E+00 0.00105  3.39719E-01 0.00704 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.59485E-03 0.00849  1.86239E-04 0.04849  1.05791E-03 0.02060  1.05441E-03 0.02110  2.99577E-03 0.01275  9.70351E-04 0.02022  3.30174E-04 0.03648 ];
LAMBDA                    (idx, [1:  14]) = [  8.11199E-01 0.01910  1.24904E-02 6.6E-06  3.15112E-02 0.00043  1.10150E-01 0.00043  3.21356E-01 0.00038  1.34385E+00 0.00027  8.93071E+00 0.00259 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0002' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:56:06 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:31:20 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595300166 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00691E+00  9.92158E-01  1.00040E+00  9.98977E-01  1.00155E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.3E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12931E-02 0.00105  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88707E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.00445E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.00867E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.70035E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.66371E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.66285E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.10705E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.33353E-01 0.00112  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000916 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00018E+04 0.00074 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00018E+04 0.00074 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.74177E+02 ;
RUNNING_TIME              (idx, 1)        =  3.52370E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.55900E-01  4.55900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.02800E-01  1.07667E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.43652E+01  3.41197E+00  2.62607E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  3.09867E-01  2.97000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.55833E-02  6.66666E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.52368E+01  1.27365E+02 ];
CPU_USAGE                 (idx, 1)        = 4.94301 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00083E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.71712E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.24871E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.83987E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.28794E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.72180E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.20214E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.52689E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.71963E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  5.80295E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.04022E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.72577E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.14784E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.07718E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.32544E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.63773E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.18636E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.19599E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.27356E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.60233E+11 ;
CS137_ACTIVITY            (idx, 1)        =  4.05123E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.10989E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.53934E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.65851E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.21664E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.35080E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 5 ;
BURNUP                     (idx, [1:  2])  = [  3.00000E+00  3.00026E+00 ];
BURN_DAYS                 (idx, 1)        =  7.50000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.45432E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  1.08253E+16 0.00067  7.84227E-01 0.00036 ];
U238_FISS                 (idx, [1:   4]) = [  9.30045E+14 0.00263  6.73656E-02 0.00244 ];
PU239_FISS                (idx, [1:   4]) = [  2.01001E+15 0.00175  1.45613E-01 0.00166 ];
PU240_FISS                (idx, [1:   4]) = [  8.50051E+11 0.09309  6.16842E-05 0.09332 ];
PU241_FISS                (idx, [1:   4]) = [  3.33927E+13 0.01351  2.41899E-03 0.01351 ];
U235_CAPT                 (idx, [1:   4]) = [  2.34559E+15 0.00172  1.20504E-01 0.00161 ];
U238_CAPT                 (idx, [1:   4]) = [  8.57839E+15 0.00102  4.40693E-01 0.00069 ];
PU239_CAPT                (idx, [1:   4]) = [  1.11808E+15 0.00255  5.74414E-02 0.00247 ];
PU240_CAPT                (idx, [1:   4]) = [  2.78460E+14 0.00484  1.43049E-02 0.00477 ];
PU241_CAPT                (idx, [1:   4]) = [  1.19450E+13 0.02427  6.14056E-04 0.02431 ];
XE135_CAPT                (idx, [1:   4]) = [  7.39713E+14 0.00298  3.80021E-02 0.00291 ];
SM149_CAPT                (idx, [1:   4]) = [  1.64193E+14 0.00600  8.43742E-03 0.00605 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000916 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.56786E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000916 5.00757E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2900537 2.90452E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2057229 2.05989E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 43150 4.31610E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000916 5.00757E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.42613E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.48858E+16 1.6E-05  3.48858E+16 1.6E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38085E+16 2.6E-06  1.38085E+16 2.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.94570E+16 0.00044  1.37833E+16 0.00046  5.67366E+15 0.00103 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.32655E+16 0.00026  2.75918E+16 0.00023  5.67366E+15 0.00103 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.35080E+16 0.00051  3.35080E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.61777E+18 0.00047  4.32628E+17 0.00048  1.18514E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.89267E+14 0.00528 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.35547E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.22904E+18 0.00058 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12165E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12165E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.62951E+00 0.00046 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.71942E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.75880E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23493E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94363E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96988E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.04985E+00 0.00053 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04078E+00 0.00053 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.52640E+00 1.9E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03429E+02 2.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04107E+00 0.00053  1.03416E+00 0.00053  6.62901E-03 0.00885 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.04127E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04125E+00 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.04127E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.05034E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74297E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74274E+01 9.0E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.40293E-07 0.00337 ];
IMP_EALF                  (idx, [1:   2]) = [  5.40364E-07 0.00157 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.27333E-01 0.00275 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.27563E-01 0.00126 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.22344E-03 0.00566  1.74112E-04 0.03327  1.01604E-03 0.01406  9.89481E-04 0.01371  2.82324E-03 0.00840  9.20523E-04 0.01396  3.00046E-04 0.02406 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.99974E-01 0.01263  1.04918E-02 0.01954  3.14397E-02 0.00033  1.10156E-01 0.00033  3.21189E-01 0.00024  1.34233E+00 0.00032  8.66777E+00 0.00867 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.43431E-03 0.00816  1.87268E-04 0.04950  1.06784E-03 0.01980  1.01895E-03 0.02093  2.86725E-03 0.01169  9.78185E-04 0.02016  3.14818E-04 0.03751 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.05835E-01 0.01919  1.24902E-02 8.8E-06  3.14172E-02 0.00049  1.10130E-01 0.00045  3.21126E-01 0.00036  1.34290E+00 0.00039  8.93414E+00 0.00265 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.75532E-05 0.00123  2.75410E-05 0.00124  2.94743E-05 0.01259 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.86808E-05 0.00112  2.86681E-05 0.00112  3.06775E-05 0.01257 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.37119E-03 0.00900  1.83272E-04 0.05421  1.02611E-03 0.02183  1.01705E-03 0.02176  2.89451E-03 0.01264  9.39898E-04 0.02233  3.10339E-04 0.04131 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.99979E-01 0.02128  1.24903E-02 1.1E-05  3.14594E-02 0.00057  1.10139E-01 0.00052  3.21312E-01 0.00042  1.34194E+00 0.00057  8.96113E+00 0.00315 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.74152E-05 0.00282  2.74067E-05 0.00283  2.77307E-05 0.02935 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.85370E-05 0.00277  2.85281E-05 0.00278  2.88612E-05 0.02936 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.28159E-03 0.02921  1.75615E-04 0.17935  1.04044E-03 0.06885  9.17707E-04 0.07157  2.95708E-03 0.04148  9.08690E-04 0.08132  2.82062E-04 0.13895 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.24806E-01 0.06990  1.24901E-02 2.7E-05  3.14402E-02 0.00134  1.10280E-01 0.00140  3.21246E-01 0.00113  1.34188E+00 0.00080  9.09859E+00 0.00798 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.31638E-03 0.02875  1.75722E-04 0.17034  1.05284E-03 0.06792  9.22627E-04 0.06988  2.98868E-03 0.04076  8.97305E-04 0.07679  2.79210E-04 0.13541 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.27124E-01 0.06794  1.24901E-02 2.7E-05  3.14334E-02 0.00135  1.10286E-01 0.00140  3.21296E-01 0.00112  1.34198E+00 0.00079  9.10099E+00 0.00798 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.30579E+02 0.02956 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.75141E-05 0.00075 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.86401E-05 0.00054 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.32198E-03 0.00508 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.29833E+02 0.00512 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.24359E-07 0.00068 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.84666E-06 0.00041  2.84650E-06 0.00041  2.86872E-06 0.00514 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.15203E-05 0.00083  4.15437E-05 0.00083  3.80683E-05 0.00922 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.73465E-01 0.00031  6.73161E-01 0.00032  7.33973E-01 0.00904 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03709E+01 0.01285 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.66285E+01 0.00046  3.52566E+01 0.00047 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.42305E+04 0.00335  2.94908E+05 0.00148  6.05544E+05 0.00083  6.50708E+05 0.00081  5.98084E+05 0.00053  6.41597E+05 0.00062  4.36154E+05 0.00079  3.85952E+05 0.00063  2.94874E+05 0.00084  2.40987E+05 0.00074  2.07951E+05 0.00103  1.87305E+05 0.00068  1.72877E+05 0.00083  1.64637E+05 0.00088  1.60111E+05 0.00076  1.38494E+05 0.00082  1.36797E+05 0.00096  1.35378E+05 0.00127  1.32968E+05 0.00095  2.60166E+05 0.00047  2.50887E+05 0.00062  1.81292E+05 0.00066  1.17271E+05 0.00101  1.35850E+05 0.00103  1.28600E+05 0.00096  1.16650E+05 0.00089  1.91494E+05 0.00062  4.38684E+04 0.00151  5.49576E+04 0.00180  4.99700E+04 0.00146  2.90477E+04 0.00176  5.04255E+04 0.00130  3.41778E+04 0.00171  2.91470E+04 0.00129  5.47342E+03 0.00303  5.33831E+03 0.00371  5.26772E+03 0.00411  5.28475E+03 0.00366  5.31706E+03 0.00315  5.41309E+03 0.00308  5.72688E+03 0.00412  5.44433E+03 0.00429  1.02938E+04 0.00306  1.65174E+04 0.00228  2.11285E+04 0.00208  5.57620E+04 0.00137  5.86176E+04 0.00129  6.38626E+04 0.00100  4.32149E+04 0.00154  3.17520E+04 0.00176  2.41028E+04 0.00141  2.86991E+04 0.00167  5.55902E+04 0.00124  7.69533E+04 0.00120  1.50395E+05 0.00111  2.34338E+05 0.00107  3.51708E+05 0.00140  2.25053E+05 0.00144  1.61578E+05 0.00132  1.16113E+05 0.00148  1.04469E+05 0.00154  1.02941E+05 0.00167  8.60526E+04 0.00175  5.80453E+04 0.00134  5.36595E+04 0.00160  4.77943E+04 0.00169  4.03987E+04 0.00163  3.18029E+04 0.00189  2.12606E+04 0.00186  7.52861E+03 0.00204 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.05032E+00 0.00051 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.25325E+18 0.00053  3.64552E+17 0.00098 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38198E-01 0.00010  1.52125E+00 0.00034 ];
INF_CAPT                  (idx, [1:   4]) = [  6.58973E-03 0.00053  3.07210E-02 0.00044 ];
INF_ABS                   (idx, [1:   4]) = [  8.62939E-03 0.00043  6.15925E-02 0.00069 ];
INF_FISS                  (idx, [1:   4]) = [  2.03966E-03 0.00064  3.08715E-02 0.00095 ];
INF_NSF                   (idx, [1:   4]) = [  5.29547E-03 0.00064  7.75037E-02 0.00097 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.59625E+00 6.7E-05  2.51053E+00 3.5E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04269E+02 7.1E-06  2.03238E+02 5.8E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.88147E-08 0.00044  2.56868E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29566E-01 0.00011  1.45966E+00 0.00038 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44442E-01 0.00019  3.85622E-01 0.00042 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64389E-02 0.00032  9.13371E-02 0.00079 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35890E-03 0.00302  2.73804E-02 0.00219 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03450E-02 0.00190 -8.59041E-03 0.00847 ];
INF_SCATT5                (idx, [1:   4]) = [  1.19952E-04 0.15233  6.45847E-03 0.00738 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07582E-03 0.00348 -1.65471E-02 0.00282 ];
INF_SCATT7                (idx, [1:   4]) = [  7.54762E-04 0.01701  4.46129E-04 0.10911 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29607E-01 0.00011  1.45966E+00 0.00038 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44442E-01 0.00019  3.85622E-01 0.00042 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64395E-02 0.00032  9.13371E-02 0.00079 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35907E-03 0.00303  2.73804E-02 0.00219 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03451E-02 0.00190 -8.59041E-03 0.00847 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.19964E-04 0.15232  6.45847E-03 0.00738 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07583E-03 0.00348 -1.65471E-02 0.00282 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.54735E-04 0.01702  4.46129E-04 0.10911 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13150E-01 0.00026  9.84296E-01 0.00034 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56385E+00 0.00026  3.38653E-01 0.00034 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.58892E-03 0.00042  6.15925E-02 0.00069 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69457E-02 0.00021  6.26856E-02 0.00090 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11252E-01 0.00010  1.83141E-02 0.00047  1.09717E-03 0.00364  1.45856E+00 0.00038 ];
INF_S1                    (idx, [1:   8]) = [  2.39091E-01 0.00019  5.35051E-03 0.00084  4.71383E-04 0.00774  3.85151E-01 0.00041 ];
INF_S2                    (idx, [1:   8]) = [  9.80263E-02 0.00032 -1.58738E-03 0.00230  2.53336E-04 0.00949  9.10838E-02 0.00078 ];
INF_S3                    (idx, [1:   8]) = [  9.23983E-03 0.00235 -1.88092E-03 0.00126  9.15322E-05 0.01820  2.72889E-02 0.00221 ];
INF_S4                    (idx, [1:   8]) = [ -9.71963E-03 0.00197 -6.25395E-04 0.00448 -6.36260E-07 1.00000 -8.58977E-03 0.00845 ];
INF_S5                    (idx, [1:   8]) = [  1.02219E-04 0.17211  1.77329E-05 0.15080 -3.92893E-05 0.04105  6.49776E-03 0.00724 ];
INF_S6                    (idx, [1:   8]) = [  5.22363E-03 0.00308 -1.47806E-04 0.02116 -4.77866E-05 0.03053 -1.64993E-02 0.00283 ];
INF_S7                    (idx, [1:   8]) = [  9.31458E-04 0.01336 -1.76696E-04 0.01642 -4.26890E-05 0.02406  4.88818E-04 0.09936 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11293E-01 0.00010  1.83141E-02 0.00047  1.09717E-03 0.00364  1.45856E+00 0.00038 ];
INF_SP1                   (idx, [1:   8]) = [  2.39092E-01 0.00019  5.35051E-03 0.00084  4.71383E-04 0.00774  3.85151E-01 0.00041 ];
INF_SP2                   (idx, [1:   8]) = [  9.80268E-02 0.00032 -1.58738E-03 0.00230  2.53336E-04 0.00949  9.10838E-02 0.00078 ];
INF_SP3                   (idx, [1:   8]) = [  9.23999E-03 0.00236 -1.88092E-03 0.00126  9.15322E-05 0.01820  2.72889E-02 0.00221 ];
INF_SP4                   (idx, [1:   8]) = [ -9.71971E-03 0.00197 -6.25395E-04 0.00448 -6.36260E-07 1.00000 -8.58977E-03 0.00845 ];
INF_SP5                   (idx, [1:   8]) = [  1.02232E-04 0.17204  1.77329E-05 0.15080 -3.92893E-05 0.04105  6.49776E-03 0.00724 ];
INF_SP6                   (idx, [1:   8]) = [  5.22363E-03 0.00308 -1.47806E-04 0.02116 -4.77866E-05 0.03053 -1.64993E-02 0.00283 ];
INF_SP7                   (idx, [1:   8]) = [  9.31431E-04 0.01336 -1.76696E-04 0.01642 -4.26890E-05 0.02406  4.88818E-04 0.09936 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32392E-01 0.00061  1.10579E+00 0.00510 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33893E-01 0.00101  1.17935E+00 0.00722 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33946E-01 0.00104  1.17969E+00 0.00677 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29404E-01 0.00084  9.83894E-01 0.00536 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43437E+00 0.00061  3.01631E-01 0.00509 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42519E+00 0.00101  2.82994E-01 0.00721 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42487E+00 0.00104  2.82874E-01 0.00683 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45307E+00 0.00084  3.39024E-01 0.00536 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.43431E-03 0.00816  1.87268E-04 0.04950  1.06784E-03 0.01980  1.01895E-03 0.02093  2.86725E-03 0.01169  9.78185E-04 0.02016  3.14818E-04 0.03751 ];
LAMBDA                    (idx, [1:  14]) = [  8.05835E-01 0.01919  1.24902E-02 8.8E-06  3.14172E-02 0.00049  1.10130E-01 0.00045  3.21126E-01 0.00036  1.34290E+00 0.00039  8.93414E+00 0.00265 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0002' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:56:06 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:37:26 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595300166 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.75904E-01  1.01467E+00  9.76230E-01  1.02228E+00  1.01092E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12782E-02 0.00112  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88722E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.01655E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.02077E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69236E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.64538E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.64450E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.99274E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.29302E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000931 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00019E+04 0.00079 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00019E+04 0.00079 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.04653E+02 ;
RUNNING_TIME              (idx, 1)        =  4.13398E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.55900E-01  4.55900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.27983E-01  1.26500E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  4.03810E+01  3.39358E+00  2.62218E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  3.71167E-01  3.08833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.88333E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.13397E+01  1.27037E+02 ];
CPU_USAGE                 (idx, 1)        = 4.95051 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99933E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.73386E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.29341E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.82456E+04 ;
TOT_SF_RATE               (idx, 1)        =  9.33109E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.73605E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.21201E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.55734E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.70334E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  6.75363E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.14429E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.19415E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.22202E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.55947E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.42209E+07 ;
SR90_ACTIVITY             (idx, 1)        =  4.76687E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.21512E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.24542E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.31341E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.22945E+12 ;
CS137_ACTIVITY            (idx, 1)        =  5.40216E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.13707E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.50770E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  8.63122E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.22596E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.40415E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 6 ;
BURNUP                     (idx, [1:  2])  = [  4.00000E+00  4.00035E+00 ];
BURN_DAYS                 (idx, 1)        =  1.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.52359E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  1.03066E+16 0.00073  7.46215E-01 0.00042 ];
U238_FISS                 (idx, [1:   4]) = [  9.49975E+14 0.00271  6.87742E-02 0.00258 ];
PU239_FISS                (idx, [1:   4]) = [  2.48389E+15 0.00166  1.79841E-01 0.00158 ];
PU240_FISS                (idx, [1:   4]) = [  1.14493E+12 0.07611  8.29315E-05 0.07614 ];
PU241_FISS                (idx, [1:   4]) = [  6.56066E+13 0.01050  4.74937E-03 0.01046 ];
U235_CAPT                 (idx, [1:   4]) = [  2.23514E+15 0.00170  1.11851E-01 0.00159 ];
U238_CAPT                 (idx, [1:   4]) = [  8.63440E+15 0.00099  4.32067E-01 0.00069 ];
PU239_CAPT                (idx, [1:   4]) = [  1.38300E+15 0.00227  6.92093E-02 0.00221 ];
PU240_CAPT                (idx, [1:   4]) = [  4.21071E+14 0.00416  2.10694E-02 0.00408 ];
PU241_CAPT                (idx, [1:   4]) = [  2.27800E+13 0.01665  1.14055E-03 0.01669 ];
XE135_CAPT                (idx, [1:   4]) = [  7.41661E+14 0.00305  3.71154E-02 0.00301 ];
SM149_CAPT                (idx, [1:   4]) = [  1.69972E+14 0.00611  8.50552E-03 0.00606 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000931 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.57561E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000931 5.00758E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2931209 2.93513E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2026040 2.02876E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 43682 4.36795E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000931 5.00758E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.16650E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.50762E+16 1.7E-05  3.50762E+16 1.7E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37936E+16 2.8E-06  1.37936E+16 2.8E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.00037E+16 0.00043  1.42937E+16 0.00043  5.71000E+15 0.00107 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.37973E+16 0.00026  2.80873E+16 0.00022  5.71000E+15 0.00107 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.40415E+16 0.00049  3.40415E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.63768E+18 0.00046  4.37011E+17 0.00045  1.20067E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.97433E+14 0.00494 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.40948E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.24231E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12049E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12049E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.62069E+00 0.00049 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.73381E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.72490E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23492E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94324E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96922E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.04085E+00 0.00053 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.03176E+00 0.00054 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.54293E+00 1.9E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03648E+02 2.8E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03170E+00 0.00055  1.02532E+00 0.00055  6.44362E-03 0.00896 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.03039E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03052E+00 0.00048 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.03039E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.03947E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73890E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73909E+01 8.2E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.62889E-07 0.00350 ];
IMP_EALF                  (idx, [1:   2]) = [  5.60361E-07 0.00143 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.32216E-01 0.00281 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.31771E-01 0.00113 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.12089E-03 0.00527  1.74572E-04 0.03288  9.83876E-04 0.01342  9.58132E-04 0.01422  2.79196E-03 0.00807  9.09943E-04 0.01449  3.02403E-04 0.02451 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.10955E-01 0.01307  1.06212E-02 0.01881  3.14001E-02 0.00035  1.10219E-01 0.00033  3.21322E-01 0.00026  1.34042E+00 0.00043  8.67838E+00 0.00873 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.16706E-03 0.00863  1.72545E-04 0.04821  1.00501E-03 0.02086  9.50370E-04 0.02124  2.82579E-03 0.01214  9.13719E-04 0.02222  2.99617E-04 0.03849 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.04402E-01 0.01964  1.24953E-02 0.00023  3.14063E-02 0.00049  1.10316E-01 0.00052  3.21407E-01 0.00038  1.34041E+00 0.00062  8.98557E+00 0.00284 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.73070E-05 0.00122  2.73024E-05 0.00123  2.79049E-05 0.01298 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.81691E-05 0.00113  2.81644E-05 0.00114  2.87886E-05 0.01297 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.25169E-03 0.00914  1.84738E-04 0.05272  9.94794E-04 0.02269  9.82773E-04 0.02280  2.87097E-03 0.01372  9.31919E-04 0.02312  2.86498E-04 0.04086 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.84113E-01 0.02194  1.24988E-02 0.00043  3.14337E-02 0.00061  1.10161E-01 0.00060  3.21279E-01 0.00039  1.34046E+00 0.00068  8.98347E+00 0.00420 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.71927E-05 0.00288  2.71859E-05 0.00287  2.71990E-05 0.03272 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.80517E-05 0.00285  2.80447E-05 0.00284  2.80642E-05 0.03277 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.79256E-03 0.02833  1.54996E-04 0.18482  9.29857E-04 0.07282  9.17080E-04 0.07400  2.72545E-03 0.04356  8.47108E-04 0.07378  2.18071E-04 0.14452 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.15220E-01 0.06923  1.25100E-02 0.00161  3.14085E-02 0.00141  1.10438E-01 0.00158  3.21083E-01 0.00119  1.34301E+00 0.00075  9.01641E+00 0.00775 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.85467E-03 0.02741  1.44566E-04 0.17578  9.37651E-04 0.07146  8.90341E-04 0.07246  2.76681E-03 0.04208  8.88735E-04 0.07196  2.26562E-04 0.13853 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.26232E-01 0.06667  1.25100E-02 0.00161  3.14148E-02 0.00139  1.10425E-01 0.00157  3.21131E-01 0.00119  1.34304E+00 0.00075  9.01773E+00 0.00776 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.14175E+02 0.02863 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.72496E-05 0.00075 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.81091E-05 0.00050 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.18010E-03 0.00510 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.26890E+02 0.00519 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.18316E-07 0.00065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.82984E-06 0.00041  2.82968E-06 0.00041  2.85528E-06 0.00538 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.11517E-05 0.00078  4.11752E-05 0.00078  3.74701E-05 0.00994 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.70093E-01 0.00033  6.69866E-01 0.00033  7.16772E-01 0.00859 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03962E+01 0.01410 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.64450E+01 0.00046  3.49753E+01 0.00046 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.51625E+04 0.00288  2.96789E+05 0.00154  6.06504E+05 0.00085  6.50202E+05 0.00108  5.97763E+05 0.00070  6.41216E+05 0.00084  4.35394E+05 0.00060  3.85145E+05 0.00074  2.94396E+05 0.00077  2.41122E+05 0.00069  2.07702E+05 0.00085  1.87549E+05 0.00088  1.72813E+05 0.00113  1.64447E+05 0.00082  1.60105E+05 0.00080  1.38528E+05 0.00078  1.36750E+05 0.00105  1.35305E+05 0.00089  1.33088E+05 0.00103  2.59733E+05 0.00080  2.50832E+05 0.00082  1.81333E+05 0.00089  1.17526E+05 0.00116  1.36023E+05 0.00070  1.28678E+05 0.00085  1.16430E+05 0.00106  1.90923E+05 0.00089  4.38326E+04 0.00156  5.50840E+04 0.00145  4.99663E+04 0.00182  2.90389E+04 0.00203  5.04761E+04 0.00175  3.40733E+04 0.00179  2.90012E+04 0.00164  5.45168E+03 0.00398  5.21602E+03 0.00292  5.03218E+03 0.00365  5.01196E+03 0.00289  5.06545E+03 0.00353  5.23604E+03 0.00269  5.60049E+03 0.00376  5.32717E+03 0.00358  1.02106E+04 0.00238  1.63364E+04 0.00289  2.09788E+04 0.00200  5.54912E+04 0.00170  5.84111E+04 0.00166  6.32566E+04 0.00117  4.25552E+04 0.00134  3.10828E+04 0.00169  2.35579E+04 0.00143  2.78174E+04 0.00149  5.41636E+04 0.00147  7.54955E+04 0.00126  1.47636E+05 0.00088  2.30624E+05 0.00099  3.46467E+05 0.00092  2.21916E+05 0.00100  1.59242E+05 0.00118  1.14802E+05 0.00110  1.03013E+05 0.00129  1.01615E+05 0.00117  8.48318E+04 0.00114  5.74244E+04 0.00099  5.30898E+04 0.00122  4.71878E+04 0.00130  3.99148E+04 0.00119  3.13965E+04 0.00119  2.10240E+04 0.00136  7.46420E+03 0.00216 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.03960E+00 0.00050 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.27279E+18 0.00052  3.64923E+17 0.00087 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38249E-01 0.00012  1.52706E+00 0.00031 ];
INF_CAPT                  (idx, [1:   4]) = [  6.71662E-03 0.00055  3.13926E-02 0.00041 ];
INF_ABS                   (idx, [1:   4]) = [  8.72741E-03 0.00043  6.21828E-02 0.00065 ];
INF_FISS                  (idx, [1:   4]) = [  2.01079E-03 0.00051  3.07902E-02 0.00090 ];
INF_NSF                   (idx, [1:   4]) = [  5.23749E-03 0.00050  7.78642E-02 0.00091 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.60469E+00 5.5E-05  2.52886E+00 3.3E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04377E+02 7.3E-06  2.03482E+02 5.4E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.85985E-08 0.00041  2.57064E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29525E-01 0.00012  1.46487E+00 0.00034 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44489E-01 0.00019  3.87441E-01 0.00047 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64434E-02 0.00028  9.17272E-02 0.00085 ];
INF_SCATT3                (idx, [1:   4]) = [  7.37721E-03 0.00268  2.76016E-02 0.00249 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03457E-02 0.00221 -8.56174E-03 0.00674 ];
INF_SCATT5                (idx, [1:   4]) = [  1.49228E-04 0.12588  6.41913E-03 0.00719 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08033E-03 0.00367 -1.66525E-02 0.00286 ];
INF_SCATT7                (idx, [1:   4]) = [  7.40808E-04 0.01944  4.12169E-04 0.11335 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29566E-01 0.00012  1.46487E+00 0.00034 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44489E-01 0.00019  3.87441E-01 0.00047 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64439E-02 0.00028  9.17272E-02 0.00085 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.37734E-03 0.00269  2.76016E-02 0.00249 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03457E-02 0.00220 -8.56174E-03 0.00674 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.49345E-04 0.12564  6.41913E-03 0.00719 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08053E-03 0.00368 -1.66525E-02 0.00286 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.40858E-04 0.01942  4.12169E-04 0.11335 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12998E-01 0.00023  9.88073E-01 0.00029 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56496E+00 0.00023  3.37358E-01 0.00029 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.68689E-03 0.00043  6.21828E-02 0.00065 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69492E-02 0.00021  6.32838E-02 0.00073 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11299E-01 0.00012  1.82258E-02 0.00041  1.09855E-03 0.00358  1.46378E+00 0.00034 ];
INF_S1                    (idx, [1:   8]) = [  2.39164E-01 0.00019  5.32547E-03 0.00126  4.70801E-04 0.00558  3.86971E-01 0.00047 ];
INF_S2                    (idx, [1:   8]) = [  9.80226E-02 0.00027 -1.57913E-03 0.00308  2.55939E-04 0.01026  9.14713E-02 0.00086 ];
INF_S3                    (idx, [1:   8]) = [  9.25500E-03 0.00217 -1.87779E-03 0.00173  9.02380E-05 0.01670  2.75114E-02 0.00250 ];
INF_S4                    (idx, [1:   8]) = [ -9.72198E-03 0.00227 -6.23696E-04 0.00604  1.26781E-06 1.00000 -8.56300E-03 0.00670 ];
INF_S5                    (idx, [1:   8]) = [  1.27176E-04 0.14827  2.20520E-05 0.13634 -3.63854E-05 0.03818  6.45552E-03 0.00710 ];
INF_S6                    (idx, [1:   8]) = [  5.22380E-03 0.00347 -1.43466E-04 0.01559 -4.51743E-05 0.02769 -1.66073E-02 0.00286 ];
INF_S7                    (idx, [1:   8]) = [  9.13594E-04 0.01592 -1.72786E-04 0.01608 -4.23474E-05 0.03069  4.54517E-04 0.10276 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11340E-01 0.00012  1.82258E-02 0.00041  1.09855E-03 0.00358  1.46378E+00 0.00034 ];
INF_SP1                   (idx, [1:   8]) = [  2.39164E-01 0.00019  5.32547E-03 0.00126  4.70801E-04 0.00558  3.86971E-01 0.00047 ];
INF_SP2                   (idx, [1:   8]) = [  9.80230E-02 0.00027 -1.57913E-03 0.00308  2.55939E-04 0.01026  9.14713E-02 0.00086 ];
INF_SP3                   (idx, [1:   8]) = [  9.25513E-03 0.00217 -1.87779E-03 0.00173  9.02380E-05 0.01670  2.75114E-02 0.00250 ];
INF_SP4                   (idx, [1:   8]) = [ -9.72204E-03 0.00227 -6.23696E-04 0.00604  1.26781E-06 1.00000 -8.56300E-03 0.00670 ];
INF_SP5                   (idx, [1:   8]) = [  1.27293E-04 0.14800  2.20520E-05 0.13634 -3.63854E-05 0.03818  6.45552E-03 0.00710 ];
INF_SP6                   (idx, [1:   8]) = [  5.22399E-03 0.00348 -1.43466E-04 0.01559 -4.51743E-05 0.02769 -1.66073E-02 0.00286 ];
INF_SP7                   (idx, [1:   8]) = [  9.13644E-04 0.01591 -1.72786E-04 0.01608 -4.23474E-05 0.03069  4.54517E-04 0.10276 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32110E-01 0.00068  1.12353E+00 0.00555 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33682E-01 0.00078  1.20114E+00 0.00707 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33720E-01 0.00080  1.20473E+00 0.00581 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29001E-01 0.00124  9.93436E-01 0.00716 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43611E+00 0.00068  2.96905E-01 0.00556 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42646E+00 0.00078  2.77844E-01 0.00702 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42623E+00 0.00080  2.76910E-01 0.00576 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45565E+00 0.00124  3.35961E-01 0.00737 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.16706E-03 0.00863  1.72545E-04 0.04821  1.00501E-03 0.02086  9.50370E-04 0.02124  2.82579E-03 0.01214  9.13719E-04 0.02222  2.99617E-04 0.03849 ];
LAMBDA                    (idx, [1:  14]) = [  8.04402E-01 0.01964  1.24953E-02 0.00023  3.14063E-02 0.00049  1.10316E-01 0.00052  3.21407E-01 0.00038  1.34041E+00 0.00062  8.98557E+00 0.00284 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0002' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:56:06 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:43:32 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595300166 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.78571E-01  9.85225E-01  9.97382E-01  1.02224E+00  1.01658E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12912E-02 0.00107  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88709E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.02711E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.03132E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68731E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.63136E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.63048E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.89863E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.27417E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001031 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00021E+04 0.00080 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00021E+04 0.00080 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.35058E+02 ;
RUNNING_TIME              (idx, 1)        =  4.74279E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.55900E-01  4.55900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.51967E-01  1.20833E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  4.63865E+01  3.38117E+00  2.62432E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  4.29583E-01  2.93000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.14000E-02  6.66666E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.74278E+01  1.26736E+02 ];
CPU_USAGE                 (idx, 1)        = 4.95611 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99965E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.74647E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.32975E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.81157E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.13095E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.75170E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.22288E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.57803E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.68926E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  7.70940E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.22515E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.75252E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.30351E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.95688E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.49480E+07 ;
SR90_ACTIVITY             (idx, 1)        =  5.86267E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.24087E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.28383E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.34879E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.96443E+12 ;
CS137_ACTIVITY            (idx, 1)        =  6.75251E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.16107E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.48069E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.29359E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.23407E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.45857E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 7 ;
BURNUP                     (idx, [1:  2])  = [  5.00000E+00  5.00044E+00 ];
BURN_DAYS                 (idx, 1)        =  1.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.63653E-01 0.00104 ];
U235_FISS                 (idx, [1:   4]) = [  9.81112E+15 0.00069  7.12167E-01 0.00045 ];
U238_FISS                 (idx, [1:   4]) = [  9.61500E+14 0.00266  6.97832E-02 0.00250 ];
PU239_FISS                (idx, [1:   4]) = [  2.88865E+15 0.00149  2.09678E-01 0.00138 ];
PU240_FISS                (idx, [1:   4]) = [  1.59623E+12 0.06603  1.15904E-04 0.06606 ];
PU241_FISS                (idx, [1:   4]) = [  1.08385E+14 0.00831  7.86599E-03 0.00824 ];
U235_CAPT                 (idx, [1:   4]) = [  2.13662E+15 0.00178  1.03950E-01 0.00167 ];
U238_CAPT                 (idx, [1:   4]) = [  8.72643E+15 0.00102  4.24531E-01 0.00067 ];
PU239_CAPT                (idx, [1:   4]) = [  1.60402E+15 0.00212  7.80409E-02 0.00206 ];
PU240_CAPT                (idx, [1:   4]) = [  5.67040E+14 0.00376  2.75880E-02 0.00372 ];
PU241_CAPT                (idx, [1:   4]) = [  3.88428E+13 0.01311  1.89038E-03 0.01316 ];
XE135_CAPT                (idx, [1:   4]) = [  7.47148E+14 0.00303  3.63513E-02 0.00299 ];
SM149_CAPT                (idx, [1:   4]) = [  1.76365E+14 0.00637  8.58157E-03 0.00639 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001031 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.66968E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001031 5.00767E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2967492 2.97152E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1989175 1.99178E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44364 4.43675E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001031 5.00767E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.33299E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.52454E+16 1.8E-05  3.52454E+16 1.8E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37802E+16 3.0E-06  1.37802E+16 3.0E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.05553E+16 0.00046  1.47926E+16 0.00046  5.76267E+15 0.00108 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.43355E+16 0.00028  2.85728E+16 0.00024  5.76267E+15 0.00108 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.45857E+16 0.00052  3.45857E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.65930E+18 0.00050  4.42132E+17 0.00050  1.21716E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.06929E+14 0.00471 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.46424E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.25740E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11932E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11932E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.60906E+00 0.00044 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.73518E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.68901E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23484E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94236E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96873E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.02800E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.01888E+00 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.55768E+00 2.0E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03846E+02 3.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.01876E+00 0.00057  1.01266E+00 0.00055  6.22031E-03 0.00881 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.01901E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.01921E+00 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.01901E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.02813E+00 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73565E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73560E+01 9.1E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.81432E-07 0.00345 ];
IMP_EALF                  (idx, [1:   2]) = [  5.80358E-07 0.00158 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.35797E-01 0.00276 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.36263E-01 0.00114 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.06808E-03 0.00569  1.79797E-04 0.03335  9.99309E-04 0.01422  9.40061E-04 0.01373  2.75741E-03 0.00846  8.96562E-04 0.01454  2.94942E-04 0.02448 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.01365E-01 0.01297  1.04476E-02 0.01983  3.13379E-02 0.00038  1.10212E-01 0.00036  3.21642E-01 0.00025  1.33682E+00 0.00064  8.47971E+00 0.01109 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.14471E-03 0.00848  1.74610E-04 0.05163  1.03766E-03 0.02057  9.54964E-04 0.01930  2.78133E-03 0.01333  8.96839E-04 0.02139  2.99309E-04 0.03766 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.07277E-01 0.02080  1.24999E-02 0.00033  3.13448E-02 0.00053  1.10282E-01 0.00051  3.21634E-01 0.00039  1.33651E+00 0.00080  8.92673E+00 0.00424 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.73215E-05 0.00125  2.73111E-05 0.00125  2.91457E-05 0.01313 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.78297E-05 0.00112  2.78191E-05 0.00112  2.96837E-05 0.01310 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.11435E-03 0.00903  1.71903E-04 0.05510  1.02649E-03 0.02292  9.43678E-04 0.02183  2.76155E-03 0.01341  9.07756E-04 0.02329  3.02975E-04 0.04061 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.07533E-01 0.02198  1.25067E-02 0.00065  3.13419E-02 0.00065  1.10268E-01 0.00063  3.21720E-01 0.00042  1.33438E+00 0.00138  8.91801E+00 0.00536 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.71821E-05 0.00276  2.71773E-05 0.00276  2.75063E-05 0.03049 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.76875E-05 0.00270  2.76825E-05 0.00270  2.80144E-05 0.03043 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.88937E-03 0.03006  1.38836E-04 0.18179  9.16651E-04 0.07082  1.03404E-03 0.07871  2.54215E-03 0.04509  9.12788E-04 0.07405  3.44896E-04 0.15611 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.01986E-01 0.07455  1.25048E-02 0.00116  3.12684E-02 0.00157  1.10261E-01 0.00155  3.22440E-01 0.00135  1.32827E+00 0.00389  9.00698E+00 0.00778 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.88223E-03 0.02891  1.43529E-04 0.17390  9.22955E-04 0.06738  1.01987E-03 0.07567  2.53122E-03 0.04410  9.29494E-04 0.07202  3.35163E-04 0.15039 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.99597E-01 0.07214  1.25050E-02 0.00117  3.12666E-02 0.00156  1.10275E-01 0.00155  3.22372E-01 0.00132  1.32655E+00 0.00410  9.00796E+00 0.00778 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.17215E+02 0.03039 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.72364E-05 0.00080 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.77431E-05 0.00057 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.95681E-03 0.00567 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.18715E+02 0.00563 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.13617E-07 0.00067 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.81174E-06 0.00042  2.81151E-06 0.00042  2.84921E-06 0.00496 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.09368E-05 0.00084  4.09600E-05 0.00084  3.74140E-05 0.00962 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.66480E-01 0.00031  6.66289E-01 0.00032  7.08443E-01 0.00853 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02572E+01 0.01348 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.63048E+01 0.00046  3.47905E+01 0.00051 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.55706E+04 0.00312  2.97975E+05 0.00118  6.07949E+05 0.00102  6.50960E+05 0.00080  5.98063E+05 0.00089  6.40794E+05 0.00061  4.35547E+05 0.00083  3.85057E+05 0.00067  2.94939E+05 0.00088  2.40770E+05 0.00092  2.07704E+05 0.00075  1.87068E+05 0.00080  1.72688E+05 0.00084  1.64312E+05 0.00084  1.60054E+05 0.00092  1.38231E+05 0.00085  1.36448E+05 0.00082  1.35309E+05 0.00079  1.32951E+05 0.00106  2.59941E+05 0.00074  2.50918E+05 0.00071  1.81251E+05 0.00094  1.17500E+05 0.00103  1.35810E+05 0.00099  1.28812E+05 0.00075  1.16441E+05 0.00085  1.90816E+05 0.00063  4.37877E+04 0.00124  5.48790E+04 0.00196  4.96535E+04 0.00121  2.88970E+04 0.00204  5.02109E+04 0.00154  3.41157E+04 0.00174  2.88768E+04 0.00176  5.38353E+03 0.00289  5.09889E+03 0.00368  4.87805E+03 0.00291  4.75615E+03 0.00332  4.85151E+03 0.00419  5.07013E+03 0.00367  5.55063E+03 0.00376  5.30421E+03 0.00346  1.00483E+04 0.00246  1.62267E+04 0.00232  2.07424E+04 0.00209  5.48992E+04 0.00123  5.78342E+04 0.00160  6.24736E+04 0.00129  4.18359E+04 0.00152  3.05212E+04 0.00149  2.31066E+04 0.00162  2.72592E+04 0.00192  5.30470E+04 0.00143  7.38996E+04 0.00114  1.45649E+05 0.00108  2.27191E+05 0.00112  3.42295E+05 0.00111  2.19810E+05 0.00107  1.57795E+05 0.00110  1.13540E+05 0.00154  1.01984E+05 0.00123  1.00555E+05 0.00126  8.41065E+04 0.00120  5.68917E+04 0.00151  5.25754E+04 0.00123  4.68398E+04 0.00132  3.96106E+04 0.00160  3.12084E+04 0.00169  2.08895E+04 0.00166  7.41259E+03 0.00217 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.02833E+00 0.00050 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.29301E+18 0.00050  3.66320E+17 0.00102 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38211E-01 0.00014  1.53286E+00 0.00034 ];
INF_CAPT                  (idx, [1:   4]) = [  6.84356E-03 0.00053  3.19600E-02 0.00050 ];
INF_ABS                   (idx, [1:   4]) = [  8.82791E-03 0.00045  6.25793E-02 0.00076 ];
INF_FISS                  (idx, [1:   4]) = [  1.98436E-03 0.00045  3.06193E-02 0.00105 ];
INF_NSF                   (idx, [1:   4]) = [  5.18461E-03 0.00044  7.79289E-02 0.00107 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.61274E+00 6.4E-05  2.54509E+00 3.3E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04481E+02 7.2E-06  2.03701E+02 5.5E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.83058E-08 0.00046  2.57383E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29392E-01 0.00015  1.47026E+00 0.00038 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44435E-01 0.00024  3.88824E-01 0.00053 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63957E-02 0.00030  9.20079E-02 0.00078 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35673E-03 0.00300  2.76795E-02 0.00273 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03409E-02 0.00215 -8.66829E-03 0.00692 ];
INF_SCATT5                (idx, [1:   4]) = [  1.20329E-04 0.14742  6.47981E-03 0.00805 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08187E-03 0.00376 -1.67266E-02 0.00299 ];
INF_SCATT7                (idx, [1:   4]) = [  7.47420E-04 0.02430  4.14817E-04 0.11027 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29433E-01 0.00015  1.47026E+00 0.00038 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44435E-01 0.00024  3.88824E-01 0.00053 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63959E-02 0.00030  9.20079E-02 0.00078 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35642E-03 0.00300  2.76795E-02 0.00273 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03411E-02 0.00215 -8.66829E-03 0.00692 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.20493E-04 0.14739  6.47981E-03 0.00805 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08189E-03 0.00377 -1.67266E-02 0.00299 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.47316E-04 0.02429  4.14817E-04 0.11027 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12858E-01 0.00036  9.92346E-01 0.00032 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56599E+00 0.00036  3.35905E-01 0.00032 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.78688E-03 0.00045  6.25793E-02 0.00076 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69506E-02 0.00024  6.37076E-02 0.00081 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11260E-01 0.00014  1.81318E-02 0.00041  1.11335E-03 0.00466  1.46915E+00 0.00038 ];
INF_S1                    (idx, [1:   8]) = [  2.39152E-01 0.00023  5.28258E-03 0.00115  4.78074E-04 0.00692  3.88346E-01 0.00053 ];
INF_S2                    (idx, [1:   8]) = [  9.79790E-02 0.00027 -1.58332E-03 0.00291  2.58665E-04 0.00982  9.17492E-02 0.00078 ];
INF_S3                    (idx, [1:   8]) = [  9.22325E-03 0.00247 -1.86653E-03 0.00163  9.32644E-05 0.02013  2.75862E-02 0.00271 ];
INF_S4                    (idx, [1:   8]) = [ -9.73243E-03 0.00223 -6.08510E-04 0.00651  1.37835E-06 1.00000 -8.66967E-03 0.00691 ];
INF_S5                    (idx, [1:   8]) = [  9.62275E-05 0.17277  2.41017E-05 0.10122 -3.71587E-05 0.03913  6.51697E-03 0.00805 ];
INF_S6                    (idx, [1:   8]) = [  5.23319E-03 0.00345 -1.51318E-04 0.02124 -4.63510E-05 0.02475 -1.66802E-02 0.00303 ];
INF_S7                    (idx, [1:   8]) = [  9.21460E-04 0.01843 -1.74039E-04 0.01534 -4.16700E-05 0.02738  4.56487E-04 0.10102 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11301E-01 0.00014  1.81318E-02 0.00041  1.11335E-03 0.00466  1.46915E+00 0.00038 ];
INF_SP1                   (idx, [1:   8]) = [  2.39153E-01 0.00023  5.28258E-03 0.00115  4.78074E-04 0.00692  3.88346E-01 0.00053 ];
INF_SP2                   (idx, [1:   8]) = [  9.79792E-02 0.00027 -1.58332E-03 0.00291  2.58665E-04 0.00982  9.17492E-02 0.00078 ];
INF_SP3                   (idx, [1:   8]) = [  9.22294E-03 0.00247 -1.86653E-03 0.00163  9.32644E-05 0.02013  2.75862E-02 0.00271 ];
INF_SP4                   (idx, [1:   8]) = [ -9.73257E-03 0.00222 -6.08510E-04 0.00651  1.37835E-06 1.00000 -8.66967E-03 0.00691 ];
INF_SP5                   (idx, [1:   8]) = [  9.63909E-05 0.17270  2.41017E-05 0.10122 -3.71587E-05 0.03913  6.51697E-03 0.00805 ];
INF_SP6                   (idx, [1:   8]) = [  5.23320E-03 0.00345 -1.51318E-04 0.02124 -4.63510E-05 0.02475 -1.66802E-02 0.00303 ];
INF_SP7                   (idx, [1:   8]) = [  9.21355E-04 0.01843 -1.74039E-04 0.01534 -4.16700E-05 0.02738  4.56487E-04 0.10102 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32117E-01 0.00053  1.13593E+00 0.00633 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33567E-01 0.00074  1.21979E+00 0.00902 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33824E-01 0.00059  1.22142E+00 0.00702 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29031E-01 0.00115  9.98557E-01 0.00656 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43607E+00 0.00053  2.93728E-01 0.00634 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42716E+00 0.00074  2.73803E-01 0.00899 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42558E+00 0.00059  2.73229E-01 0.00698 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45545E+00 0.00115  3.34152E-01 0.00642 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.14471E-03 0.00848  1.74610E-04 0.05163  1.03766E-03 0.02057  9.54964E-04 0.01930  2.78133E-03 0.01333  8.96839E-04 0.02139  2.99309E-04 0.03766 ];
LAMBDA                    (idx, [1:  14]) = [  8.07277E-01 0.02080  1.24999E-02 0.00033  3.13448E-02 0.00053  1.10282E-01 0.00051  3.21634E-01 0.00039  1.33651E+00 0.00080  8.92673E+00 0.00424 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0002' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:56:06 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:49:36 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595300166 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.92817E-01  1.00658E+00  9.89904E-01  1.00778E+00  1.00293E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.6E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12759E-02 0.00103  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88724E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.03276E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.03698E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68396E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.61802E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.61713E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.83428E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.24592E-01 0.00107  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000737 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00015E+04 0.00075 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00015E+04 0.00075 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.65407E+02 ;
RUNNING_TIME              (idx, 1)        =  5.35044E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.55900E-01  4.55900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.75700E-01  1.19667E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  5.23784E+01  3.38045E+00  2.61150E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  4.89767E-01  2.99833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.43000E-02  6.66666E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  5.35042E+01  1.26423E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96047 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99930E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75651E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.36580E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.80112E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.47559E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.77207E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.23706E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.59370E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.67739E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  8.69416E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.29394E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.39853E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.40571E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.29563E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.55337E+07 ;
SR90_ACTIVITY             (idx, 1)        =  6.92852E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.26431E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.31703E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.38062E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.87248E+12 ;
CS137_ACTIVITY            (idx, 1)        =  8.10214E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.18855E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.45746E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.92477E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.24255E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.51144E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 8 ;
BURNUP                     (idx, [1:  2])  = [  6.00000E+00  6.00054E+00 ];
BURN_DAYS                 (idx, 1)        =  1.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.73610E-01 0.00102 ];
U235_FISS                 (idx, [1:   4]) = [  9.37577E+15 0.00076  6.80974E-01 0.00049 ];
U238_FISS                 (idx, [1:   4]) = [  9.74828E+14 0.00277  7.07971E-02 0.00265 ];
PU239_FISS                (idx, [1:   4]) = [  3.24988E+15 0.00141  2.36041E-01 0.00128 ];
PU240_FISS                (idx, [1:   4]) = [  2.29355E+12 0.05295  1.66639E-04 0.05292 ];
PU241_FISS                (idx, [1:   4]) = [  1.59582E+14 0.00681  1.15899E-02 0.00678 ];
U235_CAPT                 (idx, [1:   4]) = [  2.04772E+15 0.00187  9.70984E-02 0.00178 ];
U238_CAPT                 (idx, [1:   4]) = [  8.81127E+15 0.00098  4.17797E-01 0.00070 ];
PU239_CAPT                (idx, [1:   4]) = [  1.80114E+15 0.00192  8.54106E-02 0.00189 ];
PU240_CAPT                (idx, [1:   4]) = [  7.07026E+14 0.00310  3.35247E-02 0.00304 ];
PU241_CAPT                (idx, [1:   4]) = [  5.87139E+13 0.01061  2.78342E-03 0.01054 ];
XE135_CAPT                (idx, [1:   4]) = [  7.52017E+14 0.00318  3.56640E-02 0.00322 ];
SM149_CAPT                (idx, [1:   4]) = [  1.78708E+14 0.00640  8.47305E-03 0.00635 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000737 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.75608E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000737 5.00776E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2998586 3.00293E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1957898 1.96056E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44253 4.42614E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000737 5.00776E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -9.77889E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.54025E+16 1.9E-05  3.54025E+16 1.9E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37676E+16 3.3E-06  1.37676E+16 3.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.10813E+16 0.00044  1.52723E+16 0.00044  5.80899E+15 0.00107 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.48489E+16 0.00027  2.90399E+16 0.00023  5.80899E+15 0.00107 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.51144E+16 0.00049  3.51144E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.68052E+18 0.00046  4.47407E+17 0.00045  1.23311E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.10891E+14 0.00502 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.51598E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.27191E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11816E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11816E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.59946E+00 0.00050 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.74109E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.65505E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23476E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94278E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96852E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.01736E+00 0.00051 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.00836E+00 0.00052 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.57143E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04033E+02 3.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.00823E+00 0.00054  1.00228E+00 0.00052  6.07932E-03 0.00874 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.00851E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.00833E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.00851E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.01752E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73276E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73284E+01 8.6E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.98823E-07 0.00382 ];
IMP_EALF                  (idx, [1:   2]) = [  5.96533E-07 0.00149 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.40604E-01 0.00291 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.40049E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.00612E-03 0.00569  1.73234E-04 0.03303  9.95503E-04 0.01445  9.30515E-04 0.01478  2.70341E-03 0.00867  9.24213E-04 0.01458  2.79242E-04 0.02772 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.89671E-01 0.01387  1.03533E-02 0.02041  3.12954E-02 0.00036  1.10204E-01 0.00037  3.21800E-01 0.00027  1.33444E+00 0.00080  8.23415E+00 0.01383 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.98038E-03 0.00864  1.78335E-04 0.05013  9.81888E-04 0.02204  9.29225E-04 0.02183  2.70347E-03 0.01300  9.26606E-04 0.02253  2.60857E-04 0.04064 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.68190E-01 0.02011  1.25024E-02 0.00041  3.12669E-02 0.00056  1.10216E-01 0.00054  3.22063E-01 0.00039  1.33519E+00 0.00104  8.97327E+00 0.00401 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.74165E-05 0.00124  2.74052E-05 0.00125  2.93146E-05 0.01395 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.76385E-05 0.00113  2.76270E-05 0.00114  2.95632E-05 0.01401 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.03674E-03 0.00902  1.71542E-04 0.05297  1.02158E-03 0.02326  9.07652E-04 0.02396  2.73849E-03 0.01401  9.15200E-04 0.02381  2.82277E-04 0.04302 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.87760E-01 0.02303  1.24986E-02 0.00049  3.12790E-02 0.00068  1.10177E-01 0.00066  3.22105E-01 0.00045  1.33630E+00 0.00136  8.92245E+00 0.00472 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.72243E-05 0.00290  2.72010E-05 0.00289  3.08305E-05 0.03854 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.74443E-05 0.00285  2.74208E-05 0.00284  3.10725E-05 0.03842 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.05263E-03 0.03138  1.82105E-04 0.17125  9.32502E-04 0.07349  9.66747E-04 0.07607  2.76334E-03 0.04728  9.15281E-04 0.07696  2.92653E-04 0.15172 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.77192E-01 0.07549  1.25127E-02 0.00177  3.12625E-02 0.00162  1.10314E-01 0.00156  3.21940E-01 0.00128  1.33370E+00 0.00337  8.84836E+00 0.01622 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.08980E-03 0.03046  1.73187E-04 0.17084  9.22423E-04 0.07010  9.67340E-04 0.07528  2.83450E-03 0.04565  9.13719E-04 0.07547  2.78637E-04 0.14247 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.65813E-01 0.07372  1.25127E-02 0.00177  3.12552E-02 0.00163  1.10317E-01 0.00156  3.21943E-01 0.00127  1.33415E+00 0.00323  8.84984E+00 0.01621 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.23556E+02 0.03148 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.73568E-05 0.00074 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.75781E-05 0.00053 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.97550E-03 0.00589 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.18517E+02 0.00598 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.09334E-07 0.00066 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.79765E-06 0.00044  2.79742E-06 0.00044  2.83474E-06 0.00545 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.07373E-05 0.00083  4.07566E-05 0.00083  3.76964E-05 0.00996 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.63153E-01 0.00031  6.63001E-01 0.00031  6.99361E-01 0.00870 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05902E+01 0.01359 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.61713E+01 0.00046  3.46644E+01 0.00049 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.60009E+04 0.00399  2.98307E+05 0.00128  6.07338E+05 0.00065  6.50702E+05 0.00072  5.98566E+05 0.00063  6.41155E+05 0.00072  4.35736E+05 0.00074  3.85290E+05 0.00080  2.95288E+05 0.00078  2.40957E+05 0.00102  2.07819E+05 0.00085  1.87193E+05 0.00115  1.72753E+05 0.00083  1.64559E+05 0.00087  1.59963E+05 0.00083  1.38327E+05 0.00096  1.36508E+05 0.00070  1.35373E+05 0.00067  1.33009E+05 0.00077  2.59682E+05 0.00066  2.51132E+05 0.00047  1.81155E+05 0.00078  1.17558E+05 0.00105  1.35782E+05 0.00085  1.28765E+05 0.00108  1.16212E+05 0.00089  1.90174E+05 0.00085  4.36197E+04 0.00187  5.47423E+04 0.00148  4.95883E+04 0.00161  2.89780E+04 0.00225  5.02307E+04 0.00129  3.38944E+04 0.00122  2.86534E+04 0.00196  5.29240E+03 0.00344  4.95925E+03 0.00297  4.64886E+03 0.00355  4.56267E+03 0.00413  4.63793E+03 0.00368  4.89280E+03 0.00449  5.36731E+03 0.00326  5.24383E+03 0.00308  9.98860E+03 0.00287  1.60450E+04 0.00206  2.06633E+04 0.00219  5.46500E+04 0.00121  5.74680E+04 0.00145  6.20450E+04 0.00128  4.14295E+04 0.00126  2.99504E+04 0.00172  2.26046E+04 0.00144  2.67797E+04 0.00176  5.19955E+04 0.00119  7.29072E+04 0.00096  1.43397E+05 0.00088  2.24645E+05 0.00111  3.38755E+05 0.00121  2.17574E+05 0.00120  1.56304E+05 0.00136  1.12459E+05 0.00130  1.01168E+05 0.00109  9.97455E+04 0.00125  8.33002E+04 0.00121  5.64568E+04 0.00159  5.21622E+04 0.00144  4.64281E+04 0.00136  3.92741E+04 0.00129  3.09690E+04 0.00167  2.07472E+04 0.00152  7.36642E+03 0.00194 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.01733E+00 0.00051 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.31257E+18 0.00054  3.67986E+17 0.00098 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38286E-01 9.2E-05  1.53646E+00 0.00033 ];
INF_CAPT                  (idx, [1:   4]) = [  6.95076E-03 0.00057  3.24986E-02 0.00048 ];
INF_ABS                   (idx, [1:   4]) = [  8.90405E-03 0.00046  6.29499E-02 0.00073 ];
INF_FISS                  (idx, [1:   4]) = [  1.95329E-03 0.00045  3.04513E-02 0.00100 ];
INF_NSF                   (idx, [1:   4]) = [  5.11907E-03 0.00045  7.79599E-02 0.00102 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.62074E+00 5.6E-05  2.56015E+00 4.3E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04585E+02 6.2E-06  2.03906E+02 7.2E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.80458E-08 0.00037  2.57584E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29376E-01 9.5E-05  1.47351E+00 0.00037 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44452E-01 0.00020  3.89865E-01 0.00039 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64401E-02 0.00033  9.21877E-02 0.00092 ];
INF_SCATT3                (idx, [1:   4]) = [  7.36526E-03 0.00338  2.75994E-02 0.00224 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03396E-02 0.00187 -8.82041E-03 0.00537 ];
INF_SCATT5                (idx, [1:   4]) = [  1.19155E-04 0.17171  6.45482E-03 0.00763 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08497E-03 0.00318 -1.67446E-02 0.00381 ];
INF_SCATT7                (idx, [1:   4]) = [  7.37235E-04 0.02064  5.07025E-04 0.08136 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29417E-01 9.5E-05  1.47351E+00 0.00037 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44452E-01 0.00020  3.89865E-01 0.00039 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64405E-02 0.00033  9.21877E-02 0.00092 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.36533E-03 0.00337  2.75994E-02 0.00224 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03394E-02 0.00187 -8.82041E-03 0.00537 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.19209E-04 0.17118  6.45482E-03 0.00763 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08530E-03 0.00318 -1.67446E-02 0.00381 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.36926E-04 0.02071  5.07025E-04 0.08136 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12776E-01 0.00035  9.95204E-01 0.00032 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56659E+00 0.00035  3.34940E-01 0.00032 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.86255E-03 0.00046  6.29499E-02 0.00073 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69506E-02 0.00022  6.40483E-02 0.00084 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11336E-01 8.9E-05  1.80398E-02 0.00037  1.10496E-03 0.00537  1.47241E+00 0.00037 ];
INF_S1                    (idx, [1:   8]) = [  2.39198E-01 0.00019  5.25350E-03 0.00092  4.72731E-04 0.00593  3.89392E-01 0.00039 ];
INF_S2                    (idx, [1:   8]) = [  9.80238E-02 0.00033 -1.58364E-03 0.00202  2.57323E-04 0.00854  9.19304E-02 0.00091 ];
INF_S3                    (idx, [1:   8]) = [  9.22645E-03 0.00257 -1.86118E-03 0.00196  9.48997E-05 0.02202  2.75045E-02 0.00219 ];
INF_S4                    (idx, [1:   8]) = [ -9.72875E-03 0.00195 -6.10855E-04 0.00416  2.43490E-06 0.70963 -8.82285E-03 0.00534 ];
INF_S5                    (idx, [1:   8]) = [  9.58715E-05 0.20963  2.32838E-05 0.09962 -3.45510E-05 0.04012  6.48937E-03 0.00759 ];
INF_S6                    (idx, [1:   8]) = [  5.22500E-03 0.00313 -1.40032E-04 0.02019 -4.45635E-05 0.03615 -1.67000E-02 0.00381 ];
INF_S7                    (idx, [1:   8]) = [  9.07875E-04 0.01619 -1.70640E-04 0.01377 -4.08597E-05 0.03637  5.47885E-04 0.07609 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11377E-01 8.9E-05  1.80398E-02 0.00037  1.10496E-03 0.00537  1.47241E+00 0.00037 ];
INF_SP1                   (idx, [1:   8]) = [  2.39198E-01 0.00019  5.25350E-03 0.00092  4.72731E-04 0.00593  3.89392E-01 0.00039 ];
INF_SP2                   (idx, [1:   8]) = [  9.80241E-02 0.00033 -1.58364E-03 0.00202  2.57323E-04 0.00854  9.19304E-02 0.00091 ];
INF_SP3                   (idx, [1:   8]) = [  9.22652E-03 0.00256 -1.86118E-03 0.00196  9.48997E-05 0.02202  2.75045E-02 0.00219 ];
INF_SP4                   (idx, [1:   8]) = [ -9.72859E-03 0.00196 -6.10855E-04 0.00416  2.43490E-06 0.70963 -8.82285E-03 0.00534 ];
INF_SP5                   (idx, [1:   8]) = [  9.59249E-05 0.20896  2.32838E-05 0.09962 -3.45510E-05 0.04012  6.48937E-03 0.00759 ];
INF_SP6                   (idx, [1:   8]) = [  5.22533E-03 0.00313 -1.40032E-04 0.02019 -4.45635E-05 0.03615 -1.67000E-02 0.00381 ];
INF_SP7                   (idx, [1:   8]) = [  9.07566E-04 0.01624 -1.70640E-04 0.01377 -4.08597E-05 0.03637  5.47885E-04 0.07609 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32099E-01 0.00063  1.13585E+00 0.00564 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33802E-01 0.00092  1.22037E+00 0.00734 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33708E-01 0.00106  1.22077E+00 0.00740 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28864E-01 0.00073  9.98580E-01 0.00671 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43618E+00 0.00063  2.93692E-01 0.00568 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42574E+00 0.00092  2.73491E-01 0.00731 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42632E+00 0.00106  2.73416E-01 0.00753 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45649E+00 0.00073  3.34167E-01 0.00668 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.98038E-03 0.00864  1.78335E-04 0.05013  9.81888E-04 0.02204  9.29225E-04 0.02183  2.70347E-03 0.01300  9.26606E-04 0.02253  2.60857E-04 0.04064 ];
LAMBDA                    (idx, [1:  14]) = [  7.68190E-01 0.02011  1.25024E-02 0.00041  3.12669E-02 0.00056  1.10216E-01 0.00054  3.22063E-01 0.00039  1.33519E+00 0.00104  8.97327E+00 0.00401 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0002' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:56:06 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 04:55:41 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595300166 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.87663E-01  1.00829E+00  1.01142E+00  1.01358E+00  9.79051E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.1E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12850E-02 0.00108  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88715E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.03885E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04307E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67987E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.60726E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.60636E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.77397E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.23233E-01 0.00109  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001260 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00025E+04 0.00085 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00025E+04 0.00085 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.95792E+02 ;
RUNNING_TIME              (idx, 1)        =  5.95884E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.55900E-01  4.55900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.00950E-01  1.29833E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  5.83751E+01  3.38995E+00  2.60678E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.51600E-01  3.03000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.72167E-02  6.66670E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  5.95883E+01  1.26400E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96391 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99978E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76453E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.39288E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.79139E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.03121E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.78854E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.24854E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.60431E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.66651E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  9.71199E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.35124E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.12103E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.49282E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.59096E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.60195E+07 ;
SR90_ACTIVITY             (idx, 1)        =  7.96669E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.28514E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.34641E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.40903E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.93229E+12 ;
CS137_ACTIVITY            (idx, 1)        =  9.45093E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.20893E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.43648E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.82944E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.24930E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.56068E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 9 ;
BURNUP                     (idx, [1:  2])  = [  7.00000E+00  7.00063E+00 ];
BURN_DAYS                 (idx, 1)        =  1.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.83667E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  8.97915E+15 0.00086  6.53087E-01 0.00055 ];
U238_FISS                 (idx, [1:   4]) = [  9.89302E+14 0.00282  7.19486E-02 0.00267 ];
PU239_FISS                (idx, [1:   4]) = [  3.55355E+15 0.00141  2.58466E-01 0.00127 ];
PU240_FISS                (idx, [1:   4]) = [  3.05057E+12 0.04842  2.21902E-04 0.04839 ];
PU241_FISS                (idx, [1:   4]) = [  2.16969E+14 0.00547  1.57827E-02 0.00547 ];
U235_CAPT                 (idx, [1:   4]) = [  1.96947E+15 0.00185  9.12083E-02 0.00185 ];
U238_CAPT                 (idx, [1:   4]) = [  8.89075E+15 0.00100  4.11687E-01 0.00069 ];
PU239_CAPT                (idx, [1:   4]) = [  1.97228E+15 0.00187  9.13293E-02 0.00176 ];
PU240_CAPT                (idx, [1:   4]) = [  8.41017E+14 0.00300  3.89442E-02 0.00293 ];
PU241_CAPT                (idx, [1:   4]) = [  7.76368E+13 0.00988  3.59427E-03 0.00982 ];
XE135_CAPT                (idx, [1:   4]) = [  7.45938E+14 0.00293  3.45442E-02 0.00291 ];
SM149_CAPT                (idx, [1:   4]) = [  1.84184E+14 0.00635  8.52995E-03 0.00636 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001260 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.94260E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001260 5.00794E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3028348 3.03245E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1928139 1.93072E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44773 4.47785E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001260 5.00794E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.72529E-09 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.55434E+16 2.0E-05  3.55434E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37562E+16 3.6E-06  1.37562E+16 3.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.15902E+16 0.00043  1.57322E+16 0.00042  5.85801E+15 0.00107 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.53464E+16 0.00026  2.94884E+16 0.00022  5.85801E+15 0.00107 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.56068E+16 0.00050  3.56068E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.70040E+18 0.00047  4.52187E+17 0.00045  1.24821E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.18920E+14 0.00476 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.56653E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.28597E+18 0.00061 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11700E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11700E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.59067E+00 0.00051 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.74330E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.62368E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23405E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94173E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96853E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.00672E+00 0.00059 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.97707E-01 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.58382E+00 2.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04202E+02 3.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.97860E-01 0.00060  9.91883E-01 0.00059  5.82409E-03 0.00948 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.98172E-01 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  9.98344E-01 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.98172E-01 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.00719E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73028E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73005E+01 8.6E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.13783E-07 0.00369 ];
IMP_EALF                  (idx, [1:   2]) = [  6.13412E-07 0.00148 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.43660E-01 0.00292 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.44017E-01 0.00110 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.97932E-03 0.00608  1.65465E-04 0.03603  1.01270E-03 0.01387  9.31980E-04 0.01562  2.69270E-03 0.00877  8.90132E-04 0.01512  2.86340E-04 0.02573 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.98899E-01 0.01387  9.87377E-03 0.02308  3.12301E-02 0.00039  1.10226E-01 0.00040  3.21856E-01 0.00027  1.33075E+00 0.00088  8.47046E+00 0.01165 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.83704E-03 0.00894  1.71898E-04 0.05284  9.87868E-04 0.02149  9.02159E-04 0.02289  2.60471E-03 0.01283  8.92640E-04 0.02343  2.77761E-04 0.03956 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.96273E-01 0.01981  1.24976E-02 0.00029  3.12301E-02 0.00059  1.10312E-01 0.00060  3.21808E-01 0.00041  1.33098E+00 0.00139  9.02998E+00 0.00385 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.73853E-05 0.00128  2.73764E-05 0.00128  2.89305E-05 0.01529 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.73222E-05 0.00115  2.73133E-05 0.00115  2.88625E-05 0.01523 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.83954E-03 0.00964  1.74904E-04 0.05793  9.77619E-04 0.02248  8.93599E-04 0.02502  2.62954E-03 0.01453  8.92402E-04 0.02393  2.71478E-04 0.04476 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.92251E-01 0.02327  1.25057E-02 0.00061  3.12403E-02 0.00071  1.10285E-01 0.00072  3.21843E-01 0.00045  1.33068E+00 0.00159  9.01635E+00 0.00489 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.71835E-05 0.00303  2.71794E-05 0.00304  2.66571E-05 0.03397 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.71205E-05 0.00297  2.71162E-05 0.00298  2.65956E-05 0.03398 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.61387E-03 0.03260  2.09858E-04 0.18616  8.98718E-04 0.07778  9.66598E-04 0.07520  2.36974E-03 0.04711  8.99938E-04 0.08031  2.69011E-04 0.15321 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.08132E-01 0.08062  1.25108E-02 0.00171  3.12719E-02 0.00163  1.10258E-01 0.00158  3.22230E-01 0.00138  1.32618E+00 0.00412  8.98885E+00 0.01293 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.62056E-03 0.03208  2.11937E-04 0.18052  9.11869E-04 0.07468  9.33798E-04 0.07253  2.40907E-03 0.04664  8.83769E-04 0.07856  2.70114E-04 0.15298 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.94777E-01 0.07935  1.25108E-02 0.00171  3.12719E-02 0.00163  1.10277E-01 0.00158  3.22230E-01 0.00136  1.32598E+00 0.00414  8.99810E+00 0.01297 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.07762E+02 0.03305 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.73038E-05 0.00083 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.72406E-05 0.00059 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.74144E-03 0.00585 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.10416E+02 0.00601 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.05916E-07 0.00066 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.78463E-06 0.00043  2.78447E-06 0.00043  2.81196E-06 0.00509 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.05948E-05 0.00083  4.06149E-05 0.00083  3.73473E-05 0.01036 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.59975E-01 0.00032  6.59889E-01 0.00033  6.87748E-01 0.00981 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04720E+01 0.01458 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.60636E+01 0.00046  3.45158E+01 0.00048 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.62860E+04 0.00332  2.99311E+05 0.00116  6.08387E+05 0.00105  6.50515E+05 0.00087  5.98167E+05 0.00072  6.41394E+05 0.00067  4.35140E+05 0.00068  3.85145E+05 0.00077  2.94830E+05 0.00071  2.40803E+05 0.00094  2.07608E+05 0.00068  1.87277E+05 0.00102  1.72810E+05 0.00075  1.64256E+05 0.00070  1.60133E+05 0.00077  1.38394E+05 0.00110  1.36549E+05 0.00104  1.35180E+05 0.00089  1.33115E+05 0.00097  2.59820E+05 0.00068  2.50624E+05 0.00076  1.81107E+05 0.00063  1.17637E+05 0.00098  1.36028E+05 0.00082  1.29065E+05 0.00102  1.15906E+05 0.00090  1.89965E+05 0.00066  4.37379E+04 0.00193  5.46824E+04 0.00138  4.96832E+04 0.00157  2.88764E+04 0.00247  5.01327E+04 0.00174  3.39223E+04 0.00189  2.84334E+04 0.00165  5.21552E+03 0.00346  4.82503E+03 0.00338  4.50875E+03 0.00289  4.37027E+03 0.00378  4.44044E+03 0.00421  4.75071E+03 0.00301  5.28152E+03 0.00321  5.14157E+03 0.00336  9.87952E+03 0.00277  1.58815E+04 0.00223  2.03992E+04 0.00201  5.42438E+04 0.00130  5.69608E+04 0.00153  6.15687E+04 0.00097  4.09124E+04 0.00134  2.96074E+04 0.00174  2.22222E+04 0.00110  2.62475E+04 0.00128  5.11663E+04 0.00142  7.18718E+04 0.00116  1.42029E+05 0.00117  2.22491E+05 0.00120  3.35760E+05 0.00119  2.15696E+05 0.00147  1.55064E+05 0.00126  1.11586E+05 0.00139  1.00359E+05 0.00129  9.90435E+04 0.00150  8.28237E+04 0.00129  5.60563E+04 0.00153  5.18828E+04 0.00138  4.62172E+04 0.00152  3.90128E+04 0.00166  3.07842E+04 0.00177  2.06384E+04 0.00165  7.31380E+03 0.00220 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.00737E+00 0.00061 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.33060E+18 0.00063  3.69829E+17 0.00086 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38353E-01 0.00011  1.54033E+00 0.00030 ];
INF_CAPT                  (idx, [1:   4]) = [  7.06358E-03 0.00060  3.29680E-02 0.00043 ];
INF_ABS                   (idx, [1:   4]) = [  8.99202E-03 0.00051  6.32305E-02 0.00063 ];
INF_FISS                  (idx, [1:   4]) = [  1.92844E-03 0.00046  3.02625E-02 0.00086 ];
INF_NSF                   (idx, [1:   4]) = [  5.06835E-03 0.00044  7.78846E-02 0.00089 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.62821E+00 5.9E-05  2.57363E+00 4.2E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04684E+02 6.7E-06  2.04092E+02 7.3E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.78275E-08 0.00035  2.57798E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29357E-01 0.00011  1.47713E+00 0.00034 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44493E-01 0.00020  3.90845E-01 0.00047 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64238E-02 0.00033  9.23502E-02 0.00076 ];
INF_SCATT3                (idx, [1:   4]) = [  7.36774E-03 0.00292  2.77416E-02 0.00236 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03569E-02 0.00185 -8.83607E-03 0.00476 ];
INF_SCATT5                (idx, [1:   4]) = [  1.33389E-04 0.09801  6.44860E-03 0.00555 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10157E-03 0.00316 -1.68482E-02 0.00273 ];
INF_SCATT7                (idx, [1:   4]) = [  7.55213E-04 0.02250  5.17256E-04 0.07923 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29399E-01 0.00011  1.47713E+00 0.00034 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44493E-01 0.00020  3.90845E-01 0.00047 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64237E-02 0.00033  9.23502E-02 0.00076 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.36704E-03 0.00291  2.77416E-02 0.00236 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03572E-02 0.00185 -8.83607E-03 0.00476 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.33635E-04 0.09745  6.44860E-03 0.00555 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10155E-03 0.00316 -1.68482E-02 0.00273 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.55405E-04 0.02246  5.17256E-04 0.07923 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12701E-01 0.00023  9.98021E-01 0.00025 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56715E+00 0.00023  3.33995E-01 0.00025 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.94951E-03 0.00049  6.32305E-02 0.00063 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69545E-02 0.00018  6.43095E-02 0.00084 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11399E-01 0.00011  1.79579E-02 0.00041  1.10790E-03 0.00566  1.47602E+00 0.00034 ];
INF_S1                    (idx, [1:   8]) = [  2.39259E-01 0.00020  5.23319E-03 0.00078  4.76009E-04 0.00786  3.90369E-01 0.00047 ];
INF_S2                    (idx, [1:   8]) = [  9.80008E-02 0.00033 -1.57693E-03 0.00232  2.57413E-04 0.00850  9.20928E-02 0.00076 ];
INF_S3                    (idx, [1:   8]) = [  9.21489E-03 0.00226 -1.84715E-03 0.00126  9.18306E-05 0.02112  2.76498E-02 0.00236 ];
INF_S4                    (idx, [1:   8]) = [ -9.75564E-03 0.00183 -6.01283E-04 0.00516  8.98987E-07 1.00000 -8.83697E-03 0.00479 ];
INF_S5                    (idx, [1:   8]) = [  1.08484E-04 0.11650  2.49045E-05 0.10353 -3.67997E-05 0.03586  6.48540E-03 0.00546 ];
INF_S6                    (idx, [1:   8]) = [  5.24741E-03 0.00295 -1.45837E-04 0.02047 -4.69841E-05 0.02445 -1.68012E-02 0.00272 ];
INF_S7                    (idx, [1:   8]) = [  9.30691E-04 0.01827 -1.75478E-04 0.01322 -4.34869E-05 0.03274  5.60743E-04 0.07341 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11441E-01 0.00011  1.79579E-02 0.00041  1.10790E-03 0.00566  1.47602E+00 0.00034 ];
INF_SP1                   (idx, [1:   8]) = [  2.39260E-01 0.00020  5.23319E-03 0.00078  4.76009E-04 0.00786  3.90369E-01 0.00047 ];
INF_SP2                   (idx, [1:   8]) = [  9.80006E-02 0.00033 -1.57693E-03 0.00232  2.57413E-04 0.00850  9.20928E-02 0.00076 ];
INF_SP3                   (idx, [1:   8]) = [  9.21419E-03 0.00226 -1.84715E-03 0.00126  9.18306E-05 0.02112  2.76498E-02 0.00236 ];
INF_SP4                   (idx, [1:   8]) = [ -9.75587E-03 0.00183 -6.01283E-04 0.00516  8.98987E-07 1.00000 -8.83697E-03 0.00479 ];
INF_SP5                   (idx, [1:   8]) = [  1.08730E-04 0.11581  2.49045E-05 0.10353 -3.67997E-05 0.03586  6.48540E-03 0.00546 ];
INF_SP6                   (idx, [1:   8]) = [  5.24739E-03 0.00296 -1.45837E-04 0.02047 -4.69841E-05 0.02445 -1.68012E-02 0.00272 ];
INF_SP7                   (idx, [1:   8]) = [  9.30884E-04 0.01824 -1.75478E-04 0.01322 -4.34869E-05 0.03274  5.60743E-04 0.07341 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31990E-01 0.00072  1.14087E+00 0.00687 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33749E-01 0.00108  1.22631E+00 0.00763 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33622E-01 0.00096  1.22338E+00 0.00978 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28678E-01 0.00080  1.00432E+00 0.00641 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43686E+00 0.00072  2.92503E-01 0.00680 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42607E+00 0.00108  2.72198E-01 0.00765 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42684E+00 0.00096  2.73085E-01 0.00961 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45768E+00 0.00080  3.32226E-01 0.00640 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.83704E-03 0.00894  1.71898E-04 0.05284  9.87868E-04 0.02149  9.02159E-04 0.02289  2.60471E-03 0.01283  8.92640E-04 0.02343  2.77761E-04 0.03956 ];
LAMBDA                    (idx, [1:  14]) = [  7.96273E-01 0.01981  1.24976E-02 0.00029  3.12301E-02 0.00059  1.10312E-01 0.00060  3.21808E-01 0.00041  1.33098E+00 0.00139  9.02998E+00 0.00385 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0002' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:56:06 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:01:44 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595300166 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.81716E-01  1.00139E+00  1.01132E+00  1.01434E+00  9.91224E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.3E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12701E-02 0.00107  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88730E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04605E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05026E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67833E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.60227E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.60137E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.72179E+00 0.00041  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.21518E-01 0.00109  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000928 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00019E+04 0.00084 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00019E+04 0.00084 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.25978E+02 ;
RUNNING_TIME              (idx, 1)        =  6.56331E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.55900E-01  4.55900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.25017E-01  1.20000E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.43329E+01  3.37655E+00  2.58120E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  6.13867E-01  3.05333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.02334E-02  6.66666E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  6.56329E+01  1.26195E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96667 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00086E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.77077E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.41955E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.78313E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.86750E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.80711E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.26149E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61241E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.65695E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.07725E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.40246E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.91913E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.59022E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.85341E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.64344E+07 ;
SR90_ACTIVITY             (idx, 1)        =  8.97936E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.30384E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.37250E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.43446E+14 ;
CS134_ACTIVITY            (idx, 1)        =  5.14721E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.07987E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.23064E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.41788E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  4.07642E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.25618E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.61216E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 10 ;
BURNUP                     (idx, [1:  2])  = [  8.00000E+00  8.00072E+00 ];
BURN_DAYS                 (idx, 1)        =  2.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.93864E-01 0.00110 ];
U235_FISS                 (idx, [1:   4]) = [  8.61341E+15 0.00079  6.27111E-01 0.00057 ];
U238_FISS                 (idx, [1:   4]) = [  1.00157E+15 0.00264  7.29090E-02 0.00246 ];
PU239_FISS                (idx, [1:   4]) = [  3.83269E+15 0.00133  2.79034E-01 0.00114 ];
PU240_FISS                (idx, [1:   4]) = [  3.34816E+12 0.04822  2.44007E-04 0.04834 ];
PU241_FISS                (idx, [1:   4]) = [  2.76689E+14 0.00527  2.01429E-02 0.00521 ];
U235_CAPT                 (idx, [1:   4]) = [  1.89641E+15 0.00191  8.57658E-02 0.00192 ];
U238_CAPT                 (idx, [1:   4]) = [  8.97262E+15 0.00108  4.05724E-01 0.00076 ];
PU239_CAPT                (idx, [1:   4]) = [  2.12570E+15 0.00185  9.61304E-02 0.00181 ];
PU240_CAPT                (idx, [1:   4]) = [  9.75880E+14 0.00277  4.41279E-02 0.00267 ];
PU241_CAPT                (idx, [1:   4]) = [  1.00067E+14 0.00888  4.52493E-03 0.00886 ];
XE135_CAPT                (idx, [1:   4]) = [  7.53000E+14 0.00319  3.40516E-02 0.00314 ];
SM149_CAPT                (idx, [1:   4]) = [  1.89966E+14 0.00592  8.59007E-03 0.00589 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000928 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.83197E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000928 5.00783E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3056762 3.06105E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1898755 1.90136E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45411 4.54175E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000928 5.00783E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.25963E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.56743E+16 2.0E-05  3.56743E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37454E+16 3.7E-06  1.37454E+16 3.7E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.21095E+16 0.00046  1.61777E+16 0.00046  5.93177E+15 0.00112 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.58549E+16 0.00028  2.99231E+16 0.00025  5.93177E+15 0.00112 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.61216E+16 0.00052  3.61216E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.72230E+18 0.00048  4.57302E+17 0.00043  1.26500E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.28137E+14 0.00489 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.61830E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.30270E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11584E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11584E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.58054E+00 0.00051 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.74129E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.59679E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23402E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94056E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96842E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.95969E-01 0.00059 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.86922E-01 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.59536E+00 2.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04363E+02 3.7E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.86931E-01 0.00061  9.81175E-01 0.00059  5.74722E-03 0.00902 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.87518E-01 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  9.87746E-01 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.87518E-01 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  9.96568E-01 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72757E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72749E+01 8.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.30485E-07 0.00360 ];
IMP_EALF                  (idx, [1:   2]) = [  6.29347E-07 0.00154 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.47338E-01 0.00276 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.47426E-01 0.00117 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.97616E-03 0.00577  1.73291E-04 0.03387  1.03754E-03 0.01399  9.44815E-04 0.01522  2.63461E-03 0.00881  8.96332E-04 0.01568  2.89578E-04 0.02613 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.95450E-01 0.01378  1.03991E-02 0.02012  3.11914E-02 0.00039  1.10325E-01 0.00039  3.22024E-01 0.00027  1.32842E+00 0.00106  8.53165E+00 0.01021 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.82888E-03 0.00831  1.67302E-04 0.04834  1.01838E-03 0.02217  9.14781E-04 0.02289  2.59711E-03 0.01310  8.74075E-04 0.02279  2.57229E-04 0.04057 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.53461E-01 0.01956  1.25016E-02 0.00042  3.11974E-02 0.00057  1.10343E-01 0.00058  3.22029E-01 0.00040  1.32981E+00 0.00131  8.86582E+00 0.00549 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.74870E-05 0.00133  2.74767E-05 0.00134  2.91808E-05 0.01404 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.71224E-05 0.00117  2.71123E-05 0.00117  2.87920E-05 0.01401 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.80210E-03 0.00906  1.66230E-04 0.05763  9.91301E-04 0.02369  9.18280E-04 0.02443  2.58388E-03 0.01365  8.90462E-04 0.02395  2.51946E-04 0.04392 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.51603E-01 0.02283  1.24967E-02 0.00043  3.11912E-02 0.00074  1.10332E-01 0.00074  3.22026E-01 0.00048  1.33068E+00 0.00156  8.74215E+00 0.00844 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.71539E-05 0.00314  2.71402E-05 0.00316  2.83541E-05 0.03861 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.67929E-05 0.00306  2.67795E-05 0.00308  2.79576E-05 0.03855 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.96158E-03 0.03013  1.88065E-04 0.19365  9.87366E-04 0.08115  9.58723E-04 0.08050  2.67231E-03 0.04406  8.98867E-04 0.07906  2.56254E-04 0.13977 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.93878E-01 0.07690  1.25123E-02 0.00177  3.11778E-02 0.00175  1.09838E-01 0.00150  3.21776E-01 0.00128  1.33087E+00 0.00388  9.01003E+00 0.01424 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.88855E-03 0.02897  2.00944E-04 0.18751  9.85639E-04 0.08132  9.43533E-04 0.07762  2.64523E-03 0.04191  8.69406E-04 0.07650  2.43793E-04 0.13667 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.81760E-01 0.07596  1.25123E-02 0.00177  3.11821E-02 0.00174  1.09843E-01 0.00150  3.21795E-01 0.00128  1.33036E+00 0.00393  9.01150E+00 0.01414 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.20323E+02 0.03007 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.73409E-05 0.00083 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.69788E-05 0.00059 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.76562E-03 0.00584 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.10983E+02 0.00597 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.03989E-07 0.00068 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.77474E-06 0.00045  2.77467E-06 0.00045  2.78969E-06 0.00529 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.05591E-05 0.00088  4.05832E-05 0.00088  3.65512E-05 0.00972 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.57231E-01 0.00033  6.57187E-01 0.00033  6.75658E-01 0.00920 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08250E+01 0.01425 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.60137E+01 0.00047  3.43981E+01 0.00051 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.67657E+04 0.00336  3.00085E+05 0.00173  6.08425E+05 0.00085  6.50897E+05 0.00082  5.97402E+05 0.00070  6.40208E+05 0.00068  4.35733E+05 0.00066  3.85344E+05 0.00085  2.94472E+05 0.00091  2.40738E+05 0.00070  2.07240E+05 0.00088  1.87113E+05 0.00110  1.72671E+05 0.00073  1.64071E+05 0.00102  1.60009E+05 0.00080  1.38267E+05 0.00089  1.36520E+05 0.00090  1.35390E+05 0.00103  1.33121E+05 0.00105  2.59626E+05 0.00069  2.50876E+05 0.00075  1.81334E+05 0.00071  1.17444E+05 0.00087  1.36022E+05 0.00112  1.28685E+05 0.00088  1.15859E+05 0.00112  1.89557E+05 0.00082  4.36617E+04 0.00193  5.47237E+04 0.00165  4.96664E+04 0.00135  2.88874E+04 0.00210  5.03328E+04 0.00155  3.38423E+04 0.00156  2.83404E+04 0.00174  5.22362E+03 0.00369  4.77452E+03 0.00317  4.34768E+03 0.00347  4.24006E+03 0.00387  4.29846E+03 0.00324  4.63818E+03 0.00322  5.24244E+03 0.00351  5.05503E+03 0.00451  9.73769E+03 0.00335  1.58247E+04 0.00206  2.03410E+04 0.00200  5.39344E+04 0.00141  5.69377E+04 0.00139  6.11670E+04 0.00125  4.04982E+04 0.00125  2.91061E+04 0.00152  2.19143E+04 0.00143  2.59477E+04 0.00182  5.06033E+04 0.00124  7.10464E+04 0.00111  1.40628E+05 0.00119  2.21004E+05 0.00113  3.33987E+05 0.00126  2.14754E+05 0.00121  1.54207E+05 0.00149  1.11176E+05 0.00140  1.00041E+05 0.00170  9.87244E+04 0.00120  8.26054E+04 0.00150  5.58763E+04 0.00132  5.16503E+04 0.00152  4.59978E+04 0.00169  3.88692E+04 0.00163  3.07277E+04 0.00160  2.06013E+04 0.00187  7.30395E+03 0.00240 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.96800E-01 0.00057 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.34932E+18 0.00059  3.73022E+17 0.00112 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38448E-01 0.00015  1.54465E+00 0.00039 ];
INF_CAPT                  (idx, [1:   4]) = [  7.16473E-03 0.00066  3.33579E-02 0.00055 ];
INF_ABS                   (idx, [1:   4]) = [  9.06765E-03 0.00051  6.33287E-02 0.00082 ];
INF_FISS                  (idx, [1:   4]) = [  1.90292E-03 0.00051  2.99708E-02 0.00114 ];
INF_NSF                   (idx, [1:   4]) = [  5.01448E-03 0.00053  7.75113E-02 0.00116 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.63514E+00 6.5E-05  2.58622E+00 4.8E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04780E+02 8.2E-06  2.04267E+02 8.2E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.76907E-08 0.00043  2.58022E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29378E-01 0.00015  1.48135E+00 0.00044 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44512E-01 0.00025  3.91940E-01 0.00056 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64625E-02 0.00035  9.26022E-02 0.00088 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35307E-03 0.00307  2.77867E-02 0.00196 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03462E-02 0.00175 -8.76753E-03 0.00601 ];
INF_SCATT5                (idx, [1:   4]) = [  1.48709E-04 0.11563  6.62948E-03 0.00767 ];
INF_SCATT6                (idx, [1:   4]) = [  5.11148E-03 0.00301 -1.68715E-02 0.00355 ];
INF_SCATT7                (idx, [1:   4]) = [  7.57015E-04 0.02190  4.31825E-04 0.12018 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29420E-01 0.00015  1.48135E+00 0.00044 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44513E-01 0.00025  3.91940E-01 0.00056 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64627E-02 0.00035  9.26022E-02 0.00088 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35305E-03 0.00307  2.77867E-02 0.00196 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03461E-02 0.00176 -8.76753E-03 0.00601 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.48581E-04 0.11543  6.62948E-03 0.00767 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.11148E-03 0.00301 -1.68715E-02 0.00355 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.57001E-04 0.02191  4.31825E-04 0.12018 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12570E-01 0.00033  1.00117E+00 0.00033 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56811E+00 0.00033  3.32943E-01 0.00033 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.02572E-03 0.00051  6.33287E-02 0.00082 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69620E-02 0.00019  6.44185E-02 0.00089 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11486E-01 0.00015  1.78924E-02 0.00048  1.11758E-03 0.00409  1.48023E+00 0.00044 ];
INF_S1                    (idx, [1:   8]) = [  2.39309E-01 0.00025  5.20314E-03 0.00085  4.77317E-04 0.00759  3.91463E-01 0.00056 ];
INF_S2                    (idx, [1:   8]) = [  9.80423E-02 0.00035 -1.57983E-03 0.00176  2.57193E-04 0.00931  9.23450E-02 0.00088 ];
INF_S3                    (idx, [1:   8]) = [  9.20555E-03 0.00244 -1.85249E-03 0.00179  8.98056E-05 0.02244  2.76969E-02 0.00195 ];
INF_S4                    (idx, [1:   8]) = [ -9.74285E-03 0.00184 -6.03371E-04 0.00423 -3.78733E-06 0.54224 -8.76374E-03 0.00590 ];
INF_S5                    (idx, [1:   8]) = [  1.20679E-04 0.13790  2.80307E-05 0.09013 -4.12133E-05 0.03538  6.67069E-03 0.00763 ];
INF_S6                    (idx, [1:   8]) = [  5.24925E-03 0.00287 -1.37767E-04 0.01759 -5.01173E-05 0.03141 -1.68214E-02 0.00357 ];
INF_S7                    (idx, [1:   8]) = [  9.21263E-04 0.01886 -1.64248E-04 0.01627 -4.53083E-05 0.03354  4.77133E-04 0.10897 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11528E-01 0.00015  1.78924E-02 0.00048  1.11758E-03 0.00409  1.48023E+00 0.00044 ];
INF_SP1                   (idx, [1:   8]) = [  2.39309E-01 0.00025  5.20314E-03 0.00085  4.77317E-04 0.00759  3.91463E-01 0.00056 ];
INF_SP2                   (idx, [1:   8]) = [  9.80425E-02 0.00035 -1.57983E-03 0.00176  2.57193E-04 0.00931  9.23450E-02 0.00088 ];
INF_SP3                   (idx, [1:   8]) = [  9.20554E-03 0.00244 -1.85249E-03 0.00179  8.98056E-05 0.02244  2.76969E-02 0.00195 ];
INF_SP4                   (idx, [1:   8]) = [ -9.74275E-03 0.00184 -6.03371E-04 0.00423 -3.78733E-06 0.54224 -8.76374E-03 0.00590 ];
INF_SP5                   (idx, [1:   8]) = [  1.20550E-04 0.13773  2.80307E-05 0.09013 -4.12133E-05 0.03538  6.67069E-03 0.00763 ];
INF_SP6                   (idx, [1:   8]) = [  5.24925E-03 0.00287 -1.37767E-04 0.01759 -5.01173E-05 0.03141 -1.68214E-02 0.00357 ];
INF_SP7                   (idx, [1:   8]) = [  9.21249E-04 0.01887 -1.64248E-04 0.01627 -4.53083E-05 0.03354  4.77133E-04 0.10897 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31958E-01 0.00068  1.15254E+00 0.00780 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33838E-01 0.00095  1.23343E+00 0.00931 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33536E-01 0.00084  1.24286E+00 0.00921 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28582E-01 0.00087  1.01334E+00 0.00718 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43706E+00 0.00068  2.89628E-01 0.00763 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42552E+00 0.00095  2.70807E-01 0.00924 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42736E+00 0.00084  2.68739E-01 0.00910 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45829E+00 0.00087  3.29340E-01 0.00695 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.82888E-03 0.00831  1.67302E-04 0.04834  1.01838E-03 0.02217  9.14781E-04 0.02289  2.59711E-03 0.01310  8.74075E-04 0.02279  2.57229E-04 0.04057 ];
LAMBDA                    (idx, [1:  14]) = [  7.53461E-01 0.01956  1.25016E-02 0.00042  3.11974E-02 0.00057  1.10343E-01 0.00058  3.22029E-01 0.00040  1.32981E+00 0.00131  8.86582E+00 0.00549 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0002' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:56:06 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:07:47 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595300166 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.80240E-01  9.74676E-01  1.01678E+00  1.01877E+00  1.00953E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12561E-02 0.00107  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88744E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04924E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05346E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67585E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.59421E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.59330E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.68448E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.19550E-01 0.00109  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000753 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00015E+04 0.00083 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00015E+04 0.00083 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.56191E+02 ;
RUNNING_TIME              (idx, 1)        =  7.16831E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.55900E-01  4.55900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.47817E-01  1.07833E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.02997E+01  3.39058E+00  2.57623E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  6.73700E-01  2.79167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.31834E-02  6.83331E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.16829E+01  1.25955E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96896 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99984E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.77586E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.44444E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.77602E+04 ;
TOT_SF_RATE               (idx, 1)        =  4.06175E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.82534E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.27427E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61907E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.64856E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.18832E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.44847E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.79282E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.68681E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.09041E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.67979E+07 ;
SR90_ACTIVITY             (idx, 1)        =  9.96838E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.32123E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.39614E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.45792E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.50424E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.21456E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.25134E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.40119E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.73369E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.26267E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.65997E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 11 ;
BURNUP                     (idx, [1:  2])  = [  9.00000E+00  9.00081E+00 ];
BURN_DAYS                 (idx, 1)        =  2.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.03550E-01 0.00109 ];
U235_FISS                 (idx, [1:   4]) = [  8.26859E+15 0.00083  6.02289E-01 0.00064 ];
U238_FISS                 (idx, [1:   4]) = [  1.01952E+15 0.00283  7.42452E-02 0.00261 ];
PU239_FISS                (idx, [1:   4]) = [  4.08289E+15 0.00136  2.97382E-01 0.00115 ];
PU240_FISS                (idx, [1:   4]) = [  3.75980E+12 0.04397  2.73838E-04 0.04403 ];
PU241_FISS                (idx, [1:   4]) = [  3.46040E+14 0.00446  2.52026E-02 0.00437 ];
U235_CAPT                 (idx, [1:   4]) = [  1.82705E+15 0.00203  8.08730E-02 0.00198 ];
U238_CAPT                 (idx, [1:   4]) = [  9.04736E+15 0.00102  4.00443E-01 0.00071 ];
PU239_CAPT                (idx, [1:   4]) = [  2.26426E+15 0.00182  1.00230E-01 0.00181 ];
PU240_CAPT                (idx, [1:   4]) = [  1.10424E+15 0.00286  4.88714E-02 0.00272 ];
PU241_CAPT                (idx, [1:   4]) = [  1.24164E+14 0.00782  5.49468E-03 0.00775 ];
XE135_CAPT                (idx, [1:   4]) = [  7.49988E+14 0.00289  3.31977E-02 0.00286 ];
SM149_CAPT                (idx, [1:   4]) = [  1.94443E+14 0.00640  8.60810E-03 0.00643 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000753 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.94205E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000753 5.00794E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3081900 3.08644E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1873031 1.87566E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45822 4.58430E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000753 5.00794E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.72529E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.57972E+16 2.2E-05  3.57972E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37352E+16 4.1E-06  1.37352E+16 4.1E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.25860E+16 0.00045  1.65986E+16 0.00045  5.98745E+15 0.00113 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.63212E+16 0.00028  3.03338E+16 0.00024  5.98745E+15 0.00113 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.65997E+16 0.00050  3.65997E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.74236E+18 0.00046  4.62213E+17 0.00044  1.28014E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.35611E+14 0.00516 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.66568E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.31705E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11467E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11467E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.57158E+00 0.00051 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.74379E-01 0.00035 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.57353E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23351E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94054E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96758E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.86739E-01 0.00059 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.77692E-01 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.60624E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04514E+02 4.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.77827E-01 0.00060  9.72077E-01 0.00060  5.61507E-03 0.00910 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.78123E-01 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  9.78194E-01 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.78123E-01 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  9.87174E-01 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72509E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72514E+01 8.3E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.46645E-07 0.00386 ];
IMP_EALF                  (idx, [1:   2]) = [  6.44267E-07 0.00143 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.51260E-01 0.00286 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.51218E-01 0.00106 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.91599E-03 0.00573  1.60543E-04 0.03720  1.03471E-03 0.01478  9.20405E-04 0.01465  2.64958E-03 0.00831  8.78355E-04 0.01628  2.72401E-04 0.02738 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.69472E-01 0.01407  9.58099E-03 0.02475  3.11478E-02 0.00042  1.10117E-01 0.00205  3.21912E-01 0.00028  1.32641E+00 0.00108  8.31927E+00 0.01251 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.68969E-03 0.00904  1.67539E-04 0.05616  1.00060E-03 0.02174  8.98760E-04 0.02306  2.51169E-03 0.01314  8.46199E-04 0.02407  2.64905E-04 0.04211 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.72365E-01 0.02120  1.25060E-02 0.00046  3.11849E-02 0.00061  1.10388E-01 0.00062  3.22057E-01 0.00042  1.32713E+00 0.00147  8.89561E+00 0.00589 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.76246E-05 0.00139  2.76119E-05 0.00139  2.96277E-05 0.01525 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.70067E-05 0.00122  2.69943E-05 0.00121  2.89624E-05 0.01519 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.74187E-03 0.00918  1.70800E-04 0.06056  1.00487E-03 0.02332  8.91192E-04 0.02399  2.54575E-03 0.01423  8.60311E-04 0.02603  2.68943E-04 0.04457 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.77860E-01 0.02379  1.25108E-02 0.00076  3.11727E-02 0.00076  1.10366E-01 0.00074  3.21917E-01 0.00051  1.32939E+00 0.00178  8.87157E+00 0.00776 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.73098E-05 0.00315  2.72885E-05 0.00315  2.98026E-05 0.03822 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.66998E-05 0.00310  2.66790E-05 0.00310  2.91290E-05 0.03826 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.77601E-03 0.03102  1.71833E-04 0.18257  1.08564E-03 0.07182  9.05748E-04 0.08575  2.42380E-03 0.04818  8.79523E-04 0.07998  3.09459E-04 0.14785 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.82309E-01 0.07831  1.25685E-02 0.00355  3.10985E-02 0.00175  1.10424E-01 0.00190  3.21627E-01 0.00136  1.32781E+00 0.00381  8.86195E+00 0.01786 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.77186E-03 0.03075  1.76947E-04 0.19100  1.08193E-03 0.07092  8.83469E-04 0.08632  2.43388E-03 0.04762  8.95101E-04 0.07686  3.00537E-04 0.14108 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.85184E-01 0.07538  1.25685E-02 0.00355  3.10927E-02 0.00175  1.10430E-01 0.00189  3.21614E-01 0.00134  1.32848E+00 0.00367  8.86222E+00 0.01775 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.11430E+02 0.03080 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.74818E-05 0.00078 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.68678E-05 0.00052 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.73441E-03 0.00678 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.08684E+02 0.00676 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.01538E-07 0.00071 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.75993E-06 0.00042  2.75983E-06 0.00042  2.77902E-06 0.00537 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.04733E-05 0.00087  4.04968E-05 0.00087  3.66067E-05 0.00980 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.54984E-01 0.00031  6.54986E-01 0.00031  6.66663E-01 0.00953 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08111E+01 0.01447 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.59330E+01 0.00047  3.43411E+01 0.00051 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.70314E+04 0.00353  3.00736E+05 0.00147  6.09383E+05 0.00083  6.51160E+05 0.00070  5.97844E+05 0.00066  6.40269E+05 0.00074  4.35249E+05 0.00064  3.84918E+05 0.00074  2.94811E+05 0.00078  2.40995E+05 0.00089  2.07169E+05 0.00068  1.86779E+05 0.00083  1.72707E+05 0.00089  1.64190E+05 0.00100  1.59897E+05 0.00091  1.38357E+05 0.00077  1.36631E+05 0.00106  1.35317E+05 0.00081  1.32713E+05 0.00113  2.59585E+05 0.00068  2.51033E+05 0.00078  1.80919E+05 0.00076  1.17286E+05 0.00101  1.36131E+05 0.00086  1.28815E+05 0.00096  1.15754E+05 0.00105  1.89290E+05 0.00082  4.35917E+04 0.00163  5.46008E+04 0.00139  4.95648E+04 0.00153  2.87502E+04 0.00165  4.99128E+04 0.00141  3.37736E+04 0.00135  2.83135E+04 0.00140  5.14186E+03 0.00352  4.67437E+03 0.00370  4.24082E+03 0.00303  4.06502E+03 0.00408  4.17557E+03 0.00353  4.48478E+03 0.00287  5.12608E+03 0.00355  4.99927E+03 0.00457  9.65911E+03 0.00239  1.57065E+04 0.00239  2.01250E+04 0.00201  5.36870E+04 0.00135  5.65305E+04 0.00117  6.08867E+04 0.00142  4.01752E+04 0.00117  2.88779E+04 0.00140  2.15986E+04 0.00166  2.55921E+04 0.00157  4.99599E+04 0.00148  7.02165E+04 0.00120  1.39334E+05 0.00111  2.19042E+05 0.00141  3.31663E+05 0.00130  2.13601E+05 0.00143  1.53602E+05 0.00168  1.10555E+05 0.00143  9.95761E+04 0.00139  9.83031E+04 0.00166  8.21772E+04 0.00150  5.56897E+04 0.00195  5.14340E+04 0.00153  4.58468E+04 0.00135  3.88080E+04 0.00160  3.05864E+04 0.00174  2.04789E+04 0.00162  7.26307E+03 0.00217 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.87247E-01 0.00059 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.36692E+18 0.00062  3.75473E+17 0.00108 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38440E-01 8.9E-05  1.54755E+00 0.00035 ];
INF_CAPT                  (idx, [1:   4]) = [  7.25259E-03 0.00067  3.37534E-02 0.00064 ];
INF_ABS                   (idx, [1:   4]) = [  9.13044E-03 0.00048  6.35032E-02 0.00087 ];
INF_FISS                  (idx, [1:   4]) = [  1.87785E-03 0.00058  2.97498E-02 0.00115 ];
INF_NSF                   (idx, [1:   4]) = [  4.96193E-03 0.00057  7.72883E-02 0.00119 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.64235E+00 6.0E-05  2.59794E+00 6.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04876E+02 7.9E-06  2.04431E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.74632E-08 0.00033  2.58236E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29307E-01 9.1E-05  1.48406E+00 0.00040 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44460E-01 0.00014  3.92874E-01 0.00055 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64028E-02 0.00024  9.28890E-02 0.00108 ];
INF_SCATT3                (idx, [1:   4]) = [  7.31920E-03 0.00335  2.79482E-02 0.00214 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03530E-02 0.00200 -8.77341E-03 0.00633 ];
INF_SCATT5                (idx, [1:   4]) = [  1.39255E-04 0.12934  6.60123E-03 0.00545 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10271E-03 0.00307 -1.68912E-02 0.00320 ];
INF_SCATT7                (idx, [1:   4]) = [  7.71293E-04 0.01998  5.25876E-04 0.06721 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29350E-01 9.1E-05  1.48406E+00 0.00040 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44460E-01 0.00014  3.92874E-01 0.00055 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64033E-02 0.00024  9.28890E-02 0.00108 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.31927E-03 0.00336  2.79482E-02 0.00214 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03529E-02 0.00200 -8.77341E-03 0.00633 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.39096E-04 0.12933  6.60123E-03 0.00545 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10273E-03 0.00308 -1.68912E-02 0.00320 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.71234E-04 0.01999  5.25876E-04 0.06721 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12491E-01 0.00032  1.00321E+00 0.00029 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56870E+00 0.00032  3.32268E-01 0.00029 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.08791E-03 0.00049  6.35032E-02 0.00087 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69661E-02 0.00018  6.46096E-02 0.00104 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 2.1E-07  2.05771E-07 1.00000 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  9.99964E-01 3.6E-05  3.61976E-05 1.00000 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11474E-01 9.0E-05  1.78332E-02 0.00034  1.11936E-03 0.00285  1.48294E+00 0.00040 ];
INF_S1                    (idx, [1:   8]) = [  2.39265E-01 0.00014  5.19469E-03 0.00064  4.82753E-04 0.00566  3.92391E-01 0.00055 ];
INF_S2                    (idx, [1:   8]) = [  9.79771E-02 0.00025 -1.57431E-03 0.00226  2.60529E-04 0.00807  9.26284E-02 0.00108 ];
INF_S3                    (idx, [1:   8]) = [  9.15744E-03 0.00268 -1.83823E-03 0.00133  9.51368E-05 0.01945  2.78530E-02 0.00217 ];
INF_S4                    (idx, [1:   8]) = [ -9.76279E-03 0.00207 -5.90204E-04 0.00295  3.59675E-06 0.47193 -8.77701E-03 0.00628 ];
INF_S5                    (idx, [1:   8]) = [  1.10607E-04 0.16038  2.86483E-05 0.07280 -3.49858E-05 0.04168  6.63622E-03 0.00547 ];
INF_S6                    (idx, [1:   8]) = [  5.24429E-03 0.00300 -1.41579E-04 0.01705 -4.78100E-05 0.02439 -1.68434E-02 0.00322 ];
INF_S7                    (idx, [1:   8]) = [  9.43619E-04 0.01696 -1.72326E-04 0.01364 -4.55356E-05 0.02537  5.71412E-04 0.06128 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11517E-01 9.0E-05  1.78332E-02 0.00034  1.11936E-03 0.00285  1.48294E+00 0.00040 ];
INF_SP1                   (idx, [1:   8]) = [  2.39265E-01 0.00014  5.19469E-03 0.00064  4.82753E-04 0.00566  3.92391E-01 0.00055 ];
INF_SP2                   (idx, [1:   8]) = [  9.79776E-02 0.00025 -1.57431E-03 0.00226  2.60529E-04 0.00807  9.26284E-02 0.00108 ];
INF_SP3                   (idx, [1:   8]) = [  9.15750E-03 0.00268 -1.83823E-03 0.00133  9.51368E-05 0.01945  2.78530E-02 0.00217 ];
INF_SP4                   (idx, [1:   8]) = [ -9.76265E-03 0.00207 -5.90204E-04 0.00295  3.59675E-06 0.47193 -8.77701E-03 0.00628 ];
INF_SP5                   (idx, [1:   8]) = [  1.10447E-04 0.16036  2.86483E-05 0.07280 -3.49858E-05 0.04168  6.63622E-03 0.00547 ];
INF_SP6                   (idx, [1:   8]) = [  5.24431E-03 0.00301 -1.41579E-04 0.01705 -4.78100E-05 0.02439 -1.68434E-02 0.00322 ];
INF_SP7                   (idx, [1:   8]) = [  9.43559E-04 0.01697 -1.72326E-04 0.01364 -4.55356E-05 0.02537  5.71412E-04 0.06128 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32075E-01 0.00055  1.16466E+00 0.00636 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33829E-01 0.00087  1.26037E+00 0.00510 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33641E-01 0.00109  1.25457E+00 0.00981 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28835E-01 0.00089  1.01598E+00 0.00741 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43633E+00 0.00055  2.86482E-01 0.00628 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42557E+00 0.00087  2.64635E-01 0.00504 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42673E+00 0.00109  2.66296E-01 0.00958 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45668E+00 0.00089  3.28514E-01 0.00725 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.68969E-03 0.00904  1.67539E-04 0.05616  1.00060E-03 0.02174  8.98760E-04 0.02306  2.51169E-03 0.01314  8.46199E-04 0.02407  2.64905E-04 0.04211 ];
LAMBDA                    (idx, [1:  14]) = [  7.72365E-01 0.02120  1.25060E-02 0.00046  3.11849E-02 0.00061  1.10388E-01 0.00062  3.22057E-01 0.00042  1.32713E+00 0.00147  8.89561E+00 0.00589 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0002' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:56:06 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:13:48 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595300166 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.79816E-01  1.00015E+00  1.01819E+00  9.88280E-01  1.01356E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.12559E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88744E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05444E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05866E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67517E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.59207E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.59114E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.65045E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.18792E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000907 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00018E+04 0.00080 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00018E+04 0.00080 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.86231E+02 ;
RUNNING_TIME              (idx, 1)        =  7.76988E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.55900E-01  4.55900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.72533E-01  1.21500E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.62274E+01  3.36660E+00  2.56112E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  7.36333E-01  3.11500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.61000E-02  6.83331E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.76986E+01  1.26005E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97087 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99874E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78028E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.46687E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.76932E+04 ;
TOT_SF_RATE               (idx, 1)        =  5.69486E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.84319E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.28680E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62365E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.64061E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.30450E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.49040E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  6.73789E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.78286E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.30709E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.71211E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.09348E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.33706E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.41778E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.47944E+14 ;
CS134_ACTIVITY            (idx, 1)        =  8.00765E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.34913E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.27030E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.38554E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  7.86278E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.26875E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.71061E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 12 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E+01  1.00009E+01 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.12575E-01 0.00104 ];
U233_FISS                 (idx, [1:   4]) = [  7.36733E+09 1.00000  5.41712E-07 1.00000 ];
U235_FISS                 (idx, [1:   4]) = [  7.96079E+15 0.00086  5.79263E-01 0.00065 ];
U238_FISS                 (idx, [1:   4]) = [  1.03137E+15 0.00279  7.50319E-02 0.00258 ];
PU239_FISS                (idx, [1:   4]) = [  4.32658E+15 0.00123  3.14823E-01 0.00111 ];
PU240_FISS                (idx, [1:   4]) = [  4.28240E+12 0.04300  3.11456E-04 0.04299 ];
PU241_FISS                (idx, [1:   4]) = [  4.11375E+14 0.00375  2.99344E-02 0.00373 ];
U235_CAPT                 (idx, [1:   4]) = [  1.75825E+15 0.00205  7.61960E-02 0.00201 ];
U238_CAPT                 (idx, [1:   4]) = [  9.13155E+15 0.00100  3.95690E-01 0.00069 ];
PU239_CAPT                (idx, [1:   4]) = [  2.38298E+15 0.00168  1.03267E-01 0.00162 ];
PU240_CAPT                (idx, [1:   4]) = [  1.21871E+15 0.00265  5.28093E-02 0.00255 ];
PU241_CAPT                (idx, [1:   4]) = [  1.48662E+14 0.00695  6.44133E-03 0.00687 ];
XE135_CAPT                (idx, [1:   4]) = [  7.54855E+14 0.00296  3.27112E-02 0.00290 ];
SM149_CAPT                (idx, [1:   4]) = [  1.98209E+14 0.00611  8.58913E-03 0.00608 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000907 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.84912E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000907 5.00785E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3105146 3.10955E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1849426 1.85196E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46335 4.63400E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000907 5.00785E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -7.07805E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.59116E+16 2.2E-05  3.59116E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37256E+16 4.2E-06  1.37256E+16 4.2E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.30894E+16 0.00044  1.70252E+16 0.00045  6.06420E+15 0.00113 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.68150E+16 0.00028  3.07508E+16 0.00025  6.06420E+15 0.00113 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.71061E+16 0.00051  3.71061E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.76493E+18 0.00047  4.67350E+17 0.00046  1.29758E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.43925E+14 0.00477 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.71589E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.33440E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11351E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11351E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.56496E+00 0.00051 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.73861E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.54995E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23331E+00 0.00038 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94003E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96709E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.78228E-01 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.69163E-01 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.61640E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04657E+02 4.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.69328E-01 0.00057  9.63788E-01 0.00055  5.37494E-03 0.00950 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.67987E-01 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  9.67933E-01 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.67987E-01 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  9.77042E-01 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72344E+01 0.00023 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72316E+01 9.6E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.57477E-07 0.00394 ];
IMP_EALF                  (idx, [1:   2]) = [  6.57226E-07 0.00165 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.54218E-01 0.00287 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.54051E-01 0.00126 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.89829E-03 0.00596  1.62435E-04 0.03652  1.02296E-03 0.01308  9.15193E-04 0.01481  2.61651E-03 0.00936  9.01642E-04 0.01447  2.79556E-04 0.02853 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.83789E-01 0.01431  9.71306E-03 0.02406  3.10986E-02 0.00044  1.10376E-01 0.00044  3.22253E-01 0.00030  1.32086E+00 0.00122  7.91671E+00 0.01596 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.59301E-03 0.00901  1.51422E-04 0.05424  9.65283E-04 0.02214  8.70852E-04 0.02313  2.47737E-03 0.01361  8.58347E-04 0.02134  2.69730E-04 0.04047 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.87783E-01 0.02086  1.25177E-02 0.00066  3.11039E-02 0.00061  1.10373E-01 0.00064  3.22305E-01 0.00044  1.32078E+00 0.00174  8.73810E+00 0.00731 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.77119E-05 0.00131  2.77033E-05 0.00132  2.91965E-05 0.01544 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.68577E-05 0.00118  2.68493E-05 0.00119  2.82997E-05 0.01547 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.54756E-03 0.00958  1.49728E-04 0.06067  9.49590E-04 0.02267  8.77666E-04 0.02364  2.45017E-03 0.01509  8.53163E-04 0.02453  2.67244E-04 0.04613 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.90364E-01 0.02393  1.25096E-02 0.00083  3.11204E-02 0.00079  1.10383E-01 0.00077  3.22166E-01 0.00051  1.32322E+00 0.00202  8.83873E+00 0.00839 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.71765E-05 0.00312  2.71609E-05 0.00312  2.70279E-05 0.03795 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.63377E-05 0.00304  2.63225E-05 0.00305  2.61834E-05 0.03798 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.19454E-03 0.03297  1.42872E-04 0.19847  7.83852E-04 0.08332  8.40589E-04 0.08324  2.27439E-03 0.05121  8.51327E-04 0.08173  3.01510E-04 0.15491 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  9.12930E-01 0.08283  1.25148E-02 0.00206  3.10392E-02 0.00200  1.10532E-01 0.00195  3.21894E-01 0.00146  1.32832E+00 0.00387  9.15609E+00 0.01261 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.22004E-03 0.03257  1.52687E-04 0.20713  8.04981E-04 0.08181  8.30669E-04 0.08342  2.26742E-03 0.05037  8.66144E-04 0.08029  2.98142E-04 0.14912 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.93151E-01 0.08243  1.25148E-02 0.00206  3.10329E-02 0.00199  1.10486E-01 0.00192  3.21908E-01 0.00145  1.32849E+00 0.00383  9.15821E+00 0.01260 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.92822E+02 0.03337 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.75146E-05 0.00083 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.66662E-05 0.00058 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.42856E-03 0.00663 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.97319E+02 0.00662 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.00139E-07 0.00072 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.74957E-06 0.00041  2.74955E-06 0.00041  2.75200E-06 0.00542 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.04804E-05 0.00089  4.04987E-05 0.00089  3.73884E-05 0.01088 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.52625E-01 0.00033  6.52674E-01 0.00034  6.57296E-01 0.00994 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07873E+01 0.01449 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.59114E+01 0.00049  3.42545E+01 0.00050 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.73556E+04 0.00326  3.00916E+05 0.00124  6.07822E+05 0.00085  6.52054E+05 0.00077  5.98622E+05 0.00077  6.40830E+05 0.00066  4.35035E+05 0.00067  3.84775E+05 0.00075  2.94627E+05 0.00090  2.40593E+05 0.00071  2.07474E+05 0.00079  1.87080E+05 0.00077  1.72837E+05 0.00099  1.64163E+05 0.00091  1.60206E+05 0.00088  1.38061E+05 0.00073  1.36675E+05 0.00082  1.35194E+05 0.00116  1.33011E+05 0.00084  2.59652E+05 0.00065  2.50795E+05 0.00083  1.81265E+05 0.00085  1.17510E+05 0.00078  1.35814E+05 0.00087  1.28989E+05 0.00074  1.15784E+05 0.00076  1.89409E+05 0.00062  4.34963E+04 0.00138  5.46550E+04 0.00126  4.94984E+04 0.00171  2.88692E+04 0.00222  4.99853E+04 0.00154  3.36369E+04 0.00193  2.80374E+04 0.00194  5.04029E+03 0.00393  4.56893E+03 0.00316  4.12100E+03 0.00373  3.93605E+03 0.00340  4.03137E+03 0.00351  4.40236E+03 0.00337  5.01687E+03 0.00334  4.93648E+03 0.00406  9.55346E+03 0.00325  1.56263E+04 0.00227  2.00441E+04 0.00190  5.33759E+04 0.00142  5.62726E+04 0.00136  6.03233E+04 0.00153  3.98815E+04 0.00123  2.85081E+04 0.00196  2.13785E+04 0.00168  2.52456E+04 0.00186  4.95588E+04 0.00160  6.98407E+04 0.00167  1.38431E+05 0.00117  2.18352E+05 0.00118  3.30728E+05 0.00140  2.12983E+05 0.00168  1.53072E+05 0.00133  1.10360E+05 0.00166  9.94076E+04 0.00149  9.80182E+04 0.00167  8.18185E+04 0.00181  5.55194E+04 0.00164  5.13168E+04 0.00141  4.57253E+04 0.00194  3.87290E+04 0.00191  3.05548E+04 0.00195  2.04659E+04 0.00227  7.27255E+03 0.00220 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.76987E-01 0.00043 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.38580E+18 0.00039  3.79181E+17 0.00136 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38549E-01 0.00012  1.55099E+00 0.00048 ];
INF_CAPT                  (idx, [1:   4]) = [  7.34002E-03 0.00077  3.40711E-02 0.00073 ];
INF_ABS                   (idx, [1:   4]) = [  9.19176E-03 0.00065  6.35074E-02 0.00104 ];
INF_FISS                  (idx, [1:   4]) = [  1.85174E-03 0.00047  2.94363E-02 0.00140 ];
INF_NSF                   (idx, [1:   4]) = [  4.90518E-03 0.00045  7.67967E-02 0.00145 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.64896E+00 8.5E-05  2.60891E+00 6.0E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04967E+02 1.1E-05  2.04586E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.73005E-08 0.00042  2.58420E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29355E-01 0.00012  1.48749E+00 0.00054 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44506E-01 0.00019  3.93705E-01 0.00062 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64332E-02 0.00024  9.29659E-02 0.00091 ];
INF_SCATT3                (idx, [1:   4]) = [  7.31380E-03 0.00300  2.78273E-02 0.00196 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03772E-02 0.00187 -8.82088E-03 0.00779 ];
INF_SCATT5                (idx, [1:   4]) = [  1.29631E-04 0.14392  6.63044E-03 0.00679 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08374E-03 0.00351 -1.70054E-02 0.00247 ];
INF_SCATT7                (idx, [1:   4]) = [  7.56191E-04 0.02082  4.28793E-04 0.10067 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29397E-01 0.00012  1.48749E+00 0.00054 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44507E-01 0.00019  3.93705E-01 0.00062 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64332E-02 0.00024  9.29659E-02 0.00091 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.31381E-03 0.00300  2.78273E-02 0.00196 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03772E-02 0.00187 -8.82088E-03 0.00779 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.29487E-04 0.14455  6.63044E-03 0.00679 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08348E-03 0.00351 -1.70054E-02 0.00247 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.56171E-04 0.02085  4.28793E-04 0.10067 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12554E-01 0.00023  1.00560E+00 0.00041 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56823E+00 0.00023  3.31479E-01 0.00041 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.14973E-03 0.00066  6.35074E-02 0.00104 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69632E-02 0.00025  6.46223E-02 0.00114 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11586E-01 0.00011  1.77689E-02 0.00046  1.11734E-03 0.00404  1.48637E+00 0.00054 ];
INF_S1                    (idx, [1:   8]) = [  2.39345E-01 0.00018  5.16065E-03 0.00128  4.79200E-04 0.00651  3.93225E-01 0.00062 ];
INF_S2                    (idx, [1:   8]) = [  9.80142E-02 0.00024 -1.58106E-03 0.00234  2.57566E-04 0.01076  9.27083E-02 0.00091 ];
INF_S3                    (idx, [1:   8]) = [  9.14667E-03 0.00237 -1.83288E-03 0.00115  9.05525E-05 0.02208  2.77367E-02 0.00197 ];
INF_S4                    (idx, [1:   8]) = [ -9.78846E-03 0.00193 -5.88754E-04 0.00406 -3.78095E-06 0.36298 -8.81710E-03 0.00784 ];
INF_S5                    (idx, [1:   8]) = [  9.53020E-05 0.20167  3.43285E-05 0.07655 -3.82507E-05 0.03580  6.66869E-03 0.00679 ];
INF_S6                    (idx, [1:   8]) = [  5.21892E-03 0.00354 -1.35185E-04 0.01811 -4.93112E-05 0.03012 -1.69561E-02 0.00249 ];
INF_S7                    (idx, [1:   8]) = [  9.27747E-04 0.01642 -1.71556E-04 0.01298 -4.25131E-05 0.03635  4.71307E-04 0.09167 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11628E-01 0.00011  1.77689E-02 0.00046  1.11734E-03 0.00404  1.48637E+00 0.00054 ];
INF_SP1                   (idx, [1:   8]) = [  2.39346E-01 0.00018  5.16065E-03 0.00128  4.79200E-04 0.00651  3.93225E-01 0.00062 ];
INF_SP2                   (idx, [1:   8]) = [  9.80143E-02 0.00025 -1.58106E-03 0.00234  2.57566E-04 0.01076  9.27083E-02 0.00091 ];
INF_SP3                   (idx, [1:   8]) = [  9.14669E-03 0.00236 -1.83288E-03 0.00115  9.05525E-05 0.02208  2.77367E-02 0.00197 ];
INF_SP4                   (idx, [1:   8]) = [ -9.78845E-03 0.00193 -5.88754E-04 0.00406 -3.78095E-06 0.36298 -8.81710E-03 0.00784 ];
INF_SP5                   (idx, [1:   8]) = [  9.51583E-05 0.20260  3.43285E-05 0.07655 -3.82507E-05 0.03580  6.66869E-03 0.00679 ];
INF_SP6                   (idx, [1:   8]) = [  5.21866E-03 0.00354 -1.35185E-04 0.01811 -4.93112E-05 0.03012 -1.69561E-02 0.00249 ];
INF_SP7                   (idx, [1:   8]) = [  9.27727E-04 0.01644 -1.71556E-04 0.01298 -4.25131E-05 0.03635  4.71307E-04 0.09167 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31913E-01 0.00071  1.17324E+00 0.00576 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33451E-01 0.00103  1.27003E+00 0.00928 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33510E-01 0.00107  1.26620E+00 0.00701 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28850E-01 0.00092  1.02185E+00 0.00564 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43734E+00 0.00071  2.84343E-01 0.00580 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42789E+00 0.00103  2.63007E-01 0.00932 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42753E+00 0.00106  2.63567E-01 0.00706 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45659E+00 0.00092  3.26454E-01 0.00563 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.59301E-03 0.00901  1.51422E-04 0.05424  9.65283E-04 0.02214  8.70852E-04 0.02313  2.47737E-03 0.01361  8.58347E-04 0.02134  2.69730E-04 0.04047 ];
LAMBDA                    (idx, [1:  14]) = [  7.87783E-01 0.02086  1.25177E-02 0.00066  3.11039E-02 0.00061  1.10373E-01 0.00064  3.22305E-01 0.00044  1.32078E+00 0.00174  8.73810E+00 0.00731 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0002' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:56:06 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:19:48 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595300166 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.72877E-01  1.00527E+00  1.02526E+00  1.01932E+00  9.77278E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14092E-02 0.00108  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88591E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05854E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06278E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67605E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.57532E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.57438E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.58737E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.22967E-01 0.00115  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000885 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00018E+04 0.00085 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00018E+04 0.00085 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.16241E+02 ;
RUNNING_TIME              (idx, 1)        =  8.37093E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.55900E-01  4.55900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.97917E-01  1.36833E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  8.21479E+01  3.36370E+00  2.55683E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  8.00317E-01  3.47500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.90500E-02  6.66666E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  8.37092E+01  1.25851E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97246 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00010E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78359E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.48668E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.74552E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.22618E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.87894E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.31213E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.60771E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.61427E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.62018E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.57235E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  9.42293E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.99200E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.77888E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.77315E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.32605E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.35121E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.45352E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.50578E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.23625E+13 ;
CS137_ACTIVITY            (idx, 1)        =  1.68523E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.28606E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.34533E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.55766E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.27858E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.82700E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 13 ;
BURNUP                     (idx, [1:  2])  = [  1.25000E+01  1.25011E+01 ];
BURN_DAYS                 (idx, 1)        =  3.12500E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.39879E-01 0.00111 ];
U235_FISS                 (idx, [1:   4]) = [  7.22433E+15 0.00094  5.27448E-01 0.00069 ];
U238_FISS                 (idx, [1:   4]) = [  1.05702E+15 0.00283  7.71665E-02 0.00269 ];
PU239_FISS                (idx, [1:   4]) = [  4.80250E+15 0.00117  3.50628E-01 0.00097 ];
PU240_FISS                (idx, [1:   4]) = [  6.10126E+12 0.03541  4.45323E-04 0.03538 ];
PU241_FISS                (idx, [1:   4]) = [  5.96172E+14 0.00364  4.35286E-02 0.00361 ];
U235_CAPT                 (idx, [1:   4]) = [  1.59482E+15 0.00210  6.57018E-02 0.00209 ];
U238_CAPT                 (idx, [1:   4]) = [  9.36412E+15 0.00104  3.85732E-01 0.00076 ];
PU239_CAPT                (idx, [1:   4]) = [  2.65141E+15 0.00175  1.09225E-01 0.00168 ];
PU240_CAPT                (idx, [1:   4]) = [  1.51073E+15 0.00234  6.22288E-02 0.00220 ];
PU241_CAPT                (idx, [1:   4]) = [  2.16088E+14 0.00606  8.90149E-03 0.00603 ];
XE135_CAPT                (idx, [1:   4]) = [  7.65881E+14 0.00320  3.15522E-02 0.00319 ];
SM149_CAPT                (idx, [1:   4]) = [  2.07100E+14 0.00589  8.53345E-03 0.00595 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000885 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.75033E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000885 5.00775E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3167148 3.17160E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1787173 1.78958E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46564 4.65704E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000885 5.00775E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.77420E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.61790E+16 2.3E-05  3.61790E+16 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37029E+16 4.6E-06  1.37029E+16 4.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.42516E+16 0.00046  1.80513E+16 0.00046  6.20032E+15 0.00117 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.79545E+16 0.00030  3.17541E+16 0.00026  6.20032E+15 0.00117 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.82700E+16 0.00050  3.82700E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.81391E+18 0.00049  4.79762E+17 0.00047  1.33415E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.56494E+14 0.00472 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.83109E+16 0.00030 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.36984E+18 0.00065 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11061E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11061E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.54123E+00 0.00054 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.73178E-01 0.00035 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.49523E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23234E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93941E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96725E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.53762E-01 0.00059 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.44880E-01 0.00060 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.64025E+00 2.8E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04997E+02 4.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.44719E-01 0.00061  9.39783E-01 0.00060  5.09682E-03 0.01022 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.45890E-01 0.00030 ];
COL_KEFF                  (idx, [1:   2]) = [  9.45480E-01 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.45890E-01 0.00030 ];
ABS_KINF                  (idx, [1:   2]) = [  9.54786E-01 0.00030 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71860E+01 0.00023 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71824E+01 9.5E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.90154E-07 0.00398 ];
IMP_EALF                  (idx, [1:   2]) = [  6.90409E-07 0.00163 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.61910E-01 0.00301 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.62276E-01 0.00123 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.82129E-03 0.00635  1.54086E-04 0.03615  1.00105E-03 0.01414  9.09914E-04 0.01501  2.59226E-03 0.00987  8.93729E-04 0.01491  2.70250E-04 0.02742 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.78346E-01 0.01417  9.74483E-03 0.02392  3.10055E-02 0.00043  1.10498E-01 0.00046  3.22283E-01 0.00032  1.31236E+00 0.00146  8.01842E+00 0.01515 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.43393E-03 0.00947  1.49511E-04 0.05689  9.32018E-04 0.02175  8.59289E-04 0.02186  2.42680E-03 0.01467  8.27181E-04 0.02455  2.39128E-04 0.04385 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.58973E-01 0.02136  1.25240E-02 0.00069  3.10025E-02 0.00063  1.10531E-01 0.00069  3.22275E-01 0.00046  1.31506E+00 0.00200  8.87800E+00 0.00604 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.81127E-05 0.00139  2.81023E-05 0.00139  2.96481E-05 0.01645 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.65532E-05 0.00122  2.65434E-05 0.00122  2.80157E-05 0.01648 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.38167E-03 0.01044  1.37451E-04 0.06203  9.09652E-04 0.02246  8.45469E-04 0.02440  2.43224E-03 0.01599  8.10779E-04 0.02752  2.46080E-04 0.04954 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.70929E-01 0.02495  1.25245E-02 0.00098  3.10091E-02 0.00080  1.10447E-01 0.00081  3.22069E-01 0.00054  1.31481E+00 0.00256  8.93236E+00 0.00765 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.76318E-05 0.00326  2.76188E-05 0.00329  2.71855E-05 0.03926 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.60980E-05 0.00317  2.60856E-05 0.00319  2.57125E-05 0.03937 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.11834E-03 0.03524  1.70955E-04 0.17464  7.93849E-04 0.08993  7.71043E-04 0.08853  2.33250E-03 0.05324  7.43200E-04 0.08581  3.06798E-04 0.14767 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.72356E-01 0.07748  1.25319E-02 0.00211  3.09790E-02 0.00204  1.10588E-01 0.00203  3.22875E-01 0.00152  1.31251E+00 0.00617  8.97808E+00 0.02110 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.18375E-03 0.03461  1.66750E-04 0.17614  8.24025E-04 0.08894  7.64028E-04 0.08536  2.33997E-03 0.05140  7.73089E-04 0.08465  3.15882E-04 0.14178 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.90356E-01 0.07823  1.25310E-02 0.00210  3.09831E-02 0.00203  1.10545E-01 0.00200  3.22866E-01 0.00151  1.31223E+00 0.00622  8.97575E+00 0.02111 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.87186E+02 0.03582 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.79172E-05 0.00087 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.63691E-05 0.00062 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.36071E-03 0.00572 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.92169E+02 0.00592 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.95435E-07 0.00075 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.72622E-06 0.00045  2.72606E-06 0.00045  2.76013E-06 0.00609 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.03612E-05 0.00094  4.03781E-05 0.00095  3.73170E-05 0.01089 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.47163E-01 0.00032  6.47283E-01 0.00032  6.38642E-01 0.00951 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05080E+01 0.01442 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.57438E+01 0.00051  3.40604E+01 0.00050 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.80043E+04 0.00389  3.01641E+05 0.00126  6.09525E+05 0.00087  6.50153E+05 0.00078  5.98082E+05 0.00062  6.40451E+05 0.00059  4.35147E+05 0.00053  3.84905E+05 0.00077  2.94494E+05 0.00079  2.40123E+05 0.00069  2.07468E+05 0.00078  1.86636E+05 0.00081  1.72417E+05 0.00071  1.63994E+05 0.00085  1.59770E+05 0.00088  1.38084E+05 0.00101  1.36420E+05 0.00092  1.35103E+05 0.00100  1.33196E+05 0.00099  2.59383E+05 0.00055  2.51041E+05 0.00083  1.81508E+05 0.00083  1.17525E+05 0.00101  1.35873E+05 0.00093  1.29007E+05 0.00106  1.15580E+05 0.00085  1.88612E+05 0.00072  4.34635E+04 0.00165  5.44028E+04 0.00141  4.93678E+04 0.00146  2.87837E+04 0.00204  4.98022E+04 0.00132  3.33507E+04 0.00162  2.76001E+04 0.00242  4.91177E+03 0.00325  4.35202E+03 0.00405  3.85964E+03 0.00370  3.71100E+03 0.00367  3.80468E+03 0.00382  4.14977E+03 0.00394  4.84702E+03 0.00322  4.81829E+03 0.00383  9.37901E+03 0.00320  1.53481E+04 0.00262  1.97958E+04 0.00174  5.27042E+04 0.00168  5.55492E+04 0.00134  5.97168E+04 0.00105  3.91931E+04 0.00163  2.79594E+04 0.00131  2.09047E+04 0.00131  2.47421E+04 0.00151  4.83518E+04 0.00111  6.83217E+04 0.00143  1.36171E+05 0.00134  2.14857E+05 0.00137  3.26285E+05 0.00127  2.10436E+05 0.00130  1.51362E+05 0.00144  1.09106E+05 0.00178  9.84020E+04 0.00165  9.70490E+04 0.00153  8.12120E+04 0.00152  5.50696E+04 0.00189  5.08676E+04 0.00190  4.53151E+04 0.00160  3.83558E+04 0.00143  3.03586E+04 0.00176  2.03214E+04 0.00212  7.22401E+03 0.00301 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.54369E-01 0.00051 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.42797E+18 0.00056  3.85976E+17 0.00111 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38811E-01 9.4E-05  1.55570E+00 0.00037 ];
INF_CAPT                  (idx, [1:   4]) = [  7.55052E-03 0.00070  3.49009E-02 0.00053 ];
INF_ABS                   (idx, [1:   4]) = [  9.34551E-03 0.00057  6.37670E-02 0.00077 ];
INF_FISS                  (idx, [1:   4]) = [  1.79500E-03 0.00052  2.88661E-02 0.00108 ];
INF_NSF                   (idx, [1:   4]) = [  4.78430E-03 0.00054  7.60469E-02 0.00110 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.66535E+00 6.3E-05  2.63447E+00 6.2E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05193E+02 5.7E-06  2.04952E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.69088E-08 0.00039  2.58793E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29464E-01 9.7E-05  1.49190E+00 0.00043 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44626E-01 0.00014  3.94945E-01 0.00050 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65167E-02 0.00025  9.32669E-02 0.00096 ];
INF_SCATT3                (idx, [1:   4]) = [  7.33209E-03 0.00225  2.79753E-02 0.00261 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03350E-02 0.00159 -8.77511E-03 0.00551 ];
INF_SCATT5                (idx, [1:   4]) = [  1.54105E-04 0.11778  6.71700E-03 0.00893 ];
INF_SCATT6                (idx, [1:   4]) = [  5.11744E-03 0.00297 -1.70565E-02 0.00223 ];
INF_SCATT7                (idx, [1:   4]) = [  7.56599E-04 0.02015  4.67499E-04 0.07515 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29506E-01 9.7E-05  1.49190E+00 0.00043 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44626E-01 0.00014  3.94945E-01 0.00050 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65171E-02 0.00025  9.32669E-02 0.00096 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.33205E-03 0.00225  2.79753E-02 0.00261 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03350E-02 0.00160 -8.77511E-03 0.00551 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.54045E-04 0.11813  6.71700E-03 0.00893 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.11717E-03 0.00296 -1.70565E-02 0.00223 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.56553E-04 0.02008  4.67499E-04 0.07515 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12335E-01 0.00031  1.00924E+00 0.00035 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56985E+00 0.00031  3.30284E-01 0.00035 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.30397E-03 0.00059  6.37670E-02 0.00077 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69831E-02 0.00019  6.49257E-02 0.00111 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11828E-01 9.4E-05  1.76361E-02 0.00038  1.12297E-03 0.00532  1.49077E+00 0.00043 ];
INF_S1                    (idx, [1:   8]) = [  2.39505E-01 0.00014  5.12009E-03 0.00103  4.85109E-04 0.00695  3.94460E-01 0.00050 ];
INF_S2                    (idx, [1:   8]) = [  9.80852E-02 0.00024 -1.56846E-03 0.00234  2.62406E-04 0.00998  9.30045E-02 0.00097 ];
INF_S3                    (idx, [1:   8]) = [  9.15182E-03 0.00182 -1.81973E-03 0.00168  9.17028E-05 0.02301  2.78836E-02 0.00259 ];
INF_S4                    (idx, [1:   8]) = [ -9.75070E-03 0.00159 -5.84286E-04 0.00472  1.33683E-06 1.00000 -8.77644E-03 0.00547 ];
INF_S5                    (idx, [1:   8]) = [  1.26061E-04 0.14042  2.80437E-05 0.08484 -3.65103E-05 0.04124  6.75352E-03 0.00885 ];
INF_S6                    (idx, [1:   8]) = [  5.25707E-03 0.00282 -1.39630E-04 0.01594 -4.80916E-05 0.03449 -1.70084E-02 0.00223 ];
INF_S7                    (idx, [1:   8]) = [  9.22574E-04 0.01634 -1.65974E-04 0.01283 -4.18331E-05 0.03315  5.09332E-04 0.06898 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11870E-01 9.4E-05  1.76361E-02 0.00038  1.12297E-03 0.00532  1.49077E+00 0.00043 ];
INF_SP1                   (idx, [1:   8]) = [  2.39506E-01 0.00014  5.12009E-03 0.00103  4.85109E-04 0.00695  3.94460E-01 0.00050 ];
INF_SP2                   (idx, [1:   8]) = [  9.80855E-02 0.00024 -1.56846E-03 0.00234  2.62406E-04 0.00998  9.30045E-02 0.00097 ];
INF_SP3                   (idx, [1:   8]) = [  9.15178E-03 0.00182 -1.81973E-03 0.00168  9.17028E-05 0.02301  2.78836E-02 0.00259 ];
INF_SP4                   (idx, [1:   8]) = [ -9.75068E-03 0.00160 -5.84286E-04 0.00472  1.33683E-06 1.00000 -8.77644E-03 0.00547 ];
INF_SP5                   (idx, [1:   8]) = [  1.26001E-04 0.14083  2.80437E-05 0.08484 -3.65103E-05 0.04124  6.75352E-03 0.00885 ];
INF_SP6                   (idx, [1:   8]) = [  5.25680E-03 0.00282 -1.39630E-04 0.01594 -4.80916E-05 0.03449 -1.70084E-02 0.00223 ];
INF_SP7                   (idx, [1:   8]) = [  9.22528E-04 0.01628 -1.65974E-04 0.01283 -4.18331E-05 0.03315  5.09332E-04 0.06898 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31801E-01 0.00061  1.19367E+00 0.00763 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33297E-01 0.00071  1.29996E+00 0.01011 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33749E-01 0.00077  1.29400E+00 0.00982 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28440E-01 0.00123  1.03097E+00 0.00669 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43803E+00 0.00061  2.79644E-01 0.00771 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42881E+00 0.00071  2.57047E-01 0.01010 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42605E+00 0.00077  2.58219E-01 0.01022 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45923E+00 0.00123  3.23667E-01 0.00669 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.43393E-03 0.00947  1.49511E-04 0.05689  9.32018E-04 0.02175  8.59289E-04 0.02186  2.42680E-03 0.01467  8.27181E-04 0.02455  2.39128E-04 0.04385 ];
LAMBDA                    (idx, [1:  14]) = [  7.58973E-01 0.02136  1.25240E-02 0.00069  3.10025E-02 0.00063  1.10531E-01 0.00069  3.22275E-01 0.00046  1.31506E+00 0.00200  8.87800E+00 0.00604 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0002' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:56:06 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:25:56 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595300166 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.96089E-01  1.00665E+00  1.00452E+00  1.00063E+00  9.92114E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.5E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14336E-02 0.00115  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88566E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06473E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06897E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67631E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.56846E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.56752E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.53699E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.22846E-01 0.00115  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001411 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00028E+04 0.00095 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00028E+04 0.00095 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.46840E+02 ;
RUNNING_TIME              (idx, 1)        =  8.98370E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.55900E-01  4.55900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.23333E-01  1.30667E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  8.81890E+01  3.38293E+00  2.65815E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  8.60900E-01  3.09167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.23334E-02  9.66668E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  8.98369E+01  1.26496E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97389 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99830E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78688E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.53986E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.73391E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.34737E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.92968E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.34827E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61015E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.59905E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.97620E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.65774E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.25732E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.27016E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.18880E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.83072E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.54667E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.37562E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.48922E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.54046E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.75302E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.02047E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.33442E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.31630E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.71571E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.29362E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.94069E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 14 ;
BURNUP                     (idx, [1:  2])  = [  1.50000E+01  1.50014E+01 ];
BURN_DAYS                 (idx, 1)        =  3.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.63553E-01 0.00103 ];
U235_FISS                 (idx, [1:   4]) = [  6.56657E+15 0.00100  4.79490E-01 0.00078 ];
U238_FISS                 (idx, [1:   4]) = [  1.09686E+15 0.00262  8.00807E-02 0.00243 ];
PU239_FISS                (idx, [1:   4]) = [  5.22530E+15 0.00118  3.81538E-01 0.00094 ];
PU240_FISS                (idx, [1:   4]) = [  7.86343E+12 0.03153  5.73942E-04 0.03146 ];
PU241_FISS                (idx, [1:   4]) = [  7.84889E+14 0.00313  5.73117E-02 0.00305 ];
U235_CAPT                 (idx, [1:   4]) = [  1.45699E+15 0.00237  5.73533E-02 0.00228 ];
U238_CAPT                 (idx, [1:   4]) = [  9.57764E+15 0.00103  3.77006E-01 0.00072 ];
PU239_CAPT                (idx, [1:   4]) = [  2.87262E+15 0.00152  1.13090E-01 0.00151 ];
PU240_CAPT                (idx, [1:   4]) = [  1.76845E+15 0.00223  6.96121E-02 0.00211 ];
PU241_CAPT                (idx, [1:   4]) = [  2.82544E+14 0.00509  1.11225E-02 0.00506 ];
XE135_CAPT                (idx, [1:   4]) = [  7.68121E+14 0.00311  3.02416E-02 0.00315 ];
SM149_CAPT                (idx, [1:   4]) = [  2.18238E+14 0.00624  8.59069E-03 0.00620 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001411 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.00473E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001411 5.00800E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3218831 3.22322E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1735545 1.73774E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 47035 4.70375E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001411 5.00800E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.65661E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.64141E+16 2.3E-05  3.64141E+16 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36825E+16 4.5E-06  1.36825E+16 4.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.53957E+16 0.00044  1.90282E+16 0.00047  6.36755E+15 0.00112 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.90783E+16 0.00029  3.27107E+16 0.00027  6.36755E+15 0.00112 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.94069E+16 0.00051  3.94069E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.86462E+18 0.00048  4.92473E+17 0.00048  1.37214E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.70767E+14 0.00463 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.94490E+16 0.00030 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.40794E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10772E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10772E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.52089E+00 0.00055 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.72845E-01 0.00035 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.44195E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23333E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93888E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96684E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.33808E-01 0.00063 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.25024E-01 0.00064 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.66135E+00 2.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05301E+02 4.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.25139E-01 0.00065  9.20197E-01 0.00063  4.82701E-03 0.01037 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.24578E-01 0.00030 ];
COL_KEFF                  (idx, [1:   2]) = [  9.24171E-01 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.24578E-01 0.00030 ];
ABS_KINF                  (idx, [1:   2]) = [  9.33362E-01 0.00029 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71267E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71342E+01 9.5E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.32108E-07 0.00383 ];
IMP_EALF                  (idx, [1:   2]) = [  7.24493E-07 0.00163 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.71958E-01 0.00279 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.70806E-01 0.00118 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.83042E-03 0.00626  1.40791E-04 0.03980  9.98066E-04 0.01482  9.26772E-04 0.01589  2.57604E-03 0.00932  9.08131E-04 0.01536  2.80626E-04 0.02863 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.84102E-01 0.01524  8.99285E-03 0.02806  3.09459E-02 0.00046  1.10627E-01 0.00050  3.22493E-01 0.00032  1.30213E+00 0.00165  7.94599E+00 0.01530 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.26214E-03 0.00907  1.29044E-04 0.05735  9.03420E-04 0.02346  8.18811E-04 0.02264  2.35324E-03 0.01410  8.10419E-04 0.02352  2.47206E-04 0.04278 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.73437E-01 0.02266  1.25202E-02 0.00061  3.09411E-02 0.00067  1.10731E-01 0.00074  3.22476E-01 0.00052  1.30251E+00 0.00224  8.59086E+00 0.00915 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.84599E-05 0.00148  2.84512E-05 0.00148  3.01005E-05 0.01629 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.63237E-05 0.00132  2.63157E-05 0.00133  2.78472E-05 0.01634 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.20415E-03 0.01049  1.28449E-04 0.06660  9.07054E-04 0.02540  8.14228E-04 0.02793  2.29347E-03 0.01615  8.00645E-04 0.02671  2.60312E-04 0.04974 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.05858E-01 0.02741  1.25246E-02 0.00111  3.09630E-02 0.00084  1.10624E-01 0.00094  3.22410E-01 0.00063  1.29634E+00 0.00352  8.66402E+00 0.01123 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.81337E-05 0.00337  2.81244E-05 0.00338  2.82968E-05 0.04866 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.60214E-05 0.00329  2.60129E-05 0.00330  2.61578E-05 0.04833 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.14164E-03 0.03443  1.09801E-04 0.23328  8.37429E-04 0.07746  8.62611E-04 0.08139  2.29336E-03 0.05657  7.55589E-04 0.09161  2.82849E-04 0.15158 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.21880E-01 0.08223  1.25254E-02 0.00296  3.08434E-02 0.00193  1.10513E-01 0.00202  3.23020E-01 0.00168  1.31045E+00 0.00620  8.36811E+00 0.03230 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.15383E-03 0.03369  1.08629E-04 0.22383  8.50221E-04 0.07564  8.74943E-04 0.08168  2.27892E-03 0.05512  7.56355E-04 0.08739  2.84767E-04 0.15125 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.14816E-01 0.08053  1.25254E-02 0.00296  3.08423E-02 0.00193  1.10499E-01 0.00201  3.23084E-01 0.00168  1.30983E+00 0.00623  8.36180E+00 0.03239 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.83891E+02 0.03474 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.83004E-05 0.00088 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.61765E-05 0.00062 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.24544E-03 0.00617 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.85477E+02 0.00633 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.93063E-07 0.00073 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.70686E-06 0.00044  2.70684E-06 0.00044  2.70686E-06 0.00582 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.04610E-05 0.00085  4.04787E-05 0.00085  3.72339E-05 0.01094 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.41869E-01 0.00035  6.42089E-01 0.00035  6.17402E-01 0.01027 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03494E+01 0.01413 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.56752E+01 0.00048  3.39059E+01 0.00056 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.87099E+04 0.00220  3.03478E+05 0.00182  6.10033E+05 0.00109  6.51116E+05 0.00070  5.98421E+05 0.00063  6.40159E+05 0.00071  4.34520E+05 0.00071  3.84350E+05 0.00061  2.93872E+05 0.00079  2.40621E+05 0.00075  2.07628E+05 0.00098  1.87066E+05 0.00076  1.72661E+05 0.00077  1.64035E+05 0.00083  1.59777E+05 0.00067  1.38222E+05 0.00110  1.36443E+05 0.00097  1.35107E+05 0.00099  1.32844E+05 0.00097  2.59391E+05 0.00072  2.51083E+05 0.00067  1.81132E+05 0.00110  1.17162E+05 0.00098  1.35755E+05 0.00072  1.29172E+05 0.00090  1.15264E+05 0.00092  1.87847E+05 0.00058  4.31751E+04 0.00107  5.42417E+04 0.00164  4.91681E+04 0.00166  2.86422E+04 0.00188  4.97023E+04 0.00147  3.32423E+04 0.00222  2.73098E+04 0.00233  4.77140E+03 0.00338  4.18048E+03 0.00260  3.68466E+03 0.00408  3.51618E+03 0.00284  3.61697E+03 0.00306  3.97191E+03 0.00393  4.65084E+03 0.00319  4.68795E+03 0.00276  9.20832E+03 0.00306  1.51318E+04 0.00268  1.95108E+04 0.00285  5.23840E+04 0.00179  5.49995E+04 0.00128  5.90203E+04 0.00123  3.87132E+04 0.00167  2.75201E+04 0.00146  2.05513E+04 0.00183  2.42105E+04 0.00147  4.75197E+04 0.00149  6.72965E+04 0.00147  1.34601E+05 0.00152  2.13185E+05 0.00111  3.24151E+05 0.00142  2.09355E+05 0.00126  1.50753E+05 0.00136  1.08686E+05 0.00139  9.79443E+04 0.00147  9.67187E+04 0.00160  8.08736E+04 0.00143  5.48797E+04 0.00170  5.07555E+04 0.00138  4.52486E+04 0.00147  3.82617E+04 0.00155  3.02342E+04 0.00185  2.02696E+04 0.00172  7.19024E+03 0.00234 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.32948E-01 0.00044 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.47001E+18 0.00047  3.94647E+17 0.00112 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38854E-01 0.00010  1.56051E+00 0.00038 ];
INF_CAPT                  (idx, [1:   4]) = [  7.73489E-03 0.00084  3.55427E-02 0.00064 ];
INF_ABS                   (idx, [1:   4]) = [  9.47924E-03 0.00074  6.37207E-02 0.00084 ];
INF_FISS                  (idx, [1:   4]) = [  1.74435E-03 0.00060  2.81780E-02 0.00111 ];
INF_NSF                   (idx, [1:   4]) = [  4.67612E-03 0.00061  7.48657E-02 0.00115 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.68072E+00 5.6E-05  2.65688E+00 5.3E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05407E+02 8.2E-06  2.05277E+02 9.6E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.65519E-08 0.00046  2.59163E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29363E-01 0.00011  1.49679E+00 0.00043 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44478E-01 0.00016  3.96105E-01 0.00048 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64697E-02 0.00022  9.33728E-02 0.00068 ];
INF_SCATT3                (idx, [1:   4]) = [  7.31106E-03 0.00278  2.80448E-02 0.00249 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03509E-02 0.00176 -8.88587E-03 0.00642 ];
INF_SCATT5                (idx, [1:   4]) = [  1.62391E-04 0.07536  6.69555E-03 0.00775 ];
INF_SCATT6                (idx, [1:   4]) = [  5.11290E-03 0.00297 -1.71296E-02 0.00271 ];
INF_SCATT7                (idx, [1:   4]) = [  7.49162E-04 0.02255  4.72304E-04 0.09455 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29406E-01 0.00011  1.49679E+00 0.00043 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44478E-01 0.00016  3.96105E-01 0.00048 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64699E-02 0.00022  9.33728E-02 0.00068 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.31087E-03 0.00278  2.80448E-02 0.00249 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03508E-02 0.00176 -8.88587E-03 0.00642 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.62389E-04 0.07556  6.69555E-03 0.00775 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.11293E-03 0.00297 -1.71296E-02 0.00271 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.48943E-04 0.02256  4.72304E-04 0.09455 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12334E-01 0.00028  1.01312E+00 0.00033 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56986E+00 0.00028  3.29017E-01 0.00033 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.43632E-03 0.00073  6.37207E-02 0.00084 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69878E-02 0.00025  6.48441E-02 0.00094 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11866E-01 9.9E-05  1.74964E-02 0.00050  1.12278E-03 0.00605  1.49566E+00 0.00043 ];
INF_S1                    (idx, [1:   8]) = [  2.39405E-01 0.00015  5.07238E-03 0.00114  4.86313E-04 0.00988  3.95618E-01 0.00048 ];
INF_S2                    (idx, [1:   8]) = [  9.80399E-02 0.00021 -1.57015E-03 0.00148  2.66039E-04 0.00941  9.31068E-02 0.00069 ];
INF_S3                    (idx, [1:   8]) = [  9.11844E-03 0.00213 -1.80738E-03 0.00140  9.72382E-05 0.02161  2.79475E-02 0.00251 ];
INF_S4                    (idx, [1:   8]) = [ -9.77997E-03 0.00183 -5.70902E-04 0.00518  4.57490E-08 1.00000 -8.88592E-03 0.00639 ];
INF_S5                    (idx, [1:   8]) = [  1.27847E-04 0.09938  3.45447E-05 0.08239 -3.82024E-05 0.04175  6.73375E-03 0.00771 ];
INF_S6                    (idx, [1:   8]) = [  5.25344E-03 0.00283 -1.40540E-04 0.01755 -4.89045E-05 0.03191 -1.70807E-02 0.00272 ];
INF_S7                    (idx, [1:   8]) = [  9.17367E-04 0.01872 -1.68206E-04 0.01380 -4.44986E-05 0.03228  5.16803E-04 0.08687 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11909E-01 9.9E-05  1.74964E-02 0.00050  1.12278E-03 0.00605  1.49566E+00 0.00043 ];
INF_SP1                   (idx, [1:   8]) = [  2.39406E-01 0.00015  5.07238E-03 0.00114  4.86313E-04 0.00988  3.95618E-01 0.00048 ];
INF_SP2                   (idx, [1:   8]) = [  9.80401E-02 0.00021 -1.57015E-03 0.00148  2.66039E-04 0.00941  9.31068E-02 0.00069 ];
INF_SP3                   (idx, [1:   8]) = [  9.11825E-03 0.00213 -1.80738E-03 0.00140  9.72382E-05 0.02161  2.79475E-02 0.00251 ];
INF_SP4                   (idx, [1:   8]) = [ -9.77990E-03 0.00183 -5.70902E-04 0.00518  4.57490E-08 1.00000 -8.88592E-03 0.00639 ];
INF_SP5                   (idx, [1:   8]) = [  1.27844E-04 0.09962  3.45447E-05 0.08239 -3.82024E-05 0.04175  6.73375E-03 0.00771 ];
INF_SP6                   (idx, [1:   8]) = [  5.25347E-03 0.00283 -1.40540E-04 0.01755 -4.89045E-05 0.03191 -1.70807E-02 0.00272 ];
INF_SP7                   (idx, [1:   8]) = [  9.17149E-04 0.01873 -1.68206E-04 0.01380 -4.44986E-05 0.03228  5.16803E-04 0.08687 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31945E-01 0.00045  1.19203E+00 0.00617 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33616E-01 0.00071  1.29822E+00 0.00958 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33521E-01 0.00087  1.28440E+00 0.00833 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28771E-01 0.00070  1.03466E+00 0.00561 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43713E+00 0.00045  2.79890E-01 0.00616 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42686E+00 0.00071  2.57318E-01 0.00940 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42745E+00 0.00087  2.59942E-01 0.00805 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45708E+00 0.00070  3.22409E-01 0.00560 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.26214E-03 0.00907  1.29044E-04 0.05735  9.03420E-04 0.02346  8.18811E-04 0.02264  2.35324E-03 0.01410  8.10419E-04 0.02352  2.47206E-04 0.04278 ];
LAMBDA                    (idx, [1:  14]) = [  7.73437E-01 0.02266  1.25202E-02 0.00061  3.09411E-02 0.00067  1.10731E-01 0.00074  3.22476E-01 0.00052  1.30251E+00 0.00224  8.59086E+00 0.00915 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0002' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:56:06 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:32:01 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595300166 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.96208E-01  1.00406E+00  9.99052E-01  9.97124E-01  1.00356E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14826E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88517E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06366E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06791E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67914E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.56078E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.55985E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.52437E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.23993E-01 0.00112  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000948 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00019E+04 0.00093 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00019E+04 0.00093 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.77230E+02 ;
RUNNING_TIME              (idx, 1)        =  9.59232E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.55900E-01  4.55900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.49067E-01  1.32000E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.41892E+01  3.42650E+00  2.57365E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  9.20567E-01  2.92500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.52500E-02  6.83335E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.59230E+01  1.26130E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97513 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00066E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78939E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.59049E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.72464E+04 ;
TOT_SF_RATE               (idx, 1)        =  4.08054E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.97760E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.38284E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61285E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.58633E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.37575E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.73741E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.62045E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.54388E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.55300E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.88302E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.75643E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.39644E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.51953E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.57020E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.34227E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.35472E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.37976E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.29122E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  4.29269E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.30759E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.04764E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 15 ;
BURNUP                     (idx, [1:  2])  = [  1.75000E+01  1.75016E+01 ];
BURN_DAYS                 (idx, 1)        =  4.37500E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.87628E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  5.96261E+15 0.00112  4.36553E-01 0.00094 ];
U238_FISS                 (idx, [1:   4]) = [  1.11720E+15 0.00272  8.17822E-02 0.00253 ];
PU239_FISS                (idx, [1:   4]) = [  5.57541E+15 0.00114  4.08201E-01 0.00095 ];
PU240_FISS                (idx, [1:   4]) = [  9.10920E+12 0.02873  6.66719E-04 0.02870 ];
PU241_FISS                (idx, [1:   4]) = [  9.78187E+14 0.00293  7.16146E-02 0.00284 ];
U235_CAPT                 (idx, [1:   4]) = [  1.33007E+15 0.00247  5.01824E-02 0.00237 ];
U238_CAPT                 (idx, [1:   4]) = [  9.79557E+15 0.00103  3.69572E-01 0.00074 ];
PU239_CAPT                (idx, [1:   4]) = [  3.06967E+15 0.00162  1.15823E-01 0.00156 ];
PU240_CAPT                (idx, [1:   4]) = [  2.01471E+15 0.00193  7.60122E-02 0.00180 ];
PU241_CAPT                (idx, [1:   4]) = [  3.50404E+14 0.00490  1.32219E-02 0.00490 ];
XE135_CAPT                (idx, [1:   4]) = [  7.73792E+14 0.00308  2.91966E-02 0.00306 ];
SM149_CAPT                (idx, [1:   4]) = [  2.25128E+14 0.00633  8.49394E-03 0.00630 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000948 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.08848E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000948 5.00809E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3269277 3.27403E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1684931 1.68732E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46740 4.67414E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000948 5.00809E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.47035E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.66313E+16 2.5E-05  3.66313E+16 2.5E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36635E+16 5.1E-06  1.36635E+16 5.1E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.65033E+16 0.00043  1.99956E+16 0.00045  6.50771E+15 0.00118 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.01668E+16 0.00028  3.36591E+16 0.00027  6.50771E+15 0.00118 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.04764E+16 0.00050  4.04764E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.91226E+18 0.00047  5.04923E+17 0.00047  1.40734E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.78425E+14 0.00503 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.05452E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.44299E+18 0.00063 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10482E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10482E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.49989E+00 0.00056 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.72306E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.40468E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23114E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93923E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96709E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.13323E-01 0.00064 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.04785E-01 0.00064 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.68095E+00 3.0E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05587E+02 5.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.04848E-01 0.00064  9.00089E-01 0.00064  4.69594E-03 0.01096 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.04955E-01 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  9.05114E-01 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.04955E-01 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  9.13493E-01 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70992E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70986E+01 9.5E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.52510E-07 0.00383 ];
IMP_EALF                  (idx, [1:   2]) = [  7.50723E-07 0.00163 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.77682E-01 0.00276 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.77474E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.87584E-03 0.00613  1.52911E-04 0.03941  1.04661E-03 0.01445  9.21125E-04 0.01476  2.56710E-03 0.00962  9.18570E-04 0.01618  2.69526E-04 0.02902 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.52873E-01 0.01500  9.34055E-03 0.02627  3.08533E-02 0.00047  1.10776E-01 0.00053  3.22622E-01 0.00033  1.29055E+00 0.00277  7.68741E+00 0.01667 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.21468E-03 0.00922  1.33324E-04 0.05923  9.31114E-04 0.02223  8.29703E-04 0.02306  2.30266E-03 0.01419  7.82154E-04 0.02402  2.35725E-04 0.04300 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.42742E-01 0.02202  1.25591E-02 0.00100  3.08596E-02 0.00068  1.10674E-01 0.00073  3.22667E-01 0.00053  1.29392E+00 0.00255  8.40231E+00 0.01068 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.91377E-05 0.00155  2.91268E-05 0.00156  3.16254E-05 0.01727 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.63585E-05 0.00135  2.63486E-05 0.00135  2.86086E-05 0.01726 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.18329E-03 0.01095  1.32919E-04 0.06969  9.29958E-04 0.02505  7.94804E-04 0.02695  2.29929E-03 0.01611  7.91618E-04 0.02774  2.34703E-04 0.04932 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.49203E-01 0.02713  1.25581E-02 0.00143  3.08538E-02 0.00087  1.10678E-01 0.00098  3.22784E-01 0.00061  1.29362E+00 0.00350  8.49057E+00 0.01427 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.87636E-05 0.00341  2.87625E-05 0.00344  2.69807E-05 0.04138 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.60204E-05 0.00333  2.60193E-05 0.00336  2.44231E-05 0.04143 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.98339E-03 0.03661  1.15160E-04 0.21606  9.76381E-04 0.08224  6.66886E-04 0.09579  2.21424E-03 0.05593  7.52150E-04 0.09864  2.58579E-04 0.16928 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.69306E-01 0.08379  1.26038E-02 0.00445  3.08341E-02 0.00204  1.10633E-01 0.00233  3.23225E-01 0.00171  1.29712E+00 0.00745  8.83960E+00 0.02540 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.02127E-03 0.03628  1.14936E-04 0.20861  9.56071E-04 0.08223  6.65681E-04 0.09486  2.23042E-03 0.05519  7.77847E-04 0.09559  2.76316E-04 0.16383 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.89186E-01 0.08352  1.26038E-02 0.00445  3.08351E-02 0.00203  1.10641E-01 0.00232  3.23287E-01 0.00171  1.29687E+00 0.00745  8.83960E+00 0.02540 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.74455E+02 0.03679 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.89914E-05 0.00089 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.62274E-05 0.00062 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.11874E-03 0.00673 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.76664E+02 0.00682 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.90468E-07 0.00078 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.68985E-06 0.00044  2.68962E-06 0.00045  2.73436E-06 0.00571 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.04472E-05 0.00096  4.04632E-05 0.00097  3.75796E-05 0.01012 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.38189E-01 0.00035  6.38477E-01 0.00035  6.01826E-01 0.01006 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07722E+01 0.01404 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.55985E+01 0.00051  3.38761E+01 0.00057 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.88035E+04 0.00343  3.03207E+05 0.00128  6.10050E+05 0.00107  6.50008E+05 0.00066  5.98008E+05 0.00060  6.40092E+05 0.00075  4.34983E+05 0.00062  3.84927E+05 0.00067  2.94150E+05 0.00082  2.40788E+05 0.00084  2.07446E+05 0.00088  1.87051E+05 0.00110  1.72637E+05 0.00091  1.64084E+05 0.00084  1.59971E+05 0.00102  1.38177E+05 0.00088  1.36512E+05 0.00095  1.35433E+05 0.00088  1.33373E+05 0.00097  2.59999E+05 0.00095  2.51043E+05 0.00064  1.81122E+05 0.00067  1.17497E+05 0.00091  1.35716E+05 0.00098  1.29379E+05 0.00093  1.15426E+05 0.00092  1.87473E+05 0.00074  4.33811E+04 0.00171  5.41055E+04 0.00151  4.90966E+04 0.00172  2.86842E+04 0.00172  4.96363E+04 0.00180  3.30831E+04 0.00134  2.70881E+04 0.00175  4.69631E+03 0.00377  4.07925E+03 0.00298  3.56069E+03 0.00454  3.41561E+03 0.00340  3.47715E+03 0.00229  3.84491E+03 0.00334  4.54606E+03 0.00277  4.61104E+03 0.00365  9.12516E+03 0.00251  1.49156E+04 0.00278  1.94268E+04 0.00163  5.17574E+04 0.00095  5.45126E+04 0.00112  5.85889E+04 0.00138  3.81466E+04 0.00128  2.72176E+04 0.00185  2.02144E+04 0.00219  2.38905E+04 0.00185  4.68593E+04 0.00187  6.66206E+04 0.00131  1.33463E+05 0.00134  2.11668E+05 0.00143  3.21853E+05 0.00147  2.07984E+05 0.00153  1.49781E+05 0.00165  1.08251E+05 0.00154  9.74481E+04 0.00166  9.63494E+04 0.00136  8.05372E+04 0.00178  5.45967E+04 0.00173  5.05030E+04 0.00188  4.50215E+04 0.00144  3.81258E+04 0.00224  3.00949E+04 0.00201  2.01913E+04 0.00191  7.15815E+03 0.00213 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.13654E-01 0.00052 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.50975E+18 0.00043  4.02555E+17 0.00149 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39209E-01 0.00012  1.56241E+00 0.00049 ];
INF_CAPT                  (idx, [1:   4]) = [  7.91012E-03 0.00067  3.61757E-02 0.00086 ];
INF_ABS                   (idx, [1:   4]) = [  9.60355E-03 0.00056  6.37720E-02 0.00113 ];
INF_FISS                  (idx, [1:   4]) = [  1.69344E-03 0.00037  2.75964E-02 0.00149 ];
INF_NSF                   (idx, [1:   4]) = [  4.56421E-03 0.00038  7.38939E-02 0.00155 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.69524E+00 8.1E-05  2.67766E+00 6.6E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05612E+02 1.0E-05  2.05581E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.63281E-08 0.00033  2.59388E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29617E-01 0.00012  1.49861E+00 0.00055 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44545E-01 0.00025  3.96424E-01 0.00065 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64739E-02 0.00038  9.33705E-02 0.00099 ];
INF_SCATT3                (idx, [1:   4]) = [  7.31961E-03 0.00303  2.81063E-02 0.00243 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03573E-02 0.00220 -8.97264E-03 0.00716 ];
INF_SCATT5                (idx, [1:   4]) = [  1.72442E-04 0.10867  6.63463E-03 0.00778 ];
INF_SCATT6                (idx, [1:   4]) = [  5.12298E-03 0.00309 -1.71748E-02 0.00295 ];
INF_SCATT7                (idx, [1:   4]) = [  7.60005E-04 0.01373  5.28175E-04 0.10519 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29660E-01 0.00013  1.49861E+00 0.00055 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44545E-01 0.00025  3.96424E-01 0.00065 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64739E-02 0.00038  9.33705E-02 0.00099 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.31972E-03 0.00303  2.81063E-02 0.00243 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03572E-02 0.00220 -8.97264E-03 0.00716 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.72452E-04 0.10920  6.63463E-03 0.00778 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.12315E-03 0.00309 -1.71748E-02 0.00295 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.60057E-04 0.01374  5.28175E-04 0.10519 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12390E-01 0.00028  1.01498E+00 0.00040 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56945E+00 0.00028  3.28415E-01 0.00040 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.56018E-03 0.00056  6.37720E-02 0.00113 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69895E-02 0.00028  6.49198E-02 0.00118 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12220E-01 0.00012  1.73971E-02 0.00048  1.12111E-03 0.00392  1.49749E+00 0.00055 ];
INF_S1                    (idx, [1:   8]) = [  2.39510E-01 0.00024  5.03479E-03 0.00111  4.87527E-04 0.00668  3.95937E-01 0.00065 ];
INF_S2                    (idx, [1:   8]) = [  9.80445E-02 0.00035 -1.57059E-03 0.00280  2.66347E-04 0.01107  9.31042E-02 0.00100 ];
INF_S3                    (idx, [1:   8]) = [  9.11573E-03 0.00233 -1.79612E-03 0.00173  9.88279E-05 0.02145  2.80075E-02 0.00244 ];
INF_S4                    (idx, [1:   8]) = [ -9.79015E-03 0.00233 -5.67114E-04 0.00414  8.80580E-07 1.00000 -8.97352E-03 0.00723 ];
INF_S5                    (idx, [1:   8]) = [  1.36822E-04 0.13866  3.56205E-05 0.07197 -3.75083E-05 0.04696  6.67214E-03 0.00770 ];
INF_S6                    (idx, [1:   8]) = [  5.25982E-03 0.00295 -1.36843E-04 0.01571 -4.89522E-05 0.02344 -1.71259E-02 0.00294 ];
INF_S7                    (idx, [1:   8]) = [  9.27124E-04 0.01074 -1.67119E-04 0.01154 -4.48230E-05 0.02960  5.72998E-04 0.09665 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12263E-01 0.00012  1.73971E-02 0.00048  1.12111E-03 0.00392  1.49749E+00 0.00055 ];
INF_SP1                   (idx, [1:   8]) = [  2.39510E-01 0.00024  5.03479E-03 0.00111  4.87527E-04 0.00668  3.95937E-01 0.00065 ];
INF_SP2                   (idx, [1:   8]) = [  9.80445E-02 0.00035 -1.57059E-03 0.00280  2.66347E-04 0.01107  9.31042E-02 0.00100 ];
INF_SP3                   (idx, [1:   8]) = [  9.11584E-03 0.00232 -1.79612E-03 0.00173  9.88279E-05 0.02145  2.80075E-02 0.00244 ];
INF_SP4                   (idx, [1:   8]) = [ -9.79004E-03 0.00234 -5.67114E-04 0.00414  8.80580E-07 1.00000 -8.97352E-03 0.00723 ];
INF_SP5                   (idx, [1:   8]) = [  1.36832E-04 0.13933  3.56205E-05 0.07197 -3.75083E-05 0.04696  6.67214E-03 0.00770 ];
INF_SP6                   (idx, [1:   8]) = [  5.25999E-03 0.00295 -1.36843E-04 0.01571 -4.89522E-05 0.02344 -1.71259E-02 0.00294 ];
INF_SP7                   (idx, [1:   8]) = [  9.27176E-04 0.01074 -1.67119E-04 0.01154 -4.48230E-05 0.02960  5.72998E-04 0.09665 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31958E-01 0.00062  1.20867E+00 0.00576 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33949E-01 0.00106  1.31125E+00 0.00878 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33326E-01 0.00078  1.31190E+00 0.00851 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28681E-01 0.00091  1.04637E+00 0.00574 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43705E+00 0.00062  2.76003E-01 0.00573 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42485E+00 0.00106  2.54673E-01 0.00864 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42864E+00 0.00078  2.54528E-01 0.00856 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45766E+00 0.00091  3.18808E-01 0.00563 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.21468E-03 0.00922  1.33324E-04 0.05923  9.31114E-04 0.02223  8.29703E-04 0.02306  2.30266E-03 0.01419  7.82154E-04 0.02402  2.35725E-04 0.04300 ];
LAMBDA                    (idx, [1:  14]) = [  7.42742E-01 0.02202  1.25591E-02 0.00100  3.08596E-02 0.00068  1.10674E-01 0.00073  3.22667E-01 0.00053  1.29392E+00 0.00255  8.40231E+00 0.01068 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0002' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:56:06 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:38:04 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595300166 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.71831E-01  9.91692E-01  1.01355E+00  1.01506E+00  1.00787E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15279E-02 0.00105  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88472E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06295E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06721E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68091E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.55326E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.55232E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.51026E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.25146E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001010 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00020E+04 0.00091 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00020E+04 0.00091 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.07451E+02 ;
RUNNING_TIME              (idx, 1)        =  1.01975E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.55900E-01  4.55900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.75883E-01  1.34667E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.00152E+02  3.35645E+00  2.60623E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  9.83000E-01  3.22167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.75500E-02  1.33333E-03 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.01975E+02  1.26472E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97621 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99951E+00 0.00022 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79176E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.64735E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.71808E+04 ;
TOT_SF_RATE               (idx, 1)        =  6.57785E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.02884E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.42013E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61847E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.57603E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.82321E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.81744E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.03484E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.84006E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.88365E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.93343E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.95621E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.41698E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.54756E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.59886E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.00160E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.68803E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.43139E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.26867E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  6.29323E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.32249E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.15206E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 16 ;
BURNUP                     (idx, [1:  2])  = [  2.00000E+01  2.00019E+01 ];
BURN_DAYS                 (idx, 1)        =  5.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.11192E-01 0.00107 ];
U233_FISS                 (idx, [1:   4]) = [  8.16921E+09 1.00000  6.13121E-07 1.00000 ];
U235_FISS                 (idx, [1:   4]) = [  5.40549E+15 0.00123  3.96258E-01 0.00100 ];
U238_FISS                 (idx, [1:   4]) = [  1.14805E+15 0.00284  8.41503E-02 0.00266 ];
PU239_FISS                (idx, [1:   4]) = [  5.88951E+15 0.00110  4.31759E-01 0.00094 ];
PU240_FISS                (idx, [1:   4]) = [  1.10443E+13 0.02651  8.09806E-04 0.02652 ];
PU241_FISS                (idx, [1:   4]) = [  1.16801E+15 0.00264  8.56248E-02 0.00256 ];
U235_CAPT                 (idx, [1:   4]) = [  1.20739E+15 0.00267  4.38224E-02 0.00266 ];
U238_CAPT                 (idx, [1:   4]) = [  1.00104E+16 0.00110  3.63281E-01 0.00078 ];
PU239_CAPT                (idx, [1:   4]) = [  3.22887E+15 0.00158  1.17191E-01 0.00155 ];
PU240_CAPT                (idx, [1:   4]) = [  2.23689E+15 0.00193  8.11804E-02 0.00181 ];
PU241_CAPT                (idx, [1:   4]) = [  4.17063E+14 0.00451  1.51372E-02 0.00450 ];
XE135_CAPT                (idx, [1:   4]) = [  7.78051E+14 0.00329  2.82385E-02 0.00326 ];
SM149_CAPT                (idx, [1:   4]) = [  2.31701E+14 0.00586  8.41054E-03 0.00589 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001010 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.06427E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001010 5.00806E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3313336 3.31811E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1640512 1.64279E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 47162 4.71645E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001010 5.00806E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -2.70084E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.68321E+16 2.4E-05  3.68321E+16 2.4E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36458E+16 4.8E-06  1.36458E+16 4.8E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.75646E+16 0.00044  2.09183E+16 0.00046  6.64633E+15 0.00115 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.12104E+16 0.00029  3.45641E+16 0.00028  6.64633E+15 0.00115 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.15206E+16 0.00052  4.15206E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.95940E+18 0.00050  5.17372E+17 0.00050  1.44203E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.91719E+14 0.00474 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.16022E+16 0.00030 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.47713E+18 0.00063 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10193E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10193E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.48103E+00 0.00060 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.71949E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.35903E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23174E+00 0.00040 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93843E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96704E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.95416E-01 0.00064 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.86970E-01 0.00064 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.69914E+00 2.8E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05854E+02 4.8E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.87121E-01 0.00064  8.82421E-01 0.00064  4.54857E-03 0.01097 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.86816E-01 0.00031 ];
COL_KEFF                  (idx, [1:   2]) = [  8.87198E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.86816E-01 0.00031 ];
ABS_KINF                  (idx, [1:   2]) = [  8.95256E-01 0.00030 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70574E+01 0.00025 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70587E+01 0.00010 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.85380E-07 0.00429 ];
IMP_EALF                  (idx, [1:   2]) = [  7.81312E-07 0.00171 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.86064E-01 0.00296 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.85618E-01 0.00123 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.92513E-03 0.00660  1.59126E-04 0.03879  1.03856E-03 0.01441  9.46271E-04 0.01513  2.57990E-03 0.01006  9.27814E-04 0.01558  2.73452E-04 0.02890 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.54740E-01 0.01480  9.47577E-03 0.02559  3.08022E-02 0.00047  1.10729E-01 0.00208  3.22762E-01 0.00036  1.28453E+00 0.00188  7.64537E+00 0.01736 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.09888E-03 0.00932  1.41805E-04 0.06131  9.13042E-04 0.02231  7.66644E-04 0.02439  2.24354E-03 0.01491  8.00272E-04 0.02392  2.33580E-04 0.04435 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.53308E-01 0.02354  1.25780E-02 0.00112  3.07850E-02 0.00066  1.10931E-01 0.00079  3.22770E-01 0.00054  1.28743E+00 0.00261  8.32204E+00 0.01144 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.96077E-05 0.00148  2.95954E-05 0.00149  3.21660E-05 0.01702 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.62601E-05 0.00133  2.62492E-05 0.00134  2.85387E-05 0.01704 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.13336E-03 0.01109  1.40303E-04 0.06894  9.24537E-04 0.02547  7.68909E-04 0.02800  2.25875E-03 0.01670  7.98852E-04 0.02713  2.42008E-04 0.05090 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.54402E-01 0.02747  1.25920E-02 0.00183  3.08239E-02 0.00087  1.10876E-01 0.00105  3.22442E-01 0.00065  1.28758E+00 0.00359  8.20820E+00 0.01694 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.92180E-05 0.00351  2.92132E-05 0.00353  2.81530E-05 0.04861 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.59152E-05 0.00346  2.59110E-05 0.00348  2.49848E-05 0.04889 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.94359E-03 0.03844  1.45814E-04 0.22433  7.97030E-04 0.09574  8.58147E-04 0.08772  2.07358E-03 0.05870  8.58081E-04 0.09155  2.10938E-04 0.16911 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.45718E-01 0.08564  1.25543E-02 0.00362  3.07921E-02 0.00209  1.10873E-01 0.00234  3.22603E-01 0.00180  1.28833E+00 0.00861  7.87042E+00 0.04621 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.95053E-03 0.03758  1.36787E-04 0.22337  7.90566E-04 0.09101  8.71965E-04 0.08769  2.09860E-03 0.05730  8.48299E-04 0.09006  2.04309E-04 0.16604 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.43988E-01 0.08531  1.25543E-02 0.00362  3.07947E-02 0.00209  1.10863E-01 0.00233  3.22614E-01 0.00178  1.28897E+00 0.00858  7.87449E+00 0.04623 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.70072E+02 0.03853 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.94027E-05 0.00091 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.60782E-05 0.00063 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.02634E-03 0.00717 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.70993E+02 0.00720 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.88112E-07 0.00074 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.67634E-06 0.00040  2.67631E-06 0.00040  2.68091E-06 0.00580 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.05285E-05 0.00088  4.05468E-05 0.00088  3.72193E-05 0.01140 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.33636E-01 0.00035  6.33974E-01 0.00036  5.88845E-01 0.00992 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06380E+01 0.01399 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.55232E+01 0.00048  3.38059E+01 0.00056 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.97691E+04 0.00308  3.04552E+05 0.00153  6.10439E+05 0.00100  6.50750E+05 0.00086  5.98043E+05 0.00088  6.40539E+05 0.00056  4.35069E+05 0.00080  3.85296E+05 0.00069  2.94451E+05 0.00050  2.40878E+05 0.00101  2.07498E+05 0.00071  1.86696E+05 0.00085  1.72601E+05 0.00076  1.64004E+05 0.00071  1.59846E+05 0.00085  1.38365E+05 0.00086  1.36533E+05 0.00081  1.35329E+05 0.00100  1.33053E+05 0.00099  2.59784E+05 0.00065  2.51531E+05 0.00069  1.81359E+05 0.00076  1.17735E+05 0.00087  1.35934E+05 0.00085  1.29357E+05 0.00068  1.15435E+05 0.00101  1.87128E+05 0.00078  4.32482E+04 0.00165  5.39329E+04 0.00108  4.87956E+04 0.00154  2.86057E+04 0.00173  4.95794E+04 0.00134  3.28227E+04 0.00162  2.68067E+04 0.00203  4.62034E+03 0.00364  3.94013E+03 0.00333  3.43068E+03 0.00349  3.29624E+03 0.00404  3.36103E+03 0.00362  3.74073E+03 0.00439  4.41746E+03 0.00377  4.49726E+03 0.00391  8.96294E+03 0.00273  1.48134E+04 0.00239  1.92093E+04 0.00205  5.12462E+04 0.00160  5.41820E+04 0.00187  5.80509E+04 0.00137  3.79222E+04 0.00134  2.68525E+04 0.00137  1.99587E+04 0.00173  2.35974E+04 0.00189  4.63938E+04 0.00160  6.59160E+04 0.00114  1.32029E+05 0.00145  2.10059E+05 0.00141  3.20276E+05 0.00128  2.06991E+05 0.00146  1.49272E+05 0.00148  1.07579E+05 0.00169  9.70645E+04 0.00166  9.57709E+04 0.00184  8.02512E+04 0.00148  5.43572E+04 0.00158  5.03308E+04 0.00176  4.48877E+04 0.00166  3.79660E+04 0.00167  3.00570E+04 0.00201  2.01553E+04 0.00182  7.15536E+03 0.00241 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.95647E-01 0.00063 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.54899E+18 0.00068  4.10448E+17 0.00145 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39263E-01 0.00011  1.56377E+00 0.00046 ];
INF_CAPT                  (idx, [1:   4]) = [  8.05861E-03 0.00057  3.67494E-02 0.00084 ];
INF_ABS                   (idx, [1:   4]) = [  9.70761E-03 0.00048  6.37780E-02 0.00113 ];
INF_FISS                  (idx, [1:   4]) = [  1.64900E-03 0.00046  2.70286E-02 0.00154 ];
INF_NSF                   (idx, [1:   4]) = [  4.46812E-03 0.00048  7.28891E-02 0.00158 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.70960E+00 6.7E-05  2.69673E+00 6.0E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05811E+02 7.1E-06  2.05863E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.60241E-08 0.00041  2.59629E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29552E-01 0.00012  1.50005E+00 0.00052 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44554E-01 0.00017  3.96763E-01 0.00058 ];
INF_SCATT2                (idx, [1:   4]) = [  9.65065E-02 0.00023  9.35000E-02 0.00105 ];
INF_SCATT3                (idx, [1:   4]) = [  7.33538E-03 0.00294  2.80797E-02 0.00231 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03551E-02 0.00219 -8.93413E-03 0.00718 ];
INF_SCATT5                (idx, [1:   4]) = [  1.89996E-04 0.09267  6.71941E-03 0.00610 ];
INF_SCATT6                (idx, [1:   4]) = [  5.14479E-03 0.00305 -1.71775E-02 0.00351 ];
INF_SCATT7                (idx, [1:   4]) = [  7.72411E-04 0.01979  4.47279E-04 0.09832 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29595E-01 0.00012  1.50005E+00 0.00052 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44554E-01 0.00017  3.96763E-01 0.00058 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.65067E-02 0.00023  9.35000E-02 0.00105 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.33570E-03 0.00295  2.80797E-02 0.00231 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03551E-02 0.00219 -8.93413E-03 0.00718 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.90010E-04 0.09251  6.71941E-03 0.00610 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.14475E-03 0.00305 -1.71775E-02 0.00351 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.72501E-04 0.01978  4.47279E-04 0.09832 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12251E-01 0.00031  1.01636E+00 0.00043 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.57047E+00 0.00031  3.27969E-01 0.00043 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.66438E-03 0.00047  6.37780E-02 0.00113 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69799E-02 0.00021  6.48424E-02 0.00099 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12283E-01 0.00011  1.72691E-02 0.00050  1.11978E-03 0.00445  1.49893E+00 0.00052 ];
INF_S1                    (idx, [1:   8]) = [  2.39561E-01 0.00016  4.99255E-03 0.00105  4.80789E-04 0.00762  3.96283E-01 0.00058 ];
INF_S2                    (idx, [1:   8]) = [  9.80673E-02 0.00022 -1.56086E-03 0.00305  2.65595E-04 0.00898  9.32344E-02 0.00104 ];
INF_S3                    (idx, [1:   8]) = [  9.12065E-03 0.00229 -1.78527E-03 0.00180  9.63363E-05 0.02065  2.79834E-02 0.00232 ];
INF_S4                    (idx, [1:   8]) = [ -9.79623E-03 0.00219 -5.58848E-04 0.00594  1.64474E-06 0.91767 -8.93577E-03 0.00716 ];
INF_S5                    (idx, [1:   8]) = [  1.50462E-04 0.11876  3.95341E-05 0.07445 -3.79236E-05 0.04467  6.75733E-03 0.00604 ];
INF_S6                    (idx, [1:   8]) = [  5.27723E-03 0.00300 -1.32436E-04 0.01962 -4.88326E-05 0.03799 -1.71287E-02 0.00354 ];
INF_S7                    (idx, [1:   8]) = [  9.40755E-04 0.01572 -1.68344E-04 0.01291 -4.31087E-05 0.03541  4.90388E-04 0.09059 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12326E-01 0.00011  1.72691E-02 0.00050  1.11978E-03 0.00445  1.49893E+00 0.00052 ];
INF_SP1                   (idx, [1:   8]) = [  2.39562E-01 0.00016  4.99255E-03 0.00105  4.80789E-04 0.00762  3.96283E-01 0.00058 ];
INF_SP2                   (idx, [1:   8]) = [  9.80675E-02 0.00022 -1.56086E-03 0.00305  2.65595E-04 0.00898  9.32344E-02 0.00104 ];
INF_SP3                   (idx, [1:   8]) = [  9.12097E-03 0.00230 -1.78527E-03 0.00180  9.63363E-05 0.02065  2.79834E-02 0.00232 ];
INF_SP4                   (idx, [1:   8]) = [ -9.79626E-03 0.00219 -5.58848E-04 0.00594  1.64474E-06 0.91767 -8.93577E-03 0.00716 ];
INF_SP5                   (idx, [1:   8]) = [  1.50476E-04 0.11856  3.95341E-05 0.07445 -3.79236E-05 0.04467  6.75733E-03 0.00604 ];
INF_SP6                   (idx, [1:   8]) = [  5.27719E-03 0.00299 -1.32436E-04 0.01962 -4.88326E-05 0.03799 -1.71287E-02 0.00354 ];
INF_SP7                   (idx, [1:   8]) = [  9.40845E-04 0.01572 -1.68344E-04 0.01291 -4.31087E-05 0.03541  4.90388E-04 0.09059 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32047E-01 0.00063  1.19329E+00 0.00923 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33790E-01 0.00079  1.28202E+00 0.01057 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33645E-01 0.00102  1.29875E+00 0.01116 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28782E-01 0.00082  1.03843E+00 0.00886 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43650E+00 0.00063  2.79911E-01 0.00925 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42580E+00 0.00079  2.60710E-01 0.01064 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42670E+00 0.00102  2.57418E-01 0.01107 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45701E+00 0.00082  3.21605E-01 0.00892 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.09888E-03 0.00932  1.41805E-04 0.06131  9.13042E-04 0.02231  7.66644E-04 0.02439  2.24354E-03 0.01491  8.00272E-04 0.02392  2.33580E-04 0.04435 ];
LAMBDA                    (idx, [1:  14]) = [  7.53308E-01 0.02354  1.25780E-02 0.00112  3.07850E-02 0.00066  1.10931E-01 0.00079  3.22770E-01 0.00054  1.28743E+00 0.00261  8.32204E+00 0.01144 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0002' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:56:06 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:44:09 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595300166 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.88515E-01  9.86742E-01  1.00739E+00  1.01370E+00  1.00365E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15622E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88438E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06316E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06742E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68460E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.54330E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.54236E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.48516E+00 0.00036  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.25332E-01 0.00113  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001077 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00022E+04 0.00093 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00022E+04 0.00093 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.37802E+02 ;
RUNNING_TIME              (idx, 1)        =  1.08053E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.55900E-01  4.55900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  4.03033E-01  1.37167E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.06140E+02  3.38900E+00  2.59887E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.04513E+00  3.18000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  5.09667E-02  6.83331E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.08053E+02  1.26192E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97720 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99945E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79404E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.68030E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68714E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.44153E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.10596E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.47791E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.57431E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.53932E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  3.87436E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.93927E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.02977E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.35487E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  8.44594E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.00378E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.32909E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.39780E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.55615E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.59169E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.50002E+13 ;
CS137_ACTIVITY            (idx, 1)        =  3.35175E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.45985E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.22071E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.14355E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.33924E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.35534E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 17 ;
BURNUP                     (idx, [1:  2])  = [  2.50000E+01  2.50023E+01 ];
BURN_DAYS                 (idx, 1)        =  6.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.53581E-01 0.00103 ];
U235_FISS                 (idx, [1:   4]) = [  4.46873E+15 0.00137  3.28322E-01 0.00121 ];
U238_FISS                 (idx, [1:   4]) = [  1.20045E+15 0.00280  8.81799E-02 0.00257 ];
PU239_FISS                (idx, [1:   4]) = [  6.37959E+15 0.00106  4.68721E-01 0.00088 ];
PU240_FISS                (idx, [1:   4]) = [  1.39912E+13 0.02450  1.02804E-03 0.02449 ];
PU241_FISS                (idx, [1:   4]) = [  1.52275E+15 0.00251  1.11873E-01 0.00239 ];
U235_CAPT                 (idx, [1:   4]) = [  9.98107E+14 0.00296  3.37181E-02 0.00291 ];
U238_CAPT                 (idx, [1:   4]) = [  1.04214E+16 0.00104  3.52040E-01 0.00075 ];
PU239_CAPT                (idx, [1:   4]) = [  3.51087E+15 0.00154  1.18606E-01 0.00145 ];
PU240_CAPT                (idx, [1:   4]) = [  2.63167E+15 0.00188  8.88990E-02 0.00174 ];
PU241_CAPT                (idx, [1:   4]) = [  5.45944E+14 0.00391  1.84436E-02 0.00388 ];
XE135_CAPT                (idx, [1:   4]) = [  7.91418E+14 0.00347  2.67366E-02 0.00345 ];
SM149_CAPT                (idx, [1:   4]) = [  2.41439E+14 0.00600  8.15615E-03 0.00597 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001077 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.18717E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001077 5.00819E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3393355 3.39834E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1560538 1.56266E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 47184 4.71832E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001077 5.00819E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.61239E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.71808E+16 2.5E-05  3.71808E+16 2.5E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36147E+16 5.5E-06  1.36147E+16 5.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.96076E+16 0.00046  2.26759E+16 0.00047  6.93176E+15 0.00117 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.32224E+16 0.00032  3.62906E+16 0.00029  6.93176E+15 0.00117 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.35534E+16 0.00052  4.35534E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.05119E+18 0.00049  5.41255E+17 0.00049  1.50994E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.11042E+14 0.00470 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.36334E+16 0.00032 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.54509E+18 0.00065 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.09615E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.09615E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.44327E+00 0.00063 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.70674E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.29190E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23122E+00 0.00039 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93862E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96680E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.61588E-01 0.00066 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.53457E-01 0.00066 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.73093E+00 3.0E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06324E+02 5.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.53472E-01 0.00068  8.49179E-01 0.00067  4.27788E-03 0.01166 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.53561E-01 0.00032 ];
COL_KEFF                  (idx, [1:   2]) = [  8.53799E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.53561E-01 0.00032 ];
ABS_KINF                  (idx, [1:   2]) = [  8.61690E-01 0.00032 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.69883E+01 0.00025 ];
IMP_ALF                   (idx, [1:   2]) = [  1.69897E+01 0.00010 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  8.41489E-07 0.00426 ];
IMP_EALF                  (idx, [1:   2]) = [  8.37175E-07 0.00174 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.99165E-01 0.00291 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.99158E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.98966E-03 0.00662  1.43566E-04 0.03949  1.09189E-03 0.01452  9.47056E-04 0.01556  2.55896E-03 0.00970  9.53576E-04 0.01655  2.94613E-04 0.02936 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.68579E-01 0.01543  8.90816E-03 0.02877  3.06546E-02 0.00044  1.10631E-01 0.00290  3.22802E-01 0.00036  1.26527E+00 0.00226  7.66700E+00 0.01654 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.96447E-03 0.00987  1.22544E-04 0.05938  9.02684E-04 0.02342  7.94769E-04 0.02417  2.12272E-03 0.01474  7.77970E-04 0.02511  2.43783E-04 0.04363 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.63134E-01 0.02251  1.25867E-02 0.00121  3.06602E-02 0.00064  1.11193E-01 0.00084  3.22805E-01 0.00053  1.27327E+00 0.00291  8.35602E+00 0.01198 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.07323E-05 0.00150  3.07194E-05 0.00151  3.33801E-05 0.01873 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.62226E-05 0.00131  2.62117E-05 0.00132  2.84828E-05 0.01870 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.01960E-03 0.01175  1.19372E-04 0.07357  9.15271E-04 0.02792  7.86390E-04 0.02927  2.15966E-03 0.01759  7.78198E-04 0.03039  2.60713E-04 0.04819 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.81687E-01 0.02722  1.25611E-02 0.00167  3.06153E-02 0.00088  1.11014E-01 0.00107  3.22766E-01 0.00069  1.26912E+00 0.00436  8.21095E+00 0.01709 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.02027E-05 0.00364  3.01904E-05 0.00364  2.80150E-05 0.04644 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.57712E-05 0.00357  2.57607E-05 0.00357  2.38980E-05 0.04640 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.64228E-03 0.03802  1.27803E-04 0.19726  9.00378E-04 0.09271  7.25467E-04 0.09777  1.82927E-03 0.06167  7.99700E-04 0.09126  2.59655E-04 0.18098 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.62916E-01 0.08345  1.25775E-02 0.00402  3.06649E-02 0.00208  1.10882E-01 0.00266  3.23845E-01 0.00213  1.25223E+00 0.01089  7.94298E+00 0.05042 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.61390E-03 0.03756  1.31324E-04 0.19226  8.97640E-04 0.09080  7.27087E-04 0.09391  1.81384E-03 0.05930  7.88082E-04 0.09143  2.55921E-04 0.17683 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.52433E-01 0.08143  1.25775E-02 0.00402  3.06665E-02 0.00208  1.10893E-01 0.00266  3.23835E-01 0.00212  1.25306E+00 0.01079  7.92851E+00 0.05062 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.55469E+02 0.03862 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.04087E-05 0.00099 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.59469E-05 0.00071 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.84600E-03 0.00729 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.59464E+02 0.00741 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.84996E-07 0.00073 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.64828E-06 0.00044  2.64819E-06 0.00044  2.66942E-06 0.00580 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.06649E-05 0.00089  4.06826E-05 0.00089  3.73106E-05 0.01169 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.27017E-01 0.00035  6.27499E-01 0.00035  5.58648E-01 0.01021 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06032E+01 0.01426 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.54236E+01 0.00049  3.36797E+01 0.00055 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.07948E+04 0.00244  3.05143E+05 0.00167  6.11434E+05 0.00083  6.51156E+05 0.00068  5.98333E+05 0.00071  6.40608E+05 0.00075  4.35227E+05 0.00061  3.84943E+05 0.00067  2.95037E+05 0.00085  2.40807E+05 0.00073  2.07492E+05 0.00082  1.87193E+05 0.00090  1.72626E+05 0.00073  1.64217E+05 0.00093  1.60152E+05 0.00098  1.38311E+05 0.00079  1.36548E+05 0.00093  1.35507E+05 0.00089  1.33037E+05 0.00130  2.59905E+05 0.00072  2.51283E+05 0.00071  1.81437E+05 0.00068  1.17787E+05 0.00106  1.35650E+05 0.00078  1.29527E+05 0.00068  1.15207E+05 0.00101  1.86154E+05 0.00063  4.31941E+04 0.00121  5.33007E+04 0.00177  4.85660E+04 0.00152  2.84747E+04 0.00194  4.91569E+04 0.00144  3.23329E+04 0.00168  2.62642E+04 0.00184  4.41087E+03 0.00388  3.74530E+03 0.00373  3.28423E+03 0.00487  3.17296E+03 0.00371  3.22403E+03 0.00430  3.50458E+03 0.00346  4.24632E+03 0.00322  4.37053E+03 0.00391  8.73685E+03 0.00305  1.44773E+04 0.00164  1.88819E+04 0.00193  5.06455E+04 0.00157  5.33443E+04 0.00121  5.72028E+04 0.00144  3.73405E+04 0.00139  2.64153E+04 0.00167  1.96148E+04 0.00211  2.31701E+04 0.00154  4.55559E+04 0.00144  6.48370E+04 0.00170  1.30246E+05 0.00149  2.07732E+05 0.00167  3.17730E+05 0.00143  2.05566E+05 0.00145  1.48322E+05 0.00154  1.07076E+05 0.00156  9.65316E+04 0.00159  9.54088E+04 0.00160  7.99722E+04 0.00176  5.41873E+04 0.00190  5.01170E+04 0.00188  4.47099E+04 0.00188  3.78021E+04 0.00168  2.98837E+04 0.00196  2.00083E+04 0.00198  7.12126E+03 0.00255 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.61933E-01 0.00039 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.62443E+18 0.00034  4.26800E+17 0.00133 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39530E-01 0.00011  1.56674E+00 0.00040 ];
INF_CAPT                  (idx, [1:   4]) = [  8.32510E-03 0.00055  3.76892E-02 0.00082 ];
INF_ABS                   (idx, [1:   4]) = [  9.89514E-03 0.00046  6.36184E-02 0.00102 ];
INF_FISS                  (idx, [1:   4]) = [  1.57004E-03 0.00044  2.59292E-02 0.00134 ];
INF_NSF                   (idx, [1:   4]) = [  4.29353E-03 0.00045  7.07884E-02 0.00139 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.73467E+00 8.8E-05  2.73007E+00 7.6E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06165E+02 1.1E-05  2.06361E+02 1.5E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.55163E-08 0.00036  2.60037E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29638E-01 0.00011  1.50313E+00 0.00046 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44460E-01 0.00018  3.97529E-01 0.00056 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64608E-02 0.00028  9.36942E-02 0.00098 ];
INF_SCATT3                (idx, [1:   4]) = [  7.28955E-03 0.00280  2.80864E-02 0.00280 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03709E-02 0.00174 -8.99692E-03 0.00743 ];
INF_SCATT5                (idx, [1:   4]) = [  1.68262E-04 0.09038  6.70766E-03 0.00811 ];
INF_SCATT6                (idx, [1:   4]) = [  5.14115E-03 0.00296 -1.72780E-02 0.00331 ];
INF_SCATT7                (idx, [1:   4]) = [  7.81332E-04 0.01887  4.95886E-04 0.07475 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29681E-01 0.00011  1.50313E+00 0.00046 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44461E-01 0.00018  3.97529E-01 0.00056 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64610E-02 0.00028  9.36942E-02 0.00098 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.28959E-03 0.00280  2.80864E-02 0.00280 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03714E-02 0.00174 -8.99692E-03 0.00743 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.68376E-04 0.09050  6.70766E-03 0.00811 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.14121E-03 0.00297 -1.72780E-02 0.00331 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.81262E-04 0.01889  4.95886E-04 0.07475 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12266E-01 0.00022  1.01925E+00 0.00032 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.57036E+00 0.00022  3.27038E-01 0.00032 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.85124E-03 0.00046  6.36184E-02 0.00102 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69830E-02 0.00018  6.47148E-02 0.00115 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12547E-01 0.00011  1.70909E-02 0.00043  1.11210E-03 0.00399  1.50202E+00 0.00046 ];
INF_S1                    (idx, [1:   8]) = [  2.39525E-01 0.00018  4.93451E-03 0.00091  4.83450E-04 0.00867  3.97045E-01 0.00056 ];
INF_S2                    (idx, [1:   8]) = [  9.80226E-02 0.00028 -1.56182E-03 0.00208  2.62182E-04 0.01017  9.34320E-02 0.00098 ];
INF_S3                    (idx, [1:   8]) = [  9.05885E-03 0.00231 -1.76930E-03 0.00218  9.24544E-05 0.02577  2.79939E-02 0.00279 ];
INF_S4                    (idx, [1:   8]) = [ -9.82568E-03 0.00184 -5.45251E-04 0.00488 -9.54080E-07 1.00000 -8.99596E-03 0.00746 ];
INF_S5                    (idx, [1:   8]) = [  1.23471E-04 0.11621  4.47904E-05 0.04673 -3.52669E-05 0.04617  6.74292E-03 0.00797 ];
INF_S6                    (idx, [1:   8]) = [  5.27598E-03 0.00274 -1.34829E-04 0.01427 -4.68508E-05 0.02933 -1.72311E-02 0.00330 ];
INF_S7                    (idx, [1:   8]) = [  9.49887E-04 0.01525 -1.68555E-04 0.01020 -4.29914E-05 0.03077  5.38877E-04 0.06808 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12591E-01 0.00011  1.70909E-02 0.00043  1.11210E-03 0.00399  1.50202E+00 0.00046 ];
INF_SP1                   (idx, [1:   8]) = [  2.39526E-01 0.00018  4.93451E-03 0.00091  4.83450E-04 0.00867  3.97045E-01 0.00056 ];
INF_SP2                   (idx, [1:   8]) = [  9.80228E-02 0.00028 -1.56182E-03 0.00208  2.62182E-04 0.01017  9.34320E-02 0.00098 ];
INF_SP3                   (idx, [1:   8]) = [  9.05889E-03 0.00231 -1.76930E-03 0.00218  9.24544E-05 0.02577  2.79939E-02 0.00279 ];
INF_SP4                   (idx, [1:   8]) = [ -9.82612E-03 0.00183 -5.45251E-04 0.00488 -9.54080E-07 1.00000 -8.99596E-03 0.00746 ];
INF_SP5                   (idx, [1:   8]) = [  1.23586E-04 0.11634  4.47904E-05 0.04673 -3.52669E-05 0.04617  6.74292E-03 0.00797 ];
INF_SP6                   (idx, [1:   8]) = [  5.27604E-03 0.00275 -1.34829E-04 0.01427 -4.68508E-05 0.02933 -1.72311E-02 0.00330 ];
INF_SP7                   (idx, [1:   8]) = [  9.49817E-04 0.01527 -1.68555E-04 0.01020 -4.29914E-05 0.03077  5.38877E-04 0.06808 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32146E-01 0.00066  1.21308E+00 0.00622 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33820E-01 0.00088  1.31794E+00 0.01029 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33851E-01 0.00084  1.32552E+00 0.00891 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28844E-01 0.00099  1.04351E+00 0.00452 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43590E+00 0.00066  2.75030E-01 0.00605 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42562E+00 0.00088  2.53560E-01 0.01024 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42543E+00 0.00084  2.51942E-01 0.00869 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45663E+00 0.00099  3.19589E-01 0.00442 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.96447E-03 0.00987  1.22544E-04 0.05938  9.02684E-04 0.02342  7.94769E-04 0.02417  2.12272E-03 0.01474  7.77970E-04 0.02511  2.43783E-04 0.04363 ];
LAMBDA                    (idx, [1:  14]) = [  7.63134E-01 0.02251  1.25867E-02 0.00121  3.06602E-02 0.00064  1.11193E-01 0.00084  3.22805E-01 0.00053  1.27327E+00 0.00291  8.35602E+00 0.01198 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0002' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:56:06 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:49:37 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595300166 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00733E+00  9.98141E-01  1.00312E+00  9.96255E-01  9.95158E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.6E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16262E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88374E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05739E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06166E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68574E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.52789E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.52695E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.47944E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.26562E-01 0.00112  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001639 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00033E+04 0.00097 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00033E+04 0.00097 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.65096E+02 ;
RUNNING_TIME              (idx, 1)        =  1.13520E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.55900E-01  4.55900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  4.29383E-01  1.34833E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.11519E+02  2.96245E+00  2.41637E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.10662E+00  3.14333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  5.39167E-02  6.83331E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.13520E+02  1.25310E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97794 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00024E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79658E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.80648E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68592E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.69429E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.20685E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.55381E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.59959E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.53050E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  5.14916E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.09885E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.25429E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.00084E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  8.94871E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.09801E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.67010E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.43270E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.59790E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.63822E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.19488E+13 ;
CS137_ACTIVITY            (idx, 1)        =  4.01122E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.57292E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.19168E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.78480E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.36871E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.54829E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 18 ;
BURNUP                     (idx, [1:  2])  = [  3.00000E+01  3.00029E+01 ];
BURN_DAYS                 (idx, 1)        =  7.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.95742E-01 0.00110 ];
U235_FISS                 (idx, [1:   4]) = [  3.63141E+15 0.00159  2.67567E-01 0.00141 ];
U238_FISS                 (idx, [1:   4]) = [  1.24546E+15 0.00274  9.17502E-02 0.00250 ];
PU239_FISS                (idx, [1:   4]) = [  6.79558E+15 0.00106  5.00718E-01 0.00081 ];
PU240_FISS                (idx, [1:   4]) = [  1.62892E+13 0.02354  1.19982E-03 0.02347 ];
PU241_FISS                (idx, [1:   4]) = [  1.84838E+15 0.00207  1.36199E-01 0.00199 ];
U235_CAPT                 (idx, [1:   4]) = [  8.13888E+14 0.00340  2.57900E-02 0.00337 ];
U238_CAPT                 (idx, [1:   4]) = [  1.08365E+16 0.00105  3.43353E-01 0.00075 ];
PU239_CAPT                (idx, [1:   4]) = [  3.73405E+15 0.00146  1.18328E-01 0.00144 ];
PU240_CAPT                (idx, [1:   4]) = [  2.98239E+15 0.00185  9.44946E-02 0.00167 ];
PU241_CAPT                (idx, [1:   4]) = [  6.62720E+14 0.00361  2.09997E-02 0.00357 ];
XE135_CAPT                (idx, [1:   4]) = [  8.03206E+14 0.00341  2.54531E-02 0.00341 ];
SM149_CAPT                (idx, [1:   4]) = [  2.54086E+14 0.00595  8.05290E-03 0.00600 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001639 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.13417E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001639 5.00813E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3464829 3.46935E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1490078 1.49205E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46732 4.67313E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001639 5.00813E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.47035E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.74877E+16 2.2E-05  3.74877E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35873E+16 4.8E-06  1.35873E+16 4.8E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.15650E+16 0.00045  2.43974E+16 0.00046  7.16763E+15 0.00123 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.51523E+16 0.00032  3.79847E+16 0.00030  7.16763E+15 0.00123 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.54829E+16 0.00052  4.54829E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.13695E+18 0.00049  5.65115E+17 0.00047  1.57184E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.25164E+14 0.00490 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.55775E+16 0.00032 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.60649E+18 0.00066 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.09039E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.09039E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.40797E+00 0.00063 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.70814E-01 0.00035 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.22432E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23034E+00 0.00041 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93951E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96682E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.31041E-01 0.00068 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.23274E-01 0.00068 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.75903E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06741E+02 4.8E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.23306E-01 0.00069  8.19325E-01 0.00068  3.94934E-03 0.01173 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.23910E-01 0.00033 ];
COL_KEFF                  (idx, [1:   2]) = [  8.24325E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.23910E-01 0.00033 ];
ABS_KINF                  (idx, [1:   2]) = [  8.31679E-01 0.00032 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.69300E+01 0.00026 ];
IMP_ALF                   (idx, [1:   2]) = [  1.69222E+01 0.00011 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  8.92460E-07 0.00448 ];
IMP_EALF                  (idx, [1:   2]) = [  8.95726E-07 0.00179 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.11084E-01 0.00285 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.12590E-01 0.00119 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.99964E-03 0.00679  1.54079E-04 0.04202  1.11902E-03 0.01545  9.25066E-04 0.01639  2.53324E-03 0.00963  9.75152E-04 0.01500  2.93081E-04 0.03028 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.56955E-01 0.01614  8.87112E-03 0.02920  3.05564E-02 0.00040  1.11407E-01 0.00060  3.23256E-01 0.00040  1.24645E+00 0.00241  7.34495E+00 0.01807 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.76887E-03 0.00998  1.18000E-04 0.06288  8.91113E-04 0.02391  7.49481E-04 0.02386  1.98394E-03 0.01560  7.71774E-04 0.02336  2.54562E-04 0.04373 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.87636E-01 0.02470  1.26325E-02 0.00146  3.05414E-02 0.00060  1.11326E-01 0.00087  3.23433E-01 0.00060  1.24550E+00 0.00368  7.98939E+00 0.01411 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.19053E-05 0.00160  3.18967E-05 0.00160  3.35436E-05 0.01903 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.62613E-05 0.00143  2.62541E-05 0.00143  2.76180E-05 0.01903 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.79799E-03 0.01184  1.34813E-04 0.06997  8.89138E-04 0.02914  7.64792E-04 0.02966  2.01931E-03 0.01808  7.48173E-04 0.02974  2.41770E-04 0.05264 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.57255E-01 0.02925  1.26183E-02 0.00201  3.05488E-02 0.00086  1.11469E-01 0.00123  3.23549E-01 0.00079  1.24357E+00 0.00523  7.95405E+00 0.02039 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.12605E-05 0.00366  3.12469E-05 0.00367  3.00628E-05 0.04892 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.57315E-05 0.00362  2.57204E-05 0.00363  2.47353E-05 0.04891 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.68000E-03 0.04135  1.52095E-04 0.26019  8.41070E-04 0.09847  7.84401E-04 0.08877  1.90366E-03 0.06892  7.86409E-04 0.09974  2.12360E-04 0.16639 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.25487E-01 0.09498  1.26889E-02 0.00598  3.04258E-02 0.00193  1.11387E-01 0.00272  3.22765E-01 0.00220  1.24870E+00 0.01184  7.75645E+00 0.05222 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.69755E-03 0.04002  1.53086E-04 0.25602  8.48258E-04 0.09619  7.90111E-04 0.08790  1.90614E-03 0.06602  7.87178E-04 0.09694  2.12775E-04 0.16357 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.23211E-01 0.09490  1.26889E-02 0.00598  3.04226E-02 0.00193  1.11374E-01 0.00271  3.22775E-01 0.00220  1.24837E+00 0.01185  7.74856E+00 0.05214 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.50509E+02 0.04165 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.15957E-05 0.00109 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.60060E-05 0.00077 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.80334E-03 0.00786 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.52111E+02 0.00794 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.80895E-07 0.00078 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.62996E-06 0.00042  2.62976E-06 0.00042  2.66701E-06 0.00616 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.07055E-05 0.00094  4.07245E-05 0.00094  3.69413E-05 0.01186 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.20377E-01 0.00036  6.20976E-01 0.00036  5.34836E-01 0.01152 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08247E+01 0.01525 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.52695E+01 0.00052  3.36233E+01 0.00062 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.16011E+04 0.00363  3.05791E+05 0.00129  6.11188E+05 0.00103  6.49822E+05 0.00082  5.97965E+05 0.00067  6.40950E+05 0.00067  4.35322E+05 0.00086  3.85220E+05 0.00061  2.94638E+05 0.00068  2.40859E+05 0.00091  2.08090E+05 0.00074  1.87373E+05 0.00087  1.72942E+05 0.00071  1.64526E+05 0.00077  1.60244E+05 0.00100  1.38335E+05 0.00092  1.36759E+05 0.00108  1.35650E+05 0.00093  1.33200E+05 0.00108  2.59830E+05 0.00078  2.52047E+05 0.00074  1.81707E+05 0.00075  1.18007E+05 0.00096  1.35887E+05 0.00071  1.29514E+05 0.00075  1.14868E+05 0.00100  1.85674E+05 0.00085  4.30464E+04 0.00139  5.27903E+04 0.00176  4.82159E+04 0.00177  2.84129E+04 0.00216  4.88864E+04 0.00137  3.20276E+04 0.00153  2.57982E+04 0.00142  4.29205E+03 0.00330  3.60958E+03 0.00348  3.16119E+03 0.00460  3.05442E+03 0.00419  3.11046E+03 0.00346  3.40685E+03 0.00379  4.09477E+03 0.00368  4.21953E+03 0.00304  8.55282E+03 0.00287  1.42359E+04 0.00226  1.85919E+04 0.00174  4.99439E+04 0.00108  5.27377E+04 0.00126  5.65437E+04 0.00125  3.67740E+04 0.00125  2.59708E+04 0.00126  1.92424E+04 0.00210  2.27579E+04 0.00146  4.48178E+04 0.00115  6.39253E+04 0.00152  1.28829E+05 0.00159  2.05557E+05 0.00136  3.13999E+05 0.00149  2.03808E+05 0.00135  1.46958E+05 0.00133  1.06183E+05 0.00166  9.57281E+04 0.00171  9.45772E+04 0.00164  7.91621E+04 0.00132  5.37456E+04 0.00155  4.96674E+04 0.00179  4.42880E+04 0.00167  3.75244E+04 0.00207  2.97104E+04 0.00173  1.99037E+04 0.00203  7.09518E+03 0.00234 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.32102E-01 0.00037 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.69594E+18 0.00042  4.41074E+17 0.00143 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39907E-01 0.00012  1.56635E+00 0.00045 ];
INF_CAPT                  (idx, [1:   4]) = [  8.58050E-03 0.00065  3.85766E-02 0.00085 ];
INF_ABS                   (idx, [1:   4]) = [  1.00837E-02 0.00055  6.36071E-02 0.00110 ];
INF_FISS                  (idx, [1:   4]) = [  1.50319E-03 0.00060  2.50305E-02 0.00151 ];
INF_NSF                   (idx, [1:   4]) = [  4.14496E-03 0.00061  6.90691E-02 0.00153 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.75744E+00 6.6E-05  2.75939E+00 4.9E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06486E+02 6.7E-06  2.06800E+02 9.6E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.51177E-08 0.00043  2.60270E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29832E-01 0.00012  1.50276E+00 0.00051 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44397E-01 0.00020  3.97059E-01 0.00054 ];
INF_SCATT2                (idx, [1:   4]) = [  9.64358E-02 0.00034  9.34468E-02 0.00093 ];
INF_SCATT3                (idx, [1:   4]) = [  7.25223E-03 0.00345  2.81282E-02 0.00322 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03728E-02 0.00176 -8.93805E-03 0.00779 ];
INF_SCATT5                (idx, [1:   4]) = [  1.68595E-04 0.11351  6.74484E-03 0.00887 ];
INF_SCATT6                (idx, [1:   4]) = [  5.14556E-03 0.00352 -1.72678E-02 0.00372 ];
INF_SCATT7                (idx, [1:   4]) = [  7.78916E-04 0.01909  4.79445E-04 0.10591 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29876E-01 0.00012  1.50276E+00 0.00051 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44397E-01 0.00020  3.97059E-01 0.00054 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.64360E-02 0.00034  9.34468E-02 0.00093 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.25218E-03 0.00346  2.81282E-02 0.00322 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03727E-02 0.00176 -8.93805E-03 0.00779 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.68521E-04 0.11346  6.74484E-03 0.00887 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.14545E-03 0.00352 -1.72678E-02 0.00372 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.78805E-04 0.01910  4.79445E-04 0.10591 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12406E-01 0.00033  1.01991E+00 0.00042 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56932E+00 0.00033  3.26827E-01 0.00042 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.00401E-02 0.00056  6.36071E-02 0.00110 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69875E-02 0.00018  6.46910E-02 0.00099 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12920E-01 0.00012  1.69126E-02 0.00043  1.10442E-03 0.00463  1.50166E+00 0.00051 ];
INF_S1                    (idx, [1:   8]) = [  2.39516E-01 0.00020  4.88060E-03 0.00091  4.76400E-04 0.00738  3.96582E-01 0.00054 ];
INF_S2                    (idx, [1:   8]) = [  9.79919E-02 0.00033 -1.55615E-03 0.00193  2.63509E-04 0.00880  9.31833E-02 0.00094 ];
INF_S3                    (idx, [1:   8]) = [  9.00505E-03 0.00283 -1.75283E-03 0.00138  9.44072E-05 0.02261  2.80338E-02 0.00322 ];
INF_S4                    (idx, [1:   8]) = [ -9.83957E-03 0.00187 -5.33253E-04 0.00444 -1.47804E-06 1.00000 -8.93657E-03 0.00779 ];
INF_S5                    (idx, [1:   8]) = [  1.23016E-04 0.15697  4.55784E-05 0.06066 -4.07359E-05 0.02909  6.78557E-03 0.00885 ];
INF_S6                    (idx, [1:   8]) = [  5.27967E-03 0.00350 -1.34106E-04 0.01837 -4.92581E-05 0.02485 -1.72185E-02 0.00371 ];
INF_S7                    (idx, [1:   8]) = [  9.47068E-04 0.01600 -1.68152E-04 0.01019 -4.36797E-05 0.02626  5.23124E-04 0.09734 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12963E-01 0.00012  1.69126E-02 0.00043  1.10442E-03 0.00463  1.50166E+00 0.00051 ];
INF_SP1                   (idx, [1:   8]) = [  2.39517E-01 0.00020  4.88060E-03 0.00091  4.76400E-04 0.00738  3.96582E-01 0.00054 ];
INF_SP2                   (idx, [1:   8]) = [  9.79921E-02 0.00033 -1.55615E-03 0.00193  2.63509E-04 0.00880  9.31833E-02 0.00094 ];
INF_SP3                   (idx, [1:   8]) = [  9.00500E-03 0.00284 -1.75283E-03 0.00138  9.44072E-05 0.02261  2.80338E-02 0.00322 ];
INF_SP4                   (idx, [1:   8]) = [ -9.83947E-03 0.00186 -5.33253E-04 0.00444 -1.47804E-06 1.00000 -8.93657E-03 0.00779 ];
INF_SP5                   (idx, [1:   8]) = [  1.22943E-04 0.15690  4.55784E-05 0.06066 -4.07359E-05 0.02909  6.78557E-03 0.00885 ];
INF_SP6                   (idx, [1:   8]) = [  5.27955E-03 0.00350 -1.34106E-04 0.01837 -4.92581E-05 0.02485 -1.72185E-02 0.00371 ];
INF_SP7                   (idx, [1:   8]) = [  9.46956E-04 0.01600 -1.68152E-04 0.01019 -4.36797E-05 0.02626  5.23124E-04 0.09734 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31621E-01 0.00080  1.21418E+00 0.00593 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33190E-01 0.00110  1.32258E+00 0.00841 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33079E-01 0.00100  1.32339E+00 0.00891 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28658E-01 0.00087  1.04438E+00 0.00658 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43916E+00 0.00080  2.74768E-01 0.00598 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42949E+00 0.00109  2.52455E-01 0.00832 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.43017E+00 0.00100  2.52351E-01 0.00876 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45781E+00 0.00086  3.19499E-01 0.00656 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.76887E-03 0.00998  1.18000E-04 0.06288  8.91113E-04 0.02391  7.49481E-04 0.02386  1.98394E-03 0.01560  7.71774E-04 0.02336  2.54562E-04 0.04373 ];
LAMBDA                    (idx, [1:  14]) = [  7.87636E-01 0.02470  1.26325E-02 0.00146  3.05414E-02 0.00060  1.11326E-01 0.00087  3.23433E-01 0.00060  1.24550E+00 0.00368  7.98939E+00 0.01411 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0002' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:56:06 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 05:54:52 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595300166 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.97863E-01  9.96039E-01  1.00658E+00  1.00861E+00  9.90901E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16994E-02 0.00102  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88301E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04912E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05343E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68990E+00 0.00024  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.50763E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.50671E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.47533E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.27474E-01 0.00106  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001333 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00027E+04 0.00096 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00027E+04 0.00096 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.91326E+02 ;
RUNNING_TIME              (idx, 1)        =  1.18775E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.55900E-01  4.55900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  4.53633E-01  1.15667E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.16693E+02  2.92718E+00  2.24717E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.16243E+00  2.42333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  5.62834E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.18775E+02  1.24056E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97853 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00074E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79793E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.94560E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.69216E+04 ;
TOT_SF_RATE               (idx, 1)        =  4.51678E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.30564E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.62909E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.63993E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.52921E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  6.66554E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.26507E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.72599E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.06984E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  9.39554E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.19522E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.98382E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.47404E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.64078E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.68983E+14 ;
CS134_ACTIVITY            (idx, 1)        =  8.05128E+13 ;
CS137_ACTIVITY            (idx, 1)        =  4.66671E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.69588E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.17022E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.50963E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.39869E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.72779E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 19 ;
BURNUP                     (idx, [1:  2])  = [  3.50000E+01  3.50034E+01 ];
BURN_DAYS                 (idx, 1)        =  8.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  8.33108E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  2.93273E+15 0.00170  2.15883E-01 0.00155 ];
U238_FISS                 (idx, [1:   4]) = [  1.29751E+15 0.00277  9.54935E-02 0.00253 ];
PU239_FISS                (idx, [1:   4]) = [  7.15123E+15 0.00104  5.26410E-01 0.00077 ];
PU240_FISS                (idx, [1:   4]) = [  1.89016E+13 0.02202  1.39198E-03 0.02207 ];
PU241_FISS                (idx, [1:   4]) = [  2.13738E+15 0.00204  1.57338E-01 0.00194 ];
U235_CAPT                 (idx, [1:   4]) = [  6.53888E+14 0.00391  1.96149E-02 0.00388 ];
U238_CAPT                 (idx, [1:   4]) = [  1.12181E+16 0.00103  3.36499E-01 0.00078 ];
PU239_CAPT                (idx, [1:   4]) = [  3.91187E+15 0.00144  1.17352E-01 0.00142 ];
PU240_CAPT                (idx, [1:   4]) = [  3.28761E+15 0.00165  9.86208E-02 0.00157 ];
PU241_CAPT                (idx, [1:   4]) = [  7.62503E+14 0.00336  2.28741E-02 0.00334 ];
XE135_CAPT                (idx, [1:   4]) = [  8.09527E+14 0.00333  2.42847E-02 0.00331 ];
SM149_CAPT                (idx, [1:   4]) = [  2.63472E+14 0.00592  7.90379E-03 0.00590 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001333 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.39882E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001333 5.00840E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3520541 3.52562E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1434808 1.43679E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45984 4.59842E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001333 5.00840E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -7.17118E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.77489E+16 2.2E-05  3.77489E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35641E+16 4.5E-06  1.35641E+16 4.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.33423E+16 0.00045  2.59823E+16 0.00046  7.36002E+15 0.00121 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.69064E+16 0.00032  3.95464E+16 0.00030  7.36002E+15 0.00121 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.72779E+16 0.00051  4.72779E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.21485E+18 0.00051  5.86902E+17 0.00045  1.62795E+18 0.00057 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.34881E+14 0.00515 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.73413E+16 0.00033 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.66042E+18 0.00067 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.08462E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.08462E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.37897E+00 0.00066 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.71485E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.16181E-01 0.00037 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23146E+00 0.00041 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.93988E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96796E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.07167E-01 0.00068 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  7.99744E-01 0.00069 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.78301E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.07095E+02 4.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  7.99658E-01 0.00069  7.96012E-01 0.00069  3.73232E-03 0.01297 ];
IMP_KEFF                  (idx, [1:   2]) = [  7.98748E-01 0.00033 ];
COL_KEFF                  (idx, [1:   2]) = [  7.98552E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  7.98748E-01 0.00033 ];
ABS_KINF                  (idx, [1:   2]) = [  8.06163E-01 0.00032 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.68604E+01 0.00027 ];
IMP_ALF                   (idx, [1:   2]) = [  1.68614E+01 0.00010 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  9.57024E-07 0.00460 ];
IMP_EALF                  (idx, [1:   2]) = [  9.51802E-07 0.00169 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.24697E-01 0.00281 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.24645E-01 0.00110 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.11605E-03 0.00728  1.39610E-04 0.04256  1.16553E-03 0.01548  9.44418E-04 0.01649  2.55328E-03 0.01039  1.02510E-03 0.01605  2.88117E-04 0.02925 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.38518E-01 0.01553  8.32473E-03 0.03231  3.04453E-02 0.00041  1.11404E-01 0.00211  3.23566E-01 0.00038  1.23274E+00 0.00258  7.28279E+00 0.01796 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.66733E-03 0.01047  1.07438E-04 0.06427  8.75729E-04 0.02266  7.37871E-04 0.02556  1.93121E-03 0.01629  7.87616E-04 0.02485  2.27460E-04 0.04540 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.51744E-01 0.02474  1.26541E-02 0.00163  3.04427E-02 0.00058  1.11636E-01 0.00093  3.23771E-01 0.00059  1.23114E+00 0.00388  7.95443E+00 0.01460 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.26712E-05 0.00159  3.26568E-05 0.00159  3.53805E-05 0.02041 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.61192E-05 0.00141  2.61077E-05 0.00141  2.82814E-05 0.02035 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.68620E-03 0.01293  1.16219E-04 0.08198  8.84236E-04 0.02805  7.43842E-04 0.02923  1.92590E-03 0.01981  7.89907E-04 0.02973  2.26095E-04 0.05626 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.41899E-01 0.03037  1.26322E-02 0.00246  3.04359E-02 0.00080  1.11785E-01 0.00129  3.23914E-01 0.00088  1.22734E+00 0.00556  7.82526E+00 0.02254 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.20942E-05 0.00389  3.20927E-05 0.00392  2.85798E-05 0.04695 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.56573E-05 0.00380  2.56562E-05 0.00383  2.28535E-05 0.04700 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.82442E-03 0.04135  1.21567E-04 0.26778  1.04574E-03 0.09349  7.22842E-04 0.11078  1.94977E-03 0.06342  7.94280E-04 0.10835  1.90219E-04 0.18276 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  6.89585E-01 0.09394  1.26383E-02 0.00659  3.04902E-02 0.00195  1.11543E-01 0.00294  3.24844E-01 0.00231  1.22397E+00 0.01355  7.46614E+00 0.05615 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.85382E-03 0.04091  1.21278E-04 0.26258  1.06745E-03 0.09188  7.30432E-04 0.10365  1.97012E-03 0.06371  7.71987E-04 0.10695  1.92552E-04 0.17728 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.07790E-01 0.09328  1.26383E-02 0.00659  3.04910E-02 0.00194  1.11548E-01 0.00292  3.24800E-01 0.00229  1.22286E+00 0.01358  7.47122E+00 0.05602 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.51686E+02 0.04171 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.23895E-05 0.00093 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.58943E-05 0.00062 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.83043E-03 0.00770 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.49219E+02 0.00781 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.75907E-07 0.00074 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.61266E-06 0.00043  2.61254E-06 0.00043  2.63455E-06 0.00609 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.06482E-05 0.00090  4.06651E-05 0.00091  3.72917E-05 0.01143 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.14192E-01 0.00037  6.14886E-01 0.00038  5.13825E-01 0.01095 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08339E+01 0.01445 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.50671E+01 0.00048  3.34814E+01 0.00060 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.17566E+04 0.00299  3.07651E+05 0.00108  6.10652E+05 0.00084  6.49313E+05 0.00056  5.98879E+05 0.00061  6.40810E+05 0.00075  4.35330E+05 0.00062  3.85433E+05 0.00073  2.94919E+05 0.00066  2.41240E+05 0.00059  2.07716E+05 0.00104  1.87356E+05 0.00051  1.73059E+05 0.00071  1.64520E+05 0.00102  1.60410E+05 0.00095  1.38380E+05 0.00073  1.36831E+05 0.00097  1.35643E+05 0.00095  1.33455E+05 0.00100  2.60560E+05 0.00067  2.51684E+05 0.00058  1.81171E+05 0.00084  1.17754E+05 0.00089  1.35562E+05 0.00083  1.29501E+05 0.00097  1.14671E+05 0.00092  1.84822E+05 0.00069  4.28281E+04 0.00164  5.23246E+04 0.00154  4.78581E+04 0.00119  2.82809E+04 0.00187  4.86266E+04 0.00143  3.16191E+04 0.00117  2.53565E+04 0.00168  4.16654E+03 0.00341  3.53422E+03 0.00392  3.06604E+03 0.00354  2.97606E+03 0.00346  3.01873E+03 0.00376  3.28352E+03 0.00523  3.96291E+03 0.00438  4.13363E+03 0.00335  8.36620E+03 0.00257  1.39501E+04 0.00291  1.83972E+04 0.00259  4.93793E+04 0.00138  5.19957E+04 0.00147  5.57851E+04 0.00109  3.64589E+04 0.00158  2.56598E+04 0.00135  1.89680E+04 0.00199  2.24669E+04 0.00150  4.41312E+04 0.00150  6.29831E+04 0.00146  1.26790E+05 0.00151  2.02545E+05 0.00133  3.10352E+05 0.00124  2.01174E+05 0.00136  1.45182E+05 0.00163  1.05005E+05 0.00128  9.46437E+04 0.00155  9.34675E+04 0.00138  7.83284E+04 0.00143  5.31577E+04 0.00143  4.92320E+04 0.00146  4.38746E+04 0.00161  3.71551E+04 0.00144  2.93574E+04 0.00160  1.97200E+04 0.00175  7.02409E+03 0.00182 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.05964E-01 0.00059 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.76210E+18 0.00058  4.52794E+17 0.00124 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.40066E-01 0.00010  1.56537E+00 0.00038 ];
INF_CAPT                  (idx, [1:   4]) = [  8.79806E-03 0.00064  3.94023E-02 0.00081 ];
INF_ABS                   (idx, [1:   4]) = [  1.02477E-02 0.00054  6.37219E-02 0.00099 ];
INF_FISS                  (idx, [1:   4]) = [  1.44966E-03 0.00048  2.43195E-02 0.00130 ];
INF_NSF                   (idx, [1:   4]) = [  4.02584E-03 0.00050  6.77148E-02 0.00135 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.77710E+00 6.6E-05  2.78438E+00 5.8E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06759E+02 8.8E-06  2.07173E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.47152E-08 0.00045  2.60432E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29817E-01 0.00010  1.50162E+00 0.00044 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44207E-01 0.00020  3.96561E-01 0.00058 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63580E-02 0.00031  9.33473E-02 0.00103 ];
INF_SCATT3                (idx, [1:   4]) = [  7.26402E-03 0.00410  2.80770E-02 0.00307 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.04088E-02 0.00221 -9.06879E-03 0.00711 ];
INF_SCATT5                (idx, [1:   4]) = [  1.69665E-04 0.11760  6.73407E-03 0.00675 ];
INF_SCATT6                (idx, [1:   4]) = [  5.12369E-03 0.00333 -1.71847E-02 0.00309 ];
INF_SCATT7                (idx, [1:   4]) = [  7.77836E-04 0.01852  5.11132E-04 0.08855 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29862E-01 0.00010  1.50162E+00 0.00044 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44208E-01 0.00020  3.96561E-01 0.00058 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63586E-02 0.00031  9.33473E-02 0.00103 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.26401E-03 0.00411  2.80770E-02 0.00307 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.04088E-02 0.00221 -9.06879E-03 0.00711 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.69854E-04 0.11774  6.73407E-03 0.00675 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.12357E-03 0.00333 -1.71847E-02 0.00309 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.77697E-04 0.01849  5.11132E-04 0.08855 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12563E-01 0.00029  1.02047E+00 0.00037 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56816E+00 0.00029  3.26649E-01 0.00037 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.02027E-02 0.00055  6.37219E-02 0.00099 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70015E-02 0.00021  6.48656E-02 0.00099 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.13064E-01 0.00010  1.67531E-02 0.00039  1.11792E-03 0.00420  1.50050E+00 0.00044 ];
INF_S1                    (idx, [1:   8]) = [  2.39379E-01 0.00020  4.82855E-03 0.00106  4.81720E-04 0.00713  3.96079E-01 0.00058 ];
INF_S2                    (idx, [1:   8]) = [  9.79031E-02 0.00031 -1.54506E-03 0.00247  2.63864E-04 0.00877  9.30834E-02 0.00103 ];
INF_S3                    (idx, [1:   8]) = [  9.00599E-03 0.00320 -1.74197E-03 0.00198  9.54774E-05 0.02019  2.79815E-02 0.00308 ];
INF_S4                    (idx, [1:   8]) = [ -9.87772E-03 0.00228 -5.31109E-04 0.00526  5.51808E-07 1.00000 -9.06934E-03 0.00713 ];
INF_S5                    (idx, [1:   8]) = [  1.19964E-04 0.16238  4.97015E-05 0.04307 -3.64303E-05 0.03857  6.77050E-03 0.00669 ];
INF_S6                    (idx, [1:   8]) = [  5.25340E-03 0.00303 -1.29714E-04 0.01842 -4.60887E-05 0.02481 -1.71386E-02 0.00311 ];
INF_S7                    (idx, [1:   8]) = [  9.42978E-04 0.01470 -1.65142E-04 0.01380 -4.28980E-05 0.02492  5.54030E-04 0.08166 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.13109E-01 0.00010  1.67531E-02 0.00039  1.11792E-03 0.00420  1.50050E+00 0.00044 ];
INF_SP1                   (idx, [1:   8]) = [  2.39379E-01 0.00020  4.82855E-03 0.00106  4.81720E-04 0.00713  3.96079E-01 0.00058 ];
INF_SP2                   (idx, [1:   8]) = [  9.79037E-02 0.00031 -1.54506E-03 0.00247  2.63864E-04 0.00877  9.30834E-02 0.00103 ];
INF_SP3                   (idx, [1:   8]) = [  9.00598E-03 0.00321 -1.74197E-03 0.00198  9.54774E-05 0.02019  2.79815E-02 0.00308 ];
INF_SP4                   (idx, [1:   8]) = [ -9.87764E-03 0.00228 -5.31109E-04 0.00526  5.51808E-07 1.00000 -9.06934E-03 0.00713 ];
INF_SP5                   (idx, [1:   8]) = [  1.20152E-04 0.16253  4.97015E-05 0.04307 -3.64303E-05 0.03857  6.77050E-03 0.00669 ];
INF_SP6                   (idx, [1:   8]) = [  5.25329E-03 0.00303 -1.29714E-04 0.01842 -4.60887E-05 0.02481 -1.71386E-02 0.00311 ];
INF_SP7                   (idx, [1:   8]) = [  9.42839E-04 0.01468 -1.65142E-04 0.01380 -4.28980E-05 0.02492  5.54030E-04 0.08166 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31939E-01 0.00061  1.22105E+00 0.00794 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33702E-01 0.00068  1.33867E+00 0.01012 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33540E-01 0.00069  1.31762E+00 0.01049 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28653E-01 0.00119  1.05298E+00 0.00712 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43717E+00 0.00061  2.73406E-01 0.00802 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42634E+00 0.00068  2.49601E-01 0.00986 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42732E+00 0.00070  2.53669E-01 0.01078 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45786E+00 0.00119  3.16948E-01 0.00716 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.66733E-03 0.01047  1.07438E-04 0.06427  8.75729E-04 0.02266  7.37871E-04 0.02556  1.93121E-03 0.01629  7.87616E-04 0.02485  2.27460E-04 0.04540 ];
LAMBDA                    (idx, [1:  14]) = [  7.51744E-01 0.02474  1.26541E-02 0.00163  3.04427E-02 0.00058  1.11636E-01 0.00093  3.23771E-01 0.00059  1.23114E+00 0.00388  7.95443E+00 0.01460 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0002' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 03:56:06 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 06:00:08 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595300166 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.80841E-01  1.01827E+00  1.01520E+00  1.00233E+00  9.83356E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.5E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.17648E-02 0.00108  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88235E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.03764E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04197E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69296E+00 0.00024  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.48079E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.47991E+01 0.00054  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.47200E+00 0.00042  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.27321E-01 0.00111  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001415 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00028E+04 0.00104 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00028E+04 0.00104 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  6.17548E+02 ;
RUNNING_TIME              (idx, 1)        =  1.24028E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.55900E-01  4.55900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  4.79833E-01  1.29500E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.21864E+02  2.93808E+00  2.23263E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.21807E+00  2.74000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  5.91667E-02  6.00000E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.24028E+02  1.24028E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97910 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99853E+00 0.00021 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79857E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  1.00721E+17 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.69937E+04 ;
TOT_SF_RATE               (idx, 1)        =  6.94285E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.39215E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.69632E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.67987E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.52969E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  8.41058E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.42790E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  7.43131E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.13829E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  9.79268E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.28960E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.27431E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.50993E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.67732E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.73437E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.00191E+14 ;
CS137_ACTIVITY            (idx, 1)        =  5.31797E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.80555E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.15254E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.27234E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.42513E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.88879E+12 0.00056  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 20 ;
BURNUP                     (idx, [1:  2])  = [  4.00000E+01  4.00039E+01 ];
BURN_DAYS                 (idx, 1)        =  1.00000E+03 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  8.69305E-01 0.00110 ];
U235_FISS                 (idx, [1:   4]) = [  2.33840E+15 0.00201  1.72616E-01 0.00192 ];
U238_FISS                 (idx, [1:   4]) = [  1.33638E+15 0.00286  9.86341E-02 0.00268 ];
PU239_FISS                (idx, [1:   4]) = [  7.41384E+15 0.00109  5.47249E-01 0.00079 ];
PU240_FISS                (idx, [1:   4]) = [  2.14944E+13 0.02216  1.58673E-03 0.02223 ];
PU241_FISS                (idx, [1:   4]) = [  2.37604E+15 0.00203  1.75382E-01 0.00186 ];
U235_CAPT                 (idx, [1:   4]) = [  5.20597E+14 0.00452  1.48792E-02 0.00448 ];
U238_CAPT                 (idx, [1:   4]) = [  1.15712E+16 0.00109  3.30700E-01 0.00075 ];
PU239_CAPT                (idx, [1:   4]) = [  4.05910E+15 0.00147  1.16024E-01 0.00146 ];
PU240_CAPT                (idx, [1:   4]) = [  3.55691E+15 0.00173  1.01657E-01 0.00158 ];
PU241_CAPT                (idx, [1:   4]) = [  8.52379E+14 0.00347  2.43625E-02 0.00343 ];
XE135_CAPT                (idx, [1:   4]) = [  8.23363E+14 0.00345  2.35347E-02 0.00345 ];
SM149_CAPT                (idx, [1:   4]) = [  2.70349E+14 0.00599  7.72707E-03 0.00596 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001415 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.39088E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001415 5.00839E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3573273 3.57840E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1383810 1.38566E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44332 4.43301E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001415 5.00839E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.51926E-09 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50059E+05 0.0E+00  4.50059E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.79726E+16 2.1E-05  3.79726E+16 2.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35444E+16 4.5E-06  1.35444E+16 4.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.49514E+16 0.00046  2.74787E+16 0.00047  7.47280E+15 0.00129 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.84958E+16 0.00033  4.10230E+16 0.00032  7.47280E+15 0.00129 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.88879E+16 0.00056  4.88879E+16 0.00056  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.28319E+18 0.00054  6.07541E+17 0.00053  1.67565E+18 0.00059 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.33459E+14 0.00510 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.89293E+16 0.00034 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.70380E+18 0.00070 ];
INI_FMASS                 (idx, 1)        =  1.12515E+01 ;
TOT_FMASS                 (idx, 1)        =  1.07887E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12515E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.07887E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.34962E+00 0.00069 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.72935E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.09477E-01 0.00039 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23311E+00 0.00042 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94201E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96915E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  7.83919E-01 0.00073 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  7.76968E-01 0.00073 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.80358E+00 2.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.07396E+02 4.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  7.76995E-01 0.00073  7.73421E-01 0.00073  3.54660E-03 0.01298 ];
IMP_KEFF                  (idx, [1:   2]) = [  7.77419E-01 0.00034 ];
COL_KEFF                  (idx, [1:   2]) = [  7.76850E-01 0.00056 ];
ABS_KEFF                  (idx, [1:   2]) = [  7.77419E-01 0.00034 ];
ABS_KINF                  (idx, [1:   2]) = [  7.84378E-01 0.00033 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.68027E+01 0.00029 ];
IMP_ALF                   (idx, [1:   2]) = [  1.68059E+01 0.00011 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  1.01449E-06 0.00483 ];
IMP_EALF                  (idx, [1:   2]) = [  1.00632E-06 0.00194 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.36032E-01 0.00298 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.35465E-01 0.00122 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.20922E-03 0.00690  1.46937E-04 0.04022  1.16434E-03 0.01499  9.37286E-04 0.01647  2.61036E-03 0.01051  1.04769E-03 0.01602  3.02597E-04 0.03017 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.35179E-01 0.01612  9.04613E-03 0.02852  3.03463E-02 0.00036  1.11699E-01 0.00212  3.23557E-01 0.00045  1.21855E+00 0.00272  6.77998E+00 0.02107 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.63830E-03 0.01055  1.10204E-04 0.06335  8.80674E-04 0.02429  6.94041E-04 0.02577  1.96090E-03 0.01630  7.76197E-04 0.02486  2.16286E-04 0.04493 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.15450E-01 0.02386  1.27191E-02 0.00177  3.03585E-02 0.00056  1.12016E-01 0.00096  3.23796E-01 0.00066  1.21955E+00 0.00404  7.44663E+00 0.01758 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.34229E-05 0.00165  3.34089E-05 0.00165  3.64564E-05 0.02122 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.59626E-05 0.00148  2.59517E-05 0.00149  2.83197E-05 0.02118 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.56532E-03 0.01303  9.92487E-05 0.08398  8.09068E-04 0.02931  6.77818E-04 0.03285  1.98662E-03 0.01912  7.91197E-04 0.03141  2.01369E-04 0.05987 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.18237E-01 0.03017  1.26841E-02 0.00289  3.03386E-02 0.00081  1.12038E-01 0.00137  3.23605E-01 0.00086  1.23065E+00 0.00554  7.72260E+00 0.02534 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.26684E-05 0.00384  3.26596E-05 0.00387  2.87921E-05 0.05219 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.53792E-05 0.00383  2.53723E-05 0.00385  2.23739E-05 0.05228 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.47289E-03 0.04545  9.53529E-05 0.32878  8.27838E-04 0.11147  5.79238E-04 0.11896  2.08348E-03 0.06434  7.56824E-04 0.10184  1.30161E-04 0.21791 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  5.88858E-01 0.08455  1.28810E-02 0.01029  3.03607E-02 0.00190  1.11966E-01 0.00342  3.24873E-01 0.00232  1.20891E+00 0.01432  6.92894E+00 0.08061 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.48993E-03 0.04501  9.19931E-05 0.33452  8.36473E-04 0.11048  5.80059E-04 0.11775  2.06367E-03 0.06351  7.73435E-04 0.10403  1.44301E-04 0.21459 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  5.98441E-01 0.08477  1.28810E-02 0.01029  3.03546E-02 0.00189  1.12005E-01 0.00341  3.24979E-01 0.00232  1.20869E+00 0.01430  6.92894E+00 0.08061 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.38087E+02 0.04589 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.30252E-05 0.00101 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.56535E-05 0.00069 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.58832E-03 0.00847 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.39052E+02 0.00861 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.68637E-07 0.00085 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.59967E-06 0.00045  2.59955E-06 0.00045  2.62217E-06 0.00625 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.04011E-05 0.00099  4.04174E-05 0.00098  3.70487E-05 0.01160 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.07648E-01 0.00039  6.08412E-01 0.00040  4.96188E-01 0.01039 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08489E+01 0.01488 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.47991E+01 0.00054  3.33030E+01 0.00065 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.20653E+04 0.00353  3.07704E+05 0.00163  6.10551E+05 0.00071  6.50040E+05 0.00069  5.98690E+05 0.00086  6.42227E+05 0.00082  4.35862E+05 0.00077  3.86235E+05 0.00088  2.95014E+05 0.00074  2.41671E+05 0.00072  2.08458E+05 0.00095  1.87475E+05 0.00087  1.73317E+05 0.00083  1.64933E+05 0.00067  1.60560E+05 0.00079  1.38620E+05 0.00101  1.37087E+05 0.00116  1.35963E+05 0.00089  1.33678E+05 0.00084  2.60551E+05 0.00077  2.52045E+05 0.00072  1.81627E+05 0.00096  1.17913E+05 0.00079  1.35209E+05 0.00100  1.29384E+05 0.00071  1.14508E+05 0.00112  1.84241E+05 0.00080  4.27656E+04 0.00162  5.18199E+04 0.00109  4.75318E+04 0.00155  2.81624E+04 0.00181  4.81097E+04 0.00141  3.10399E+04 0.00176  2.48610E+04 0.00214  4.12774E+03 0.00403  3.41813E+03 0.00336  2.99629E+03 0.00454  2.92144E+03 0.00457  2.94235E+03 0.00442  3.19911E+03 0.00275  3.88108E+03 0.00439  4.03477E+03 0.00433  8.19049E+03 0.00264  1.38310E+04 0.00234  1.81412E+04 0.00169  4.87616E+04 0.00121  5.12978E+04 0.00133  5.53048E+04 0.00135  3.57990E+04 0.00153  2.53041E+04 0.00170  1.87127E+04 0.00157  2.20943E+04 0.00150  4.33941E+04 0.00174  6.17477E+04 0.00127  1.24694E+05 0.00141  1.99100E+05 0.00163  3.05061E+05 0.00183  1.97859E+05 0.00186  1.42844E+05 0.00201  1.03220E+05 0.00213  9.31794E+04 0.00221  9.20897E+04 0.00180  7.71168E+04 0.00223  5.23129E+04 0.00214  4.83907E+04 0.00190  4.31493E+04 0.00224  3.65376E+04 0.00252  2.88466E+04 0.00238  1.94068E+04 0.00235  6.90357E+03 0.00259 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  7.83800E-01 0.00063 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.82263E+18 0.00060  4.60610E+17 0.00147 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.40179E-01 0.00013  1.56160E+00 0.00048 ];
INF_CAPT                  (idx, [1:   4]) = [  9.00800E-03 0.00059  4.02419E-02 0.00085 ];
INF_ABS                   (idx, [1:   4]) = [  1.04137E-02 0.00051  6.40902E-02 0.00106 ];
INF_FISS                  (idx, [1:   4]) = [  1.40573E-03 0.00046  2.38484E-02 0.00144 ];
INF_NSF                   (idx, [1:   4]) = [  3.92767E-03 0.00046  6.69139E-02 0.00149 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.79405E+00 7.4E-05  2.80580E+00 6.9E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06993E+02 1.0E-05  2.07490E+02 1.4E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.42964E-08 0.00040  2.60403E-06 0.00021 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29752E-01 0.00014  1.49748E+00 0.00055 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43894E-01 0.00021  3.95312E-01 0.00053 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62548E-02 0.00027  9.31119E-02 0.00068 ];
INF_SCATT3                (idx, [1:   4]) = [  7.24928E-03 0.00283  2.79455E-02 0.00248 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03088E-02 0.00164 -8.96709E-03 0.00737 ];
INF_SCATT5                (idx, [1:   4]) = [  2.06533E-04 0.07201  6.80944E-03 0.00723 ];
INF_SCATT6                (idx, [1:   4]) = [  5.16140E-03 0.00279 -1.71233E-02 0.00240 ];
INF_SCATT7                (idx, [1:   4]) = [  7.68288E-04 0.01500  5.53792E-04 0.09214 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29797E-01 0.00014  1.49748E+00 0.00055 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43895E-01 0.00021  3.95312E-01 0.00053 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62549E-02 0.00027  9.31119E-02 0.00068 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.24956E-03 0.00283  2.79455E-02 0.00248 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03088E-02 0.00164 -8.96709E-03 0.00737 ];
INF_SCATTP5               (idx, [1:   4]) = [  2.06436E-04 0.07208  6.80944E-03 0.00723 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.16127E-03 0.00279 -1.71233E-02 0.00240 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.68305E-04 0.01501  5.53792E-04 0.09214 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12750E-01 0.00030  1.01916E+00 0.00047 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56679E+00 0.00030  3.27068E-01 0.00047 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.03687E-02 0.00051  6.40902E-02 0.00106 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69920E-02 0.00026  6.52241E-02 0.00130 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.13187E-01 0.00013  1.65649E-02 0.00057  1.10701E-03 0.00549  1.49637E+00 0.00055 ];
INF_S1                    (idx, [1:   8]) = [  2.39128E-01 0.00021  4.76631E-03 0.00100  4.75577E-04 0.00700  3.94836E-01 0.00053 ];
INF_S2                    (idx, [1:   8]) = [  9.77903E-02 0.00026 -1.53541E-03 0.00228  2.57722E-04 0.00897  9.28542E-02 0.00068 ];
INF_S3                    (idx, [1:   8]) = [  8.97000E-03 0.00231 -1.72071E-03 0.00160  9.30184E-05 0.01473  2.78525E-02 0.00248 ];
INF_S4                    (idx, [1:   8]) = [ -9.78913E-03 0.00180 -5.19631E-04 0.00423  1.26454E-06 1.00000 -8.96835E-03 0.00745 ];
INF_S5                    (idx, [1:   8]) = [  1.55386E-04 0.09037  5.11473E-05 0.03715 -3.73430E-05 0.04275  6.84678E-03 0.00722 ];
INF_S6                    (idx, [1:   8]) = [  5.28894E-03 0.00256 -1.27541E-04 0.01815 -4.51743E-05 0.02783 -1.70782E-02 0.00241 ];
INF_S7                    (idx, [1:   8]) = [  9.29612E-04 0.01066 -1.61325E-04 0.01631 -4.28360E-05 0.03001  5.96628E-04 0.08512 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.13232E-01 0.00013  1.65649E-02 0.00057  1.10701E-03 0.00549  1.49637E+00 0.00055 ];
INF_SP1                   (idx, [1:   8]) = [  2.39129E-01 0.00021  4.76631E-03 0.00100  4.75577E-04 0.00700  3.94836E-01 0.00053 ];
INF_SP2                   (idx, [1:   8]) = [  9.77904E-02 0.00026 -1.53541E-03 0.00228  2.57722E-04 0.00897  9.28542E-02 0.00068 ];
INF_SP3                   (idx, [1:   8]) = [  8.97028E-03 0.00231 -1.72071E-03 0.00160  9.30184E-05 0.01473  2.78525E-02 0.00248 ];
INF_SP4                   (idx, [1:   8]) = [ -9.78922E-03 0.00180 -5.19631E-04 0.00423  1.26454E-06 1.00000 -8.96835E-03 0.00745 ];
INF_SP5                   (idx, [1:   8]) = [  1.55289E-04 0.09049  5.11473E-05 0.03715 -3.73430E-05 0.04275  6.84678E-03 0.00722 ];
INF_SP6                   (idx, [1:   8]) = [  5.28881E-03 0.00257 -1.27541E-04 0.01815 -4.51743E-05 0.02783 -1.70782E-02 0.00241 ];
INF_SP7                   (idx, [1:   8]) = [  9.29630E-04 0.01067 -1.61325E-04 0.01631 -4.28360E-05 0.03001  5.96628E-04 0.08512 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32254E-01 0.00062  1.21709E+00 0.00776 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33889E-01 0.00088  1.31840E+00 0.00861 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33838E-01 0.00088  1.33749E+00 0.01044 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29105E-01 0.00091  1.04426E+00 0.00778 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43523E+00 0.00062  2.74266E-01 0.00764 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42520E+00 0.00088  2.53279E-01 0.00854 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42551E+00 0.00088  2.49859E-01 0.01021 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45496E+00 0.00091  3.19661E-01 0.00765 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.63830E-03 0.01055  1.10204E-04 0.06335  8.80674E-04 0.02429  6.94041E-04 0.02577  1.96090E-03 0.01630  7.76197E-04 0.02486  2.16286E-04 0.04493 ];
LAMBDA                    (idx, [1:  14]) = [  7.15450E-01 0.02386  1.27191E-02 0.00177  3.03585E-02 0.00056  1.12016E-01 0.00096  3.23796E-01 0.00066  1.21955E+00 0.00404  7.44663E+00 0.01758 ];

