#! /usr/bin/env python3
import pickle
#from pygmo import *
from pygmo import population, problem, algorithm, plot_non_dominated_fronts, moead, nsga2, hypervolume, fast_non_dominated_sorting, plot_non_dominated_fronts, bfe, member_bfe, hypervolume
import sys, os
import copy
import problem_dir_micro as pdm
import matplotlib.pyplot as plt
import numpy as np
from pygmo_micro import save_data

hyper = []
pickle_dir = sys.argv[1]
file_list = [x.split(".")[0] for x in os.listdir(pickle_dir) if x.startswith("pop") and x.endswith(".pickle")]
#run through the files in order.
print (str(file_list))
for data_file in sorted(file_list):#, key=lambda a: a.split("/")[2]):
    try:
        print (pickle_dir+"/"+data_file+".pickle")
        pickle_file = open(pickle_dir+"/"+data_file+".pickle", "rb")
        print("opened file")
        save = pickle.load(pickle_file)
    except:
        print("error opening '' pickle file")
        exit()
    else:
        hyper.append(hypervolume(save.pop))

ref_point = hyper[0].refpoint(offset = 10.)
# Just a figure and one subplot
f, ax = plt.subplots()
ax.plot([x for x in range(1,101)],[x.compute(ref_point) for x in hyper])
ax.set_title('Hypervolume plot')
plt.show()
#for results_dir in [x[0].startswith("gen") for x in os.walk(directory)]
