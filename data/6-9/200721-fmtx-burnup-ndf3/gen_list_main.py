import sys, os
import re
import numpy as np
import pickle

from data_store import data_store

def slurp_the_serpent(action_numbers, s_dir="./runs/"):
    template_file = "slurm_template"
    slurm_out = "./slurm_serpent_{:04d}".format(int(action_numbers[0]))
    options = ""
    for a in action_numbers:
        options += " "+str(a)
        options += " "+s_dir
        #options += " "+"/hpc_{:04d}".format(int(a)) #"+str(a).zfill(4)
    try:
        tempfile = open(template_file, 'r')
    except:
        print("failed to open template")
    else:
        filedata = tempfile.read()
        # Replace the target strings
        filedata = filedata.replace('#options#', '"'+options+'"')
        # Write the file out again
        try:
            f = open(slurm_out, 'w')
        except:
            print("failed to open serpent file")
        else:
            f.write(filedata)
            f.close()
    os.system("sbatch " + slurm_out)

def main():
    num_runs = int(sys.argv[1])
    serpent_dir = "./runs"
    try:
        os.stat(serpent_dir)
    except:
        os.mkdir(serpent_dir)
    action_list = []
    for i in range(num_runs):
        #working_file = {:04d}".format(int(i))
        os.system("python3 set_assembly_u235.py {:04d} ./runs/hpc_{:04d} not_random".format(int(i), int(i)))
        action_list.append(i)
        if len(action_list) == 6:
            slurp_the_serpent(action_list)
            action_list = []
            #print(Y[i])
    if len(action_list) > 0:
        slurp_the_serpent(action_list)
        action_list = []

if __name__ == "__main__":
    main()
