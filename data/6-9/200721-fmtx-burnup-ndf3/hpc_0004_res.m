
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0004' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:47:07 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:51:04 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292427 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00602E+00  1.00378E+00  1.00955E+00  9.91847E-01  9.88804E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13887E-02 0.00103  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88611E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.95178E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.95604E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.72243E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.68713E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.68634E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.47001E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.44305E-01 0.00107  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000245 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00005E+04 0.00066 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00005E+04 0.00066 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.78166E+01 ;
RUNNING_TIME              (idx, 1)        =  3.95127E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  4.83567E-01  4.83567E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.51667E-03  1.51667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.46592E+00  3.46592E+00  0.00000E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.95087E+00  0.00000E+00 ];
CPU_USAGE                 (idx, 1)        = 4.50908 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99895E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  8.65330E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.64301E+08 ;
TOT_DECAY_HEAT            (idx, 1)        =  6.58382E-04 ;
TOT_SF_RATE               (idx, 1)        =  7.42217E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  8.64301E+08 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  6.58382E-04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  7.90874E+03 ;
INGESTION_TOXICITY        (idx, 1)        =  4.17489E+01 ;
ACTINIDE_INH_TOX          (idx, 1)        =  7.90874E+03 ;
ACTINIDE_ING_TOX          (idx, 1)        =  4.17489E+01 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  1.10784E+08 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  8.63924E+08 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  3.54612E+08 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.99257E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 0 ;
BURNUP                     (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
BURN_DAYS                 (idx, 1)        =  0.00000E+00 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.99619E-01 0.00109 ];
U235_FISS                 (idx, [1:   4]) = [  1.30403E+16 0.00057  9.40108E-01 0.00016 ];
U238_FISS                 (idx, [1:   4]) = [  8.29381E+14 0.00269  5.97870E-02 0.00256 ];
U235_CAPT                 (idx, [1:   4]) = [  2.78440E+15 0.00134  1.75578E-01 0.00130 ];
U238_CAPT                 (idx, [1:   4]) = [  7.85034E+15 0.00100  4.94967E-01 0.00064 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000245 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.37057E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000245 5.00737E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2646031 2.64984E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2314379 2.31769E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 39835 3.98396E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000245 5.00737E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -1.13621E-07 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.41074E+16 1.1E-05  3.41074E+16 1.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38681E+16 1.3E-06  1.38681E+16 1.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.58635E+16 0.00048  1.07464E+16 0.00051  5.11709E+15 0.00097 ];
TOT_ABSRATE               (idx, [1:   6]) = [  2.97316E+16 0.00026  2.46145E+16 0.00022  5.11709E+15 0.00097 ];
TOT_SRCRATE               (idx, [1:   6]) = [  2.99257E+16 0.00049  2.99257E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.45528E+18 0.00046  3.93148E+17 0.00043  1.06214E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.38475E+14 0.00505 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  2.99701E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.10465E+18 0.00057 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12514E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.78563E+00 0.00037 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.70279E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.77407E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23350E+00 0.00033 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94792E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97226E-01 2.2E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.14923E+00 0.00049 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.14007E+00 0.00049 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.45941E+00 1.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02554E+02 1.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.13995E+00 0.00049  1.13202E+00 0.00049  8.05040E-03 0.00763 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.13974E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.13987E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.13974E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.14890E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.76129E+01 0.00018 ];
IMP_ALF                   (idx, [1:   2]) = [  1.76145E+01 7.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.49684E-07 0.00316 ];
IMP_EALF                  (idx, [1:   2]) = [  4.48094E-07 0.00135 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.02805E-01 0.00293 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.02028E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.28350E-03 0.00561  1.79165E-04 0.03227  9.96631E-04 0.01334  9.87973E-04 0.01292  2.88552E-03 0.00827  9.26932E-04 0.01453  3.07283E-04 0.02437 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.01589E-01 0.01263  1.06921E-02 0.01836  3.16626E-02 0.00021  1.10129E-01 0.00028  3.20554E-01 0.00021  1.34605E+00 0.00016  8.65882E+00 0.00751 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.06892E-03 0.00804  2.11064E-04 0.04839  1.12223E-03 0.02019  1.13711E-03 0.01938  3.18103E-03 0.01166  1.07543E-03 0.02099  3.42050E-04 0.03653 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.00322E-01 0.01896  1.24907E-02 2.7E-06  3.16506E-02 0.00033  1.10171E-01 0.00041  3.20618E-01 0.00032  1.34585E+00 0.00023  8.88670E+00 0.00203 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.66952E-05 0.00111  2.66838E-05 0.00111  2.82562E-05 0.01145 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.04271E-05 0.00097  3.04140E-05 0.00097  3.22089E-05 0.01144 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.06561E-03 0.00776  2.05792E-04 0.04794  1.10697E-03 0.02013  1.14315E-03 0.01932  3.20629E-03 0.01115  1.05699E-03 0.02123  3.46412E-04 0.03608 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.05687E-01 0.01958  1.24907E-02 3.9E-06  3.16568E-02 0.00032  1.10150E-01 0.00044  3.20407E-01 0.00033  1.34600E+00 0.00026  8.90409E+00 0.00238 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.65783E-05 0.00245  2.65638E-05 0.00246  2.83892E-05 0.02663 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.02952E-05 0.00243  3.02786E-05 0.00244  3.23836E-05 0.02672 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.94370E-03 0.02456  1.99171E-04 0.14826  1.12229E-03 0.06310  1.16565E-03 0.06109  3.06662E-03 0.03714  1.00626E-03 0.06323  3.83707E-04 0.11126 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.56381E-01 0.05875  1.24909E-02 9.4E-06  3.16275E-02 0.00085  1.10171E-01 0.00102  3.20172E-01 0.00097  1.34643E+00 0.00058  8.89245E+00 0.00505 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.88204E-03 0.02383  1.90185E-04 0.15064  1.12819E-03 0.06131  1.16035E-03 0.05919  3.01901E-03 0.03602  1.00740E-03 0.06155  3.76904E-04 0.10628 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.75358E-01 0.05774  1.24909E-02 9.4E-06  3.16272E-02 0.00084  1.10179E-01 0.00102  3.20152E-01 0.00094  1.34636E+00 0.00058  8.89386E+00 0.00505 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.62254E+02 0.02468 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.66135E-05 0.00069 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.03342E-05 0.00047 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.10475E-03 0.00471 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.67004E+02 0.00473 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.35491E-07 0.00058 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87937E-06 0.00043  2.87903E-06 0.00043  2.92891E-06 0.00475 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.25199E-05 0.00074  4.25416E-05 0.00074  3.95781E-05 0.00796 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.75195E-01 0.00032  6.74495E-01 0.00032  7.98940E-01 0.00883 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.01394E+01 0.01257 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.68634E+01 0.00043  3.61833E+01 0.00044 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.15967E+04 0.00353  2.91059E+05 0.00130  6.03113E+05 0.00133  6.49752E+05 0.00071  5.99716E+05 0.00084  6.43333E+05 0.00057  4.36824E+05 0.00073  3.85823E+05 0.00071  2.95703E+05 0.00071  2.41623E+05 0.00073  2.08253E+05 0.00081  1.87623E+05 0.00079  1.73271E+05 0.00077  1.64959E+05 0.00088  1.60565E+05 0.00063  1.38786E+05 0.00087  1.37157E+05 0.00085  1.35771E+05 0.00082  1.33113E+05 0.00086  2.60401E+05 0.00072  2.51089E+05 0.00071  1.81276E+05 0.00093  1.17225E+05 0.00108  1.35410E+05 0.00090  1.28043E+05 0.00098  1.16393E+05 0.00109  1.91551E+05 0.00058  4.36886E+04 0.00180  5.48709E+04 0.00101  4.98570E+04 0.00190  2.88933E+04 0.00201  5.04524E+04 0.00157  3.41923E+04 0.00155  2.92219E+04 0.00182  5.57663E+03 0.00403  5.48729E+03 0.00344  5.63876E+03 0.00402  5.84813E+03 0.00389  5.77278E+03 0.00363  5.67490E+03 0.00289  5.90578E+03 0.00393  5.57057E+03 0.00411  1.04740E+04 0.00319  1.67663E+04 0.00243  2.13116E+04 0.00214  5.61298E+04 0.00144  5.91975E+04 0.00120  6.51321E+04 0.00123  4.49011E+04 0.00128  3.39265E+04 0.00118  2.63950E+04 0.00160  3.13639E+04 0.00112  5.99453E+04 0.00120  8.15944E+04 0.00139  1.57493E+05 0.00094  2.43183E+05 0.00089  3.63040E+05 0.00094  2.31696E+05 0.00081  1.65667E+05 0.00100  1.18887E+05 0.00110  1.06585E+05 0.00095  1.04838E+05 0.00115  8.74647E+04 0.00141  5.90179E+04 0.00107  5.44422E+04 0.00137  4.84214E+04 0.00144  4.08876E+04 0.00121  3.21843E+04 0.00119  2.15398E+04 0.00119  7.62139E+03 0.00151 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.14903E+00 0.00046 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.11933E+18 0.00050  3.35979E+17 0.00082 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37807E-01 0.00012  1.49619E+00 0.00030 ];
INF_CAPT                  (idx, [1:   4]) = [  6.34614E-03 0.00072  2.60751E-02 0.00023 ];
INF_ABS                   (idx, [1:   4]) = [  8.59663E-03 0.00059  5.98594E-02 0.00057 ];
INF_FISS                  (idx, [1:   4]) = [  2.25049E-03 0.00048  3.37844E-02 0.00085 ];
INF_NSF                   (idx, [1:   4]) = [  5.76516E-03 0.00047  8.23224E-02 0.00085 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.56174E+00 6.1E-05  2.43670E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03832E+02 6.2E-06  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  5.90913E-08 0.00048  2.55590E-06 0.00012 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29212E-01 0.00012  1.43633E+00 0.00033 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43986E-01 0.00019  3.79124E-01 0.00041 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61885E-02 0.00025  8.99785E-02 0.00086 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32462E-03 0.00277  2.70842E-02 0.00268 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03240E-02 0.00177 -8.31886E-03 0.00765 ];
INF_SCATT5                (idx, [1:   4]) = [  1.16176E-04 0.15521  6.25309E-03 0.00512 ];
INF_SCATT6                (idx, [1:   4]) = [  5.05763E-03 0.00339 -1.61851E-02 0.00280 ];
INF_SCATT7                (idx, [1:   4]) = [  7.36060E-04 0.02235  3.82586E-04 0.12775 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29252E-01 0.00012  1.43633E+00 0.00033 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43986E-01 0.00019  3.79124E-01 0.00041 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61884E-02 0.00025  8.99785E-02 0.00086 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32457E-03 0.00278  2.70842E-02 0.00268 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03241E-02 0.00177 -8.31886E-03 0.00765 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.16342E-04 0.15498  6.25309E-03 0.00512 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.05744E-03 0.00340 -1.61851E-02 0.00280 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.36112E-04 0.02236  3.82586E-04 0.12775 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13729E-01 0.00024  9.65792E-01 0.00029 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55961E+00 0.00024  3.45140E-01 0.00029 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.55722E-03 0.00058  5.98594E-02 0.00057 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69570E-02 0.00022  6.09277E-02 0.00062 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10850E-01 0.00012  1.83617E-02 0.00042  1.07132E-03 0.00379  1.43526E+00 0.00033 ];
INF_S1                    (idx, [1:   8]) = [  2.38618E-01 0.00019  5.36764E-03 0.00101  4.59118E-04 0.00700  3.78665E-01 0.00042 ];
INF_S2                    (idx, [1:   8]) = [  9.77652E-02 0.00025 -1.57668E-03 0.00226  2.50051E-04 0.00894  8.97284E-02 0.00087 ];
INF_S3                    (idx, [1:   8]) = [  9.21157E-03 0.00213 -1.88695E-03 0.00213  8.64209E-05 0.01922  2.69977E-02 0.00271 ];
INF_S4                    (idx, [1:   8]) = [ -9.68969E-03 0.00192 -6.34303E-04 0.00420 -2.27877E-07 1.00000 -8.31863E-03 0.00773 ];
INF_S5                    (idx, [1:   8]) = [  9.86057E-05 0.18054  1.75706E-05 0.15673 -3.60817E-05 0.03274  6.28917E-03 0.00510 ];
INF_S6                    (idx, [1:   8]) = [  5.20174E-03 0.00310 -1.44106E-04 0.01646 -4.65906E-05 0.02483 -1.61385E-02 0.00280 ];
INF_S7                    (idx, [1:   8]) = [  9.11164E-04 0.01765 -1.75103E-04 0.01508 -4.10414E-05 0.02772  4.23628E-04 0.11568 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10890E-01 0.00012  1.83617E-02 0.00042  1.07132E-03 0.00379  1.43526E+00 0.00033 ];
INF_SP1                   (idx, [1:   8]) = [  2.38619E-01 0.00019  5.36764E-03 0.00101  4.59118E-04 0.00700  3.78665E-01 0.00042 ];
INF_SP2                   (idx, [1:   8]) = [  9.77651E-02 0.00025 -1.57668E-03 0.00226  2.50051E-04 0.00894  8.97284E-02 0.00087 ];
INF_SP3                   (idx, [1:   8]) = [  9.21151E-03 0.00214 -1.88695E-03 0.00213  8.64209E-05 0.01922  2.69977E-02 0.00271 ];
INF_SP4                   (idx, [1:   8]) = [ -9.68981E-03 0.00192 -6.34303E-04 0.00420 -2.27877E-07 1.00000 -8.31863E-03 0.00773 ];
INF_SP5                   (idx, [1:   8]) = [  9.87713E-05 0.18021  1.75706E-05 0.15673 -3.60817E-05 0.03274  6.28917E-03 0.00510 ];
INF_SP6                   (idx, [1:   8]) = [  5.20155E-03 0.00311 -1.44106E-04 0.01646 -4.65906E-05 0.02483 -1.61385E-02 0.00280 ];
INF_SP7                   (idx, [1:   8]) = [  9.11215E-04 0.01766 -1.75103E-04 0.01508 -4.10414E-05 0.02772  4.23628E-04 0.11568 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32500E-01 0.00065  1.05945E+00 0.00407 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33881E-01 0.00104  1.11919E+00 0.00566 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33893E-01 0.00105  1.12450E+00 0.00524 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29785E-01 0.00081  9.54033E-01 0.00464 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43370E+00 0.00065  3.14753E-01 0.00407 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42526E+00 0.00104  2.98060E-01 0.00558 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42519E+00 0.00106  2.96625E-01 0.00530 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45066E+00 0.00081  3.49574E-01 0.00463 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.06892E-03 0.00804  2.11064E-04 0.04839  1.12223E-03 0.02019  1.13711E-03 0.01938  3.18103E-03 0.01166  1.07543E-03 0.02099  3.42050E-04 0.03653 ];
LAMBDA                    (idx, [1:  14]) = [  8.00322E-01 0.01896  1.24907E-02 2.7E-06  3.16506E-02 0.00033  1.10171E-01 0.00041  3.20618E-01 0.00032  1.34585E+00 0.00023  8.88670E+00 0.00203 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0004' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:47:07 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:57:18 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292427 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00988E+00  9.98234E-01  1.00506E+00  1.00252E+00  9.84305E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13432E-02 0.00099  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88657E-01 1.1E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.96961E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.97387E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.71417E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.65871E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.65790E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.29403E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.37232E-01 0.00102  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000705 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00014E+04 0.00077 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00014E+04 0.00077 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.89150E+01 ;
RUNNING_TIME              (idx, 1)        =  1.01766E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.83567E-01  4.83567E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.50833E-02  6.63333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.61968E+00  3.44753E+00  2.70623E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.74833E-02  2.83833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.30000E-03  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.01765E+01  1.28778E+02 ];
CPU_USAGE                 (idx, 1)        = 4.80659 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99947E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.38983E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  7.89562E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.75676E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.42202E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.20973E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  8.57831E+02 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  6.68587E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.67096E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.22278E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  1.75546E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.38285E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  3.50963E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.84492E+06 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  1.40449E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.28332E+11 ;
TE132_ACTIVITY            (idx, 1)        =  2.51444E+14 ;
I131_ACTIVITY             (idx, 1)        =  7.42428E+13 ;
I132_ACTIVITY             (idx, 1)        =  2.43621E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.63319E+07 ;
CS137_ACTIVITY            (idx, 1)        =  1.34676E+11 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  6.58192E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.65011E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.31585E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  9.07700E+16 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.10463E+12 0.00048  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 1 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E-01  1.00009E-01 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+00 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.13634E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  1.29781E+16 0.00058  9.36326E-01 0.00018 ];
U238_FISS                 (idx, [1:   4]) = [  8.56710E+14 0.00284  6.18005E-02 0.00268 ];
PU239_FISS                (idx, [1:   4]) = [  2.40393E+13 0.01630  1.73401E-03 0.01626 ];
PU241_FISS                (idx, [1:   4]) = [  6.37930E+09 1.00000  4.65008E-07 1.00000 ];
U235_CAPT                 (idx, [1:   4]) = [  2.79451E+15 0.00148  1.64592E-01 0.00135 ];
U238_CAPT                 (idx, [1:   4]) = [  8.06325E+15 0.00099  4.74888E-01 0.00065 ];
PU239_CAPT                (idx, [1:   4]) = [  1.38586E+13 0.02143  8.16596E-04 0.02140 ];
PU240_CAPT                (idx, [1:   4]) = [  1.79468E+11 0.19910  1.05771E-05 0.19926 ];
XE135_CAPT                (idx, [1:   4]) = [  7.16204E+14 0.00295  4.21826E-02 0.00288 ];
SM149_CAPT                (idx, [1:   4]) = [  2.05056E+13 0.01813  1.20825E-03 0.01819 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000705 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.25283E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000705 5.00725E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2730809 2.73441E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2229413 2.23236E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 40483 4.04867E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000705 5.00725E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.19095E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.41267E+16 1.2E-05  3.41267E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38667E+16 1.4E-06  1.38667E+16 1.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.69616E+16 0.00046  1.17372E+16 0.00046  5.22442E+15 0.00106 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.08283E+16 0.00025  2.56039E+16 0.00021  5.22442E+15 0.00106 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.10463E+16 0.00048  3.10463E+16 0.00048  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.50205E+18 0.00046  4.05115E+17 0.00044  1.09693E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.51423E+14 0.00535 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.10797E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.13715E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12503E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12503E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.69926E+00 0.00042 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.74023E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.77445E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24328E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94759E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97129E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.10771E+00 0.00051 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.09874E+00 0.00052 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46105E+00 1.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02574E+02 1.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.09856E+00 0.00053  1.09103E+00 0.00053  7.71251E-03 0.00811 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.09968E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.09935E+00 0.00048 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.09968E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.10866E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75393E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75389E+01 8.2E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.84343E-07 0.00355 ];
IMP_EALF                  (idx, [1:   2]) = [  4.83310E-07 0.00144 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.09063E-01 0.00301 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.09796E-01 0.00118 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.49562E-03 0.00516  1.83950E-04 0.03079  1.02304E-03 0.01259  1.02133E-03 0.01357  2.99167E-03 0.00759  9.53899E-04 0.01419  3.21736E-04 0.02398 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.09011E-01 0.01257  1.09419E-02 0.01684  3.16420E-02 0.00023  1.10192E-01 0.00027  3.20568E-01 0.00021  1.34539E+00 0.00016  8.71450E+00 0.00662 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.09360E-03 0.00760  1.95953E-04 0.04912  1.11389E-03 0.01961  1.09943E-03 0.01931  3.29456E-03 0.01110  1.03007E-03 0.02076  3.59713E-04 0.03465 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.21367E-01 0.01846  1.24907E-02 2.5E-06  3.16449E-02 0.00032  1.10257E-01 0.00042  3.20599E-01 0.00033  1.34542E+00 0.00024  8.88329E+00 0.00195 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.65570E-05 0.00113  2.65430E-05 0.00113  2.85001E-05 0.01108 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.91699E-05 0.00096  2.91545E-05 0.00095  3.13053E-05 0.01107 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.02275E-03 0.00816  1.76164E-04 0.05037  1.09545E-03 0.02061  1.10928E-03 0.02104  3.23250E-03 0.01161  1.03745E-03 0.02158  3.71903E-04 0.03485 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.39621E-01 0.01871  1.24907E-02 3.6E-06  3.16506E-02 0.00037  1.10275E-01 0.00048  3.20612E-01 0.00034  1.34533E+00 0.00027  8.88511E+00 0.00236 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.64802E-05 0.00249  2.64659E-05 0.00251  2.79637E-05 0.02756 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.90863E-05 0.00244  2.90705E-05 0.00246  3.07286E-05 0.02759 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.03149E-03 0.02681  1.39278E-04 0.17731  1.09376E-03 0.06511  1.13255E-03 0.06484  3.23742E-03 0.03695  1.14476E-03 0.06350  2.83716E-04 0.11637 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.84788E-01 0.05698  1.24909E-02 1.2E-05  3.16564E-02 0.00082  1.10233E-01 0.00111  3.20702E-01 0.00099  1.34485E+00 0.00062  8.95252E+00 0.00594 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.01668E-03 0.02601  1.43838E-04 0.17836  1.09166E-03 0.06237  1.11602E-03 0.06356  3.20748E-03 0.03614  1.17618E-03 0.06074  2.81501E-04 0.11188 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.91793E-01 0.05459  1.24909E-02 1.2E-05  3.16557E-02 0.00082  1.10238E-01 0.00111  3.20632E-01 0.00096  1.34490E+00 0.00061  8.95909E+00 0.00596 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.66195E+02 0.02673 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.65148E-05 0.00074 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.91240E-05 0.00051 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.98219E-03 0.00467 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.63351E+02 0.00464 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.25093E-07 0.00065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87822E-06 0.00043  2.87803E-06 0.00044  2.90423E-06 0.00486 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.15469E-05 0.00080  4.15713E-05 0.00081  3.81484E-05 0.00829 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.75232E-01 0.00032  6.74716E-01 0.00032  7.64326E-01 0.00815 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02437E+01 0.01312 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.65790E+01 0.00047  3.55640E+01 0.00042 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.19209E+04 0.00249  2.90692E+05 0.00146  6.03008E+05 0.00084  6.49230E+05 0.00079  5.98558E+05 0.00076  6.43658E+05 0.00089  4.37050E+05 0.00073  3.86337E+05 0.00074  2.95572E+05 0.00083  2.41619E+05 0.00083  2.08560E+05 0.00074  1.87646E+05 0.00097  1.73254E+05 0.00078  1.64680E+05 0.00086  1.60425E+05 0.00073  1.38502E+05 0.00095  1.36803E+05 0.00084  1.35519E+05 0.00080  1.33190E+05 0.00094  2.59893E+05 0.00054  2.50520E+05 0.00062  1.81185E+05 0.00063  1.17130E+05 0.00119  1.35443E+05 0.00094  1.28268E+05 0.00107  1.16564E+05 0.00107  1.91563E+05 0.00070  4.37553E+04 0.00175  5.46312E+04 0.00165  4.99168E+04 0.00174  2.89866E+04 0.00182  5.03045E+04 0.00157  3.42269E+04 0.00145  2.91719E+04 0.00242  5.56275E+03 0.00348  5.52777E+03 0.00336  5.66002E+03 0.00340  5.87001E+03 0.00362  5.77629E+03 0.00311  5.68486E+03 0.00335  5.88081E+03 0.00345  5.51000E+03 0.00433  1.04817E+04 0.00273  1.67618E+04 0.00240  2.12470E+04 0.00212  5.61148E+04 0.00149  5.92050E+04 0.00129  6.51155E+04 0.00128  4.48145E+04 0.00152  3.37856E+04 0.00143  2.61846E+04 0.00174  3.11081E+04 0.00140  5.94322E+04 0.00130  8.04944E+04 0.00115  1.54662E+05 0.00113  2.37913E+05 0.00090  3.54116E+05 0.00099  2.25647E+05 0.00091  1.61289E+05 0.00106  1.15760E+05 0.00119  1.03891E+05 0.00139  1.02228E+05 0.00142  8.52521E+04 0.00133  5.75722E+04 0.00136  5.31008E+04 0.00120  4.72531E+04 0.00139  3.98976E+04 0.00148  3.13592E+04 0.00192  2.09953E+04 0.00201  7.42033E+03 0.00236 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.10832E+00 0.00061 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.16080E+18 0.00067  3.41275E+17 0.00099 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37892E-01 0.00012  1.50257E+00 0.00045 ];
INF_CAPT                  (idx, [1:   4]) = [  6.35231E-03 0.00061  2.80964E-02 0.00032 ];
INF_ABS                   (idx, [1:   4]) = [  8.59673E-03 0.00049  6.10995E-02 0.00070 ];
INF_FISS                  (idx, [1:   4]) = [  2.24442E-03 0.00052  3.30031E-02 0.00103 ];
INF_NSF                   (idx, [1:   4]) = [  5.75143E-03 0.00052  8.04474E-02 0.00103 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.56255E+00 4.8E-05  2.43757E+00 3.6E-07 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03839E+02 5.5E-06  2.02281E+02 5.6E-08 ];
INF_INVV                  (idx, [1:   4]) = [  5.90936E-08 0.00033  2.55038E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29296E-01 0.00013  1.44144E+00 0.00048 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44082E-01 0.00019  3.81284E-01 0.00047 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62395E-02 0.00026  9.05063E-02 0.00057 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32453E-03 0.00252  2.72693E-02 0.00187 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03260E-02 0.00211 -8.32377E-03 0.00585 ];
INF_SCATT5                (idx, [1:   4]) = [  1.36103E-04 0.16195  6.29494E-03 0.00569 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07163E-03 0.00220 -1.62750E-02 0.00291 ];
INF_SCATT7                (idx, [1:   4]) = [  7.40903E-04 0.01802  2.92396E-04 0.13650 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29335E-01 0.00013  1.44144E+00 0.00048 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44082E-01 0.00019  3.81284E-01 0.00047 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62397E-02 0.00026  9.05063E-02 0.00057 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32453E-03 0.00252  2.72693E-02 0.00187 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03260E-02 0.00211 -8.32377E-03 0.00585 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.36146E-04 0.16208  6.29494E-03 0.00569 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07152E-03 0.00220 -1.62750E-02 0.00291 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.40700E-04 0.01801  2.92396E-04 0.13650 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13652E-01 0.00027  9.68067E-01 0.00039 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56018E+00 0.00027  3.44330E-01 0.00039 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.55794E-03 0.00049  6.10995E-02 0.00070 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69653E-02 0.00018  6.22217E-02 0.00073 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10927E-01 0.00012  1.83694E-02 0.00039  1.09190E-03 0.00395  1.44035E+00 0.00049 ];
INF_S1                    (idx, [1:   8]) = [  2.38703E-01 0.00018  5.37816E-03 0.00114  4.65621E-04 0.00784  3.80819E-01 0.00047 ];
INF_S2                    (idx, [1:   8]) = [  9.78220E-02 0.00025 -1.58249E-03 0.00226  2.56210E-04 0.01033  9.02501E-02 0.00057 ];
INF_S3                    (idx, [1:   8]) = [  9.21080E-03 0.00193 -1.88627E-03 0.00173  9.27919E-05 0.02492  2.71765E-02 0.00187 ];
INF_S4                    (idx, [1:   8]) = [ -9.69648E-03 0.00221 -6.29470E-04 0.00468 -1.02530E-07 1.00000 -8.32367E-03 0.00585 ];
INF_S5                    (idx, [1:   8]) = [  1.22855E-04 0.17707  1.32484E-05 0.21704 -3.87346E-05 0.04035  6.33367E-03 0.00566 ];
INF_S6                    (idx, [1:   8]) = [  5.21928E-03 0.00234 -1.47650E-04 0.01487 -4.84012E-05 0.03094 -1.62266E-02 0.00291 ];
INF_S7                    (idx, [1:   8]) = [  9.16603E-04 0.01483 -1.75700E-04 0.01399 -4.44567E-05 0.02823  3.36852E-04 0.12022 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10965E-01 0.00013  1.83694E-02 0.00039  1.09190E-03 0.00395  1.44035E+00 0.00049 ];
INF_SP1                   (idx, [1:   8]) = [  2.38704E-01 0.00018  5.37816E-03 0.00114  4.65621E-04 0.00784  3.80819E-01 0.00047 ];
INF_SP2                   (idx, [1:   8]) = [  9.78222E-02 0.00025 -1.58249E-03 0.00226  2.56210E-04 0.01033  9.02501E-02 0.00057 ];
INF_SP3                   (idx, [1:   8]) = [  9.21080E-03 0.00193 -1.88627E-03 0.00173  9.27919E-05 0.02492  2.71765E-02 0.00187 ];
INF_SP4                   (idx, [1:   8]) = [ -9.69650E-03 0.00221 -6.29470E-04 0.00468 -1.02530E-07 1.00000 -8.32367E-03 0.00585 ];
INF_SP5                   (idx, [1:   8]) = [  1.22897E-04 0.17721  1.32484E-05 0.21704 -3.87346E-05 0.04035  6.33367E-03 0.00566 ];
INF_SP6                   (idx, [1:   8]) = [  5.21917E-03 0.00235 -1.47650E-04 0.01487 -4.84012E-05 0.03094 -1.62266E-02 0.00291 ];
INF_SP7                   (idx, [1:   8]) = [  9.16400E-04 0.01482 -1.75700E-04 0.01399 -4.44567E-05 0.02823  3.36852E-04 0.12022 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32372E-01 0.00051  1.08073E+00 0.00714 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34109E-01 0.00084  1.14640E+00 0.00857 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33801E-01 0.00084  1.15930E+00 0.00935 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29278E-01 0.00096  9.61382E-01 0.00591 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43449E+00 0.00051  3.08806E-01 0.00707 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42386E+00 0.00085  2.91279E-01 0.00856 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42574E+00 0.00084  2.88130E-01 0.00930 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45387E+00 0.00096  3.47011E-01 0.00585 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.09360E-03 0.00760  1.95953E-04 0.04912  1.11389E-03 0.01961  1.09943E-03 0.01931  3.29456E-03 0.01110  1.03007E-03 0.02076  3.59713E-04 0.03465 ];
LAMBDA                    (idx, [1:  14]) = [  8.21367E-01 0.01846  1.24907E-02 2.5E-06  3.16449E-02 0.00032  1.10257E-01 0.00042  3.20599E-01 0.00033  1.34542E+00 0.00024  8.88329E+00 0.00195 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0004' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:47:07 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:03:30 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292427 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.01037E+00  9.97972E-01  1.00597E+00  1.00182E+00  9.83868E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 6.6E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13377E-02 0.00102  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88662E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.97695E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.98121E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.71103E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.64357E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.64276E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.21362E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.34222E-01 0.00105  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000677 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00014E+04 0.00075 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00014E+04 0.00075 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  7.99320E+01 ;
RUNNING_TIME              (idx, 1)        =  1.63857E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.83567E-01  4.83567E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.02167E-02  7.15000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.57556E+01  3.44583E+00  2.69012E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.15133E-01  2.82333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  6.66667E-03  8.16667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.63855E+01  1.28137E+02 ];
CPU_USAGE                 (idx, 1)        = 4.87816 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00035E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.56657E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.74400E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.83678E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.43573E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.60126E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.11903E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.14272E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.72485E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.67716E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.01375E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  8.85172E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.53407E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  1.79199E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  2.36034E+07 ;
SR90_ACTIVITY             (idx, 1)        =  6.37787E+11 ;
TE132_ACTIVITY            (idx, 1)        =  5.63386E+14 ;
I131_ACTIVITY             (idx, 1)        =  2.65358E+14 ;
I132_ACTIVITY             (idx, 1)        =  5.66291E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.80379E+09 ;
CS137_ACTIVITY            (idx, 1)        =  6.73861E+11 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.67198E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.63907E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  6.76544E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.12037E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.14015E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 2 ;
BURNUP                     (idx, [1:  2])  = [  5.00000E-01  5.00044E-01 ];
BURN_DAYS                 (idx, 1)        =  1.25000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.13349E-01 0.00110 ];
U235_FISS                 (idx, [1:   4]) = [  1.26943E+16 0.00058  9.15670E-01 0.00019 ];
U238_FISS                 (idx, [1:   4]) = [  8.70090E+14 0.00271  6.27495E-02 0.00250 ];
PU239_FISS                (idx, [1:   4]) = [  2.96477E+14 0.00446  2.13846E-02 0.00440 ];
PU240_FISS                (idx, [1:   4]) = [  3.13692E+10 0.44547  2.25352E-06 0.44545 ];
PU241_FISS                (idx, [1:   4]) = [  1.31966E+11 0.21384  9.52721E-06 0.21385 ];
U235_CAPT                 (idx, [1:   4]) = [  2.74462E+15 0.00148  1.58395E-01 0.00140 ];
U238_CAPT                 (idx, [1:   4]) = [  8.09792E+15 0.00104  4.67297E-01 0.00069 ];
PU239_CAPT                (idx, [1:   4]) = [  1.64933E+14 0.00616  9.51747E-03 0.00610 ];
PU240_CAPT                (idx, [1:   4]) = [  7.53840E+12 0.02774  4.35063E-04 0.02773 ];
PU241_CAPT                (idx, [1:   4]) = [  5.00668E+10 0.35114  2.89260E-06 0.35109 ];
XE135_CAPT                (idx, [1:   4]) = [  7.23145E+14 0.00300  4.17351E-02 0.00298 ];
SM149_CAPT                (idx, [1:   4]) = [  1.24316E+14 0.00678  7.17382E-03 0.00674 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000677 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.28079E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000677 5.00728E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2755465 2.75918E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2204665 2.20756E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 40547 4.05493E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000677 5.00728E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.23986E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.42295E+16 1.1E-05  3.42295E+16 1.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38589E+16 1.4E-06  1.38589E+16 1.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.73014E+16 0.00047  1.20588E+16 0.00048  5.24262E+15 0.00102 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.11603E+16 0.00026  2.59177E+16 0.00022  5.24262E+15 0.00102 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.14015E+16 0.00050  3.14015E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.51502E+18 0.00045  4.08291E+17 0.00043  1.10673E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.54673E+14 0.00487 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.14150E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.14542E+18 0.00058 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12456E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12456E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.68272E+00 0.00041 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.75423E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.76958E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24470E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94747E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97128E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.09939E+00 0.00051 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.09048E+00 0.00052 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46986E+00 1.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02688E+02 1.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.09065E+00 0.00052  1.08292E+00 0.00052  7.55546E-03 0.00782 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.09123E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.09019E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.09123E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.10016E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75078E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75127E+01 7.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.99655E-07 0.00333 ];
IMP_EALF                  (idx, [1:   2]) = [  4.96072E-07 0.00138 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.12870E-01 0.00290 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.12162E-01 0.00113 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.49836E-03 0.00522  1.84438E-04 0.03127  1.04396E-03 0.01364  1.01661E-03 0.01296  2.98193E-03 0.00781  9.64110E-04 0.01422  3.07307E-04 0.02549 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.88527E-01 0.01304  1.07170E-02 0.01821  3.16253E-02 0.00024  1.10196E-01 0.00031  3.20760E-01 0.00022  1.34546E+00 0.00017  8.55718E+00 0.00905 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.00442E-03 0.00716  1.98371E-04 0.04653  1.13618E-03 0.01848  1.06497E-03 0.01912  3.26010E-03 0.01104  1.01533E-03 0.02067  3.29473E-04 0.03811 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.84177E-01 0.01933  1.24907E-02 3.8E-06  3.16193E-02 0.00036  1.10217E-01 0.00043  3.20799E-01 0.00031  1.34543E+00 0.00024  8.90085E+00 0.00209 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.63279E-05 0.00117  2.63147E-05 0.00118  2.84924E-05 0.01126 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.87107E-05 0.00106  2.86963E-05 0.00107  3.10747E-05 0.01127 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.91941E-03 0.00793  1.90711E-04 0.04923  1.09983E-03 0.01988  1.08209E-03 0.01965  3.19420E-03 0.01158  1.01911E-03 0.02219  3.33471E-04 0.03833 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.93161E-01 0.01993  1.24907E-02 4.3E-06  3.16287E-02 0.00038  1.10180E-01 0.00045  3.20702E-01 0.00037  1.34549E+00 0.00027  8.87505E+00 0.00247 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.62493E-05 0.00267  2.62403E-05 0.00268  2.74531E-05 0.02885 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.86262E-05 0.00266  2.86163E-05 0.00266  2.99531E-05 0.02892 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.74574E-03 0.02591  2.07553E-04 0.14545  1.03691E-03 0.06322  9.92976E-04 0.06177  3.10129E-03 0.03917  1.03736E-03 0.06619  3.69653E-04 0.11462 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.40373E-01 0.05949  1.24905E-02 1.5E-05  3.16131E-02 0.00097  1.10102E-01 0.00107  3.20796E-01 0.00103  1.34603E+00 0.00064  8.90318E+00 0.00561 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.74366E-03 0.02589  2.09633E-04 0.14484  1.04784E-03 0.06138  9.79217E-04 0.06068  3.13466E-03 0.03862  1.01753E-03 0.06493  3.54775E-04 0.11630 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.24632E-01 0.05909  1.24905E-02 1.5E-05  3.16092E-02 0.00096  1.10096E-01 0.00105  3.20827E-01 0.00102  1.34600E+00 0.00063  8.90003E+00 0.00561 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.57799E+02 0.02583 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.62541E-05 0.00076 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.86302E-05 0.00058 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.84576E-03 0.00514 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.60862E+02 0.00525 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.20594E-07 0.00064 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87421E-06 0.00042  2.87389E-06 0.00042  2.91869E-06 0.00505 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.11243E-05 0.00078  4.11437E-05 0.00079  3.85279E-05 0.00869 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.74754E-01 0.00032  6.74253E-01 0.00032  7.62353E-01 0.00839 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02502E+01 0.01293 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.64276E+01 0.00044  3.53073E+01 0.00046 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.22839E+04 0.00381  2.91410E+05 0.00178  6.02540E+05 0.00103  6.49157E+05 0.00059  5.98043E+05 0.00063  6.42515E+05 0.00062  4.36700E+05 0.00053  3.86513E+05 0.00080  2.95860E+05 0.00076  2.41694E+05 0.00085  2.08347E+05 0.00102  1.87280E+05 0.00095  1.72773E+05 0.00074  1.64669E+05 0.00061  1.60388E+05 0.00080  1.38574E+05 0.00089  1.36645E+05 0.00081  1.35507E+05 0.00099  1.33055E+05 0.00081  2.60041E+05 0.00069  2.51178E+05 0.00067  1.81211E+05 0.00060  1.17255E+05 0.00074  1.35326E+05 0.00082  1.28001E+05 0.00073  1.16365E+05 0.00088  1.91120E+05 0.00089  4.36101E+04 0.00185  5.48009E+04 0.00112  4.98062E+04 0.00190  2.89556E+04 0.00165  5.02831E+04 0.00137  3.42971E+04 0.00155  2.90844E+04 0.00196  5.53752E+03 0.00287  5.53298E+03 0.00253  5.63237E+03 0.00376  5.80974E+03 0.00316  5.76201E+03 0.00303  5.69682E+03 0.00321  5.91284E+03 0.00415  5.51585E+03 0.00403  1.04659E+04 0.00320  1.66467E+04 0.00184  2.11641E+04 0.00220  5.59516E+04 0.00141  5.91550E+04 0.00125  6.47464E+04 0.00130  4.44945E+04 0.00119  3.33010E+04 0.00171  2.57460E+04 0.00164  3.05047E+04 0.00172  5.84641E+04 0.00091  7.94802E+04 0.00085  1.52699E+05 0.00096  2.35066E+05 0.00090  3.50268E+05 0.00084  2.23110E+05 0.00090  1.59553E+05 0.00083  1.14581E+05 0.00109  1.02796E+05 0.00089  1.01198E+05 0.00078  8.42942E+04 0.00105  5.70748E+04 0.00118  5.25859E+04 0.00116  4.67420E+04 0.00102  3.95089E+04 0.00115  3.11244E+04 0.00152  2.07962E+04 0.00120  7.36407E+03 0.00186 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.09911E+00 0.00044 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.17362E+18 0.00043  3.41437E+17 0.00069 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37905E-01 9.4E-05  1.50584E+00 0.00029 ];
INF_CAPT                  (idx, [1:   4]) = [  6.36923E-03 0.00076  2.87819E-02 0.00029 ];
INF_ABS                   (idx, [1:   4]) = [  8.59976E-03 0.00058  6.17100E-02 0.00053 ];
INF_FISS                  (idx, [1:   4]) = [  2.23053E-03 0.00042  3.29281E-02 0.00075 ];
INF_NSF                   (idx, [1:   4]) = [  5.72341E-03 0.00042  8.05911E-02 0.00075 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.56594E+00 5.6E-05  2.44748E+00 3.8E-06 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03881E+02 6.1E-06  2.02411E+02 5.9E-07 ];
INF_INVV                  (idx, [1:   4]) = [  5.90322E-08 0.00042  2.55097E-06 0.00011 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29293E-01 9.6E-05  1.44409E+00 0.00032 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44068E-01 0.00017  3.82062E-01 0.00049 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62389E-02 0.00034  9.07226E-02 0.00102 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35557E-03 0.00287  2.72980E-02 0.00243 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03155E-02 0.00211 -8.41091E-03 0.00733 ];
INF_SCATT5                (idx, [1:   4]) = [  1.33490E-04 0.13597  6.32658E-03 0.00870 ];
INF_SCATT6                (idx, [1:   4]) = [  5.06438E-03 0.00279 -1.61814E-02 0.00253 ];
INF_SCATT7                (idx, [1:   4]) = [  7.48905E-04 0.01882  4.30790E-04 0.11290 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29332E-01 9.6E-05  1.44409E+00 0.00032 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44069E-01 0.00017  3.82062E-01 0.00049 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62393E-02 0.00034  9.07226E-02 0.00102 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35559E-03 0.00286  2.72980E-02 0.00243 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03153E-02 0.00211 -8.41091E-03 0.00733 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.33527E-04 0.13592  6.32658E-03 0.00870 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.06455E-03 0.00279 -1.61814E-02 0.00253 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.48811E-04 0.01884  4.30790E-04 0.11290 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13613E-01 0.00033  9.70544E-01 0.00022 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56046E+00 0.00033  3.43450E-01 0.00022 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.56080E-03 0.00060  6.17100E-02 0.00053 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69773E-02 0.00015  6.28638E-02 0.00066 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10928E-01 9.5E-05  1.83651E-02 0.00029  1.10924E-03 0.00336  1.44298E+00 0.00032 ];
INF_S1                    (idx, [1:   8]) = [  2.38693E-01 0.00018  5.37409E-03 0.00082  4.70785E-04 0.00418  3.81591E-01 0.00049 ];
INF_S2                    (idx, [1:   8]) = [  9.78195E-02 0.00033 -1.58053E-03 0.00184  2.56443E-04 0.00978  9.04662E-02 0.00102 ];
INF_S3                    (idx, [1:   8]) = [  9.24091E-03 0.00212 -1.88534E-03 0.00189  9.08459E-05 0.02086  2.72071E-02 0.00244 ];
INF_S4                    (idx, [1:   8]) = [ -9.68263E-03 0.00221 -6.32827E-04 0.00399 -1.97092E-07 1.00000 -8.41072E-03 0.00736 ];
INF_S5                    (idx, [1:   8]) = [  1.20686E-04 0.14538  1.28042E-05 0.21984 -3.94059E-05 0.04077  6.36599E-03 0.00863 ];
INF_S6                    (idx, [1:   8]) = [  5.20751E-03 0.00284 -1.43128E-04 0.01633 -4.67912E-05 0.02115 -1.61347E-02 0.00253 ];
INF_S7                    (idx, [1:   8]) = [  9.21466E-04 0.01527 -1.72562E-04 0.01295 -4.30579E-05 0.02254  4.73848E-04 0.10204 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10967E-01 9.5E-05  1.83651E-02 0.00029  1.10924E-03 0.00336  1.44298E+00 0.00032 ];
INF_SP1                   (idx, [1:   8]) = [  2.38695E-01 0.00017  5.37409E-03 0.00082  4.70785E-04 0.00418  3.81591E-01 0.00049 ];
INF_SP2                   (idx, [1:   8]) = [  9.78198E-02 0.00033 -1.58053E-03 0.00184  2.56443E-04 0.00978  9.04662E-02 0.00102 ];
INF_SP3                   (idx, [1:   8]) = [  9.24093E-03 0.00212 -1.88534E-03 0.00189  9.08459E-05 0.02086  2.72071E-02 0.00244 ];
INF_SP4                   (idx, [1:   8]) = [ -9.68247E-03 0.00221 -6.32827E-04 0.00399 -1.97092E-07 1.00000 -8.41072E-03 0.00736 ];
INF_SP5                   (idx, [1:   8]) = [  1.20723E-04 0.14533  1.28042E-05 0.21984 -3.94059E-05 0.04077  6.36599E-03 0.00863 ];
INF_SP6                   (idx, [1:   8]) = [  5.20768E-03 0.00284 -1.43128E-04 0.01633 -4.67912E-05 0.02115 -1.61347E-02 0.00253 ];
INF_SP7                   (idx, [1:   8]) = [  9.21373E-04 0.01529 -1.72562E-04 0.01295 -4.30579E-05 0.02254  4.73848E-04 0.10204 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32565E-01 0.00064  1.09209E+00 0.00634 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34349E-01 0.00100  1.16342E+00 0.00794 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33989E-01 0.00075  1.15743E+00 0.00760 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29430E-01 0.00097  9.77918E-01 0.00666 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43330E+00 0.00064  3.05519E-01 0.00632 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42242E+00 0.00100  2.86935E-01 0.00775 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42459E+00 0.00075  2.88396E-01 0.00763 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45291E+00 0.00097  3.41225E-01 0.00670 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.00442E-03 0.00716  1.98371E-04 0.04653  1.13618E-03 0.01848  1.06497E-03 0.01912  3.26010E-03 0.01104  1.01533E-03 0.02067  3.29473E-04 0.03811 ];
LAMBDA                    (idx, [1:  14]) = [  7.84177E-01 0.01933  1.24907E-02 3.8E-06  3.16193E-02 0.00036  1.10217E-01 0.00043  3.20799E-01 0.00031  1.34543E+00 0.00024  8.90085E+00 0.00209 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0004' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:47:07 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:09:42 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292427 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.01057E+00  9.97816E-01  1.00534E+00  1.00159E+00  9.84686E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.0E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13581E-02 0.00105  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88642E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.98136E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.98563E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.70888E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.62810E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.62730E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.14930E+00 0.00036  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.32831E-01 0.00105  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000373 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00007E+04 0.00071 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00007E+04 0.00071 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.10862E+02 ;
RUNNING_TIME              (idx, 1)        =  2.25779E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.83567E-01  4.83567E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  4.71000E-02  8.10000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  2.18733E+01  3.43203E+00  2.68567E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.72100E-01  2.75333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.01333E-02  8.16667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  2.25777E+01  1.28025E+02 ];
CPU_USAGE                 (idx, 1)        = 4.91023 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99917E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64586E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.95027E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.85297E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.48956E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.62921E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.13765E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.32104E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.73918E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  3.54675E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.48914E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.01798E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.73603E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  2.52878E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  2.81553E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.26394E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.04605E+14 ;
I131_ACTIVITY             (idx, 1)        =  3.60057E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.09815E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.66357E+10 ;
CS137_ACTIVITY            (idx, 1)        =  1.34862E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.86864E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.61964E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.50351E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.15892E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.15895E+12 0.00048  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 3 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E+00  1.00009E+00 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.12004E-01 0.00105 ];
U235_FISS                 (idx, [1:   4]) = [  1.22935E+16 0.00057  8.88344E-01 0.00024 ];
U238_FISS                 (idx, [1:   4]) = [  8.71809E+14 0.00278  6.29888E-02 0.00263 ];
PU239_FISS                (idx, [1:   4]) = [  6.69053E+14 0.00296  4.83467E-02 0.00292 ];
PU240_FISS                (idx, [1:   4]) = [  6.35461E+10 0.31342  4.58022E-06 0.31345 ];
PU241_FISS                (idx, [1:   4]) = [  1.43371E+12 0.06543  1.03628E-04 0.06539 ];
U235_CAPT                 (idx, [1:   4]) = [  2.66583E+15 0.00150  1.51972E-01 0.00139 ];
U238_CAPT                 (idx, [1:   4]) = [  8.10341E+15 0.00098  4.61927E-01 0.00066 ];
PU239_CAPT                (idx, [1:   4]) = [  3.73660E+14 0.00408  2.13017E-02 0.00405 ];
PU240_CAPT                (idx, [1:   4]) = [  3.35702E+13 0.01328  1.91388E-03 0.01327 ];
PU241_CAPT                (idx, [1:   4]) = [  3.54216E+11 0.13844  2.01388E-05 0.13828 ];
XE135_CAPT                (idx, [1:   4]) = [  7.20453E+14 0.00284  4.10730E-02 0.00283 ];
SM149_CAPT                (idx, [1:   4]) = [  1.49958E+14 0.00628  8.54917E-03 0.00626 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000373 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.25807E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000373 5.00726E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2772665 2.77655E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2187514 2.19051E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 40194 4.02070E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000373 5.00726E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.40167E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.43663E+16 1.2E-05  3.43663E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38484E+16 1.6E-06  1.38484E+16 1.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.75524E+16 0.00047  1.23274E+16 0.00046  5.22502E+15 0.00106 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.14009E+16 0.00026  2.61758E+16 0.00022  5.22502E+15 0.00106 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.15895E+16 0.00048  3.15895E+16 0.00048  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.52088E+18 0.00045  4.10029E+17 0.00042  1.11085E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.54075E+14 0.00529 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.16549E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.14739E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12398E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12398E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.67372E+00 0.00041 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.78353E-01 0.00030 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.76605E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24349E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94774E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97169E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.09600E+00 0.00050 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.08718E+00 0.00050 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.48160E+00 1.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02841E+02 1.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.08726E+00 0.00052  1.07985E+00 0.00050  7.33721E-03 0.00756 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.08729E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.08803E+00 0.00047 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.08729E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.09609E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74938E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74905E+01 7.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.06741E-07 0.00334 ];
IMP_EALF                  (idx, [1:   2]) = [  5.07230E-07 0.00137 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.13349E-01 0.00295 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.14062E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.36509E-03 0.00511  1.71447E-04 0.03386  1.00790E-03 0.01326  1.00685E-03 0.01364  2.90811E-03 0.00768  9.46059E-04 0.01331  3.24724E-04 0.02334 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.25092E-01 0.01256  1.03672E-02 0.02026  3.15821E-02 0.00025  1.10206E-01 0.00030  3.20780E-01 0.00022  1.34534E+00 0.00016  8.66198E+00 0.00779 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.94615E-03 0.00737  1.84550E-04 0.04847  1.12996E-03 0.01941  1.10114E-03 0.01864  3.16359E-03 0.01140  1.00881E-03 0.02112  3.58088E-04 0.03418 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.20291E-01 0.01813  1.24906E-02 5.1E-06  3.15733E-02 0.00037  1.10195E-01 0.00043  3.20740E-01 0.00032  1.34526E+00 0.00023  8.91118E+00 0.00211 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.60583E-05 0.00115  2.60480E-05 0.00115  2.76469E-05 0.01158 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.83282E-05 0.00102  2.83170E-05 0.00102  3.00566E-05 0.01158 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.75195E-03 0.00765  1.95591E-04 0.04957  1.06721E-03 0.01985  1.05838E-03 0.02068  3.09845E-03 0.01191  9.91014E-04 0.02159  3.41303E-04 0.03742 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.14101E-01 0.01949  1.24906E-02 6.6E-06  3.15749E-02 0.00042  1.10224E-01 0.00049  3.20824E-01 0.00036  1.34481E+00 0.00029  8.90744E+00 0.00260 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.59394E-05 0.00267  2.59336E-05 0.00268  2.64609E-05 0.02802 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.82000E-05 0.00264  2.81936E-05 0.00265  2.87644E-05 0.02797 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.60551E-03 0.02780  1.90090E-04 0.16266  1.09258E-03 0.06945  9.71973E-04 0.07120  3.12616E-03 0.04134  9.07195E-04 0.06917  3.17511E-04 0.13623 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.74764E-01 0.06707  1.24907E-02 1.6E-05  3.16059E-02 0.00098  1.10402E-01 0.00127  3.20820E-01 0.00100  1.34512E+00 0.00069  8.91949E+00 0.00613 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.66087E-03 0.02668  1.92359E-04 0.16506  1.09373E-03 0.06731  9.70468E-04 0.06912  3.16841E-03 0.03950  9.21240E-04 0.06810  3.14658E-04 0.13176 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.71246E-01 0.06556  1.24907E-02 1.6E-05  3.16028E-02 0.00099  1.10407E-01 0.00126  3.20728E-01 0.00098  1.34502E+00 0.00069  8.92708E+00 0.00615 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.55798E+02 0.02782 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.59838E-05 0.00071 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.82473E-05 0.00048 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.69454E-03 0.00498 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.57744E+02 0.00510 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.15050E-07 0.00064 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87331E-06 0.00041  2.87297E-06 0.00042  2.92082E-06 0.00497 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.06612E-05 0.00081  4.06806E-05 0.00082  3.79867E-05 0.00860 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.74411E-01 0.00033  6.73900E-01 0.00033  7.64607E-01 0.00791 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.01461E+01 0.01268 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.62730E+01 0.00046  3.51323E+01 0.00044 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.27855E+04 0.00292  2.91648E+05 0.00081  6.03911E+05 0.00113  6.50002E+05 0.00074  5.98668E+05 0.00088  6.43283E+05 0.00054  4.36314E+05 0.00071  3.86103E+05 0.00075  2.95439E+05 0.00058  2.41808E+05 0.00070  2.08484E+05 0.00084  1.87895E+05 0.00102  1.73286E+05 0.00076  1.64772E+05 0.00092  1.60663E+05 0.00096  1.38514E+05 0.00080  1.36898E+05 0.00059  1.35796E+05 0.00103  1.33176E+05 0.00097  2.60356E+05 0.00055  2.51127E+05 0.00054  1.81151E+05 0.00093  1.17331E+05 0.00081  1.35646E+05 0.00096  1.28119E+05 0.00111  1.16465E+05 0.00104  1.90987E+05 0.00072  4.35841E+04 0.00173  5.49047E+04 0.00134  4.98755E+04 0.00171  2.89167E+04 0.00154  5.02935E+04 0.00141  3.41284E+04 0.00192  2.90489E+04 0.00177  5.54266E+03 0.00427  5.48806E+03 0.00409  5.59124E+03 0.00266  5.71666E+03 0.00384  5.72873E+03 0.00399  5.65888E+03 0.00324  5.88090E+03 0.00297  5.53199E+03 0.00319  1.04250E+04 0.00251  1.66492E+04 0.00246  2.11863E+04 0.00128  5.61559E+04 0.00138  5.90386E+04 0.00112  6.45522E+04 0.00150  4.39550E+04 0.00133  3.27133E+04 0.00161  2.51846E+04 0.00169  2.98854E+04 0.00155  5.72530E+04 0.00117  7.80505E+04 0.00094  1.50570E+05 0.00096  2.32283E+05 0.00117  3.45918E+05 0.00120  2.20359E+05 0.00119  1.57730E+05 0.00126  1.13205E+05 0.00112  1.01600E+05 0.00136  1.00141E+05 0.00158  8.34434E+04 0.00135  5.63458E+04 0.00143  5.20327E+04 0.00146  4.62658E+04 0.00164  3.91538E+04 0.00134  3.07790E+04 0.00152  2.05610E+04 0.00195  7.24703E+03 0.00253 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.09685E+00 0.00055 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.18160E+18 0.00051  3.39313E+17 0.00097 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37908E-01 0.00012  1.50840E+00 0.00038 ];
INF_CAPT                  (idx, [1:   4]) = [  6.41030E-03 0.00071  2.94086E-02 0.00038 ];
INF_ABS                   (idx, [1:   4]) = [  8.62546E-03 0.00060  6.25130E-02 0.00068 ];
INF_FISS                  (idx, [1:   4]) = [  2.21516E-03 0.00054  3.31044E-02 0.00096 ];
INF_NSF                   (idx, [1:   4]) = [  5.69429E-03 0.00053  8.14654E-02 0.00097 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.57060E+00 5.1E-05  2.46086E+00 1.0E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03941E+02 4.8E-06  2.02585E+02 1.7E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.89790E-08 0.00038  2.55158E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29293E-01 0.00012  1.44592E+00 0.00042 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44040E-01 0.00019  3.82639E-01 0.00052 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62377E-02 0.00030  9.09115E-02 0.00089 ];
INF_SCATT3                (idx, [1:   4]) = [  7.40072E-03 0.00287  2.74081E-02 0.00197 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02810E-02 0.00193 -8.32172E-03 0.00701 ];
INF_SCATT5                (idx, [1:   4]) = [  1.66133E-04 0.10287  6.30589E-03 0.00775 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08951E-03 0.00327 -1.62668E-02 0.00326 ];
INF_SCATT7                (idx, [1:   4]) = [  7.60485E-04 0.02341  3.24775E-04 0.18018 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29331E-01 0.00012  1.44592E+00 0.00042 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44041E-01 0.00019  3.82639E-01 0.00052 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62377E-02 0.00030  9.09115E-02 0.00089 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.40065E-03 0.00286  2.74081E-02 0.00197 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02809E-02 0.00193 -8.32172E-03 0.00701 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.66130E-04 0.10286  6.30589E-03 0.00775 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08941E-03 0.00327 -1.62668E-02 0.00326 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.60343E-04 0.02345  3.24775E-04 0.18018 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13625E-01 0.00029  9.72934E-01 0.00032 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56037E+00 0.00029  3.42607E-01 0.00032 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.58665E-03 0.00062  6.25130E-02 0.00068 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69571E-02 0.00023  6.36112E-02 0.00082 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10951E-01 0.00011  1.83419E-02 0.00042  1.12588E-03 0.00438  1.44479E+00 0.00042 ];
INF_S1                    (idx, [1:   8]) = [  2.38673E-01 0.00019  5.36712E-03 0.00079  4.77321E-04 0.00748  3.82162E-01 0.00052 ];
INF_S2                    (idx, [1:   8]) = [  9.78172E-02 0.00031 -1.57953E-03 0.00240  2.61198E-04 0.00865  9.06503E-02 0.00089 ];
INF_S3                    (idx, [1:   8]) = [  9.28670E-03 0.00233 -1.88599E-03 0.00141  9.25590E-05 0.02419  2.73156E-02 0.00199 ];
INF_S4                    (idx, [1:   8]) = [ -9.65185E-03 0.00211 -6.29136E-04 0.00435  2.25896E-06 0.87658 -8.32398E-03 0.00698 ];
INF_S5                    (idx, [1:   8]) = [  1.55034E-04 0.11691  1.10994E-05 0.19496 -3.68451E-05 0.05701  6.34273E-03 0.00766 ];
INF_S6                    (idx, [1:   8]) = [  5.23754E-03 0.00314 -1.48032E-04 0.01573 -4.52691E-05 0.03312 -1.62215E-02 0.00328 ];
INF_S7                    (idx, [1:   8]) = [  9.34228E-04 0.01889 -1.73743E-04 0.01487 -3.91829E-05 0.03368  3.63958E-04 0.16075 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10989E-01 0.00011  1.83419E-02 0.00042  1.12588E-03 0.00438  1.44479E+00 0.00042 ];
INF_SP1                   (idx, [1:   8]) = [  2.38674E-01 0.00019  5.36712E-03 0.00079  4.77321E-04 0.00748  3.82162E-01 0.00052 ];
INF_SP2                   (idx, [1:   8]) = [  9.78172E-02 0.00031 -1.57953E-03 0.00240  2.61198E-04 0.00865  9.06503E-02 0.00089 ];
INF_SP3                   (idx, [1:   8]) = [  9.28663E-03 0.00232 -1.88599E-03 0.00141  9.25590E-05 0.02419  2.73156E-02 0.00199 ];
INF_SP4                   (idx, [1:   8]) = [ -9.65177E-03 0.00211 -6.29136E-04 0.00435  2.25896E-06 0.87658 -8.32398E-03 0.00698 ];
INF_SP5                   (idx, [1:   8]) = [  1.55030E-04 0.11690  1.10994E-05 0.19496 -3.68451E-05 0.05701  6.34273E-03 0.00766 ];
INF_SP6                   (idx, [1:   8]) = [  5.23744E-03 0.00313 -1.48032E-04 0.01573 -4.52691E-05 0.03312 -1.62215E-02 0.00328 ];
INF_SP7                   (idx, [1:   8]) = [  9.34086E-04 0.01892 -1.73743E-04 0.01487 -3.91829E-05 0.03368  3.63958E-04 0.16075 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32523E-01 0.00066  1.07734E+00 0.00795 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34362E-01 0.00092  1.14075E+00 0.00952 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33818E-01 0.00088  1.14771E+00 0.01010 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29458E-01 0.00099  9.65636E-01 0.00733 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43357E+00 0.00066  3.09876E-01 0.00799 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42233E+00 0.00092  2.92848E-01 0.00961 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42564E+00 0.00088  2.91142E-01 0.01005 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45273E+00 0.00099  3.45638E-01 0.00728 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.94615E-03 0.00737  1.84550E-04 0.04847  1.12996E-03 0.01941  1.10114E-03 0.01864  3.16359E-03 0.01140  1.00881E-03 0.02112  3.58088E-04 0.03418 ];
LAMBDA                    (idx, [1:  14]) = [  8.20291E-01 0.01813  1.24906E-02 5.1E-06  3.15733E-02 0.00037  1.10195E-01 0.00043  3.20740E-01 0.00032  1.34526E+00 0.00023  8.91118E+00 0.00211 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0004' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:47:07 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:15:51 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292427 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00982E+00  9.97780E-01  1.00562E+00  1.00280E+00  9.83986E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.6E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13629E-02 0.00108  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88637E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.99382E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.99810E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.70174E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.59724E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.59643E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.00109E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.27845E-01 0.00112  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001000 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00020E+04 0.00080 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00020E+04 0.00080 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.41586E+02 ;
RUNNING_TIME              (idx, 1)        =  2.87286E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.83567E-01  4.83567E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  6.38333E-02  7.86667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  2.79505E+01  3.41132E+00  2.66590E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  2.28250E-01  2.72500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.35667E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  2.87284E+01  1.27419E+02 ];
CPU_USAGE                 (idx, 1)        = 4.92840 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00039E+00 0.00016 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.69082E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.11194E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.85359E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.71792E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.63841E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.14388E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.47350E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.73918E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  4.74380E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.85043E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.28792E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.78950E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  3.45589E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.17148E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.48248E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.13568E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.08235E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.20514E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.56838E+11 ;
CS137_ACTIVITY            (idx, 1)        =  2.69944E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.98743E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.58250E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.28615E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.18536E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.21173E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 4 ;
BURNUP                     (idx, [1:  2])  = [  2.00000E+00  2.00016E+00 ];
BURN_DAYS                 (idx, 1)        =  5.00000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.16205E-01 0.00110 ];
U235_FISS                 (idx, [1:   4]) = [  1.16229E+16 0.00066  8.40843E-01 0.00032 ];
U238_FISS                 (idx, [1:   4]) = [  8.88049E+14 0.00282  6.42362E-02 0.00266 ];
PU239_FISS                (idx, [1:   4]) = [  1.29815E+15 0.00218  9.39142E-02 0.00212 ];
PU240_FISS                (idx, [1:   4]) = [  3.98027E+11 0.12539  2.88807E-05 0.12535 ];
PU241_FISS                (idx, [1:   4]) = [  9.94098E+12 0.02625  7.19609E-04 0.02629 ];
U235_CAPT                 (idx, [1:   4]) = [  2.52659E+15 0.00157  1.39769E-01 0.00152 ];
U238_CAPT                 (idx, [1:   4]) = [  8.17531E+15 0.00100  4.52206E-01 0.00066 ];
PU239_CAPT                (idx, [1:   4]) = [  7.27087E+14 0.00300  4.02206E-02 0.00295 ];
PU240_CAPT                (idx, [1:   4]) = [  1.25763E+14 0.00697  6.95758E-03 0.00698 ];
PU241_CAPT                (idx, [1:   4]) = [  3.30240E+12 0.04411  1.82545E-04 0.04409 ];
XE135_CAPT                (idx, [1:   4]) = [  7.25253E+14 0.00297  4.01206E-02 0.00295 ];
SM149_CAPT                (idx, [1:   4]) = [  1.57766E+14 0.00652  8.72562E-03 0.00644 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001000 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.33037E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001000 5.00733E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2810742 2.81439E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2149371 2.15205E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 40887 4.08961E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001000 5.00733E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.58794E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.46063E+16 1.3E-05  3.46063E+16 1.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38301E+16 1.9E-06  1.38301E+16 1.9E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.80632E+16 0.00047  1.28412E+16 0.00045  5.22201E+15 0.00112 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.18933E+16 0.00027  2.66713E+16 0.00022  5.22201E+15 0.00112 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.21173E+16 0.00049  3.21173E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.53895E+18 0.00047  4.14642E+17 0.00046  1.12431E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.62730E+14 0.00550 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.21561E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.15663E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12281E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12281E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.66192E+00 0.00044 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.80808E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.73140E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24314E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94697E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97108E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.08581E+00 0.00054 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07693E+00 0.00054 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.50224E+00 1.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03110E+02 1.9E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07700E+00 0.00057  1.06995E+00 0.00054  6.97836E-03 0.00844 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07786E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07763E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07786E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.08675E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74477E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74443E+01 8.4E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.30653E-07 0.00334 ];
IMP_EALF                  (idx, [1:   2]) = [  5.31241E-07 0.00146 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.18178E-01 0.00281 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.19052E-01 0.00110 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.14614E-03 0.00582  1.80923E-04 0.03203  9.92223E-04 0.01370  9.68998E-04 0.01420  2.79595E-03 0.00852  9.16606E-04 0.01449  2.91445E-04 0.02571 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.94142E-01 0.01337  1.09415E-02 0.01684  3.15355E-02 0.00029  1.10190E-01 0.00033  3.20999E-01 0.00023  1.34388E+00 0.00028  8.53465E+00 0.01002 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.58802E-03 0.00869  1.91412E-04 0.04665  1.08948E-03 0.02046  1.03329E-03 0.02055  2.96452E-03 0.01285  9.83463E-04 0.02280  3.25857E-04 0.03852 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.05958E-01 0.02033  1.24903E-02 7.9E-06  3.15324E-02 0.00044  1.10200E-01 0.00046  3.21049E-01 0.00035  1.34357E+00 0.00042  8.93577E+00 0.00233 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.56680E-05 0.00119  2.56556E-05 0.00119  2.75307E-05 0.01166 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.76401E-05 0.00104  2.76267E-05 0.00104  2.96492E-05 0.01168 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.46461E-03 0.00845  1.97744E-04 0.04726  1.05235E-03 0.02094  9.95698E-04 0.02124  2.92663E-03 0.01264  9.80828E-04 0.02198  3.11358E-04 0.04139 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.96971E-01 0.02096  1.24902E-02 1.0E-05  3.15223E-02 0.00050  1.10213E-01 0.00056  3.21000E-01 0.00037  1.34291E+00 0.00083  8.90554E+00 0.00261 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.54355E-05 0.00265  2.54243E-05 0.00268  2.71710E-05 0.02859 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.73904E-05 0.00261  2.73784E-05 0.00264  2.92454E-05 0.02853 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.28436E-03 0.02810  1.99607E-04 0.16663  1.15049E-03 0.06461  9.48137E-04 0.07356  2.86012E-03 0.04286  8.30128E-04 0.07414  2.95882E-04 0.13423 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.62410E-01 0.06485  1.24900E-02 2.8E-05  3.15581E-02 0.00105  1.10359E-01 0.00139  3.20988E-01 0.00109  1.34366E+00 0.00073  8.90984E+00 0.00627 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.24356E-03 0.02702  1.96087E-04 0.16592  1.14275E-03 0.06231  9.49745E-04 0.07027  2.82045E-03 0.04136  8.30968E-04 0.07141  3.03568E-04 0.13165 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.62521E-01 0.06483  1.24900E-02 2.8E-05  3.15570E-02 0.00105  1.10358E-01 0.00138  3.20942E-01 0.00107  1.34371E+00 0.00072  8.90937E+00 0.00627 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.47909E+02 0.02809 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.55114E-05 0.00076 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.74713E-05 0.00050 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.40645E-03 0.00563 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.51191E+02 0.00567 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.05097E-07 0.00070 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.85949E-06 0.00043  2.85929E-06 0.00043  2.88652E-06 0.00531 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.99637E-05 0.00088  3.99834E-05 0.00087  3.70806E-05 0.00921 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.70902E-01 0.00031  6.70438E-01 0.00032  7.58337E-01 0.00893 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05315E+01 0.01373 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.59643E+01 0.00048  3.47938E+01 0.00045 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.36252E+04 0.00318  2.94322E+05 0.00130  6.05534E+05 0.00076  6.50828E+05 0.00063  5.99107E+05 0.00062  6.44132E+05 0.00064  4.36792E+05 0.00077  3.86390E+05 0.00067  2.95611E+05 0.00070  2.41400E+05 0.00077  2.08480E+05 0.00075  1.87492E+05 0.00107  1.73324E+05 0.00077  1.64735E+05 0.00096  1.60321E+05 0.00089  1.38470E+05 0.00082  1.36937E+05 0.00110  1.35732E+05 0.00101  1.33256E+05 0.00062  2.60134E+05 0.00078  2.51010E+05 0.00067  1.81331E+05 0.00066  1.17220E+05 0.00119  1.35430E+05 0.00080  1.27939E+05 0.00077  1.16126E+05 0.00096  1.90749E+05 0.00089  4.35325E+04 0.00173  5.46404E+04 0.00130  4.96659E+04 0.00143  2.88693E+04 0.00168  5.01390E+04 0.00112  3.40887E+04 0.00168  2.90064E+04 0.00250  5.50044E+03 0.00393  5.40270E+03 0.00324  5.45795E+03 0.00390  5.53654E+03 0.00386  5.50999E+03 0.00368  5.51510E+03 0.00459  5.75871E+03 0.00277  5.46984E+03 0.00381  1.02999E+04 0.00277  1.65417E+04 0.00221  2.11575E+04 0.00211  5.57844E+04 0.00134  5.85727E+04 0.00092  6.37330E+04 0.00095  4.30824E+04 0.00151  3.18012E+04 0.00166  2.43201E+04 0.00204  2.86776E+04 0.00182  5.49508E+04 0.00153  7.58308E+04 0.00103  1.46760E+05 0.00117  2.26633E+05 0.00098  3.37918E+05 0.00113  2.15379E+05 0.00116  1.54320E+05 0.00117  1.10836E+05 0.00132  9.94115E+04 0.00108  9.79884E+04 0.00154  8.16271E+04 0.00133  5.53185E+04 0.00147  5.10324E+04 0.00168  4.52908E+04 0.00150  3.82734E+04 0.00136  3.01069E+04 0.00156  2.01939E+04 0.00161  7.13851E+03 0.00212 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.08651E+00 0.00049 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.20188E+18 0.00048  3.37101E+17 0.00088 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37616E-01 9.3E-05  1.51438E+00 0.00030 ];
INF_CAPT                  (idx, [1:   4]) = [  6.51466E-03 0.00059  3.03596E-02 0.00036 ];
INF_ABS                   (idx, [1:   4]) = [  8.69722E-03 0.00046  6.36100E-02 0.00062 ];
INF_FISS                  (idx, [1:   4]) = [  2.18256E-03 0.00046  3.32504E-02 0.00087 ];
INF_NSF                   (idx, [1:   4]) = [  5.63098E-03 0.00046  8.25953E-02 0.00088 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.57999E+00 4.3E-05  2.48404E+00 1.7E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04056E+02 4.5E-06  2.02889E+02 2.7E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.87109E-08 0.00036  2.55327E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28913E-01 0.00010  1.45077E+00 0.00033 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43854E-01 0.00016  3.84179E-01 0.00042 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61959E-02 0.00027  9.12819E-02 0.00100 ];
INF_SCATT3                (idx, [1:   4]) = [  7.39433E-03 0.00270  2.74564E-02 0.00233 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02605E-02 0.00173 -8.36976E-03 0.00576 ];
INF_SCATT5                (idx, [1:   4]) = [  1.36676E-04 0.12326  6.42889E-03 0.00731 ];
INF_SCATT6                (idx, [1:   4]) = [  5.06657E-03 0.00260 -1.63658E-02 0.00231 ];
INF_SCATT7                (idx, [1:   4]) = [  7.30638E-04 0.01371  2.90202E-04 0.14429 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28952E-01 0.00010  1.45077E+00 0.00033 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43855E-01 0.00016  3.84179E-01 0.00042 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61960E-02 0.00027  9.12819E-02 0.00100 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.39430E-03 0.00271  2.74564E-02 0.00233 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02604E-02 0.00173 -8.36976E-03 0.00576 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.36537E-04 0.12328  6.42889E-03 0.00731 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.06663E-03 0.00260 -1.63658E-02 0.00231 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.30689E-04 0.01367  2.90202E-04 0.14429 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13336E-01 0.00026  9.77573E-01 0.00030 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56248E+00 0.00026  3.40981E-01 0.00030 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.65804E-03 0.00042  6.36100E-02 0.00062 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69404E-02 0.00019  6.47515E-02 0.00074 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10675E-01 9.5E-05  1.82381E-02 0.00038  1.14201E-03 0.00486  1.44963E+00 0.00033 ];
INF_S1                    (idx, [1:   8]) = [  2.38532E-01 0.00016  5.32139E-03 0.00097  4.87708E-04 0.00583  3.83692E-01 0.00042 ];
INF_S2                    (idx, [1:   8]) = [  9.77734E-02 0.00027 -1.57745E-03 0.00240  2.64512E-04 0.00885  9.10174E-02 0.00098 ];
INF_S3                    (idx, [1:   8]) = [  9.26985E-03 0.00214 -1.87552E-03 0.00150  9.52959E-05 0.02022  2.73611E-02 0.00235 ];
INF_S4                    (idx, [1:   8]) = [ -9.63462E-03 0.00184 -6.25904E-04 0.00400  1.29400E-06 1.00000 -8.37105E-03 0.00571 ];
INF_S5                    (idx, [1:   8]) = [  1.15647E-04 0.14674  2.10291E-05 0.10764 -3.82939E-05 0.03742  6.46718E-03 0.00722 ];
INF_S6                    (idx, [1:   8]) = [  5.20854E-03 0.00269 -1.41969E-04 0.01459 -5.00202E-05 0.03056 -1.63158E-02 0.00227 ];
INF_S7                    (idx, [1:   8]) = [  9.03343E-04 0.01114 -1.72705E-04 0.01488 -4.50207E-05 0.02462  3.35223E-04 0.12545 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10714E-01 9.5E-05  1.82381E-02 0.00038  1.14201E-03 0.00486  1.44963E+00 0.00033 ];
INF_SP1                   (idx, [1:   8]) = [  2.38533E-01 0.00016  5.32139E-03 0.00097  4.87708E-04 0.00583  3.83692E-01 0.00042 ];
INF_SP2                   (idx, [1:   8]) = [  9.77735E-02 0.00027 -1.57745E-03 0.00240  2.64512E-04 0.00885  9.10174E-02 0.00098 ];
INF_SP3                   (idx, [1:   8]) = [  9.26982E-03 0.00214 -1.87552E-03 0.00150  9.52959E-05 0.02022  2.73611E-02 0.00235 ];
INF_SP4                   (idx, [1:   8]) = [ -9.63448E-03 0.00184 -6.25904E-04 0.00400  1.29400E-06 1.00000 -8.37105E-03 0.00571 ];
INF_SP5                   (idx, [1:   8]) = [  1.15508E-04 0.14677  2.10291E-05 0.10764 -3.82939E-05 0.03742  6.46718E-03 0.00722 ];
INF_SP6                   (idx, [1:   8]) = [  5.20860E-03 0.00269 -1.41969E-04 0.01459 -5.00202E-05 0.03056 -1.63158E-02 0.00227 ];
INF_SP7                   (idx, [1:   8]) = [  9.03394E-04 0.01109 -1.72705E-04 0.01488 -4.50207E-05 0.02462  3.35223E-04 0.12545 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32498E-01 0.00058  1.10146E+00 0.00788 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34041E-01 0.00064  1.17853E+00 0.00829 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34021E-01 0.00103  1.17328E+00 0.00809 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29500E-01 0.00106  9.78171E-01 0.00865 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43372E+00 0.00058  3.03076E-01 0.00781 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42427E+00 0.00064  2.83307E-01 0.00835 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42441E+00 0.00103  2.84546E-01 0.00801 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45247E+00 0.00106  3.41375E-01 0.00852 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.58802E-03 0.00869  1.91412E-04 0.04665  1.08948E-03 0.02046  1.03329E-03 0.02055  2.96452E-03 0.01285  9.83463E-04 0.02280  3.25857E-04 0.03852 ];
LAMBDA                    (idx, [1:  14]) = [  8.05958E-01 0.02033  1.24903E-02 7.9E-06  3.15324E-02 0.00044  1.10200E-01 0.00046  3.21049E-01 0.00035  1.34357E+00 0.00042  8.93577E+00 0.00233 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0004' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:47:07 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:21:59 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292427 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00968E+00  9.97070E-01  1.00567E+00  1.00145E+00  9.86131E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 9.3E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13613E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88639E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.00731E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.01159E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69578E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.58245E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.58163E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.88878E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.24840E-01 0.00113  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001101 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00022E+04 0.00077 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00022E+04 0.00077 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.72223E+02 ;
RUNNING_TIME              (idx, 1)        =  3.48621E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.83567E-01  4.83567E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  8.13833E-02  8.60000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.40104E+01  3.40415E+00  2.65575E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  2.83683E-01  2.76167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.69833E-02  8.16667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.48619E+01  1.26954E+02 ];
CPU_USAGE                 (idx, 1)        = 4.94012 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99852E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.71979E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.18073E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.83862E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.17787E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.65210E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.15332E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.52860E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.72326E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  5.71977E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.00884E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.64023E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.85971E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.07954E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.32287E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.66063E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.16705E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.17931E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.24897E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.43575E+11 ;
CS137_ACTIVITY            (idx, 1)        =  4.05000E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.03016E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.54724E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.37758E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.19832E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.26573E+12 0.00047  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 5 ;
BURNUP                     (idx, [1:  2])  = [  3.00000E+00  3.00025E+00 ];
BURN_DAYS                 (idx, 1)        =  7.50000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.22700E-01 0.00102 ];
U235_FISS                 (idx, [1:   4]) = [  1.10602E+16 0.00067  8.00156E-01 0.00033 ];
U238_FISS                 (idx, [1:   4]) = [  9.04416E+14 0.00285  6.54203E-02 0.00267 ];
PU239_FISS                (idx, [1:   4]) = [  1.82621E+15 0.00185  1.32117E-01 0.00175 ];
PU240_FISS                (idx, [1:   4]) = [  5.68208E+11 0.10007  4.10827E-05 0.10009 ];
PU241_FISS                (idx, [1:   4]) = [  2.76211E+13 0.01479  1.99815E-03 0.01479 ];
U235_CAPT                 (idx, [1:   4]) = [  2.41506E+15 0.00164  1.29741E-01 0.00155 ];
U238_CAPT                 (idx, [1:   4]) = [  8.24855E+15 0.00095  4.43113E-01 0.00070 ];
PU239_CAPT                (idx, [1:   4]) = [  1.01654E+15 0.00245  5.46101E-02 0.00239 ];
PU240_CAPT                (idx, [1:   4]) = [  2.46199E+14 0.00499  1.32256E-02 0.00494 ];
PU241_CAPT                (idx, [1:   4]) = [  1.01086E+13 0.02573  5.43129E-04 0.02573 ];
XE135_CAPT                (idx, [1:   4]) = [  7.26868E+14 0.00299  3.90498E-02 0.00296 ];
SM149_CAPT                (idx, [1:   4]) = [  1.62269E+14 0.00638  8.71737E-03 0.00637 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001101 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.52456E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001101 5.00752E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2846299 2.85003E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2113696 2.11639E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 41106 4.11132E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001101 5.00752E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.51926E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.48088E+16 1.4E-05  3.48088E+16 1.4E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38145E+16 2.3E-06  1.38145E+16 2.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.86101E+16 0.00045  1.33391E+16 0.00045  5.27092E+15 0.00109 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.24245E+16 0.00026  2.71536E+16 0.00022  5.27092E+15 0.00109 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.26573E+16 0.00047  3.26573E+16 0.00047  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.55958E+18 0.00046  4.19151E+17 0.00042  1.14043E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.68555E+14 0.00518 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.26931E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.17127E+18 0.00061 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12165E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12165E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.65184E+00 0.00046 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.82266E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.70270E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24168E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94657E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97104E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.07537E+00 0.00053 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06652E+00 0.00053 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.51974E+00 1.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03340E+02 2.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06663E+00 0.00055  1.05977E+00 0.00053  6.75437E-03 0.00868 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06635E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06600E+00 0.00047 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06635E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.07519E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74151E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74096E+01 8.3E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.48261E-07 0.00340 ];
IMP_EALF                  (idx, [1:   2]) = [  5.50009E-07 0.00145 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.22149E-01 0.00287 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.22798E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.04915E-03 0.00581  1.69413E-04 0.03380  9.75471E-04 0.01356  9.50029E-04 0.01479  2.73405E-03 0.00856  9.19208E-04 0.01462  3.00982E-04 0.02526 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.17701E-01 0.01316  1.05212E-02 0.01939  3.14644E-02 0.00030  1.10153E-01 0.00033  3.21063E-01 0.00024  1.34375E+00 0.00030  8.61162E+00 0.00916 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.38039E-03 0.00846  1.71663E-04 0.04798  1.03402E-03 0.02016  9.83160E-04 0.02048  2.90615E-03 0.01243  9.69781E-04 0.02213  3.15617E-04 0.03912 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.16683E-01 0.02028  1.24955E-02 0.00030  3.14447E-02 0.00048  1.10148E-01 0.00045  3.21300E-01 0.00039  1.34370E+00 0.00034  8.94948E+00 0.00268 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.55338E-05 0.00122  2.55255E-05 0.00122  2.68033E-05 0.01275 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.72309E-05 0.00107  2.72220E-05 0.00108  2.85840E-05 0.01271 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.34484E-03 0.00886  1.68049E-04 0.05023  1.04027E-03 0.02038  9.92863E-04 0.02216  2.89149E-03 0.01315  9.57424E-04 0.02210  2.94737E-04 0.04280 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.83212E-01 0.02138  1.24943E-02 0.00033  3.14464E-02 0.00054  1.10074E-01 0.00053  3.21037E-01 0.00040  1.34417E+00 0.00039  8.93835E+00 0.00354 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.52959E-05 0.00285  2.52853E-05 0.00287  2.56526E-05 0.03257 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.69768E-05 0.00277  2.69655E-05 0.00279  2.73733E-05 0.03268 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.50216E-03 0.02854  1.48790E-04 0.18183  1.00470E-03 0.07385  1.05146E-03 0.06814  2.85807E-03 0.04272  1.12513E-03 0.07541  3.14013E-04 0.13220 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.09311E-01 0.06919  1.24905E-02 2.5E-05  3.14225E-02 0.00133  1.10356E-01 0.00135  3.20947E-01 0.00112  1.34215E+00 0.00153  8.98539E+00 0.00708 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.46684E-03 0.02819  1.53213E-04 0.17538  9.73138E-04 0.07295  1.05306E-03 0.06698  2.86754E-03 0.04133  1.10544E-03 0.07425  3.14451E-04 0.13042 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.07622E-01 0.06788  1.24905E-02 2.5E-05  3.14252E-02 0.00131  1.10347E-01 0.00135  3.21001E-01 0.00111  1.34224E+00 0.00153  8.98567E+00 0.00707 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.58880E+02 0.02897 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.53847E-05 0.00073 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.70721E-05 0.00049 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.36361E-03 0.00538 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.50747E+02 0.00541 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.00086E-07 0.00065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.84504E-06 0.00042  2.84490E-06 0.00042  2.86476E-06 0.00547 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.96491E-05 0.00083  3.96725E-05 0.00083  3.61258E-05 0.00915 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.68045E-01 0.00030  6.67675E-01 0.00031  7.41610E-01 0.00917 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05143E+01 0.01458 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.58163E+01 0.00045  3.45514E+01 0.00045 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.38227E+04 0.00361  2.95038E+05 0.00123  6.05763E+05 0.00075  6.50286E+05 0.00074  5.99091E+05 0.00064  6.43100E+05 0.00065  4.36449E+05 0.00084  3.86692E+05 0.00078  2.95529E+05 0.00078  2.41633E+05 0.00088  2.08201E+05 0.00094  1.87728E+05 0.00092  1.73508E+05 0.00085  1.64602E+05 0.00083  1.60353E+05 0.00100  1.38532E+05 0.00071  1.36753E+05 0.00073  1.35531E+05 0.00108  1.33400E+05 0.00094  2.60262E+05 0.00070  2.51050E+05 0.00063  1.81015E+05 0.00084  1.17375E+05 0.00085  1.35590E+05 0.00078  1.28045E+05 0.00079  1.15859E+05 0.00099  1.90109E+05 0.00077  4.35348E+04 0.00135  5.46641E+04 0.00138  4.96709E+04 0.00124  2.88341E+04 0.00159  5.01757E+04 0.00127  3.39293E+04 0.00201  2.89469E+04 0.00158  5.45043E+03 0.00359  5.28418E+03 0.00273  5.24536E+03 0.00321  5.27809E+03 0.00398  5.29910E+03 0.00330  5.36960E+03 0.00345  5.68588E+03 0.00203  5.39339E+03 0.00312  1.02779E+04 0.00257  1.63563E+04 0.00248  2.09040E+04 0.00208  5.53430E+04 0.00178  5.83419E+04 0.00176  6.31588E+04 0.00134  4.24115E+04 0.00151  3.09777E+04 0.00174  2.35478E+04 0.00170  2.78258E+04 0.00125  5.36515E+04 0.00156  7.39752E+04 0.00125  1.43760E+05 0.00104  2.22852E+05 0.00121  3.33237E+05 0.00119  2.13053E+05 0.00119  1.52498E+05 0.00156  1.09609E+05 0.00125  9.84071E+04 0.00158  9.68370E+04 0.00156  8.09731E+04 0.00162  5.47581E+04 0.00168  5.05404E+04 0.00163  4.49266E+04 0.00190  3.79733E+04 0.00180  2.99446E+04 0.00203  2.00094E+04 0.00148  7.08940E+03 0.00214 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.07484E+00 0.00042 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.22154E+18 0.00046  3.38063E+17 0.00124 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37787E-01 8.3E-05  1.52147E+00 0.00043 ];
INF_CAPT                  (idx, [1:   4]) = [  6.62649E-03 0.00057  3.11080E-02 0.00052 ];
INF_ABS                   (idx, [1:   4]) = [  8.77442E-03 0.00047  6.42161E-02 0.00090 ];
INF_FISS                  (idx, [1:   4]) = [  2.14793E-03 0.00060  3.31080E-02 0.00127 ];
INF_NSF                   (idx, [1:   4]) = [  5.55939E-03 0.00061  8.28917E-02 0.00129 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.58826E+00 6.4E-05  2.50367E+00 3.0E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04163E+02 4.9E-06  2.03148E+02 4.9E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.84828E-08 0.00043  2.55721E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29004E-01 8.9E-05  1.45725E+00 0.00050 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43943E-01 0.00017  3.86059E-01 0.00057 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62229E-02 0.00029  9.16585E-02 0.00115 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35873E-03 0.00294  2.75683E-02 0.00223 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03268E-02 0.00240 -8.49424E-03 0.00571 ];
INF_SCATT5                (idx, [1:   4]) = [  1.37959E-04 0.15466  6.31310E-03 0.00724 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08218E-03 0.00270 -1.64575E-02 0.00269 ];
INF_SCATT7                (idx, [1:   4]) = [  7.56523E-04 0.01718  4.24109E-04 0.11533 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29044E-01 8.8E-05  1.45725E+00 0.00050 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43943E-01 0.00017  3.86059E-01 0.00057 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62229E-02 0.00029  9.16585E-02 0.00115 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35880E-03 0.00294  2.75683E-02 0.00223 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03269E-02 0.00240 -8.49424E-03 0.00571 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.37936E-04 0.15498  6.31310E-03 0.00724 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08222E-03 0.00270 -1.64575E-02 0.00269 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.56616E-04 0.01722  4.24109E-04 0.11533 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13289E-01 0.00024  9.82772E-01 0.00036 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56282E+00 0.00024  3.39178E-01 0.00036 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.73419E-03 0.00045  6.42161E-02 0.00090 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69500E-02 0.00026  6.53676E-02 0.00111 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10837E-01 8.2E-05  1.81670E-02 0.00037  1.14912E-03 0.00475  1.45610E+00 0.00050 ];
INF_S1                    (idx, [1:   8]) = [  2.38643E-01 0.00017  5.29968E-03 0.00095  4.92929E-04 0.00730  3.85566E-01 0.00057 ];
INF_S2                    (idx, [1:   8]) = [  9.78044E-02 0.00030 -1.58148E-03 0.00302  2.70457E-04 0.00783  9.13880E-02 0.00116 ];
INF_S3                    (idx, [1:   8]) = [  9.22985E-03 0.00232 -1.87112E-03 0.00154  9.52604E-05 0.01881  2.74730E-02 0.00222 ];
INF_S4                    (idx, [1:   8]) = [ -9.70637E-03 0.00242 -6.20407E-04 0.00385 -1.13537E-06 1.00000 -8.49311E-03 0.00573 ];
INF_S5                    (idx, [1:   8]) = [  1.15118E-04 0.17889  2.28411E-05 0.12698 -3.91928E-05 0.04163  6.35229E-03 0.00713 ];
INF_S6                    (idx, [1:   8]) = [  5.22041E-03 0.00258 -1.38226E-04 0.01763 -5.03524E-05 0.03009 -1.64071E-02 0.00269 ];
INF_S7                    (idx, [1:   8]) = [  9.27093E-04 0.01395 -1.70571E-04 0.01321 -4.52321E-05 0.03076  4.69341E-04 0.10366 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10877E-01 8.2E-05  1.81670E-02 0.00037  1.14912E-03 0.00475  1.45610E+00 0.00050 ];
INF_SP1                   (idx, [1:   8]) = [  2.38644E-01 0.00017  5.29968E-03 0.00095  4.92929E-04 0.00730  3.85566E-01 0.00057 ];
INF_SP2                   (idx, [1:   8]) = [  9.78044E-02 0.00029 -1.58148E-03 0.00302  2.70457E-04 0.00783  9.13880E-02 0.00116 ];
INF_SP3                   (idx, [1:   8]) = [  9.22992E-03 0.00232 -1.87112E-03 0.00154  9.52604E-05 0.01881  2.74730E-02 0.00222 ];
INF_SP4                   (idx, [1:   8]) = [ -9.70646E-03 0.00242 -6.20407E-04 0.00385 -1.13537E-06 1.00000 -8.49311E-03 0.00573 ];
INF_SP5                   (idx, [1:   8]) = [  1.15095E-04 0.17930  2.28411E-05 0.12698 -3.91928E-05 0.04163  6.35229E-03 0.00713 ];
INF_SP6                   (idx, [1:   8]) = [  5.22044E-03 0.00259 -1.38226E-04 0.01763 -5.03524E-05 0.03009 -1.64071E-02 0.00269 ];
INF_SP7                   (idx, [1:   8]) = [  9.27187E-04 0.01398 -1.70571E-04 0.01321 -4.52321E-05 0.03076  4.69341E-04 0.10366 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32395E-01 0.00055  1.10825E+00 0.00583 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34152E-01 0.00059  1.19020E+00 0.00812 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33967E-01 0.00097  1.18152E+00 0.00861 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29144E-01 0.00093  9.81347E-01 0.00582 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43435E+00 0.00055  3.01016E-01 0.00571 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42359E+00 0.00059  2.80490E-01 0.00778 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42473E+00 0.00097  2.82616E-01 0.00845 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45472E+00 0.00093  3.39942E-01 0.00575 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.38039E-03 0.00846  1.71663E-04 0.04798  1.03402E-03 0.02016  9.83160E-04 0.02048  2.90615E-03 0.01243  9.69781E-04 0.02213  3.15617E-04 0.03912 ];
LAMBDA                    (idx, [1:  14]) = [  8.16683E-01 0.02028  1.24955E-02 0.00030  3.14447E-02 0.00048  1.10148E-01 0.00045  3.21300E-01 0.00039  1.34370E+00 0.00034  8.94948E+00 0.00268 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0004' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:47:07 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:28:05 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292427 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00838E+00  9.97680E-01  1.00437E+00  1.00207E+00  9.87504E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13578E-02 0.00104  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88642E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.01616E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.02044E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68900E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.56425E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.56342E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.79448E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.21835E-01 0.00108  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000521 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00010E+04 0.00077 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00010E+04 0.00077 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.02732E+02 ;
RUNNING_TIME              (idx, 1)        =  4.09701E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.83567E-01  4.83567E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.00000E-01  9.15000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  4.00425E+01  3.38478E+00  2.64725E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  3.40417E-01  2.79333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.03167E-02  8.16667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.09699E+01  1.26728E+02 ];
CPU_USAGE                 (idx, 1)        = 4.94829 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99907E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.73992E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.22991E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.82506E+04 ;
TOT_SF_RATE               (idx, 1)        =  9.08741E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.66796E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.16432E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.56192E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.70861E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  6.64349E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.11410E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.08012E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.93988E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.56338E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.42011E+07 ;
SR90_ACTIVITY             (idx, 1)        =  4.80369E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.19486E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.22617E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.28733E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.20075E+12 ;
CS137_ACTIVITY            (idx, 1)        =  5.40010E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.06132E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.51757E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  8.12939E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.20824E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.31733E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 6 ;
BURNUP                     (idx, [1:  2])  = [  4.00000E+00  4.00034E+00 ];
BURN_DAYS                 (idx, 1)        =  1.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.30745E-01 0.00104 ];
U235_FISS                 (idx, [1:   4]) = [  1.05531E+16 0.00070  7.64263E-01 0.00039 ];
U238_FISS                 (idx, [1:   4]) = [  9.18784E+14 0.00276  6.65351E-02 0.00265 ];
PU239_FISS                (idx, [1:   4]) = [  2.27510E+15 0.00166  1.64767E-01 0.00158 ];
PU240_FISS                (idx, [1:   4]) = [  1.09966E+12 0.07843  7.95850E-05 0.07844 ];
PU241_FISS                (idx, [1:   4]) = [  5.54950E+13 0.01082  4.01899E-03 0.01080 ];
U235_CAPT                 (idx, [1:   4]) = [  2.30879E+15 0.00180  1.20642E-01 0.00170 ];
U238_CAPT                 (idx, [1:   4]) = [  8.30882E+15 0.00099  4.34149E-01 0.00069 ];
PU239_CAPT                (idx, [1:   4]) = [  1.26287E+15 0.00229  6.59879E-02 0.00219 ];
PU240_CAPT                (idx, [1:   4]) = [  3.80409E+14 0.00403  1.98762E-02 0.00394 ];
PU241_CAPT                (idx, [1:   4]) = [  2.00166E+13 0.01867  1.04592E-03 0.01864 ];
XE135_CAPT                (idx, [1:   4]) = [  7.32832E+14 0.00307  3.82968E-02 0.00308 ];
SM149_CAPT                (idx, [1:   4]) = [  1.70167E+14 0.00584  8.89233E-03 0.00583 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000521 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.47307E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000521 5.00747E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2880456 2.88451E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2078463 2.08134E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 41602 4.16231E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000521 5.00747E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -8.66130E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.49880E+16 1.7E-05  3.49880E+16 1.7E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38005E+16 2.8E-06  1.38005E+16 2.8E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.91339E+16 0.00047  1.38340E+16 0.00045  5.29989E+15 0.00113 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.29343E+16 0.00028  2.76344E+16 0.00023  5.29989E+15 0.00113 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.31733E+16 0.00052  3.31733E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.57926E+18 0.00050  4.24232E+17 0.00046  1.15503E+18 0.00056 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.76206E+14 0.00534 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.32105E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.18375E+18 0.00064 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12048E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12048E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.64025E+00 0.00047 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.83173E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.67058E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24201E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94596E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97064E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.06421E+00 0.00054 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.05535E+00 0.00054 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.53528E+00 2.0E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03546E+02 2.8E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.05528E+00 0.00055  1.04878E+00 0.00054  6.56590E-03 0.00874 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.05516E+00 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  1.05484E+00 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.05516E+00 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  1.06402E+00 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73788E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73740E+01 8.5E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.68628E-07 0.00350 ];
IMP_EALF                  (idx, [1:   2]) = [  5.69972E-07 0.00147 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.26126E-01 0.00295 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.26896E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.98160E-03 0.00558  1.72506E-04 0.03265  9.79139E-04 0.01345  9.38066E-04 0.01454  2.74310E-03 0.00813  8.81473E-04 0.01397  2.67320E-04 0.02694 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.66596E-01 0.01336  1.04944E-02 0.01954  3.14157E-02 0.00034  1.10187E-01 0.00035  3.21191E-01 0.00024  1.34246E+00 0.00035  8.47179E+00 0.01065 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.25135E-03 0.00830  1.87553E-04 0.04917  1.03270E-03 0.02080  9.70472E-04 0.02162  2.85099E-03 0.01230  9.24519E-04 0.02036  2.85117E-04 0.03741 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.80064E-01 0.01911  1.24936E-02 0.00022  3.14321E-02 0.00048  1.10160E-01 0.00046  3.21358E-01 0.00038  1.34191E+00 0.00064  8.94343E+00 0.00286 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.54064E-05 0.00125  2.53974E-05 0.00124  2.69419E-05 0.01368 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.68068E-05 0.00111  2.67973E-05 0.00111  2.84211E-05 0.01362 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.22738E-03 0.00881  1.84311E-04 0.05172  1.02962E-03 0.02163  9.72737E-04 0.02229  2.82165E-03 0.01313  9.52577E-04 0.02147  2.66486E-04 0.04164 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.54721E-01 0.01999  1.24952E-02 0.00033  3.14200E-02 0.00059  1.10202E-01 0.00060  3.21240E-01 0.00042  1.34189E+00 0.00066  8.96153E+00 0.00411 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.51240E-05 0.00283  2.51083E-05 0.00283  2.64108E-05 0.03449 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.65088E-05 0.00277  2.64922E-05 0.00277  2.78747E-05 0.03458 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.09103E-03 0.02828  1.51273E-04 0.16852  9.67901E-04 0.07144  1.03735E-03 0.06903  2.77448E-03 0.04228  8.99460E-04 0.07754  2.60571E-04 0.12746 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.96767E-01 0.06547  1.24900E-02 2.9E-05  3.13792E-02 0.00140  1.10271E-01 0.00132  3.21078E-01 0.00114  1.34173E+00 0.00112  8.87882E+00 0.00961 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.08786E-03 0.02727  1.53515E-04 0.16647  9.73588E-04 0.06789  1.02685E-03 0.06890  2.79816E-03 0.04138  8.80614E-04 0.07536  2.55136E-04 0.12333 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.84490E-01 0.06281  1.24900E-02 2.9E-05  3.13848E-02 0.00138  1.10251E-01 0.00130  3.21059E-01 0.00113  1.34182E+00 0.00105  8.87882E+00 0.00961 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.43352E+02 0.02849 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.52685E-05 0.00080 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.66612E-05 0.00057 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.09278E-03 0.00569 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.41186E+02 0.00573 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.94048E-07 0.00066 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.83085E-06 0.00044  2.83058E-06 0.00044  2.87164E-06 0.00531 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.92934E-05 0.00083  3.93174E-05 0.00083  3.56092E-05 0.00958 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.64855E-01 0.00032  6.64514E-01 0.00032  7.33461E-01 0.00899 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06620E+01 0.01347 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.56342E+01 0.00045  3.43527E+01 0.00046 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.47226E+04 0.00313  2.95972E+05 0.00161  6.07219E+05 0.00094  6.49868E+05 0.00067  5.98426E+05 0.00078  6.43248E+05 0.00076  4.36568E+05 0.00080  3.86505E+05 0.00055  2.95806E+05 0.00067  2.41505E+05 0.00074  2.08370E+05 0.00070  1.87307E+05 0.00068  1.73193E+05 0.00101  1.64790E+05 0.00086  1.60389E+05 0.00062  1.38557E+05 0.00087  1.36782E+05 0.00112  1.35537E+05 0.00105  1.33428E+05 0.00094  2.60083E+05 0.00079  2.51132E+05 0.00086  1.81065E+05 0.00053  1.17375E+05 0.00089  1.35629E+05 0.00081  1.28284E+05 0.00085  1.15960E+05 0.00100  1.90074E+05 0.00088  4.36808E+04 0.00163  5.46662E+04 0.00114  4.95295E+04 0.00199  2.88977E+04 0.00173  5.01167E+04 0.00169  3.41137E+04 0.00165  2.87760E+04 0.00199  5.38774E+03 0.00334  5.20696E+03 0.00373  5.05269E+03 0.00462  5.05500E+03 0.00345  5.07312E+03 0.00371  5.20941E+03 0.00317  5.59388E+03 0.00342  5.30491E+03 0.00320  1.01701E+04 0.00249  1.62713E+04 0.00222  2.07432E+04 0.00235  5.49545E+04 0.00129  5.78329E+04 0.00162  6.25776E+04 0.00129  4.17892E+04 0.00128  3.02862E+04 0.00208  2.30084E+04 0.00178  2.71475E+04 0.00173  5.23243E+04 0.00136  7.23104E+04 0.00121  1.41195E+05 0.00130  2.19599E+05 0.00120  3.28280E+05 0.00117  2.09855E+05 0.00131  1.50476E+05 0.00121  1.08094E+05 0.00139  9.71595E+04 0.00148  9.57306E+04 0.00142  7.99464E+04 0.00157  5.40288E+04 0.00127  4.99238E+04 0.00154  4.44071E+04 0.00121  3.75113E+04 0.00169  2.95826E+04 0.00145  1.97758E+04 0.00190  7.00127E+03 0.00235 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.06370E+00 0.00049 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.24095E+18 0.00041  3.38342E+17 0.00113 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37817E-01 9.2E-05  1.52615E+00 0.00046 ];
INF_CAPT                  (idx, [1:   4]) = [  6.75157E-03 0.00064  3.17919E-02 0.00059 ];
INF_ABS                   (idx, [1:   4]) = [  8.86989E-03 0.00051  6.48173E-02 0.00089 ];
INF_FISS                  (idx, [1:   4]) = [  2.11832E-03 0.00045  3.30254E-02 0.00118 ];
INF_NSF                   (idx, [1:   4]) = [  5.49948E-03 0.00044  8.32555E-02 0.00121 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.59615E+00 6.1E-05  2.52095E+00 4.3E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04264E+02 6.4E-06  2.03378E+02 7.1E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.82939E-08 0.00032  2.55917E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28951E-01 9.3E-05  1.46129E+00 0.00052 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43887E-01 0.00021  3.87152E-01 0.00059 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62222E-02 0.00033  9.19139E-02 0.00087 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34614E-03 0.00210  2.76199E-02 0.00224 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03061E-02 0.00164 -8.53061E-03 0.00574 ];
INF_SCATT5                (idx, [1:   4]) = [  1.37137E-04 0.13290  6.44320E-03 0.00717 ];
INF_SCATT6                (idx, [1:   4]) = [  5.05460E-03 0.00312 -1.64445E-02 0.00262 ];
INF_SCATT7                (idx, [1:   4]) = [  7.55571E-04 0.02067  3.93120E-04 0.13415 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28991E-01 9.3E-05  1.46129E+00 0.00052 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43888E-01 0.00021  3.87152E-01 0.00059 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62227E-02 0.00033  9.19139E-02 0.00087 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34641E-03 0.00209  2.76199E-02 0.00224 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03061E-02 0.00163 -8.53061E-03 0.00574 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.37321E-04 0.13275  6.44320E-03 0.00717 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.05464E-03 0.00313 -1.64445E-02 0.00262 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.55705E-04 0.02064  3.93120E-04 0.13415 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13247E-01 0.00024  9.86576E-01 0.00038 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56314E+00 0.00024  3.37870E-01 0.00038 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.82994E-03 0.00051  6.48173E-02 0.00089 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69431E-02 0.00022  6.60165E-02 0.00105 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10874E-01 8.9E-05  1.80769E-02 0.00035  1.15654E-03 0.00438  1.46013E+00 0.00052 ];
INF_S1                    (idx, [1:   8]) = [  2.38614E-01 0.00021  5.27300E-03 0.00102  4.91021E-04 0.00639  3.86661E-01 0.00059 ];
INF_S2                    (idx, [1:   8]) = [  9.77980E-02 0.00032 -1.57583E-03 0.00248  2.69699E-04 0.00886  9.16442E-02 0.00088 ];
INF_S3                    (idx, [1:   8]) = [  9.20964E-03 0.00159 -1.86351E-03 0.00166  9.37459E-05 0.01811  2.75262E-02 0.00223 ];
INF_S4                    (idx, [1:   8]) = [ -9.69226E-03 0.00173 -6.13802E-04 0.00517 -8.28658E-07 1.00000 -8.52978E-03 0.00571 ];
INF_S5                    (idx, [1:   8]) = [  1.19170E-04 0.15863  1.79671E-05 0.16913 -3.94764E-05 0.03259  6.48267E-03 0.00711 ];
INF_S6                    (idx, [1:   8]) = [  5.19930E-03 0.00290 -1.44699E-04 0.01573 -4.83183E-05 0.02984 -1.63962E-02 0.00261 ];
INF_S7                    (idx, [1:   8]) = [  9.25608E-04 0.01750 -1.70037E-04 0.01705 -4.23927E-05 0.03161  4.35513E-04 0.12007 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10914E-01 8.9E-05  1.80769E-02 0.00035  1.15654E-03 0.00438  1.46013E+00 0.00052 ];
INF_SP1                   (idx, [1:   8]) = [  2.38615E-01 0.00021  5.27300E-03 0.00102  4.91021E-04 0.00639  3.86661E-01 0.00059 ];
INF_SP2                   (idx, [1:   8]) = [  9.77985E-02 0.00032 -1.57583E-03 0.00248  2.69699E-04 0.00886  9.16442E-02 0.00088 ];
INF_SP3                   (idx, [1:   8]) = [  9.20991E-03 0.00159 -1.86351E-03 0.00166  9.37459E-05 0.01811  2.75262E-02 0.00223 ];
INF_SP4                   (idx, [1:   8]) = [ -9.69226E-03 0.00173 -6.13802E-04 0.00517 -8.28658E-07 1.00000 -8.52978E-03 0.00571 ];
INF_SP5                   (idx, [1:   8]) = [  1.19354E-04 0.15838  1.79671E-05 0.16913 -3.94764E-05 0.03259  6.48267E-03 0.00711 ];
INF_SP6                   (idx, [1:   8]) = [  5.19933E-03 0.00290 -1.44699E-04 0.01573 -4.83183E-05 0.02984 -1.63962E-02 0.00261 ];
INF_SP7                   (idx, [1:   8]) = [  9.25742E-04 0.01749 -1.70037E-04 0.01705 -4.23927E-05 0.03161  4.35513E-04 0.12007 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32076E-01 0.00063  1.12992E+00 0.00511 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33580E-01 0.00107  1.21304E+00 0.00734 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33925E-01 0.00088  1.21437E+00 0.00633 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28803E-01 0.00092  9.93682E-01 0.00534 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43632E+00 0.00063  2.95189E-01 0.00505 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42710E+00 0.00107  2.75139E-01 0.00717 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42498E+00 0.00088  2.74751E-01 0.00626 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45689E+00 0.00092  3.35678E-01 0.00524 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.25135E-03 0.00830  1.87553E-04 0.04917  1.03270E-03 0.02080  9.70472E-04 0.02162  2.85099E-03 0.01230  9.24519E-04 0.02036  2.85117E-04 0.03741 ];
LAMBDA                    (idx, [1:  14]) = [  7.80064E-01 0.01911  1.24936E-02 0.00022  3.14321E-02 0.00048  1.10160E-01 0.00046  3.21358E-01 0.00038  1.34191E+00 0.00064  8.94343E+00 0.00286 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0004' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:47:07 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:34:11 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292427 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00914E+00  9.98240E-01  1.00510E+00  1.00186E+00  9.85666E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13516E-02 0.00113  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88648E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.02814E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.03241E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68271E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.55667E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.55582E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.70968E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.19885E-01 0.00116  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000725 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00015E+04 0.00073 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00015E+04 0.00073 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.33215E+02 ;
RUNNING_TIME              (idx, 1)        =  4.70729E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.83567E-01  4.83567E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.18350E-01  8.71667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  4.60728E+01  3.37990E+00  2.65043E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  3.93917E-01  2.62833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.35667E-02  7.50001E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.70728E+01  1.26441E+02 ];
CPU_USAGE                 (idx, 1)        = 4.95434 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99967E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75480E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.27060E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.81345E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.08017E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.68578E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.17671E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.58479E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.69575E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  7.56873E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.19641E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.60664E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.02962E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.96209E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.49344E+07 ;
SR90_ACTIVITY             (idx, 1)        =  5.91530E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.22010E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.26295E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.32180E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.92436E+12 ;
CS137_ACTIVITY            (idx, 1)        =  6.74960E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.08946E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.49204E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.20494E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.21706E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.36861E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 7 ;
BURNUP                     (idx, [1:  2])  = [  5.00000E+00  5.00043E+00 ];
BURN_DAYS                 (idx, 1)        =  1.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.39872E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  1.01047E+16 0.00070  7.32957E-01 0.00045 ];
U238_FISS                 (idx, [1:   4]) = [  9.33917E+14 0.00286  6.77322E-02 0.00270 ];
PU239_FISS                (idx, [1:   4]) = [  2.64836E+15 0.00166  1.92095E-01 0.00153 ];
PU240_FISS                (idx, [1:   4]) = [  1.53609E+12 0.06714  1.11291E-04 0.06717 ];
PU241_FISS                (idx, [1:   4]) = [  9.26579E+13 0.00894  6.72047E-03 0.00892 ];
U235_CAPT                 (idx, [1:   4]) = [  2.20967E+15 0.00175  1.12356E-01 0.00162 ];
U238_CAPT                 (idx, [1:   4]) = [  8.38460E+15 0.00104  4.26323E-01 0.00072 ];
PU239_CAPT                (idx, [1:   4]) = [  1.48805E+15 0.00203  7.56673E-02 0.00197 ];
PU240_CAPT                (idx, [1:   4]) = [  5.11518E+14 0.00365  2.60122E-02 0.00365 ];
PU241_CAPT                (idx, [1:   4]) = [  3.30763E+13 0.01478  1.68197E-03 0.01477 ];
XE135_CAPT                (idx, [1:   4]) = [  7.32245E+14 0.00316  3.72377E-02 0.00317 ];
SM149_CAPT                (idx, [1:   4]) = [  1.73789E+14 0.00660  8.83717E-03 0.00658 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000725 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.83474E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000725 5.00783E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2914982 2.91907E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2043396 2.04640E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 42347 4.23654E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000725 5.00783E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.19095E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.51488E+16 1.7E-05  3.51488E+16 1.7E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37878E+16 2.8E-06  1.37878E+16 2.8E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.96687E+16 0.00046  1.43043E+16 0.00046  5.36438E+15 0.00111 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.34565E+16 0.00027  2.80921E+16 0.00023  5.36438E+15 0.00111 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.36861E+16 0.00051  3.36861E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.60030E+18 0.00047  4.28951E+17 0.00046  1.17134E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.85460E+14 0.00512 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.37420E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.19952E+18 0.00061 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11932E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11932E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.62978E+00 0.00046 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.83049E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.64064E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24156E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94503E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97007E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.05212E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04320E+00 0.00052 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.54927E+00 1.9E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03734E+02 2.8E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04326E+00 0.00053  1.03682E+00 0.00052  6.38621E-03 0.00907 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.04333E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04355E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.04333E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.05224E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73462E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73441E+01 8.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.87582E-07 0.00361 ];
IMP_EALF                  (idx, [1:   2]) = [  5.87272E-07 0.00154 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.30150E-01 0.00296 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.30982E-01 0.00122 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.98891E-03 0.00583  1.77743E-04 0.03142  9.82810E-04 0.01446  9.45164E-04 0.01429  2.71263E-03 0.00858  8.91326E-04 0.01465  2.79238E-04 0.02540 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.90101E-01 0.01365  1.04464E-02 0.01983  3.13580E-02 0.00037  1.10130E-01 0.00035  3.21459E-01 0.00026  1.33993E+00 0.00051  8.44426E+00 0.01138 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.13156E-03 0.00870  1.81199E-04 0.04726  9.92680E-04 0.02099  9.67128E-04 0.02167  2.74625E-03 0.01329  9.46476E-04 0.02201  2.97829E-04 0.03634 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.14619E-01 0.01950  1.24947E-02 0.00023  3.13664E-02 0.00049  1.10134E-01 0.00050  3.21480E-01 0.00037  1.34029E+00 0.00064  8.93888E+00 0.00345 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.54665E-05 0.00125  2.54570E-05 0.00126  2.69791E-05 0.01325 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.65642E-05 0.00111  2.65543E-05 0.00111  2.81482E-05 0.01328 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.11942E-03 0.00917  1.79800E-04 0.04873  9.86376E-04 0.02143  9.64440E-04 0.02221  2.74202E-03 0.01341  9.52096E-04 0.02258  2.94685E-04 0.03964 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.05565E-01 0.02099  1.24967E-02 0.00035  3.13337E-02 0.00066  1.10058E-01 0.00058  3.21495E-01 0.00043  1.33997E+00 0.00103  8.95033E+00 0.00328 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.50807E-05 0.00273  2.50775E-05 0.00274  2.54898E-05 0.03263 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.61624E-05 0.00268  2.61590E-05 0.00269  2.66032E-05 0.03277 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.07552E-03 0.02873  1.49795E-04 0.17274  9.11548E-04 0.07422  9.78608E-04 0.07689  2.84075E-03 0.04204  9.32152E-04 0.07425  2.62661E-04 0.14156 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.93995E-01 0.07140  1.24905E-02 2.5E-05  3.13103E-02 0.00155  1.10031E-01 0.00135  3.22491E-01 0.00128  1.34174E+00 0.00165  9.03668E+00 0.00756 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.08375E-03 0.02768  1.42064E-04 0.16837  9.29961E-04 0.07240  9.88509E-04 0.07351  2.83253E-03 0.04120  9.30613E-04 0.07175  2.60073E-04 0.13376 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.97356E-01 0.07011  1.24905E-02 2.5E-05  3.13159E-02 0.00153  1.10032E-01 0.00134  3.22418E-01 0.00125  1.34184E+00 0.00164  9.03478E+00 0.00755 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.43510E+02 0.02894 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.53029E-05 0.00072 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.63941E-05 0.00051 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.13892E-03 0.00552 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.42696E+02 0.00560 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.91347E-07 0.00070 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.81535E-06 0.00042  2.81527E-06 0.00042  2.82702E-06 0.00519 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.91960E-05 0.00086  3.92146E-05 0.00086  3.62995E-05 0.00987 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.61830E-01 0.00034  6.61538E-01 0.00034  7.22918E-01 0.00945 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05325E+01 0.01368 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.55582E+01 0.00047  3.41665E+01 0.00047 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.51440E+04 0.00290  2.96481E+05 0.00164  6.07107E+05 0.00096  6.51052E+05 0.00084  5.99049E+05 0.00091  6.42065E+05 0.00071  4.36409E+05 0.00063  3.86213E+05 0.00070  2.95306E+05 0.00076  2.41090E+05 0.00101  2.08108E+05 0.00081  1.87404E+05 0.00086  1.73068E+05 0.00106  1.64605E+05 0.00083  1.60198E+05 0.00075  1.38464E+05 0.00081  1.37085E+05 0.00102  1.35836E+05 0.00116  1.33269E+05 0.00090  2.59957E+05 0.00072  2.50967E+05 0.00068  1.81412E+05 0.00076  1.17385E+05 0.00090  1.35461E+05 0.00115  1.28399E+05 0.00091  1.15806E+05 0.00082  1.89497E+05 0.00070  4.35486E+04 0.00213  5.46692E+04 0.00135  4.96378E+04 0.00147  2.88193E+04 0.00214  5.00359E+04 0.00142  3.39382E+04 0.00154  2.85924E+04 0.00175  5.30472E+03 0.00386  5.11727E+03 0.00351  4.87200E+03 0.00286  4.79449E+03 0.00327  4.85279E+03 0.00344  5.02484E+03 0.00347  5.49076E+03 0.00354  5.22874E+03 0.00349  1.00262E+04 0.00251  1.61778E+04 0.00163  2.06241E+04 0.00248  5.46136E+04 0.00139  5.74022E+04 0.00142  6.19592E+04 0.00160  4.10946E+04 0.00119  2.97947E+04 0.00175  2.24422E+04 0.00242  2.65403E+04 0.00180  5.15148E+04 0.00156  7.13861E+04 0.00130  1.39669E+05 0.00141  2.17334E+05 0.00150  3.25950E+05 0.00151  2.08632E+05 0.00135  1.49375E+05 0.00156  1.07496E+05 0.00162  9.66271E+04 0.00182  9.52302E+04 0.00191  7.96046E+04 0.00185  5.37992E+04 0.00193  4.96540E+04 0.00180  4.42286E+04 0.00191  3.73371E+04 0.00191  2.94545E+04 0.00221  1.97400E+04 0.00244  6.98197E+03 0.00261 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.05247E+00 0.00051 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.25956E+18 0.00043  3.40775E+17 0.00134 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37886E-01 0.00012  1.53192E+00 0.00047 ];
INF_CAPT                  (idx, [1:   4]) = [  6.86695E-03 0.00066  3.23396E-02 0.00061 ];
INF_ABS                   (idx, [1:   4]) = [  8.95435E-03 0.00055  6.50906E-02 0.00097 ];
INF_FISS                  (idx, [1:   4]) = [  2.08740E-03 0.00043  3.27510E-02 0.00132 ];
INF_NSF                   (idx, [1:   4]) = [  5.43563E-03 0.00043  8.30686E-02 0.00135 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.60402E+00 6.0E-05  2.53636E+00 3.2E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04365E+02 6.1E-06  2.03585E+02 5.4E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.80651E-08 0.00036  2.56255E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28936E-01 0.00012  1.46683E+00 0.00054 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43907E-01 0.00020  3.88758E-01 0.00068 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62252E-02 0.00033  9.20854E-02 0.00098 ];
INF_SCATT3                (idx, [1:   4]) = [  7.39254E-03 0.00280  2.76367E-02 0.00245 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02737E-02 0.00158 -8.51688E-03 0.00752 ];
INF_SCATT5                (idx, [1:   4]) = [  1.59440E-04 0.11337  6.47897E-03 0.00827 ];
INF_SCATT6                (idx, [1:   4]) = [  5.05171E-03 0.00244 -1.66448E-02 0.00291 ];
INF_SCATT7                (idx, [1:   4]) = [  7.18630E-04 0.01613  3.27076E-04 0.14432 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28978E-01 0.00012  1.46683E+00 0.00054 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43908E-01 0.00020  3.88758E-01 0.00068 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62254E-02 0.00033  9.20854E-02 0.00098 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.39282E-03 0.00281  2.76367E-02 0.00245 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02738E-02 0.00158 -8.51688E-03 0.00752 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.59448E-04 0.11341  6.47897E-03 0.00827 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.05177E-03 0.00244 -1.66448E-02 0.00291 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.18623E-04 0.01612  3.27076E-04 0.14432 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13157E-01 0.00029  9.90823E-01 0.00040 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56379E+00 0.00029  3.36422E-01 0.00040 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.91244E-03 0.00055  6.50906E-02 0.00097 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69509E-02 0.00025  6.62413E-02 0.00122 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10935E-01 0.00012  1.80007E-02 0.00036  1.15173E-03 0.00456  1.46568E+00 0.00054 ];
INF_S1                    (idx, [1:   8]) = [  2.38664E-01 0.00020  5.24341E-03 0.00084  4.91947E-04 0.00731  3.88266E-01 0.00069 ];
INF_S2                    (idx, [1:   8]) = [  9.78007E-02 0.00032 -1.57546E-03 0.00218  2.66195E-04 0.00849  9.18192E-02 0.00098 ];
INF_S3                    (idx, [1:   8]) = [  9.24763E-03 0.00232 -1.85509E-03 0.00180  9.70590E-05 0.02221  2.75397E-02 0.00243 ];
INF_S4                    (idx, [1:   8]) = [ -9.66546E-03 0.00169 -6.08273E-04 0.00519 -6.93969E-07 1.00000 -8.51619E-03 0.00754 ];
INF_S5                    (idx, [1:   8]) = [  1.36757E-04 0.13078  2.26833E-05 0.12363 -3.75220E-05 0.04238  6.51650E-03 0.00829 ];
INF_S6                    (idx, [1:   8]) = [  5.19107E-03 0.00223 -1.39355E-04 0.02270 -4.94440E-05 0.03361 -1.65954E-02 0.00295 ];
INF_S7                    (idx, [1:   8]) = [  8.88440E-04 0.01231 -1.69810E-04 0.01219 -4.41490E-05 0.02820  3.71225E-04 0.12856 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10977E-01 0.00012  1.80007E-02 0.00036  1.15173E-03 0.00456  1.46568E+00 0.00054 ];
INF_SP1                   (idx, [1:   8]) = [  2.38665E-01 0.00020  5.24341E-03 0.00084  4.91947E-04 0.00731  3.88266E-01 0.00069 ];
INF_SP2                   (idx, [1:   8]) = [  9.78009E-02 0.00032 -1.57546E-03 0.00218  2.66195E-04 0.00849  9.18192E-02 0.00098 ];
INF_SP3                   (idx, [1:   8]) = [  9.24791E-03 0.00232 -1.85509E-03 0.00180  9.70590E-05 0.02221  2.75397E-02 0.00243 ];
INF_SP4                   (idx, [1:   8]) = [ -9.66550E-03 0.00169 -6.08273E-04 0.00519 -6.93969E-07 1.00000 -8.51619E-03 0.00754 ];
INF_SP5                   (idx, [1:   8]) = [  1.36764E-04 0.13083  2.26833E-05 0.12363 -3.75220E-05 0.04238  6.51650E-03 0.00829 ];
INF_SP6                   (idx, [1:   8]) = [  5.19112E-03 0.00223 -1.39355E-04 0.02270 -4.94440E-05 0.03361 -1.65954E-02 0.00295 ];
INF_SP7                   (idx, [1:   8]) = [  8.88433E-04 0.01232 -1.69810E-04 0.01219 -4.41490E-05 0.02820  3.71225E-04 0.12856 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32306E-01 0.00053  1.13123E+00 0.00670 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33690E-01 0.00111  1.20966E+00 0.00957 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33877E-01 0.00096  1.21055E+00 0.00794 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29418E-01 0.00079  1.00191E+00 0.00624 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43490E+00 0.00053  2.94980E-01 0.00664 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42643E+00 0.00111  2.76161E-01 0.00949 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42528E+00 0.00096  2.75767E-01 0.00780 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45297E+00 0.00079  3.33011E-01 0.00629 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.13156E-03 0.00870  1.81199E-04 0.04726  9.92680E-04 0.02099  9.67128E-04 0.02167  2.74625E-03 0.01329  9.46476E-04 0.02201  2.97829E-04 0.03634 ];
LAMBDA                    (idx, [1:  14]) = [  8.14619E-01 0.01950  1.24947E-02 0.00023  3.13664E-02 0.00049  1.10134E-01 0.00050  3.21480E-01 0.00037  1.34029E+00 0.00064  8.93888E+00 0.00345 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0004' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:47:07 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:40:17 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292427 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00909E+00  9.97264E-01  1.00628E+00  1.00056E+00  9.86817E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13162E-02 0.00107  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88684E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.03643E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04068E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67842E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.54751E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.54665E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.64178E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.16779E-01 0.00113  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000752 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00015E+04 0.00080 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00015E+04 0.00080 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.63640E+02 ;
RUNNING_TIME              (idx, 1)        =  5.31640E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.83567E-01  4.83567E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.36933E-01  9.13333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  5.20904E+01  3.38202E+00  2.63555E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  4.48233E-01  2.68333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.69000E-02  7.83332E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  5.31638E+01  1.26212E+02 ];
CPU_USAGE                 (idx, 1)        = 4.95899 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99862E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76634E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.30489E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.80353E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.37890E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.70341E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.18897E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.60145E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.68460E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  8.51541E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.26438E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.21368E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.11984E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.30173E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.55240E+07 ;
SR90_ACTIVITY             (idx, 1)        =  6.99839E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.24277E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.29483E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.35254E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.80907E+12 ;
CS137_ACTIVITY            (idx, 1)        =  8.09833E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.11459E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.46997E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.77423E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.22487E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.42168E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 8 ;
BURNUP                     (idx, [1:  2])  = [  6.00000E+00  6.00052E+00 ];
BURN_DAYS                 (idx, 1)        =  1.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.49496E-01 0.00105 ];
U235_FISS                 (idx, [1:   4]) = [  9.68278E+15 0.00075  7.02679E-01 0.00046 ];
U238_FISS                 (idx, [1:   4]) = [  9.51320E+14 0.00277  6.90278E-02 0.00260 ];
PU239_FISS                (idx, [1:   4]) = [  3.00083E+15 0.00155  2.17771E-01 0.00143 ];
PU240_FISS                (idx, [1:   4]) = [  2.09460E+12 0.05930  1.52044E-04 0.05934 ];
PU241_FISS                (idx, [1:   4]) = [  1.36851E+14 0.00715  9.92931E-03 0.00707 ];
U235_CAPT                 (idx, [1:   4]) = [  2.13673E+15 0.00175  1.05793E-01 0.00166 ];
U238_CAPT                 (idx, [1:   4]) = [  8.46614E+15 0.00100  4.19148E-01 0.00068 ];
PU239_CAPT                (idx, [1:   4]) = [  1.66841E+15 0.00196  8.26085E-02 0.00191 ];
PU240_CAPT                (idx, [1:   4]) = [  6.43672E+14 0.00325  3.18679E-02 0.00318 ];
PU241_CAPT                (idx, [1:   4]) = [  4.93619E+13 0.01206  2.44458E-03 0.01210 ];
XE135_CAPT                (idx, [1:   4]) = [  7.39173E+14 0.00286  3.66010E-02 0.00286 ];
SM149_CAPT                (idx, [1:   4]) = [  1.80709E+14 0.00593  8.94812E-03 0.00594 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000752 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.73671E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000752 5.00774E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2947230 2.95143E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2010925 2.01370E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 42597 4.26067E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000752 5.00774E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.84288E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.52943E+16 1.8E-05  3.52943E+16 1.8E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37761E+16 3.0E-06  1.37761E+16 3.0E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.01859E+16 0.00046  1.47603E+16 0.00047  5.42559E+15 0.00111 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.39621E+16 0.00027  2.85365E+16 0.00024  5.42559E+15 0.00111 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.42168E+16 0.00051  3.42168E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.62209E+18 0.00048  4.33746E+17 0.00047  1.18834E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.91617E+14 0.00505 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.42537E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.21522E+18 0.00061 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11816E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11816E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.61870E+00 0.00048 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.83106E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.61052E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24209E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94472E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96990E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.04075E+00 0.00054 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.03188E+00 0.00054 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.56198E+00 2.0E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03906E+02 3.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03190E+00 0.00056  1.02569E+00 0.00055  6.19277E-03 0.00865 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.03200E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03162E+00 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.03200E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.04087E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73132E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73168E+01 9.3E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.07324E-07 0.00362 ];
IMP_EALF                  (idx, [1:   2]) = [  6.03549E-07 0.00161 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.34807E-01 0.00281 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.34438E-01 0.00114 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.98090E-03 0.00604  1.66427E-04 0.03321  1.02064E-03 0.01451  9.31038E-04 0.01415  2.71208E-03 0.00909  8.79039E-04 0.01470  2.71681E-04 0.02630 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.71472E-01 0.01380  1.05699E-02 0.01910  3.13289E-02 0.00039  1.10261E-01 0.00037  3.21452E-01 0.00026  1.33771E+00 0.00063  8.43943E+00 0.01151 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.01033E-03 0.00847  1.68788E-04 0.05043  1.01580E-03 0.02034  9.30263E-04 0.02132  2.72726E-03 0.01334  8.90985E-04 0.02126  2.77244E-04 0.04043 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.80160E-01 0.02100  1.24978E-02 0.00033  3.13445E-02 0.00053  1.10183E-01 0.00053  3.21568E-01 0.00039  1.33759E+00 0.00095  8.95175E+00 0.00427 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.54533E-05 0.00134  2.54439E-05 0.00135  2.70761E-05 0.01322 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.62602E-05 0.00116  2.62506E-05 0.00117  2.79261E-05 0.01313 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.02168E-03 0.00876  1.75740E-04 0.05330  1.01559E-03 0.02319  9.50859E-04 0.02206  2.72125E-03 0.01417  8.80976E-04 0.02400  2.77259E-04 0.04301 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.79129E-01 0.02320  1.24982E-02 0.00048  3.13275E-02 0.00068  1.10201E-01 0.00063  3.21461E-01 0.00042  1.33877E+00 0.00092  9.00740E+00 0.00493 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.50903E-05 0.00307  2.50819E-05 0.00307  2.60989E-05 0.03315 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.58876E-05 0.00304  2.58788E-05 0.00304  2.69331E-05 0.03315 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.94205E-03 0.03016  2.31718E-04 0.15601  1.00325E-03 0.07031  1.01711E-03 0.07652  2.56476E-03 0.04350  9.15225E-04 0.08173  2.09992E-04 0.15135 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  6.98569E-01 0.07457  1.24897E-02 3.2E-05  3.12734E-02 0.00157  1.10280E-01 0.00157  3.21616E-01 0.00124  1.33631E+00 0.00276  8.98453E+00 0.01175 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.93000E-03 0.02937  2.27525E-04 0.14813  9.99201E-04 0.07039  1.02758E-03 0.07349  2.55725E-03 0.04267  9.06590E-04 0.08044  2.11845E-04 0.13831 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.10231E-01 0.07120  1.24897E-02 3.2E-05  3.12725E-02 0.00157  1.10284E-01 0.00156  3.21592E-01 0.00122  1.33696E+00 0.00261  8.98826E+00 0.01175 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.37915E+02 0.03027 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.52923E-05 0.00084 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.60949E-05 0.00061 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.90808E-03 0.00581 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.33638E+02 0.00583 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.88118E-07 0.00072 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.80080E-06 0.00040  2.80059E-06 0.00041  2.83482E-06 0.00538 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.90618E-05 0.00090  3.90831E-05 0.00091  3.56692E-05 0.00955 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.58849E-01 0.00033  6.58671E-01 0.00033  7.01661E-01 0.00946 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06255E+01 0.01311 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.54665E+01 0.00047  3.40366E+01 0.00047 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.55803E+04 0.00304  2.98523E+05 0.00164  6.07711E+05 0.00096  6.52259E+05 0.00078  5.98547E+05 0.00078  6.42041E+05 0.00064  4.36015E+05 0.00057  3.85692E+05 0.00070  2.94880E+05 0.00060  2.40976E+05 0.00068  2.07853E+05 0.00072  1.87328E+05 0.00067  1.73002E+05 0.00078  1.64649E+05 0.00096  1.60365E+05 0.00071  1.38472E+05 0.00077  1.36613E+05 0.00089  1.35468E+05 0.00081  1.33069E+05 0.00086  2.60137E+05 0.00075  2.50721E+05 0.00079  1.80934E+05 0.00081  1.17243E+05 0.00077  1.35458E+05 0.00088  1.28350E+05 0.00094  1.15635E+05 0.00102  1.89171E+05 0.00085  4.34951E+04 0.00123  5.46503E+04 0.00105  4.95329E+04 0.00158  2.87953E+04 0.00167  4.99664E+04 0.00142  3.37337E+04 0.00177  2.84983E+04 0.00155  5.27912E+03 0.00314  4.98083E+03 0.00314  4.68420E+03 0.00351  4.58656E+03 0.00417  4.64598E+03 0.00239  4.92026E+03 0.00343  5.40444E+03 0.00339  5.22841E+03 0.00359  9.91508E+03 0.00287  1.60543E+04 0.00271  2.05304E+04 0.00258  5.44173E+04 0.00148  5.71188E+04 0.00146  6.14236E+04 0.00127  4.06890E+04 0.00121  2.93052E+04 0.00155  2.20594E+04 0.00189  2.60266E+04 0.00149  5.05172E+04 0.00122  7.03147E+04 0.00113  1.38081E+05 0.00107  2.15191E+05 0.00110  3.22985E+05 0.00106  2.07067E+05 0.00106  1.48551E+05 0.00130  1.06706E+05 0.00130  9.59987E+04 0.00130  9.44824E+04 0.00122  7.90358E+04 0.00120  5.34471E+04 0.00147  4.94550E+04 0.00148  4.39727E+04 0.00154  3.71634E+04 0.00171  2.93154E+04 0.00146  1.95852E+04 0.00167  6.94379E+03 0.00203 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.04049E+00 0.00049 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.27903E+18 0.00050  3.43098E+17 0.00106 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37885E-01 0.00011  1.53683E+00 0.00032 ];
INF_CAPT                  (idx, [1:   4]) = [  6.97398E-03 0.00051  3.28396E-02 0.00050 ];
INF_ABS                   (idx, [1:   4]) = [  9.02973E-03 0.00040  6.53341E-02 0.00078 ];
INF_FISS                  (idx, [1:   4]) = [  2.05575E-03 0.00044  3.24945E-02 0.00107 ];
INF_NSF                   (idx, [1:   4]) = [  5.36806E-03 0.00042  8.28729E-02 0.00109 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.61124E+00 5.3E-05  2.55037E+00 3.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04461E+02 6.1E-06  2.03775E+02 5.2E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.78638E-08 0.00030  2.56497E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28853E-01 0.00012  1.47146E+00 0.00037 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44001E-01 0.00020  3.90044E-01 0.00048 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62224E-02 0.00021  9.24052E-02 0.00107 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35795E-03 0.00227  2.76608E-02 0.00252 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03217E-02 0.00199 -8.65147E-03 0.00610 ];
INF_SCATT5                (idx, [1:   4]) = [  1.47410E-04 0.12820  6.51047E-03 0.00827 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07368E-03 0.00313 -1.66504E-02 0.00335 ];
INF_SCATT7                (idx, [1:   4]) = [  7.48861E-04 0.01920  4.33462E-04 0.11094 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28895E-01 0.00012  1.47146E+00 0.00037 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44002E-01 0.00020  3.90044E-01 0.00048 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62227E-02 0.00021  9.24052E-02 0.00107 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35859E-03 0.00227  2.76608E-02 0.00252 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03219E-02 0.00198 -8.65147E-03 0.00610 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.47403E-04 0.12830  6.51047E-03 0.00827 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07370E-03 0.00312 -1.66504E-02 0.00335 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.48856E-04 0.01918  4.33462E-04 0.11094 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12823E-01 0.00028  9.94172E-01 0.00027 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56625E+00 0.00028  3.35288E-01 0.00027 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.98834E-03 0.00042  6.53341E-02 0.00078 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69587E-02 0.00019  6.65321E-02 0.00084 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10926E-01 0.00011  1.79270E-02 0.00042  1.16675E-03 0.00448  1.47030E+00 0.00037 ];
INF_S1                    (idx, [1:   8]) = [  2.38768E-01 0.00019  5.23358E-03 0.00087  4.96816E-04 0.00738  3.89547E-01 0.00048 ];
INF_S2                    (idx, [1:   8]) = [  9.77940E-02 0.00021 -1.57159E-03 0.00261  2.72839E-04 0.00822  9.21324E-02 0.00107 ];
INF_S3                    (idx, [1:   8]) = [  9.20594E-03 0.00179 -1.84800E-03 0.00180  9.99170E-05 0.02092  2.75609E-02 0.00252 ];
INF_S4                    (idx, [1:   8]) = [ -9.71530E-03 0.00201 -6.06432E-04 0.00327  2.78808E-06 0.52792 -8.65426E-03 0.00610 ];
INF_S5                    (idx, [1:   8]) = [  1.26915E-04 0.14562  2.04956E-05 0.11871 -3.95241E-05 0.03986  6.54999E-03 0.00829 ];
INF_S6                    (idx, [1:   8]) = [  5.21338E-03 0.00308 -1.39705E-04 0.01558 -4.96706E-05 0.02342 -1.66007E-02 0.00338 ];
INF_S7                    (idx, [1:   8]) = [  9.18362E-04 0.01563 -1.69501E-04 0.00852 -4.72069E-05 0.02702  4.80669E-04 0.10049 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10968E-01 0.00011  1.79270E-02 0.00042  1.16675E-03 0.00448  1.47030E+00 0.00037 ];
INF_SP1                   (idx, [1:   8]) = [  2.38768E-01 0.00020  5.23358E-03 0.00087  4.96816E-04 0.00738  3.89547E-01 0.00048 ];
INF_SP2                   (idx, [1:   8]) = [  9.77943E-02 0.00021 -1.57159E-03 0.00261  2.72839E-04 0.00822  9.21324E-02 0.00107 ];
INF_SP3                   (idx, [1:   8]) = [  9.20658E-03 0.00179 -1.84800E-03 0.00180  9.99170E-05 0.02092  2.75609E-02 0.00252 ];
INF_SP4                   (idx, [1:   8]) = [ -9.71544E-03 0.00201 -6.06432E-04 0.00327  2.78808E-06 0.52792 -8.65426E-03 0.00610 ];
INF_SP5                   (idx, [1:   8]) = [  1.26907E-04 0.14570  2.04956E-05 0.11871 -3.95241E-05 0.03986  6.54999E-03 0.00829 ];
INF_SP6                   (idx, [1:   8]) = [  5.21341E-03 0.00307 -1.39705E-04 0.01558 -4.96706E-05 0.02342 -1.66007E-02 0.00338 ];
INF_SP7                   (idx, [1:   8]) = [  9.18357E-04 0.01560 -1.69501E-04 0.00852 -4.72069E-05 0.02702  4.80669E-04 0.10049 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31962E-01 0.00066  1.15469E+00 0.00704 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33551E-01 0.00103  1.23613E+00 0.00731 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33547E-01 0.00101  1.24872E+00 0.01030 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28860E-01 0.00083  1.01316E+00 0.00746 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43703E+00 0.00066  2.89014E-01 0.00689 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42727E+00 0.00103  2.69999E-01 0.00718 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42730E+00 0.00101  2.67616E-01 0.01024 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45652E+00 0.00083  3.29426E-01 0.00718 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.01033E-03 0.00847  1.68788E-04 0.05043  1.01580E-03 0.02034  9.30263E-04 0.02132  2.72726E-03 0.01334  8.90985E-04 0.02126  2.77244E-04 0.04043 ];
LAMBDA                    (idx, [1:  14]) = [  7.80160E-01 0.02100  1.24978E-02 0.00033  3.13445E-02 0.00053  1.10183E-01 0.00053  3.21568E-01 0.00039  1.33759E+00 0.00095  8.95175E+00 0.00427 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0004' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:47:07 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:46:21 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292427 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00960E+00  9.98002E-01  1.00468E+00  1.00156E+00  9.86162E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13265E-02 0.00119  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88674E-01 1.4E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.03885E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04313E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67661E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.53572E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.53487E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.59965E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.15092E-01 0.00117  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001021 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00020E+04 0.00087 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00020E+04 0.00087 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.93970E+02 ;
RUNNING_TIME              (idx, 1)        =  5.92364E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.83567E-01  4.83567E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.56033E-01  9.48333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  5.80877E+01  3.35933E+00  2.63797E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.03633E-01  2.69500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.02667E-02  8.16667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  5.92363E+01  1.26258E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96266 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00017E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.77529E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.33244E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.79450E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.85955E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.71928E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.20000E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61313E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.67447E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  9.49599E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.32173E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.89824E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.20414E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.59775E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.60132E+07 ;
SR90_ACTIVITY             (idx, 1)        =  8.05546E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.26268E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.32279E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.37960E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.84398E+12 ;
CS137_ACTIVITY            (idx, 1)        =  9.44614E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.13519E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.45006E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.58448E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.23153E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.46812E+12 0.00046  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 9 ;
BURNUP                     (idx, [1:  2])  = [  7.00000E+00  7.00062E+00 ];
BURN_DAYS                 (idx, 1)        =  1.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.59607E-01 0.00114 ];
U235_FISS                 (idx, [1:   4]) = [  9.30209E+15 0.00080  6.75740E-01 0.00051 ];
U238_FISS                 (idx, [1:   4]) = [  9.59329E+14 0.00279  6.96807E-02 0.00262 ];
PU239_FISS                (idx, [1:   4]) = [  3.30840E+15 0.00143  2.40338E-01 0.00131 ];
PU240_FISS                (idx, [1:   4]) = [  2.35662E+12 0.04942  1.71085E-04 0.04936 ];
PU241_FISS                (idx, [1:   4]) = [  1.86803E+14 0.00610  1.35693E-02 0.00603 ];
U235_CAPT                 (idx, [1:   4]) = [  2.05064E+15 0.00180  9.92035E-02 0.00171 ];
U238_CAPT                 (idx, [1:   4]) = [  8.54803E+15 0.00106  4.13498E-01 0.00074 ];
PU239_CAPT                (idx, [1:   4]) = [  1.83354E+15 0.00187  8.87052E-02 0.00185 ];
PU240_CAPT                (idx, [1:   4]) = [  7.70198E+14 0.00307  3.72589E-02 0.00301 ];
PU241_CAPT                (idx, [1:   4]) = [  6.69051E+13 0.00977  3.23676E-03 0.00976 ];
XE135_CAPT                (idx, [1:   4]) = [  7.38103E+14 0.00306  3.57096E-02 0.00306 ];
SM149_CAPT                (idx, [1:   4]) = [  1.84813E+14 0.00587  8.93993E-03 0.00582 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001021 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.68553E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001021 5.00769E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2976220 2.98019E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1982017 1.98471E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 42784 4.27889E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001021 5.00769E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.53903E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.54313E+16 1.9E-05  3.54313E+16 1.9E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37651E+16 3.4E-06  1.37651E+16 3.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.06698E+16 0.00045  1.52093E+16 0.00048  5.46050E+15 0.00111 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.44349E+16 0.00027  2.89744E+16 0.00025  5.46050E+15 0.00111 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.46812E+16 0.00046  3.46812E+16 0.00046  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.64091E+18 0.00043  4.38955E+17 0.00043  1.20196E+18 0.00049 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.96827E+14 0.00503 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.47317E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.22764E+18 0.00059 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11699E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11699E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.61092E+00 0.00049 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.83679E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.57995E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24067E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94442E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96983E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.03054E+00 0.00059 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.02172E+00 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.57400E+00 2.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04070E+02 3.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.02149E+00 0.00061  1.01559E+00 0.00060  6.13229E-03 0.00912 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.02176E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.02173E+00 0.00046 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.02176E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.03058E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72984E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72899E+01 8.4E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.16400E-07 0.00364 ];
IMP_EALF                  (idx, [1:   2]) = [  6.19946E-07 0.00145 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.36787E-01 0.00290 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.38397E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.90977E-03 0.00590  1.57809E-04 0.03428  9.78198E-04 0.01403  9.33639E-04 0.01441  2.66755E-03 0.00871  8.91680E-04 0.01449  2.80889E-04 0.02727 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.95997E-01 0.01374  1.03016E-02 0.02069  3.12871E-02 0.00038  1.10243E-01 0.00038  3.21644E-01 0.00027  1.33589E+00 0.00072  8.53174E+00 0.01069 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.02357E-03 0.00826  1.71028E-04 0.05238  1.02340E-03 0.01983  9.12638E-04 0.02149  2.71791E-03 0.01295  9.13895E-04 0.02158  2.84707E-04 0.04222 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.91021E-01 0.02118  1.25018E-02 0.00041  3.13005E-02 0.00055  1.10267E-01 0.00052  3.21668E-01 0.00040  1.33559E+00 0.00114  8.97296E+00 0.00470 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.55190E-05 0.00124  2.55076E-05 0.00124  2.74175E-05 0.01476 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.60624E-05 0.00107  2.60508E-05 0.00108  2.80003E-05 0.01474 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.01531E-03 0.00927  1.60594E-04 0.05739  1.00503E-03 0.02217  9.39238E-04 0.02299  2.71696E-03 0.01406  9.04221E-04 0.02512  2.89264E-04 0.04362 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.03295E-01 0.02298  1.24967E-02 0.00041  3.12965E-02 0.00066  1.10215E-01 0.00063  3.21625E-01 0.00045  1.33537E+00 0.00148  8.92826E+00 0.00650 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.51586E-05 0.00302  2.51515E-05 0.00304  2.59526E-05 0.03268 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.56941E-05 0.00295  2.56870E-05 0.00298  2.64932E-05 0.03267 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.00820E-03 0.02998  1.56285E-04 0.16388  9.95170E-04 0.07247  9.77983E-04 0.07483  2.68775E-03 0.04453  9.08451E-04 0.08869  2.82562E-04 0.13411 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.06215E-01 0.07194  1.24903E-02 2.1E-05  3.12926E-02 0.00156  1.10146E-01 0.00144  3.21441E-01 0.00124  1.33969E+00 0.00207  8.96654E+00 0.01064 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.03964E-03 0.02844  1.63363E-04 0.16032  1.00772E-03 0.06991  9.79286E-04 0.07350  2.69609E-03 0.04347  9.01154E-04 0.08433  2.92025E-04 0.12742 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.17274E-01 0.07234  1.24903E-02 2.1E-05  3.12983E-02 0.00155  1.10170E-01 0.00144  3.21380E-01 0.00122  1.33986E+00 0.00203  8.97345E+00 0.01064 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.38618E+02 0.02966 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.53416E-05 0.00083 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.58813E-05 0.00057 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.07162E-03 0.00568 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.39651E+02 0.00571 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.84373E-07 0.00070 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.79010E-06 0.00043  2.78994E-06 0.00043  2.81510E-06 0.00529 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.88960E-05 0.00085  3.89183E-05 0.00085  3.53636E-05 0.01021 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.55784E-01 0.00034  6.55589E-01 0.00035  7.00195E-01 0.00890 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05294E+01 0.01285 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.53487E+01 0.00047  3.39368E+01 0.00045 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.60061E+04 0.00269  2.98495E+05 0.00132  6.08212E+05 0.00081  6.52102E+05 0.00072  5.99097E+05 0.00062  6.41968E+05 0.00073  4.36106E+05 0.00056  3.86538E+05 0.00066  2.94913E+05 0.00059  2.41281E+05 0.00081  2.08290E+05 0.00065  1.87275E+05 0.00081  1.73065E+05 0.00079  1.64279E+05 0.00093  1.60527E+05 0.00064  1.38486E+05 0.00100  1.36531E+05 0.00120  1.35174E+05 0.00096  1.33005E+05 0.00099  2.59866E+05 0.00087  2.50838E+05 0.00057  1.81149E+05 0.00079  1.17269E+05 0.00134  1.35379E+05 0.00104  1.28336E+05 0.00080  1.15757E+05 0.00093  1.89202E+05 0.00078  4.34552E+04 0.00181  5.46238E+04 0.00144  4.94932E+04 0.00140  2.87830E+04 0.00202  4.99976E+04 0.00128  3.37593E+04 0.00187  2.83891E+04 0.00184  5.17129E+03 0.00291  4.85213E+03 0.00324  4.55992E+03 0.00274  4.40420E+03 0.00334  4.47248E+03 0.00368  4.74177E+03 0.00303  5.26529E+03 0.00445  5.14699E+03 0.00406  9.80837E+03 0.00318  1.58921E+04 0.00205  2.02902E+04 0.00190  5.41302E+04 0.00154  5.68046E+04 0.00130  6.09794E+04 0.00106  4.02600E+04 0.00116  2.89728E+04 0.00169  2.16861E+04 0.00183  2.56237E+04 0.00177  4.96567E+04 0.00131  6.94070E+04 0.00155  1.36314E+05 0.00145  2.12844E+05 0.00148  3.20046E+05 0.00155  2.05330E+05 0.00154  1.47209E+05 0.00157  1.05858E+05 0.00167  9.52232E+04 0.00183  9.38849E+04 0.00181  7.83926E+04 0.00187  5.30700E+04 0.00177  4.90513E+04 0.00160  4.35704E+04 0.00228  3.68976E+04 0.00192  2.91367E+04 0.00201  1.95051E+04 0.00195  6.89677E+03 0.00247 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.03055E+00 0.00052 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.29645E+18 0.00043  3.44492E+17 0.00142 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37954E-01 9.2E-05  1.53917E+00 0.00047 ];
INF_CAPT                  (idx, [1:   4]) = [  7.08501E-03 0.00067  3.33401E-02 0.00074 ];
INF_ABS                   (idx, [1:   4]) = [  9.11357E-03 0.00052  6.56691E-02 0.00109 ];
INF_FISS                  (idx, [1:   4]) = [  2.02856E-03 0.00034  3.23290E-02 0.00146 ];
INF_NSF                   (idx, [1:   4]) = [  5.31212E-03 0.00034  8.28738E-02 0.00150 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.61867E+00 7.2E-05  2.56345E+00 5.0E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04559E+02 7.5E-06  2.03954E+02 8.6E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.76587E-08 0.00035  2.56673E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28842E-01 1.0E-04  1.47349E+00 0.00054 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43969E-01 0.00019  3.90589E-01 0.00056 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62299E-02 0.00036  9.26927E-02 0.00107 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32428E-03 0.00270  2.77835E-02 0.00258 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03197E-02 0.00162 -8.64092E-03 0.00723 ];
INF_SCATT5                (idx, [1:   4]) = [  1.20954E-04 0.14243  6.53141E-03 0.00550 ];
INF_SCATT6                (idx, [1:   4]) = [  5.03524E-03 0.00410 -1.67304E-02 0.00284 ];
INF_SCATT7                (idx, [1:   4]) = [  7.27870E-04 0.02374  4.45895E-04 0.11198 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28883E-01 1.0E-04  1.47349E+00 0.00054 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43970E-01 0.00020  3.90589E-01 0.00056 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62297E-02 0.00036  9.26927E-02 0.00107 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32460E-03 0.00270  2.77835E-02 0.00258 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03194E-02 0.00162 -8.64092E-03 0.00723 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.20921E-04 0.14253  6.53141E-03 0.00550 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.03523E-03 0.00410 -1.67304E-02 0.00284 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.27872E-04 0.02374  4.45895E-04 0.11198 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12802E-01 0.00025  9.96278E-01 0.00040 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56640E+00 0.00025  3.34580E-01 0.00040 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.07245E-03 0.00052  6.56691E-02 0.00109 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69524E-02 0.00021  6.68411E-02 0.00116 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11001E-01 9.4E-05  1.78406E-02 0.00043  1.16301E-03 0.00508  1.47232E+00 0.00054 ];
INF_S1                    (idx, [1:   8]) = [  2.38783E-01 0.00019  5.18687E-03 0.00089  4.97103E-04 0.00662  3.90091E-01 0.00056 ];
INF_S2                    (idx, [1:   8]) = [  9.78032E-02 0.00036 -1.57328E-03 0.00244  2.72474E-04 0.01132  9.24203E-02 0.00107 ];
INF_S3                    (idx, [1:   8]) = [  9.16116E-03 0.00220 -1.83689E-03 0.00181  9.58919E-05 0.01935  2.76876E-02 0.00258 ];
INF_S4                    (idx, [1:   8]) = [ -9.72309E-03 0.00162 -5.96662E-04 0.00583  2.08730E-06 0.80381 -8.64301E-03 0.00719 ];
INF_S5                    (idx, [1:   8]) = [  9.63540E-05 0.16892  2.45996E-05 0.08890 -3.70088E-05 0.04270  6.56842E-03 0.00558 ];
INF_S6                    (idx, [1:   8]) = [  5.17636E-03 0.00390 -1.41116E-04 0.02029 -4.93416E-05 0.02649 -1.66810E-02 0.00287 ];
INF_S7                    (idx, [1:   8]) = [  8.98300E-04 0.01819 -1.70430E-04 0.01576 -4.49593E-05 0.02900  4.90855E-04 0.10237 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11042E-01 9.4E-05  1.78406E-02 0.00043  1.16301E-03 0.00508  1.47232E+00 0.00054 ];
INF_SP1                   (idx, [1:   8]) = [  2.38783E-01 0.00019  5.18687E-03 0.00089  4.97103E-04 0.00662  3.90091E-01 0.00056 ];
INF_SP2                   (idx, [1:   8]) = [  9.78030E-02 0.00036 -1.57328E-03 0.00244  2.72474E-04 0.01132  9.24203E-02 0.00107 ];
INF_SP3                   (idx, [1:   8]) = [  9.16148E-03 0.00220 -1.83689E-03 0.00181  9.58919E-05 0.01935  2.76876E-02 0.00258 ];
INF_SP4                   (idx, [1:   8]) = [ -9.72273E-03 0.00162 -5.96662E-04 0.00583  2.08730E-06 0.80381 -8.64301E-03 0.00719 ];
INF_SP5                   (idx, [1:   8]) = [  9.63217E-05 0.16909  2.45996E-05 0.08890 -3.70088E-05 0.04270  6.56842E-03 0.00558 ];
INF_SP6                   (idx, [1:   8]) = [  5.17634E-03 0.00390 -1.41116E-04 0.02029 -4.93416E-05 0.02649 -1.66810E-02 0.00287 ];
INF_SP7                   (idx, [1:   8]) = [  8.98302E-04 0.01818 -1.70430E-04 0.01576 -4.49593E-05 0.02900  4.90855E-04 0.10237 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32023E-01 0.00067  1.14928E+00 0.00691 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33652E-01 0.00096  1.23594E+00 0.00864 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33672E-01 0.00090  1.23421E+00 0.00918 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28820E-01 0.00095  1.01036E+00 0.00701 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43666E+00 0.00067  2.90372E-01 0.00696 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42666E+00 0.00096  2.70177E-01 0.00850 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42653E+00 0.00090  2.70639E-01 0.00943 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45678E+00 0.00095  3.30301E-01 0.00692 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.02357E-03 0.00826  1.71028E-04 0.05238  1.02340E-03 0.01983  9.12638E-04 0.02149  2.71791E-03 0.01295  9.13895E-04 0.02158  2.84707E-04 0.04222 ];
LAMBDA                    (idx, [1:  14]) = [  7.91021E-01 0.02118  1.25018E-02 0.00041  3.13005E-02 0.00055  1.10267E-01 0.00052  3.21668E-01 0.00040  1.33559E+00 0.00114  8.97296E+00 0.00470 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0004' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:47:07 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:52:25 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292427 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00885E+00  9.97053E-01  1.00452E+00  1.00303E+00  9.86548E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13226E-02 0.00114  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88677E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04467E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04894E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67305E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.53272E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.53185E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.56070E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.14231E-01 0.00118  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000890 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00018E+04 0.00080 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00018E+04 0.00080 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.24220E+02 ;
RUNNING_TIME              (idx, 1)        =  6.52927E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.83567E-01  4.83567E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.74750E-01  9.03333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.40700E+01  3.35678E+00  2.62558E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.58217E-01  2.70000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.36667E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  6.52926E+01  1.25862E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96564 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00089E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78264E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.36076E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.78675E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.58714E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.73875E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.21360E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62198E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.66536E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.05196E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.37365E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.65904E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.30449E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.86057E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.64320E+07 ;
SR90_ACTIVITY             (idx, 1)        =  9.08785E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.28151E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.34843E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.40506E+14 ;
CS134_ACTIVITY            (idx, 1)        =  5.03672E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.07930E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.15860E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.43211E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.70407E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.23869E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.51781E+12 0.00047  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 10 ;
BURNUP                     (idx, [1:  2])  = [  8.00000E+00  8.00070E+00 ];
BURN_DAYS                 (idx, 1)        =  2.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.69220E-01 0.00109 ];
U235_FISS                 (idx, [1:   4]) = [  8.94270E+15 0.00081  6.49915E-01 0.00053 ];
U238_FISS                 (idx, [1:   4]) = [  9.78723E+14 0.00272  7.11192E-02 0.00254 ];
PU239_FISS                (idx, [1:   4]) = [  3.58557E+15 0.00138  2.60586E-01 0.00125 ];
PU240_FISS                (idx, [1:   4]) = [  3.10898E+12 0.04493  2.26080E-04 0.04493 ];
PU241_FISS                (idx, [1:   4]) = [  2.42594E+14 0.00573  1.76308E-02 0.00571 ];
U235_CAPT                 (idx, [1:   4]) = [  1.98407E+15 0.00183  9.37434E-02 0.00178 ];
U238_CAPT                 (idx, [1:   4]) = [  8.62891E+15 0.00101  4.07658E-01 0.00068 ];
PU239_CAPT                (idx, [1:   4]) = [  1.98399E+15 0.00186  9.37385E-02 0.00180 ];
PU240_CAPT                (idx, [1:   4]) = [  8.95190E+14 0.00299  4.22932E-02 0.00291 ];
PU241_CAPT                (idx, [1:   4]) = [  8.76405E+13 0.00880  4.14129E-03 0.00883 ];
XE135_CAPT                (idx, [1:   4]) = [  7.39374E+14 0.00297  3.49327E-02 0.00292 ];
SM149_CAPT                (idx, [1:   4]) = [  1.86489E+14 0.00651  8.80984E-03 0.00645 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000890 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.74398E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000890 5.00774E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3004077 3.00841E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1953304 1.95582E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 43509 4.35165E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000890 5.00774E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.28408E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.55585E+16 2.0E-05  3.55585E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37547E+16 3.6E-06  1.37547E+16 3.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.11811E+16 0.00043  1.56464E+16 0.00044  5.53470E+15 0.00106 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.49358E+16 0.00026  2.94010E+16 0.00023  5.53470E+15 0.00106 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.51781E+16 0.00047  3.51781E+16 0.00047  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.66227E+18 0.00045  4.43807E+17 0.00045  1.21846E+18 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.06186E+14 0.00493 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.52419E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.24418E+18 0.00058 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11583E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11583E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.60129E+00 0.00051 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.83358E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.55553E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24067E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94322E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96957E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.02016E+00 0.00057 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.01128E+00 0.00057 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.58520E+00 2.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04224E+02 3.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.01116E+00 0.00058  1.00544E+00 0.00057  5.83969E-03 0.00872 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.01058E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.01093E+00 0.00047 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.01058E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.01945E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72682E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72682E+01 8.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.35280E-07 0.00366 ];
IMP_EALF                  (idx, [1:   2]) = [  6.33595E-07 0.00152 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.41110E-01 0.00277 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.41743E-01 0.00112 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.86334E-03 0.00586  1.57557E-04 0.03594  9.87192E-04 0.01463  9.18472E-04 0.01447  2.64128E-03 0.00857  8.87704E-04 0.01472  2.71141E-04 0.02662 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.81055E-01 0.01377  9.74914E-03 0.02378  3.12410E-02 0.00040  1.10335E-01 0.00042  3.21826E-01 0.00028  1.33134E+00 0.00091  8.34837E+00 0.01231 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.81980E-03 0.00866  1.59736E-04 0.05251  9.69338E-04 0.02131  9.17271E-04 0.02213  2.63175E-03 0.01290  8.71395E-04 0.02250  2.70304E-04 0.03885 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.82410E-01 0.02018  1.25007E-02 0.00042  3.12345E-02 0.00060  1.10388E-01 0.00057  3.21842E-01 0.00041  1.33163E+00 0.00119  8.85331E+00 0.00589 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.56390E-05 0.00132  2.56284E-05 0.00133  2.75444E-05 0.01524 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.59200E-05 0.00113  2.59093E-05 0.00114  2.78463E-05 0.01520 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.76804E-03 0.00884  1.56661E-04 0.05714  9.46735E-04 0.02370  9.21357E-04 0.02489  2.58194E-03 0.01399  8.94668E-04 0.02239  2.66684E-04 0.04287 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.85917E-01 0.02257  1.25032E-02 0.00060  3.12382E-02 0.00073  1.10327E-01 0.00074  3.21852E-01 0.00048  1.33328E+00 0.00129  8.93060E+00 0.00653 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.52276E-05 0.00309  2.52103E-05 0.00313  2.83897E-05 0.04124 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.55043E-05 0.00302  2.54868E-05 0.00306  2.87030E-05 0.04113 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.59604E-03 0.03142  1.31791E-04 0.18559  8.90630E-04 0.08203  9.13979E-04 0.07829  2.59028E-03 0.04862  8.25321E-04 0.07987  2.44045E-04 0.14512 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.69359E-01 0.07189  1.24889E-02 4.8E-05  3.13018E-02 0.00160  1.10199E-01 0.00163  3.22274E-01 0.00141  1.33345E+00 0.00333  8.83764E+00 0.01406 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.59821E-03 0.03119  1.26312E-04 0.17892  9.00763E-04 0.07990  9.20089E-04 0.07917  2.59647E-03 0.04738  8.07759E-04 0.07780  2.46816E-04 0.15031 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.60623E-01 0.07058  1.24890E-02 4.7E-05  3.13060E-02 0.00159  1.10198E-01 0.00163  3.22252E-01 0.00139  1.33408E+00 0.00318  8.83811E+00 0.01407 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.22644E+02 0.03173 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.54401E-05 0.00081 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.57198E-05 0.00059 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.82540E-03 0.00601 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.29085E+02 0.00611 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.83153E-07 0.00068 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.77562E-06 0.00043  2.77542E-06 0.00044  2.81017E-06 0.00561 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.89182E-05 0.00084  3.89399E-05 0.00084  3.53429E-05 0.01029 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.53310E-01 0.00031  6.53187E-01 0.00032  6.85512E-01 0.00899 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04971E+01 0.01458 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.53185E+01 0.00045  3.38364E+01 0.00049 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.61645E+04 0.00408  2.98963E+05 0.00165  6.09145E+05 0.00109  6.52031E+05 0.00076  5.98647E+05 0.00068  6.41634E+05 0.00069  4.35577E+05 0.00084  3.85282E+05 0.00059  2.95580E+05 0.00079  2.41288E+05 0.00089  2.07932E+05 0.00073  1.87264E+05 0.00084  1.72899E+05 0.00088  1.64265E+05 0.00073  1.60192E+05 0.00116  1.38302E+05 0.00098  1.36667E+05 0.00081  1.35406E+05 0.00094  1.33166E+05 0.00090  2.60088E+05 0.00062  2.51264E+05 0.00061  1.80951E+05 0.00093  1.17329E+05 0.00104  1.35781E+05 0.00093  1.28425E+05 0.00107  1.15323E+05 0.00106  1.88702E+05 0.00075  4.34121E+04 0.00127  5.43331E+04 0.00162  4.95494E+04 0.00133  2.87169E+04 0.00170  4.98123E+04 0.00188  3.37435E+04 0.00172  2.82041E+04 0.00221  5.12936E+03 0.00355  4.73696E+03 0.00303  4.37429E+03 0.00382  4.23366E+03 0.00377  4.32444E+03 0.00265  4.65479E+03 0.00387  5.18774E+03 0.00431  5.03981E+03 0.00348  9.68624E+03 0.00255  1.57109E+04 0.00240  2.01764E+04 0.00251  5.36817E+04 0.00131  5.64693E+04 0.00131  6.05901E+04 0.00171  3.99116E+04 0.00120  2.86043E+04 0.00174  2.13919E+04 0.00173  2.52533E+04 0.00144  4.91388E+04 0.00142  6.86768E+04 0.00137  1.35302E+05 0.00135  2.11697E+05 0.00096  3.18865E+05 0.00106  2.04618E+05 0.00115  1.46862E+05 0.00133  1.05530E+05 0.00123  9.49754E+04 0.00121  9.36753E+04 0.00136  7.83068E+04 0.00129  5.30076E+04 0.00121  4.90137E+04 0.00147  4.35693E+04 0.00168  3.68558E+04 0.00178  2.90850E+04 0.00154  1.94603E+04 0.00125  6.92193E+03 0.00230 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.01980E+00 0.00038 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.31442E+18 0.00042  3.47871E+17 0.00086 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38054E-01 0.00012  1.54360E+00 0.00028 ];
INF_CAPT                  (idx, [1:   4]) = [  7.18486E-03 0.00061  3.37427E-02 0.00043 ];
INF_ABS                   (idx, [1:   4]) = [  9.18288E-03 0.00051  6.57376E-02 0.00063 ];
INF_FISS                  (idx, [1:   4]) = [  1.99802E-03 0.00042  3.19949E-02 0.00085 ];
INF_NSF                   (idx, [1:   4]) = [  5.24617E-03 0.00042  8.24075E-02 0.00087 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.62568E+00 6.4E-05  2.57564E+00 3.6E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04654E+02 8.0E-06  2.04123E+02 6.1E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.74323E-08 0.00043  2.56969E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28874E-01 0.00012  1.47788E+00 0.00032 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44029E-01 0.00016  3.91755E-01 0.00042 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62901E-02 0.00022  9.27704E-02 0.00100 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35065E-03 0.00299  2.78361E-02 0.00255 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03101E-02 0.00178 -8.75523E-03 0.00749 ];
INF_SCATT5                (idx, [1:   4]) = [  1.59570E-04 0.11027  6.51076E-03 0.00901 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10417E-03 0.00268 -1.67928E-02 0.00261 ];
INF_SCATT7                (idx, [1:   4]) = [  7.77819E-04 0.01478  4.38504E-04 0.06736 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28915E-01 0.00012  1.47788E+00 0.00032 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44030E-01 0.00017  3.91755E-01 0.00042 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62904E-02 0.00022  9.27704E-02 0.00100 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35088E-03 0.00299  2.78361E-02 0.00255 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03100E-02 0.00178 -8.75523E-03 0.00749 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.59747E-04 0.11007  6.51076E-03 0.00901 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10409E-03 0.00268 -1.67928E-02 0.00261 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.77774E-04 0.01480  4.38504E-04 0.06736 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12720E-01 0.00029  9.99366E-01 0.00028 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56701E+00 0.00029  3.33545E-01 0.00028 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.14143E-03 0.00053  6.57376E-02 0.00063 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69611E-02 0.00019  6.68818E-02 0.00086 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11092E-01 0.00012  1.77815E-02 0.00039  1.16570E-03 0.00439  1.47672E+00 0.00032 ];
INF_S1                    (idx, [1:   8]) = [  2.38857E-01 0.00016  5.17237E-03 0.00105  4.99007E-04 0.00673  3.91256E-01 0.00042 ];
INF_S2                    (idx, [1:   8]) = [  9.78604E-02 0.00022 -1.57037E-03 0.00189  2.73592E-04 0.00865  9.24968E-02 0.00100 ];
INF_S3                    (idx, [1:   8]) = [  9.18678E-03 0.00225 -1.83614E-03 0.00216  1.00182E-04 0.01599  2.77359E-02 0.00255 ];
INF_S4                    (idx, [1:   8]) = [ -9.71483E-03 0.00187 -5.95285E-04 0.00473  2.53080E-06 0.53234 -8.75776E-03 0.00748 ];
INF_S5                    (idx, [1:   8]) = [  1.27717E-04 0.13720  3.18531E-05 0.06112 -3.77862E-05 0.03957  6.54855E-03 0.00898 ];
INF_S6                    (idx, [1:   8]) = [  5.24019E-03 0.00240 -1.36023E-04 0.02159 -5.08131E-05 0.02617 -1.67420E-02 0.00260 ];
INF_S7                    (idx, [1:   8]) = [  9.45992E-04 0.01266 -1.68173E-04 0.01248 -4.53001E-05 0.02622  4.83804E-04 0.06150 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11134E-01 0.00012  1.77815E-02 0.00039  1.16570E-03 0.00439  1.47672E+00 0.00032 ];
INF_SP1                   (idx, [1:   8]) = [  2.38858E-01 0.00016  5.17237E-03 0.00105  4.99007E-04 0.00673  3.91256E-01 0.00042 ];
INF_SP2                   (idx, [1:   8]) = [  9.78608E-02 0.00022 -1.57037E-03 0.00189  2.73592E-04 0.00865  9.24968E-02 0.00100 ];
INF_SP3                   (idx, [1:   8]) = [  9.18701E-03 0.00225 -1.83614E-03 0.00216  1.00182E-04 0.01599  2.77359E-02 0.00255 ];
INF_SP4                   (idx, [1:   8]) = [ -9.71468E-03 0.00187 -5.95285E-04 0.00473  2.53080E-06 0.53234 -8.75776E-03 0.00748 ];
INF_SP5                   (idx, [1:   8]) = [  1.27894E-04 0.13687  3.18531E-05 0.06112 -3.77862E-05 0.03957  6.54855E-03 0.00898 ];
INF_SP6                   (idx, [1:   8]) = [  5.24011E-03 0.00241 -1.36023E-04 0.02159 -5.08131E-05 0.02617 -1.67420E-02 0.00260 ];
INF_SP7                   (idx, [1:   8]) = [  9.45947E-04 0.01268 -1.68173E-04 0.01248 -4.53001E-05 0.02622  4.83804E-04 0.06150 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31920E-01 0.00065  1.15312E+00 0.00828 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33346E-01 0.00094  1.23396E+00 0.01023 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33423E-01 0.00089  1.25005E+00 0.00990 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29049E-01 0.00072  1.00964E+00 0.00710 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43729E+00 0.00065  2.89532E-01 0.00802 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42852E+00 0.00094  2.70792E-01 0.00994 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42805E+00 0.00089  2.67264E-01 0.00960 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45531E+00 0.00072  3.30539E-01 0.00690 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.81980E-03 0.00866  1.59736E-04 0.05251  9.69338E-04 0.02131  9.17271E-04 0.02213  2.63175E-03 0.01290  8.71395E-04 0.02250  2.70304E-04 0.03885 ];
LAMBDA                    (idx, [1:  14]) = [  7.82410E-01 0.02018  1.25007E-02 0.00042  3.12345E-02 0.00060  1.10388E-01 0.00057  3.21842E-01 0.00041  1.33163E+00 0.00119  8.85331E+00 0.00589 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0004' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:47:07 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:58:26 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292427 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.01001E+00  9.96720E-01  1.00522E+00  1.00184E+00  9.86210E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 6.6E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13290E-02 0.00107  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88671E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05018E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05445E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67162E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.52908E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.52820E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.52181E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.13602E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000724 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00014E+04 0.00081 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00014E+04 0.00081 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.54303E+02 ;
RUNNING_TIME              (idx, 1)        =  7.13158E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.83567E-01  4.83567E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.93983E-01  9.58333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.00176E+01  3.33433E+00  2.61323E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  6.13917E-01  2.69500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.70333E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.13156E+01  1.25703E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96809 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99884E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78859E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.38695E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.77957E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.63133E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.75874E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.22761E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62819E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.65678E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.15905E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.42055E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.49301E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.40787E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.09747E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.67977E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.00967E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.29906E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.37210E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.42884E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.36950E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.21387E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.18111E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.41524E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.19929E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.24561E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.56769E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 11 ;
BURNUP                     (idx, [1:  2])  = [  9.00000E+00  9.00078E+00 ];
BURN_DAYS                 (idx, 1)        =  2.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.79849E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  8.62543E+15 0.00087  6.27559E-01 0.00059 ];
U238_FISS                 (idx, [1:   4]) = [  9.90626E+14 0.00269  7.20685E-02 0.00255 ];
PU239_FISS                (idx, [1:   4]) = [  3.81725E+15 0.00135  2.77736E-01 0.00122 ];
PU240_FISS                (idx, [1:   4]) = [  3.26273E+12 0.04774  2.37679E-04 0.04782 ];
PU241_FISS                (idx, [1:   4]) = [  2.99626E+14 0.00466  2.18009E-02 0.00465 ];
U235_CAPT                 (idx, [1:   4]) = [  1.91598E+15 0.00187  8.83940E-02 0.00183 ];
U238_CAPT                 (idx, [1:   4]) = [  8.72433E+15 0.00106  4.02453E-01 0.00072 ];
PU239_CAPT                (idx, [1:   4]) = [  2.11663E+15 0.00178  9.76531E-02 0.00176 ];
PU240_CAPT                (idx, [1:   4]) = [  1.01199E+15 0.00261  4.66884E-02 0.00258 ];
PU241_CAPT                (idx, [1:   4]) = [  1.09025E+14 0.00795  5.02938E-03 0.00792 ];
XE135_CAPT                (idx, [1:   4]) = [  7.42994E+14 0.00306  3.42809E-02 0.00309 ];
SM149_CAPT                (idx, [1:   4]) = [  1.93683E+14 0.00573  8.93435E-03 0.00567 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000724 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.87612E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000724 5.00788E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3033508 3.03792E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1923577 1.92632E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 43639 4.36433E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000724 5.00788E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.47035E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.56746E+16 2.0E-05  3.56746E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37451E+16 3.7E-06  1.37451E+16 3.7E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.16694E+16 0.00045  1.60626E+16 0.00044  5.60685E+15 0.00111 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.54145E+16 0.00028  2.98077E+16 0.00024  5.60685E+15 0.00111 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.56769E+16 0.00051  3.56769E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.68363E+18 0.00048  4.48758E+17 0.00046  1.23487E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.11453E+14 0.00488 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.57260E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.26055E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11467E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11467E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.59147E+00 0.00052 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.82602E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.52848E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24064E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94334E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96919E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.00870E+00 0.00056 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.99899E-01 0.00056 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.59544E+00 2.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04366E+02 3.7E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.99908E-01 0.00058  9.94107E-01 0.00057  5.79170E-03 0.00967 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.00018E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.00006E+00 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.00018E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.00898E+00 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72450E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72450E+01 9.2E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.50340E-07 0.00379 ];
IMP_EALF                  (idx, [1:   2]) = [  6.48485E-07 0.00158 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.45598E-01 0.00275 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.45591E-01 0.00120 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.91310E-03 0.00617  1.68081E-04 0.03547  1.02419E-03 0.01325  9.32299E-04 0.01429  2.63055E-03 0.00907  8.83905E-04 0.01534  2.74075E-04 0.02610 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.74200E-01 0.01338  9.92679E-03 0.02280  3.12081E-02 0.00041  1.10312E-01 0.00041  3.21823E-01 0.00028  1.32824E+00 0.00101  8.27668E+00 0.01297 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.74469E-03 0.00926  1.66968E-04 0.05201  9.96109E-04 0.01986  9.29658E-04 0.02230  2.54151E-03 0.01318  8.56270E-04 0.02322  2.54177E-04 0.04002 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.55583E-01 0.02048  1.25057E-02 0.00045  3.12150E-02 0.00056  1.10266E-01 0.00056  3.21805E-01 0.00041  1.33139E+00 0.00109  8.80683E+00 0.00662 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.57767E-05 0.00134  2.57615E-05 0.00134  2.85564E-05 0.01334 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.57702E-05 0.00122  2.57550E-05 0.00122  2.85443E-05 0.01330 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.79143E-03 0.00985  1.79797E-04 0.05544  1.01076E-03 0.02268  9.07533E-04 0.02361  2.55212E-03 0.01440  8.69991E-04 0.02477  2.71220E-04 0.04411 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.73530E-01 0.02324  1.25069E-02 0.00056  3.11989E-02 0.00072  1.10234E-01 0.00071  3.21841E-01 0.00046  1.33054E+00 0.00151  8.74975E+00 0.00889 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.53698E-05 0.00299  2.53501E-05 0.00299  2.74335E-05 0.03370 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.53637E-05 0.00295  2.53440E-05 0.00294  2.74302E-05 0.03374 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.84243E-03 0.03393  1.87288E-04 0.17097  1.05057E-03 0.07405  9.45763E-04 0.08395  2.49765E-03 0.05021  9.04594E-04 0.07770  2.56571E-04 0.15244 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.00868E-01 0.07609  1.24890E-02 4.3E-05  3.12240E-02 0.00167  1.10345E-01 0.00176  3.22003E-01 0.00140  1.33111E+00 0.00374  8.69596E+00 0.02061 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.84779E-03 0.03342  1.91000E-04 0.16411  1.05613E-03 0.07246  9.60561E-04 0.08268  2.47234E-03 0.05011  9.19314E-04 0.07639  2.48442E-04 0.14975 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.88366E-01 0.07444  1.24890E-02 4.4E-05  3.12218E-02 0.00166  1.10339E-01 0.00174  3.21912E-01 0.00138  1.33143E+00 0.00372  8.69642E+00 0.02061 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.31428E+02 0.03414 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.55880E-05 0.00080 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.55813E-05 0.00054 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.89556E-03 0.00577 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.30439E+02 0.00577 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.81784E-07 0.00071 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.76485E-06 0.00042  2.76471E-06 0.00042  2.78739E-06 0.00576 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.89459E-05 0.00087  3.89646E-05 0.00088  3.59266E-05 0.01053 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.50643E-01 0.00035  6.50527E-01 0.00035  6.84524E-01 0.01022 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08655E+01 0.01357 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.52820E+01 0.00048  3.37512E+01 0.00050 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.70465E+04 0.00363  2.99614E+05 0.00179  6.08447E+05 0.00096  6.51954E+05 0.00081  5.98016E+05 0.00067  6.41837E+05 0.00079  4.35406E+05 0.00071  3.85198E+05 0.00063  2.95058E+05 0.00056  2.40758E+05 0.00102  2.07536E+05 0.00082  1.87018E+05 0.00086  1.73082E+05 0.00089  1.64500E+05 0.00089  1.60267E+05 0.00082  1.38471E+05 0.00091  1.36767E+05 0.00098  1.35584E+05 0.00083  1.33289E+05 0.00076  2.60002E+05 0.00059  2.50766E+05 0.00081  1.80633E+05 0.00064  1.17265E+05 0.00102  1.35737E+05 0.00099  1.28422E+05 0.00080  1.15496E+05 0.00110  1.88374E+05 0.00081  4.34088E+04 0.00140  5.43606E+04 0.00158  4.93450E+04 0.00141  2.88094E+04 0.00161  4.96958E+04 0.00127  3.35609E+04 0.00162  2.81296E+04 0.00181  5.07890E+03 0.00303  4.66226E+03 0.00288  4.25391E+03 0.00328  4.10528E+03 0.00416  4.17838E+03 0.00228  4.52756E+03 0.00272  5.09008E+03 0.00275  4.99574E+03 0.00322  9.70772E+03 0.00229  1.57063E+04 0.00264  2.00865E+04 0.00180  5.34619E+04 0.00179  5.62383E+04 0.00159  6.02932E+04 0.00122  3.94423E+04 0.00121  2.82675E+04 0.00124  2.11582E+04 0.00178  2.49030E+04 0.00144  4.86048E+04 0.00157  6.80949E+04 0.00159  1.34224E+05 0.00128  2.10748E+05 0.00117  3.17300E+05 0.00116  2.03723E+05 0.00127  1.46389E+05 0.00125  1.05389E+05 0.00158  9.48656E+04 0.00152  9.35849E+04 0.00150  7.80353E+04 0.00169  5.29197E+04 0.00169  4.88633E+04 0.00166  4.35013E+04 0.00161  3.67885E+04 0.00179  2.90303E+04 0.00187  1.94344E+04 0.00165  6.89462E+03 0.00202 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.00887E+00 0.00050 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.33248E+18 0.00048  3.51184E+17 0.00115 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38161E-01 0.00013  1.54758E+00 0.00038 ];
INF_CAPT                  (idx, [1:   4]) = [  7.27930E-03 0.00062  3.40880E-02 0.00062 ];
INF_ABS                   (idx, [1:   4]) = [  9.25147E-03 0.00050  6.57503E-02 0.00088 ];
INF_FISS                  (idx, [1:   4]) = [  1.97217E-03 0.00048  3.16623E-02 0.00117 ];
INF_NSF                   (idx, [1:   4]) = [  5.19174E-03 0.00048  8.19000E-02 0.00120 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.63250E+00 7.2E-05  2.58667E+00 4.4E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04743E+02 8.1E-06  2.04277E+02 7.6E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.72967E-08 0.00047  2.57195E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28905E-01 0.00013  1.48183E+00 0.00043 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44040E-01 0.00024  3.93011E-01 0.00058 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62710E-02 0.00042  9.30815E-02 0.00102 ];
INF_SCATT3                (idx, [1:   4]) = [  7.33833E-03 0.00417  2.78272E-02 0.00245 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03287E-02 0.00163 -8.81475E-03 0.00681 ];
INF_SCATT5                (idx, [1:   4]) = [  1.48435E-04 0.09644  6.55027E-03 0.00916 ];
INF_SCATT6                (idx, [1:   4]) = [  5.09859E-03 0.00270 -1.68564E-02 0.00386 ];
INF_SCATT7                (idx, [1:   4]) = [  7.39636E-04 0.02053  4.91837E-04 0.10348 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28947E-01 0.00013  1.48183E+00 0.00043 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44041E-01 0.00024  3.93011E-01 0.00058 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62709E-02 0.00042  9.30815E-02 0.00102 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.33807E-03 0.00416  2.78272E-02 0.00245 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03287E-02 0.00164 -8.81475E-03 0.00681 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.48486E-04 0.09643  6.55027E-03 0.00916 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.09858E-03 0.00270 -1.68564E-02 0.00386 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.39643E-04 0.02047  4.91837E-04 0.10348 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12677E-01 0.00027  1.00209E+00 0.00034 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56732E+00 0.00027  3.32639E-01 0.00034 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.20929E-03 0.00050  6.57503E-02 0.00088 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69730E-02 0.00020  6.69131E-02 0.00092 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11188E-01 0.00013  1.77171E-02 0.00039  1.16758E-03 0.00311  1.48067E+00 0.00043 ];
INF_S1                    (idx, [1:   8]) = [  2.38887E-01 0.00024  5.15346E-03 0.00088  4.99658E-04 0.00710  3.92511E-01 0.00058 ];
INF_S2                    (idx, [1:   8]) = [  9.78461E-02 0.00041 -1.57504E-03 0.00265  2.69588E-04 0.00963  9.28119E-02 0.00102 ];
INF_S3                    (idx, [1:   8]) = [  9.16903E-03 0.00331 -1.83070E-03 0.00168  9.76856E-05 0.02784  2.77295E-02 0.00244 ];
INF_S4                    (idx, [1:   8]) = [ -9.73816E-03 0.00176 -5.90494E-04 0.00557 -1.13162E-06 1.00000 -8.81362E-03 0.00681 ];
INF_S5                    (idx, [1:   8]) = [  1.17104E-04 0.12486  3.13313E-05 0.08868 -3.94115E-05 0.04010  6.58968E-03 0.00903 ];
INF_S6                    (idx, [1:   8]) = [  5.23596E-03 0.00261 -1.37366E-04 0.01458 -5.23733E-05 0.03326 -1.68041E-02 0.00383 ];
INF_S7                    (idx, [1:   8]) = [  9.09862E-04 0.01743 -1.70226E-04 0.01443 -4.77557E-05 0.02529  5.39593E-04 0.09423 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11230E-01 0.00013  1.77171E-02 0.00039  1.16758E-03 0.00311  1.48067E+00 0.00043 ];
INF_SP1                   (idx, [1:   8]) = [  2.38887E-01 0.00024  5.15346E-03 0.00088  4.99658E-04 0.00710  3.92511E-01 0.00058 ];
INF_SP2                   (idx, [1:   8]) = [  9.78459E-02 0.00041 -1.57504E-03 0.00265  2.69588E-04 0.00963  9.28119E-02 0.00102 ];
INF_SP3                   (idx, [1:   8]) = [  9.16877E-03 0.00331 -1.83070E-03 0.00168  9.76856E-05 0.02784  2.77295E-02 0.00244 ];
INF_SP4                   (idx, [1:   8]) = [ -9.73816E-03 0.00176 -5.90494E-04 0.00557 -1.13162E-06 1.00000 -8.81362E-03 0.00681 ];
INF_SP5                   (idx, [1:   8]) = [  1.17155E-04 0.12485  3.13313E-05 0.08868 -3.94115E-05 0.04010  6.58968E-03 0.00903 ];
INF_SP6                   (idx, [1:   8]) = [  5.23594E-03 0.00261 -1.37366E-04 0.01458 -5.23733E-05 0.03326 -1.68041E-02 0.00383 ];
INF_SP7                   (idx, [1:   8]) = [  9.09869E-04 0.01738 -1.70226E-04 0.01443 -4.77557E-05 0.02529  5.39593E-04 0.09423 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32143E-01 0.00053  1.16403E+00 0.00650 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33463E-01 0.00096  1.25663E+00 0.00741 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33843E-01 0.00067  1.26043E+00 0.00800 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29187E-01 0.00088  1.01310E+00 0.00750 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43591E+00 0.00053  2.86645E-01 0.00632 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42781E+00 0.00096  2.65611E-01 0.00743 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42547E+00 0.00067  2.64863E-01 0.00793 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45444E+00 0.00088  3.29460E-01 0.00737 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.74469E-03 0.00926  1.66968E-04 0.05201  9.96109E-04 0.01986  9.29658E-04 0.02230  2.54151E-03 0.01318  8.56270E-04 0.02322  2.54177E-04 0.04002 ];
LAMBDA                    (idx, [1:  14]) = [  7.55583E-01 0.02048  1.25057E-02 0.00045  3.12150E-02 0.00056  1.10266E-01 0.00056  3.21805E-01 0.00041  1.33139E+00 0.00109  8.80683E+00 0.00662 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0004' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:47:07 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:04:29 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292427 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00926E+00  9.97271E-01  1.00486E+00  1.00206E+00  9.86544E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13025E-02 0.00107  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88698E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05282E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05709E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67170E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.52507E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.52419E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.49777E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.11833E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000858 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00017E+04 0.00087 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00017E+04 0.00087 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.84499E+02 ;
RUNNING_TIME              (idx, 1)        =  7.73614E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.83567E-01  4.83567E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.13533E-01  9.66667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.59878E+01  3.35490E+00  2.61533E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  6.69233E-01  2.75667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.04667E-02  8.16667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.73613E+01  1.25554E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97017 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99999E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79367E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.41012E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.77350E+04 ;
TOT_SF_RATE               (idx, 1)        =  5.05776E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.77645E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.24002E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.63364E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.64947E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.27113E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.46263E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  6.39722E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.50313E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.31406E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.71232E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.10843E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.31440E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.39295E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.44953E+14 ;
CS134_ACTIVITY            (idx, 1)        =  7.84540E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.34833E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.20078E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.40062E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  7.12037E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.25171E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.61536E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 12 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E+01  1.00009E+01 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.89554E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  8.30521E+15 0.00083  6.04614E-01 0.00058 ];
U238_FISS                 (idx, [1:   4]) = [  1.00370E+15 0.00273  7.30581E-02 0.00256 ];
PU239_FISS                (idx, [1:   4]) = [  4.04914E+15 0.00125  2.94777E-01 0.00112 ];
PU240_FISS                (idx, [1:   4]) = [  3.78227E+12 0.04459  2.75375E-04 0.04461 ];
PU241_FISS                (idx, [1:   4]) = [  3.65706E+14 0.00446  2.66207E-02 0.00439 ];
U235_CAPT                 (idx, [1:   4]) = [  1.84432E+15 0.00209  8.32514E-02 0.00201 ];
U238_CAPT                 (idx, [1:   4]) = [  8.79878E+15 0.00107  3.97143E-01 0.00073 ];
PU239_CAPT                (idx, [1:   4]) = [  2.24991E+15 0.00177  1.01563E-01 0.00172 ];
PU240_CAPT                (idx, [1:   4]) = [  1.13841E+15 0.00249  5.13852E-02 0.00239 ];
PU241_CAPT                (idx, [1:   4]) = [  1.31347E+14 0.00730  5.92812E-03 0.00724 ];
XE135_CAPT                (idx, [1:   4]) = [  7.42953E+14 0.00320  3.35366E-02 0.00315 ];
SM149_CAPT                (idx, [1:   4]) = [  1.96850E+14 0.00582  8.88628E-03 0.00582 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000858 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.74535E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000858 5.00775E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3059721 3.06387E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1897105 1.89984E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44032 4.40384E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000858 5.00775E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -2.98023E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.57898E+16 2.0E-05  3.57898E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37355E+16 3.7E-06  1.37355E+16 3.7E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.21543E+16 0.00046  1.64862E+16 0.00045  5.66805E+15 0.00114 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.58898E+16 0.00028  3.02218E+16 0.00025  5.66805E+15 0.00114 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.61536E+16 0.00052  3.61536E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.70470E+18 0.00050  4.54191E+17 0.00048  1.25050E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.18466E+14 0.00496 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.62083E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.27592E+18 0.00064 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11351E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11351E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.58236E+00 0.00052 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.82246E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.50727E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24014E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94292E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96883E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.98820E-01 0.00057 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.90023E-01 0.00057 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.60563E+00 2.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04509E+02 3.7E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.89908E-01 0.00059  9.84440E-01 0.00057  5.58277E-03 0.00952 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.90031E-01 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  9.90069E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.90031E-01 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  9.98828E-01 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72264E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72255E+01 9.1E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.62554E-07 0.00375 ];
IMP_EALF                  (idx, [1:   2]) = [  6.61243E-07 0.00156 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.48520E-01 0.00285 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.48854E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.81418E-03 0.00607  1.58316E-04 0.03562  1.00809E-03 0.01457  9.02378E-04 0.01519  2.57887E-03 0.00866  8.94372E-04 0.01485  2.72158E-04 0.02869 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.85184E-01 0.01475  1.01786E-02 0.02140  3.11532E-02 0.00040  1.10356E-01 0.00043  3.21939E-01 0.00030  1.32420E+00 0.00120  8.09314E+00 0.01490 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.63070E-03 0.00867  1.51815E-04 0.05414  9.66415E-04 0.02217  8.56612E-04 0.02256  2.52591E-03 0.01302  8.65356E-04 0.02269  2.64586E-04 0.03982 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.89763E-01 0.02122  1.24986E-02 0.00034  3.11546E-02 0.00063  1.10356E-01 0.00061  3.21894E-01 0.00044  1.32422E+00 0.00175  8.87163E+00 0.00607 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.59294E-05 0.00129  2.59190E-05 0.00129  2.79027E-05 0.01471 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.56634E-05 0.00116  2.56532E-05 0.00116  2.76109E-05 0.01464 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.64950E-03 0.00976  1.44038E-04 0.06157  9.67215E-04 0.02519  8.70852E-04 0.02477  2.54593E-03 0.01504  8.67636E-04 0.02505  2.53827E-04 0.04451 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.77682E-01 0.02435  1.25045E-02 0.00065  3.11445E-02 0.00075  1.10407E-01 0.00076  3.21908E-01 0.00052  1.32932E+00 0.00158  8.88864E+00 0.00750 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.54580E-05 0.00303  2.54507E-05 0.00303  2.68405E-05 0.03630 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.51967E-05 0.00297  2.51895E-05 0.00296  2.65446E-05 0.03618 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.69266E-03 0.03175  1.35161E-04 0.21697  9.12241E-04 0.07639  9.06063E-04 0.09057  2.61512E-03 0.04478  8.64608E-04 0.08753  2.59466E-04 0.15079 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.08946E-01 0.07780  1.26011E-02 0.00475  3.10499E-02 0.00179  1.10779E-01 0.00191  3.22528E-01 0.00139  1.32365E+00 0.00490  8.86048E+00 0.01738 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.72600E-03 0.03113  1.28601E-04 0.21819  9.26287E-04 0.07369  8.99171E-04 0.09078  2.64146E-03 0.04406  8.75512E-04 0.08436  2.54972E-04 0.14640 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.06859E-01 0.07723  1.26031E-02 0.00476  3.10484E-02 0.00178  1.10772E-01 0.00190  3.22559E-01 0.00138  1.32334E+00 0.00496  8.86500E+00 0.01739 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.24224E+02 0.03211 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.57230E-05 0.00083 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.54592E-05 0.00062 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.67807E-03 0.00558 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.20790E+02 0.00560 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.80172E-07 0.00072 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.75301E-06 0.00043  2.75277E-06 0.00044  2.79541E-06 0.00564 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.89265E-05 0.00088  3.89449E-05 0.00088  3.58611E-05 0.01030 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.48535E-01 0.00033  6.48470E-01 0.00034  6.71731E-01 0.00929 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06606E+01 0.01420 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.52419E+01 0.00048  3.36913E+01 0.00049 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.73846E+04 0.00289  3.00086E+05 0.00132  6.09122E+05 0.00079  6.50735E+05 0.00086  5.98238E+05 0.00056  6.42073E+05 0.00064  4.36478E+05 0.00064  3.85521E+05 0.00062  2.95381E+05 0.00059  2.40728E+05 0.00074  2.07685E+05 0.00075  1.87380E+05 0.00092  1.72978E+05 0.00085  1.64298E+05 0.00088  1.60204E+05 0.00088  1.38484E+05 0.00101  1.36620E+05 0.00091  1.35528E+05 0.00106  1.33184E+05 0.00108  2.59944E+05 0.00078  2.51072E+05 0.00076  1.81052E+05 0.00075  1.17173E+05 0.00094  1.35573E+05 0.00114  1.28504E+05 0.00075  1.15411E+05 0.00086  1.88244E+05 0.00073  4.32940E+04 0.00174  5.43473E+04 0.00129  4.93346E+04 0.00133  2.86953E+04 0.00173  4.96885E+04 0.00150  3.35276E+04 0.00152  2.79063E+04 0.00160  5.05494E+03 0.00349  4.56778E+03 0.00277  4.14781E+03 0.00315  3.96095E+03 0.00372  4.04093E+03 0.00478  4.42902E+03 0.00301  5.02510E+03 0.00381  4.93109E+03 0.00290  9.59379E+03 0.00225  1.55531E+04 0.00180  1.99882E+04 0.00249  5.32441E+04 0.00172  5.59233E+04 0.00168  5.98727E+04 0.00121  3.92714E+04 0.00115  2.80396E+04 0.00138  2.09436E+04 0.00180  2.47022E+04 0.00124  4.81258E+04 0.00142  6.74385E+04 0.00144  1.33285E+05 0.00113  2.09495E+05 0.00110  3.16258E+05 0.00120  2.03264E+05 0.00111  1.45837E+05 0.00138  1.05116E+05 0.00142  9.44578E+04 0.00133  9.32609E+04 0.00130  7.79839E+04 0.00147  5.27527E+04 0.00151  4.87564E+04 0.00131  4.33761E+04 0.00151  3.66926E+04 0.00156  2.89378E+04 0.00203  1.93980E+04 0.00161  6.88509E+03 0.00190 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.98866E-01 0.00052 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.35041E+18 0.00049  3.54318E+17 0.00101 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38269E-01 9.6E-05  1.54961E+00 0.00030 ];
INF_CAPT                  (idx, [1:   4]) = [  7.36766E-03 0.00067  3.44495E-02 0.00051 ];
INF_ABS                   (idx, [1:   4]) = [  9.31151E-03 0.00056  6.58128E-02 0.00074 ];
INF_FISS                  (idx, [1:   4]) = [  1.94385E-03 0.00047  3.13633E-02 0.00100 ];
INF_NSF                   (idx, [1:   4]) = [  5.13030E-03 0.00049  8.14722E-02 0.00102 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.63924E+00 5.1E-05  2.59769E+00 3.8E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04835E+02 6.6E-06  2.04432E+02 6.6E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.71156E-08 0.00034  2.57383E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28959E-01 9.8E-05  1.48380E+00 0.00035 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44058E-01 0.00014  3.93709E-01 0.00040 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62693E-02 0.00026  9.31740E-02 0.00101 ];
INF_SCATT3                (idx, [1:   4]) = [  7.33869E-03 0.00296  2.79580E-02 0.00208 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03250E-02 0.00192 -8.66309E-03 0.00588 ];
INF_SCATT5                (idx, [1:   4]) = [  1.48423E-04 0.11145  6.60003E-03 0.00668 ];
INF_SCATT6                (idx, [1:   4]) = [  5.09739E-03 0.00360 -1.69005E-02 0.00303 ];
INF_SCATT7                (idx, [1:   4]) = [  7.42316E-04 0.02435  4.26477E-04 0.09958 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29000E-01 9.8E-05  1.48380E+00 0.00035 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44058E-01 0.00014  3.93709E-01 0.00040 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62696E-02 0.00026  9.31740E-02 0.00101 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.33850E-03 0.00296  2.79580E-02 0.00208 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03247E-02 0.00192 -8.66309E-03 0.00588 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.48486E-04 0.11107  6.60003E-03 0.00668 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.09741E-03 0.00359 -1.69005E-02 0.00303 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.42273E-04 0.02439  4.26477E-04 0.09958 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12668E-01 0.00024  1.00363E+00 0.00028 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56739E+00 0.00024  3.32129E-01 0.00028 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.27004E-03 0.00059  6.58128E-02 0.00074 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69674E-02 0.00013  6.69829E-02 0.00100 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11301E-01 9.8E-05  1.76574E-02 0.00032  1.16802E-03 0.00472  1.48263E+00 0.00036 ];
INF_S1                    (idx, [1:   8]) = [  2.38927E-01 0.00014  5.13068E-03 0.00080  5.00484E-04 0.00733  3.93209E-01 0.00040 ];
INF_S2                    (idx, [1:   8]) = [  9.78374E-02 0.00026 -1.56815E-03 0.00211  2.67832E-04 0.00993  9.29061E-02 0.00101 ];
INF_S3                    (idx, [1:   8]) = [  9.16478E-03 0.00234 -1.82610E-03 0.00189  9.60981E-05 0.02455  2.78619E-02 0.00206 ];
INF_S4                    (idx, [1:   8]) = [ -9.73925E-03 0.00192 -5.85707E-04 0.00458  1.00346E-06 1.00000 -8.66409E-03 0.00581 ];
INF_S5                    (idx, [1:   8]) = [  1.17170E-04 0.13848  3.12524E-05 0.09292 -3.77499E-05 0.03336  6.63778E-03 0.00664 ];
INF_S6                    (idx, [1:   8]) = [  5.23695E-03 0.00328 -1.39561E-04 0.01659 -4.80163E-05 0.03310 -1.68525E-02 0.00299 ];
INF_S7                    (idx, [1:   8]) = [  9.12210E-04 0.01882 -1.69895E-04 0.01168 -4.41027E-05 0.03130  4.70580E-04 0.09030 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11343E-01 9.8E-05  1.76574E-02 0.00032  1.16802E-03 0.00472  1.48263E+00 0.00036 ];
INF_SP1                   (idx, [1:   8]) = [  2.38928E-01 0.00014  5.13068E-03 0.00080  5.00484E-04 0.00733  3.93209E-01 0.00040 ];
INF_SP2                   (idx, [1:   8]) = [  9.78377E-02 0.00026 -1.56815E-03 0.00211  2.67832E-04 0.00993  9.29061E-02 0.00101 ];
INF_SP3                   (idx, [1:   8]) = [  9.16459E-03 0.00234 -1.82610E-03 0.00189  9.60981E-05 0.02455  2.78619E-02 0.00206 ];
INF_SP4                   (idx, [1:   8]) = [ -9.73904E-03 0.00192 -5.85707E-04 0.00458  1.00346E-06 1.00000 -8.66409E-03 0.00581 ];
INF_SP5                   (idx, [1:   8]) = [  1.17234E-04 0.13803  3.12524E-05 0.09292 -3.77499E-05 0.03336  6.63778E-03 0.00664 ];
INF_SP6                   (idx, [1:   8]) = [  5.23697E-03 0.00327 -1.39561E-04 0.01659 -4.80163E-05 0.03310 -1.68525E-02 0.00299 ];
INF_SP7                   (idx, [1:   8]) = [  9.12167E-04 0.01886 -1.69895E-04 0.01168 -4.41027E-05 0.03130  4.70580E-04 0.09030 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31808E-01 0.00050  1.17522E+00 0.00691 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33423E-01 0.00093  1.27353E+00 0.00854 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33473E-01 0.00099  1.26513E+00 0.00853 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28609E-01 0.00096  1.02478E+00 0.00777 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43798E+00 0.00050  2.83968E-01 0.00706 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42805E+00 0.00092  2.62201E-01 0.00861 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42775E+00 0.00099  2.63932E-01 0.00842 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45813E+00 0.00096  3.25769E-01 0.00817 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.63070E-03 0.00867  1.51815E-04 0.05414  9.66415E-04 0.02217  8.56612E-04 0.02256  2.52591E-03 0.01302  8.65356E-04 0.02269  2.64586E-04 0.03982 ];
LAMBDA                    (idx, [1:  14]) = [  7.89763E-01 0.02122  1.24986E-02 0.00034  3.11546E-02 0.00063  1.10356E-01 0.00061  3.21894E-01 0.00044  1.32422E+00 0.00175  8.87163E+00 0.00607 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0004' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:47:07 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:10:32 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292427 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00968E+00  9.97883E-01  1.00472E+00  1.00125E+00  9.86464E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 9.3E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14218E-02 0.00111  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88578E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05689E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06116E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67287E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.51420E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.51331E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.44943E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.15527E-01 0.00111  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001122 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00022E+04 0.00091 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00022E+04 0.00091 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.14723E+02 ;
RUNNING_TIME              (idx, 1)        =  8.34128E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.83567E-01  4.83567E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.33683E-01  9.85000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  8.19638E+01  3.35492E+00  2.62107E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  7.23850E-01  2.69500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.39167E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  8.34127E+01  1.25760E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97194 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00137E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79800E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.42534E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.74807E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.08518E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.81232E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.26540E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61299E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.62150E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.57639E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.54304E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  8.98059E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.71128E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.78328E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.77191E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.34642E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.32491E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.42630E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.47219E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.21238E+13 ;
CS137_ACTIVITY            (idx, 1)        =  1.68413E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.21314E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.35980E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.41576E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.26110E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.72888E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 13 ;
BURNUP                     (idx, [1:  2])  = [  1.25000E+01  1.25011E+01 ];
BURN_DAYS                 (idx, 1)        =  3.12500E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.15104E-01 0.00104 ];
U235_FISS                 (idx, [1:   4]) = [  7.58021E+15 0.00094  5.53256E-01 0.00069 ];
U238_FISS                 (idx, [1:   4]) = [  1.03159E+15 0.00273  7.52844E-02 0.00257 ];
PU239_FISS                (idx, [1:   4]) = [  4.53625E+15 0.00126  3.31093E-01 0.00112 ];
PU240_FISS                (idx, [1:   4]) = [  5.75712E+12 0.03610  4.19975E-04 0.03603 ];
PU241_FISS                (idx, [1:   4]) = [  5.35822E+14 0.00395  3.91054E-02 0.00386 ];
U235_CAPT                 (idx, [1:   4]) = [  1.68707E+15 0.00218  7.23577E-02 0.00205 ];
U238_CAPT                 (idx, [1:   4]) = [  9.02856E+15 0.00103  3.87236E-01 0.00074 ];
PU239_CAPT                (idx, [1:   4]) = [  2.52090E+15 0.00168  1.08128E-01 0.00160 ];
PU240_CAPT                (idx, [1:   4]) = [  1.40682E+15 0.00231  6.03376E-02 0.00219 ];
PU241_CAPT                (idx, [1:   4]) = [  1.94150E+14 0.00585  8.32778E-03 0.00583 ];
XE135_CAPT                (idx, [1:   4]) = [  7.52458E+14 0.00323  3.22776E-02 0.00324 ];
SM149_CAPT                (idx, [1:   4]) = [  2.07503E+14 0.00604  8.90040E-03 0.00602 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001122 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.96801E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001122 5.00797E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3121897 3.12624E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1834778 1.83727E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44447 4.44535E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001122 5.00797E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.65661E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.60508E+16 2.1E-05  3.60508E+16 2.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37134E+16 4.1E-06  1.37134E+16 4.1E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.33018E+16 0.00045  1.74888E+16 0.00047  5.81305E+15 0.00118 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.70153E+16 0.00028  3.12022E+16 0.00026  5.81305E+15 0.00118 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.72888E+16 0.00052  3.72888E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.75396E+18 0.00050  4.66631E+17 0.00048  1.28732E+18 0.00056 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.31558E+14 0.00492 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.73468E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.31197E+18 0.00066 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11061E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11061E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.55887E+00 0.00053 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.81611E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.45634E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23889E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94227E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96864E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.74519E-01 0.00062 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.65855E-01 0.00063 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.62886E+00 2.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04838E+02 4.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.65904E-01 0.00063  9.60574E-01 0.00063  5.28137E-03 0.01053 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.66857E-01 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  9.66929E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.66857E-01 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  9.75530E-01 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71808E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71808E+01 9.0E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.93473E-07 0.00375 ];
IMP_EALF                  (idx, [1:   2]) = [  6.91427E-07 0.00154 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.55827E-01 0.00278 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.56482E-01 0.00112 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.81799E-03 0.00665  1.42216E-04 0.03803  9.84985E-04 0.01491  9.17788E-04 0.01575  2.58422E-03 0.00910  9.11489E-04 0.01556  2.77284E-04 0.02822 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.89949E-01 0.01423  9.41290E-03 0.02571  3.10218E-02 0.00205  1.10513E-01 0.00052  3.22303E-01 0.00030  1.31672E+00 0.00127  8.12986E+00 0.01357 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.54288E-03 0.00942  1.33714E-04 0.05524  9.44841E-04 0.02232  8.46089E-04 0.02364  2.49377E-03 0.01375  8.68492E-04 0.02226  2.55977E-04 0.04274 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.78198E-01 0.02109  1.25163E-02 0.00065  3.10955E-02 0.00062  1.10521E-01 0.00070  3.22195E-01 0.00044  1.31539E+00 0.00191  8.77924E+00 0.00637 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.64434E-05 0.00137  2.64335E-05 0.00138  2.81425E-05 0.01636 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.55376E-05 0.00127  2.55280E-05 0.00128  2.71804E-05 0.01637 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.46226E-03 0.01074  1.38774E-04 0.06139  9.21000E-04 0.02479  8.56195E-04 0.02650  2.44196E-03 0.01584  8.45595E-04 0.02613  2.58736E-04 0.05021 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.92224E-01 0.02744  1.25140E-02 0.00080  3.10740E-02 0.00081  1.10476E-01 0.00083  3.22323E-01 0.00050  1.31401E+00 0.00261  8.78727E+00 0.00896 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.60126E-05 0.00317  2.60012E-05 0.00319  2.68943E-05 0.03982 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.51201E-05 0.00309  2.51092E-05 0.00311  2.59523E-05 0.03988 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.29706E-03 0.03298  1.55235E-04 0.20355  8.55286E-04 0.07878  7.33060E-04 0.08613  2.42929E-03 0.04968  8.64859E-04 0.08323  2.59335E-04 0.14711 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.52249E-01 0.07921  1.24889E-02 4.8E-05  3.10346E-02 0.00195  1.10545E-01 0.00197  3.22733E-01 0.00153  1.31720E+00 0.00528  8.77384E+00 0.02006 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.35091E-03 0.03251  1.55229E-04 0.19510  8.62873E-04 0.07839  7.45228E-04 0.08314  2.44084E-03 0.04948  8.72966E-04 0.07992  2.73774E-04 0.14417 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.64036E-01 0.07822  1.24889E-02 4.8E-05  3.10355E-02 0.00195  1.10590E-01 0.00198  3.22732E-01 0.00152  1.31706E+00 0.00525  8.76508E+00 0.02025 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.04902E+02 0.03343 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.62304E-05 0.00090 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.53310E-05 0.00065 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.45286E-03 0.00679 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.08014E+02 0.00692 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.76692E-07 0.00074 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.72972E-06 0.00043  2.72955E-06 0.00043  2.75480E-06 0.00556 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.88948E-05 0.00089  3.89112E-05 0.00090  3.59393E-05 0.01170 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.43449E-01 0.00035  6.43498E-01 0.00035  6.50546E-01 0.01042 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02801E+01 0.01343 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.51331E+01 0.00049  3.35768E+01 0.00050 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.77750E+04 0.00331  3.01063E+05 0.00191  6.09390E+05 0.00087  6.50662E+05 0.00069  5.99075E+05 0.00085  6.42015E+05 0.00054  4.36004E+05 0.00062  3.85468E+05 0.00069  2.95235E+05 0.00081  2.41017E+05 0.00077  2.07797E+05 0.00085  1.87280E+05 0.00085  1.72951E+05 0.00099  1.64485E+05 0.00068  1.60072E+05 0.00068  1.38343E+05 0.00095  1.36325E+05 0.00096  1.35456E+05 0.00112  1.33255E+05 0.00106  2.59815E+05 0.00071  2.51004E+05 0.00063  1.81133E+05 0.00080  1.17351E+05 0.00097  1.35769E+05 0.00063  1.28744E+05 0.00079  1.15053E+05 0.00091  1.87704E+05 0.00070  4.32744E+04 0.00160  5.41961E+04 0.00156  4.92401E+04 0.00140  2.86600E+04 0.00128  4.94972E+04 0.00141  3.34337E+04 0.00165  2.76604E+04 0.00161  4.88669E+03 0.00359  4.35878E+03 0.00395  3.89856E+03 0.00386  3.71421E+03 0.00371  3.81104E+03 0.00285  4.21995E+03 0.00422  4.81774E+03 0.00281  4.80739E+03 0.00318  9.36066E+03 0.00219  1.52193E+04 0.00176  1.96937E+04 0.00255  5.25741E+04 0.00170  5.52484E+04 0.00162  5.91140E+04 0.00138  3.86448E+04 0.00152  2.74290E+04 0.00203  2.04084E+04 0.00205  2.41258E+04 0.00138  4.70527E+04 0.00130  6.63197E+04 0.00150  1.31480E+05 0.00144  2.07053E+05 0.00163  3.12823E+05 0.00158  2.01466E+05 0.00175  1.44917E+05 0.00181  1.04331E+05 0.00199  9.38869E+04 0.00193  9.26631E+04 0.00206  7.73616E+04 0.00201  5.23726E+04 0.00216  4.85586E+04 0.00203  4.31287E+04 0.00225  3.65846E+04 0.00220  2.88770E+04 0.00213  1.93078E+04 0.00229  6.86897E+03 0.00220 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.75603E-01 0.00061 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.39228E+18 0.00053  3.61703E+17 0.00149 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38483E-01 0.00013  1.55450E+00 0.00045 ];
INF_CAPT                  (idx, [1:   4]) = [  7.57387E-03 0.00075  3.52728E-02 0.00077 ];
INF_ABS                   (idx, [1:   4]) = [  9.45283E-03 0.00063  6.59601E-02 0.00108 ];
INF_FISS                  (idx, [1:   4]) = [  1.87896E-03 0.00045  3.06873E-02 0.00146 ];
INF_NSF                   (idx, [1:   4]) = [  4.98934E-03 0.00046  8.04811E-02 0.00149 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.65537E+00 5.2E-05  2.62261E+00 4.9E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05057E+02 7.4E-06  2.04787E+02 8.8E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.67306E-08 0.00033  2.57827E-06 0.00020 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29034E-01 0.00013  1.48853E+00 0.00052 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44122E-01 0.00020  3.94542E-01 0.00066 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63161E-02 0.00031  9.32883E-02 0.00101 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32435E-03 0.00267  2.80071E-02 0.00178 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03192E-02 0.00215 -8.79996E-03 0.00835 ];
INF_SCATT5                (idx, [1:   4]) = [  1.61883E-04 0.12298  6.68008E-03 0.00843 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10985E-03 0.00337 -1.70120E-02 0.00342 ];
INF_SCATT7                (idx, [1:   4]) = [  7.52938E-04 0.02240  4.70882E-04 0.10350 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29077E-01 0.00013  1.48853E+00 0.00052 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44122E-01 0.00020  3.94542E-01 0.00066 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63162E-02 0.00031  9.32883E-02 0.00101 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32445E-03 0.00267  2.80071E-02 0.00178 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03195E-02 0.00215 -8.79996E-03 0.00835 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.61885E-04 0.12279  6.68008E-03 0.00843 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10973E-03 0.00337 -1.70120E-02 0.00342 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.52680E-04 0.02244  4.70882E-04 0.10350 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12635E-01 0.00036  1.00794E+00 0.00040 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56763E+00 0.00036  3.30708E-01 0.00040 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.41015E-03 0.00063  6.59601E-02 0.00108 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69715E-02 0.00021  6.71289E-02 0.00124 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11511E-01 0.00013  1.75224E-02 0.00048  1.15829E-03 0.00370  1.48737E+00 0.00052 ];
INF_S1                    (idx, [1:   8]) = [  2.39032E-01 0.00020  5.09008E-03 0.00089  4.95344E-04 0.00644  3.94047E-01 0.00066 ];
INF_S2                    (idx, [1:   8]) = [  9.78808E-02 0.00033 -1.56468E-03 0.00206  2.70368E-04 0.00774  9.30180E-02 0.00102 ];
INF_S3                    (idx, [1:   8]) = [  9.13868E-03 0.00204 -1.81433E-03 0.00198  9.77010E-05 0.01468  2.79094E-02 0.00178 ];
INF_S4                    (idx, [1:   8]) = [ -9.74068E-03 0.00224 -5.78502E-04 0.00492  3.09940E-06 0.48708 -8.80306E-03 0.00835 ];
INF_S5                    (idx, [1:   8]) = [  1.27737E-04 0.15530  3.41466E-05 0.10055 -3.61247E-05 0.03756  6.71620E-03 0.00835 ];
INF_S6                    (idx, [1:   8]) = [  5.24540E-03 0.00323 -1.35545E-04 0.01283 -4.72355E-05 0.03460 -1.69648E-02 0.00339 ];
INF_S7                    (idx, [1:   8]) = [  9.24805E-04 0.01820 -1.71866E-04 0.01189 -4.59606E-05 0.02757  5.16842E-04 0.09355 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11554E-01 0.00013  1.75224E-02 0.00048  1.15829E-03 0.00370  1.48737E+00 0.00052 ];
INF_SP1                   (idx, [1:   8]) = [  2.39032E-01 0.00020  5.09008E-03 0.00089  4.95344E-04 0.00644  3.94047E-01 0.00066 ];
INF_SP2                   (idx, [1:   8]) = [  9.78809E-02 0.00033 -1.56468E-03 0.00206  2.70368E-04 0.00774  9.30180E-02 0.00102 ];
INF_SP3                   (idx, [1:   8]) = [  9.13878E-03 0.00204 -1.81433E-03 0.00198  9.77010E-05 0.01468  2.79094E-02 0.00178 ];
INF_SP4                   (idx, [1:   8]) = [ -9.74096E-03 0.00224 -5.78502E-04 0.00492  3.09940E-06 0.48708 -8.80306E-03 0.00835 ];
INF_SP5                   (idx, [1:   8]) = [  1.27738E-04 0.15509  3.41466E-05 0.10055 -3.61247E-05 0.03756  6.71620E-03 0.00835 ];
INF_SP6                   (idx, [1:   8]) = [  5.24527E-03 0.00323 -1.35545E-04 0.01283 -4.72355E-05 0.03460 -1.69648E-02 0.00339 ];
INF_SP7                   (idx, [1:   8]) = [  9.24546E-04 0.01823 -1.71866E-04 0.01189 -4.59606E-05 0.02757  5.16842E-04 0.09355 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32053E-01 0.00065  1.18616E+00 0.00780 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33670E-01 0.00084  1.27256E+00 0.00877 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33965E-01 0.00098  1.29830E+00 0.00959 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28612E-01 0.00109  1.02854E+00 0.00775 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43647E+00 0.00065  2.81433E-01 0.00788 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42653E+00 0.00084  2.62431E-01 0.00892 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42475E+00 0.00098  2.57313E-01 0.00960 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45812E+00 0.00109  3.24553E-01 0.00776 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.54288E-03 0.00942  1.33714E-04 0.05524  9.44841E-04 0.02232  8.46089E-04 0.02364  2.49377E-03 0.01375  8.68492E-04 0.02226  2.55977E-04 0.04274 ];
LAMBDA                    (idx, [1:  14]) = [  7.78198E-01 0.02109  1.25163E-02 0.00065  3.10955E-02 0.00062  1.10521E-01 0.00070  3.22195E-01 0.00044  1.31539E+00 0.00191  8.77924E+00 0.00637 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0004' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:47:07 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:16:35 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292427 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00899E+00  9.97017E-01  1.00567E+00  1.00217E+00  9.86149E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15153E-02 0.00103  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88485E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05836E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06265E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67436E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.50557E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.50466E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.42058E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.18133E-01 0.00109  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000756 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00015E+04 0.00092 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00015E+04 0.00092 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.44938E+02 ;
RUNNING_TIME              (idx, 1)        =  8.94624E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.83567E-01  4.83567E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.54117E-01  1.01167E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  8.79363E+01  3.34158E+00  2.63083E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  7.79900E-01  2.72667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.72500E-02  7.83332E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  8.94622E+01  1.25825E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97346 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99972E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80177E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.47736E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.73587E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.08758E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.86355E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.30186E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61378E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.60566E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.92083E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.62822E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.20177E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.98996E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.19059E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.82922E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.57256E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.34947E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.46230E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.50721E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.72124E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.01905E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.26125E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.33029E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.48699E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.27613E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.83890E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 14 ;
BURNUP                     (idx, [1:  2])  = [  1.50000E+01  1.50014E+01 ];
BURN_DAYS                 (idx, 1)        =  3.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.38727E-01 0.00103 ];
U235_FISS                 (idx, [1:   4]) = [  6.92586E+15 0.00101  5.05873E-01 0.00077 ];
U238_FISS                 (idx, [1:   4]) = [  1.06467E+15 0.00259  7.77566E-02 0.00242 ];
PU239_FISS                (idx, [1:   4]) = [  4.97012E+15 0.00117  3.63025E-01 0.00098 ];
PU240_FISS                (idx, [1:   4]) = [  6.82055E+12 0.03461  4.97854E-04 0.03463 ];
PU241_FISS                (idx, [1:   4]) = [  7.10246E+14 0.00316  5.18794E-02 0.00312 ];
U235_CAPT                 (idx, [1:   4]) = [  1.54804E+15 0.00227  6.34188E-02 0.00221 ];
U238_CAPT                 (idx, [1:   4]) = [  9.24492E+15 0.00100  3.78717E-01 0.00072 ];
PU239_CAPT                (idx, [1:   4]) = [  2.75020E+15 0.00168  1.12668E-01 0.00160 ];
PU240_CAPT                (idx, [1:   4]) = [  1.65724E+15 0.00220  6.78888E-02 0.00210 ];
PU241_CAPT                (idx, [1:   4]) = [  2.54378E+14 0.00545  1.04212E-02 0.00543 ];
XE135_CAPT                (idx, [1:   4]) = [  7.58886E+14 0.00332  3.10898E-02 0.00329 ];
SM149_CAPT                (idx, [1:   4]) = [  2.16970E+14 0.00590  8.89095E-03 0.00597 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000756 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.92100E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000756 5.00792E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3174648 3.17936E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1780828 1.78327E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45280 4.52907E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000756 5.00792E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.02914E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.62837E+16 2.3E-05  3.62837E+16 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36934E+16 4.2E-06  1.36934E+16 4.2E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.44145E+16 0.00045  1.84556E+16 0.00045  5.95891E+15 0.00114 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.81079E+16 0.00029  3.21490E+16 0.00026  5.95891E+15 0.00114 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.83890E+16 0.00049  3.83890E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.80194E+18 0.00047  4.78795E+17 0.00044  1.32315E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.47806E+14 0.00492 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.84557E+16 0.00030 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.34735E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10771E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10771E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.53904E+00 0.00057 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.81279E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.40732E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23803E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94075E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96848E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.53754E-01 0.00064 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.45115E-01 0.00064 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.64972E+00 2.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05138E+02 4.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.45223E-01 0.00065  9.40056E-01 0.00064  5.05864E-03 0.00983 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.45061E-01 0.00030 ];
COL_KEFF                  (idx, [1:   2]) = [  9.45270E-01 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.45061E-01 0.00030 ];
ABS_KINF                  (idx, [1:   2]) = [  9.53698E-01 0.00029 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71394E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71400E+01 8.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.22857E-07 0.00381 ];
IMP_EALF                  (idx, [1:   2]) = [  7.20260E-07 0.00151 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.64792E-01 0.00282 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.64135E-01 0.00119 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.84615E-03 0.00602  1.55915E-04 0.03822  1.02785E-03 0.01409  9.26750E-04 0.01506  2.55898E-03 0.00895  8.92535E-04 0.01591  2.84117E-04 0.02868 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.85155E-01 0.01500  9.59775E-03 0.02475  3.09801E-02 0.00043  1.10577E-01 0.00047  3.22329E-01 0.00030  1.30655E+00 0.00166  7.99986E+00 0.01513 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.37925E-03 0.00928  1.34921E-04 0.05672  9.46809E-04 0.02110  8.70324E-04 0.02349  2.33555E-03 0.01376  8.34851E-04 0.02343  2.56789E-04 0.04246 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.85960E-01 0.02268  1.25313E-02 0.00082  3.09688E-02 0.00063  1.10650E-01 0.00067  3.22552E-01 0.00049  1.30565E+00 0.00229  8.69319E+00 0.00849 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.68537E-05 0.00150  2.68441E-05 0.00150  2.86670E-05 0.01589 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.53763E-05 0.00129  2.53673E-05 0.00129  2.70928E-05 0.01589 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.35849E-03 0.00999  1.33013E-04 0.06559  9.13944E-04 0.02472  8.59542E-04 0.02649  2.34202E-03 0.01507  8.52313E-04 0.02676  2.57660E-04 0.04612 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.90960E-01 0.02547  1.25268E-02 0.00114  3.09451E-02 0.00085  1.10458E-01 0.00085  3.22479E-01 0.00054  1.30600E+00 0.00293  8.68860E+00 0.01123 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.63761E-05 0.00304  2.63744E-05 0.00303  2.56806E-05 0.03814 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.49248E-05 0.00293  2.49231E-05 0.00293  2.42591E-05 0.03802 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.22409E-03 0.03507  1.21745E-04 0.19800  1.04864E-03 0.08004  7.10779E-04 0.08940  2.31922E-03 0.05316  7.85090E-04 0.09084  2.38612E-04 0.14160 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.54448E-01 0.07518  1.24892E-02 5.4E-05  3.09160E-02 0.00188  1.10558E-01 0.00210  3.23498E-01 0.00151  1.31467E+00 0.00599  8.71552E+00 0.02423 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.23129E-03 0.03428  1.20793E-04 0.19386  1.04228E-03 0.07859  7.14197E-04 0.08914  2.32984E-03 0.05160  7.81300E-04 0.08942  2.42880E-04 0.14066 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.60462E-01 0.07391  1.24892E-02 5.4E-05  3.09167E-02 0.00188  1.10582E-01 0.00210  3.23505E-01 0.00149  1.31452E+00 0.00596  8.67811E+00 0.02456 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.98828E+02 0.03499 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.65931E-05 0.00088 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.51310E-05 0.00058 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.31305E-03 0.00720 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.99847E+02 0.00722 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.74298E-07 0.00072 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.71207E-06 0.00044  2.71204E-06 0.00044  2.71639E-06 0.00538 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.89502E-05 0.00088  3.89699E-05 0.00088  3.55633E-05 0.01071 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.38511E-01 0.00033  6.38629E-01 0.00034  6.30726E-01 0.00962 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06944E+01 0.01402 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.50466E+01 0.00048  3.34632E+01 0.00051 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.80337E+04 0.00322  3.01877E+05 0.00159  6.10285E+05 0.00087  6.51107E+05 0.00076  5.98686E+05 0.00084  6.41086E+05 0.00064  4.35164E+05 0.00066  3.85465E+05 0.00065  2.94419E+05 0.00076  2.40921E+05 0.00077  2.07707E+05 0.00087  1.87012E+05 0.00102  1.72754E+05 0.00074  1.64461E+05 0.00073  1.60186E+05 0.00093  1.38192E+05 0.00109  1.36783E+05 0.00083  1.35543E+05 0.00122  1.33139E+05 0.00066  2.60088E+05 0.00071  2.51112E+05 0.00077  1.81012E+05 0.00067  1.17297E+05 0.00089  1.35606E+05 0.00083  1.28736E+05 0.00061  1.15160E+05 0.00095  1.86995E+05 0.00073  4.31447E+04 0.00125  5.39304E+04 0.00124  4.90630E+04 0.00124  2.86007E+04 0.00137  4.94776E+04 0.00148  3.31174E+04 0.00203  2.72688E+04 0.00197  4.77991E+03 0.00465  4.19901E+03 0.00323  3.71958E+03 0.00341  3.54652E+03 0.00264  3.64182E+03 0.00303  3.99580E+03 0.00450  4.67832E+03 0.00365  4.70852E+03 0.00416  9.23386E+03 0.00301  1.50730E+04 0.00189  1.95401E+04 0.00237  5.20501E+04 0.00127  5.48996E+04 0.00137  5.85884E+04 0.00082  3.81929E+04 0.00158  2.69797E+04 0.00170  2.00929E+04 0.00196  2.36769E+04 0.00151  4.62763E+04 0.00128  6.53053E+04 0.00161  1.29889E+05 0.00137  2.05198E+05 0.00143  3.10543E+05 0.00134  2.00051E+05 0.00144  1.44114E+05 0.00155  1.03882E+05 0.00132  9.33731E+04 0.00163  9.21838E+04 0.00144  7.71484E+04 0.00164  5.22824E+04 0.00162  4.83504E+04 0.00162  4.30484E+04 0.00175  3.64278E+04 0.00168  2.87862E+04 0.00138  1.92532E+04 0.00193  6.83469E+03 0.00179 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.53911E-01 0.00043 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.43241E+18 0.00045  3.69566E+17 0.00124 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38681E-01 0.00015  1.55792E+00 0.00036 ];
INF_CAPT                  (idx, [1:   4]) = [  7.76547E-03 0.00058  3.59679E-02 0.00068 ];
INF_ABS                   (idx, [1:   4]) = [  9.58804E-03 0.00047  6.59617E-02 0.00092 ];
INF_FISS                  (idx, [1:   4]) = [  1.82257E-03 0.00041  2.99938E-02 0.00123 ];
INF_NSF                   (idx, [1:   4]) = [  4.86738E-03 0.00043  7.93277E-02 0.00125 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.67061E+00 7.4E-05  2.64480E+00 4.9E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05270E+02 7.9E-06  2.05107E+02 9.0E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.64157E-08 0.00040  2.58171E-06 0.00013 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29098E-01 0.00016  1.49197E+00 0.00041 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44149E-01 0.00029  3.95628E-01 0.00061 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63418E-02 0.00032  9.34247E-02 0.00106 ];
INF_SCATT3                (idx, [1:   4]) = [  7.30418E-03 0.00290  2.80219E-02 0.00275 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03542E-02 0.00220 -8.90959E-03 0.00626 ];
INF_SCATT5                (idx, [1:   4]) = [  1.49206E-04 0.13484  6.56543E-03 0.00815 ];
INF_SCATT6                (idx, [1:   4]) = [  5.09011E-03 0.00320 -1.70600E-02 0.00308 ];
INF_SCATT7                (idx, [1:   4]) = [  7.46598E-04 0.02091  4.03117E-04 0.11694 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29140E-01 0.00016  1.49197E+00 0.00041 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44150E-01 0.00029  3.95628E-01 0.00061 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63420E-02 0.00032  9.34247E-02 0.00106 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.30434E-03 0.00290  2.80219E-02 0.00275 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03541E-02 0.00220 -8.90959E-03 0.00626 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.49303E-04 0.13437  6.56543E-03 0.00815 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.09026E-03 0.00319 -1.70600E-02 0.00308 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.46582E-04 0.02098  4.03117E-04 0.11694 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12547E-01 0.00029  1.01049E+00 0.00029 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56828E+00 0.00029  3.29872E-01 0.00029 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.54558E-03 0.00048  6.59617E-02 0.00092 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69855E-02 0.00018  6.71241E-02 0.00099 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11696E-01 0.00015  1.74018E-02 0.00049  1.16870E-03 0.00491  1.49080E+00 0.00041 ];
INF_S1                    (idx, [1:   8]) = [  2.39108E-01 0.00029  5.04136E-03 0.00099  5.04976E-04 0.00825  3.95123E-01 0.00061 ];
INF_S2                    (idx, [1:   8]) = [  9.79019E-02 0.00032 -1.56011E-03 0.00285  2.75263E-04 0.01018  9.31495E-02 0.00107 ];
INF_S3                    (idx, [1:   8]) = [  9.09825E-03 0.00230 -1.79406E-03 0.00203  9.95960E-05 0.02287  2.79223E-02 0.00273 ];
INF_S4                    (idx, [1:   8]) = [ -9.78561E-03 0.00231 -5.68630E-04 0.00486 -1.44295E-06 1.00000 -8.90815E-03 0.00625 ];
INF_S5                    (idx, [1:   8]) = [  1.12721E-04 0.17200  3.64855E-05 0.07952 -4.03355E-05 0.03161  6.60577E-03 0.00813 ];
INF_S6                    (idx, [1:   8]) = [  5.22474E-03 0.00301 -1.34635E-04 0.01480 -5.22627E-05 0.02634 -1.70077E-02 0.00311 ];
INF_S7                    (idx, [1:   8]) = [  9.15907E-04 0.01716 -1.69310E-04 0.01405 -4.59987E-05 0.03155  4.49116E-04 0.10385 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11738E-01 0.00015  1.74018E-02 0.00049  1.16870E-03 0.00491  1.49080E+00 0.00041 ];
INF_SP1                   (idx, [1:   8]) = [  2.39108E-01 0.00029  5.04136E-03 0.00099  5.04976E-04 0.00825  3.95123E-01 0.00061 ];
INF_SP2                   (idx, [1:   8]) = [  9.79021E-02 0.00032 -1.56011E-03 0.00285  2.75263E-04 0.01018  9.31495E-02 0.00107 ];
INF_SP3                   (idx, [1:   8]) = [  9.09841E-03 0.00230 -1.79406E-03 0.00203  9.95960E-05 0.02287  2.79223E-02 0.00273 ];
INF_SP4                   (idx, [1:   8]) = [ -9.78551E-03 0.00231 -5.68630E-04 0.00486 -1.44295E-06 1.00000 -8.90815E-03 0.00625 ];
INF_SP5                   (idx, [1:   8]) = [  1.12817E-04 0.17135  3.64855E-05 0.07952 -4.03355E-05 0.03161  6.60577E-03 0.00813 ];
INF_SP6                   (idx, [1:   8]) = [  5.22489E-03 0.00301 -1.34635E-04 0.01480 -5.22627E-05 0.02634 -1.70077E-02 0.00311 ];
INF_SP7                   (idx, [1:   8]) = [  9.15892E-04 0.01722 -1.69310E-04 0.01405 -4.59987E-05 0.03155  4.49116E-04 0.10385 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32034E-01 0.00053  1.19560E+00 0.00657 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33818E-01 0.00098  1.28999E+00 0.00762 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33582E-01 0.00067  1.29193E+00 0.00874 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28781E-01 0.00100  1.04289E+00 0.00736 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43658E+00 0.00053  2.79090E-01 0.00657 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42564E+00 0.00098  2.58765E-01 0.00771 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42707E+00 0.00067  2.58466E-01 0.00837 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45703E+00 0.00100  3.20039E-01 0.00735 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.37925E-03 0.00928  1.34921E-04 0.05672  9.46809E-04 0.02110  8.70324E-04 0.02349  2.33555E-03 0.01376  8.34851E-04 0.02343  2.56789E-04 0.04246 ];
LAMBDA                    (idx, [1:  14]) = [  7.85960E-01 0.02268  1.25313E-02 0.00082  3.09688E-02 0.00063  1.10650E-01 0.00067  3.22552E-01 0.00049  1.30565E+00 0.00229  8.69319E+00 0.00849 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0004' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:47:07 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:22:36 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292427 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.01010E+00  9.96540E-01  1.00618E+00  1.00122E+00  9.85967E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15204E-02 0.00112  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88480E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06031E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06459E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67598E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.50065E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.49975E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.39842E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.17805E-01 0.00118  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000952 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00019E+04 0.00086 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00019E+04 0.00086 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.75000E+02 ;
RUNNING_TIME              (idx, 1)        =  9.54809E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.83567E-01  4.83567E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.74200E-01  9.65000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.38800E+01  3.33657E+00  2.60715E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  8.34200E-01  2.62167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  5.06333E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.54807E+01  1.25583E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97481 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99881E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80495E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.53075E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.72696E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.64052E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.91372E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.33794E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61700E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.59313E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.30785E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.70928E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.55256E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.27212E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.55291E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.88207E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.78806E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.37190E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.49374E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.53864E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.30260E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.35305E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.31033E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.30523E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.95805E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.29081E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.95067E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 15 ;
BURNUP                     (idx, [1:  2])  = [  1.75000E+01  1.75016E+01 ];
BURN_DAYS                 (idx, 1)        =  4.37500E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.63316E-01 0.00110 ];
U235_FISS                 (idx, [1:   4]) = [  6.33918E+15 0.00101  4.63360E-01 0.00083 ];
U238_FISS                 (idx, [1:   4]) = [  1.09477E+15 0.00265  8.00094E-02 0.00246 ];
PU239_FISS                (idx, [1:   4]) = [  5.32455E+15 0.00120  3.89182E-01 0.00098 ];
PU240_FISS                (idx, [1:   4]) = [  8.97485E+12 0.03031  6.56053E-04 0.03027 ];
PU241_FISS                (idx, [1:   4]) = [  8.98314E+14 0.00298  6.56580E-02 0.00289 ];
U235_CAPT                 (idx, [1:   4]) = [  1.41902E+15 0.00244  5.55760E-02 0.00236 ];
U238_CAPT                 (idx, [1:   4]) = [  9.48605E+15 0.00107  3.71505E-01 0.00075 ];
PU239_CAPT                (idx, [1:   4]) = [  2.94352E+15 0.00158  1.15287E-01 0.00150 ];
PU240_CAPT                (idx, [1:   4]) = [  1.89227E+15 0.00206  7.41103E-02 0.00196 ];
PU241_CAPT                (idx, [1:   4]) = [  3.21328E+14 0.00480  1.25879E-02 0.00487 ];
XE135_CAPT                (idx, [1:   4]) = [  7.69724E+14 0.00326  3.01484E-02 0.00324 ];
SM149_CAPT                (idx, [1:   4]) = [  2.24409E+14 0.00573  8.78855E-03 0.00568 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000952 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.13388E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000952 5.00813E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3226753 3.23146E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1729119 1.73159E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45080 4.50821E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000952 5.00813E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.53903E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.65001E+16 2.2E-05  3.65001E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36746E+16 4.4E-06  1.36746E+16 4.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.55162E+16 0.00044  1.93994E+16 0.00045  6.11680E+15 0.00111 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.91908E+16 0.00028  3.30740E+16 0.00027  6.11680E+15 0.00111 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.95067E+16 0.00052  3.95067E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.85254E+18 0.00050  4.91970E+17 0.00048  1.36057E+18 0.00056 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.56239E+14 0.00512 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.95471E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.38465E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10481E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10481E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.51699E+00 0.00059 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.80401E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.36473E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23809E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94111E-01 3.8E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96854E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.32816E-01 0.00061 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.24406E-01 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.66919E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05420E+02 4.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.24492E-01 0.00062  9.19522E-01 0.00061  4.88329E-03 0.01030 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.24476E-01 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  9.24020E-01 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.24476E-01 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  9.32891E-01 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70956E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70969E+01 9.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.55146E-07 0.00378 ];
IMP_EALF                  (idx, [1:   2]) = [  7.52099E-07 0.00170 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.72219E-01 0.00269 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.72370E-01 0.00118 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.82639E-03 0.00648  1.46984E-04 0.03965  1.00541E-03 0.01449  9.50284E-04 0.01656  2.54565E-03 0.00921  9.14957E-04 0.01568  2.63100E-04 0.03040 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.53700E-01 0.01516  9.13262E-03 0.02738  3.09079E-02 0.00047  1.10690E-01 0.00049  3.22506E-01 0.00032  1.29670E+00 0.00167  7.79777E+00 0.01625 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.27794E-03 0.00908  1.43593E-04 0.05464  8.93374E-04 0.02173  8.55074E-04 0.02352  2.31380E-03 0.01439  8.25955E-04 0.02155  2.46145E-04 0.04487 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.70360E-01 0.02328  1.25378E-02 0.00086  3.08953E-02 0.00066  1.10725E-01 0.00071  3.22691E-01 0.00048  1.29573E+00 0.00247  8.58761E+00 0.00913 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.73957E-05 0.00145  2.73840E-05 0.00145  2.96411E-05 0.01666 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.53223E-05 0.00131  2.53115E-05 0.00131  2.74013E-05 0.01665 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.27926E-03 0.01033  1.36061E-04 0.06792  9.14162E-04 0.02465  8.17822E-04 0.02646  2.30289E-03 0.01607  8.51207E-04 0.02729  2.57120E-04 0.04646 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.92994E-01 0.02511  1.25429E-02 0.00138  3.09147E-02 0.00086  1.10812E-01 0.00095  3.22545E-01 0.00057  1.29751E+00 0.00319  8.57330E+00 0.01220 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.67843E-05 0.00336  2.67779E-05 0.00337  2.62606E-05 0.04262 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.47566E-05 0.00329  2.47508E-05 0.00331  2.42574E-05 0.04239 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.22340E-03 0.03343  1.33458E-04 0.20371  8.72464E-04 0.09359  7.79209E-04 0.08911  2.30010E-03 0.04982  8.49835E-04 0.08531  2.88334E-04 0.15428 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.61823E-01 0.07988  1.25174E-02 0.00234  3.07981E-02 0.00196  1.10800E-01 0.00221  3.22851E-01 0.00169  1.30463E+00 0.00668  8.35462E+00 0.03689 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.21445E-03 0.03302  1.30220E-04 0.19929  8.92772E-04 0.09204  7.83071E-04 0.08958  2.28915E-03 0.04884  8.34313E-04 0.08497  2.84926E-04 0.15168 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.60275E-01 0.07903  1.25174E-02 0.00234  3.08016E-02 0.00196  1.10819E-01 0.00220  3.22859E-01 0.00168  1.30432E+00 0.00666  8.35761E+00 0.03689 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.96128E+02 0.03355 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.71249E-05 0.00088 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.50717E-05 0.00059 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.24292E-03 0.00638 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.93367E+02 0.00645 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.72424E-07 0.00072 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.69497E-06 0.00042  2.69474E-06 0.00042  2.73555E-06 0.00587 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.90420E-05 0.00089  3.90604E-05 0.00089  3.58761E-05 0.01178 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.34320E-01 0.00036  6.34527E-01 0.00036  6.10962E-01 0.00957 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03511E+01 0.01422 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.49975E+01 0.00047  3.33928E+01 0.00053 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.88889E+04 0.00357  3.03768E+05 0.00162  6.10267E+05 0.00094  6.51543E+05 0.00077  5.98422E+05 0.00077  6.40789E+05 0.00066  4.35617E+05 0.00080  3.85970E+05 0.00065  2.95377E+05 0.00070  2.40560E+05 0.00078  2.07833E+05 0.00083  1.86949E+05 0.00086  1.72766E+05 0.00089  1.64584E+05 0.00076  1.60089E+05 0.00097  1.38335E+05 0.00075  1.36540E+05 0.00117  1.35620E+05 0.00089  1.32911E+05 0.00082  2.59728E+05 0.00075  2.51538E+05 0.00060  1.81314E+05 0.00087  1.17467E+05 0.00094  1.35488E+05 0.00083  1.28802E+05 0.00065  1.15012E+05 0.00094  1.86655E+05 0.00070  4.29612E+04 0.00132  5.38305E+04 0.00138  4.88764E+04 0.00131  2.84977E+04 0.00245  4.94042E+04 0.00164  3.29905E+04 0.00171  2.71134E+04 0.00161  4.71568E+03 0.00306  4.04853E+03 0.00462  3.56983E+03 0.00343  3.40035E+03 0.00373  3.48871E+03 0.00460  3.81186E+03 0.00358  4.56787E+03 0.00342  4.60373E+03 0.00247  9.04448E+03 0.00277  1.48695E+04 0.00203  1.93374E+04 0.00200  5.15949E+04 0.00117  5.42100E+04 0.00168  5.80857E+04 0.00096  3.77471E+04 0.00130  2.66483E+04 0.00168  1.97790E+04 0.00151  2.33496E+04 0.00178  4.57011E+04 0.00163  6.44855E+04 0.00114  1.28926E+05 0.00127  2.03901E+05 0.00125  3.09071E+05 0.00120  1.99500E+05 0.00136  1.43534E+05 0.00138  1.03452E+05 0.00149  9.32347E+04 0.00160  9.19952E+04 0.00174  7.69101E+04 0.00155  5.21596E+04 0.00186  4.82610E+04 0.00164  4.29576E+04 0.00151  3.63004E+04 0.00147  2.86770E+04 0.00207  1.92476E+04 0.00211  6.83531E+03 0.00202 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.32427E-01 0.00036 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.47430E+18 0.00041  3.78268E+17 0.00102 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38777E-01 0.00013  1.56069E+00 0.00035 ];
INF_CAPT                  (idx, [1:   4]) = [  7.92603E-03 0.00056  3.65676E-02 0.00054 ];
INF_ABS                   (idx, [1:   4]) = [  9.69486E-03 0.00045  6.58296E-02 0.00074 ];
INF_FISS                  (idx, [1:   4]) = [  1.76883E-03 0.00042  2.92620E-02 0.00101 ];
INF_NSF                   (idx, [1:   4]) = [  4.75013E-03 0.00042  7.79934E-02 0.00103 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.68546E+00 7.4E-05  2.66535E+00 5.5E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05476E+02 8.7E-06  2.05406E+02 1.0E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.61161E-08 0.00039  2.58486E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29080E-01 0.00014  1.49481E+00 0.00040 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44096E-01 0.00023  3.96171E-01 0.00048 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63208E-02 0.00032  9.35729E-02 0.00098 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32327E-03 0.00301  2.79851E-02 0.00272 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03195E-02 0.00162 -8.78085E-03 0.00647 ];
INF_SCATT5                (idx, [1:   4]) = [  1.82360E-04 0.08723  6.74859E-03 0.00825 ];
INF_SCATT6                (idx, [1:   4]) = [  5.09612E-03 0.00285 -1.70735E-02 0.00313 ];
INF_SCATT7                (idx, [1:   4]) = [  7.37051E-04 0.01813  4.78969E-04 0.09614 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29124E-01 0.00014  1.49481E+00 0.00040 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44097E-01 0.00023  3.96171E-01 0.00048 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63209E-02 0.00032  9.35729E-02 0.00098 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32320E-03 0.00302  2.79851E-02 0.00272 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03194E-02 0.00163 -8.78085E-03 0.00647 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.82418E-04 0.08745  6.74859E-03 0.00825 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.09611E-03 0.00285 -1.70735E-02 0.00313 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.37217E-04 0.01813  4.78969E-04 0.09614 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12460E-01 0.00027  1.01316E+00 0.00032 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56893E+00 0.00027  3.29005E-01 0.00032 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.65127E-03 0.00046  6.58296E-02 0.00074 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69805E-02 0.00022  6.70365E-02 0.00097 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11797E-01 0.00013  1.72833E-02 0.00042  1.16076E-03 0.00418  1.49365E+00 0.00040 ];
INF_S1                    (idx, [1:   8]) = [  2.39088E-01 0.00023  5.00792E-03 0.00118  4.99198E-04 0.00840  3.95671E-01 0.00048 ];
INF_S2                    (idx, [1:   8]) = [  9.78791E-02 0.00032 -1.55824E-03 0.00237  2.74257E-04 0.00825  9.32987E-02 0.00098 ];
INF_S3                    (idx, [1:   8]) = [  9.10741E-03 0.00242 -1.78414E-03 0.00177  9.79539E-05 0.01904  2.78871E-02 0.00272 ];
INF_S4                    (idx, [1:   8]) = [ -9.76013E-03 0.00183 -5.59399E-04 0.00448 -1.21191E-08 1.00000 -8.78084E-03 0.00645 ];
INF_S5                    (idx, [1:   8]) = [  1.42861E-04 0.10227  3.94997E-05 0.06823 -4.06187E-05 0.03570  6.78921E-03 0.00813 ];
INF_S6                    (idx, [1:   8]) = [  5.22927E-03 0.00259 -1.33149E-04 0.01993 -5.30727E-05 0.03142 -1.70204E-02 0.00310 ];
INF_S7                    (idx, [1:   8]) = [  9.07254E-04 0.01436 -1.70203E-04 0.01362 -4.75559E-05 0.03029  5.26525E-04 0.08647 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11840E-01 0.00013  1.72833E-02 0.00042  1.16076E-03 0.00418  1.49365E+00 0.00040 ];
INF_SP1                   (idx, [1:   8]) = [  2.39089E-01 0.00023  5.00792E-03 0.00118  4.99198E-04 0.00840  3.95671E-01 0.00048 ];
INF_SP2                   (idx, [1:   8]) = [  9.78791E-02 0.00032 -1.55824E-03 0.00237  2.74257E-04 0.00825  9.32987E-02 0.00098 ];
INF_SP3                   (idx, [1:   8]) = [  9.10734E-03 0.00243 -1.78414E-03 0.00177  9.79539E-05 0.01904  2.78871E-02 0.00272 ];
INF_SP4                   (idx, [1:   8]) = [ -9.75997E-03 0.00183 -5.59399E-04 0.00448 -1.21191E-08 1.00000 -8.78084E-03 0.00645 ];
INF_SP5                   (idx, [1:   8]) = [  1.42918E-04 0.10249  3.94997E-05 0.06823 -4.06187E-05 0.03570  6.78921E-03 0.00813 ];
INF_SP6                   (idx, [1:   8]) = [  5.22926E-03 0.00259 -1.33149E-04 0.01993 -5.30727E-05 0.03142 -1.70204E-02 0.00310 ];
INF_SP7                   (idx, [1:   8]) = [  9.07421E-04 0.01436 -1.70203E-04 0.01362 -4.75559E-05 0.03029  5.26525E-04 0.08647 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31924E-01 0.00076  1.20724E+00 0.00672 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33676E-01 0.00101  1.30657E+00 0.00914 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33719E-01 0.00115  1.30637E+00 0.00849 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28464E-01 0.00090  1.04952E+00 0.00722 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43727E+00 0.00076  2.76404E-01 0.00658 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42651E+00 0.00100  2.55624E-01 0.00900 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42626E+00 0.00116  2.55591E-01 0.00828 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45905E+00 0.00090  3.17997E-01 0.00711 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.27794E-03 0.00908  1.43593E-04 0.05464  8.93374E-04 0.02173  8.55074E-04 0.02352  2.31380E-03 0.01439  8.25955E-04 0.02155  2.46145E-04 0.04487 ];
LAMBDA                    (idx, [1:  14]) = [  7.70360E-01 0.02328  1.25378E-02 0.00086  3.08953E-02 0.00066  1.10725E-01 0.00071  3.22691E-01 0.00048  1.29573E+00 0.00247  8.58761E+00 0.00913 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0004' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:47:07 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:28:39 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292427 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00936E+00  9.95908E-01  1.00604E+00  1.00122E+00  9.87471E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15709E-02 0.00114  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88429E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06347E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06776E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67765E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.50235E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.50144E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.38528E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.19897E-01 0.00115  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000493 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00010E+04 0.00083 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00010E+04 0.00083 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.05184E+02 ;
RUNNING_TIME              (idx, 1)        =  1.01524E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.83567E-01  4.83567E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.95467E-01  1.02833E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.98459E+01  3.34073E+00  2.62517E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  8.90150E-01  2.75167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  5.39833E-02  8.16667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.01524E+02  1.25674E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97598 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00067E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80781E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.58551E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.71999E+04 ;
TOT_SF_RATE               (idx, 1)        =  5.88987E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.96392E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.37442E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62155E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.58252E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.74113E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.78822E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.95307E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.56208E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.88061E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.93201E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.99357E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.39256E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.52215E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.56753E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.95174E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.68608E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.36029E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.28289E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.84304E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.30536E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.05662E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 16 ;
BURNUP                     (idx, [1:  2])  = [  2.00000E+01  2.00018E+01 ];
BURN_DAYS                 (idx, 1)        =  5.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.85934E-01 0.00110 ];
U235_FISS                 (idx, [1:   4]) = [  5.81343E+15 0.00117  4.25575E-01 0.00095 ];
U238_FISS                 (idx, [1:   4]) = [  1.12084E+15 0.00273  8.20426E-02 0.00256 ];
PU239_FISS                (idx, [1:   4]) = [  5.63376E+15 0.00109  4.12444E-01 0.00097 ];
PU240_FISS                (idx, [1:   4]) = [  9.74388E+12 0.02906  7.13063E-04 0.02901 ];
PU241_FISS                (idx, [1:   4]) = [  1.06320E+15 0.00266  7.78303E-02 0.00255 ];
U235_CAPT                 (idx, [1:   4]) = [  1.30185E+15 0.00253  4.89390E-02 0.00250 ];
U238_CAPT                 (idx, [1:   4]) = [  9.69256E+15 0.00103  3.64328E-01 0.00075 ];
PU239_CAPT                (idx, [1:   4]) = [  3.10457E+15 0.00157  1.16708E-01 0.00154 ];
PU240_CAPT                (idx, [1:   4]) = [  2.10636E+15 0.00203  7.91746E-02 0.00190 ];
PU241_CAPT                (idx, [1:   4]) = [  3.78801E+14 0.00485  1.42410E-02 0.00486 ];
XE135_CAPT                (idx, [1:   4]) = [  7.73522E+14 0.00325  2.90788E-02 0.00324 ];
SM149_CAPT                (idx, [1:   4]) = [  2.31138E+14 0.00627  8.68910E-03 0.00626 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000493 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.19905E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000493 5.00820E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3273816 3.27894E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1681158 1.68373E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45519 4.55312E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000493 5.00820E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -8.56817E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.66914E+16 2.3E-05  3.66914E+16 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36578E+16 4.7E-06  1.36578E+16 4.7E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.65840E+16 0.00045  2.02914E+16 0.00046  6.29255E+15 0.00123 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.02418E+16 0.00030  3.39492E+16 0.00027  6.29255E+15 0.00123 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.05662E+16 0.00050  4.05662E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.90067E+18 0.00048  5.03697E+17 0.00048  1.39697E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.69442E+14 0.00490 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.06112E+16 0.00030 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.42249E+18 0.00065 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10192E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10192E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.49940E+00 0.00053 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.78221E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.33037E-01 0.00038 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23602E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94053E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96822E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.12933E-01 0.00058 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.04619E-01 0.00058 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.68648E+00 2.8E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05673E+02 4.7E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.04722E-01 0.00059  8.99896E-01 0.00058  4.72259E-03 0.01052 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.04990E-01 0.00031 ];
COL_KEFF                  (idx, [1:   2]) = [  9.04593E-01 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.04990E-01 0.00031 ];
ABS_KINF                  (idx, [1:   2]) = [  9.13310E-01 0.00030 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70656E+01 0.00023 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70638E+01 9.6E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.78469E-07 0.00395 ];
IMP_EALF                  (idx, [1:   2]) = [  7.77373E-07 0.00165 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.78993E-01 0.00287 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.79198E-01 0.00119 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.88792E-03 0.00630  1.50984E-04 0.04020  1.05753E-03 0.01508  9.19315E-04 0.01559  2.57202E-03 0.00951  9.23179E-04 0.01560  2.64893E-04 0.02909 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.46636E-01 0.01441  9.00949E-03 0.02822  3.08335E-02 0.00046  1.10815E-01 0.00051  3.22789E-01 0.00034  1.28332E+00 0.00274  7.84295E+00 0.01587 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.24012E-03 0.00986  1.28212E-04 0.06349  9.24801E-04 0.02177  8.19528E-04 0.02402  2.29836E-03 0.01467  8.33877E-04 0.02429  2.35339E-04 0.04654 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.45468E-01 0.02166  1.25785E-02 0.00119  3.08237E-02 0.00067  1.10733E-01 0.00075  3.22866E-01 0.00050  1.28590E+00 0.00271  8.50421E+00 0.00997 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.80327E-05 0.00144  2.80182E-05 0.00145  3.09695E-05 0.01796 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.53575E-05 0.00131  2.53443E-05 0.00132  2.80176E-05 0.01793 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.21150E-03 0.01062  1.16305E-04 0.07496  9.44124E-04 0.02527  8.25157E-04 0.02782  2.27740E-03 0.01632  8.21722E-04 0.02719  2.26789E-04 0.04867 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.36276E-01 0.02522  1.25571E-02 0.00162  3.08313E-02 0.00085  1.10905E-01 0.00099  3.22622E-01 0.00059  1.29009E+00 0.00342  8.57098E+00 0.01316 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.74374E-05 0.00342  2.74173E-05 0.00344  2.82316E-05 0.04030 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.48192E-05 0.00337  2.48010E-05 0.00339  2.55503E-05 0.04032 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.14056E-03 0.03623  6.94776E-05 0.25032  1.00411E-03 0.08512  7.83704E-04 0.09193  2.24455E-03 0.05678  8.41170E-04 0.09017  1.97546E-04 0.17695 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.18032E-01 0.07969  1.24898E-02 6.9E-05  3.08316E-02 0.00188  1.11281E-01 0.00239  3.22815E-01 0.00176  1.29833E+00 0.00727  8.64412E+00 0.03276 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.16853E-03 0.03509  7.96746E-05 0.23993  1.00107E-03 0.08292  7.96658E-04 0.08777  2.25254E-03 0.05652  8.35067E-04 0.08552  2.03522E-04 0.17693 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.12152E-01 0.07779  1.24898E-02 6.9E-05  3.08308E-02 0.00189  1.11276E-01 0.00238  3.22914E-01 0.00175  1.29733E+00 0.00735  8.64930E+00 0.03252 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.89246E+02 0.03660 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.77554E-05 0.00087 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.51066E-05 0.00065 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.17125E-03 0.00639 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.86374E+02 0.00644 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.72910E-07 0.00080 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.67571E-06 0.00042  2.67559E-06 0.00042  2.69892E-06 0.00611 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.92645E-05 0.00097  3.92816E-05 0.00097  3.61191E-05 0.01153 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.30895E-01 0.00038  6.31170E-01 0.00038  5.97314E-01 0.01072 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06936E+01 0.01341 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.50144E+01 0.00050  3.33792E+01 0.00053 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.91863E+04 0.00316  3.02931E+05 0.00117  6.10412E+05 0.00080  6.50711E+05 0.00081  5.98445E+05 0.00074  6.40989E+05 0.00078  4.35213E+05 0.00065  3.85280E+05 0.00082  2.94569E+05 0.00087  2.40813E+05 0.00084  2.07426E+05 0.00109  1.87160E+05 0.00085  1.72727E+05 0.00063  1.64278E+05 0.00092  1.60452E+05 0.00076  1.38666E+05 0.00078  1.36925E+05 0.00090  1.35626E+05 0.00115  1.33048E+05 0.00092  2.59733E+05 0.00080  2.51195E+05 0.00080  1.81197E+05 0.00106  1.17550E+05 0.00099  1.35545E+05 0.00105  1.29010E+05 0.00123  1.14751E+05 0.00105  1.86394E+05 0.00084  4.30753E+04 0.00168  5.36119E+04 0.00161  4.88213E+04 0.00187  2.85777E+04 0.00190  4.92499E+04 0.00139  3.26031E+04 0.00176  2.66504E+04 0.00179  4.60129E+03 0.00312  3.98214E+03 0.00379  3.46823E+03 0.00364  3.31165E+03 0.00342  3.39553E+03 0.00394  3.72560E+03 0.00333  4.44647E+03 0.00310  4.47754E+03 0.00290  8.90843E+03 0.00208  1.47459E+04 0.00224  1.91220E+04 0.00197  5.09903E+04 0.00190  5.38286E+04 0.00179  5.75282E+04 0.00126  3.74365E+04 0.00124  2.64076E+04 0.00147  1.95425E+04 0.00181  2.31185E+04 0.00125  4.52687E+04 0.00153  6.41667E+04 0.00125  1.28209E+05 0.00152  2.03431E+05 0.00164  3.08982E+05 0.00183  1.99503E+05 0.00186  1.43709E+05 0.00171  1.03634E+05 0.00185  9.34557E+04 0.00208  9.22500E+04 0.00208  7.72047E+04 0.00195  5.22048E+04 0.00189  4.84179E+04 0.00192  4.30466E+04 0.00203  3.65100E+04 0.00196  2.88406E+04 0.00220  1.93070E+04 0.00252  6.86304E+03 0.00264 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.12906E-01 0.00055 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.51271E+18 0.00054  3.87988E+17 0.00154 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39145E-01 0.00011  1.56432E+00 0.00047 ];
INF_CAPT                  (idx, [1:   4]) = [  8.07318E-03 0.00079  3.70454E-02 0.00088 ];
INF_ABS                   (idx, [1:   4]) = [  9.79144E-03 0.00065  6.55534E-02 0.00117 ];
INF_FISS                  (idx, [1:   4]) = [  1.71827E-03 0.00053  2.85080E-02 0.00156 ];
INF_NSF                   (idx, [1:   4]) = [  4.63742E-03 0.00051  7.65033E-02 0.00161 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.69890E+00 7.3E-05  2.68356E+00 7.2E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05665E+02 1.0E-05  2.05675E+02 1.3E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.58577E-08 0.00048  2.58897E-06 0.00020 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29350E-01 0.00012  1.49873E+00 0.00055 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44207E-01 0.00021  3.97061E-01 0.00063 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63639E-02 0.00035  9.36998E-02 0.00085 ];
INF_SCATT3                (idx, [1:   4]) = [  7.29698E-03 0.00331  2.80955E-02 0.00295 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03489E-02 0.00199 -8.86083E-03 0.00600 ];
INF_SCATT5                (idx, [1:   4]) = [  1.77656E-04 0.10012  6.67792E-03 0.00817 ];
INF_SCATT6                (idx, [1:   4]) = [  5.12885E-03 0.00371 -1.72172E-02 0.00298 ];
INF_SCATT7                (idx, [1:   4]) = [  7.52573E-04 0.02169  4.92093E-04 0.08039 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29394E-01 0.00012  1.49873E+00 0.00055 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44208E-01 0.00021  3.97061E-01 0.00063 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63644E-02 0.00035  9.36998E-02 0.00085 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.29713E-03 0.00331  2.80955E-02 0.00295 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03487E-02 0.00199 -8.86083E-03 0.00600 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.77790E-04 0.10022  6.67792E-03 0.00817 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.12892E-03 0.00370 -1.72172E-02 0.00298 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.52604E-04 0.02167  4.92093E-04 0.08039 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12438E-01 0.00033  1.01596E+00 0.00042 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56909E+00 0.00033  3.28098E-01 0.00042 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.74747E-03 0.00066  6.55534E-02 0.00117 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69966E-02 0.00029  6.67384E-02 0.00131 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12148E-01 0.00011  1.72014E-02 0.00054  1.15096E-03 0.00410  1.49758E+00 0.00055 ];
INF_S1                    (idx, [1:   8]) = [  2.39229E-01 0.00020  4.97783E-03 0.00099  4.98060E-04 0.00742  3.96563E-01 0.00063 ];
INF_S2                    (idx, [1:   8]) = [  9.79201E-02 0.00034 -1.55624E-03 0.00244  2.72727E-04 0.00952  9.34270E-02 0.00086 ];
INF_S3                    (idx, [1:   8]) = [  9.07551E-03 0.00253 -1.77853E-03 0.00194  9.68396E-05 0.02097  2.79987E-02 0.00296 ];
INF_S4                    (idx, [1:   8]) = [ -9.79342E-03 0.00203 -5.55466E-04 0.00314 -1.32808E-06 1.00000 -8.85950E-03 0.00599 ];
INF_S5                    (idx, [1:   8]) = [  1.39244E-04 0.12874  3.84119E-05 0.05893 -4.01157E-05 0.03975  6.71803E-03 0.00801 ];
INF_S6                    (idx, [1:   8]) = [  5.26484E-03 0.00359 -1.35988E-04 0.01464 -4.96026E-05 0.03499 -1.71676E-02 0.00298 ];
INF_S7                    (idx, [1:   8]) = [  9.19194E-04 0.01760 -1.66621E-04 0.01449 -4.40490E-05 0.03034  5.36142E-04 0.07428 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12192E-01 0.00011  1.72014E-02 0.00054  1.15096E-03 0.00410  1.49758E+00 0.00055 ];
INF_SP1                   (idx, [1:   8]) = [  2.39230E-01 0.00020  4.97783E-03 0.00099  4.98060E-04 0.00742  3.96563E-01 0.00063 ];
INF_SP2                   (idx, [1:   8]) = [  9.79206E-02 0.00034 -1.55624E-03 0.00244  2.72727E-04 0.00952  9.34270E-02 0.00086 ];
INF_SP3                   (idx, [1:   8]) = [  9.07567E-03 0.00253 -1.77853E-03 0.00194  9.68396E-05 0.02097  2.79987E-02 0.00296 ];
INF_SP4                   (idx, [1:   8]) = [ -9.79320E-03 0.00203 -5.55466E-04 0.00314 -1.32808E-06 1.00000 -8.85950E-03 0.00599 ];
INF_SP5                   (idx, [1:   8]) = [  1.39378E-04 0.12886  3.84119E-05 0.05893 -4.01157E-05 0.03975  6.71803E-03 0.00801 ];
INF_SP6                   (idx, [1:   8]) = [  5.26491E-03 0.00359 -1.35988E-04 0.01464 -4.96026E-05 0.03499 -1.71676E-02 0.00298 ];
INF_SP7                   (idx, [1:   8]) = [  9.19225E-04 0.01758 -1.66621E-04 0.01449 -4.40490E-05 0.03034  5.36142E-04 0.07428 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32022E-01 0.00049  1.21201E+00 0.00629 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33548E-01 0.00094  1.31406E+00 0.00936 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33933E-01 0.00067  1.32346E+00 0.00753 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28668E-01 0.00082  1.04441E+00 0.00622 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43665E+00 0.00049  2.75286E-01 0.00630 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42729E+00 0.00094  2.54197E-01 0.00929 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42493E+00 0.00067  2.52211E-01 0.00759 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45774E+00 0.00082  3.19451E-01 0.00609 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.24012E-03 0.00986  1.28212E-04 0.06349  9.24801E-04 0.02177  8.19528E-04 0.02402  2.29836E-03 0.01467  8.33877E-04 0.02429  2.35339E-04 0.04654 ];
LAMBDA                    (idx, [1:  14]) = [  7.45468E-01 0.02166  1.25785E-02 0.00119  3.08237E-02 0.00067  1.10733E-01 0.00075  3.22866E-01 0.00050  1.28590E+00 0.00271  8.50421E+00 0.00997 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0004' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:47:07 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:34:43 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292427 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00925E+00  9.97797E-01  1.00569E+00  1.00123E+00  9.86028E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15962E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88404E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06509E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06939E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68078E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.50074E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.49982E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.37267E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.20658E-01 0.00108  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001007 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00020E+04 0.00098 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00020E+04 0.00098 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.35489E+02 ;
RUNNING_TIME              (idx, 1)        =  1.07592E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.83567E-01  4.83567E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.17000E-01  1.04167E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.05836E+02  3.35257E+00  2.63743E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  9.45750E-01  2.72333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  5.73833E-02  8.16667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.07592E+02  1.25752E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97702 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99990E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81039E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.60881E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68487E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.29691E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.04285E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.43337E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.56592E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.54150E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  3.75776E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.90550E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.91445E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.08050E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  8.43310E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.99745E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.37761E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.36703E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.52702E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.55429E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.41963E+13 ;
CS137_ACTIVITY            (idx, 1)        =  3.34906E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.38281E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.23195E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.07451E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.32156E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.26415E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 17 ;
BURNUP                     (idx, [1:  2])  = [  2.50000E+01  2.50023E+01 ];
BURN_DAYS                 (idx, 1)        =  6.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.29653E-01 0.00110 ];
U235_FISS                 (idx, [1:   4]) = [  4.83526E+15 0.00129  3.54434E-01 0.00110 ];
U238_FISS                 (idx, [1:   4]) = [  1.17579E+15 0.00267  8.61809E-02 0.00252 ];
PU239_FISS                (idx, [1:   4]) = [  6.16754E+15 0.00112  4.52085E-01 0.00086 ];
PU240_FISS                (idx, [1:   4]) = [  1.27663E+13 0.02523  9.36167E-04 0.02524 ];
PU241_FISS                (idx, [1:   4]) = [  1.42545E+15 0.00240  1.04489E-01 0.00230 ];
U235_CAPT                 (idx, [1:   4]) = [  1.08243E+15 0.00285  3.77475E-02 0.00278 ];
U238_CAPT                 (idx, [1:   4]) = [  1.01275E+16 0.00104  3.53163E-01 0.00076 ];
PU239_CAPT                (idx, [1:   4]) = [  3.38927E+15 0.00162  1.18196E-01 0.00153 ];
PU240_CAPT                (idx, [1:   4]) = [  2.49816E+15 0.00187  8.71189E-02 0.00178 ];
PU241_CAPT                (idx, [1:   4]) = [  5.08414E+14 0.00414  1.77312E-02 0.00414 ];
XE135_CAPT                (idx, [1:   4]) = [  7.81322E+14 0.00340  2.72480E-02 0.00336 ];
SM149_CAPT                (idx, [1:   4]) = [  2.41275E+14 0.00605  8.41358E-03 0.00601 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001007 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.30707E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001007 5.00831E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3357425 3.36238E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1597404 1.59975E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 46178 4.61856E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001007 5.00831E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -7.91624E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.70483E+16 2.3E-05  3.70483E+16 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36261E+16 4.8E-06  1.36261E+16 4.8E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.86674E+16 0.00044  2.20532E+16 0.00046  6.61427E+15 0.00120 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.22935E+16 0.00030  3.56793E+16 0.00029  6.61427E+15 0.00120 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.26415E+16 0.00050  4.26415E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.99591E+18 0.00047  5.28022E+17 0.00048  1.46788E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.93907E+14 0.00464 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.26874E+16 0.00031 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.49464E+18 0.00063 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.09614E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.09614E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.46196E+00 0.00064 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.75957E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.26614E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23537E+00 0.00038 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94004E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96739E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.78086E-01 0.00069 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.69975E-01 0.00069 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.71893E+00 2.8E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06151E+02 4.8E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.69991E-01 0.00070  8.65618E-01 0.00069  4.35770E-03 0.01137 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.69363E-01 0.00031 ];
COL_KEFF                  (idx, [1:   2]) = [  8.68940E-01 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.69363E-01 0.00031 ];
ABS_KINF                  (idx, [1:   2]) = [  8.77472E-01 0.00030 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.69933E+01 0.00025 ];
IMP_ALF                   (idx, [1:   2]) = [  1.69965E+01 9.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  8.37215E-07 0.00422 ];
IMP_EALF                  (idx, [1:   2]) = [  8.31487E-07 0.00168 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.93656E-01 0.00277 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.93145E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.00681E-03 0.00612  1.41683E-04 0.04213  1.10366E-03 0.01429  9.36069E-04 0.01495  2.57503E-03 0.00935  9.77045E-04 0.01561  2.73330E-04 0.02933 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.39436E-01 0.01434  8.73242E-03 0.02975  3.06957E-02 0.00043  1.11067E-01 0.00055  3.22762E-01 0.00037  1.27225E+00 0.00208  7.41669E+00 0.01795 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.10241E-03 0.00929  1.30993E-04 0.06560  9.33708E-04 0.02162  7.93089E-04 0.02351  2.18138E-03 0.01438  8.29746E-04 0.02323  2.33489E-04 0.04493 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.40830E-01 0.02179  1.25739E-02 0.00117  3.06882E-02 0.00063  1.11086E-01 0.00082  3.22769E-01 0.00054  1.27032E+00 0.00309  8.23937E+00 0.01183 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.91472E-05 0.00157  2.91345E-05 0.00157  3.21415E-05 0.01856 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.53504E-05 0.00133  2.53393E-05 0.00133  2.79490E-05 0.01846 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.99649E-03 0.01134  1.22703E-04 0.07254  8.97108E-04 0.02497  7.78186E-04 0.02917  2.15961E-03 0.01640  8.17547E-04 0.02844  2.21328E-04 0.05286 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.32761E-01 0.02711  1.25867E-02 0.00185  3.06335E-02 0.00085  1.11079E-01 0.00113  3.22796E-01 0.00071  1.27553E+00 0.00401  8.14678E+00 0.01833 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.85716E-05 0.00364  2.85633E-05 0.00364  2.76528E-05 0.04377 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.48508E-05 0.00357  2.48439E-05 0.00357  2.39982E-05 0.04357 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.97483E-03 0.04011  1.58878E-04 0.24714  8.43867E-04 0.09996  7.90657E-04 0.09037  2.13198E-03 0.06266  8.51241E-04 0.10467  1.98207E-04 0.18735 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.07996E-01 0.08283  1.25606E-02 0.00396  3.05382E-02 0.00190  1.11135E-01 0.00256  3.22612E-01 0.00190  1.25150E+00 0.01121  8.34465E+00 0.04550 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.00230E-03 0.03886  1.51701E-04 0.24171  8.47735E-04 0.09619  8.03880E-04 0.08773  2.12417E-03 0.06072  8.73616E-04 0.10247  2.01200E-04 0.18338 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.01923E-01 0.08126  1.25606E-02 0.00396  3.05358E-02 0.00189  1.11138E-01 0.00256  3.22602E-01 0.00189  1.25161E+00 0.01111  8.34281E+00 0.04549 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.75707E+02 0.04066 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.89366E-05 0.00100 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.51683E-05 0.00070 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.99921E-03 0.00599 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.72802E+02 0.00598 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.72443E-07 0.00080 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.65124E-06 0.00045  2.65099E-06 0.00045  2.70134E-06 0.00597 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.96254E-05 0.00097  3.96418E-05 0.00097  3.66714E-05 0.01189 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.24551E-01 0.00035  6.24965E-01 0.00035  5.66606E-01 0.00984 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05766E+01 0.01365 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.49982E+01 0.00050  3.33201E+01 0.00054 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.04228E+04 0.00376  3.04828E+05 0.00171  6.10371E+05 0.00113  6.50899E+05 0.00078  5.98548E+05 0.00070  6.41092E+05 0.00075  4.34973E+05 0.00074  3.85240E+05 0.00078  2.95089E+05 0.00080  2.40935E+05 0.00083  2.07652E+05 0.00071  1.87248E+05 0.00104  1.72939E+05 0.00087  1.64170E+05 0.00076  1.60157E+05 0.00108  1.38331E+05 0.00096  1.36490E+05 0.00092  1.35460E+05 0.00084  1.33102E+05 0.00098  2.59968E+05 0.00081  2.51551E+05 0.00074  1.81095E+05 0.00074  1.17364E+05 0.00101  1.35459E+05 0.00084  1.28872E+05 0.00116  1.14644E+05 0.00093  1.85439E+05 0.00083  4.28668E+04 0.00149  5.31180E+04 0.00166  4.83444E+04 0.00172  2.82771E+04 0.00160  4.88991E+04 0.00156  3.23353E+04 0.00177  2.62453E+04 0.00197  4.45085E+03 0.00384  3.77250E+03 0.00425  3.29373E+03 0.00480  3.17755E+03 0.00343  3.23082E+03 0.00416  3.53417E+03 0.00452  4.24922E+03 0.00524  4.37116E+03 0.00421  8.68138E+03 0.00217  1.44588E+04 0.00286  1.87739E+04 0.00181  5.05776E+04 0.00123  5.31512E+04 0.00142  5.67215E+04 0.00147  3.68733E+04 0.00129  2.60144E+04 0.00191  1.92569E+04 0.00175  2.27203E+04 0.00138  4.46723E+04 0.00118  6.33621E+04 0.00111  1.27297E+05 0.00141  2.02281E+05 0.00144  3.08373E+05 0.00164  1.99429E+05 0.00166  1.43600E+05 0.00157  1.03801E+05 0.00182  9.35734E+04 0.00149  9.22464E+04 0.00153  7.72659E+04 0.00185  5.23634E+04 0.00187  4.85013E+04 0.00190  4.32264E+04 0.00164  3.66371E+04 0.00234  2.88997E+04 0.00194  1.93687E+04 0.00204  6.88151E+03 0.00217 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.77041E-01 0.00063 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.58946E+18 0.00061  4.06482E+17 0.00134 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39400E-01 0.00010  1.56787E+00 0.00037 ];
INF_CAPT                  (idx, [1:   4]) = [  8.33513E-03 0.00070  3.79369E-02 0.00079 ];
INF_ABS                   (idx, [1:   4]) = [  9.96266E-03 0.00060  6.50998E-02 0.00100 ];
INF_FISS                  (idx, [1:   4]) = [  1.62754E-03 0.00053  2.71629E-02 0.00129 ];
INF_NSF                   (idx, [1:   4]) = [  4.43460E-03 0.00054  7.38169E-02 0.00135 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.72473E+00 7.3E-05  2.71756E+00 6.6E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06029E+02 8.5E-06  2.06180E+02 1.3E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.53942E-08 0.00038  2.59440E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29431E-01 0.00010  1.50275E+00 0.00044 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44232E-01 0.00019  3.97892E-01 0.00048 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63590E-02 0.00034  9.37609E-02 0.00080 ];
INF_SCATT3                (idx, [1:   4]) = [  7.28190E-03 0.00259  2.81335E-02 0.00274 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03421E-02 0.00174 -8.91106E-03 0.00881 ];
INF_SCATT5                (idx, [1:   4]) = [  1.86050E-04 0.09794  6.76620E-03 0.00736 ];
INF_SCATT6                (idx, [1:   4]) = [  5.14705E-03 0.00283 -1.72477E-02 0.00304 ];
INF_SCATT7                (idx, [1:   4]) = [  7.64032E-04 0.01844  5.44168E-04 0.08509 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29475E-01 0.00010  1.50275E+00 0.00044 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44232E-01 0.00019  3.97892E-01 0.00048 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63593E-02 0.00034  9.37609E-02 0.00080 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.28186E-03 0.00259  2.81335E-02 0.00274 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03423E-02 0.00174 -8.91106E-03 0.00881 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.86060E-04 0.09778  6.76620E-03 0.00736 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.14701E-03 0.00283 -1.72477E-02 0.00304 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.64201E-04 0.01838  5.44168E-04 0.08509 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12333E-01 0.00029  1.01919E+00 0.00033 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56986E+00 0.00029  3.27057E-01 0.00033 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.91809E-03 0.00061  6.50998E-02 0.00100 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70041E-02 0.00025  6.62737E-02 0.00123 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12396E-01 9.8E-05  1.70350E-02 0.00051  1.14451E-03 0.00407  1.50160E+00 0.00044 ];
INF_S1                    (idx, [1:   8]) = [  2.39311E-01 0.00019  4.92061E-03 0.00081  4.91737E-04 0.00593  3.97400E-01 0.00049 ];
INF_S2                    (idx, [1:   8]) = [  9.79139E-02 0.00033 -1.55496E-03 0.00296  2.68304E-04 0.00746  9.34926E-02 0.00080 ];
INF_S3                    (idx, [1:   8]) = [  9.04627E-03 0.00208 -1.76436E-03 0.00223  9.60141E-05 0.01948  2.80375E-02 0.00276 ];
INF_S4                    (idx, [1:   8]) = [ -9.79805E-03 0.00176 -5.44087E-04 0.00551  9.85629E-07 1.00000 -8.91205E-03 0.00878 ];
INF_S5                    (idx, [1:   8]) = [  1.39144E-04 0.12252  4.69062E-05 0.05945 -3.77385E-05 0.04299  6.80394E-03 0.00730 ];
INF_S6                    (idx, [1:   8]) = [  5.27755E-03 0.00274 -1.30496E-04 0.01818 -4.95046E-05 0.02856 -1.71982E-02 0.00304 ];
INF_S7                    (idx, [1:   8]) = [  9.33368E-04 0.01530 -1.69336E-04 0.01509 -4.37611E-05 0.02583  5.87929E-04 0.07851 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12440E-01 9.8E-05  1.70350E-02 0.00051  1.14451E-03 0.00407  1.50160E+00 0.00044 ];
INF_SP1                   (idx, [1:   8]) = [  2.39312E-01 0.00019  4.92061E-03 0.00081  4.91737E-04 0.00593  3.97400E-01 0.00049 ];
INF_SP2                   (idx, [1:   8]) = [  9.79142E-02 0.00033 -1.55496E-03 0.00296  2.68304E-04 0.00746  9.34926E-02 0.00080 ];
INF_SP3                   (idx, [1:   8]) = [  9.04622E-03 0.00208 -1.76436E-03 0.00223  9.60141E-05 0.01948  2.80375E-02 0.00276 ];
INF_SP4                   (idx, [1:   8]) = [ -9.79822E-03 0.00176 -5.44087E-04 0.00551  9.85629E-07 1.00000 -8.91205E-03 0.00878 ];
INF_SP5                   (idx, [1:   8]) = [  1.39154E-04 0.12227  4.69062E-05 0.05945 -3.77385E-05 0.04299  6.80394E-03 0.00730 ];
INF_SP6                   (idx, [1:   8]) = [  5.27751E-03 0.00275 -1.30496E-04 0.01818 -4.95046E-05 0.02856 -1.71982E-02 0.00304 ];
INF_SP7                   (idx, [1:   8]) = [  9.33537E-04 0.01525 -1.69336E-04 0.01509 -4.37611E-05 0.02583  5.87929E-04 0.07851 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32035E-01 0.00069  1.22739E+00 0.01027 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33675E-01 0.00097  1.33225E+00 0.01218 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33714E-01 0.00105  1.34534E+00 0.01156 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28796E-01 0.00114  1.05335E+00 0.00949 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43658E+00 0.00069  2.72266E-01 0.01025 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42652E+00 0.00097  2.51092E-01 0.01213 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42628E+00 0.00105  2.48568E-01 0.01160 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45695E+00 0.00113  3.17139E-01 0.00952 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.10241E-03 0.00929  1.30993E-04 0.06560  9.33708E-04 0.02162  7.93089E-04 0.02351  2.18138E-03 0.01438  8.29746E-04 0.02323  2.33489E-04 0.04493 ];
LAMBDA                    (idx, [1:  14]) = [  7.40830E-01 0.02179  1.25739E-02 0.00117  3.06882E-02 0.00063  1.11086E-01 0.00082  3.22769E-01 0.00054  1.27032E+00 0.00309  8.23937E+00 0.01183 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0004' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:47:07 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:40:46 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292427 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00871E+00  9.96679E-01  1.00595E+00  1.00208E+00  9.86575E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16096E-02 0.00106  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88390E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05870E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06301E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68463E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.49313E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.49221E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.38882E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.20753E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000867 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00017E+04 0.00102 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00017E+04 0.00102 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.65750E+02 ;
RUNNING_TIME              (idx, 1)        =  1.13651E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.83567E-01  4.83567E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.38767E-01  1.07500E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.11817E+02  3.34583E+00  2.63558E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.00102E+00  2.67833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  6.08000E-02  7.83332E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.13651E+02  1.25778E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97795 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00078E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81268E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.73149E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68209E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.44341E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.14425E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.50944E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.58720E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.53111E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  4.99492E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.06306E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.10199E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.73195E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  8.92930E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.08987E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.72921E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.40121E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.56905E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.60044E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.09927E+13 ;
CS137_ACTIVITY            (idx, 1)        =  4.00779E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.49405E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.20176E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.69660E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.35087E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.45715E+12 0.00053  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 18 ;
BURNUP                     (idx, [1:  2])  = [  3.00000E+01  3.00028E+01 ];
BURN_DAYS                 (idx, 1)        =  7.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.71479E-01 0.00120 ];
U235_FISS                 (idx, [1:   4]) = [  3.97970E+15 0.00150  2.92473E-01 0.00129 ];
U238_FISS                 (idx, [1:   4]) = [  1.22395E+15 0.00258  8.99439E-02 0.00241 ];
PU239_FISS                (idx, [1:   4]) = [  6.59803E+15 0.00109  4.84914E-01 0.00085 ];
PU240_FISS                (idx, [1:   4]) = [  1.56573E+13 0.02430  1.15096E-03 0.02433 ];
PU241_FISS                (idx, [1:   4]) = [  1.75635E+15 0.00217  1.29086E-01 0.00211 ];
U235_CAPT                 (idx, [1:   4]) = [  8.90579E+14 0.00333  2.90787E-02 0.00333 ];
U238_CAPT                 (idx, [1:   4]) = [  1.05330E+16 0.00115  3.43864E-01 0.00080 ];
PU239_CAPT                (idx, [1:   4]) = [  3.61858E+15 0.00145  1.18154E-01 0.00146 ];
PU240_CAPT                (idx, [1:   4]) = [  2.84972E+15 0.00187  9.30337E-02 0.00169 ];
PU241_CAPT                (idx, [1:   4]) = [  6.24582E+14 0.00374  2.03947E-02 0.00376 ];
XE135_CAPT                (idx, [1:   4]) = [  7.89302E+14 0.00339  2.57714E-02 0.00337 ];
SM149_CAPT                (idx, [1:   4]) = [  2.53512E+14 0.00578  8.27802E-03 0.00580 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000867 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.19635E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000867 5.00820E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3430810 3.43595E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1524319 1.52650E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45738 4.57466E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000867 5.00820E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.33299E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.73631E+16 2.3E-05  3.73631E+16 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35980E+16 4.8E-06  1.35980E+16 4.8E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.06414E+16 0.00045  2.37664E+16 0.00046  6.87501E+15 0.00114 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.42393E+16 0.00031  3.73643E+16 0.00029  6.87501E+15 0.00114 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.45715E+16 0.00053  4.45715E+16 0.00053  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.08447E+18 0.00050  5.52329E+17 0.00049  1.53214E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.07856E+14 0.00489 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.46472E+16 0.00032 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.55884E+18 0.00064 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.09037E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.09037E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.42612E+00 0.00064 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.75168E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.20562E-01 0.00037 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23417E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94065E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96766E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.46601E-01 0.00070 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.38856E-01 0.00070 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.74770E+00 2.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06578E+02 4.8E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.38889E-01 0.00071  8.34836E-01 0.00071  4.02017E-03 0.01210 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.38269E-01 0.00032 ];
COL_KEFF                  (idx, [1:   2]) = [  8.38392E-01 0.00053 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.38269E-01 0.00032 ];
ABS_KINF                  (idx, [1:   2]) = [  8.46008E-01 0.00032 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.69364E+01 0.00023 ];
IMP_ALF                   (idx, [1:   2]) = [  1.69334E+01 0.00010 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  8.85678E-07 0.00390 ];
IMP_EALF                  (idx, [1:   2]) = [  8.85665E-07 0.00170 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.05878E-01 0.00265 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.06312E-01 0.00110 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.97651E-03 0.00717  1.42199E-04 0.04149  1.11550E-03 0.01501  9.26947E-04 0.01659  2.52275E-03 0.01035  9.84071E-04 0.01621  2.85046E-04 0.03022 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.49358E-01 0.01503  8.78206E-03 0.02961  3.05996E-02 0.00043  1.11195E-01 0.00209  3.23371E-01 0.00040  1.25423E+00 0.00247  7.42622E+00 0.01796 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.83039E-03 0.01041  1.10171E-04 0.06602  9.01179E-04 0.02296  7.46430E-04 0.02604  2.04005E-03 0.01594  7.98427E-04 0.02577  2.34130E-04 0.04750 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.55030E-01 0.02301  1.26227E-02 0.00143  3.06057E-02 0.00064  1.11440E-01 0.00088  3.23573E-01 0.00060  1.25501E+00 0.00345  8.23391E+00 0.01281 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.03390E-05 0.00162  3.03268E-05 0.00164  3.26948E-05 0.01871 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.54446E-05 0.00146  2.54344E-05 0.00147  2.74196E-05 0.01871 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.79166E-03 0.01214  1.06861E-04 0.07893  8.82584E-04 0.02824  7.48926E-04 0.03000  2.04135E-03 0.01840  7.84574E-04 0.02892  2.27368E-04 0.05447 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.65063E-01 0.02899  1.26381E-02 0.00248  3.05857E-02 0.00087  1.11414E-01 0.00119  3.23750E-01 0.00079  1.25932E+00 0.00478  8.24094E+00 0.01804 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.97155E-05 0.00357  2.97059E-05 0.00357  2.70595E-05 0.04463 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.49223E-05 0.00351  2.49143E-05 0.00351  2.26997E-05 0.04470 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.83384E-03 0.04151  5.82957E-05 0.35584  1.02209E-03 0.08856  7.23096E-04 0.10967  2.18349E-03 0.05986  6.62958E-04 0.09950  1.83911E-04 0.21318 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  6.45748E-01 0.09667  1.26959E-02 0.00882  3.06091E-02 0.00193  1.11015E-01 0.00308  3.24012E-01 0.00207  1.23460E+00 0.01254  8.12518E+00 0.05020 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.80148E-03 0.04067  6.11556E-05 0.32446  1.00572E-03 0.08672  7.05200E-04 0.10922  2.16622E-03 0.05900  6.85542E-04 0.09908  1.77634E-04 0.20258 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  6.47848E-01 0.09464  1.26959E-02 0.00882  3.06075E-02 0.00192  1.11020E-01 0.00308  3.24033E-01 0.00206  1.23387E+00 0.01256  8.09621E+00 0.05095 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.63542E+02 0.04159 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.00312E-05 0.00097 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.51863E-05 0.00064 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.82292E-03 0.00775 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.60747E+02 0.00793 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.70135E-07 0.00074 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.63117E-06 0.00043  2.63093E-06 0.00043  2.67470E-06 0.00591 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.98128E-05 0.00090  3.98287E-05 0.00090  3.66200E-05 0.01137 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.18587E-01 0.00037  6.19117E-01 0.00037  5.46237E-01 0.01196 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08041E+01 0.01538 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.49221E+01 0.00047  3.32885E+01 0.00055 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.04109E+04 0.00304  3.05814E+05 0.00115  6.11975E+05 0.00089  6.51673E+05 0.00075  5.99031E+05 0.00057  6.41856E+05 0.00072  4.35773E+05 0.00067  3.85524E+05 0.00058  2.95043E+05 0.00086  2.41214E+05 0.00096  2.07744E+05 0.00088  1.87291E+05 0.00074  1.72998E+05 0.00093  1.64543E+05 0.00080  1.60330E+05 0.00091  1.38522E+05 0.00077  1.36754E+05 0.00080  1.35656E+05 0.00098  1.33297E+05 0.00096  2.60283E+05 0.00057  2.51772E+05 0.00059  1.81168E+05 0.00082  1.17688E+05 0.00094  1.35298E+05 0.00063  1.29230E+05 0.00074  1.14528E+05 0.00104  1.84695E+05 0.00076  4.29202E+04 0.00133  5.26901E+04 0.00121  4.80784E+04 0.00125  2.83608E+04 0.00246  4.87010E+04 0.00173  3.19015E+04 0.00168  2.56878E+04 0.00145  4.29160E+03 0.00409  3.62091E+03 0.00271  3.15662E+03 0.00364  3.05650E+03 0.00368  3.09826E+03 0.00412  3.40103E+03 0.00295  4.07575E+03 0.00388  4.26850E+03 0.00432  8.54611E+03 0.00291  1.42365E+04 0.00268  1.85577E+04 0.00220  4.99626E+04 0.00161  5.23514E+04 0.00154  5.61855E+04 0.00123  3.63638E+04 0.00117  2.56339E+04 0.00157  1.90125E+04 0.00152  2.24111E+04 0.00171  4.40927E+04 0.00148  6.27738E+04 0.00125  1.25972E+05 0.00122  2.00747E+05 0.00138  3.06571E+05 0.00156  1.98443E+05 0.00169  1.43124E+05 0.00157  1.03445E+05 0.00153  9.31580E+04 0.00192  9.19956E+04 0.00161  7.71368E+04 0.00167  5.22982E+04 0.00165  4.83937E+04 0.00180  4.31648E+04 0.00236  3.65378E+04 0.00172  2.88737E+04 0.00218  1.93293E+04 0.00199  6.85416E+03 0.00229 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.46133E-01 0.00048 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.66223E+18 0.00041  4.22284E+17 0.00134 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39571E-01 0.00011  1.56760E+00 0.00039 ];
INF_CAPT                  (idx, [1:   4]) = [  8.57348E-03 0.00046  3.88178E-02 0.00082 ];
INF_ABS                   (idx, [1:   4]) = [  1.01260E-02 0.00040  6.49130E-02 0.00103 ];
INF_FISS                  (idx, [1:   4]) = [  1.55256E-03 0.00043  2.60951E-02 0.00135 ];
INF_NSF                   (idx, [1:   4]) = [  4.26634E-03 0.00043  7.17002E-02 0.00139 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.74794E+00 6.2E-05  2.74764E+00 6.2E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06361E+02 9.4E-06  2.06629E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.49791E-08 0.00042  2.59807E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29446E-01 0.00012  1.50270E+00 0.00046 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44074E-01 0.00017  3.97563E-01 0.00059 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63159E-02 0.00027  9.37783E-02 0.00124 ];
INF_SCATT3                (idx, [1:   4]) = [  7.29062E-03 0.00259  2.80612E-02 0.00227 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03364E-02 0.00215 -9.00002E-03 0.00708 ];
INF_SCATT5                (idx, [1:   4]) = [  1.85577E-04 0.10376  6.71753E-03 0.00917 ];
INF_SCATT6                (idx, [1:   4]) = [  5.14958E-03 0.00324 -1.72533E-02 0.00232 ];
INF_SCATT7                (idx, [1:   4]) = [  7.46325E-04 0.01798  5.30340E-04 0.08542 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29490E-01 0.00012  1.50270E+00 0.00046 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44075E-01 0.00017  3.97563E-01 0.00059 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63156E-02 0.00027  9.37783E-02 0.00124 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.29065E-03 0.00259  2.80612E-02 0.00227 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03366E-02 0.00215 -9.00002E-03 0.00708 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.85575E-04 0.10388  6.71753E-03 0.00917 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.14945E-03 0.00324 -1.72533E-02 0.00232 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.46318E-04 0.01798  5.30340E-04 0.08542 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12458E-01 0.00029  1.02029E+00 0.00030 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56894E+00 0.00029  3.26704E-01 0.00030 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.00821E-02 0.00040  6.49130E-02 0.00103 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69889E-02 0.00022  6.60431E-02 0.00114 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12582E-01 0.00011  1.68637E-02 0.00039  1.14085E-03 0.00363  1.50156E+00 0.00046 ];
INF_S1                    (idx, [1:   8]) = [  2.39210E-01 0.00017  4.86416E-03 0.00093  4.94204E-04 0.00683  3.97069E-01 0.00059 ];
INF_S2                    (idx, [1:   8]) = [  9.78631E-02 0.00026 -1.54717E-03 0.00216  2.69898E-04 0.00811  9.35084E-02 0.00125 ];
INF_S3                    (idx, [1:   8]) = [  9.03337E-03 0.00213 -1.74275E-03 0.00191  9.63554E-05 0.01792  2.79648E-02 0.00229 ];
INF_S4                    (idx, [1:   8]) = [ -9.80249E-03 0.00227 -5.33950E-04 0.00563 -1.82636E-06 0.88231 -8.99819E-03 0.00711 ];
INF_S5                    (idx, [1:   8]) = [  1.39342E-04 0.14196  4.62342E-05 0.05399 -3.93702E-05 0.04907  6.75690E-03 0.00908 ];
INF_S6                    (idx, [1:   8]) = [  5.27670E-03 0.00318 -1.27121E-04 0.01460 -5.06157E-05 0.02222 -1.72027E-02 0.00232 ];
INF_S7                    (idx, [1:   8]) = [  9.12407E-04 0.01512 -1.66081E-04 0.01383 -4.47935E-05 0.02772  5.75133E-04 0.07906 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12626E-01 0.00011  1.68637E-02 0.00039  1.14085E-03 0.00363  1.50156E+00 0.00046 ];
INF_SP1                   (idx, [1:   8]) = [  2.39210E-01 0.00017  4.86416E-03 0.00093  4.94204E-04 0.00683  3.97069E-01 0.00059 ];
INF_SP2                   (idx, [1:   8]) = [  9.78627E-02 0.00026 -1.54717E-03 0.00216  2.69898E-04 0.00811  9.35084E-02 0.00125 ];
INF_SP3                   (idx, [1:   8]) = [  9.03339E-03 0.00213 -1.74275E-03 0.00191  9.63554E-05 0.01792  2.79648E-02 0.00229 ];
INF_SP4                   (idx, [1:   8]) = [ -9.80269E-03 0.00227 -5.33950E-04 0.00563 -1.82636E-06 0.88231 -8.99819E-03 0.00711 ];
INF_SP5                   (idx, [1:   8]) = [  1.39341E-04 0.14214  4.62342E-05 0.05399 -3.93702E-05 0.04907  6.75690E-03 0.00908 ];
INF_SP6                   (idx, [1:   8]) = [  5.27658E-03 0.00319 -1.27121E-04 0.01460 -5.06157E-05 0.02222 -1.72027E-02 0.00232 ];
INF_SP7                   (idx, [1:   8]) = [  9.12399E-04 0.01511 -1.66081E-04 0.01383 -4.47935E-05 0.02772  5.75133E-04 0.07906 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31732E-01 0.00070  1.21513E+00 0.00662 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33172E-01 0.00092  1.33328E+00 0.00845 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33798E-01 0.00082  1.31693E+00 0.00783 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28311E-01 0.00107  1.04332E+00 0.00740 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43846E+00 0.00070  2.74611E-01 0.00669 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42959E+00 0.00092  2.50442E-01 0.00850 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42575E+00 0.00082  2.53476E-01 0.00762 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.46004E+00 0.00107  3.19914E-01 0.00743 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.83039E-03 0.01041  1.10171E-04 0.06602  9.01179E-04 0.02296  7.46430E-04 0.02604  2.04005E-03 0.01594  7.98427E-04 0.02577  2.34130E-04 0.04750 ];
LAMBDA                    (idx, [1:  14]) = [  7.55030E-01 0.02301  1.26227E-02 0.00143  3.06057E-02 0.00064  1.11440E-01 0.00088  3.23573E-01 0.00060  1.25501E+00 0.00345  8.23391E+00 0.01281 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0004' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:47:07 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:46:42 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292427 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00420E+00  1.01171E+00  1.01390E+00  1.00977E+00  9.60418E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.1E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.17113E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88289E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05094E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05527E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68771E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.48043E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.47952E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.39974E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.23979E-01 0.00111  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001136 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00023E+04 0.00097 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00023E+04 0.00097 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.95326E+02 ;
RUNNING_TIME              (idx, 1)        =  1.19573E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.83567E-01  4.83567E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.60017E-01  1.03333E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.17666E+02  3.31538E+00  2.53287E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.05272E+00  2.46167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  6.39833E-02  7.16666E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.19573E+02  1.25521E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97876 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99871E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81438E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.86536E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68589E+04 ;
TOT_SF_RATE               (idx, 1)        =  4.11980E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.24393E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.58538E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62139E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.52731E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  6.46330E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.22632E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.52633E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.04197E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  9.36975E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.18435E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.05257E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.44037E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.61129E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.65028E+14 ;
CS134_ACTIVITY            (idx, 1)        =  7.94042E+13 ;
CS137_ACTIVITY            (idx, 1)        =  4.66250E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.61416E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.17809E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.40850E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.38063E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.64591E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 19 ;
BURNUP                     (idx, [1:  2])  = [  3.50000E+01  3.50034E+01 ];
BURN_DAYS                 (idx, 1)        =  8.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  8.12596E-01 0.00107 ];
U233_FISS                 (idx, [1:   4]) = [  9.32695E+09 1.00000  6.69120E-07 1.00000 ];
U235_FISS                 (idx, [1:   4]) = [  3.24333E+15 0.00166  2.38832E-01 0.00152 ];
U238_FISS                 (idx, [1:   4]) = [  1.27138E+15 0.00288  9.36068E-02 0.00268 ];
PU239_FISS                (idx, [1:   4]) = [  6.96039E+15 0.00106  5.12543E-01 0.00079 ];
PU240_FISS                (idx, [1:   4]) = [  1.84092E+13 0.02302  1.35566E-03 0.02301 ];
PU241_FISS                (idx, [1:   4]) = [  2.04306E+15 0.00212  1.50439E-01 0.00196 ];
U235_CAPT                 (idx, [1:   4]) = [  7.24810E+14 0.00348  2.22804E-02 0.00348 ];
U238_CAPT                 (idx, [1:   4]) = [  1.09563E+16 0.00103  3.36756E-01 0.00079 ];
PU239_CAPT                (idx, [1:   4]) = [  3.82500E+15 0.00151  1.17573E-01 0.00144 ];
PU240_CAPT                (idx, [1:   4]) = [  3.17441E+15 0.00172  9.75681E-02 0.00157 ];
PU241_CAPT                (idx, [1:   4]) = [  7.28875E+14 0.00364  2.24030E-02 0.00358 ];
XE135_CAPT                (idx, [1:   4]) = [  8.04918E+14 0.00343  2.47432E-02 0.00344 ];
SM149_CAPT                (idx, [1:   4]) = [  2.59042E+14 0.00588  7.96173E-03 0.00582 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001136 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.23688E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001136 5.00824E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3496253 3.50137E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1459621 1.46161E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45262 4.52619E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001136 5.00824E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.63216E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.76355E+16 2.1E-05  3.76355E+16 2.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35739E+16 4.4E-06  1.35739E+16 4.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.25291E+16 0.00046  2.54218E+16 0.00048  7.10723E+15 0.00123 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.61029E+16 0.00033  3.89957E+16 0.00031  7.10723E+15 0.00123 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.64591E+16 0.00052  4.64591E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.16823E+18 0.00051  5.75478E+17 0.00050  1.59276E+18 0.00056 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.20601E+14 0.00477 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.65235E+16 0.00033 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.61897E+18 0.00066 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.08461E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.08461E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.39139E+00 0.00065 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.75305E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.14441E-01 0.00037 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23410E+00 0.00043 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94095E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96834E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.17925E-01 0.00070 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.10521E-01 0.00070 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.77265E+00 2.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06945E+02 4.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.10604E-01 0.00071  8.06718E-01 0.00070  3.80300E-03 0.01165 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.10355E-01 0.00033 ];
COL_KEFF                  (idx, [1:   2]) = [  8.10187E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.10355E-01 0.00033 ];
ABS_KINF                  (idx, [1:   2]) = [  8.17759E-01 0.00033 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.68748E+01 0.00028 ];
IMP_ALF                   (idx, [1:   2]) = [  1.68725E+01 0.00011 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  9.43773E-07 0.00478 ];
IMP_EALF                  (idx, [1:   2]) = [  9.41407E-07 0.00191 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.19314E-01 0.00303 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.19392E-01 0.00120 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.01745E-03 0.00645  1.37820E-04 0.04106  1.07140E-03 0.01562  9.41715E-04 0.01626  2.55977E-03 0.00971  1.01994E-03 0.01646  2.86810E-04 0.02851 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.42607E-01 0.01472  8.68013E-03 0.03032  3.04818E-02 0.00041  1.11568E-01 0.00064  3.23047E-01 0.00042  1.23270E+00 0.00267  7.09298E+00 0.01950 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.68765E-03 0.01015  1.11646E-04 0.06149  8.26940E-04 0.02454  7.60237E-04 0.02469  1.98001E-03 0.01576  7.87533E-04 0.02509  2.21283E-04 0.04616 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.44005E-01 0.02419  1.26528E-02 0.00159  3.04852E-02 0.00064  1.11559E-01 0.00091  3.23169E-01 0.00063  1.23901E+00 0.00377  8.00712E+00 0.01400 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.15664E-05 0.00169  3.15531E-05 0.00169  3.44703E-05 0.01947 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.55802E-05 0.00146  2.55694E-05 0.00146  2.79440E-05 0.01953 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.69768E-03 0.01174  1.08982E-04 0.07739  8.63990E-04 0.02815  7.27765E-04 0.02973  1.96273E-03 0.01814  8.19669E-04 0.02800  2.14547E-04 0.05314 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.43673E-01 0.02888  1.26351E-02 0.00239  3.04777E-02 0.00086  1.11575E-01 0.00125  3.23152E-01 0.00084  1.24289E+00 0.00505  8.09492E+00 0.02058 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.08638E-05 0.00368  3.08474E-05 0.00366  3.09407E-05 0.05409 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.50122E-05 0.00362  2.49988E-05 0.00360  2.50984E-05 0.05424 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.54849E-03 0.04480  7.17557E-05 0.29532  9.82882E-04 0.10266  7.19734E-04 0.10632  1.88376E-03 0.06629  7.16283E-04 0.10366  1.74070E-04 0.22960 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.10191E-01 0.09799  1.26170E-02 0.00712  3.04251E-02 0.00195  1.11571E-01 0.00319  3.24131E-01 0.00222  1.24232E+00 0.01208  8.57121E+00 0.04586 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.57143E-03 0.04350  7.04953E-05 0.29450  9.84102E-04 0.10025  7.36708E-04 0.10480  1.87060E-03 0.06492  7.32379E-04 0.10076  1.77138E-04 0.22157 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.13406E-01 0.09645  1.26165E-02 0.00710  3.04243E-02 0.00194  1.11538E-01 0.00318  3.24067E-01 0.00221  1.24139E+00 0.01211  8.58140E+00 0.04573 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.47927E+02 0.04506 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.12030E-05 0.00103 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.52870E-05 0.00075 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.56901E-03 0.00914 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.46472E+02 0.00916 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.67040E-07 0.00080 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.61417E-06 0.00044  2.61404E-06 0.00044  2.63967E-06 0.00615 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.99110E-05 0.00097  3.99304E-05 0.00098  3.60871E-05 0.01175 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.12521E-01 0.00036  6.13166E-01 0.00037  5.17316E-01 0.01060 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04526E+01 0.01417 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.47952E+01 0.00050  3.32422E+01 0.00059 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.15783E+04 0.00305  3.06483E+05 0.00160  6.11177E+05 0.00102  6.49250E+05 0.00080  5.98301E+05 0.00066  6.41937E+05 0.00084  4.36357E+05 0.00085  3.85757E+05 0.00074  2.95001E+05 0.00072  2.41124E+05 0.00083  2.07933E+05 0.00079  1.87354E+05 0.00109  1.73330E+05 0.00088  1.64623E+05 0.00074  1.60377E+05 0.00079  1.38743E+05 0.00105  1.37032E+05 0.00082  1.35750E+05 0.00094  1.33523E+05 0.00099  2.60359E+05 0.00049  2.52120E+05 0.00073  1.81599E+05 0.00096  1.17694E+05 0.00089  1.35262E+05 0.00099  1.29487E+05 0.00078  1.14385E+05 0.00078  1.84155E+05 0.00079  4.27137E+04 0.00169  5.22107E+04 0.00123  4.77705E+04 0.00134  2.81380E+04 0.00203  4.84597E+04 0.00184  3.15322E+04 0.00172  2.52730E+04 0.00186  4.17762E+03 0.00314  3.52122E+03 0.00356  3.07652E+03 0.00374  2.99651E+03 0.00423  3.03233E+03 0.00361  3.26769E+03 0.00332  3.97851E+03 0.00351  4.13194E+03 0.00419  8.37260E+03 0.00204  1.40574E+04 0.00218  1.83523E+04 0.00289  4.93552E+04 0.00127  5.18303E+04 0.00135  5.54874E+04 0.00128  3.60669E+04 0.00131  2.53236E+04 0.00173  1.87595E+04 0.00187  2.20976E+04 0.00197  4.35188E+04 0.00161  6.19057E+04 0.00128  1.24619E+05 0.00154  1.99037E+05 0.00141  3.03969E+05 0.00156  1.97229E+05 0.00161  1.41990E+05 0.00162  1.02604E+05 0.00159  9.26142E+04 0.00187  9.13585E+04 0.00196  7.66083E+04 0.00202  5.19520E+04 0.00191  4.80627E+04 0.00223  4.28877E+04 0.00206  3.62815E+04 0.00183  2.87155E+04 0.00190  1.92344E+04 0.00209  6.85921E+03 0.00245 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.17588E-01 0.00071 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.73179E+18 0.00076  4.36484E+17 0.00139 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.40005E-01 0.00012  1.56666E+00 0.00042 ];
INF_CAPT                  (idx, [1:   4]) = [  8.80571E-03 0.00054  3.95919E-02 0.00081 ];
INF_ABS                   (idx, [1:   4]) = [  1.02956E-02 0.00046  6.47840E-02 0.00104 ];
INF_FISS                  (idx, [1:   4]) = [  1.48985E-03 0.00043  2.51921E-02 0.00141 ];
INF_NSF                   (idx, [1:   4]) = [  4.12526E-03 0.00043  6.98708E-02 0.00144 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.76891E+00 7.2E-05  2.77352E+00 5.2E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06646E+02 8.3E-06  2.07015E+02 1.0E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.46305E-08 0.00036  2.60021E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29711E-01 0.00013  1.50185E+00 0.00049 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44033E-01 0.00022  3.97110E-01 0.00054 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63322E-02 0.00030  9.35543E-02 0.00092 ];
INF_SCATT3                (idx, [1:   4]) = [  7.30658E-03 0.00321  2.79558E-02 0.00349 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02937E-02 0.00222 -9.02557E-03 0.00774 ];
INF_SCATT5                (idx, [1:   4]) = [  2.00644E-04 0.08084  6.65910E-03 0.00857 ];
INF_SCATT6                (idx, [1:   4]) = [  5.14403E-03 0.00328 -1.73524E-02 0.00296 ];
INF_SCATT7                (idx, [1:   4]) = [  7.91287E-04 0.01993  4.82446E-04 0.09724 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29755E-01 0.00013  1.50185E+00 0.00049 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44034E-01 0.00022  3.97110E-01 0.00054 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63326E-02 0.00030  9.35543E-02 0.00092 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.30691E-03 0.00320  2.79558E-02 0.00349 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02935E-02 0.00221 -9.02557E-03 0.00774 ];
INF_SCATTP5               (idx, [1:   4]) = [  2.00732E-04 0.08074  6.65910E-03 0.00857 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.14390E-03 0.00327 -1.73524E-02 0.00296 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.91425E-04 0.01993  4.82446E-04 0.09724 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12724E-01 0.00028  1.02059E+00 0.00034 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56698E+00 0.00028  3.26609E-01 0.00034 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.02514E-02 0.00046  6.47840E-02 0.00104 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69980E-02 0.00023  6.59415E-02 0.00120 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.13007E-01 0.00012  1.67040E-02 0.00056  1.13070E-03 0.00382  1.50072E+00 0.00049 ];
INF_S1                    (idx, [1:   8]) = [  2.39223E-01 0.00021  4.81064E-03 0.00094  4.94249E-04 0.00785  3.96616E-01 0.00054 ];
INF_S2                    (idx, [1:   8]) = [  9.78688E-02 0.00031 -1.53654E-03 0.00221  2.66567E-04 0.01196  9.32878E-02 0.00092 ];
INF_S3                    (idx, [1:   8]) = [  9.03183E-03 0.00254 -1.72525E-03 0.00130  9.63134E-05 0.02031  2.78594E-02 0.00349 ];
INF_S4                    (idx, [1:   8]) = [ -9.77071E-03 0.00243 -5.22959E-04 0.00581  3.64531E-07 1.00000 -9.02594E-03 0.00774 ];
INF_S5                    (idx, [1:   8]) = [  1.54469E-04 0.10273  4.61748E-05 0.05792 -3.84128E-05 0.04376  6.69752E-03 0.00857 ];
INF_S6                    (idx, [1:   8]) = [  5.27804E-03 0.00305 -1.34010E-04 0.01811 -5.03222E-05 0.02735 -1.73021E-02 0.00297 ];
INF_S7                    (idx, [1:   8]) = [  9.58634E-04 0.01550 -1.67347E-04 0.01275 -4.40705E-05 0.02669  5.26516E-04 0.08967 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.13051E-01 0.00012  1.67040E-02 0.00056  1.13070E-03 0.00382  1.50072E+00 0.00049 ];
INF_SP1                   (idx, [1:   8]) = [  2.39223E-01 0.00021  4.81064E-03 0.00094  4.94249E-04 0.00785  3.96616E-01 0.00054 ];
INF_SP2                   (idx, [1:   8]) = [  9.78691E-02 0.00031 -1.53654E-03 0.00221  2.66567E-04 0.01196  9.32878E-02 0.00092 ];
INF_SP3                   (idx, [1:   8]) = [  9.03216E-03 0.00254 -1.72525E-03 0.00130  9.63134E-05 0.02031  2.78594E-02 0.00349 ];
INF_SP4                   (idx, [1:   8]) = [ -9.77049E-03 0.00243 -5.22959E-04 0.00581  3.64531E-07 1.00000 -9.02594E-03 0.00774 ];
INF_SP5                   (idx, [1:   8]) = [  1.54557E-04 0.10261  4.61748E-05 0.05792 -3.84128E-05 0.04376  6.69752E-03 0.00857 ];
INF_SP6                   (idx, [1:   8]) = [  5.27791E-03 0.00304 -1.34010E-04 0.01811 -5.03222E-05 0.02735 -1.73021E-02 0.00297 ];
INF_SP7                   (idx, [1:   8]) = [  9.58772E-04 0.01550 -1.67347E-04 0.01275 -4.40705E-05 0.02669  5.26516E-04 0.08967 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32197E-01 0.00064  1.22644E+00 0.01018 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33762E-01 0.00092  1.33409E+00 0.01206 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33650E-01 0.00076  1.34607E+00 0.01368 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29242E-01 0.00098  1.05067E+00 0.00949 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43558E+00 0.00064  2.72461E-01 0.01013 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42598E+00 0.00092  2.50731E-01 0.01206 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42665E+00 0.00076  2.48717E-01 0.01331 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45410E+00 0.00098  3.17936E-01 0.00940 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.68765E-03 0.01015  1.11646E-04 0.06149  8.26940E-04 0.02454  7.60237E-04 0.02469  1.98001E-03 0.01576  7.87533E-04 0.02509  2.21283E-04 0.04616 ];
LAMBDA                    (idx, [1:  14]) = [  7.44005E-01 0.02419  1.26528E-02 0.00159  3.04852E-02 0.00064  1.11559E-01 0.00091  3.23169E-01 0.00063  1.23901E+00 0.00377  8.00712E+00 0.01400 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0004' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:47:07 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:52:32 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292427 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00196E+00  9.96632E-01  1.00448E+00  1.00250E+00  9.94422E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.17734E-02 0.00106  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88227E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04195E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04630E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69216E+00 0.00024  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.46514E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.46426E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.41074E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.25049E-01 0.00109  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001199 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00024E+04 0.00106 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00024E+04 0.00106 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  6.24474E+02 ;
RUNNING_TIME              (idx, 1)        =  1.25410E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.83567E-01  4.83567E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.82517E-01  1.10167E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.23422E+02  3.17410E+00  2.58242E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.10963E+00  2.79500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  6.71500E-02  7.83336E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.25409E+02  1.25409E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97948 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00079E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81582E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.99963E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.69300E+04 ;
TOT_SF_RATE               (idx, 1)        =  6.38181E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.33882E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.65850E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.66076E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.52710E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  8.16289E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.39176E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  7.18655E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.11317E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  9.76340E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.27859E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.35133E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.47903E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.65067E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.69832E+14 ;
CS134_ACTIVITY            (idx, 1)        =  9.91054E+13 ;
CS137_ACTIVITY            (idx, 1)        =  5.31295E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.73340E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.15894E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.16837E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.40929E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.81772E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 20 ;
BURNUP                     (idx, [1:  2])  = [  4.00000E+01  4.00039E+01 ];
BURN_DAYS                 (idx, 1)        =  1.00000E+03 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  8.51203E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  2.60802E+15 0.00203  1.92513E-01 0.00185 ];
U238_FISS                 (idx, [1:   4]) = [  1.30483E+15 0.00288  9.63080E-02 0.00269 ];
PU239_FISS                (idx, [1:   4]) = [  7.25960E+15 0.00107  5.35904E-01 0.00082 ];
PU240_FISS                (idx, [1:   4]) = [  2.14642E+13 0.02105  1.58370E-03 0.02096 ];
PU241_FISS                (idx, [1:   4]) = [  2.29138E+15 0.00209  1.69141E-01 0.00192 ];
U235_CAPT                 (idx, [1:   4]) = [  5.85894E+14 0.00423  1.70909E-02 0.00421 ];
U238_CAPT                 (idx, [1:   4]) = [  1.13292E+16 0.00102  3.30455E-01 0.00075 ];
PU239_CAPT                (idx, [1:   4]) = [  3.97453E+15 0.00155  1.15942E-01 0.00152 ];
PU240_CAPT                (idx, [1:   4]) = [  3.46056E+15 0.00172  1.00938E-01 0.00156 ];
PU241_CAPT                (idx, [1:   4]) = [  8.17075E+14 0.00324  2.38358E-02 0.00325 ];
XE135_CAPT                (idx, [1:   4]) = [  8.14488E+14 0.00348  2.37606E-02 0.00349 ];
SM149_CAPT                (idx, [1:   4]) = [  2.70016E+14 0.00606  7.87713E-03 0.00607 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001199 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.34006E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001199 5.00834E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3552723 3.55795E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1404111 1.40601E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44365 4.43744E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001199 5.00834E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -8.66130E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.78731E+16 2.0E-05  3.78731E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35529E+16 4.1E-06  1.35529E+16 4.1E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.42744E+16 0.00044  2.69820E+16 0.00044  7.29244E+15 0.00131 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.78273E+16 0.00032  4.05349E+16 0.00029  7.29244E+15 0.00131 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.81772E+16 0.00051  4.81772E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.24436E+18 0.00049  5.97112E+17 0.00045  1.64725E+18 0.00056 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.27647E+14 0.00491 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.82549E+16 0.00033 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.67155E+18 0.00069 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.07885E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.07885E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.36132E+00 0.00070 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.75575E-01 0.00038 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.08468E-01 0.00037 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23426E+00 0.00039 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94212E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96895E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  7.92836E-01 0.00072 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  7.85800E-01 0.00072 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.79447E+00 2.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.07265E+02 4.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  7.85808E-01 0.00074  7.82180E-01 0.00072  3.62032E-03 0.01192 ];
IMP_KEFF                  (idx, [1:   2]) = [  7.86210E-01 0.00033 ];
COL_KEFF                  (idx, [1:   2]) = [  7.86222E-01 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  7.86210E-01 0.00033 ];
ABS_KINF                  (idx, [1:   2]) = [  7.93249E-01 0.00032 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.68242E+01 0.00028 ];
IMP_ALF                   (idx, [1:   2]) = [  1.68182E+01 0.00010 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  9.92412E-07 0.00464 ];
IMP_EALF                  (idx, [1:   2]) = [  9.93864E-07 0.00175 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.28459E-01 0.00292 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.30158E-01 0.00118 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.14535E-03 0.00680  1.33689E-04 0.04260  1.16000E-03 0.01562  9.35788E-04 0.01693  2.57457E-03 0.01078  1.04372E-03 0.01621  2.97576E-04 0.03004 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.48717E-01 0.01572  8.28232E-03 0.03260  3.04001E-02 0.00040  1.11634E-01 0.00067  3.23474E-01 0.00041  1.21977E+00 0.00342  7.08894E+00 0.01990 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.62856E-03 0.01029  1.04422E-04 0.07107  8.53778E-04 0.02389  7.20878E-04 0.02663  1.93295E-03 0.01537  7.92993E-04 0.02567  2.23539E-04 0.04548 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.47828E-01 0.02422  1.26697E-02 0.00168  3.03934E-02 0.00059  1.11692E-01 0.00098  3.23424E-01 0.00064  1.22199E+00 0.00400  7.84481E+00 0.01552 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.25364E-05 0.00165  3.25250E-05 0.00166  3.49302E-05 0.02008 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.55605E-05 0.00148  2.55516E-05 0.00149  2.74472E-05 0.02008 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.59990E-03 0.01206  1.05078E-04 0.08367  8.79291E-04 0.02827  6.90158E-04 0.03215  1.90787E-03 0.01914  7.68788E-04 0.03178  2.48723E-04 0.05558 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.06836E-01 0.03199  1.26766E-02 0.00279  3.04041E-02 0.00080  1.12004E-01 0.00134  3.23586E-01 0.00083  1.22717E+00 0.00556  7.94039E+00 0.02225 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.14892E-05 0.00408  3.14862E-05 0.00410  2.80907E-05 0.05708 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.47342E-05 0.00395  2.47321E-05 0.00397  2.20359E-05 0.05679 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.63062E-03 0.04152  1.25265E-04 0.23335  1.01362E-03 0.09405  6.37016E-04 0.11879  1.81016E-03 0.06563  7.57293E-04 0.11297  2.87263E-04 0.16342 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  9.09619E-01 0.09612  1.27389E-02 0.00799  3.03176E-02 0.00177  1.11752E-01 0.00324  3.23506E-01 0.00226  1.21813E+00 0.01414  7.92265E+00 0.04729 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.62796E-03 0.04069  1.22888E-04 0.23719  9.81463E-04 0.09281  6.54226E-04 0.11766  1.84194E-03 0.06523  7.51953E-04 0.11057  2.75484E-04 0.15889 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.95585E-01 0.09362  1.27389E-02 0.00799  3.03189E-02 0.00177  1.11756E-01 0.00324  3.23487E-01 0.00226  1.21919E+00 0.01408  7.89774E+00 0.04742 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.47844E+02 0.04218 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.20198E-05 0.00110 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.51544E-05 0.00081 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.64040E-03 0.00861 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.45037E+02 0.00873 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.62924E-07 0.00082 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.59771E-06 0.00042  2.59759E-06 0.00042  2.62151E-06 0.00625 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.99193E-05 0.00097  3.99401E-05 0.00097  3.57490E-05 0.01164 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.06666E-01 0.00037  6.07390E-01 0.00037  5.01313E-01 0.01069 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07303E+01 0.01512 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.46426E+01 0.00053  3.31803E+01 0.00058 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.19038E+04 0.00295  3.06939E+05 0.00129  6.10453E+05 0.00087  6.50667E+05 0.00089  5.99042E+05 0.00074  6.42419E+05 0.00069  4.36367E+05 0.00070  3.86338E+05 0.00081  2.95278E+05 0.00078  2.41679E+05 0.00078  2.08337E+05 0.00103  1.87818E+05 0.00080  1.73127E+05 0.00094  1.64952E+05 0.00061  1.60643E+05 0.00095  1.38863E+05 0.00087  1.37164E+05 0.00085  1.35847E+05 0.00098  1.33917E+05 0.00094  2.60413E+05 0.00072  2.52216E+05 0.00064  1.81457E+05 0.00091  1.17958E+05 0.00100  1.35228E+05 0.00088  1.29166E+05 0.00110  1.14474E+05 0.00102  1.83532E+05 0.00066  4.27481E+04 0.00137  5.17458E+04 0.00162  4.74858E+04 0.00145  2.79562E+04 0.00216  4.81557E+04 0.00125  3.11091E+04 0.00151  2.48711E+04 0.00182  4.07924E+03 0.00343  3.41900E+03 0.00369  3.00893E+03 0.00339  2.91265E+03 0.00409  2.96337E+03 0.00522  3.21222E+03 0.00432  3.86101E+03 0.00393  4.05808E+03 0.00328  8.24901E+03 0.00257  1.38318E+04 0.00233  1.81121E+04 0.00152  4.85944E+04 0.00119  5.10940E+04 0.00119  5.49047E+04 0.00158  3.56528E+04 0.00178  2.50232E+04 0.00166  1.85644E+04 0.00141  2.19065E+04 0.00141  4.28580E+04 0.00164  6.11057E+04 0.00189  1.23264E+05 0.00137  1.96823E+05 0.00151  3.01125E+05 0.00153  1.95313E+05 0.00157  1.40951E+05 0.00199  1.01748E+05 0.00176  9.16143E+04 0.00205  9.07314E+04 0.00192  7.58954E+04 0.00208  5.15362E+04 0.00203  4.77069E+04 0.00211  4.25793E+04 0.00223  3.60367E+04 0.00202  2.84432E+04 0.00200  1.90823E+04 0.00236  6.77038E+03 0.00237 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  7.93262E-01 0.00051 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.79618E+18 0.00046  4.48225E+17 0.00159 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.40154E-01 9.9E-05  1.56473E+00 0.00042 ];
INF_CAPT                  (idx, [1:   4]) = [  9.01682E-03 0.00063  4.03384E-02 0.00085 ];
INF_ABS                   (idx, [1:   4]) = [  1.04546E-02 0.00057  6.48186E-02 0.00113 ];
INF_FISS                  (idx, [1:   4]) = [  1.43781E-03 0.00042  2.44802E-02 0.00159 ];
INF_NSF                   (idx, [1:   4]) = [  4.00681E-03 0.00046  6.84538E-02 0.00161 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.78674E+00 6.2E-05  2.79629E+00 3.7E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06895E+02 5.2E-06  2.07352E+02 7.4E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.42319E-08 0.00037  2.60159E-06 0.00021 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29700E-01 0.00010  1.49990E+00 0.00049 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43821E-01 0.00018  3.96396E-01 0.00043 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62574E-02 0.00026  9.33643E-02 0.00074 ];
INF_SCATT3                (idx, [1:   4]) = [  7.27822E-03 0.00320  2.80271E-02 0.00238 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03347E-02 0.00170 -8.97702E-03 0.00581 ];
INF_SCATT5                (idx, [1:   4]) = [  1.92167E-04 0.08477  6.66342E-03 0.00886 ];
INF_SCATT6                (idx, [1:   4]) = [  5.11891E-03 0.00322 -1.72235E-02 0.00355 ];
INF_SCATT7                (idx, [1:   4]) = [  7.60844E-04 0.02117  5.12314E-04 0.06385 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29745E-01 0.00010  1.49990E+00 0.00049 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43821E-01 0.00018  3.96396E-01 0.00043 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62574E-02 0.00025  9.33643E-02 0.00074 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.27796E-03 0.00320  2.80271E-02 0.00238 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03350E-02 0.00170 -8.97702E-03 0.00581 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.92143E-04 0.08488  6.66342E-03 0.00886 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.11865E-03 0.00322 -1.72235E-02 0.00355 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.60926E-04 0.02111  5.12314E-04 0.06385 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12846E-01 0.00025  1.02035E+00 0.00043 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56608E+00 0.00025  3.26688E-01 0.00043 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.04099E-02 0.00056  6.48186E-02 0.00113 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69919E-02 0.00026  6.59468E-02 0.00125 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.13162E-01 9.7E-05  1.65387E-02 0.00050  1.12357E-03 0.00484  1.49878E+00 0.00049 ];
INF_S1                    (idx, [1:   8]) = [  2.39059E-01 0.00018  4.76119E-03 0.00113  4.87107E-04 0.00758  3.95909E-01 0.00043 ];
INF_S2                    (idx, [1:   8]) = [  9.77936E-02 0.00026 -1.53623E-03 0.00219  2.65349E-04 0.00801  9.30989E-02 0.00074 ];
INF_S3                    (idx, [1:   8]) = [  8.99351E-03 0.00265 -1.71529E-03 0.00170  9.69103E-05 0.02197  2.79302E-02 0.00236 ];
INF_S4                    (idx, [1:   8]) = [ -9.81882E-03 0.00171 -5.15925E-04 0.00517  1.97938E-06 0.82105 -8.97900E-03 0.00583 ];
INF_S5                    (idx, [1:   8]) = [  1.37833E-04 0.11327  5.43344E-05 0.03965 -3.58021E-05 0.03524  6.69922E-03 0.00882 ];
INF_S6                    (idx, [1:   8]) = [  5.24729E-03 0.00300 -1.28374E-04 0.01773 -4.76987E-05 0.02983 -1.71758E-02 0.00353 ];
INF_S7                    (idx, [1:   8]) = [  9.30300E-04 0.01754 -1.69456E-04 0.01085 -4.51950E-05 0.02996  5.57509E-04 0.05824 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.13206E-01 9.6E-05  1.65387E-02 0.00050  1.12357E-03 0.00484  1.49878E+00 0.00049 ];
INF_SP1                   (idx, [1:   8]) = [  2.39060E-01 0.00018  4.76119E-03 0.00113  4.87107E-04 0.00758  3.95909E-01 0.00043 ];
INF_SP2                   (idx, [1:   8]) = [  9.77937E-02 0.00026 -1.53623E-03 0.00219  2.65349E-04 0.00801  9.30989E-02 0.00074 ];
INF_SP3                   (idx, [1:   8]) = [  8.99325E-03 0.00266 -1.71529E-03 0.00170  9.69103E-05 0.02197  2.79302E-02 0.00236 ];
INF_SP4                   (idx, [1:   8]) = [ -9.81907E-03 0.00171 -5.15925E-04 0.00517  1.97938E-06 0.82105 -8.97900E-03 0.00583 ];
INF_SP5                   (idx, [1:   8]) = [  1.37809E-04 0.11338  5.43344E-05 0.03965 -3.58021E-05 0.03524  6.69922E-03 0.00882 ];
INF_SP6                   (idx, [1:   8]) = [  5.24703E-03 0.00300 -1.28374E-04 0.01773 -4.76987E-05 0.02983 -1.71758E-02 0.00353 ];
INF_SP7                   (idx, [1:   8]) = [  9.30382E-04 0.01749 -1.69456E-04 0.01085 -4.51950E-05 0.02996  5.57509E-04 0.05824 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32109E-01 0.00072  1.21466E+00 0.00763 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33796E-01 0.00099  1.32463E+00 0.01128 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33693E-01 0.00096  1.32571E+00 0.01078 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28916E-01 0.00108  1.04257E+00 0.00480 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43612E+00 0.00072  2.74803E-01 0.00752 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42578E+00 0.00099  2.52401E-01 0.01111 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42641E+00 0.00096  2.52108E-01 0.01033 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45618E+00 0.00108  3.19900E-01 0.00481 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.62856E-03 0.01029  1.04422E-04 0.07107  8.53778E-04 0.02389  7.20878E-04 0.02663  1.93295E-03 0.01537  7.92993E-04 0.02567  2.23539E-04 0.04548 ];
LAMBDA                    (idx, [1:  14]) = [  7.47828E-01 0.02422  1.26697E-02 0.00168  3.03934E-02 0.00059  1.11692E-01 0.00098  3.23424E-01 0.00064  1.22199E+00 0.00400  7.84481E+00 0.01552 ];

