import sys, os
import re
import random
import pickle
import data_store as ds
from decimal import *
getcontext().prec = 3       # Set a new precision to 4

def feq(a,b):
    if abs(a-b)<0.0001:
        return True
    else:
        return False

def up_round(x, base=0.2):
    if feq(Decimal(x) % Decimal(base), Decimal(0.2) ):
        return x
    elif feq(Decimal(x) % Decimal(base), Decimal(0) ):
        return round(x,1)
    elif feq(Decimal(x) % Decimal(base), Decimal(0.1) ):
        return round(x+0.1, 1)
    else:
        print ("Error invalid rounding value..." +str(x))
        print (str(Decimal(x) % Decimal(base)) )
        print(str(Decimal(0.1)))
        print(Decimal(Decimal(x) % Decimal(base)) == Decimal(0.1))
        exit()

def aw_round(x, base=0.2):
    return base * round(x/base)

# 200615 - FMTX burnup ndf
input_test_list =[[1.6, 1.6, 1.6, 1.6, 1.6, 1.6, 2.4, 3.2, 3.2],
 [1.6, 1.6, 1.6, 2.4, 1.6, 1.6, 3.2, 1.6, 1.6],
 [1.6, 1.6, 1.6, 3.2, 1.6, 1.6, 2.4, 1.6, 1.6],
 [1.6, 1.6, 1.6, 3.2, 1.6, 1.6, 3.2, 1.6, 1.6],
 [1.6, 1.6, 2.4, 1.6, 1.6, 1.6, 2.4, 3.2, 3.2],
 [1.6, 1.6, 2.4, 1.6, 1.6, 1.6, 3.2, 1.6, 1.6],
 [1.6, 1.6, 3.2, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6],
 [1.6, 1.6, 3.2, 1.6, 1.6, 1.6, 2.4, 3.2, 1.6],
 [1.6, 1.6, 3.2, 1.6, 1.6, 1.6, 2.4, 3.2, 3.2],
 [1.6, 1.6, 3.2, 1.6, 1.6, 1.6, 3.2, 1.6, 1.6],
 [1.6, 1.6, 3.2, 1.6, 1.6, 1.6, 3.2, 1.6, 2.4],
 [1.6, 1.6, 3.2, 1.6, 1.6, 2.4, 2.4, 1.6, 1.6],
 [1.6, 1.6, 3.2, 2.4, 1.6, 1.6, 1.6, 1.6, 1.6],
 [1.6, 1.6, 3.2, 2.4, 1.6, 1.6, 2.4, 1.6, 1.6],
 [1.6, 1.6, 3.2, 2.4, 1.6, 1.6, 3.2, 1.6, 1.6],
 [1.6, 2.4, 1.6, 2.4, 1.6, 1.6, 1.6, 1.6, 1.6],
 [1.6, 2.4, 1.6, 3.2, 1.6, 1.6, 3.2, 1.6, 1.6],
 [1.6, 3.2, 1.6, 3.2, 1.6, 1.6, 1.6, 1.6, 1.6],
 [2.4, 2.4, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6],
 [2.4, 2.4, 1.6, 2.4, 1.6, 1.6, 1.6, 1.6, 1.6],
 [2.4, 2.4, 1.6, 3.2, 1.6, 1.6, 1.6, 1.6, 1.6],
 [2.4, 3.2, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6],
 [2.4, 3.2, 1.6, 2.4, 1.6, 1.6, 1.6, 1.6, 1.6],
 [2.4, 3.2, 1.6, 3.2, 1.6, 1.6, 1.6, 1.6, 1.6],
 [2.4, 3.2, 1.6, 3.2, 1.6, 1.6, 2.4, 1.6, 1.6],
 [2.4, 3.2, 1.6, 3.2, 1.6, 1.6, 3.2, 1.6, 1.6],
 [2.4, 3.2, 2.4, 3.2, 1.6, 1.6, 1.6, 1.6, 1.6],
 [2.4, 3.2, 3.2, 2.4, 1.6, 1.6, 2.4, 1.6, 1.6],
 [2.4, 3.2, 3.2, 3.2, 1.6, 1.6, 1.6, 1.6, 1.6],
 [2.4, 3.2, 3.2, 3.2, 1.6, 1.6, 2.4, 1.6, 1.6],
 [2.4, 3.2, 3.2, 3.2, 1.6, 1.6, 3.2, 1.6, 1.6],
 [3.2, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6],
 [3.2, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6, 2.4],
 [3.2, 1.6, 1.6, 3.2, 1.6, 1.6, 1.6, 1.6, 1.6],
 [3.2, 2.4, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6],
 [3.2, 2.4, 1.6, 2.4, 1.6, 1.6, 1.6, 1.6, 1.6],
 [3.2, 2.4, 1.6, 3.2, 1.6, 1.6, 1.6, 1.6, 1.6],
 [3.2, 3.2, 1.6, 2.4, 1.6, 1.6, 1.6, 1.6, 1.6],
 [3.2, 3.2, 1.6, 3.2, 1.6, 1.6, 1.6, 1.6, 1.6],
 [3.2, 3.2, 1.6, 3.2, 1.6, 1.6, 1.6, 1.6, 2.4],
 [3.2, 3.2, 1.6, 3.2, 1.6, 1.6, 2.4, 1.6, 1.6],
 [3.2, 3.2, 1.6, 3.2, 1.6, 1.6, 3.2, 1.6, 1.6],
 [3.2, 3.2, 2.4, 3.2, 1.6, 1.6, 1.6, 1.6, 1.6],
 [3.2, 3.2, 2.4, 3.2, 1.6, 1.6, 3.2, 1.6, 1.6],
 [3.2, 3.2, 3.2, 3.2, 1.6, 1.6, 1.6, 1.6, 1.6],
 [3.2, 3.2, 3.2, 3.2, 1.6, 1.6, 2.4, 1.6, 1.6]]

# 200616 - FMTX burnup ndf - tweaked algorithm
input_test_list = [[1.6, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6, 3.2, 3.2],
 [1.6, 1.6, 1.6, 1.6, 1.6, 1.6, 3.2, 3.2, 1.6],
 [1.6, 1.6, 1.6, 2.4, 1.6, 1.6, 2.4, 1.6, 1.6],
 [1.6, 1.6, 1.6, 2.4, 1.6, 1.6, 3.2, 1.6, 1.6],
 [1.6, 1.6, 2.4, 1.6, 1.6, 1.6, 3.2, 1.6, 1.6],
 [1.6, 1.6, 2.4, 2.4, 1.6, 1.6, 3.2, 1.6, 1.6],
 [1.6, 1.6, 3.2, 1.6, 1.6, 1.6, 1.6, 3.2, 3.2],
 [1.6, 1.6, 3.2, 1.6, 1.6, 1.6, 2.4, 3.2, 1.6],
 [1.6, 1.6, 3.2, 1.6, 1.6, 1.6, 2.4, 3.2, 3.2],
 [1.6, 1.6, 3.2, 1.6, 1.6, 1.6, 3.2, 1.6, 1.6],
 [1.6, 1.6, 3.2, 1.6, 1.6, 3.2, 3.2, 3.2, 3.2],
 [1.6, 1.6, 3.2, 2.4, 1.6, 1.6, 1.6, 1.6, 1.6],
 [1.6, 1.6, 3.2, 2.4, 1.6, 1.6, 3.2, 1.6, 1.6],
 [1.6, 2.4, 1.6, 2.4, 1.6, 1.6, 1.6, 1.6, 1.6],
 [1.6, 2.4, 1.6, 2.4, 1.6, 1.6, 3.2, 1.6, 1.6],
 [1.6, 2.4, 2.4, 3.2, 1.6, 1.6, 1.6, 1.6, 1.6],
 [2.4, 2.4, 1.6, 2.4, 1.6, 1.6, 1.6, 1.6, 1.6],
 [2.4, 2.4, 1.6, 3.2, 1.6, 1.6, 1.6, 1.6, 1.6],
 [2.4, 3.2, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6],
 [2.4, 3.2, 1.6, 2.4, 1.6, 1.6, 1.6, 1.6, 1.6],
 [2.4, 3.2, 1.6, 3.2, 1.6, 1.6, 1.6, 1.6, 1.6],
 [2.4, 3.2, 1.6, 3.2, 1.6, 1.6, 2.4, 1.6, 1.6],
 [2.4, 3.2, 1.6, 3.2, 1.6, 1.6, 3.2, 1.6, 1.6],
 [2.4, 3.2, 2.4, 2.4, 1.6, 1.6, 3.2, 1.6, 1.6],
 [2.4, 3.2, 3.2, 3.2, 1.6, 1.6, 1.6, 1.6, 1.6],
 [2.4, 3.2, 3.2, 3.2, 1.6, 1.6, 2.4, 1.6, 1.6],
 [2.4, 3.2, 3.2, 3.2, 1.6, 1.6, 3.2, 1.6, 1.6],
 [3.2, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6],
 [3.2, 1.6, 1.6, 3.2, 1.6, 1.6, 1.6, 1.6, 1.6],
 [3.2, 2.4, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6],
 [3.2, 2.4, 1.6, 2.4, 1.6, 1.6, 1.6, 1.6, 1.6],
 [3.2, 2.4, 1.6, 3.2, 1.6, 1.6, 1.6, 1.6, 1.6],
 [3.2, 3.2, 1.6, 2.4, 1.6, 1.6, 1.6, 1.6, 1.6],
 [3.2, 3.2, 1.6, 3.2, 1.6, 1.6, 1.6, 1.6, 1.6],
 [3.2, 3.2, 1.6, 3.2, 1.6, 1.6, 2.4, 1.6, 1.6],
 [3.2, 3.2, 1.6, 3.2, 1.6, 1.6, 3.2, 1.6, 1.6],
 [3.2, 3.2, 2.4, 3.2, 1.6, 1.6, 1.6, 1.6, 1.6],
 [3.2, 3.2, 2.4, 3.2, 1.6, 1.6, 3.2, 1.6, 1.6],
 [3.2, 3.2, 3.2, 3.2, 1.6, 1.6, 1.6, 1.6, 1.6],
 [3.2, 3.2, 3.2, 3.2, 1.6, 1.6, 2.4, 1.6, 1.6]]

# 200616 - FMTX burnup ndf - tweaked algorithm
input_test_list =    [[1.6, 1.6, 3.2, 1.6, 1.6, 3.2, 3.2, 3.2, 3.2],
 [1.6, 1.6, 3.2, 2.4, 1.6, 3.2, 3.2, 3.2, 3.2],
 [1.6, 1.6, 3.2, 3.2, 1.6, 3.2, 3.2, 3.2, 3.2],
 [1.6, 2.4, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2],
 [1.6, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2],
 [2.4, 1.6, 3.2, 2.4, 3.2, 3.2, 3.2, 3.2, 3.2],
 [2.4, 1.6, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2],
 [2.4, 2.4, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2],
 [2.4, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2],
 [3.2, 3.2, 3.2, 3.2, 1.6, 1.6, 1.6, 3.2, 3.2],
 [3.2, 3.2, 3.2, 3.2, 1.6, 1.6, 3.2, 3.2, 3.2],
 [3.2, 3.2, 3.2, 3.2, 3.2, 1.6, 3.2, 3.2, 3.2],
 [3.2, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2]]

# 200616 - FMTX burnup ndf - fixed for Geoff
input_test_list = [[1.6, 1.6, 3.2, 2.4, 1.6, 3.2,3.2, 3.2, 3.2],
 [1.6, 1.6, 3.2, 2.4, 2.4, 3.2, 3.2, 3.2, 3.2],
 [1.6, 2.4, 3.2, 2.4, 1.6, 3.2, 3.2, 3.2, 3.2],
 [1.6, 2.4, 3.2, 2.4, 2.4, 3.2, 3.2, 3.2, 3.2],
 [1.6, 2.4, 3.2, 3.2, 2.4, 3.2, 3.2, 3.2, 3.2],
 [1.6, 2.4, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2],
 [2.4, 1.6, 3.2, 2.4, 3.2, 3.2, 3.2, 3.2, 3.2],
 [2.4, 2.4, 3.2, 2.4, 3.2, 3.2, 3.2, 3.2, 3.2],
 [2.4, 2.4, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2],
 [2.4, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2],
 [3.2, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2, 3.2]]

def create_input_file(list, num_pins = 9, filename = "default_serpent_input"):
    try:
        file = open("./super_cell", 'r')
    except in_file_error:
        usage_error("input file error: (check path and that it exists!)")
        quit()
    else:
        filedata = file.read()
        for pn in range(1, num_pins+1):
            print ("pin number "+str(pn))
            filedata = filedata.replace('%pin'+str(pn)+'%', "UO2_{:02d}".format(int(list[pn-1] * 10)))
        try:
            file = open(filename, 'w')
        except out_file_error:
            usage_error("file output error")
            quit()
        else:
            file.write(filedata)

def main():
    inputs = ds.data_store()
    num_pins = 9 # number of assemblies (with single pin conc)
    filename = sys.argv[2]
    line_number = int(sys.argv[1])
    if len(sys.argv)  == 4 and sys.argv[3] == 'not_random':
        print("Using test enrichments: "+sys.argv[2])
        use_random = False
    else:
        print("Random output:"+sys.argv[2])
        use_random = True
        print("this file is not for doing random iterations, see the 'per_pin' directory")
        exit()
    inputs.enrichments = []
    try:
        file = open("./super_cell", 'r')
    except in_file_error:
        usage_error("input file error: (check path and that it exists!)")
        quit()
    else:
        filedata = file.read()
        for pn in range(1, num_pins+1):
            if use_random:
                print("random")
                enrichment = aw_round(random.uniform(0.8, 5.0))
            else:
                enrichment = up_round(input_test_list[line_number][pn-1])
            print ("pin number "+str(pn))
            filedata = filedata.replace('%pin'+str(pn)+'%', "UO2_{:02d}".format(int(enrichment * 10)))
            inputs.enrichments.append(enrichment)
        # Write the file out again
        try:
            file = open(filename, 'w')
        except out_file_error:
            usage_error("file output error")
            quit()
        else:
            file.write(filedata)
            try:
                f = open(filename+".pickle", "wb+")  # you should be creating state.pickle here...
            except:
                print ("Error opening output file")
                exit()
            else:
                f.write(pickle.dumps(inputs))
                f.close()

if __name__ == "__main__":
    main()
