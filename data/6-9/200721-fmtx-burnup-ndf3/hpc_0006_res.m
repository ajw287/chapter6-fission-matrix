
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0006' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:46 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Mon Jul 20 23:44:38 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284846 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.93223E-01  9.97862E-01  1.00190E+00  1.00458E+00  1.00243E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.1E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14794E-02 0.00102  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88521E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.93737E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.94166E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.72431E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.67567E+01 0.00041  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.67490E+01 0.00041  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.52431E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.47626E-01 0.00108  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000551 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00011E+04 0.00071 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00011E+04 0.00071 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.76614E+01 ;
RUNNING_TIME              (idx, 1)        =  3.87982E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  4.32900E-01  4.32900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.28333E-03  1.28333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.44537E+00  3.44537E+00  0.00000E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.87943E+00  0.00000E+00 ];
CPU_USAGE                 (idx, 1)        = 4.55213 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99844E+00 0.00035 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  8.76390E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.64301E+08 ;
TOT_DECAY_HEAT            (idx, 1)        =  6.58382E-04 ;
TOT_SF_RATE               (idx, 1)        =  7.42217E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  8.64301E+08 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  6.58382E-04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  7.90874E+03 ;
INGESTION_TOXICITY        (idx, 1)        =  4.17489E+01 ;
ACTINIDE_INH_TOX          (idx, 1)        =  7.90874E+03 ;
ACTINIDE_ING_TOX          (idx, 1)        =  4.17489E+01 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  1.10784E+08 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  8.63924E+08 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  3.54612E+08 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.99035E+12 0.00053  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 0 ;
BURNUP                     (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
BURN_DAYS                 (idx, 1)        =  0.00000E+00 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.02601E-01 0.00110 ];
U235_FISS                 (idx, [1:   4]) = [  1.30372E+16 0.00056  9.40214E-01 0.00017 ];
U238_FISS                 (idx, [1:   4]) = [  8.27630E+14 0.00286  5.96782E-02 0.00270 ];
U235_CAPT                 (idx, [1:   4]) = [  2.78163E+15 0.00154  1.75509E-01 0.00141 ];
U238_CAPT                 (idx, [1:   4]) = [  7.89539E+15 0.00105  4.98130E-01 0.00065 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000551 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.18681E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000551 5.00719E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2646437 2.65005E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2315613 2.31863E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 38501 3.85094E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000551 5.00719E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.79865E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.41072E+16 1.1E-05  3.41072E+16 1.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38681E+16 1.4E-06  1.38681E+16 1.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.58588E+16 0.00050  1.07880E+16 0.00053  5.07082E+15 0.00093 ];
TOT_ABSRATE               (idx, [1:   6]) = [  2.97269E+16 0.00027  2.46561E+16 0.00023  5.07082E+15 0.00093 ];
TOT_SRCRATE               (idx, [1:   6]) = [  2.99035E+16 0.00053  2.99035E+16 0.00053  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.45382E+18 0.00048  3.94399E+17 0.00046  1.05942E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.30344E+14 0.00498 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  2.99572E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.10039E+18 0.00057 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12514E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.78359E+00 0.00038 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.72604E-01 0.00030 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.77104E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23188E+00 0.00033 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95025E-01 3.1E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97259E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.14935E+00 0.00049 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.14050E+00 0.00050 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.45940E+00 1.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02554E+02 1.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.14054E+00 0.00051  1.13238E+00 0.00050  8.11790E-03 0.00762 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.14022E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.14073E+00 0.00053 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.14022E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.14907E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.76220E+01 0.00018 ];
IMP_ALF                   (idx, [1:   2]) = [  1.76213E+01 8.1E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.45683E-07 0.00326 ];
IMP_EALF                  (idx, [1:   2]) = [  4.45054E-07 0.00142 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.02053E-01 0.00293 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.01953E-01 0.00118 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.32103E-03 0.00538  1.75525E-04 0.03216  9.82975E-04 0.01427  9.99602E-04 0.01257  2.89707E-03 0.00773  9.33766E-04 0.01483  3.32091E-04 0.02348 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.34872E-01 0.01249  1.07170E-02 0.01821  3.16569E-02 0.00023  1.10137E-01 0.00027  3.20504E-01 0.00021  1.34621E+00 0.00017  8.68674E+00 0.00658 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.14349E-03 0.00765  1.99086E-04 0.05005  1.10265E-03 0.01977  1.15563E-03 0.01899  3.26206E-03 0.01143  1.06010E-03 0.02042  3.63968E-04 0.03382 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.25401E-01 0.01795  1.24907E-02 2.2E-06  3.16665E-02 0.00031  1.10129E-01 0.00037  3.20660E-01 0.00032  1.34622E+00 0.00024  8.87922E+00 0.00194 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.68058E-05 0.00115  2.67925E-05 0.00115  2.86732E-05 0.01099 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  3.05685E-05 0.00099  3.05533E-05 0.00099  3.27037E-05 0.01101 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.09558E-03 0.00773  1.95670E-04 0.04734  1.09379E-03 0.01978  1.13267E-03 0.01838  3.24714E-03 0.01154  1.06419E-03 0.02155  3.62119E-04 0.03439 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.28862E-01 0.01857  1.24907E-02 3.1E-06  3.16577E-02 0.00034  1.10120E-01 0.00041  3.20631E-01 0.00033  1.34635E+00 0.00027  8.89968E+00 0.00239 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.68268E-05 0.00251  2.68112E-05 0.00253  2.88903E-05 0.02524 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  3.05927E-05 0.00244  3.05749E-05 0.00246  3.29325E-05 0.02517 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.01414E-03 0.02384  1.80933E-04 0.14339  1.13779E-03 0.06109  1.03339E-03 0.06219  3.28109E-03 0.03507  1.05869E-03 0.06346  3.22246E-04 0.12139 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.58778E-01 0.05625  1.24907E-02 7.0E-06  3.16783E-02 0.00076  1.10181E-01 0.00102  3.21330E-01 0.00100  1.34586E+00 0.00062  8.85306E+00 0.00500 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.98732E-03 0.02335  1.84999E-04 0.14212  1.11892E-03 0.05891  1.05925E-03 0.05859  3.24211E-03 0.03403  1.05894E-03 0.06332  3.23107E-04 0.11549 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.68838E-01 0.05571  1.24907E-02 7.0E-06  3.16771E-02 0.00075  1.10161E-01 0.00100  3.21273E-01 0.00097  1.34582E+00 0.00061  8.85053E+00 0.00500 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.62469E+02 0.02396 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.68094E-05 0.00069 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  3.05732E-05 0.00045 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.19635E-03 0.00528 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.68528E+02 0.00537 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.32953E-07 0.00056 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.88459E-06 0.00043  2.88427E-06 0.00043  2.92261E-06 0.00471 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.23280E-05 0.00071  4.23531E-05 0.00071  3.89870E-05 0.00863 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.75032E-01 0.00033  6.74303E-01 0.00033  8.01494E-01 0.00868 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02650E+01 0.01262 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.67490E+01 0.00041  3.62237E+01 0.00044 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.13696E+04 0.00296  2.90371E+05 0.00166  6.02797E+05 0.00083  6.50057E+05 0.00076  5.99734E+05 0.00071  6.44055E+05 0.00086  4.37385E+05 0.00065  3.87120E+05 0.00053  2.96225E+05 0.00082  2.41971E+05 0.00060  2.08748E+05 0.00081  1.87830E+05 0.00091  1.73474E+05 0.00083  1.65327E+05 0.00082  1.60692E+05 0.00104  1.39009E+05 0.00106  1.37050E+05 0.00100  1.36039E+05 0.00094  1.33213E+05 0.00095  2.60615E+05 0.00089  2.51292E+05 0.00074  1.81502E+05 0.00080  1.17401E+05 0.00104  1.35600E+05 0.00080  1.28137E+05 0.00106  1.16582E+05 0.00103  1.91710E+05 0.00076  4.38261E+04 0.00161  5.49772E+04 0.00158  4.99825E+04 0.00157  2.89527E+04 0.00143  5.04303E+04 0.00159  3.42909E+04 0.00177  2.91913E+04 0.00184  5.55779E+03 0.00408  5.50524E+03 0.00404  5.71191E+03 0.00446  5.86207E+03 0.00355  5.78770E+03 0.00366  5.69904E+03 0.00211  5.90062E+03 0.00301  5.53880E+03 0.00434  1.04920E+04 0.00270  1.67100E+04 0.00202  2.12941E+04 0.00149  5.62519E+04 0.00152  5.93556E+04 0.00155  6.50737E+04 0.00130  4.49974E+04 0.00132  3.39859E+04 0.00150  2.63939E+04 0.00144  3.13504E+04 0.00131  5.98784E+04 0.00123  8.14061E+04 0.00128  1.56926E+05 0.00104  2.41850E+05 0.00101  3.61204E+05 0.00108  2.30580E+05 0.00083  1.64958E+05 0.00107  1.18293E+05 0.00102  1.06219E+05 0.00113  1.04352E+05 0.00110  8.69321E+04 0.00137  5.87424E+04 0.00122  5.41766E+04 0.00144  4.81514E+04 0.00156  4.06730E+04 0.00107  3.19629E+04 0.00145  2.13802E+04 0.00147  7.54588E+03 0.00180 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.14959E+00 0.00040 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.11948E+18 0.00045  3.34380E+17 0.00093 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37758E-01 9.6E-05  1.49056E+00 0.00039 ];
INF_CAPT                  (idx, [1:   4]) = [  6.36392E-03 0.00079  2.61238E-02 0.00030 ];
INF_ABS                   (idx, [1:   4]) = [  8.60553E-03 0.00064  6.00996E-02 0.00069 ];
INF_FISS                  (idx, [1:   4]) = [  2.24161E-03 0.00042  3.39758E-02 0.00101 ];
INF_NSF                   (idx, [1:   4]) = [  5.74337E-03 0.00041  8.27887E-02 0.00101 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.56216E+00 6.0E-05  2.43670E+00 5.4E-09 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03840E+02 6.1E-06  2.02270E+02 5.4E-09 ];
INF_INVV                  (idx, [1:   4]) = [  5.91084E-08 0.00031  2.55423E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29161E-01 9.9E-05  1.43047E+00 0.00044 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43767E-01 0.00021  3.77391E-01 0.00050 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61236E-02 0.00033  8.96054E-02 0.00078 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35830E-03 0.00298  2.69500E-02 0.00268 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03215E-02 0.00192 -8.37249E-03 0.00626 ];
INF_SCATT5                (idx, [1:   4]) = [  1.05986E-04 0.15043  6.23181E-03 0.00856 ];
INF_SCATT6                (idx, [1:   4]) = [  5.04291E-03 0.00295 -1.60412E-02 0.00313 ];
INF_SCATT7                (idx, [1:   4]) = [  7.43878E-04 0.01801  3.24115E-04 0.11703 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29200E-01 9.9E-05  1.43047E+00 0.00044 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43767E-01 0.00021  3.77391E-01 0.00050 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61235E-02 0.00033  8.96054E-02 0.00078 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35840E-03 0.00299  2.69500E-02 0.00268 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03214E-02 0.00192 -8.37249E-03 0.00626 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.05925E-04 0.15108  6.23181E-03 0.00856 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.04293E-03 0.00296 -1.60412E-02 0.00313 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.44082E-04 0.01806  3.24115E-04 0.11703 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13961E-01 0.00031  9.63106E-01 0.00033 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55792E+00 0.00031  3.46103E-01 0.00033 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.56714E-03 0.00064  6.00996E-02 0.00069 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69372E-02 0.00022  6.11578E-02 0.00074 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10821E-01 1.0E-04  1.83400E-02 0.00029  1.07009E-03 0.00358  1.42940E+00 0.00044 ];
INF_S1                    (idx, [1:   8]) = [  2.38408E-01 0.00021  5.35912E-03 0.00074  4.54856E-04 0.00619  3.76936E-01 0.00050 ];
INF_S2                    (idx, [1:   8]) = [  9.77035E-02 0.00032 -1.57993E-03 0.00202  2.48746E-04 0.00957  8.93566E-02 0.00078 ];
INF_S3                    (idx, [1:   8]) = [  9.24019E-03 0.00233 -1.88189E-03 0.00221  8.93542E-05 0.01891  2.68607E-02 0.00267 ];
INF_S4                    (idx, [1:   8]) = [ -9.69415E-03 0.00206 -6.27309E-04 0.00464 -2.73962E-08 1.00000 -8.37246E-03 0.00620 ];
INF_S5                    (idx, [1:   8]) = [  9.11829E-05 0.18585  1.48035E-05 0.17531 -3.49013E-05 0.03377  6.26671E-03 0.00849 ];
INF_S6                    (idx, [1:   8]) = [  5.18780E-03 0.00283 -1.44896E-04 0.01802 -4.63270E-05 0.03389 -1.59949E-02 0.00313 ];
INF_S7                    (idx, [1:   8]) = [  9.16507E-04 0.01479 -1.72629E-04 0.01176 -4.16841E-05 0.03320  3.65799E-04 0.10298 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10860E-01 0.00010  1.83400E-02 0.00029  1.07009E-03 0.00358  1.42940E+00 0.00044 ];
INF_SP1                   (idx, [1:   8]) = [  2.38408E-01 0.00021  5.35912E-03 0.00074  4.54856E-04 0.00619  3.76936E-01 0.00050 ];
INF_SP2                   (idx, [1:   8]) = [  9.77034E-02 0.00032 -1.57993E-03 0.00202  2.48746E-04 0.00957  8.93566E-02 0.00078 ];
INF_SP3                   (idx, [1:   8]) = [  9.24029E-03 0.00234 -1.88189E-03 0.00221  8.93542E-05 0.01891  2.68607E-02 0.00267 ];
INF_SP4                   (idx, [1:   8]) = [ -9.69404E-03 0.00206 -6.27309E-04 0.00464 -2.73962E-08 1.00000 -8.37246E-03 0.00620 ];
INF_SP5                   (idx, [1:   8]) = [  9.11219E-05 0.18667  1.48035E-05 0.17531 -3.49013E-05 0.03377  6.26671E-03 0.00849 ];
INF_SP6                   (idx, [1:   8]) = [  5.18783E-03 0.00284 -1.44896E-04 0.01802 -4.63270E-05 0.03389 -1.59949E-02 0.00313 ];
INF_SP7                   (idx, [1:   8]) = [  9.16711E-04 0.01483 -1.72629E-04 0.01176 -4.16841E-05 0.03320  3.65799E-04 0.10298 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32262E-01 0.00055  1.05218E+00 0.00462 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33827E-01 0.00073  1.10925E+00 0.00653 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33738E-01 0.00093  1.11646E+00 0.00572 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29289E-01 0.00108  9.49409E-01 0.00483 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43517E+00 0.00055  3.16963E-01 0.00455 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42557E+00 0.00073  3.00806E-01 0.00645 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42613E+00 0.00093  2.98794E-01 0.00563 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45381E+00 0.00108  3.51290E-01 0.00478 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.14349E-03 0.00765  1.99086E-04 0.05005  1.10265E-03 0.01977  1.15563E-03 0.01899  3.26206E-03 0.01143  1.06010E-03 0.02042  3.63968E-04 0.03382 ];
LAMBDA                    (idx, [1:  14]) = [  8.25401E-01 0.01795  1.24907E-02 2.2E-06  3.16665E-02 0.00031  1.10129E-01 0.00037  3.20660E-01 0.00032  1.34622E+00 0.00024  8.87922E+00 0.00194 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0006' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:46 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Mon Jul 20 23:50:45 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284846 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.93785E-01  9.99029E-01  1.00361E+00  1.00266E+00  1.00091E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13988E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88601E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.95804E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.96233E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.71718E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.64696E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.64617E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.33057E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.38714E-01 0.00112  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000395 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00008E+04 0.00076 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00008E+04 0.00076 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.81574E+01 ;
RUNNING_TIME              (idx, 1)        =  9.98485E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  4.32900E-01  4.32900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.44167E-02  6.58333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.48048E+00  3.39887E+00  2.63625E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.62500E-02  2.95000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.41667E-03  8.83333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.98432E+00  1.26906E+02 ];
CPU_USAGE                 (idx, 1)        = 4.82305 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99811E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.43186E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  7.90163E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.75715E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.42203E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.21593E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  8.62257E+02 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  6.68568E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.67090E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.22498E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  1.75721E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.40503E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  3.52749E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.84472E+06 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  1.40446E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.28326E+11 ;
TE132_ACTIVITY            (idx, 1)        =  2.51443E+14 ;
I131_ACTIVITY             (idx, 1)        =  7.42420E+13 ;
I132_ACTIVITY             (idx, 1)        =  2.43627E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.69840E+07 ;
CS137_ACTIVITY            (idx, 1)        =  1.34675E+11 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  6.58792E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.65010E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.31826E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  9.09050E+16 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.10328E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 1 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E-01  1.00009E-01 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+00 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.14999E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  1.29830E+16 0.00056  9.35826E-01 0.00017 ];
U238_FISS                 (idx, [1:   4]) = [  8.63191E+14 0.00269  6.22130E-02 0.00256 ];
PU239_FISS                (idx, [1:   4]) = [  2.54686E+13 0.01442  1.83614E-03 0.01442 ];
U235_CAPT                 (idx, [1:   4]) = [  2.79151E+15 0.00140  1.64610E-01 0.00128 ];
U238_CAPT                 (idx, [1:   4]) = [  8.08719E+15 0.00102  4.76847E-01 0.00065 ];
PU239_CAPT                (idx, [1:   4]) = [  1.45071E+13 0.02060  8.54569E-04 0.02045 ];
PU240_CAPT                (idx, [1:   4]) = [  2.59719E+11 0.15188  1.53442E-05 0.15164 ];
XE135_CAPT                (idx, [1:   4]) = [  7.17313E+14 0.00281  4.23025E-02 0.00283 ];
SM149_CAPT                (idx, [1:   4]) = [  2.05453E+13 0.01887  1.21118E-03 0.01880 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000395 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.23697E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000395 5.00724E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2728617 2.73243E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2232394 2.23540E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 39384 3.94004E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000395 5.00724E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -9.59262E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.41269E+16 1.1E-05  3.41269E+16 1.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38666E+16 1.3E-06  1.38666E+16 1.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.69455E+16 0.00048  1.17640E+16 0.00046  5.18153E+15 0.00105 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.08121E+16 0.00026  2.56306E+16 0.00021  5.18153E+15 0.00105 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.10328E+16 0.00051  3.10328E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.50074E+18 0.00047  4.06267E+17 0.00044  1.09447E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.44572E+14 0.00535 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.10567E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.13303E+18 0.00061 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12503E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12503E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.69867E+00 0.00040 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.75956E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.76870E-01 0.00030 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24313E+00 0.00033 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94903E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97202E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.10904E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.10030E+00 0.00052 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46108E+00 1.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02575E+02 1.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.10044E+00 0.00054  1.09269E+00 0.00053  7.61215E-03 0.00791 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.10049E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.09985E+00 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.10049E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.10924E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75402E+01 0.00018 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75421E+01 8.0E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.83607E-07 0.00314 ];
IMP_EALF                  (idx, [1:   2]) = [  4.81713E-07 0.00141 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.09825E-01 0.00269 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.09962E-01 0.00114 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.42564E-03 0.00547  1.90112E-04 0.03057  9.93129E-04 0.01373  1.01311E-03 0.01337  2.96915E-03 0.00812  9.58634E-04 0.01394  3.01507E-04 0.02608 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.89755E-01 0.01320  1.12667E-02 0.01476  3.16612E-02 0.00021  1.10214E-01 0.00029  3.20528E-01 0.00023  1.34553E+00 0.00016  8.59668E+00 0.00858 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.97801E-03 0.00793  2.08021E-04 0.04472  1.07260E-03 0.02105  1.13372E-03 0.02042  3.17948E-03 0.01205  1.04865E-03 0.02035  3.35552E-04 0.03796 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.95897E-01 0.01878  1.24907E-02 3.4E-06  3.16591E-02 0.00031  1.10187E-01 0.00040  3.20504E-01 0.00031  1.34570E+00 0.00023  8.88132E+00 0.00205 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.66021E-05 0.00109  2.65899E-05 0.00110  2.83791E-05 0.01139 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.92700E-05 0.00097  2.92566E-05 0.00097  3.12251E-05 0.01137 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.91170E-03 0.00808  2.05537E-04 0.05117  1.05042E-03 0.02122  1.09661E-03 0.01997  3.19379E-03 0.01211  1.03749E-03 0.02132  3.27853E-04 0.03765 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.91757E-01 0.01925  1.24907E-02 3.3E-06  3.16488E-02 0.00039  1.10258E-01 0.00046  3.20564E-01 0.00033  1.34565E+00 0.00026  8.89520E+00 0.00251 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.65326E-05 0.00255  2.65130E-05 0.00254  2.90434E-05 0.02818 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.91924E-05 0.00247  2.91710E-05 0.00247  3.19248E-05 0.02809 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.81300E-03 0.02669  1.96501E-04 0.13916  1.05274E-03 0.06267  1.07961E-03 0.06718  3.07263E-03 0.03609  1.06539E-03 0.07085  3.46133E-04 0.11669 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.96041E-01 0.06038  1.24906E-02 0.0E+00  3.16561E-02 0.00088  1.10365E-01 0.00120  3.20177E-01 0.00092  1.34622E+00 0.00061  8.91100E+00 0.00573 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.77971E-03 0.02582  1.93407E-04 0.13518  1.03459E-03 0.06061  1.05756E-03 0.06527  3.08363E-03 0.03475  1.06574E-03 0.06840  3.44773E-04 0.11675 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.87288E-01 0.05855  1.24906E-02 0.0E+00  3.16550E-02 0.00088  1.10361E-01 0.00118  3.20115E-01 0.00090  1.34630E+00 0.00060  8.91377E+00 0.00576 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.57977E+02 0.02683 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.65651E-05 0.00074 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.92291E-05 0.00052 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.84850E-03 0.00572 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.57885E+02 0.00577 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.22602E-07 0.00061 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.88313E-06 0.00042  2.88276E-06 0.00042  2.93451E-06 0.00518 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.13751E-05 0.00077  4.13944E-05 0.00077  3.86454E-05 0.00882 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.74724E-01 0.00030  6.74185E-01 0.00030  7.70755E-01 0.00889 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.01413E+01 0.01206 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.64617E+01 0.00044  3.55643E+01 0.00044 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.17637E+04 0.00394  2.90913E+05 0.00186  6.02791E+05 0.00090  6.51059E+05 0.00096  5.99435E+05 0.00071  6.42882E+05 0.00051  4.36898E+05 0.00065  3.86957E+05 0.00072  2.95809E+05 0.00087  2.41668E+05 0.00062  2.08533E+05 0.00079  1.87738E+05 0.00090  1.73385E+05 0.00080  1.64905E+05 0.00073  1.60853E+05 0.00077  1.38663E+05 0.00104  1.37068E+05 0.00079  1.35836E+05 0.00098  1.33298E+05 0.00096  2.60195E+05 0.00069  2.50812E+05 0.00061  1.80992E+05 0.00091  1.17294E+05 0.00098  1.35692E+05 0.00097  1.28213E+05 0.00098  1.16512E+05 0.00123  1.91614E+05 0.00066  4.38150E+04 0.00140  5.48357E+04 0.00128  4.99437E+04 0.00149  2.89412E+04 0.00185  5.03260E+04 0.00160  3.42349E+04 0.00182  2.91974E+04 0.00188  5.55788E+03 0.00334  5.52705E+03 0.00252  5.65485E+03 0.00336  5.82514E+03 0.00307  5.82681E+03 0.00391  5.73563E+03 0.00354  5.92910E+03 0.00278  5.55177E+03 0.00349  1.04639E+04 0.00290  1.67541E+04 0.00211  2.12552E+04 0.00206  5.61218E+04 0.00151  5.91575E+04 0.00136  6.49069E+04 0.00123  4.47741E+04 0.00119  3.36893E+04 0.00151  2.60948E+04 0.00167  3.09781E+04 0.00165  5.91286E+04 0.00129  8.00893E+04 0.00132  1.54194E+05 0.00109  2.36884E+05 0.00091  3.52704E+05 0.00087  2.24648E+05 0.00104  1.60562E+05 0.00115  1.15203E+05 0.00119  1.03356E+05 0.00101  1.01631E+05 0.00131  8.47881E+04 0.00131  5.73160E+04 0.00115  5.28936E+04 0.00116  4.69475E+04 0.00118  3.96212E+04 0.00121  3.11533E+04 0.00123  2.08512E+04 0.00128  7.35920E+03 0.00160 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.10858E+00 0.00048 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.16113E+18 0.00047  3.39642E+17 0.00079 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37709E-01 0.00013  1.49774E+00 0.00033 ];
INF_CAPT                  (idx, [1:   4]) = [  6.35925E-03 0.00070  2.81542E-02 0.00030 ];
INF_ABS                   (idx, [1:   4]) = [  8.59812E-03 0.00053  6.13330E-02 0.00058 ];
INF_FISS                  (idx, [1:   4]) = [  2.23887E-03 0.00045  3.31788E-02 0.00082 ];
INF_NSF                   (idx, [1:   4]) = [  5.73795E-03 0.00044  8.08765E-02 0.00082 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.56288E+00 6.8E-05  2.43759E+00 4.2E-07 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03847E+02 6.9E-06  2.02282E+02 6.6E-08 ];
INF_INVV                  (idx, [1:   4]) = [  5.90895E-08 0.00041  2.54919E-06 0.00012 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29102E-01 0.00013  1.43638E+00 0.00036 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43803E-01 0.00020  3.79699E-01 0.00044 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61703E-02 0.00039  9.02265E-02 0.00087 ];
INF_SCATT3                (idx, [1:   4]) = [  7.36953E-03 0.00332  2.71390E-02 0.00219 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02876E-02 0.00170 -8.31197E-03 0.00692 ];
INF_SCATT5                (idx, [1:   4]) = [  1.56157E-04 0.12922  6.28774E-03 0.00811 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07985E-03 0.00407 -1.61072E-02 0.00320 ];
INF_SCATT7                (idx, [1:   4]) = [  7.53085E-04 0.01481  3.64083E-04 0.16023 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29141E-01 0.00013  1.43638E+00 0.00036 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43803E-01 0.00020  3.79699E-01 0.00044 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61705E-02 0.00039  9.02265E-02 0.00087 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.36959E-03 0.00331  2.71390E-02 0.00219 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02879E-02 0.00171 -8.31197E-03 0.00692 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.56237E-04 0.12917  6.28774E-03 0.00811 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07974E-03 0.00407 -1.61072E-02 0.00320 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.52945E-04 0.01480  3.64083E-04 0.16023 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13835E-01 0.00034  9.65902E-01 0.00033 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55884E+00 0.00034  3.45102E-01 0.00033 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.55944E-03 0.00052  6.13330E-02 0.00058 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69485E-02 0.00020  6.24512E-02 0.00067 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10760E-01 0.00013  1.83418E-02 0.00034  1.09313E-03 0.00410  1.43529E+00 0.00036 ];
INF_S1                    (idx, [1:   8]) = [  2.38439E-01 0.00020  5.36363E-03 0.00065  4.63814E-04 0.00616  3.79236E-01 0.00044 ];
INF_S2                    (idx, [1:   8]) = [  9.77447E-02 0.00037 -1.57443E-03 0.00223  2.53692E-04 0.00884  8.99729E-02 0.00087 ];
INF_S3                    (idx, [1:   8]) = [  9.25413E-03 0.00263 -1.88459E-03 0.00149  9.17918E-05 0.02342  2.70472E-02 0.00220 ];
INF_S4                    (idx, [1:   8]) = [ -9.65488E-03 0.00176 -6.32763E-04 0.00412  1.54525E-06 1.00000 -8.31351E-03 0.00689 ];
INF_S5                    (idx, [1:   8]) = [  1.42658E-04 0.13921  1.34992E-05 0.24527 -3.71664E-05 0.04841  6.32491E-03 0.00805 ];
INF_S6                    (idx, [1:   8]) = [  5.22940E-03 0.00392 -1.49552E-04 0.01783 -4.51324E-05 0.02597 -1.60620E-02 0.00318 ];
INF_S7                    (idx, [1:   8]) = [  9.25647E-04 0.01224 -1.72562E-04 0.00980 -4.36395E-05 0.02536  4.07722E-04 0.14259 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10799E-01 0.00013  1.83418E-02 0.00034  1.09313E-03 0.00410  1.43529E+00 0.00036 ];
INF_SP1                   (idx, [1:   8]) = [  2.38439E-01 0.00020  5.36363E-03 0.00065  4.63814E-04 0.00616  3.79236E-01 0.00044 ];
INF_SP2                   (idx, [1:   8]) = [  9.77450E-02 0.00037 -1.57443E-03 0.00223  2.53692E-04 0.00884  8.99729E-02 0.00087 ];
INF_SP3                   (idx, [1:   8]) = [  9.25418E-03 0.00262 -1.88459E-03 0.00149  9.17918E-05 0.02342  2.70472E-02 0.00220 ];
INF_SP4                   (idx, [1:   8]) = [ -9.65509E-03 0.00177 -6.32763E-04 0.00412  1.54525E-06 1.00000 -8.31351E-03 0.00689 ];
INF_SP5                   (idx, [1:   8]) = [  1.42738E-04 0.13916  1.34992E-05 0.24527 -3.71664E-05 0.04841  6.32491E-03 0.00805 ];
INF_SP6                   (idx, [1:   8]) = [  5.22929E-03 0.00392 -1.49552E-04 0.01783 -4.51324E-05 0.02597 -1.60620E-02 0.00318 ];
INF_SP7                   (idx, [1:   8]) = [  9.25507E-04 0.01223 -1.72562E-04 0.00980 -4.36395E-05 0.02536  4.07722E-04 0.14259 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32652E-01 0.00071  1.07287E+00 0.00693 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34238E-01 0.00095  1.13633E+00 0.00861 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34241E-01 0.00105  1.14120E+00 0.00746 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29550E-01 0.00101  9.62264E-01 0.00697 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43277E+00 0.00071  3.11047E-01 0.00685 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42309E+00 0.00095  2.93854E-01 0.00845 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42308E+00 0.00105  2.92483E-01 0.00749 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45215E+00 0.00101  3.46805E-01 0.00689 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.97801E-03 0.00793  2.08021E-04 0.04472  1.07260E-03 0.02105  1.13372E-03 0.02042  3.17948E-03 0.01205  1.04865E-03 0.02035  3.35552E-04 0.03796 ];
LAMBDA                    (idx, [1:  14]) = [  7.95897E-01 0.01878  1.24907E-02 3.4E-06  3.16591E-02 0.00031  1.10187E-01 0.00040  3.20504E-01 0.00031  1.34570E+00 0.00023  8.88132E+00 0.00205 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0006' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:46 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Mon Jul 20 23:56:52 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284846 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.93351E-01  9.98018E-01  1.00415E+00  1.00284E+00  1.00164E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.1E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13951E-02 0.00105  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88605E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.96617E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.97044E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.71329E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.63678E+01 0.00041  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.63598E+01 0.00041  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.25811E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.36848E-01 0.00111  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000582 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00012E+04 0.00075 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00012E+04 0.00075 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  7.87303E+01 ;
RUNNING_TIME              (idx, 1)        =  1.61055E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.32900E-01  4.32900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.02000E-02  7.83333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.55262E+01  3.40442E+00  2.64125E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.14933E-01  2.92167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  6.84999E-03  8.00002E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.61049E+01  1.26161E+02 ];
CPU_USAGE                 (idx, 1)        = 4.88841 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00068E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.59294E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.74934E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.83718E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.43610E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.60633E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.12263E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.14299E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.72489E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.67998E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.01584E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  8.88024E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.55464E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  1.79196E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  2.36037E+07 ;
SR90_ACTIVITY             (idx, 1)        =  6.37643E+11 ;
TE132_ACTIVITY            (idx, 1)        =  5.63436E+14 ;
I131_ACTIVITY             (idx, 1)        =  2.65371E+14 ;
I132_ACTIVITY             (idx, 1)        =  5.66370E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.92933E+09 ;
CS137_ACTIVITY            (idx, 1)        =  6.73854E+11 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.67799E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.63899E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  6.78865E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.12171E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.13404E+12 0.00047  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 2 ;
BURNUP                     (idx, [1:  2])  = [  5.00000E-01  5.00045E-01 ];
BURN_DAYS                 (idx, 1)        =  1.25000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.14362E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  1.26826E+16 0.00057  9.14795E-01 0.00021 ];
U238_FISS                 (idx, [1:   4]) = [  8.72493E+14 0.00290  6.29244E-02 0.00275 ];
PU239_FISS                (idx, [1:   4]) = [  3.06481E+14 0.00464  2.21047E-02 0.00457 ];
PU240_FISS                (idx, [1:   4]) = [  1.86962E+10 0.57630  1.36472E-06 0.57637 ];
PU241_FISS                (idx, [1:   4]) = [  1.49673E+11 0.20819  1.07968E-05 0.20838 ];
U235_CAPT                 (idx, [1:   4]) = [  2.73130E+15 0.00152  1.58127E-01 0.00140 ];
U238_CAPT                 (idx, [1:   4]) = [  8.11064E+15 0.00098  4.69542E-01 0.00066 ];
PU239_CAPT                (idx, [1:   4]) = [  1.72163E+14 0.00599  9.96859E-03 0.00601 ];
PU240_CAPT                (idx, [1:   4]) = [  8.00179E+12 0.02739  4.63234E-04 0.02739 ];
PU241_CAPT                (idx, [1:   4]) = [  6.91057E+10 0.29851  4.02285E-06 0.29854 ];
XE135_CAPT                (idx, [1:   4]) = [  7.20264E+14 0.00294  4.16998E-02 0.00289 ];
SM149_CAPT                (idx, [1:   4]) = [  1.21892E+14 0.00691  7.05697E-03 0.00689 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000582 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.24372E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000582 5.00724E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2751975 2.75570E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2208999 2.21193E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 39608 3.96154E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000582 5.00724E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.00469E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.42324E+16 1.2E-05  3.42324E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38586E+16 1.4E-06  1.38586E+16 1.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.72875E+16 0.00045  1.20832E+16 0.00047  5.20434E+15 0.00095 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.11461E+16 0.00025  2.59418E+16 0.00022  5.20434E+15 0.00095 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.13404E+16 0.00047  3.13404E+16 0.00047  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.51265E+18 0.00044  4.09132E+17 0.00044  1.10352E+18 0.00048 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.48355E+14 0.00519 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.13945E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.14107E+18 0.00055 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12456E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12456E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.68251E+00 0.00043 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.77726E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.76871E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24369E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94877E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97186E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.10148E+00 0.00051 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.09275E+00 0.00051 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.47012E+00 1.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02692E+02 1.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.09253E+00 0.00052  1.08513E+00 0.00052  7.62203E-03 0.00795 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.09201E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.09240E+00 0.00046 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.09201E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.10073E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75134E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75168E+01 7.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.97067E-07 0.00352 ];
IMP_EALF                  (idx, [1:   2]) = [  4.94060E-07 0.00139 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.12519E-01 0.00294 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.12472E-01 0.00119 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.44278E-03 0.00564  1.85681E-04 0.03125  1.03249E-03 0.01313  1.00462E-03 0.01356  2.95064E-03 0.00865  9.61089E-04 0.01424  3.08270E-04 0.02348 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.98534E-01 0.01231  1.08169E-02 0.01761  3.16150E-02 0.00023  1.10227E-01 0.00028  3.20747E-01 0.00021  1.34519E+00 0.00017  8.56928E+00 0.00906 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.01341E-03 0.00773  1.96794E-04 0.04549  1.11328E-03 0.01939  1.10921E-03 0.01919  3.21374E-03 0.01244  1.05429E-03 0.02112  3.26099E-04 0.03501 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.84857E-01 0.01742  1.24907E-02 4.9E-06  3.16198E-02 0.00034  1.10251E-01 0.00043  3.20865E-01 0.00034  1.34506E+00 0.00025  8.92379E+00 0.00214 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.64655E-05 0.00119  2.64542E-05 0.00119  2.81606E-05 0.01141 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.89104E-05 0.00106  2.88981E-05 0.00107  3.07652E-05 0.01142 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.96231E-03 0.00797  2.18396E-04 0.04451  1.12204E-03 0.02002  1.06305E-03 0.02097  3.18355E-03 0.01241  1.05509E-03 0.02188  3.20186E-04 0.03741 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.89058E-01 0.01993  1.24906E-02 6.2E-06  3.16300E-02 0.00039  1.10256E-01 0.00052  3.20867E-01 0.00034  1.34519E+00 0.00029  8.94309E+00 0.00273 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.63844E-05 0.00249  2.63781E-05 0.00250  2.73373E-05 0.02777 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.88220E-05 0.00243  2.88152E-05 0.00245  2.98550E-05 0.02781 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.69269E-03 0.02570  2.10149E-04 0.14334  1.06141E-03 0.06148  1.04490E-03 0.06471  3.08071E-03 0.03848  9.84790E-04 0.06762  3.10740E-04 0.11000 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.94903E-01 0.05967  1.24909E-02 8.8E-06  3.16136E-02 0.00093  1.10135E-01 0.00105  3.21072E-01 0.00106  1.34555E+00 0.00063  8.87477E+00 0.00527 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.68686E-03 0.02480  2.17443E-04 0.14051  1.07391E-03 0.05970  1.01569E-03 0.06118  3.05491E-03 0.03777  1.00442E-03 0.06389  3.20495E-04 0.10718 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.91421E-01 0.05701  1.24909E-02 8.8E-06  3.16118E-02 0.00093  1.10143E-01 0.00106  3.21185E-01 0.00106  1.34557E+00 0.00062  8.87306E+00 0.00526 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.54083E+02 0.02571 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.64124E-05 0.00066 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.88527E-05 0.00044 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.93451E-03 0.00440 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.62629E+02 0.00449 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.18766E-07 0.00059 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87938E-06 0.00040  2.87923E-06 0.00040  2.90079E-06 0.00496 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.10093E-05 0.00072  4.10324E-05 0.00072  3.79174E-05 0.00906 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.74719E-01 0.00031  6.74185E-01 0.00031  7.69070E-01 0.00879 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03504E+01 0.01312 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.63598E+01 0.00041  3.53684E+01 0.00045 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.21114E+04 0.00353  2.91154E+05 0.00163  6.03861E+05 0.00083  6.50110E+05 0.00059  5.98501E+05 0.00074  6.44124E+05 0.00087  4.37168E+05 0.00071  3.86383E+05 0.00069  2.95994E+05 0.00076  2.41794E+05 0.00082  2.08414E+05 0.00098  1.87833E+05 0.00079  1.73471E+05 0.00098  1.64875E+05 0.00076  1.60499E+05 0.00087  1.38899E+05 0.00090  1.36947E+05 0.00083  1.35699E+05 0.00109  1.33266E+05 0.00078  2.60546E+05 0.00095  2.51273E+05 0.00101  1.81257E+05 0.00075  1.17405E+05 0.00115  1.35536E+05 0.00083  1.27981E+05 0.00084  1.16297E+05 0.00102  1.91268E+05 0.00085  4.36463E+04 0.00187  5.48964E+04 0.00135  4.98918E+04 0.00137  2.89020E+04 0.00164  5.03619E+04 0.00158  3.42547E+04 0.00213  2.92521E+04 0.00205  5.56099E+03 0.00382  5.53964E+03 0.00392  5.63376E+03 0.00459  5.82038E+03 0.00327  5.76563E+03 0.00313  5.66418E+03 0.00346  5.87187E+03 0.00288  5.55564E+03 0.00349  1.04412E+04 0.00351  1.66852E+04 0.00205  2.12512E+04 0.00202  5.61464E+04 0.00147  5.92436E+04 0.00168  6.49763E+04 0.00128  4.43901E+04 0.00102  3.33497E+04 0.00170  2.57957E+04 0.00205  3.05099E+04 0.00145  5.82696E+04 0.00112  7.93723E+04 0.00086  1.52783E+05 0.00093  2.34650E+05 0.00084  3.49278E+05 0.00094  2.22264E+05 0.00067  1.58837E+05 0.00109  1.14070E+05 0.00093  1.02336E+05 0.00111  1.00850E+05 0.00141  8.41230E+04 0.00117  5.68330E+04 0.00127  5.24175E+04 0.00137  4.65799E+04 0.00113  3.93941E+04 0.00116  3.10158E+04 0.00133  2.07635E+04 0.00145  7.33440E+03 0.00159 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.10112E+00 0.00045 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.17273E+18 0.00040  3.39955E+17 0.00085 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37753E-01 0.00010  1.50153E+00 0.00033 ];
INF_CAPT                  (idx, [1:   4]) = [  6.37729E-03 0.00053  2.88549E-02 0.00032 ];
INF_ABS                   (idx, [1:   4]) = [  8.60106E-03 0.00041  6.19545E-02 0.00060 ];
INF_FISS                  (idx, [1:   4]) = [  2.22377E-03 0.00039  3.30996E-02 0.00086 ];
INF_NSF                   (idx, [1:   4]) = [  5.70751E-03 0.00042  8.10198E-02 0.00086 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.56659E+00 4.5E-05  2.44775E+00 5.5E-06 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03893E+02 4.7E-06  2.02414E+02 8.6E-07 ];
INF_INVV                  (idx, [1:   4]) = [  5.90373E-08 0.00041  2.54973E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29147E-01 0.00011  1.43963E+00 0.00037 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43844E-01 0.00017  3.80702E-01 0.00043 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61874E-02 0.00025  9.04894E-02 0.00081 ];
INF_SCATT3                (idx, [1:   4]) = [  7.39176E-03 0.00324  2.71246E-02 0.00256 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02921E-02 0.00211 -8.32436E-03 0.00749 ];
INF_SCATT5                (idx, [1:   4]) = [  1.35250E-04 0.13196  6.29443E-03 0.00807 ];
INF_SCATT6                (idx, [1:   4]) = [  5.05701E-03 0.00319 -1.61138E-02 0.00274 ];
INF_SCATT7                (idx, [1:   4]) = [  7.41367E-04 0.02033  3.15357E-04 0.09366 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29186E-01 0.00011  1.43963E+00 0.00037 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43845E-01 0.00017  3.80702E-01 0.00043 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61876E-02 0.00025  9.04894E-02 0.00081 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.39183E-03 0.00325  2.71246E-02 0.00256 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02920E-02 0.00211 -8.32436E-03 0.00749 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.35160E-04 0.13203  6.29443E-03 0.00807 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.05713E-03 0.00319 -1.61138E-02 0.00274 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.41232E-04 0.02035  3.15357E-04 0.09366 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13805E-01 0.00025  9.68360E-01 0.00034 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55906E+00 0.00025  3.44225E-01 0.00034 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.56234E-03 0.00040  6.19545E-02 0.00060 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69475E-02 0.00025  6.30151E-02 0.00062 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10805E-01 0.00010  1.83418E-02 0.00040  1.10836E-03 0.00540  1.43852E+00 0.00037 ];
INF_S1                    (idx, [1:   8]) = [  2.38479E-01 0.00018  5.36533E-03 0.00083  4.72384E-04 0.00522  3.80230E-01 0.00043 ];
INF_S2                    (idx, [1:   8]) = [  9.77630E-02 0.00026 -1.57567E-03 0.00273  2.56361E-04 0.00731  9.02330E-02 0.00081 ];
INF_S3                    (idx, [1:   8]) = [  9.28112E-03 0.00246 -1.88937E-03 0.00203  8.89272E-05 0.02075  2.70357E-02 0.00257 ];
INF_S4                    (idx, [1:   8]) = [ -9.65613E-03 0.00227 -6.35919E-04 0.00580  1.12662E-06 1.00000 -8.32549E-03 0.00745 ];
INF_S5                    (idx, [1:   8]) = [  1.23600E-04 0.13583  1.16506E-05 0.22698 -3.27462E-05 0.04658  6.32718E-03 0.00800 ];
INF_S6                    (idx, [1:   8]) = [  5.20389E-03 0.00294 -1.46880E-04 0.01722 -4.36386E-05 0.03330 -1.60702E-02 0.00274 ];
INF_S7                    (idx, [1:   8]) = [  9.12253E-04 0.01719 -1.70886E-04 0.01522 -4.08634E-05 0.02653  3.56220E-04 0.08276 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10844E-01 0.00010  1.83418E-02 0.00040  1.10836E-03 0.00540  1.43852E+00 0.00037 ];
INF_SP1                   (idx, [1:   8]) = [  2.38480E-01 0.00018  5.36533E-03 0.00083  4.72384E-04 0.00522  3.80230E-01 0.00043 ];
INF_SP2                   (idx, [1:   8]) = [  9.77633E-02 0.00026 -1.57567E-03 0.00273  2.56361E-04 0.00731  9.02330E-02 0.00081 ];
INF_SP3                   (idx, [1:   8]) = [  9.28120E-03 0.00247 -1.88937E-03 0.00203  8.89272E-05 0.02075  2.70357E-02 0.00257 ];
INF_SP4                   (idx, [1:   8]) = [ -9.65612E-03 0.00227 -6.35919E-04 0.00580  1.12662E-06 1.00000 -8.32549E-03 0.00745 ];
INF_SP5                   (idx, [1:   8]) = [  1.23510E-04 0.13592  1.16506E-05 0.22698 -3.27462E-05 0.04658  6.32718E-03 0.00800 ];
INF_SP6                   (idx, [1:   8]) = [  5.20401E-03 0.00294 -1.46880E-04 0.01722 -4.36386E-05 0.03330 -1.60702E-02 0.00274 ];
INF_SP7                   (idx, [1:   8]) = [  9.12119E-04 0.01721 -1.70886E-04 0.01522 -4.08634E-05 0.02653  3.56220E-04 0.08276 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32648E-01 0.00056  1.06356E+00 0.00518 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34305E-01 0.00063  1.12879E+00 0.00767 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34176E-01 0.00076  1.12601E+00 0.00632 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29532E-01 0.00103  9.56229E-01 0.00531 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43279E+00 0.00056  3.13614E-01 0.00517 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42266E+00 0.00063  2.95702E-01 0.00737 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42345E+00 0.00076  2.96316E-01 0.00633 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45227E+00 0.00103  3.48824E-01 0.00523 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.01341E-03 0.00773  1.96794E-04 0.04549  1.11328E-03 0.01939  1.10921E-03 0.01919  3.21374E-03 0.01244  1.05429E-03 0.02112  3.26099E-04 0.03501 ];
LAMBDA                    (idx, [1:  14]) = [  7.84857E-01 0.01742  1.24907E-02 4.9E-06  3.16198E-02 0.00034  1.10251E-01 0.00043  3.20865E-01 0.00034  1.34506E+00 0.00025  8.92379E+00 0.00214 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0006' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:46 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:02:59 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284846 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.95114E-01  9.97224E-01  1.00417E+00  1.00146E+00  1.00203E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.0E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14012E-02 0.00105  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88599E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.97296E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.97725E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.71041E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.61702E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.61623E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.16894E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.33633E-01 0.00109  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000772 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00015E+04 0.00074 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00015E+04 0.00074 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.09284E+02 ;
RUNNING_TIME              (idx, 1)        =  2.22227E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.32900E-01  4.32900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  4.71333E-02  8.46667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  2.15664E+01  3.39148E+00  2.64877E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.74350E-01  2.94167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.03833E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  2.22226E+01  1.26415E+02 ];
CPU_USAGE                 (idx, 1)        = 4.91769 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99883E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.66521E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.95491E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.85310E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.49111E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.63381E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.14090E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.32109E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.73898E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  3.55002E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.49114E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.02132E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.75475E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  2.52870E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  2.81567E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.26340E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.04732E+14 ;
I131_ACTIVITY             (idx, 1)        =  3.60124E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.09994E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.71703E+10 ;
CS137_ACTIVITY            (idx, 1)        =  1.34863E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.87400E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.61910E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.50866E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.16014E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.16259E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 3 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E+00  1.00009E+00 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.14848E-01 0.00104 ];
U235_FISS                 (idx, [1:   4]) = [  1.22870E+16 0.00060  8.87332E-01 0.00025 ];
U238_FISS                 (idx, [1:   4]) = [  8.77374E+14 0.00277  6.33514E-02 0.00260 ];
PU239_FISS                (idx, [1:   4]) = [  6.78363E+14 0.00315  4.89910E-02 0.00312 ];
PU240_FISS                (idx, [1:   4]) = [  9.46720E+10 0.25457  6.85403E-06 0.25457 ];
PU241_FISS                (idx, [1:   4]) = [  1.63763E+12 0.06371  1.18212E-04 0.06373 ];
U235_CAPT                 (idx, [1:   4]) = [  2.65694E+15 0.00145  1.51177E-01 0.00137 ];
U238_CAPT                 (idx, [1:   4]) = [  8.15116E+15 0.00101  4.63747E-01 0.00065 ];
PU239_CAPT                (idx, [1:   4]) = [  3.84139E+14 0.00391  2.18553E-02 0.00385 ];
PU240_CAPT                (idx, [1:   4]) = [  3.54882E+13 0.01329  2.01936E-03 0.01331 ];
PU241_CAPT                (idx, [1:   4]) = [  5.80495E+11 0.10159  3.31389E-05 0.10164 ];
XE135_CAPT                (idx, [1:   4]) = [  7.21435E+14 0.00288  4.10501E-02 0.00286 ];
SM149_CAPT                (idx, [1:   4]) = [  1.49841E+14 0.00661  8.52530E-03 0.00656 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000772 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.54696E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000772 5.00755E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2774917 2.77872E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2186367 2.18933E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 39488 3.94916E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000772 5.00755E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -2.14204E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.43731E+16 1.2E-05  3.43731E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38479E+16 1.6E-06  1.38479E+16 1.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.75539E+16 0.00049  1.23598E+16 0.00052  5.19411E+15 0.00105 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.14019E+16 0.00027  2.62078E+16 0.00024  5.19411E+15 0.00105 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.16259E+16 0.00052  3.16259E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.52157E+18 0.00049  4.11314E+17 0.00049  1.11026E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.49828E+14 0.00507 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.16517E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.14522E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12398E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12398E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.67181E+00 0.00041 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.79529E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.76186E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24314E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94856E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97232E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.09541E+00 0.00051 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.08676E+00 0.00051 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.48218E+00 1.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02849E+02 1.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.08674E+00 0.00053  1.07941E+00 0.00051  7.34384E-03 0.00875 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.08764E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.08701E+00 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.08764E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.09630E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74922E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74904E+01 8.4E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.07535E-07 0.00332 ];
IMP_EALF                  (idx, [1:   2]) = [  5.07325E-07 0.00148 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.14177E-01 0.00278 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.15047E-01 0.00110 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.36477E-03 0.00607  1.82102E-04 0.03164  1.04285E-03 0.01428  9.79803E-04 0.01443  2.90941E-03 0.00816  9.52557E-04 0.01421  2.98051E-04 0.02521 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.86047E-01 0.01287  1.09668E-02 0.01669  3.15733E-02 0.00028  1.10196E-01 0.00031  3.20747E-01 0.00022  1.34528E+00 0.00017  8.51607E+00 0.00977 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.83579E-03 0.00874  1.99320E-04 0.04835  1.10127E-03 0.02086  1.06044E-03 0.02119  3.13301E-03 0.01219  1.02272E-03 0.02116  3.19046E-04 0.03665 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.84529E-01 0.01791  1.24906E-02 5.1E-06  3.15743E-02 0.00036  1.10226E-01 0.00045  3.20702E-01 0.00034  1.34486E+00 0.00025  8.92386E+00 0.00223 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.61673E-05 0.00113  2.61587E-05 0.00113  2.74113E-05 0.01150 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.84331E-05 0.00100  2.84237E-05 0.00100  2.97865E-05 0.01150 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.74085E-03 0.00893  1.77837E-04 0.05140  1.11554E-03 0.02270  1.04851E-03 0.02156  3.07536E-03 0.01241  9.97327E-04 0.02241  3.26273E-04 0.03860 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.01391E-01 0.02018  1.24906E-02 7.0E-06  3.15635E-02 0.00046  1.10192E-01 0.00051  3.20724E-01 0.00037  1.34452E+00 0.00029  8.92230E+00 0.00270 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.61417E-05 0.00255  2.61211E-05 0.00256  2.85273E-05 0.02669 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.84050E-05 0.00249  2.83828E-05 0.00250  3.09945E-05 0.02668 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.71195E-03 0.02564  1.60933E-04 0.17768  1.10262E-03 0.06567  9.62084E-04 0.06633  3.00052E-03 0.03936  1.11263E-03 0.07025  3.73167E-04 0.11897 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.79494E-01 0.06536  1.24906E-02 1.8E-05  3.15179E-02 0.00111  1.10297E-01 0.00125  3.20793E-01 0.00105  1.34620E+00 0.00061  9.01411E+00 0.00636 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.78086E-03 0.02534  1.64627E-04 0.17258  1.08423E-03 0.06438  9.78652E-04 0.06475  3.04491E-03 0.03870  1.13140E-03 0.06743  3.77042E-04 0.12061 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.81997E-01 0.06525  1.24906E-02 1.8E-05  3.15203E-02 0.00111  1.10294E-01 0.00125  3.20704E-01 0.00101  1.34627E+00 0.00061  9.01371E+00 0.00637 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.57108E+02 0.02572 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.61077E-05 0.00074 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.83681E-05 0.00049 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.76894E-03 0.00484 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.59254E+02 0.00475 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.12732E-07 0.00065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87452E-06 0.00042  2.87432E-06 0.00042  2.90389E-06 0.00488 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.04937E-05 0.00080  4.05172E-05 0.00080  3.72131E-05 0.00854 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.74016E-01 0.00033  6.73535E-01 0.00033  7.62967E-01 0.00937 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03615E+01 0.01267 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.61623E+01 0.00045  3.51443E+01 0.00043 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.24372E+04 0.00299  2.92460E+05 0.00159  6.04045E+05 0.00066  6.51166E+05 0.00073  5.98229E+05 0.00075  6.43020E+05 0.00069  4.36905E+05 0.00069  3.86786E+05 0.00081  2.95776E+05 0.00074  2.41961E+05 0.00078  2.08193E+05 0.00084  1.87711E+05 0.00077  1.73214E+05 0.00093  1.64721E+05 0.00087  1.60634E+05 0.00078  1.38631E+05 0.00121  1.36914E+05 0.00100  1.35768E+05 0.00094  1.33529E+05 0.00083  2.60146E+05 0.00091  2.50888E+05 0.00083  1.81415E+05 0.00104  1.17322E+05 0.00093  1.35420E+05 0.00085  1.27961E+05 0.00079  1.16423E+05 0.00100  1.91371E+05 0.00060  4.37159E+04 0.00139  5.49399E+04 0.00144  4.98386E+04 0.00184  2.89130E+04 0.00175  5.01609E+04 0.00166  3.42130E+04 0.00167  2.90958E+04 0.00233  5.56243E+03 0.00310  5.49904E+03 0.00286  5.59599E+03 0.00266  5.75623E+03 0.00313  5.70964E+03 0.00383  5.65806E+03 0.00428  5.90094E+03 0.00389  5.48643E+03 0.00302  1.04233E+04 0.00272  1.66592E+04 0.00226  2.12792E+04 0.00217  5.59485E+04 0.00148  5.91495E+04 0.00128  6.45234E+04 0.00152  4.40224E+04 0.00170  3.27249E+04 0.00180  2.51314E+04 0.00182  2.97763E+04 0.00132  5.70261E+04 0.00138  7.78559E+04 0.00145  1.50168E+05 0.00122  2.31290E+05 0.00114  3.44223E+05 0.00134  2.19330E+05 0.00149  1.56746E+05 0.00121  1.12548E+05 0.00162  1.01052E+05 0.00141  9.95165E+04 0.00165  8.30582E+04 0.00140  5.60799E+04 0.00182  5.17551E+04 0.00178  4.59676E+04 0.00169  3.88649E+04 0.00163  3.05863E+04 0.00163  2.04596E+04 0.00176  7.23021E+03 0.00234 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.09566E+00 0.00044 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.18335E+18 0.00042  3.38252E+17 0.00105 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37667E-01 0.00011  1.50480E+00 0.00040 ];
INF_CAPT                  (idx, [1:   4]) = [  6.41130E-03 0.00054  2.94692E-02 0.00041 ];
INF_ABS                   (idx, [1:   4]) = [  8.61856E-03 0.00046  6.26933E-02 0.00075 ];
INF_FISS                  (idx, [1:   4]) = [  2.20726E-03 0.00043  3.32241E-02 0.00105 ];
INF_NSF                   (idx, [1:   4]) = [  5.67626E-03 0.00044  8.17774E-02 0.00106 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.57163E+00 4.8E-05  2.46139E+00 1.5E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03953E+02 5.4E-06  2.02592E+02 2.4E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.89654E-08 0.00041  2.55015E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29041E-01 0.00011  1.44207E+00 0.00046 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43810E-01 0.00019  3.81473E-01 0.00058 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61471E-02 0.00033  9.06169E-02 0.00109 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34828E-03 0.00372  2.72302E-02 0.00194 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03301E-02 0.00200 -8.35367E-03 0.00821 ];
INF_SCATT5                (idx, [1:   4]) = [  1.31155E-04 0.15974  6.26216E-03 0.00675 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07568E-03 0.00368 -1.62751E-02 0.00272 ];
INF_SCATT7                (idx, [1:   4]) = [  7.45351E-04 0.02635  3.34214E-04 0.12868 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29081E-01 0.00011  1.44207E+00 0.00046 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43811E-01 0.00019  3.81473E-01 0.00058 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61474E-02 0.00033  9.06169E-02 0.00109 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34832E-03 0.00373  2.72302E-02 0.00194 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03301E-02 0.00200 -8.35367E-03 0.00821 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.31117E-04 0.15980  6.26216E-03 0.00675 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07561E-03 0.00369 -1.62751E-02 0.00272 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.45225E-04 0.02631  3.34214E-04 0.12868 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13608E-01 0.00033  9.71052E-01 0.00032 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56050E+00 0.00033  3.43271E-01 0.00032 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.57822E-03 0.00043  6.26933E-02 0.00075 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69502E-02 0.00019  6.38493E-02 0.00107 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10717E-01 0.00011  1.83241E-02 0.00032  1.12498E-03 0.00429  1.44095E+00 0.00046 ];
INF_S1                    (idx, [1:   8]) = [  2.38460E-01 0.00019  5.34998E-03 0.00068  4.81102E-04 0.00782  3.80992E-01 0.00058 ];
INF_S2                    (idx, [1:   8]) = [  9.77266E-02 0.00032 -1.57954E-03 0.00247  2.59516E-04 0.01063  9.03573E-02 0.00110 ];
INF_S3                    (idx, [1:   8]) = [  9.23541E-03 0.00280 -1.88713E-03 0.00188  9.39841E-05 0.01973  2.71362E-02 0.00197 ];
INF_S4                    (idx, [1:   8]) = [ -9.69288E-03 0.00207 -6.37207E-04 0.00395  6.64255E-07 1.00000 -8.35434E-03 0.00825 ];
INF_S5                    (idx, [1:   8]) = [  1.16657E-04 0.17458  1.44979E-05 0.14462 -3.74275E-05 0.04215  6.29958E-03 0.00682 ];
INF_S6                    (idx, [1:   8]) = [  5.21592E-03 0.00354 -1.40240E-04 0.01658 -4.79588E-05 0.02834 -1.62272E-02 0.00276 ];
INF_S7                    (idx, [1:   8]) = [  9.13868E-04 0.02178 -1.68516E-04 0.01448 -4.48748E-05 0.02865  3.79089E-04 0.11444 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10757E-01 0.00011  1.83241E-02 0.00032  1.12498E-03 0.00429  1.44095E+00 0.00046 ];
INF_SP1                   (idx, [1:   8]) = [  2.38461E-01 0.00019  5.34998E-03 0.00068  4.81102E-04 0.00782  3.80992E-01 0.00058 ];
INF_SP2                   (idx, [1:   8]) = [  9.77269E-02 0.00032 -1.57954E-03 0.00247  2.59516E-04 0.01063  9.03573E-02 0.00110 ];
INF_SP3                   (idx, [1:   8]) = [  9.23545E-03 0.00281 -1.88713E-03 0.00188  9.39841E-05 0.01973  2.71362E-02 0.00197 ];
INF_SP4                   (idx, [1:   8]) = [ -9.69288E-03 0.00207 -6.37207E-04 0.00395  6.64255E-07 1.00000 -8.35434E-03 0.00825 ];
INF_SP5                   (idx, [1:   8]) = [  1.16619E-04 0.17466  1.44979E-05 0.14462 -3.74275E-05 0.04215  6.29958E-03 0.00682 ];
INF_SP6                   (idx, [1:   8]) = [  5.21585E-03 0.00354 -1.40240E-04 0.01658 -4.79588E-05 0.02834 -1.62272E-02 0.00276 ];
INF_SP7                   (idx, [1:   8]) = [  9.13741E-04 0.02175 -1.68516E-04 0.01448 -4.48748E-05 0.02865  3.79089E-04 0.11444 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32326E-01 0.00073  1.08527E+00 0.00672 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33690E-01 0.00096  1.15227E+00 0.00857 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33987E-01 0.00091  1.15509E+00 0.00751 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29366E-01 0.00093  9.70941E-01 0.00657 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43478E+00 0.00073  3.07475E-01 0.00670 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42642E+00 0.00096  2.89791E-01 0.00852 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42461E+00 0.00091  2.88971E-01 0.00756 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45331E+00 0.00093  3.43661E-01 0.00650 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.83579E-03 0.00874  1.99320E-04 0.04835  1.10127E-03 0.02086  1.06044E-03 0.02119  3.13301E-03 0.01219  1.02272E-03 0.02116  3.19046E-04 0.03665 ];
LAMBDA                    (idx, [1:  14]) = [  7.84529E-01 0.01791  1.24906E-02 5.1E-06  3.15743E-02 0.00036  1.10226E-01 0.00045  3.20702E-01 0.00034  1.34486E+00 0.00025  8.92386E+00 0.00223 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0006' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:46 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:09:05 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284846 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.91431E-01  9.97784E-01  1.00299E+00  1.00323E+00  1.00456E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13897E-02 0.00103  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88610E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.98864E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.99293E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.70308E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.59487E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.59407E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.02440E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.28989E-01 0.00108  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000377 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00008E+04 0.00076 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00008E+04 0.00076 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.39724E+02 ;
RUNNING_TIME              (idx, 1)        =  2.83169E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.32900E-01  4.32900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  6.45167E-02  8.85000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  2.75846E+01  3.39083E+00  2.62737E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  2.32333E-01  2.91500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.39000E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  2.83168E+01  1.25834E+02 ];
CPU_USAGE                 (idx, 1)        = 4.93430 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99974E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.70610E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.11705E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.85369E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.72463E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.64321E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.14727E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.47382E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.73894E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  4.75011E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.85287E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.29429E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.80945E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  3.45582E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.17192E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.48080E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.13818E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.08423E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.20832E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.59856E+11 ;
CS137_ACTIVITY            (idx, 1)        =  2.69961E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.99335E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.58169E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.30025E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.18670E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.21462E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 4 ;
BURNUP                     (idx, [1:  2])  = [  2.00000E+00  2.00018E+00 ];
BURN_DAYS                 (idx, 1)        =  5.00000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.18146E-01 0.00110 ];
U235_FISS                 (idx, [1:   4]) = [  1.15978E+16 0.00062  8.38858E-01 0.00030 ];
U238_FISS                 (idx, [1:   4]) = [  8.92953E+14 0.00282  6.45758E-02 0.00264 ];
PU239_FISS                (idx, [1:   4]) = [  1.32151E+15 0.00215  9.55807E-02 0.00205 ];
PU240_FISS                (idx, [1:   4]) = [  2.05052E+11 0.18732  1.48076E-05 0.18729 ];
PU241_FISS                (idx, [1:   4]) = [  9.95654E+12 0.02622  7.19886E-04 0.02610 ];
U235_CAPT                 (idx, [1:   4]) = [  2.52733E+15 0.00157  1.39557E-01 0.00148 ];
U238_CAPT                 (idx, [1:   4]) = [  8.20710E+15 0.00103  4.53149E-01 0.00065 ];
PU239_CAPT                (idx, [1:   4]) = [  7.35979E+14 0.00295  4.06430E-02 0.00295 ];
PU240_CAPT                (idx, [1:   4]) = [  1.29177E+14 0.00675  7.13184E-03 0.00667 ];
PU241_CAPT                (idx, [1:   4]) = [  3.91311E+12 0.04209  2.16176E-04 0.04216 ];
XE135_CAPT                (idx, [1:   4]) = [  7.26915E+14 0.00295  4.01403E-02 0.00291 ];
SM149_CAPT                (idx, [1:   4]) = [  1.58047E+14 0.00613  8.72776E-03 0.00615 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000377 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.45529E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000377 5.00746E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2812803 2.81686E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2147558 2.15057E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 40016 4.00216E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000377 5.00746E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.05360E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.46140E+16 1.2E-05  3.46140E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38295E+16 1.9E-06  1.38295E+16 1.9E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.80998E+16 0.00045  1.28834E+16 0.00044  5.21642E+15 0.00102 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.19294E+16 0.00026  2.67129E+16 0.00021  5.21642E+15 0.00102 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.21462E+16 0.00051  3.21462E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.54058E+18 0.00047  4.15693E+17 0.00045  1.12489E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.57330E+14 0.00522 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.21867E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.15692E+18 0.00059 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12281E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12281E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.66046E+00 0.00045 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.81679E-01 0.00030 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.73155E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24211E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94764E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97217E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.08520E+00 0.00052 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07651E+00 0.00052 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.50291E+00 1.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03119E+02 1.9E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07632E+00 0.00054  1.06939E+00 0.00053  7.12511E-03 0.00884 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07707E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07691E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07707E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.08576E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74489E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74446E+01 7.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.30144E-07 0.00351 ];
IMP_EALF                  (idx, [1:   2]) = [  5.31036E-07 0.00136 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.18670E-01 0.00282 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.19550E-01 0.00110 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.22488E-03 0.00589  1.75810E-04 0.03291  1.01619E-03 0.01428  9.47786E-04 0.01450  2.83018E-03 0.00826  9.46415E-04 0.01381  3.08501E-04 0.02439 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.15595E-01 0.01265  1.05675E-02 0.01910  3.15291E-02 0.00029  1.10165E-01 0.00030  3.20964E-01 0.00024  1.34437E+00 0.00023  8.63995E+00 0.00891 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.68992E-03 0.00860  1.94389E-04 0.04857  1.09183E-03 0.02092  1.03212E-03 0.02087  3.03228E-03 0.01223  1.00819E-03 0.02138  3.31116E-04 0.03737 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.13709E-01 0.01915  1.24928E-02 0.00020  3.15199E-02 0.00043  1.10157E-01 0.00044  3.21056E-01 0.00035  1.34429E+00 0.00030  8.98519E+00 0.00283 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.57826E-05 0.00117  2.57696E-05 0.00117  2.78246E-05 0.01219 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.77463E-05 0.00104  2.77323E-05 0.00104  2.99441E-05 0.01219 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.60086E-03 0.00902  1.92047E-04 0.05023  1.06835E-03 0.02216  9.84012E-04 0.02259  3.04090E-03 0.01344  9.78953E-04 0.02276  3.36604E-04 0.03916 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.23686E-01 0.02001  1.24943E-02 0.00032  3.15189E-02 0.00047  1.10207E-01 0.00054  3.21035E-01 0.00039  1.34486E+00 0.00036  9.00084E+00 0.00320 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.55361E-05 0.00244  2.55170E-05 0.00245  2.81616E-05 0.03020 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.74817E-05 0.00240  2.74612E-05 0.00241  3.02872E-05 0.03022 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.40586E-03 0.02822  2.22438E-04 0.15295  1.14294E-03 0.06626  9.66794E-04 0.07106  2.78625E-03 0.04285  9.66023E-04 0.07257  3.21421E-04 0.11996 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.07769E-01 0.06666  1.25069E-02 0.00130  3.14364E-02 0.00129  1.10302E-01 0.00130  3.21224E-01 0.00116  1.34458E+00 0.00067  9.01279E+00 0.00685 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.40684E-03 0.02733  2.17326E-04 0.15068  1.14980E-03 0.06393  9.69776E-04 0.06900  2.79532E-03 0.04137  9.56729E-04 0.07042  3.17884E-04 0.11927 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.02532E-01 0.06473  1.25069E-02 0.00130  3.14352E-02 0.00130  1.10303E-01 0.00129  3.21253E-01 0.00115  1.34453E+00 0.00067  9.00701E+00 0.00679 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.51777E+02 0.02820 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.56756E-05 0.00078 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.76308E-05 0.00053 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.49055E-03 0.00474 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.52865E+02 0.00480 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  6.04646E-07 0.00063 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.86422E-06 0.00041  2.86368E-06 0.00042  2.93859E-06 0.00496 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.99385E-05 0.00078  3.99595E-05 0.00079  3.70170E-05 0.00855 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.70946E-01 0.00032  6.70495E-01 0.00032  7.56092E-01 0.00899 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03728E+01 0.01364 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.59407E+01 0.00043  3.48147E+01 0.00046 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.36430E+04 0.00348  2.94188E+05 0.00148  6.06205E+05 0.00076  6.50951E+05 0.00071  5.99851E+05 0.00073  6.43245E+05 0.00079  4.36674E+05 0.00063  3.86458E+05 0.00048  2.95771E+05 0.00088  2.41708E+05 0.00083  2.08149E+05 0.00099  1.87779E+05 0.00069  1.73405E+05 0.00101  1.64621E+05 0.00101  1.60781E+05 0.00086  1.38706E+05 0.00099  1.36900E+05 0.00087  1.35848E+05 0.00102  1.33381E+05 0.00111  2.60142E+05 0.00074  2.51131E+05 0.00060  1.81081E+05 0.00080  1.17551E+05 0.00085  1.35575E+05 0.00088  1.28255E+05 0.00069  1.16247E+05 0.00104  1.90869E+05 0.00073  4.36620E+04 0.00182  5.48107E+04 0.00206  4.97015E+04 0.00143  2.87840E+04 0.00174  5.04570E+04 0.00119  3.41311E+04 0.00195  2.89821E+04 0.00143  5.51940E+03 0.00282  5.41968E+03 0.00390  5.41672E+03 0.00371  5.53965E+03 0.00351  5.50986E+03 0.00266  5.51890E+03 0.00421  5.79291E+03 0.00237  5.44748E+03 0.00321  1.03548E+04 0.00251  1.65620E+04 0.00193  2.10505E+04 0.00221  5.57190E+04 0.00163  5.86607E+04 0.00147  6.37643E+04 0.00123  4.31286E+04 0.00165  3.18601E+04 0.00155  2.42737E+04 0.00141  2.86708E+04 0.00132  5.50292E+04 0.00106  7.55467E+04 0.00111  1.46675E+05 0.00128  2.26336E+05 0.00112  3.37476E+05 0.00123  2.15213E+05 0.00109  1.53922E+05 0.00114  1.10812E+05 0.00148  9.93825E+04 0.00128  9.78609E+04 0.00145  8.16459E+04 0.00128  5.52282E+04 0.00137  5.09385E+04 0.00163  4.53821E+04 0.00137  3.82730E+04 0.00182  3.01728E+04 0.00198  2.01867E+04 0.00189  7.11964E+03 0.00211 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.08559E+00 0.00055 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.20347E+18 0.00061  3.37144E+17 0.00121 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37554E-01 0.00011  1.51274E+00 0.00037 ];
INF_CAPT                  (idx, [1:   4]) = [  6.52597E-03 0.00053  3.03938E-02 0.00049 ];
INF_ABS                   (idx, [1:   4]) = [  8.70241E-03 0.00044  6.36506E-02 0.00088 ];
INF_FISS                  (idx, [1:   4]) = [  2.17644E-03 0.00050  3.32568E-02 0.00124 ];
INF_NSF                   (idx, [1:   4]) = [  5.61670E-03 0.00049  8.26344E-02 0.00125 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.58068E+00 5.6E-05  2.48474E+00 2.0E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04066E+02 5.1E-06  2.02898E+02 3.2E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.87331E-08 0.00037  2.55345E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28853E-01 0.00011  1.44907E+00 0.00042 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43760E-01 0.00021  3.83589E-01 0.00048 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61317E-02 0.00025  9.11519E-02 0.00089 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35371E-03 0.00332  2.74951E-02 0.00323 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02829E-02 0.00166 -8.37161E-03 0.00833 ];
INF_SCATT5                (idx, [1:   4]) = [  1.19106E-04 0.12803  6.36316E-03 0.00741 ];
INF_SCATT6                (idx, [1:   4]) = [  5.05123E-03 0.00321 -1.62745E-02 0.00260 ];
INF_SCATT7                (idx, [1:   4]) = [  7.28174E-04 0.01758  3.69701E-04 0.10775 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28893E-01 0.00011  1.44907E+00 0.00042 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43760E-01 0.00021  3.83589E-01 0.00048 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61316E-02 0.00025  9.11519E-02 0.00089 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35395E-03 0.00332  2.74951E-02 0.00323 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02828E-02 0.00166 -8.37161E-03 0.00833 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.19006E-04 0.12821  6.36316E-03 0.00741 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.05132E-03 0.00321 -1.62745E-02 0.00260 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.28405E-04 0.01761  3.69701E-04 0.10775 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13443E-01 0.00027  9.76923E-01 0.00039 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56170E+00 0.00027  3.41209E-01 0.00039 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.66258E-03 0.00045  6.36506E-02 0.00088 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69285E-02 0.00025  6.48048E-02 0.00092 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10625E-01 0.00011  1.82281E-02 0.00037  1.13266E-03 0.00479  1.44794E+00 0.00042 ];
INF_S1                    (idx, [1:   8]) = [  2.38423E-01 0.00021  5.33641E-03 0.00102  4.83792E-04 0.00648  3.83105E-01 0.00048 ];
INF_S2                    (idx, [1:   8]) = [  9.77019E-02 0.00024 -1.57017E-03 0.00243  2.66523E-04 0.00999  9.08853E-02 0.00089 ];
INF_S3                    (idx, [1:   8]) = [  9.22676E-03 0.00249 -1.87305E-03 0.00185  9.70190E-05 0.01793  2.73981E-02 0.00324 ];
INF_S4                    (idx, [1:   8]) = [ -9.65854E-03 0.00168 -6.24395E-04 0.00557  3.35008E-06 0.40207 -8.37496E-03 0.00831 ];
INF_S5                    (idx, [1:   8]) = [  1.05320E-04 0.14790  1.37861E-05 0.18293 -3.59862E-05 0.04154  6.39915E-03 0.00734 ];
INF_S6                    (idx, [1:   8]) = [  5.19673E-03 0.00317 -1.45494E-04 0.01513 -4.87461E-05 0.03386 -1.62257E-02 0.00259 ];
INF_S7                    (idx, [1:   8]) = [  9.01513E-04 0.01440 -1.73339E-04 0.01466 -4.22578E-05 0.03226  4.11958E-04 0.09631 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10665E-01 0.00011  1.82281E-02 0.00037  1.13266E-03 0.00479  1.44794E+00 0.00042 ];
INF_SP1                   (idx, [1:   8]) = [  2.38424E-01 0.00021  5.33641E-03 0.00102  4.83792E-04 0.00648  3.83105E-01 0.00048 ];
INF_SP2                   (idx, [1:   8]) = [  9.77017E-02 0.00024 -1.57017E-03 0.00243  2.66523E-04 0.00999  9.08853E-02 0.00089 ];
INF_SP3                   (idx, [1:   8]) = [  9.22700E-03 0.00249 -1.87305E-03 0.00185  9.70190E-05 0.01793  2.73981E-02 0.00324 ];
INF_SP4                   (idx, [1:   8]) = [ -9.65845E-03 0.00168 -6.24395E-04 0.00557  3.35008E-06 0.40207 -8.37496E-03 0.00831 ];
INF_SP5                   (idx, [1:   8]) = [  1.05220E-04 0.14804  1.37861E-05 0.18293 -3.59862E-05 0.04154  6.39915E-03 0.00734 ];
INF_SP6                   (idx, [1:   8]) = [  5.19681E-03 0.00317 -1.45494E-04 0.01513 -4.87461E-05 0.03386 -1.62257E-02 0.00259 ];
INF_SP7                   (idx, [1:   8]) = [  9.01744E-04 0.01441 -1.73339E-04 0.01466 -4.22578E-05 0.03226  4.11958E-04 0.09631 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32345E-01 0.00069  1.09621E+00 0.00734 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33961E-01 0.00093  1.15602E+00 0.00842 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.34080E-01 0.00085  1.16864E+00 0.00843 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29074E-01 0.00115  9.85414E-01 0.00860 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43467E+00 0.00069  3.04467E-01 0.00726 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42477E+00 0.00093  2.88834E-01 0.00838 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42404E+00 0.00085  2.85694E-01 0.00803 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45518E+00 0.00115  3.38872E-01 0.00865 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.68992E-03 0.00860  1.94389E-04 0.04857  1.09183E-03 0.02092  1.03212E-03 0.02087  3.03228E-03 0.01223  1.00819E-03 0.02138  3.31116E-04 0.03737 ];
LAMBDA                    (idx, [1:  14]) = [  8.13709E-01 0.01915  1.24928E-02 0.00020  3.15199E-02 0.00043  1.10157E-01 0.00044  3.21056E-01 0.00035  1.34429E+00 0.00030  8.98519E+00 0.00283 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0006' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:46 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:15:10 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284846 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.89174E-01  1.00114E+00  1.00279E+00  1.00274E+00  1.00416E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14159E-02 0.00106  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88584E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.99948E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.00377E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69696E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.57322E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.57241E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.90945E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.26489E-01 0.00107  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000892 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00018E+04 0.00077 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00018E+04 0.00077 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.70144E+02 ;
RUNNING_TIME              (idx, 1)        =  3.44100E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.32900E-01  4.32900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  8.24667E-02  8.58334E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.36020E+01  3.37983E+00  2.63752E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  2.88467E-01  2.71000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.73333E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.44099E+01  1.25906E+02 ];
CPU_USAGE                 (idx, 1)        = 4.94461 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99621E+00 0.00030 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.73134E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.18551E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.83833E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.20019E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.65754E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.15716E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.52794E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.72259E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  5.72983E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.01129E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.65064E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.88225E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.07919E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.32307E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.65750E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.16898E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.18120E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.25159E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.48959E+11 ;
CS137_ACTIVITY            (idx, 1)        =  4.05026E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.03600E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.54604E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.41656E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.19973E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.26447E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 5 ;
BURNUP                     (idx, [1:  2])  = [  3.00000E+00  3.00027E+00 ];
BURN_DAYS                 (idx, 1)        =  7.50000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.24573E-01 0.00105 ];
U235_FISS                 (idx, [1:   4]) = [  1.10228E+16 0.00067  7.97994E-01 0.00033 ];
U238_FISS                 (idx, [1:   4]) = [  9.11284E+14 0.00259  6.59671E-02 0.00245 ];
PU239_FISS                (idx, [1:   4]) = [  1.84509E+15 0.00190  1.33576E-01 0.00180 ];
PU240_FISS                (idx, [1:   4]) = [  6.51494E+11 0.09826  4.71198E-05 0.09828 ];
PU241_FISS                (idx, [1:   4]) = [  2.91163E+13 0.01505  2.10771E-03 0.01505 ];
U235_CAPT                 (idx, [1:   4]) = [  2.40028E+15 0.00168  1.28939E-01 0.00157 ];
U238_CAPT                 (idx, [1:   4]) = [  8.26267E+15 0.00101  4.43835E-01 0.00070 ];
PU239_CAPT                (idx, [1:   4]) = [  1.03002E+15 0.00252  5.53322E-02 0.00247 ];
PU240_CAPT                (idx, [1:   4]) = [  2.51920E+14 0.00524  1.35332E-02 0.00522 ];
PU241_CAPT                (idx, [1:   4]) = [  1.05619E+13 0.02487  5.67174E-04 0.02484 ];
XE135_CAPT                (idx, [1:   4]) = [  7.25253E+14 0.00302  3.89642E-02 0.00304 ];
SM149_CAPT                (idx, [1:   4]) = [  1.64496E+14 0.00640  8.83803E-03 0.00644 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000892 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.34174E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000892 5.00734E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2847570 2.85129E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2113028 2.11576E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 40294 4.02938E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000892 5.00734E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.51926E-09 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.48190E+16 1.5E-05  3.48190E+16 1.5E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38136E+16 2.3E-06  1.38136E+16 2.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.86181E+16 0.00046  1.33817E+16 0.00047  5.23646E+15 0.00106 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.24318E+16 0.00026  2.71953E+16 0.00023  5.23646E+15 0.00106 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.26447E+16 0.00050  3.26447E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.55798E+18 0.00047  4.19900E+17 0.00044  1.13808E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.63086E+14 0.00527 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.26948E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.16779E+18 0.00059 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12165E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12165E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.64970E+00 0.00046 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.83447E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.69675E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24235E+00 0.00032 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94788E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97138E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.07521E+00 0.00051 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06654E+00 0.00051 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.52063E+00 1.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03353E+02 2.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06676E+00 0.00054  1.05972E+00 0.00051  6.82309E-03 0.00881 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06661E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06674E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06661E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.07527E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74072E+01 0.00018 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74089E+01 8.2E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.52423E-07 0.00316 ];
IMP_EALF                  (idx, [1:   2]) = [  5.50369E-07 0.00144 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.23395E-01 0.00273 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.23222E-01 0.00114 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.11810E-03 0.00590  1.75598E-04 0.03304  9.93550E-04 0.01388  9.50483E-04 0.01482  2.78403E-03 0.00848  9.27249E-04 0.01434  2.87188E-04 0.02619 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.94406E-01 0.01339  1.05678E-02 0.01910  3.14564E-02 0.00034  1.10175E-01 0.00033  3.21189E-01 0.00024  1.34322E+00 0.00031  8.55391E+00 0.01028 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.51844E-03 0.00815  1.79926E-04 0.04795  1.06686E-03 0.02068  1.00063E-03 0.02190  2.99218E-03 0.01234  9.84704E-04 0.02200  2.94134E-04 0.03865 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.80488E-01 0.01970  1.24906E-02 3.4E-05  3.14420E-02 0.00048  1.10178E-01 0.00049  3.21185E-01 0.00035  1.34315E+00 0.00036  8.98813E+00 0.00275 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.55585E-05 0.00117  2.55448E-05 0.00117  2.77069E-05 0.01216 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.72607E-05 0.00103  2.72461E-05 0.00104  2.95515E-05 0.01213 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.41435E-03 0.00884  1.83739E-04 0.05202  1.03612E-03 0.02134  9.94761E-04 0.02220  2.92456E-03 0.01331  9.76986E-04 0.02246  2.98180E-04 0.04141 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.94035E-01 0.02181  1.24923E-02 0.00017  3.14482E-02 0.00058  1.10186E-01 0.00057  3.21227E-01 0.00041  1.34279E+00 0.00055  8.97227E+00 0.00299 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.52732E-05 0.00273  2.52566E-05 0.00275  2.72857E-05 0.02995 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.69571E-05 0.00270  2.69393E-05 0.00272  2.91071E-05 0.02998 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.47402E-03 0.02758  1.90537E-04 0.16149  9.41877E-04 0.07284  1.02747E-03 0.07240  3.00076E-03 0.04239  1.04428E-03 0.07007  2.69091E-04 0.13110 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.78275E-01 0.06296  1.24903E-02 2.4E-05  3.14119E-02 0.00139  1.10131E-01 0.00120  3.21507E-01 0.00117  1.34254E+00 0.00085  8.91982E+00 0.00650 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.44173E-03 0.02630  1.94800E-04 0.15759  9.58555E-04 0.07151  9.98538E-04 0.07097  2.99025E-03 0.04049  1.02877E-03 0.06762  2.70819E-04 0.12873 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.89769E-01 0.06381  1.24903E-02 2.4E-05  3.14160E-02 0.00138  1.10143E-01 0.00120  3.21543E-01 0.00115  1.34250E+00 0.00086  8.91932E+00 0.00649 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.57140E+02 0.02755 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.54253E-05 0.00073 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.71190E-05 0.00054 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.37232E-03 0.00553 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.50659E+02 0.00552 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.98003E-07 0.00066 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.84643E-06 0.00045  2.84609E-06 0.00045  2.89709E-06 0.00505 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.95127E-05 0.00081  3.95344E-05 0.00081  3.62254E-05 0.00880 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.67535E-01 0.00031  6.67118E-01 0.00031  7.49021E-01 0.00924 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04425E+01 0.01352 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.57241E+01 0.00044  3.45457E+01 0.00045 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.38289E+04 0.00372  2.94680E+05 0.00139  6.07518E+05 0.00093  6.51132E+05 0.00074  5.99131E+05 0.00067  6.42456E+05 0.00069  4.36608E+05 0.00067  3.86459E+05 0.00067  2.95315E+05 0.00071  2.41306E+05 0.00078  2.08114E+05 0.00092  1.87668E+05 0.00065  1.73342E+05 0.00095  1.64533E+05 0.00082  1.60498E+05 0.00096  1.38871E+05 0.00093  1.36776E+05 0.00093  1.35629E+05 0.00086  1.33323E+05 0.00100  2.60489E+05 0.00065  2.50859E+05 0.00056  1.81372E+05 0.00072  1.17257E+05 0.00078  1.35433E+05 0.00088  1.28396E+05 0.00079  1.16111E+05 0.00105  1.90293E+05 0.00071  4.36070E+04 0.00145  5.48606E+04 0.00174  4.95905E+04 0.00115  2.88225E+04 0.00175  5.01597E+04 0.00166  3.40701E+04 0.00125  2.89623E+04 0.00165  5.43821E+03 0.00303  5.28455E+03 0.00321  5.23120E+03 0.00352  5.26441E+03 0.00326  5.30427E+03 0.00362  5.37869E+03 0.00372  5.69274E+03 0.00282  5.42439E+03 0.00346  1.02389E+04 0.00345  1.64027E+04 0.00205  2.08920E+04 0.00178  5.51587E+04 0.00139  5.82313E+04 0.00152  6.31004E+04 0.00152  4.23499E+04 0.00155  3.09998E+04 0.00166  2.34424E+04 0.00222  2.77802E+04 0.00157  5.34090E+04 0.00127  7.37852E+04 0.00109  1.43390E+05 0.00115  2.22121E+05 0.00094  3.31763E+05 0.00106  2.11979E+05 0.00113  1.51765E+05 0.00108  1.09170E+05 0.00158  9.79031E+04 0.00138  9.65578E+04 0.00135  8.05131E+04 0.00141  5.43737E+04 0.00136  5.02627E+04 0.00164  4.46879E+04 0.00145  3.78294E+04 0.00151  2.97142E+04 0.00122  1.99090E+04 0.00134  7.04009E+03 0.00195 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.07541E+00 0.00045 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.22143E+18 0.00049  3.36579E+17 0.00104 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37660E-01 0.00011  1.51851E+00 0.00034 ];
INF_CAPT                  (idx, [1:   4]) = [  6.64422E-03 0.00057  3.12071E-02 0.00050 ];
INF_ABS                   (idx, [1:   4]) = [  8.78939E-03 0.00044  6.44695E-02 0.00081 ];
INF_FISS                  (idx, [1:   4]) = [  2.14517E-03 0.00037  3.32625E-02 0.00110 ];
INF_NSF                   (idx, [1:   4]) = [  5.55372E-03 0.00035  8.33104E-02 0.00112 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.58894E+00 6.7E-05  2.50464E+00 2.6E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04173E+02 6.8E-06  2.03160E+02 4.2E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.84878E-08 0.00036  2.55599E-06 0.00012 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28863E-01 0.00012  1.45408E+00 0.00039 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43783E-01 0.00018  3.85132E-01 0.00051 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61459E-02 0.00032  9.14004E-02 0.00102 ];
INF_SCATT3                (idx, [1:   4]) = [  7.36174E-03 0.00259  2.75785E-02 0.00261 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03007E-02 0.00164 -8.35038E-03 0.00726 ];
INF_SCATT5                (idx, [1:   4]) = [  1.52229E-04 0.08679  6.41628E-03 0.00890 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07559E-03 0.00334 -1.64133E-02 0.00285 ];
INF_SCATT7                (idx, [1:   4]) = [  7.43993E-04 0.02239  2.85425E-04 0.15487 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28902E-01 0.00012  1.45408E+00 0.00039 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43784E-01 0.00018  3.85132E-01 0.00051 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61462E-02 0.00032  9.14004E-02 0.00102 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.36171E-03 0.00259  2.75785E-02 0.00261 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03008E-02 0.00164 -8.35038E-03 0.00726 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.52220E-04 0.08648  6.41628E-03 0.00890 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07556E-03 0.00335 -1.64133E-02 0.00285 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.43813E-04 0.02240  2.85425E-04 0.15487 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13399E-01 0.00028  9.81482E-01 0.00030 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56202E+00 0.00028  3.39623E-01 0.00030 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.75014E-03 0.00044  6.44695E-02 0.00081 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69430E-02 0.00018  6.55778E-02 0.00090 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10717E-01 0.00011  1.81464E-02 0.00037  1.14844E-03 0.00410  1.45294E+00 0.00039 ];
INF_S1                    (idx, [1:   8]) = [  2.38488E-01 0.00017  5.29495E-03 0.00105  4.88700E-04 0.00699  3.84643E-01 0.00051 ];
INF_S2                    (idx, [1:   8]) = [  9.77207E-02 0.00030 -1.57483E-03 0.00295  2.65307E-04 0.00860  9.11351E-02 0.00102 ];
INF_S3                    (idx, [1:   8]) = [  9.23336E-03 0.00220 -1.87162E-03 0.00186  9.46399E-05 0.01995  2.74838E-02 0.00261 ];
INF_S4                    (idx, [1:   8]) = [ -9.68041E-03 0.00166 -6.20277E-04 0.00498 -2.70252E-06 0.72904 -8.34768E-03 0.00727 ];
INF_S5                    (idx, [1:   8]) = [  1.32769E-04 0.08995  1.94604E-05 0.15820 -4.01776E-05 0.04145  6.45646E-03 0.00882 ];
INF_S6                    (idx, [1:   8]) = [  5.21866E-03 0.00311 -1.43069E-04 0.01950 -4.91816E-05 0.03102 -1.63641E-02 0.00287 ];
INF_S7                    (idx, [1:   8]) = [  9.13992E-04 0.01744 -1.69999E-04 0.01484 -4.26481E-05 0.02274  3.28074E-04 0.13487 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10756E-01 0.00011  1.81464E-02 0.00037  1.14844E-03 0.00410  1.45294E+00 0.00039 ];
INF_SP1                   (idx, [1:   8]) = [  2.38489E-01 0.00017  5.29495E-03 0.00105  4.88700E-04 0.00699  3.84643E-01 0.00051 ];
INF_SP2                   (idx, [1:   8]) = [  9.77210E-02 0.00030 -1.57483E-03 0.00295  2.65307E-04 0.00860  9.11351E-02 0.00102 ];
INF_SP3                   (idx, [1:   8]) = [  9.23333E-03 0.00219 -1.87162E-03 0.00186  9.46399E-05 0.01995  2.74838E-02 0.00261 ];
INF_SP4                   (idx, [1:   8]) = [ -9.68053E-03 0.00166 -6.20277E-04 0.00498 -2.70252E-06 0.72904 -8.34768E-03 0.00727 ];
INF_SP5                   (idx, [1:   8]) = [  1.32760E-04 0.08964  1.94604E-05 0.15820 -4.01776E-05 0.04145  6.45646E-03 0.00882 ];
INF_SP6                   (idx, [1:   8]) = [  5.21863E-03 0.00311 -1.43069E-04 0.01950 -4.91816E-05 0.03102 -1.63641E-02 0.00287 ];
INF_SP7                   (idx, [1:   8]) = [  9.13812E-04 0.01746 -1.69999E-04 0.01484 -4.26481E-05 0.02274  3.28074E-04 0.13487 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32047E-01 0.00061  1.09467E+00 0.00582 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33535E-01 0.00118  1.17614E+00 0.00795 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33485E-01 0.00077  1.15813E+00 0.00716 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29182E-01 0.00074  9.74712E-01 0.00565 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43650E+00 0.00061  3.04754E-01 0.00584 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42739E+00 0.00118  2.83844E-01 0.00794 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42766E+00 0.00077  2.88173E-01 0.00713 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45446E+00 0.00074  3.42244E-01 0.00568 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.51844E-03 0.00815  1.79926E-04 0.04795  1.06686E-03 0.02068  1.00063E-03 0.02190  2.99218E-03 0.01234  9.84704E-04 0.02200  2.94134E-04 0.03865 ];
LAMBDA                    (idx, [1:  14]) = [  7.80488E-01 0.01970  1.24906E-02 3.4E-05  3.14420E-02 0.00048  1.10178E-01 0.00049  3.21185E-01 0.00035  1.34315E+00 0.00036  8.98813E+00 0.00275 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0006' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:46 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:21:13 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284846 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.89243E-01  1.00180E+00  1.00302E+00  1.00147E+00  1.00447E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13829E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88617E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.01467E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.01895E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68933E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.56160E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.56078E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.79624E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.22692E-01 0.00115  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000705 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00014E+04 0.00080 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00014E+04 0.00080 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.00297E+02 ;
RUNNING_TIME              (idx, 1)        =  4.04470E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.32900E-01  4.32900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.00317E-01  8.41667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.95664E+01  3.35243E+00  2.61197E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  3.42583E-01  2.53667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.91167E-02  8.66663E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.04469E+01  1.25278E+02 ];
CPU_USAGE                 (idx, 1)        = 4.95207 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99862E+00 0.00021 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.74936E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.23285E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.82467E+04 ;
TOT_SF_RATE               (idx, 1)        =  9.14724E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.67152E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.16684E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.56131E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.70796E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  6.65659E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.11574E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.09367E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.95479E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.56292E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.42026E+07 ;
SR90_ACTIVITY             (idx, 1)        =  4.79905E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.19679E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.22814E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.28989E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.21056E+12 ;
CS137_ACTIVITY            (idx, 1)        =  5.40049E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.06503E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.51643E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  8.21972E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.20917E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.31970E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 6 ;
BURNUP                     (idx, [1:  2])  = [  4.00000E+00  4.00036E+00 ];
BURN_DAYS                 (idx, 1)        =  1.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.32877E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  1.05178E+16 0.00071  7.62356E-01 0.00039 ];
U238_FISS                 (idx, [1:   4]) = [  9.25087E+14 0.00249  6.70465E-02 0.00234 ];
PU239_FISS                (idx, [1:   4]) = [  2.28944E+15 0.00170  1.65943E-01 0.00158 ];
PU240_FISS                (idx, [1:   4]) = [  1.19749E+12 0.07417  8.66641E-05 0.07414 ];
PU241_FISS                (idx, [1:   4]) = [  5.80756E+13 0.01061  4.20907E-03 0.01057 ];
U235_CAPT                 (idx, [1:   4]) = [  2.30273E+15 0.00171  1.20076E-01 0.00158 ];
U238_CAPT                 (idx, [1:   4]) = [  8.33316E+15 0.00105  4.34519E-01 0.00073 ];
PU239_CAPT                (idx, [1:   4]) = [  1.27711E+15 0.00241  6.65988E-02 0.00237 ];
PU240_CAPT                (idx, [1:   4]) = [  3.87254E+14 0.00403  2.01917E-02 0.00393 ];
PU241_CAPT                (idx, [1:   4]) = [  2.09933E+13 0.01842  1.09487E-03 0.01842 ];
XE135_CAPT                (idx, [1:   4]) = [  7.31298E+14 0.00304  3.81385E-02 0.00305 ];
SM149_CAPT                (idx, [1:   4]) = [  1.69576E+14 0.00670  8.84201E-03 0.00665 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000705 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.56234E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000705 5.00756E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2884381 2.88838E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2075218 2.07805E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 41106 4.11273E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000705 5.00756E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -7.45058E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.49968E+16 1.6E-05  3.49968E+16 1.6E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37998E+16 2.5E-06  1.37998E+16 2.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.91661E+16 0.00043  1.38700E+16 0.00046  5.29610E+15 0.00104 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.29658E+16 0.00025  2.76697E+16 0.00023  5.29610E+15 0.00104 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.31970E+16 0.00050  3.31970E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.57986E+18 0.00046  4.24659E+17 0.00046  1.15520E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.73079E+14 0.00503 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.32389E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.18369E+18 0.00057 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12048E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12048E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.63863E+00 0.00048 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.83526E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.66441E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24207E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94671E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97088E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.06271E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.05397E+00 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.53604E+00 1.8E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03557E+02 2.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.05411E+00 0.00056  1.04742E+00 0.00055  6.54306E-03 0.00908 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.05452E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.05435E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.05452E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.06326E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73730E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73761E+01 8.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.71754E-07 0.00327 ];
IMP_EALF                  (idx, [1:   2]) = [  5.68791E-07 0.00152 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.27034E-01 0.00261 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.27006E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.02030E-03 0.00572  1.64185E-04 0.03440  9.85441E-04 0.01434  9.54834E-04 0.01444  2.73773E-03 0.00824  9.00184E-04 0.01485  2.77927E-04 0.02696 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.80822E-01 0.01365  1.02192E-02 0.02112  3.14237E-02 0.00032  1.10239E-01 0.00037  3.21292E-01 0.00024  1.34228E+00 0.00035  8.38443E+00 0.01212 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.24858E-03 0.00849  1.65931E-04 0.05251  1.04282E-03 0.02134  9.68315E-04 0.02083  2.86948E-03 0.01287  9.14589E-04 0.02213  2.87445E-04 0.04039 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.75919E-01 0.02043  1.24939E-02 0.00021  3.14126E-02 0.00047  1.10283E-01 0.00052  3.21469E-01 0.00036  1.34233E+00 0.00055  8.96904E+00 0.00351 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.54528E-05 0.00131  2.54418E-05 0.00131  2.72217E-05 0.01363 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.68258E-05 0.00117  2.68142E-05 0.00118  2.86888E-05 0.01360 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.22138E-03 0.00915  1.66980E-04 0.05384  1.05422E-03 0.02270  9.83546E-04 0.02309  2.82239E-03 0.01334  9.15270E-04 0.02373  2.78966E-04 0.04027 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.64109E-01 0.02103  1.25014E-02 0.00054  3.14372E-02 0.00055  1.10205E-01 0.00057  3.21406E-01 0.00041  1.34345E+00 0.00042  8.97025E+00 0.00411 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.51907E-05 0.00279  2.51825E-05 0.00280  2.68985E-05 0.05297 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.65490E-05 0.00272  2.65402E-05 0.00272  2.83936E-05 0.05392 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.35772E-03 0.02818  1.83178E-04 0.17022  1.13203E-03 0.07041  1.05892E-03 0.07095  2.87854E-03 0.04334  8.65659E-04 0.07113  2.39395E-04 0.13278 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  6.86138E-01 0.06310  1.24901E-02 2.8E-05  3.14177E-02 0.00130  1.10152E-01 0.00132  3.21239E-01 0.00115  1.34456E+00 0.00071  8.90495E+00 0.01357 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.33674E-03 0.02757  1.88640E-04 0.16551  1.15072E-03 0.06703  1.04963E-03 0.06893  2.84335E-03 0.04295  8.66012E-04 0.07040  2.38385E-04 0.13064 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  6.89369E-01 0.06278  1.24901E-02 2.8E-05  3.14165E-02 0.00130  1.10160E-01 0.00130  3.21273E-01 0.00114  1.34460E+00 0.00070  8.90424E+00 0.01355 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.52849E+02 0.02800 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.53332E-05 0.00073 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.67000E-05 0.00048 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.33236E-03 0.00546 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.49966E+02 0.00541 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.93711E-07 0.00070 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.83222E-06 0.00041  2.83204E-06 0.00041  2.85671E-06 0.00535 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.92744E-05 0.00084  3.92922E-05 0.00085  3.64912E-05 0.00972 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.64285E-01 0.00034  6.63995E-01 0.00034  7.24470E-01 0.00921 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04201E+01 0.01411 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.56078E+01 0.00046  3.43398E+01 0.00046 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.47764E+04 0.00291  2.95433E+05 0.00187  6.06418E+05 0.00086  6.51479E+05 0.00083  5.98452E+05 0.00064  6.42768E+05 0.00067  4.36267E+05 0.00083  3.86237E+05 0.00063  2.95674E+05 0.00062  2.41430E+05 0.00073  2.08248E+05 0.00098  1.87318E+05 0.00100  1.73304E+05 0.00085  1.64522E+05 0.00066  1.60199E+05 0.00083  1.38675E+05 0.00103  1.37259E+05 0.00114  1.35528E+05 0.00092  1.33258E+05 0.00096  2.60358E+05 0.00068  2.50767E+05 0.00078  1.81197E+05 0.00085  1.17271E+05 0.00105  1.35551E+05 0.00088  1.28263E+05 0.00077  1.15810E+05 0.00098  1.89635E+05 0.00085  4.35321E+04 0.00200  5.47661E+04 0.00159  4.97901E+04 0.00142  2.88854E+04 0.00188  5.01851E+04 0.00133  3.39230E+04 0.00150  2.88070E+04 0.00172  5.41810E+03 0.00321  5.19939E+03 0.00413  5.03548E+03 0.00358  4.98441E+03 0.00407  5.05816E+03 0.00378  5.20043E+03 0.00408  5.60088E+03 0.00246  5.32201E+03 0.00374  1.00977E+04 0.00297  1.62818E+04 0.00243  2.08505E+04 0.00184  5.50315E+04 0.00145  5.79444E+04 0.00164  6.25256E+04 0.00113  4.17528E+04 0.00161  3.04020E+04 0.00139  2.29527E+04 0.00156  2.71525E+04 0.00152  5.22809E+04 0.00135  7.23571E+04 0.00150  1.41241E+05 0.00148  2.19323E+05 0.00140  3.27973E+05 0.00136  2.09645E+05 0.00169  1.50250E+05 0.00130  1.08069E+05 0.00133  9.71264E+04 0.00161  9.55008E+04 0.00162  7.97248E+04 0.00137  5.40048E+04 0.00144  4.99529E+04 0.00138  4.43038E+04 0.00156  3.74959E+04 0.00179  2.95130E+04 0.00178  1.97506E+04 0.00231  7.00495E+03 0.00273 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.06309E+00 0.00051 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.24158E+18 0.00049  3.38317E+17 0.00124 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37806E-01 0.00014  1.52519E+00 0.00034 ];
INF_CAPT                  (idx, [1:   4]) = [  6.76289E-03 0.00064  3.18357E-02 0.00056 ];
INF_ABS                   (idx, [1:   4]) = [  8.87458E-03 0.00047  6.48816E-02 0.00090 ];
INF_FISS                  (idx, [1:   4]) = [  2.11169E-03 0.00046  3.30459E-02 0.00123 ];
INF_NSF                   (idx, [1:   4]) = [  5.48363E-03 0.00046  8.33349E-02 0.00124 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.59680E+00 5.5E-05  2.52179E+00 2.8E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04273E+02 5.7E-06  2.03389E+02 4.6E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.82970E-08 0.00034  2.55873E-06 0.00012 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28921E-01 0.00014  1.46033E+00 0.00040 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43832E-01 0.00022  3.86946E-01 0.00048 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61415E-02 0.00030  9.17794E-02 0.00100 ];
INF_SCATT3                (idx, [1:   4]) = [  7.33015E-03 0.00238  2.74297E-02 0.00249 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03375E-02 0.00179 -8.50194E-03 0.00668 ];
INF_SCATT5                (idx, [1:   4]) = [  1.43960E-04 0.11896  6.44310E-03 0.00707 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08363E-03 0.00363 -1.64928E-02 0.00312 ];
INF_SCATT7                (idx, [1:   4]) = [  7.34086E-04 0.02009  4.34243E-04 0.08602 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28961E-01 0.00014  1.46033E+00 0.00040 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43833E-01 0.00022  3.86946E-01 0.00048 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61414E-02 0.00030  9.17794E-02 0.00100 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.33062E-03 0.00238  2.74297E-02 0.00249 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03375E-02 0.00179 -8.50194E-03 0.00668 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.43912E-04 0.11887  6.44310E-03 0.00707 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08369E-03 0.00363 -1.64928E-02 0.00312 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.34113E-04 0.02006  4.34243E-04 0.08602 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13228E-01 0.00032  9.85968E-01 0.00027 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56328E+00 0.00032  3.38078E-01 0.00027 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.83414E-03 0.00050  6.48816E-02 0.00090 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69500E-02 0.00030  6.60110E-02 0.00104 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10856E-01 0.00014  1.80643E-02 0.00049  1.15354E-03 0.00459  1.45918E+00 0.00040 ];
INF_S1                    (idx, [1:   8]) = [  2.38565E-01 0.00022  5.26766E-03 0.00092  4.90151E-04 0.00733  3.86455E-01 0.00048 ];
INF_S2                    (idx, [1:   8]) = [  9.77146E-02 0.00029 -1.57317E-03 0.00250  2.68493E-04 0.01201  9.15110E-02 0.00101 ];
INF_S3                    (idx, [1:   8]) = [  9.18533E-03 0.00172 -1.85519E-03 0.00188  9.72110E-05 0.02420  2.73325E-02 0.00251 ];
INF_S4                    (idx, [1:   8]) = [ -9.72836E-03 0.00184 -6.09116E-04 0.00410  2.20379E-07 1.00000 -8.50216E-03 0.00667 ];
INF_S5                    (idx, [1:   8]) = [  1.23181E-04 0.13872  2.07796E-05 0.14180 -3.94568E-05 0.04043  6.48256E-03 0.00694 ];
INF_S6                    (idx, [1:   8]) = [  5.22553E-03 0.00352 -1.41899E-04 0.02062 -4.78295E-05 0.02938 -1.64450E-02 0.00313 ];
INF_S7                    (idx, [1:   8]) = [  9.09179E-04 0.01588 -1.75093E-04 0.01154 -4.39556E-05 0.02649  4.78199E-04 0.07804 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10897E-01 0.00014  1.80643E-02 0.00049  1.15354E-03 0.00459  1.45918E+00 0.00040 ];
INF_SP1                   (idx, [1:   8]) = [  2.38565E-01 0.00022  5.26766E-03 0.00092  4.90151E-04 0.00733  3.86455E-01 0.00048 ];
INF_SP2                   (idx, [1:   8]) = [  9.77146E-02 0.00029 -1.57317E-03 0.00250  2.68493E-04 0.01201  9.15110E-02 0.00101 ];
INF_SP3                   (idx, [1:   8]) = [  9.18581E-03 0.00172 -1.85519E-03 0.00188  9.72110E-05 0.02420  2.73325E-02 0.00251 ];
INF_SP4                   (idx, [1:   8]) = [ -9.72843E-03 0.00184 -6.09116E-04 0.00410  2.20379E-07 1.00000 -8.50216E-03 0.00667 ];
INF_SP5                   (idx, [1:   8]) = [  1.23133E-04 0.13867  2.07796E-05 0.14180 -3.94568E-05 0.04043  6.48256E-03 0.00694 ];
INF_SP6                   (idx, [1:   8]) = [  5.22559E-03 0.00352 -1.41899E-04 0.02062 -4.78295E-05 0.02938 -1.64450E-02 0.00313 ];
INF_SP7                   (idx, [1:   8]) = [  9.09206E-04 0.01585 -1.75093E-04 0.01154 -4.39556E-05 0.02649  4.78199E-04 0.07804 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32306E-01 0.00077  1.10605E+00 0.00550 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33788E-01 0.00097  1.19021E+00 0.00674 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33701E-01 0.00105  1.17653E+00 0.00743 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29491E-01 0.00116  9.79215E-01 0.00611 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43491E+00 0.00077  3.01591E-01 0.00548 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42582E+00 0.00097  2.80371E-01 0.00679 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42636E+00 0.00104  2.83692E-01 0.00739 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45254E+00 0.00116  3.40709E-01 0.00602 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.24858E-03 0.00849  1.65931E-04 0.05251  1.04282E-03 0.02134  9.68315E-04 0.02083  2.86948E-03 0.01287  9.14589E-04 0.02213  2.87445E-04 0.04039 ];
LAMBDA                    (idx, [1:  14]) = [  7.75919E-01 0.02043  1.24939E-02 0.00021  3.14126E-02 0.00047  1.10283E-01 0.00052  3.21469E-01 0.00036  1.34233E+00 0.00055  8.96904E+00 0.00351 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0006' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:46 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:27:15 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284846 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.90488E-01  1.00190E+00  1.00244E+00  1.00121E+00  1.00396E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.1E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13443E-02 0.00108  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88656E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.02528E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.02955E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68381E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.55225E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.55141E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.71463E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.19199E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000842 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00017E+04 0.00081 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00017E+04 0.00081 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.30508E+02 ;
RUNNING_TIME              (idx, 1)        =  4.64950E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.32900E-01  4.32900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.19083E-01  9.60000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  4.55392E+01  3.35867E+00  2.61417E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  3.98633E-01  2.80833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.25667E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.64948E+01  1.25040E+02 ];
CPU_USAGE                 (idx, 1)        = 4.95770 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99998E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76288E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.27273E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.81320E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.09320E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.68826E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.17845E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.58445E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.69533E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  7.58422E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.19750E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.62257E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.04003E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.96165E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.49349E+07 ;
SR90_ACTIVITY             (idx, 1)        =  5.90945E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.22145E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.26446E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.32357E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.93823E+12 ;
CS137_ACTIVITY            (idx, 1)        =  6.75007E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.09209E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.49128E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.22248E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.21771E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.37333E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 7 ;
BURNUP                     (idx, [1:  2])  = [  5.00000E+00  5.00045E+00 ];
BURN_DAYS                 (idx, 1)        =  1.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.40385E-01 0.00109 ];
U235_FISS                 (idx, [1:   4]) = [  1.00854E+16 0.00075  7.30800E-01 0.00043 ];
U238_FISS                 (idx, [1:   4]) = [  9.37872E+14 0.00281  6.79530E-02 0.00267 ];
PU239_FISS                (idx, [1:   4]) = [  2.67459E+15 0.00155  1.93813E-01 0.00148 ];
PU240_FISS                (idx, [1:   4]) = [  1.48196E+12 0.06614  1.07431E-04 0.06620 ];
PU241_FISS                (idx, [1:   4]) = [  9.58432E+13 0.00848  6.94506E-03 0.00846 ];
U235_CAPT                 (idx, [1:   4]) = [  2.21430E+15 0.00163  1.12396E-01 0.00155 ];
U238_CAPT                 (idx, [1:   4]) = [  8.39783E+15 0.00103  4.26235E-01 0.00070 ];
PU239_CAPT                (idx, [1:   4]) = [  1.49533E+15 0.00216  7.59006E-02 0.00208 ];
PU240_CAPT                (idx, [1:   4]) = [  5.18959E+14 0.00346  2.63403E-02 0.00338 ];
PU241_CAPT                (idx, [1:   4]) = [  3.46468E+13 0.01395  1.75906E-03 0.01398 ];
XE135_CAPT                (idx, [1:   4]) = [  7.33485E+14 0.00310  3.72333E-02 0.00309 ];
SM149_CAPT                (idx, [1:   4]) = [  1.73833E+14 0.00651  8.82524E-03 0.00654 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000842 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.65876E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000842 5.00766E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2916277 2.92019E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2042717 2.04562E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 41848 4.18565E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000842 5.00766E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.58794E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.51559E+16 1.8E-05  3.51559E+16 1.8E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37872E+16 3.0E-06  1.37872E+16 3.0E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.96997E+16 0.00045  1.43422E+16 0.00045  5.35750E+15 0.00111 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.34869E+16 0.00027  2.81294E+16 0.00023  5.35750E+15 0.00111 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.37333E+16 0.00049  3.37333E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.60177E+18 0.00046  4.29595E+17 0.00044  1.17218E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.82437E+14 0.00513 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.37693E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.19967E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11932E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11932E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.62941E+00 0.00048 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.83530E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.63996E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24106E+00 0.00033 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94553E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97060E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.05200E+00 0.00056 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04319E+00 0.00056 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.54990E+00 2.0E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03742E+02 3.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04315E+00 0.00057  1.03681E+00 0.00056  6.37880E-03 0.00839 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.04270E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04230E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.04270E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.05150E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73487E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73442E+01 7.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.85946E-07 0.00341 ];
IMP_EALF                  (idx, [1:   2]) = [  5.87133E-07 0.00136 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.30893E-01 0.00292 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.31144E-01 0.00107 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.99965E-03 0.00542  1.64609E-04 0.03465  9.57227E-04 0.01453  9.40479E-04 0.01428  2.73757E-03 0.00833  9.09875E-04 0.01496  2.89889E-04 0.02609 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.02455E-01 0.01321  1.02966E-02 0.02069  3.13471E-02 0.00036  1.10170E-01 0.00035  3.21455E-01 0.00025  1.33637E+00 0.00209  8.50574E+00 0.01080 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.12551E-03 0.00793  1.79601E-04 0.05099  9.75068E-04 0.02062  9.54755E-04 0.02036  2.79974E-03 0.01266  9.18876E-04 0.02180  2.97467E-04 0.03845 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.01373E-01 0.01995  1.24974E-02 0.00035  3.13520E-02 0.00052  1.10236E-01 0.00049  3.21490E-01 0.00037  1.33779E+00 0.00107  8.96326E+00 0.00313 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.54158E-05 0.00127  2.54093E-05 0.00127  2.64767E-05 0.01357 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.65085E-05 0.00115  2.65017E-05 0.00115  2.76127E-05 0.01353 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.13176E-03 0.00853  1.83820E-04 0.05366  9.75370E-04 0.02335  9.56896E-04 0.02172  2.80195E-03 0.01291  9.25144E-04 0.02413  2.88575E-04 0.04203 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.90859E-01 0.02192  1.24897E-02 1.4E-05  3.13528E-02 0.00066  1.10168E-01 0.00057  3.21573E-01 0.00042  1.33894E+00 0.00117  8.93444E+00 0.00442 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.51254E-05 0.00299  2.51186E-05 0.00300  2.59722E-05 0.03374 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.62057E-05 0.00294  2.61987E-05 0.00295  2.70804E-05 0.03367 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.93039E-03 0.03006  1.94510E-04 0.14727  9.95035E-04 0.07488  9.45724E-04 0.07060  2.65735E-03 0.04627  8.15570E-04 0.07666  3.22207E-04 0.12778 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.12186E-01 0.06768  1.24897E-02 2.8E-05  3.12489E-02 0.00157  1.09918E-01 0.00132  3.22127E-01 0.00131  1.34174E+00 0.00175  8.87752E+00 0.00954 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.92133E-03 0.02934  1.91117E-04 0.13756  1.00162E-03 0.07333  9.16900E-04 0.06838  2.64450E-03 0.04546  8.31585E-04 0.07449  3.35620E-04 0.12644 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.13140E-01 0.06500  1.24896E-02 2.8E-05  3.12446E-02 0.00158  1.09929E-01 0.00132  3.22133E-01 0.00130  1.34154E+00 0.00175  8.87190E+00 0.00956 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.37338E+02 0.03041 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.52837E-05 0.00078 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.63704E-05 0.00052 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.07508E-03 0.00534 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.40400E+02 0.00547 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.90409E-07 0.00071 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.81624E-06 0.00044  2.81610E-06 0.00044  2.84220E-06 0.00519 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.91039E-05 0.00087  3.91268E-05 0.00087  3.55277E-05 0.00977 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.61794E-01 0.00033  6.61532E-01 0.00034  7.14998E-01 0.00833 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.00997E+01 0.01350 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.55141E+01 0.00046  3.41566E+01 0.00046 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.51060E+04 0.00234  2.96692E+05 0.00137  6.07123E+05 0.00105  6.50976E+05 0.00092  5.98524E+05 0.00051  6.42703E+05 0.00080  4.36417E+05 0.00061  3.86261E+05 0.00061  2.94748E+05 0.00077  2.41405E+05 0.00085  2.08036E+05 0.00057  1.87454E+05 0.00108  1.73164E+05 0.00086  1.64525E+05 0.00073  1.60396E+05 0.00067  1.38510E+05 0.00073  1.36879E+05 0.00117  1.35693E+05 0.00106  1.33099E+05 0.00085  2.59733E+05 0.00073  2.51173E+05 0.00060  1.81222E+05 0.00084  1.17224E+05 0.00094  1.35500E+05 0.00113  1.28337E+05 0.00086  1.15814E+05 0.00082  1.89772E+05 0.00059  4.35228E+04 0.00124  5.47404E+04 0.00149  4.97678E+04 0.00121  2.88463E+04 0.00191  5.00568E+04 0.00170  3.39051E+04 0.00170  2.86078E+04 0.00166  5.31054E+03 0.00331  5.06466E+03 0.00308  4.88525E+03 0.00369  4.79140E+03 0.00331  4.84681E+03 0.00453  5.09124E+03 0.00354  5.48238E+03 0.00308  5.24189E+03 0.00301  1.00029E+04 0.00250  1.61155E+04 0.00217  2.06204E+04 0.00226  5.46762E+04 0.00165  5.73430E+04 0.00106  6.19147E+04 0.00130  4.11100E+04 0.00123  2.97569E+04 0.00161  2.24501E+04 0.00209  2.64978E+04 0.00166  5.12862E+04 0.00151  7.10712E+04 0.00121  1.39379E+05 0.00144  2.16940E+05 0.00131  3.25079E+05 0.00142  2.08162E+05 0.00155  1.49260E+05 0.00148  1.07297E+05 0.00164  9.64161E+04 0.00160  9.49883E+04 0.00141  7.93112E+04 0.00170  5.36555E+04 0.00183  4.95755E+04 0.00188  4.40457E+04 0.00166  3.72598E+04 0.00155  2.93914E+04 0.00173  1.97063E+04 0.00178  6.96221E+03 0.00231 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.05110E+00 0.00052 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.26130E+18 0.00052  3.40510E+17 0.00101 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37861E-01 9.4E-05  1.53091E+00 0.00037 ];
INF_CAPT                  (idx, [1:   4]) = [  6.87651E-03 0.00066  3.23843E-02 0.00045 ];
INF_ABS                   (idx, [1:   4]) = [  8.95828E-03 0.00048  6.51683E-02 0.00072 ];
INF_FISS                  (idx, [1:   4]) = [  2.08177E-03 0.00038  3.27840E-02 0.00099 ];
INF_NSF                   (idx, [1:   4]) = [  5.42198E-03 0.00038  8.31747E-02 0.00101 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.60450E+00 5.1E-05  2.53705E+00 3.8E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04372E+02 5.9E-06  2.03594E+02 6.3E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.80871E-08 0.00035  2.56230E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28909E-01 9.4E-05  1.46568E+00 0.00042 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43921E-01 0.00016  3.88359E-01 0.00053 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62038E-02 0.00032  9.20396E-02 0.00116 ];
INF_SCATT3                (idx, [1:   4]) = [  7.33211E-03 0.00303  2.76270E-02 0.00272 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03184E-02 0.00159 -8.60020E-03 0.00629 ];
INF_SCATT5                (idx, [1:   4]) = [  1.40254E-04 0.12810  6.39921E-03 0.00956 ];
INF_SCATT6                (idx, [1:   4]) = [  5.06142E-03 0.00270 -1.66685E-02 0.00346 ];
INF_SCATT7                (idx, [1:   4]) = [  7.45142E-04 0.01737  4.07565E-04 0.11331 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28950E-01 9.4E-05  1.46568E+00 0.00042 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43921E-01 0.00016  3.88359E-01 0.00053 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62038E-02 0.00032  9.20396E-02 0.00116 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.33208E-03 0.00303  2.76270E-02 0.00272 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03185E-02 0.00159 -8.60020E-03 0.00629 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.40143E-04 0.12821  6.39921E-03 0.00956 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.06141E-03 0.00269 -1.66685E-02 0.00346 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.45308E-04 0.01738  4.07565E-04 0.11331 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13051E-01 0.00026  9.90359E-01 0.00035 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56457E+00 0.00026  3.36579E-01 0.00035 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.91731E-03 0.00049  6.51683E-02 0.00072 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69544E-02 0.00020  6.63944E-02 0.00098 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10907E-01 9.5E-05  1.80023E-02 0.00039  1.16390E-03 0.00422  1.46452E+00 0.00042 ];
INF_S1                    (idx, [1:   8]) = [  2.38681E-01 0.00016  5.23985E-03 0.00078  5.03329E-04 0.00629  3.87855E-01 0.00054 ];
INF_S2                    (idx, [1:   8]) = [  9.77836E-02 0.00032 -1.57981E-03 0.00210  2.69744E-04 0.01078  9.17698E-02 0.00117 ];
INF_S3                    (idx, [1:   8]) = [  9.18387E-03 0.00246 -1.85176E-03 0.00136  9.81000E-05 0.02209  2.75289E-02 0.00274 ];
INF_S4                    (idx, [1:   8]) = [ -9.70893E-03 0.00172 -6.09428E-04 0.00387 -2.42100E-07 1.00000 -8.59996E-03 0.00633 ];
INF_S5                    (idx, [1:   8]) = [  1.19354E-04 0.15480  2.09001E-05 0.09315 -4.11955E-05 0.04361  6.44041E-03 0.00952 ];
INF_S6                    (idx, [1:   8]) = [  5.20718E-03 0.00263 -1.45754E-04 0.01530 -4.90313E-05 0.03263 -1.66195E-02 0.00347 ];
INF_S7                    (idx, [1:   8]) = [  9.18407E-04 0.01349 -1.73265E-04 0.01207 -4.44207E-05 0.03359  4.51986E-04 0.10204 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10948E-01 9.5E-05  1.80023E-02 0.00039  1.16390E-03 0.00422  1.46452E+00 0.00042 ];
INF_SP1                   (idx, [1:   8]) = [  2.38681E-01 0.00016  5.23985E-03 0.00078  5.03329E-04 0.00629  3.87855E-01 0.00054 ];
INF_SP2                   (idx, [1:   8]) = [  9.77836E-02 0.00032 -1.57981E-03 0.00210  2.69744E-04 0.01078  9.17698E-02 0.00117 ];
INF_SP3                   (idx, [1:   8]) = [  9.18384E-03 0.00246 -1.85176E-03 0.00136  9.81000E-05 0.02209  2.75289E-02 0.00274 ];
INF_SP4                   (idx, [1:   8]) = [ -9.70911E-03 0.00172 -6.09428E-04 0.00387 -2.42100E-07 1.00000 -8.59996E-03 0.00633 ];
INF_SP5                   (idx, [1:   8]) = [  1.19243E-04 0.15499  2.09001E-05 0.09315 -4.11955E-05 0.04361  6.44041E-03 0.00952 ];
INF_SP6                   (idx, [1:   8]) = [  5.20717E-03 0.00263 -1.45754E-04 0.01530 -4.90313E-05 0.03263 -1.66195E-02 0.00347 ];
INF_SP7                   (idx, [1:   8]) = [  9.18573E-04 0.01350 -1.73265E-04 0.01207 -4.44207E-05 0.03359  4.51986E-04 0.10204 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31952E-01 0.00048  1.14361E+00 0.00665 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33379E-01 0.00072  1.22557E+00 0.00890 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33581E-01 0.00098  1.22411E+00 0.00740 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28963E-01 0.00072  1.01077E+00 0.00716 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43709E+00 0.00048  2.91792E-01 0.00682 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42831E+00 0.00072  2.72512E-01 0.00910 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42709E+00 0.00098  2.72674E-01 0.00758 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45586E+00 0.00072  3.30191E-01 0.00720 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.12551E-03 0.00793  1.79601E-04 0.05099  9.75068E-04 0.02062  9.54755E-04 0.02036  2.79974E-03 0.01266  9.18876E-04 0.02180  2.97467E-04 0.03845 ];
LAMBDA                    (idx, [1:  14]) = [  8.01373E-01 0.01995  1.24974E-02 0.00035  3.13520E-02 0.00052  1.10236E-01 0.00049  3.21490E-01 0.00037  1.33779E+00 0.00107  8.96326E+00 0.00313 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0006' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:46 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:33:19 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284846 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.90067E-01  1.00220E+00  1.00337E+00  1.00177E+00  1.00259E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13285E-02 0.00105  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88671E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.03474E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.03901E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67886E+00 0.00020  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.54803E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.54718E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.65219E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.17042E-01 0.00109  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000834 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00017E+04 0.00082 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00017E+04 0.00082 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.60743E+02 ;
RUNNING_TIME              (idx, 1)        =  5.25488E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.32900E-01  4.32900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.37733E-01  9.26667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  5.15176E+01  3.36473E+00  2.61367E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  4.54767E-01  2.80667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.59500E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  5.25486E+01  1.25113E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96193 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00050E+00 0.00021 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.77316E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.30706E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.80303E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.40406E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.70655E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.19117E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.60048E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.68388E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  8.53447E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.26560E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.23327E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.13307E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.30121E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.55229E+07 ;
SR90_ACTIVITY             (idx, 1)        =  6.99125E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.24371E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.29606E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.35393E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.82717E+12 ;
CS137_ACTIVITY            (idx, 1)        =  8.09883E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.11745E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.46878E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.80509E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.22564E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.42390E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 8 ;
BURNUP                     (idx, [1:  2])  = [  6.00000E+00  6.00053E+00 ];
BURN_DAYS                 (idx, 1)        =  1.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.50834E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  9.66783E+15 0.00077  7.02112E-01 0.00046 ];
U238_FISS                 (idx, [1:   4]) = [  9.48161E+14 0.00278  6.88518E-02 0.00264 ];
PU239_FISS                (idx, [1:   4]) = [  3.00610E+15 0.00142  2.18317E-01 0.00131 ];
PU240_FISS                (idx, [1:   4]) = [  2.18479E+12 0.05500  1.58614E-04 0.05494 ];
PU241_FISS                (idx, [1:   4]) = [  1.39878E+14 0.00720  1.01573E-02 0.00714 ];
U235_CAPT                 (idx, [1:   4]) = [  2.13359E+15 0.00179  1.05471E-01 0.00175 ];
U238_CAPT                 (idx, [1:   4]) = [  8.48193E+15 0.00108  4.19237E-01 0.00069 ];
PU239_CAPT                (idx, [1:   4]) = [  1.67878E+15 0.00194  8.29867E-02 0.00189 ];
PU240_CAPT                (idx, [1:   4]) = [  6.49224E+14 0.00319  3.20939E-02 0.00318 ];
PU241_CAPT                (idx, [1:   4]) = [  4.98702E+13 0.01189  2.46484E-03 0.01185 ];
XE135_CAPT                (idx, [1:   4]) = [  7.35754E+14 0.00310  3.63720E-02 0.00309 ];
SM149_CAPT                (idx, [1:   4]) = [  1.79996E+14 0.00652  8.89823E-03 0.00653 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000834 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.70112E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000834 5.00770E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2950230 2.95431E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2008133 2.01092E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 42471 4.24768E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000834 5.00770E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.49480E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.52997E+16 1.9E-05  3.52997E+16 1.9E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37757E+16 3.3E-06  1.37757E+16 3.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.02060E+16 0.00046  1.47768E+16 0.00044  5.42915E+15 0.00114 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.39816E+16 0.00027  2.85525E+16 0.00023  5.42915E+15 0.00114 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.42390E+16 0.00052  3.42390E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.62360E+18 0.00050  4.34501E+17 0.00047  1.18909E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.90916E+14 0.00494 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.42726E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.21618E+18 0.00064 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11816E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11816E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.61819E+00 0.00050 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.83062E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.61177E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24066E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94470E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97018E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.03936E+00 0.00056 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.03053E+00 0.00056 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.56247E+00 2.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03913E+02 3.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03031E+00 0.00058  1.02428E+00 0.00056  6.25814E-03 0.00880 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.03159E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03112E+00 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.03159E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.04043E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73216E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73154E+01 9.1E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.02102E-07 0.00350 ];
IMP_EALF                  (idx, [1:   2]) = [  6.04389E-07 0.00158 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.33634E-01 0.00278 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.34916E-01 0.00114 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.98907E-03 0.00577  1.59942E-04 0.03435  9.77756E-04 0.01381  9.46416E-04 0.01446  2.72768E-03 0.00832  8.95723E-04 0.01489  2.81550E-04 0.02595 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.91285E-01 0.01354  1.02485E-02 0.02098  3.13185E-02 0.00038  1.10248E-01 0.00039  3.21567E-01 0.00026  1.33601E+00 0.00077  8.49961E+00 0.01091 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.06731E-03 0.00842  1.63140E-04 0.05290  1.01638E-03 0.02095  9.53485E-04 0.02126  2.75026E-03 0.01241  9.05251E-04 0.02320  2.78799E-04 0.03828 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.80080E-01 0.02036  1.24948E-02 0.00028  3.13140E-02 0.00054  1.10292E-01 0.00052  3.21494E-01 0.00040  1.33640E+00 0.00103  8.96866E+00 0.00375 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.55447E-05 0.00127  2.55326E-05 0.00127  2.73457E-05 0.01338 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.63146E-05 0.00114  2.63022E-05 0.00115  2.81618E-05 0.01330 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.08614E-03 0.00883  1.57190E-04 0.05639  1.00468E-03 0.02202  9.60603E-04 0.02347  2.73631E-03 0.01321  9.46649E-04 0.02429  2.80716E-04 0.04145 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.85225E-01 0.02131  1.24943E-02 0.00037  3.13064E-02 0.00068  1.10322E-01 0.00068  3.21733E-01 0.00044  1.33615E+00 0.00113  8.91728E+00 0.00497 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.51886E-05 0.00304  2.51829E-05 0.00303  2.47888E-05 0.03298 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.59467E-05 0.00296  2.59409E-05 0.00296  2.55402E-05 0.03305 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.97206E-03 0.03158  1.67614E-04 0.19857  8.53926E-04 0.07646  9.86297E-04 0.07761  2.76868E-03 0.04903  9.41113E-04 0.07375  2.54435E-04 0.14286 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.20678E-01 0.07768  1.25109E-02 0.00178  3.13739E-02 0.00151  1.10274E-01 0.00146  3.21021E-01 0.00115  1.33472E+00 0.00309  8.68561E+00 0.01492 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.98181E-03 0.03058  1.80128E-04 0.19361  8.70396E-04 0.07431  9.72310E-04 0.07668  2.76785E-03 0.04736  9.33129E-04 0.07079  2.58000E-04 0.13607 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.30369E-01 0.07604  1.25109E-02 0.00178  3.13789E-02 0.00149  1.10313E-01 0.00147  3.21054E-01 0.00114  1.33479E+00 0.00308  8.68840E+00 0.01492 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.38236E+02 0.03172 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.53440E-05 0.00080 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.61075E-05 0.00051 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.07155E-03 0.00570 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.39592E+02 0.00568 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.88052E-07 0.00071 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.79975E-06 0.00043  2.79949E-06 0.00043  2.84275E-06 0.00526 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.90753E-05 0.00085  3.90939E-05 0.00085  3.60810E-05 0.01020 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.58961E-01 0.00033  6.58737E-01 0.00033  7.08313E-01 0.00920 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02943E+01 0.01382 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.54718E+01 0.00047  3.40490E+01 0.00048 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.54773E+04 0.00356  2.98344E+05 0.00204  6.08618E+05 0.00100  6.51578E+05 0.00087  5.98603E+05 0.00078  6.42297E+05 0.00075  4.36257E+05 0.00063  3.85890E+05 0.00072  2.95215E+05 0.00064  2.41015E+05 0.00063  2.07841E+05 0.00085  1.87878E+05 0.00099  1.73141E+05 0.00101  1.64689E+05 0.00104  1.60154E+05 0.00105  1.38611E+05 0.00071  1.36723E+05 0.00090  1.35405E+05 0.00112  1.33123E+05 0.00088  2.59626E+05 0.00069  2.51306E+05 0.00064  1.81011E+05 0.00072  1.17256E+05 0.00079  1.35337E+05 0.00088  1.28463E+05 0.00075  1.15663E+05 0.00118  1.89376E+05 0.00063  4.34746E+04 0.00152  5.46140E+04 0.00127  4.94968E+04 0.00153  2.88154E+04 0.00195  4.99753E+04 0.00142  3.38449E+04 0.00163  2.85061E+04 0.00166  5.25306E+03 0.00340  4.94251E+03 0.00296  4.68888E+03 0.00290  4.57645E+03 0.00354  4.65571E+03 0.00326  4.89729E+03 0.00331  5.41462E+03 0.00341  5.20712E+03 0.00306  9.90698E+03 0.00285  1.60443E+04 0.00236  2.05270E+04 0.00160  5.43317E+04 0.00127  5.71717E+04 0.00098  6.15625E+04 0.00147  4.07100E+04 0.00121  2.94311E+04 0.00153  2.20711E+04 0.00164  2.60583E+04 0.00186  5.05602E+04 0.00112  7.03458E+04 0.00137  1.37916E+05 0.00107  2.15250E+05 0.00117  3.23127E+05 0.00114  2.06985E+05 0.00127  1.48495E+05 0.00126  1.07014E+05 0.00143  9.59549E+04 0.00135  9.45993E+04 0.00158  7.89699E+04 0.00138  5.34211E+04 0.00138  4.94107E+04 0.00111  4.39601E+04 0.00164  3.71528E+04 0.00145  2.92835E+04 0.00171  1.96094E+04 0.00179  6.95853E+03 0.00268 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.03995E+00 0.00062 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.28021E+18 0.00062  3.43420E+17 0.00115 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37859E-01 0.00012  1.53639E+00 0.00035 ];
INF_CAPT                  (idx, [1:   4]) = [  6.97312E-03 0.00043  3.28463E-02 0.00056 ];
INF_ABS                   (idx, [1:   4]) = [  9.02504E-03 0.00039  6.53165E-02 0.00086 ];
INF_FISS                  (idx, [1:   4]) = [  2.05192E-03 0.00049  3.24701E-02 0.00118 ];
INF_NSF                   (idx, [1:   4]) = [  5.35923E-03 0.00050  8.28263E-02 0.00120 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.61181E+00 5.7E-05  2.55084E+00 3.7E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04469E+02 6.2E-06  2.03782E+02 6.3E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.78451E-08 0.00041  2.56470E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28833E-01 0.00012  1.47103E+00 0.00040 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43920E-01 0.00017  3.89985E-01 0.00054 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61757E-02 0.00022  9.23178E-02 0.00074 ];
INF_SCATT3                (idx, [1:   4]) = [  7.33243E-03 0.00238  2.76990E-02 0.00208 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03244E-02 0.00195 -8.60184E-03 0.00824 ];
INF_SCATT5                (idx, [1:   4]) = [  1.53437E-04 0.10134  6.55200E-03 0.00814 ];
INF_SCATT6                (idx, [1:   4]) = [  5.09339E-03 0.00280 -1.67164E-02 0.00227 ];
INF_SCATT7                (idx, [1:   4]) = [  7.54637E-04 0.02116  3.88872E-04 0.10641 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28874E-01 0.00012  1.47103E+00 0.00040 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43920E-01 0.00017  3.89985E-01 0.00054 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61760E-02 0.00022  9.23178E-02 0.00074 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.33215E-03 0.00238  2.76990E-02 0.00208 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03242E-02 0.00195 -8.60184E-03 0.00824 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.53773E-04 0.10139  6.55200E-03 0.00814 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.09361E-03 0.00281 -1.67164E-02 0.00227 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.54581E-04 0.02118  3.88872E-04 0.10641 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12885E-01 0.00038  9.93781E-01 0.00029 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56579E+00 0.00038  3.35420E-01 0.00029 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.98385E-03 0.00037  6.53165E-02 0.00086 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69499E-02 0.00028  6.65227E-02 0.00085 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10909E-01 0.00011  1.79239E-02 0.00046  1.16206E-03 0.00327  1.46987E+00 0.00040 ];
INF_S1                    (idx, [1:   8]) = [  2.38705E-01 0.00017  5.21452E-03 0.00099  4.98812E-04 0.00774  3.89487E-01 0.00054 ];
INF_S2                    (idx, [1:   8]) = [  9.77528E-02 0.00022 -1.57706E-03 0.00297  2.72582E-04 0.00828  9.20452E-02 0.00074 ];
INF_S3                    (idx, [1:   8]) = [  9.17806E-03 0.00176 -1.84562E-03 0.00210  9.84031E-05 0.02087  2.76006E-02 0.00206 ];
INF_S4                    (idx, [1:   8]) = [ -9.72243E-03 0.00200 -6.01922E-04 0.00561 -6.81725E-08 1.00000 -8.60177E-03 0.00821 ];
INF_S5                    (idx, [1:   8]) = [  1.29292E-04 0.11178  2.41446E-05 0.13502 -4.04300E-05 0.03294  6.59243E-03 0.00802 ];
INF_S6                    (idx, [1:   8]) = [  5.23751E-03 0.00276 -1.44124E-04 0.01629 -4.95168E-05 0.02505 -1.66669E-02 0.00228 ];
INF_S7                    (idx, [1:   8]) = [  9.26833E-04 0.01681 -1.72196E-04 0.01637 -4.66417E-05 0.02608  4.35513E-04 0.09428 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10950E-01 0.00011  1.79239E-02 0.00046  1.16206E-03 0.00327  1.46987E+00 0.00040 ];
INF_SP1                   (idx, [1:   8]) = [  2.38706E-01 0.00017  5.21452E-03 0.00099  4.98812E-04 0.00774  3.89487E-01 0.00054 ];
INF_SP2                   (idx, [1:   8]) = [  9.77530E-02 0.00022 -1.57706E-03 0.00297  2.72582E-04 0.00828  9.20452E-02 0.00074 ];
INF_SP3                   (idx, [1:   8]) = [  9.17777E-03 0.00176 -1.84562E-03 0.00210  9.84031E-05 0.02087  2.76006E-02 0.00206 ];
INF_SP4                   (idx, [1:   8]) = [ -9.72231E-03 0.00199 -6.01922E-04 0.00561 -6.81725E-08 1.00000 -8.60177E-03 0.00821 ];
INF_SP5                   (idx, [1:   8]) = [  1.29629E-04 0.11177  2.41446E-05 0.13502 -4.04300E-05 0.03294  6.59243E-03 0.00802 ];
INF_SP6                   (idx, [1:   8]) = [  5.23773E-03 0.00277 -1.44124E-04 0.01629 -4.95168E-05 0.02505 -1.66669E-02 0.00228 ];
INF_SP7                   (idx, [1:   8]) = [  9.26776E-04 0.01683 -1.72196E-04 0.01637 -4.66417E-05 0.02608  4.35513E-04 0.09428 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31995E-01 0.00059  1.15354E+00 0.00660 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33806E-01 0.00078  1.23890E+00 0.00866 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33512E-01 0.00105  1.24311E+00 0.00636 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28745E-01 0.00093  1.01194E+00 0.00760 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43683E+00 0.00059  2.89262E-01 0.00649 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42570E+00 0.00078  2.69533E-01 0.00852 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42752E+00 0.00106  2.68403E-01 0.00632 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45726E+00 0.00093  3.29849E-01 0.00745 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.06731E-03 0.00842  1.63140E-04 0.05290  1.01638E-03 0.02095  9.53485E-04 0.02126  2.75026E-03 0.01241  9.05251E-04 0.02320  2.78799E-04 0.03828 ];
LAMBDA                    (idx, [1:  14]) = [  7.80080E-01 0.02036  1.24948E-02 0.00028  3.13140E-02 0.00054  1.10292E-01 0.00052  3.21494E-01 0.00040  1.33640E+00 0.00103  8.96866E+00 0.00375 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0006' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:46 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:39:22 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284846 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.89821E-01  1.00277E+00  1.00260E+00  1.00178E+00  1.00303E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 6.6E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13454E-02 0.00107  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88655E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.03661E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04089E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67765E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.53601E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.53516E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.61260E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.16000E-01 0.00111  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000761 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00015E+04 0.00084 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00015E+04 0.00084 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.90953E+02 ;
RUNNING_TIME              (idx, 1)        =  5.85975E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.32900E-01  4.32900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.56483E-01  8.90000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  5.74920E+01  3.36075E+00  2.61362E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.09550E-01  2.72500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.93667E-02  8.16667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  5.85973E+01  1.25154E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96528 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00005E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78139E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.33594E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.79453E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.90180E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.72279E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.20249E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61312E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.67426E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  9.51616E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.32316E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.91880E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.21862E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.59736E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.60130E+07 ;
SR90_ACTIVITY             (idx, 1)        =  8.04728E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.26382E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.32394E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.38105E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.86994E+12 ;
CS137_ACTIVITY            (idx, 1)        =  9.44672E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.13919E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.44957E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.63217E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.23246E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.47096E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 9 ;
BURNUP                     (idx, [1:  2])  = [  7.00000E+00  7.00063E+00 ];
BURN_DAYS                 (idx, 1)        =  1.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.61135E-01 0.00114 ];
U235_FISS                 (idx, [1:   4]) = [  9.27514E+15 0.00079  6.74124E-01 0.00051 ];
U238_FISS                 (idx, [1:   4]) = [  9.65754E+14 0.00274  7.01838E-02 0.00258 ];
PU239_FISS                (idx, [1:   4]) = [  3.31774E+15 0.00145  2.41140E-01 0.00134 ];
PU240_FISS                (idx, [1:   4]) = [  2.73469E+12 0.05310  1.98665E-04 0.05302 ];
PU241_FISS                (idx, [1:   4]) = [  1.90829E+14 0.00602  1.38685E-02 0.00597 ];
U235_CAPT                 (idx, [1:   4]) = [  2.04602E+15 0.00171  9.88051E-02 0.00169 ];
U238_CAPT                 (idx, [1:   4]) = [  8.56131E+15 0.00107  4.13383E-01 0.00072 ];
PU239_CAPT                (idx, [1:   4]) = [  1.84702E+15 0.00196  8.91932E-02 0.00191 ];
PU240_CAPT                (idx, [1:   4]) = [  7.81036E+14 0.00303  3.77132E-02 0.00294 ];
PU241_CAPT                (idx, [1:   4]) = [  6.88895E+13 0.01000  3.32692E-03 0.01001 ];
XE135_CAPT                (idx, [1:   4]) = [  7.37221E+14 0.00293  3.56033E-02 0.00294 ];
SM149_CAPT                (idx, [1:   4]) = [  1.83916E+14 0.00619  8.88043E-03 0.00614 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000761 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.75992E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000761 5.00776E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2979042 2.98322E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1979278 1.98209E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 42441 4.24465E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000761 5.00776E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -7.45058E-09 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.54392E+16 1.9E-05  3.54392E+16 1.9E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37644E+16 3.4E-06  1.37644E+16 3.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.07081E+16 0.00044  1.52427E+16 0.00046  5.46539E+15 0.00113 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.44725E+16 0.00027  2.90071E+16 0.00024  5.46539E+15 0.00113 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.47096E+16 0.00051  3.47096E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.64258E+18 0.00048  4.39484E+17 0.00048  1.20310E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.94674E+14 0.00507 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.47672E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.22878E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11699E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11699E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.60873E+00 0.00048 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.83954E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.58197E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24016E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94509E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96985E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.02939E+00 0.00056 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.02065E+00 0.00056 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.57470E+00 2.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04080E+02 3.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.02053E+00 0.00058  1.01463E+00 0.00056  6.01977E-03 0.00960 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.02094E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.02115E+00 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.02094E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.02967E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72920E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72918E+01 8.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.20229E-07 0.00353 ];
IMP_EALF                  (idx, [1:   2]) = [  6.18818E-07 0.00151 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.38539E-01 0.00277 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.38524E-01 0.00113 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.89891E-03 0.00588  1.66363E-04 0.03452  9.94048E-04 0.01430  9.02349E-04 0.01561  2.64595E-03 0.00831  9.01914E-04 0.01465  2.88279E-04 0.02734 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.08606E-01 0.01455  1.02217E-02 0.02112  3.12888E-02 0.00038  1.10329E-01 0.00039  3.21654E-01 0.00027  1.33450E+00 0.00074  8.37718E+00 0.01246 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.94945E-03 0.00856  1.72311E-04 0.04957  9.94310E-04 0.02188  9.18060E-04 0.02268  2.66876E-03 0.01270  9.17354E-04 0.02221  2.78656E-04 0.03834 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.92139E-01 0.02006  1.24974E-02 0.00029  3.12789E-02 0.00054  1.10307E-01 0.00054  3.21785E-01 0.00041  1.33496E+00 0.00100  8.95588E+00 0.00429 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.56050E-05 0.00129  2.55911E-05 0.00130  2.79246E-05 0.01470 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.61263E-05 0.00116  2.61122E-05 0.00117  2.84876E-05 0.01466 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.90556E-03 0.00963  1.71071E-04 0.05348  9.81931E-04 0.02429  9.23206E-04 0.02357  2.62895E-03 0.01396  8.98328E-04 0.02533  3.02072E-04 0.04326 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.22407E-01 0.02372  1.24985E-02 0.00036  3.12761E-02 0.00071  1.10382E-01 0.00070  3.21809E-01 0.00046  1.33331E+00 0.00143  8.86290E+00 0.00586 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.53720E-05 0.00300  2.53479E-05 0.00299  2.83381E-05 0.03779 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.58864E-05 0.00288  2.58618E-05 0.00288  2.89149E-05 0.03780 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.64279E-03 0.03203  2.51466E-04 0.17497  9.32576E-04 0.07376  8.44900E-04 0.07626  2.47024E-03 0.05071  8.37169E-04 0.07649  3.06441E-04 0.13742 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.86732E-01 0.07017  1.25081E-02 0.00150  3.13182E-02 0.00156  1.10395E-01 0.00172  3.22344E-01 0.00135  1.33029E+00 0.00385  9.04953E+00 0.00768 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.60509E-03 0.03079  2.42778E-04 0.17414  9.49845E-04 0.07127  8.26888E-04 0.07330  2.44366E-03 0.04900  8.44577E-04 0.07584  2.97341E-04 0.13250 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.91342E-01 0.06874  1.25081E-02 0.00150  3.13229E-02 0.00155  1.10411E-01 0.00172  3.22416E-01 0.00133  1.33063E+00 0.00383  9.05723E+00 0.00767 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.23489E+02 0.03231 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.54686E-05 0.00074 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.59871E-05 0.00048 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.75171E-03 0.00554 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.25871E+02 0.00555 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.84395E-07 0.00073 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.78812E-06 0.00044  2.78812E-06 0.00044  2.78708E-06 0.00552 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.89046E-05 0.00088  3.89209E-05 0.00088  3.62110E-05 0.01009 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.56039E-01 0.00033  6.55884E-01 0.00033  6.94846E-01 0.00960 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04509E+01 0.01355 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.53516E+01 0.00047  3.39439E+01 0.00047 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.61057E+04 0.00339  2.98751E+05 0.00135  6.08622E+05 0.00101  6.51534E+05 0.00064  5.98942E+05 0.00076  6.42336E+05 0.00080  4.35997E+05 0.00078  3.86098E+05 0.00066  2.95131E+05 0.00069  2.40890E+05 0.00082  2.08211E+05 0.00080  1.87693E+05 0.00076  1.73223E+05 0.00082  1.64404E+05 0.00061  1.60374E+05 0.00063  1.38453E+05 0.00084  1.36820E+05 0.00124  1.35161E+05 0.00107  1.32989E+05 0.00090  2.59953E+05 0.00068  2.51234E+05 0.00058  1.81369E+05 0.00083  1.17229E+05 0.00076  1.35676E+05 0.00083  1.28427E+05 0.00072  1.15599E+05 0.00111  1.89122E+05 0.00086  4.35314E+04 0.00146  5.46061E+04 0.00172  4.95297E+04 0.00159  2.87904E+04 0.00213  4.98651E+04 0.00102  3.37830E+04 0.00162  2.83366E+04 0.00207  5.21282E+03 0.00460  4.87819E+03 0.00272  4.51390E+03 0.00335  4.36495E+03 0.00322  4.47120E+03 0.00330  4.77231E+03 0.00343  5.24787E+03 0.00363  5.11330E+03 0.00349  9.82139E+03 0.00272  1.58174E+04 0.00270  2.03827E+04 0.00273  5.40005E+04 0.00148  5.68821E+04 0.00186  6.10209E+04 0.00125  4.02645E+04 0.00134  2.89528E+04 0.00181  2.16713E+04 0.00159  2.55906E+04 0.00183  4.98217E+04 0.00108  6.94257E+04 0.00100  1.36479E+05 0.00101  2.13013E+05 0.00118  3.20275E+05 0.00098  2.05449E+05 0.00100  1.47231E+05 0.00129  1.05901E+05 0.00130  9.52390E+04 0.00133  9.38289E+04 0.00142  7.84753E+04 0.00156  5.30568E+04 0.00129  4.90363E+04 0.00150  4.35636E+04 0.00156  3.68664E+04 0.00164  2.90950E+04 0.00207  1.94707E+04 0.00173  6.90965E+03 0.00217 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.02990E+00 0.00048 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.29768E+18 0.00054  3.44929E+17 0.00096 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37948E-01 7.7E-05  1.53863E+00 0.00034 ];
INF_CAPT                  (idx, [1:   4]) = [  7.09081E-03 0.00059  3.33623E-02 0.00051 ];
INF_ABS                   (idx, [1:   4]) = [  9.11212E-03 0.00047  6.56685E-02 0.00074 ];
INF_FISS                  (idx, [1:   4]) = [  2.02132E-03 0.00049  3.23062E-02 0.00100 ];
INF_NSF                   (idx, [1:   4]) = [  5.29445E-03 0.00047  8.28394E-02 0.00102 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.61931E+00 5.4E-05  2.56419E+00 4.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04567E+02 5.1E-06  2.03965E+02 7.0E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.76360E-08 0.00042  2.56628E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28842E-01 7.8E-05  1.47295E+00 0.00039 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43935E-01 0.00015  3.90395E-01 0.00053 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62013E-02 0.00032  9.26109E-02 0.00084 ];
INF_SCATT3                (idx, [1:   4]) = [  7.36093E-03 0.00235  2.78798E-02 0.00264 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02735E-02 0.00185 -8.54760E-03 0.00708 ];
INF_SCATT5                (idx, [1:   4]) = [  1.81740E-04 0.07411  6.54768E-03 0.00818 ];
INF_SCATT6                (idx, [1:   4]) = [  5.09567E-03 0.00315 -1.67391E-02 0.00339 ];
INF_SCATT7                (idx, [1:   4]) = [  7.26208E-04 0.02072  4.53553E-04 0.08017 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28883E-01 7.8E-05  1.47295E+00 0.00039 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43936E-01 0.00015  3.90395E-01 0.00053 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62010E-02 0.00032  9.26109E-02 0.00084 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.36098E-03 0.00234  2.78798E-02 0.00264 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02736E-02 0.00185 -8.54760E-03 0.00708 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.81524E-04 0.07406  6.54768E-03 0.00818 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.09595E-03 0.00314 -1.67391E-02 0.00339 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.26179E-04 0.02074  4.53553E-04 0.08017 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12845E-01 0.00028  9.96013E-01 0.00030 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56609E+00 0.00028  3.34668E-01 0.00030 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.07061E-03 0.00048  6.56685E-02 0.00074 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69550E-02 0.00020  6.68496E-02 0.00082 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10993E-01 7.6E-05  1.78488E-02 0.00037  1.17776E-03 0.00414  1.47178E+00 0.00039 ];
INF_S1                    (idx, [1:   8]) = [  2.38741E-01 0.00015  5.19481E-03 0.00078  5.01162E-04 0.00668  3.89894E-01 0.00053 ];
INF_S2                    (idx, [1:   8]) = [  9.77753E-02 0.00031 -1.57397E-03 0.00269  2.72022E-04 0.00737  9.23388E-02 0.00085 ];
INF_S3                    (idx, [1:   8]) = [  9.20070E-03 0.00184 -1.83976E-03 0.00163  9.52622E-05 0.02492  2.77845E-02 0.00266 ];
INF_S4                    (idx, [1:   8]) = [ -9.68013E-03 0.00192 -5.93412E-04 0.00473 -3.20350E-06 0.59179 -8.54440E-03 0.00706 ];
INF_S5                    (idx, [1:   8]) = [  1.53608E-04 0.08459  2.81321E-05 0.08468 -4.10536E-05 0.03529  6.58873E-03 0.00810 ];
INF_S6                    (idx, [1:   8]) = [  5.23750E-03 0.00303 -1.41828E-04 0.01570 -4.99299E-05 0.02382 -1.66891E-02 0.00339 ];
INF_S7                    (idx, [1:   8]) = [  8.97503E-04 0.01630 -1.71295E-04 0.01440 -4.56022E-05 0.03348  4.99155E-04 0.07369 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11035E-01 7.6E-05  1.78488E-02 0.00037  1.17776E-03 0.00414  1.47178E+00 0.00039 ];
INF_SP1                   (idx, [1:   8]) = [  2.38742E-01 0.00015  5.19481E-03 0.00078  5.01162E-04 0.00668  3.89894E-01 0.00053 ];
INF_SP2                   (idx, [1:   8]) = [  9.77749E-02 0.00031 -1.57397E-03 0.00269  2.72022E-04 0.00737  9.23388E-02 0.00085 ];
INF_SP3                   (idx, [1:   8]) = [  9.20074E-03 0.00183 -1.83976E-03 0.00163  9.52622E-05 0.02492  2.77845E-02 0.00266 ];
INF_SP4                   (idx, [1:   8]) = [ -9.68022E-03 0.00192 -5.93412E-04 0.00473 -3.20350E-06 0.59179 -8.54440E-03 0.00706 ];
INF_SP5                   (idx, [1:   8]) = [  1.53392E-04 0.08454  2.81321E-05 0.08468 -4.10536E-05 0.03529  6.58873E-03 0.00810 ];
INF_SP6                   (idx, [1:   8]) = [  5.23778E-03 0.00303 -1.41828E-04 0.01570 -4.99299E-05 0.02382 -1.66891E-02 0.00339 ];
INF_SP7                   (idx, [1:   8]) = [  8.97474E-04 0.01632 -1.71295E-04 0.01440 -4.56022E-05 0.03348  4.99155E-04 0.07369 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31891E-01 0.00055  1.14646E+00 0.00448 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33448E-01 0.00090  1.22760E+00 0.00520 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33452E-01 0.00105  1.23197E+00 0.00693 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28842E-01 0.00069  1.01088E+00 0.00648 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43747E+00 0.00055  2.90889E-01 0.00444 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42790E+00 0.00090  2.71709E-01 0.00518 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42788E+00 0.00105  2.70881E-01 0.00692 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45662E+00 0.00069  3.30078E-01 0.00645 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.94945E-03 0.00856  1.72311E-04 0.04957  9.94310E-04 0.02188  9.18060E-04 0.02268  2.66876E-03 0.01270  9.17354E-04 0.02221  2.78656E-04 0.03834 ];
LAMBDA                    (idx, [1:  14]) = [  7.92139E-01 0.02006  1.24974E-02 0.00029  3.12789E-02 0.00054  1.10307E-01 0.00054  3.21785E-01 0.00041  1.33496E+00 0.00100  8.95588E+00 0.00429 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0006' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:46 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:45:24 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284846 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.90211E-01  1.00169E+00  1.00333E+00  1.00204E+00  1.00273E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13099E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88690E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04654E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05081E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67413E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.53537E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.53450E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.55673E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.13820E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000870 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00017E+04 0.00087 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00017E+04 0.00087 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.21083E+02 ;
RUNNING_TIME              (idx, 1)        =  6.46303E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.32900E-01  4.32900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.75300E-01  9.28333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.34493E+01  3.34987E+00  2.60750E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.65600E-01  2.73500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.28000E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  6.46302E+01  1.25045E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96800 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99995E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78795E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.36385E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.78657E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.65464E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.74212E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.21599E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62170E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.66494E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.05422E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.37506E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.68187E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.31834E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.86030E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.64323E+07 ;
SR90_ACTIVITY             (idx, 1)        =  9.07850E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.28270E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.34968E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.40664E+14 ;
CS134_ACTIVITY            (idx, 1)        =  5.06024E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.07936E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.16220E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.43120E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.77462E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.23956E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.51938E+12 0.00048  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 10 ;
BURNUP                     (idx, [1:  2])  = [  8.00000E+00  8.00072E+00 ];
BURN_DAYS                 (idx, 1)        =  2.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.70463E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  8.93592E+15 0.00079  6.49813E-01 0.00056 ];
U238_FISS                 (idx, [1:   4]) = [  9.80553E+14 0.00285  7.12906E-02 0.00264 ];
PU239_FISS                (idx, [1:   4]) = [  3.57764E+15 0.00142  2.60159E-01 0.00128 ];
PU240_FISS                (idx, [1:   4]) = [  3.32152E+12 0.04827  2.41268E-04 0.04830 ];
PU241_FISS                (idx, [1:   4]) = [  2.47059E+14 0.00532  1.79658E-02 0.00529 ];
U235_CAPT                 (idx, [1:   4]) = [  1.97523E+15 0.00187  9.32081E-02 0.00178 ];
U238_CAPT                 (idx, [1:   4]) = [  8.63640E+15 0.00103  4.07523E-01 0.00075 ];
PU239_CAPT                (idx, [1:   4]) = [  1.98549E+15 0.00180  9.36979E-02 0.00177 ];
PU240_CAPT                (idx, [1:   4]) = [  8.99621E+14 0.00286  4.24470E-02 0.00273 ];
PU241_CAPT                (idx, [1:   4]) = [  8.88331E+13 0.00904  4.19238E-03 0.00905 ];
XE135_CAPT                (idx, [1:   4]) = [  7.38976E+14 0.00299  3.48743E-02 0.00299 ];
SM149_CAPT                (idx, [1:   4]) = [  1.88164E+14 0.00613  8.87863E-03 0.00609 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000870 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.74878E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000870 5.00775E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3006495 3.01073E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1951182 1.95382E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 43193 4.32049E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000870 5.00775E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -1.00583E-07 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.55597E+16 2.1E-05  3.55597E+16 2.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37546E+16 4.0E-06  1.37546E+16 4.0E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.11997E+16 0.00045  1.56501E+16 0.00045  5.54958E+15 0.00120 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.49542E+16 0.00027  2.94047E+16 0.00024  5.54958E+15 0.00120 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.51938E+16 0.00048  3.51938E+16 0.00048  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.66328E+18 0.00048  4.44001E+17 0.00044  1.21928E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.04142E+14 0.00499 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.52584E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.24569E+18 0.00065 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11583E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11583E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.60107E+00 0.00049 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.82739E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.55431E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24075E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94408E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96934E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.01909E+00 0.00058 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.01028E+00 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.58530E+00 2.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04226E+02 4.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.01009E+00 0.00059  1.00447E+00 0.00059  5.81347E-03 0.00962 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.01015E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.01051E+00 0.00048 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.01015E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.01896E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72671E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72686E+01 8.4E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.36058E-07 0.00368 ];
IMP_EALF                  (idx, [1:   2]) = [  6.33289E-07 0.00145 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.42103E-01 0.00289 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.42310E-01 0.00113 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.91168E-03 0.00589  1.64833E-04 0.03477  1.02119E-03 0.01361  9.19075E-04 0.01529  2.66439E-03 0.00862  8.77169E-04 0.01455  2.65017E-04 0.02665 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.67176E-01 0.01378  1.00275E-02 0.02225  3.11724E-02 0.00205  1.10249E-01 0.00040  3.21834E-01 0.00026  1.32957E+00 0.00089  8.30864E+00 0.01319 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.82722E-03 0.00855  1.59919E-04 0.05460  1.03592E-03 0.02024  8.87146E-04 0.02200  2.63688E-03 0.01235  8.54810E-04 0.02243  2.52549E-04 0.04003 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.56634E-01 0.02054  1.24994E-02 0.00036  3.12359E-02 0.00056  1.10235E-01 0.00059  3.21561E-01 0.00039  1.33091E+00 0.00125  8.96841E+00 0.00501 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.57176E-05 0.00134  2.57050E-05 0.00134  2.79952E-05 0.01523 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.59727E-05 0.00121  2.59600E-05 0.00121  2.82708E-05 0.01518 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.75298E-03 0.00971  1.61103E-04 0.05505  1.01514E-03 0.02288  8.81410E-04 0.02525  2.59349E-03 0.01402  8.41235E-04 0.02369  2.60595E-04 0.04422 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.76675E-01 0.02382  1.24898E-02 1.5E-05  3.12131E-02 0.00071  1.10352E-01 0.00074  3.21711E-01 0.00042  1.33011E+00 0.00173  9.05766E+00 0.00576 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.53177E-05 0.00293  2.53080E-05 0.00294  2.57437E-05 0.03660 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.55690E-05 0.00288  2.55591E-05 0.00288  2.60303E-05 0.03673 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.69725E-03 0.03156  1.96082E-04 0.15960  9.58742E-04 0.07745  8.41263E-04 0.07785  2.55557E-03 0.04680  8.81152E-04 0.08312  2.64436E-04 0.12443 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.41967E-01 0.06950  1.24893E-02 3.9E-05  3.11653E-02 0.00175  1.10072E-01 0.00158  3.21587E-01 0.00126  1.33425E+00 0.00333  9.15033E+00 0.00802 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.72244E-03 0.03058  2.00890E-04 0.15998  9.39979E-04 0.07548  8.50407E-04 0.07715  2.58688E-03 0.04511  8.84508E-04 0.08273  2.59770E-04 0.12438 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.29318E-01 0.07002  1.24893E-02 3.9E-05  3.11648E-02 0.00174  1.10088E-01 0.00159  3.21588E-01 0.00125  1.33433E+00 0.00333  9.14893E+00 0.00800 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.25951E+02 0.03168 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.55038E-05 0.00082 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.57565E-05 0.00056 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.76873E-03 0.00549 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.26327E+02 0.00565 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.84073E-07 0.00074 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.77460E-06 0.00042  2.77433E-06 0.00042  2.81770E-06 0.00539 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.89943E-05 0.00093  3.90140E-05 0.00094  3.58119E-05 0.01081 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.53245E-01 0.00033  6.53132E-01 0.00033  6.83596E-01 0.00936 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07381E+01 0.01386 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.53450E+01 0.00049  3.38694E+01 0.00048 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.65063E+04 0.00306  2.99641E+05 0.00130  6.09208E+05 0.00107  6.50924E+05 0.00056  5.98654E+05 0.00090  6.40841E+05 0.00075  4.35282E+05 0.00054  3.85713E+05 0.00061  2.94983E+05 0.00080  2.41067E+05 0.00059  2.07901E+05 0.00077  1.86981E+05 0.00068  1.73065E+05 0.00083  1.64596E+05 0.00107  1.60299E+05 0.00088  1.38423E+05 0.00091  1.36686E+05 0.00094  1.35383E+05 0.00082  1.33203E+05 0.00093  2.59681E+05 0.00073  2.50892E+05 0.00074  1.80928E+05 0.00075  1.17401E+05 0.00115  1.35420E+05 0.00078  1.28363E+05 0.00079  1.15478E+05 0.00061  1.88730E+05 0.00074  4.34185E+04 0.00166  5.44175E+04 0.00162  4.94684E+04 0.00194  2.86206E+04 0.00211  4.98372E+04 0.00145  3.37149E+04 0.00168  2.81895E+04 0.00132  5.13969E+03 0.00379  4.77606E+03 0.00374  4.37954E+03 0.00336  4.24348E+03 0.00376  4.31111E+03 0.00474  4.64547E+03 0.00299  5.16220E+03 0.00306  5.06242E+03 0.00361  9.77922E+03 0.00263  1.57556E+04 0.00204  2.02255E+04 0.00230  5.37429E+04 0.00174  5.65420E+04 0.00149  6.06098E+04 0.00149  3.98288E+04 0.00149  2.85071E+04 0.00174  2.13789E+04 0.00161  2.52519E+04 0.00168  4.91636E+04 0.00155  6.86754E+04 0.00152  1.35414E+05 0.00150  2.12138E+05 0.00171  3.19549E+05 0.00171  2.05075E+05 0.00177  1.47116E+05 0.00189  1.05968E+05 0.00195  9.53498E+04 0.00192  9.39387E+04 0.00150  7.83946E+04 0.00165  5.30061E+04 0.00171  4.90654E+04 0.00161  4.36114E+04 0.00210  3.69329E+04 0.00228  2.91457E+04 0.00200  1.95442E+04 0.00226  6.91727E+03 0.00255 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.01932E+00 0.00056 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.31469E+18 0.00062  3.48614E+17 0.00142 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38063E-01 0.00012  1.54414E+00 0.00047 ];
INF_CAPT                  (idx, [1:   4]) = [  7.18545E-03 0.00064  3.37167E-02 0.00077 ];
INF_ABS                   (idx, [1:   4]) = [  9.18199E-03 0.00048  6.56483E-02 0.00109 ];
INF_FISS                  (idx, [1:   4]) = [  1.99653E-03 0.00050  3.19316E-02 0.00144 ];
INF_NSF                   (idx, [1:   4]) = [  5.24318E-03 0.00050  8.22451E-02 0.00149 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.62614E+00 5.8E-05  2.57567E+00 6.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04657E+02 8.6E-06  2.04124E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.74605E-08 0.00046  2.57015E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28877E-01 0.00013  1.47854E+00 0.00054 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43977E-01 0.00021  3.91993E-01 0.00068 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62270E-02 0.00035  9.28572E-02 0.00078 ];
INF_SCATT3                (idx, [1:   4]) = [  7.31759E-03 0.00378  2.79418E-02 0.00246 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03246E-02 0.00175 -8.75980E-03 0.00829 ];
INF_SCATT5                (idx, [1:   4]) = [  1.75673E-04 0.11315  6.50675E-03 0.00817 ];
INF_SCATT6                (idx, [1:   4]) = [  5.09673E-03 0.00300 -1.68499E-02 0.00268 ];
INF_SCATT7                (idx, [1:   4]) = [  7.53266E-04 0.02185  3.86405E-04 0.08544 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28918E-01 0.00013  1.47854E+00 0.00054 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43978E-01 0.00021  3.91993E-01 0.00068 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62276E-02 0.00035  9.28572E-02 0.00078 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.31736E-03 0.00378  2.79418E-02 0.00246 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03248E-02 0.00175 -8.75980E-03 0.00829 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.75685E-04 0.11339  6.50675E-03 0.00817 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.09676E-03 0.00300 -1.68499E-02 0.00268 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.53285E-04 0.02183  3.86405E-04 0.08544 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12743E-01 0.00032  9.99567E-01 0.00036 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56684E+00 0.00032  3.33479E-01 0.00036 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.14050E-03 0.00046  6.56483E-02 0.00109 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69695E-02 0.00021  6.67605E-02 0.00137 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11093E-01 0.00012  1.77837E-02 0.00040  1.16157E-03 0.00354  1.47738E+00 0.00054 ];
INF_S1                    (idx, [1:   8]) = [  2.38807E-01 0.00021  5.17050E-03 0.00076  4.99682E-04 0.00730  3.91494E-01 0.00068 ];
INF_S2                    (idx, [1:   8]) = [  9.77909E-02 0.00034 -1.56385E-03 0.00202  2.74322E-04 0.00927  9.25828E-02 0.00078 ];
INF_S3                    (idx, [1:   8]) = [  9.14088E-03 0.00286 -1.82329E-03 0.00147  9.70109E-05 0.02334  2.78448E-02 0.00249 ];
INF_S4                    (idx, [1:   8]) = [ -9.72944E-03 0.00184 -5.95188E-04 0.00476  3.70610E-07 1.00000 -8.76017E-03 0.00828 ];
INF_S5                    (idx, [1:   8]) = [  1.51828E-04 0.13595  2.38452E-05 0.08376 -4.09782E-05 0.03572  6.54773E-03 0.00819 ];
INF_S6                    (idx, [1:   8]) = [  5.23954E-03 0.00286 -1.42808E-04 0.01401 -4.98820E-05 0.03060 -1.68000E-02 0.00274 ];
INF_S7                    (idx, [1:   8]) = [  9.26503E-04 0.01740 -1.73237E-04 0.01306 -4.60904E-05 0.02716  4.32495E-04 0.07589 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11135E-01 0.00012  1.77837E-02 0.00040  1.16157E-03 0.00354  1.47738E+00 0.00054 ];
INF_SP1                   (idx, [1:   8]) = [  2.38807E-01 0.00021  5.17050E-03 0.00076  4.99682E-04 0.00730  3.91494E-01 0.00068 ];
INF_SP2                   (idx, [1:   8]) = [  9.77914E-02 0.00034 -1.56385E-03 0.00202  2.74322E-04 0.00927  9.25828E-02 0.00078 ];
INF_SP3                   (idx, [1:   8]) = [  9.14065E-03 0.00286 -1.82329E-03 0.00147  9.70109E-05 0.02334  2.78448E-02 0.00249 ];
INF_SP4                   (idx, [1:   8]) = [ -9.72961E-03 0.00184 -5.95188E-04 0.00476  3.70610E-07 1.00000 -8.76017E-03 0.00828 ];
INF_SP5                   (idx, [1:   8]) = [  1.51840E-04 0.13624  2.38452E-05 0.08376 -4.09782E-05 0.03572  6.54773E-03 0.00819 ];
INF_SP6                   (idx, [1:   8]) = [  5.23957E-03 0.00287 -1.42808E-04 0.01401 -4.98820E-05 0.03060 -1.68000E-02 0.00274 ];
INF_SP7                   (idx, [1:   8]) = [  9.26522E-04 0.01738 -1.73237E-04 0.01306 -4.60904E-05 0.02716  4.32495E-04 0.07589 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32086E-01 0.00054  1.14765E+00 0.00732 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33601E-01 0.00073  1.24083E+00 0.01039 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33590E-01 0.00089  1.23294E+00 0.00823 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29131E-01 0.00096  1.00406E+00 0.00662 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43626E+00 0.00053  2.90819E-01 0.00725 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42695E+00 0.00074  2.69330E-01 0.01036 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42703E+00 0.00089  2.70798E-01 0.00825 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45480E+00 0.00096  3.32329E-01 0.00652 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.82722E-03 0.00855  1.59919E-04 0.05460  1.03592E-03 0.02024  8.87146E-04 0.02200  2.63688E-03 0.01235  8.54810E-04 0.02243  2.52549E-04 0.04003 ];
LAMBDA                    (idx, [1:  14]) = [  7.56634E-01 0.02054  1.24994E-02 0.00036  3.12359E-02 0.00056  1.10235E-01 0.00059  3.21561E-01 0.00039  1.33091E+00 0.00125  8.96841E+00 0.00501 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0006' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:46 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:51:25 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284846 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.90471E-01  1.00192E+00  1.00163E+00  1.00279E+00  1.00319E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13452E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88655E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04730E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05158E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67372E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.52640E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.52553E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.53085E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.14210E-01 0.00111  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000664 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00013E+04 0.00084 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00013E+04 0.00084 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.51172E+02 ;
RUNNING_TIME              (idx, 1)        =  7.06543E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.32900E-01  4.32900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.94900E-01  9.65000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.93980E+01  3.34168E+00  2.60702E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  6.20783E-01  2.74833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.61833E-02  8.16667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.06542E+01  1.24935E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97028 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99906E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79342E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.38903E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.77972E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.72753E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.76058E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.22889E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62842E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.65680E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.16129E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.42126E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.51551E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.41566E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.09734E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.67969E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.00869E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.29935E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.37251E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.42919E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.40149E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.21394E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.18333E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.41515E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.28938E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.24610E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.56849E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 11 ;
BURNUP                     (idx, [1:  2])  = [  9.00000E+00  9.00080E+00 ];
BURN_DAYS                 (idx, 1)        =  2.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.80653E-01 0.00100 ];
U235_FISS                 (idx, [1:   4]) = [  8.61058E+15 0.00085  6.26422E-01 0.00054 ];
U238_FISS                 (idx, [1:   4]) = [  9.91336E+14 0.00277  7.21155E-02 0.00264 ];
PU239_FISS                (idx, [1:   4]) = [  3.82484E+15 0.00126  2.78272E-01 0.00117 ];
PU240_FISS                (idx, [1:   4]) = [  3.59197E+12 0.04525  2.61740E-04 0.04525 ];
PU241_FISS                (idx, [1:   4]) = [  3.07075E+14 0.00520  2.23374E-02 0.00511 ];
U235_CAPT                 (idx, [1:   4]) = [  1.91384E+15 0.00206  8.82640E-02 0.00192 ];
U238_CAPT                 (idx, [1:   4]) = [  8.73911E+15 0.00099  4.03043E-01 0.00071 ];
PU239_CAPT                (idx, [1:   4]) = [  2.12560E+15 0.00182  9.80405E-02 0.00179 ];
PU240_CAPT                (idx, [1:   4]) = [  1.01557E+15 0.00262  4.68392E-02 0.00256 ];
PU241_CAPT                (idx, [1:   4]) = [  1.09896E+14 0.00772  5.06880E-03 0.00771 ];
XE135_CAPT                (idx, [1:   4]) = [  7.40182E+14 0.00298  3.41399E-02 0.00296 ];
SM149_CAPT                (idx, [1:   4]) = [  1.95076E+14 0.00609  8.99805E-03 0.00610 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000664 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.61386E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000664 5.00761E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3033742 3.03801E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1923383 1.92604E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 43539 4.35581E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000664 5.00761E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -2.04891E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.56796E+16 2.0E-05  3.56796E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37446E+16 3.7E-06  1.37446E+16 3.7E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.16854E+16 0.00047  1.60900E+16 0.00042  5.59539E+15 0.00120 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.54300E+16 0.00029  2.98346E+16 0.00023  5.59539E+15 0.00120 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.56849E+16 0.00050  3.56849E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.68402E+18 0.00049  4.49304E+17 0.00044  1.23471E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.10888E+14 0.00500 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.57409E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.25983E+18 0.00067 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11467E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11467E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.59150E+00 0.00052 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.83276E-01 0.00035 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.52558E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24014E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94360E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96911E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.00874E+00 0.00057 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.99955E-01 0.00057 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.59590E+00 2.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04374E+02 3.7E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.00008E+00 0.00059  9.94179E-01 0.00057  5.77629E-03 0.00948 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.99875E-01 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  9.99974E-01 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.99875E-01 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  1.00866E+00 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72480E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72472E+01 9.0E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.48357E-07 0.00370 ];
IMP_EALF                  (idx, [1:   2]) = [  6.47010E-07 0.00156 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.45540E-01 0.00288 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.45243E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.89702E-03 0.00594  1.66866E-04 0.03471  9.94605E-04 0.01424  9.13371E-04 0.01497  2.65245E-03 0.00862  9.03242E-04 0.01401  2.66486E-04 0.02878 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.69793E-01 0.01438  9.98585E-03 0.02253  3.12138E-02 0.00039  1.10390E-01 0.00044  3.22000E-01 0.00029  1.32988E+00 0.00093  8.27354E+00 0.01259 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.75660E-03 0.00876  1.71614E-04 0.05281  9.69111E-04 0.02129  8.79174E-04 0.02230  2.60344E-03 0.01322  8.69586E-04 0.02205  2.63674E-04 0.04093 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.77485E-01 0.02106  1.25070E-02 0.00049  3.12227E-02 0.00056  1.10404E-01 0.00063  3.22215E-01 0.00043  1.33066E+00 0.00120  8.87599E+00 0.00564 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.58415E-05 0.00137  2.58355E-05 0.00137  2.68137E-05 0.01425 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.58392E-05 0.00124  2.58332E-05 0.00124  2.68060E-05 0.01420 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.77457E-03 0.00956  1.61351E-04 0.05666  9.79020E-04 0.02388  9.11458E-04 0.02335  2.60074E-03 0.01418  8.64974E-04 0.02481  2.57019E-04 0.04431 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.62152E-01 0.02358  1.25009E-02 0.00056  3.12090E-02 0.00071  1.10439E-01 0.00075  3.22132E-01 0.00048  1.32801E+00 0.00171  8.92751E+00 0.00675 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.54547E-05 0.00314  2.54450E-05 0.00314  2.56316E-05 0.03947 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.54520E-05 0.00308  2.54424E-05 0.00308  2.56138E-05 0.03944 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.78559E-03 0.03119  2.01352E-04 0.18647  1.07399E-03 0.07739  9.38249E-04 0.07545  2.48762E-03 0.04790  8.45153E-04 0.08639  2.39226E-04 0.15750 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.38760E-01 0.07121  1.25111E-02 0.00174  3.11247E-02 0.00171  1.10263E-01 0.00157  3.23105E-01 0.00148  1.32964E+00 0.00382  8.66865E+00 0.02172 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.80773E-03 0.03016  2.01653E-04 0.18129  1.05522E-03 0.07559  9.52141E-04 0.07371  2.52391E-03 0.04695  8.48467E-04 0.08353  2.26343E-04 0.15352 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.38558E-01 0.06934  1.25111E-02 0.00174  3.11262E-02 0.00171  1.10262E-01 0.00158  3.23116E-01 0.00147  1.32918E+00 0.00389  8.67992E+00 0.02160 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.28112E+02 0.03121 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.56382E-05 0.00086 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.56361E-05 0.00063 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.91535E-03 0.00603 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.30806E+02 0.00607 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.81009E-07 0.00071 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.76630E-06 0.00044  2.76603E-06 0.00044  2.81105E-06 0.00539 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.88860E-05 0.00090  3.89070E-05 0.00089  3.53099E-05 0.01043 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.50358E-01 0.00032  6.50303E-01 0.00033  6.72111E-01 0.00977 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07344E+01 0.01364 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.52553E+01 0.00049  3.37690E+01 0.00049 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.64117E+04 0.00315  2.99413E+05 0.00148  6.09542E+05 0.00087  6.51603E+05 0.00068  5.98414E+05 0.00079  6.41398E+05 0.00058  4.35936E+05 0.00055  3.85587E+05 0.00058  2.95107E+05 0.00070  2.41269E+05 0.00088  2.07638E+05 0.00093  1.87592E+05 0.00081  1.72869E+05 0.00101  1.64342E+05 0.00110  1.60434E+05 0.00062  1.38496E+05 0.00080  1.36736E+05 0.00114  1.35467E+05 0.00094  1.33196E+05 0.00109  2.60177E+05 0.00081  2.51229E+05 0.00059  1.81263E+05 0.00079  1.17352E+05 0.00116  1.35590E+05 0.00092  1.28485E+05 0.00104  1.15368E+05 0.00077  1.88355E+05 0.00048  4.34483E+04 0.00170  5.44580E+04 0.00143  4.93443E+04 0.00158  2.87448E+04 0.00134  4.97403E+04 0.00149  3.35994E+04 0.00139  2.80707E+04 0.00181  5.09795E+03 0.00319  4.63399E+03 0.00509  4.23026E+03 0.00365  4.08677E+03 0.00390  4.17326E+03 0.00316  4.50059E+03 0.00373  5.09530E+03 0.00382  5.00304E+03 0.00251  9.66633E+03 0.00291  1.56181E+04 0.00239  2.01654E+04 0.00196  5.33992E+04 0.00162  5.61958E+04 0.00167  6.01526E+04 0.00151  3.95353E+04 0.00107  2.82325E+04 0.00178  2.11448E+04 0.00169  2.49093E+04 0.00110  4.85822E+04 0.00147  6.80055E+04 0.00108  1.34243E+05 0.00092  2.10279E+05 0.00111  3.16998E+05 0.00132  2.03481E+05 0.00153  1.46249E+05 0.00171  1.05167E+05 0.00140  9.46918E+04 0.00162  9.32940E+04 0.00167  7.79031E+04 0.00187  5.27416E+04 0.00171  4.88104E+04 0.00163  4.34372E+04 0.00185  3.67193E+04 0.00192  2.89931E+04 0.00194  1.94315E+04 0.00207  6.89081E+03 0.00193 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.00876E+00 0.00060 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.33325E+18 0.00057  3.50799E+17 0.00141 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38155E-01 9.9E-05  1.54603E+00 0.00047 ];
INF_CAPT                  (idx, [1:   4]) = [  7.28462E-03 0.00058  3.41346E-02 0.00073 ];
INF_ABS                   (idx, [1:   4]) = [  9.25286E-03 0.00047  6.58410E-02 0.00109 ];
INF_FISS                  (idx, [1:   4]) = [  1.96823E-03 0.00042  3.17065E-02 0.00148 ];
INF_NSF                   (idx, [1:   4]) = [  5.18156E-03 0.00040  8.20321E-02 0.00151 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.63260E+00 7.0E-05  2.58723E+00 4.7E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04748E+02 8.4E-06  2.04285E+02 8.2E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.72678E-08 0.00039  2.57169E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28896E-01 9.9E-05  1.48022E+00 0.00053 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44008E-01 0.00023  3.92423E-01 0.00060 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62520E-02 0.00030  9.30193E-02 0.00092 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32454E-03 0.00259  2.79513E-02 0.00277 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03346E-02 0.00220 -8.71240E-03 0.00752 ];
INF_SCATT5                (idx, [1:   4]) = [  1.35256E-04 0.12486  6.57122E-03 0.00706 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07642E-03 0.00221 -1.69202E-02 0.00219 ];
INF_SCATT7                (idx, [1:   4]) = [  7.60394E-04 0.01878  4.24605E-04 0.10005 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28937E-01 9.9E-05  1.48022E+00 0.00053 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44008E-01 0.00023  3.92423E-01 0.00060 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62522E-02 0.00030  9.30193E-02 0.00092 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32427E-03 0.00259  2.79513E-02 0.00277 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03344E-02 0.00220 -8.71240E-03 0.00752 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.35098E-04 0.12473  6.57122E-03 0.00706 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07616E-03 0.00221 -1.69202E-02 0.00219 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.60335E-04 0.01881  4.24605E-04 0.10005 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12779E-01 0.00031  1.00133E+00 0.00041 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56658E+00 0.00031  3.32891E-01 0.00041 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.21210E-03 0.00048  6.58410E-02 0.00109 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69611E-02 0.00024  6.69675E-02 0.00107 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11194E-01 9.7E-05  1.77019E-02 0.00033  1.16252E-03 0.00518  1.47906E+00 0.00053 ];
INF_S1                    (idx, [1:   8]) = [  2.38864E-01 0.00023  5.14349E-03 0.00090  4.99294E-04 0.00729  3.91924E-01 0.00060 ];
INF_S2                    (idx, [1:   8]) = [  9.78232E-02 0.00030 -1.57125E-03 0.00163  2.71376E-04 0.00765  9.27480E-02 0.00093 ];
INF_S3                    (idx, [1:   8]) = [  9.15292E-03 0.00198 -1.82838E-03 0.00171  1.00555E-04 0.01486  2.78508E-02 0.00278 ];
INF_S4                    (idx, [1:   8]) = [ -9.74443E-03 0.00221 -5.90127E-04 0.00522  1.40439E-06 0.93593 -8.71381E-03 0.00747 ];
INF_S5                    (idx, [1:   8]) = [  1.06425E-04 0.15274  2.88312E-05 0.08271 -4.02621E-05 0.03364  6.61148E-03 0.00700 ];
INF_S6                    (idx, [1:   8]) = [  5.21645E-03 0.00218 -1.40028E-04 0.01427 -4.95777E-05 0.01786 -1.68706E-02 0.00217 ];
INF_S7                    (idx, [1:   8]) = [  9.25776E-04 0.01538 -1.65382E-04 0.01339 -4.50086E-05 0.02575  4.69614E-04 0.08967 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11235E-01 9.7E-05  1.77019E-02 0.00033  1.16252E-03 0.00518  1.47906E+00 0.00053 ];
INF_SP1                   (idx, [1:   8]) = [  2.38865E-01 0.00023  5.14349E-03 0.00090  4.99294E-04 0.00729  3.91924E-01 0.00060 ];
INF_SP2                   (idx, [1:   8]) = [  9.78235E-02 0.00030 -1.57125E-03 0.00163  2.71376E-04 0.00765  9.27480E-02 0.00093 ];
INF_SP3                   (idx, [1:   8]) = [  9.15265E-03 0.00199 -1.82838E-03 0.00171  1.00555E-04 0.01486  2.78508E-02 0.00278 ];
INF_SP4                   (idx, [1:   8]) = [ -9.74428E-03 0.00221 -5.90127E-04 0.00522  1.40439E-06 0.93593 -8.71381E-03 0.00747 ];
INF_SP5                   (idx, [1:   8]) = [  1.06267E-04 0.15260  2.88312E-05 0.08271 -4.02621E-05 0.03364  6.61148E-03 0.00700 ];
INF_SP6                   (idx, [1:   8]) = [  5.21619E-03 0.00218 -1.40028E-04 0.01427 -4.95777E-05 0.01786 -1.68706E-02 0.00217 ];
INF_SP7                   (idx, [1:   8]) = [  9.25717E-04 0.01539 -1.65382E-04 0.01339 -4.50086E-05 0.02575  4.69614E-04 0.08967 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32014E-01 0.00055  1.16005E+00 0.00811 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33768E-01 0.00080  1.24342E+00 0.00837 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33631E-01 0.00079  1.24654E+00 0.01003 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28724E-01 0.00110  1.02180E+00 0.00864 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43670E+00 0.00055  2.87783E-01 0.00788 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42594E+00 0.00080  2.68526E-01 0.00831 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42677E+00 0.00079  2.68032E-01 0.00970 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45740E+00 0.00110  3.26791E-01 0.00843 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.75660E-03 0.00876  1.71614E-04 0.05281  9.69111E-04 0.02129  8.79174E-04 0.02230  2.60344E-03 0.01322  8.69586E-04 0.02205  2.63674E-04 0.04093 ];
LAMBDA                    (idx, [1:  14]) = [  7.77485E-01 0.02106  1.25070E-02 0.00049  3.12227E-02 0.00056  1.10404E-01 0.00063  3.22215E-01 0.00043  1.33066E+00 0.00120  8.87599E+00 0.00564 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0006' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:46 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:57:27 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284846 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.90903E-01  1.00333E+00  1.00210E+00  1.00140E+00  1.00227E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.5E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13166E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88683E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05122E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05550E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67215E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.52411E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.52324E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.50411E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.12376E-01 0.00115  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000599 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00012E+04 0.00082 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00012E+04 0.00082 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.81313E+02 ;
RUNNING_TIME              (idx, 1)        =  7.66889E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.32900E-01  4.32900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.14783E-01  9.73333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.53572E+01  3.35380E+00  2.60535E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  6.76017E-01  2.75833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.96333E-02  8.16663E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.66888E+01  1.24862E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97221 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99902E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79809E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.41255E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.77344E+04 ;
TOT_SF_RATE               (idx, 1)        =  5.19254E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.77914E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.24192E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.63338E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.64922E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.27326E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.46359E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  6.41865E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.51422E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.31394E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.71217E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.10738E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.31486E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.39346E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.45018E+14 ;
CS134_ACTIVITY            (idx, 1)        =  7.87602E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.34840E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.20358E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.40010E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  7.23522E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.25239E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.61280E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 12 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E+01  1.00009E+01 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.90685E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  8.28832E+15 0.00082  6.04009E-01 0.00058 ];
U238_FISS                 (idx, [1:   4]) = [  1.00272E+15 0.00269  7.30617E-02 0.00251 ];
PU239_FISS                (idx, [1:   4]) = [  4.04920E+15 0.00125  2.95086E-01 0.00112 ];
PU240_FISS                (idx, [1:   4]) = [  4.24948E+12 0.04030  3.09500E-04 0.04027 ];
PU241_FISS                (idx, [1:   4]) = [  3.68536E+14 0.00442  2.68572E-02 0.00439 ];
U235_CAPT                 (idx, [1:   4]) = [  1.83576E+15 0.00196  8.28998E-02 0.00192 ];
U238_CAPT                 (idx, [1:   4]) = [  8.80635E+15 0.00105  3.97639E-01 0.00073 ];
PU239_CAPT                (idx, [1:   4]) = [  2.24902E+15 0.00180  1.01559E-01 0.00172 ];
PU240_CAPT                (idx, [1:   4]) = [  1.13577E+15 0.00248  5.12852E-02 0.00239 ];
PU241_CAPT                (idx, [1:   4]) = [  1.32783E+14 0.00758  5.99595E-03 0.00756 ];
XE135_CAPT                (idx, [1:   4]) = [  7.48268E+14 0.00311  3.37911E-02 0.00310 ];
SM149_CAPT                (idx, [1:   4]) = [  1.94990E+14 0.00571  8.80525E-03 0.00569 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000599 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.00671E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000599 5.00801E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3060238 3.06488E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1896465 1.89922E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 43896 4.39029E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000599 5.00801E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.56348E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.57928E+16 2.1E-05  3.57928E+16 2.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37353E+16 4.0E-06  1.37353E+16 4.0E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.21558E+16 0.00044  1.64937E+16 0.00043  5.66210E+15 0.00113 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.58911E+16 0.00027  3.02290E+16 0.00023  5.66210E+15 0.00113 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.61280E+16 0.00052  3.61280E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.70346E+18 0.00049  4.54105E+17 0.00048  1.24935E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.17228E+14 0.00518 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.62083E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.27477E+18 0.00063 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11351E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11351E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.58111E+00 0.00049 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.82628E-01 0.00034 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.51098E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23952E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94300E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96902E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.98590E-01 0.00056 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.89822E-01 0.00057 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.60591E+00 2.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04513E+02 4.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.89733E-01 0.00058  9.84241E-01 0.00057  5.58040E-03 0.00891 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.90112E-01 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  9.90853E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.90112E-01 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  9.98876E-01 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72272E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72280E+01 8.6E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.61884E-07 0.00364 ];
IMP_EALF                  (idx, [1:   2]) = [  6.59562E-07 0.00148 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.48828E-01 0.00282 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.48856E-01 0.00109 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.88545E-03 0.00588  1.60426E-04 0.03641  9.92851E-04 0.01448  9.11283E-04 0.01426  2.64585E-03 0.00903  9.02502E-04 0.01603  2.72534E-04 0.02753 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.83138E-01 0.01443  9.83011E-03 0.02336  3.11550E-02 0.00042  1.10375E-01 0.00041  3.21884E-01 0.00028  1.32400E+00 0.00117  8.22651E+00 0.01352 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.70835E-03 0.00879  1.50070E-04 0.05484  9.90306E-04 0.02181  8.64315E-04 0.02400  2.57300E-03 0.01315  8.70326E-04 0.02176  2.60337E-04 0.04069 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.75184E-01 0.02052  1.25092E-02 0.00051  3.11722E-02 0.00059  1.10493E-01 0.00060  3.21924E-01 0.00043  1.32325E+00 0.00164  8.87112E+00 0.00581 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.60130E-05 0.00133  2.60016E-05 0.00133  2.79753E-05 0.01463 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.57417E-05 0.00120  2.57304E-05 0.00120  2.76806E-05 0.01461 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.63588E-03 0.00908  1.40419E-04 0.06330  9.74349E-04 0.02312  8.67453E-04 0.02417  2.54023E-03 0.01450  8.65528E-04 0.02437  2.47910E-04 0.04635 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.58041E-01 0.02335  1.25021E-02 0.00056  3.11435E-02 0.00075  1.10443E-01 0.00078  3.21836E-01 0.00048  1.32224E+00 0.00216  8.81596E+00 0.00849 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.55189E-05 0.00294  2.55036E-05 0.00296  2.72352E-05 0.04792 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.52518E-05 0.00286  2.52367E-05 0.00288  2.69558E-05 0.04813 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.81794E-03 0.03239  1.49398E-04 0.18329  1.05172E-03 0.07658  8.55607E-04 0.07792  2.47106E-03 0.04786  1.03013E-03 0.07472  2.60020E-04 0.14129 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.18025E-01 0.07582  1.24892E-02 4.5E-05  3.11493E-02 0.00178  1.10164E-01 0.00175  3.21605E-01 0.00133  1.33234E+00 0.00323  8.95414E+00 0.01667 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.85242E-03 0.03143  1.41698E-04 0.18171  1.03967E-03 0.07420  8.72060E-04 0.07698  2.50924E-03 0.04670  1.03596E-03 0.07194  2.53798E-04 0.13860 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.06840E-01 0.07466  1.24891E-02 4.5E-05  3.11496E-02 0.00176  1.10154E-01 0.00174  3.21497E-01 0.00132  1.33240E+00 0.00322  8.95083E+00 0.01669 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.28521E+02 0.03215 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.57916E-05 0.00083 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.55224E-05 0.00059 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.68762E-03 0.00644 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.20580E+02 0.00645 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.80194E-07 0.00071 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.75392E-06 0.00042  2.75377E-06 0.00042  2.78443E-06 0.00523 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.88994E-05 0.00087  3.89142E-05 0.00088  3.63804E-05 0.01111 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.48896E-01 0.00032  6.48864E-01 0.00033  6.65224E-01 0.00887 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07681E+01 0.01458 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.52324E+01 0.00047  3.36915E+01 0.00050 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.72910E+04 0.00254  3.00601E+05 0.00155  6.09130E+05 0.00101  6.51334E+05 0.00076  5.98218E+05 0.00067  6.41942E+05 0.00081  4.35403E+05 0.00053  3.85401E+05 0.00089  2.94849E+05 0.00061  2.40985E+05 0.00067  2.08043E+05 0.00074  1.87444E+05 0.00069  1.72947E+05 0.00074  1.64467E+05 0.00079  1.60035E+05 0.00064  1.38652E+05 0.00089  1.36795E+05 0.00108  1.35325E+05 0.00111  1.33120E+05 0.00097  2.60065E+05 0.00061  2.51110E+05 0.00071  1.80906E+05 0.00093  1.17161E+05 0.00091  1.35374E+05 0.00094  1.28552E+05 0.00065  1.15412E+05 0.00104  1.88326E+05 0.00074  4.33782E+04 0.00184  5.43404E+04 0.00126  4.92809E+04 0.00172  2.87768E+04 0.00216  4.98501E+04 0.00115  3.35650E+04 0.00179  2.80195E+04 0.00146  5.05017E+03 0.00279  4.55038E+03 0.00363  4.13008E+03 0.00368  3.97274E+03 0.00451  4.05599E+03 0.00319  4.41779E+03 0.00412  5.02892E+03 0.00281  4.91220E+03 0.00365  9.58627E+03 0.00265  1.55191E+04 0.00246  2.00044E+04 0.00159  5.31121E+04 0.00166  5.58254E+04 0.00157  5.99966E+04 0.00115  3.92288E+04 0.00136  2.80538E+04 0.00170  2.09129E+04 0.00170  2.46845E+04 0.00181  4.80898E+04 0.00124  6.74429E+04 0.00098  1.33444E+05 0.00116  2.09557E+05 0.00116  3.16133E+05 0.00096  2.02878E+05 0.00113  1.45870E+05 0.00131  1.05036E+05 0.00140  9.45507E+04 0.00145  9.32763E+04 0.00136  7.79372E+04 0.00148  5.27276E+04 0.00138  4.87755E+04 0.00163  4.34521E+04 0.00142  3.67089E+04 0.00163  2.90133E+04 0.00148  1.93983E+04 0.00180  6.89149E+03 0.00162 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.99631E-01 0.00049 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.34946E+18 0.00042  3.54034E+17 0.00107 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38251E-01 9.6E-05  1.54913E+00 0.00038 ];
INF_CAPT                  (idx, [1:   4]) = [  7.37233E-03 0.00048  3.44840E-02 0.00058 ];
INF_ABS                   (idx, [1:   4]) = [  9.31320E-03 0.00038  6.58884E-02 0.00083 ];
INF_FISS                  (idx, [1:   4]) = [  1.94087E-03 0.00054  3.14044E-02 0.00111 ];
INF_NSF                   (idx, [1:   4]) = [  5.12329E-03 0.00055  8.15870E-02 0.00114 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.63969E+00 5.1E-05  2.59795E+00 4.9E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04840E+02 6.3E-06  2.04436E+02 8.6E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.71248E-08 0.00029  2.57409E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28949E-01 9.7E-05  1.48327E+00 0.00043 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44037E-01 0.00016  3.93436E-01 0.00057 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62734E-02 0.00025  9.32235E-02 0.00099 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32654E-03 0.00303  2.78922E-02 0.00328 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03191E-02 0.00185 -8.75722E-03 0.00721 ];
INF_SCATT5                (idx, [1:   4]) = [  1.64076E-04 0.11262  6.56587E-03 0.00772 ];
INF_SCATT6                (idx, [1:   4]) = [  5.11186E-03 0.00330 -1.69625E-02 0.00281 ];
INF_SCATT7                (idx, [1:   4]) = [  7.39960E-04 0.01716  5.22687E-04 0.08170 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28992E-01 9.7E-05  1.48327E+00 0.00043 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44037E-01 0.00016  3.93436E-01 0.00057 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62732E-02 0.00025  9.32235E-02 0.00099 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32656E-03 0.00303  2.78922E-02 0.00328 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03191E-02 0.00185 -8.75722E-03 0.00721 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.63884E-04 0.11267  6.56587E-03 0.00772 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.11216E-03 0.00330 -1.69625E-02 0.00281 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.39699E-04 0.01710  5.22687E-04 0.08170 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12701E-01 0.00024  1.00346E+00 0.00031 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56714E+00 0.00024  3.32184E-01 0.00031 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.27033E-03 0.00040  6.58884E-02 0.00083 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69681E-02 0.00027  6.70252E-02 0.00094 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11283E-01 9.2E-05  1.76663E-02 0.00036  1.16476E-03 0.00494  1.48211E+00 0.00043 ];
INF_S1                    (idx, [1:   8]) = [  2.38907E-01 0.00017  5.12937E-03 0.00072  5.00213E-04 0.00564  3.92935E-01 0.00057 ];
INF_S2                    (idx, [1:   8]) = [  9.78458E-02 0.00024 -1.57244E-03 0.00198  2.70229E-04 0.00767  9.29533E-02 0.00099 ];
INF_S3                    (idx, [1:   8]) = [  9.14992E-03 0.00255 -1.82338E-03 0.00157  9.84728E-05 0.01621  2.77937E-02 0.00329 ];
INF_S4                    (idx, [1:   8]) = [ -9.73081E-03 0.00206 -5.88339E-04 0.00420 -9.41829E-07 1.00000 -8.75628E-03 0.00716 ];
INF_S5                    (idx, [1:   8]) = [  1.33815E-04 0.13172  3.02613E-05 0.08318 -3.98588E-05 0.04455  6.60573E-03 0.00763 ];
INF_S6                    (idx, [1:   8]) = [  5.24372E-03 0.00315 -1.31854E-04 0.01693 -4.94198E-05 0.03029 -1.69131E-02 0.00281 ];
INF_S7                    (idx, [1:   8]) = [  9.07989E-04 0.01372 -1.68029E-04 0.01160 -4.44157E-05 0.02819  5.67103E-04 0.07568 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11325E-01 9.2E-05  1.76663E-02 0.00036  1.16476E-03 0.00494  1.48211E+00 0.00043 ];
INF_SP1                   (idx, [1:   8]) = [  2.38908E-01 0.00017  5.12937E-03 0.00072  5.00213E-04 0.00564  3.92935E-01 0.00057 ];
INF_SP2                   (idx, [1:   8]) = [  9.78456E-02 0.00024 -1.57244E-03 0.00198  2.70229E-04 0.00767  9.29533E-02 0.00099 ];
INF_SP3                   (idx, [1:   8]) = [  9.14993E-03 0.00255 -1.82338E-03 0.00157  9.84728E-05 0.01621  2.77937E-02 0.00329 ];
INF_SP4                   (idx, [1:   8]) = [ -9.73076E-03 0.00206 -5.88339E-04 0.00420 -9.41829E-07 1.00000 -8.75628E-03 0.00716 ];
INF_SP5                   (idx, [1:   8]) = [  1.33623E-04 0.13183  3.02613E-05 0.08318 -3.98588E-05 0.04455  6.60573E-03 0.00763 ];
INF_SP6                   (idx, [1:   8]) = [  5.24402E-03 0.00316 -1.31854E-04 0.01693 -4.94198E-05 0.03029 -1.69131E-02 0.00281 ];
INF_SP7                   (idx, [1:   8]) = [  9.07728E-04 0.01367 -1.68029E-04 0.01160 -4.44157E-05 0.02819  5.67103E-04 0.07568 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31807E-01 0.00044  1.16223E+00 0.00592 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33298E-01 0.00073  1.25802E+00 0.00817 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33438E-01 0.00080  1.25485E+00 0.00896 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28751E-01 0.00073  1.01196E+00 0.00524 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43799E+00 0.00044  2.87041E-01 0.00583 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42881E+00 0.00073  2.65378E-01 0.00794 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42795E+00 0.00080  2.66136E-01 0.00877 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45721E+00 0.00073  3.29609E-01 0.00517 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.70835E-03 0.00879  1.50070E-04 0.05484  9.90306E-04 0.02181  8.64315E-04 0.02400  2.57300E-03 0.01315  8.70326E-04 0.02176  2.60337E-04 0.04069 ];
LAMBDA                    (idx, [1:  14]) = [  7.75184E-01 0.02052  1.25092E-02 0.00051  3.11722E-02 0.00059  1.10493E-01 0.00060  3.21924E-01 0.00043  1.32325E+00 0.00164  8.87112E+00 0.00581 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0006' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:46 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:03:29 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284846 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.89187E-01  1.00175E+00  1.00368E+00  1.00223E+00  1.00316E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14352E-02 0.00111  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88565E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05872E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06299E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67225E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.51454E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.51365E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.44029E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.16103E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001208 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00024E+04 0.00090 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00024E+04 0.00090 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.11415E+02 ;
RUNNING_TIME              (idx, 1)        =  8.27157E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.32900E-01  4.32900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.34883E-01  9.90000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  8.13072E+01  3.33933E+00  2.61070E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  7.32367E-01  2.79833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.30500E-02  8.16667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  8.27156E+01  1.24997E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97384 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00057E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80207E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.42488E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.74785E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.10973E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.81234E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.26542E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61251E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.62128E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.57802E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.54274E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  8.99693E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.71125E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.78327E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.77161E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.34527E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.32474E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.42629E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.47205E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.21449E+13 ;
CS137_ACTIVITY            (idx, 1)        =  1.68419E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.21273E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.35952E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.43178E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.26105E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.73635E+12 0.00053  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 13 ;
BURNUP                     (idx, [1:  2])  = [  1.25000E+01  1.25011E+01 ];
BURN_DAYS                 (idx, 1)        =  3.12500E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.14383E-01 0.00112 ];
U235_FISS                 (idx, [1:   4]) = [  7.58159E+15 0.00087  5.52460E-01 0.00067 ];
U238_FISS                 (idx, [1:   4]) = [  1.03407E+15 0.00286  7.53365E-02 0.00266 ];
PU239_FISS                (idx, [1:   4]) = [  4.55336E+15 0.00127  3.31779E-01 0.00104 ];
PU240_FISS                (idx, [1:   4]) = [  5.79522E+12 0.03519  4.22178E-04 0.03518 ];
PU241_FISS                (idx, [1:   4]) = [  5.37750E+14 0.00367  3.91776E-02 0.00352 ];
U235_CAPT                 (idx, [1:   4]) = [  1.69042E+15 0.00196  7.23517E-02 0.00190 ];
U238_CAPT                 (idx, [1:   4]) = [  9.02414E+15 0.00108  3.86208E-01 0.00076 ];
PU239_CAPT                (idx, [1:   4]) = [  2.51645E+15 0.00166  1.07711E-01 0.00164 ];
PU240_CAPT                (idx, [1:   4]) = [  1.41113E+15 0.00246  6.03923E-02 0.00233 ];
PU241_CAPT                (idx, [1:   4]) = [  1.95238E+14 0.00597  8.35702E-03 0.00597 ];
XE135_CAPT                (idx, [1:   4]) = [  7.57237E+14 0.00317  3.24100E-02 0.00313 ];
SM149_CAPT                (idx, [1:   4]) = [  2.05575E+14 0.00600  8.80095E-03 0.00606 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001208 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.77719E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001208 5.00778E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3122503 3.12671E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1834268 1.83662E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44437 4.44416E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001208 5.00778E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -7.26432E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.60510E+16 2.1E-05  3.60510E+16 2.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37134E+16 4.1E-06  1.37134E+16 4.1E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.33263E+16 0.00046  1.74971E+16 0.00046  5.82916E+15 0.00122 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.70397E+16 0.00029  3.12106E+16 0.00026  5.82916E+15 0.00122 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.73635E+16 0.00053  3.73635E+16 0.00053  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.75731E+18 0.00050  4.67074E+17 0.00049  1.29024E+18 0.00056 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.32123E+14 0.00489 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.73718E+16 0.00030 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.31471E+18 0.00067 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11061E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11061E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.56087E+00 0.00052 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.80952E-01 0.00036 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.45143E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23905E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94238E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96855E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.74340E-01 0.00063 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.65680E-01 0.00063 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.62888E+00 2.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04839E+02 4.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.65778E-01 0.00065  9.60264E-01 0.00063  5.41644E-03 0.00924 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.66227E-01 0.00030 ];
COL_KEFF                  (idx, [1:   2]) = [  9.65003E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.66227E-01 0.00030 ];
ABS_KINF                  (idx, [1:   2]) = [  9.74903E-01 0.00029 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71797E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71794E+01 9.4E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.94316E-07 0.00381 ];
IMP_EALF                  (idx, [1:   2]) = [  6.92479E-07 0.00161 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.56698E-01 0.00287 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.56931E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.87300E-03 0.00623  1.56182E-04 0.03694  1.02747E-03 0.01498  9.30087E-04 0.01523  2.56348E-03 0.00951  9.14591E-04 0.01580  2.81186E-04 0.02723 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.93295E-01 0.01427  9.54729E-03 0.02503  3.10783E-02 0.00046  1.10351E-01 0.00042  3.22165E-01 0.00031  1.31521E+00 0.00162  8.36359E+00 0.01179 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.62692E-03 0.00944  1.44703E-04 0.05390  9.69663E-04 0.02229  8.84313E-04 0.02310  2.46904E-03 0.01441  8.73969E-04 0.02480  2.85236E-04 0.04265 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.17663E-01 0.02290  1.25321E-02 0.00077  3.10865E-02 0.00063  1.10311E-01 0.00061  3.22182E-01 0.00043  1.31568E+00 0.00204  8.81447E+00 0.00665 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.63822E-05 0.00138  2.63691E-05 0.00139  2.87717E-05 0.01514 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.54738E-05 0.00121  2.54612E-05 0.00122  2.77792E-05 0.01512 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.61407E-03 0.00931  1.50142E-04 0.05939  9.84443E-04 0.02425  9.13301E-04 0.02333  2.39829E-03 0.01602  8.86493E-04 0.02489  2.81402E-04 0.04543 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.10362E-01 0.02458  1.25342E-02 0.00115  3.10797E-02 0.00077  1.10240E-01 0.00073  3.21979E-01 0.00052  1.31469E+00 0.00255  8.84002E+00 0.00858 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.59091E-05 0.00326  2.58946E-05 0.00326  2.65505E-05 0.03531 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.50176E-05 0.00319  2.50033E-05 0.00320  2.56615E-05 0.03538 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.46060E-03 0.03480  1.40785E-04 0.19363  1.09922E-03 0.07950  8.26107E-04 0.08574  2.29065E-03 0.05257  8.21070E-04 0.08412  2.82770E-04 0.14382 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.85660E-01 0.07758  1.25427E-02 0.00302  3.10810E-02 0.00176  1.10229E-01 0.00185  3.22037E-01 0.00144  1.32020E+00 0.00495  8.78258E+00 0.02053 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.41838E-03 0.03362  1.28612E-04 0.18934  1.10726E-03 0.07765  8.10289E-04 0.08507  2.28906E-03 0.05096  8.02115E-04 0.08094  2.81037E-04 0.13786 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.85853E-01 0.07725  1.25427E-02 0.00302  3.10827E-02 0.00175  1.10227E-01 0.00184  3.22040E-01 0.00144  1.32062E+00 0.00489  8.77526E+00 0.02078 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.11907E+02 0.03504 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.61394E-05 0.00091 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.52393E-05 0.00060 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.54230E-03 0.00645 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.12123E+02 0.00651 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.76836E-07 0.00077 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.72928E-06 0.00043  2.72929E-06 0.00043  2.72852E-06 0.00545 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.89245E-05 0.00093  3.89430E-05 0.00092  3.57990E-05 0.01049 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.42975E-01 0.00034  6.42982E-01 0.00034  6.54574E-01 0.00983 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05675E+01 0.01455 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.51365E+01 0.00050  3.35482E+01 0.00050 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.78880E+04 0.00310  3.01202E+05 0.00180  6.09552E+05 0.00078  6.51428E+05 0.00061  5.98164E+05 0.00071  6.42142E+05 0.00076  4.35703E+05 0.00066  3.85110E+05 0.00070  2.95069E+05 0.00080  2.41238E+05 0.00070  2.07883E+05 0.00103  1.87122E+05 0.00076  1.72988E+05 0.00089  1.64279E+05 0.00103  1.60091E+05 0.00081  1.38251E+05 0.00106  1.36544E+05 0.00090  1.35330E+05 0.00092  1.33101E+05 0.00078  2.59666E+05 0.00077  2.50945E+05 0.00065  1.81183E+05 0.00072  1.17324E+05 0.00089  1.35476E+05 0.00054  1.28633E+05 0.00099  1.15171E+05 0.00115  1.87483E+05 0.00051  4.33008E+04 0.00176  5.42490E+04 0.00157  4.91629E+04 0.00122  2.88217E+04 0.00194  4.94910E+04 0.00167  3.33195E+04 0.00193  2.75733E+04 0.00174  4.90198E+03 0.00306  4.35100E+03 0.00296  3.87827E+03 0.00324  3.73500E+03 0.00320  3.81861E+03 0.00388  4.16742E+03 0.00359  4.85043E+03 0.00353  4.80577E+03 0.00378  9.35781E+03 0.00245  1.52469E+04 0.00193  1.97519E+04 0.00188  5.25994E+04 0.00157  5.52685E+04 0.00116  5.92728E+04 0.00112  3.86779E+04 0.00153  2.74349E+04 0.00171  2.04535E+04 0.00173  2.41067E+04 0.00159  4.69865E+04 0.00154  6.62258E+04 0.00154  1.31361E+05 0.00135  2.07031E+05 0.00146  3.13119E+05 0.00128  2.01405E+05 0.00151  1.44795E+05 0.00152  1.04390E+05 0.00181  9.38442E+04 0.00170  9.26107E+04 0.00170  7.76054E+04 0.00167  5.25253E+04 0.00194  4.85448E+04 0.00207  4.32007E+04 0.00229  3.65495E+04 0.00224  2.88420E+04 0.00191  1.92990E+04 0.00192  6.85084E+03 0.00259 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.73658E-01 0.00064 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.39484E+18 0.00066  3.62501E+17 0.00150 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38401E-01 0.00012  1.55514E+00 0.00048 ];
INF_CAPT                  (idx, [1:   4]) = [  7.56740E-03 0.00062  3.52341E-02 0.00084 ];
INF_ABS                   (idx, [1:   4]) = [  9.44429E-03 0.00050  6.58487E-02 0.00117 ];
INF_FISS                  (idx, [1:   4]) = [  1.87689E-03 0.00039  3.06146E-02 0.00156 ];
INF_NSF                   (idx, [1:   4]) = [  4.98417E-03 0.00038  8.02896E-02 0.00160 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.65555E+00 7.1E-05  2.62259E+00 5.7E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05059E+02 9.5E-06  2.04786E+02 1.0E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.67297E-08 0.00043  2.57827E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28939E-01 0.00012  1.48924E+00 0.00055 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44124E-01 0.00025  3.94937E-01 0.00061 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62814E-02 0.00030  9.33131E-02 0.00108 ];
INF_SCATT3                (idx, [1:   4]) = [  7.31924E-03 0.00290  2.80309E-02 0.00245 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03100E-02 0.00172 -8.81534E-03 0.00532 ];
INF_SCATT5                (idx, [1:   4]) = [  1.66862E-04 0.10033  6.53248E-03 0.00630 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10854E-03 0.00331 -1.69784E-02 0.00232 ];
INF_SCATT7                (idx, [1:   4]) = [  7.60420E-04 0.02250  4.42611E-04 0.06371 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28980E-01 0.00012  1.48924E+00 0.00055 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44124E-01 0.00025  3.94937E-01 0.00061 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62815E-02 0.00030  9.33131E-02 0.00108 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.31943E-03 0.00290  2.80309E-02 0.00245 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03100E-02 0.00172 -8.81534E-03 0.00532 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.67055E-04 0.09992  6.53248E-03 0.00630 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10880E-03 0.00330 -1.69784E-02 0.00232 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.60345E-04 0.02248  4.42611E-04 0.06371 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12445E-01 0.00031  1.00811E+00 0.00040 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56903E+00 0.00031  3.30652E-01 0.00040 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.40263E-03 0.00051  6.58487E-02 0.00117 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69766E-02 0.00023  6.70707E-02 0.00118 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11425E-01 0.00012  1.75139E-02 0.00046  1.16348E-03 0.00397  1.48807E+00 0.00055 ];
INF_S1                    (idx, [1:   8]) = [  2.39039E-01 0.00025  5.08441E-03 0.00114  5.01774E-04 0.00660  3.94435E-01 0.00061 ];
INF_S2                    (idx, [1:   8]) = [  9.78493E-02 0.00030 -1.56793E-03 0.00227  2.74583E-04 0.00844  9.30385E-02 0.00109 ];
INF_S3                    (idx, [1:   8]) = [  9.12924E-03 0.00234 -1.81000E-03 0.00181  1.00237E-04 0.01793  2.79306E-02 0.00247 ];
INF_S4                    (idx, [1:   8]) = [ -9.74029E-03 0.00184 -5.69725E-04 0.00526  4.01204E-07 1.00000 -8.81574E-03 0.00527 ];
INF_S5                    (idx, [1:   8]) = [  1.31202E-04 0.12939  3.56595E-05 0.06678 -4.04071E-05 0.03548  6.57289E-03 0.00621 ];
INF_S6                    (idx, [1:   8]) = [  5.24831E-03 0.00327 -1.39773E-04 0.01076 -4.95754E-05 0.03910 -1.69288E-02 0.00231 ];
INF_S7                    (idx, [1:   8]) = [  9.31331E-04 0.01825 -1.70911E-04 0.01212 -4.53604E-05 0.02695  4.87972E-04 0.05825 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11467E-01 0.00012  1.75139E-02 0.00046  1.16348E-03 0.00397  1.48807E+00 0.00055 ];
INF_SP1                   (idx, [1:   8]) = [  2.39040E-01 0.00025  5.08441E-03 0.00114  5.01774E-04 0.00660  3.94435E-01 0.00061 ];
INF_SP2                   (idx, [1:   8]) = [  9.78494E-02 0.00030 -1.56793E-03 0.00227  2.74583E-04 0.00844  9.30385E-02 0.00109 ];
INF_SP3                   (idx, [1:   8]) = [  9.12943E-03 0.00233 -1.81000E-03 0.00181  1.00237E-04 0.01793  2.79306E-02 0.00247 ];
INF_SP4                   (idx, [1:   8]) = [ -9.74032E-03 0.00183 -5.69725E-04 0.00526  4.01204E-07 1.00000 -8.81574E-03 0.00527 ];
INF_SP5                   (idx, [1:   8]) = [  1.31395E-04 0.12886  3.56595E-05 0.06678 -4.04071E-05 0.03548  6.57289E-03 0.00621 ];
INF_SP6                   (idx, [1:   8]) = [  5.24857E-03 0.00326 -1.39773E-04 0.01076 -4.95754E-05 0.03910 -1.69288E-02 0.00231 ];
INF_SP7                   (idx, [1:   8]) = [  9.31256E-04 0.01823 -1.70911E-04 0.01212 -4.53604E-05 0.02695  4.87972E-04 0.05825 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32189E-01 0.00075  1.20199E+00 0.00767 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33711E-01 0.00092  1.30982E+00 0.00953 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33974E-01 0.00111  1.30650E+00 0.00922 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28958E-01 0.00088  1.03528E+00 0.00776 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43563E+00 0.00075  2.77702E-01 0.00752 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42629E+00 0.00093  2.55036E-01 0.00941 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42470E+00 0.00111  2.55639E-01 0.00891 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45590E+00 0.00088  3.22431E-01 0.00763 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.62692E-03 0.00944  1.44703E-04 0.05390  9.69663E-04 0.02229  8.84313E-04 0.02310  2.46904E-03 0.01441  8.73969E-04 0.02480  2.85236E-04 0.04265 ];
LAMBDA                    (idx, [1:  14]) = [  8.17663E-01 0.02290  1.25321E-02 0.00077  3.10865E-02 0.00063  1.10311E-01 0.00061  3.22182E-01 0.00043  1.31568E+00 0.00204  8.81447E+00 0.00665 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0006' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:46 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:09:29 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284846 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.89361E-01  1.00268E+00  1.00246E+00  1.00250E+00  1.00300E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15191E-02 0.00116  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88481E-01 1.4E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06013E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06441E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67490E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.50889E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.50799E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.41898E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.18817E-01 0.00117  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000619 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00012E+04 0.00088 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00012E+04 0.00088 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.41447E+02 ;
RUNNING_TIME              (idx, 1)        =  8.87286E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.32900E-01  4.32900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.54983E-01  9.66667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  8.72452E+01  3.34183E+00  2.59612E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  7.86600E-01  2.66167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.65166E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  8.87284E+01  1.24777E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97525 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99935E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80547E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.48010E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.73631E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.12903E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.86535E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.30312E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61473E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.60596E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.92354E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.62920E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.20444E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.99757E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.19108E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.82944E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.57135E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.35018E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.46271E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.50796E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.72166E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.01911E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.26393E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.33038E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.50761E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.27668E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.84095E+12 0.00053  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 14 ;
BURNUP                     (idx, [1:  2])  = [  1.50000E+01  1.50014E+01 ];
BURN_DAYS                 (idx, 1)        =  3.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.38266E-01 0.00111 ];
U235_FISS                 (idx, [1:   4]) = [  6.92546E+15 0.00095  5.05377E-01 0.00075 ];
U238_FISS                 (idx, [1:   4]) = [  1.06602E+15 0.00268  7.77819E-02 0.00251 ];
PU239_FISS                (idx, [1:   4]) = [  4.97686E+15 0.00119  3.63177E-01 0.00101 ];
PU240_FISS                (idx, [1:   4]) = [  7.13768E+12 0.03304  5.20933E-04 0.03300 ];
PU241_FISS                (idx, [1:   4]) = [  7.14588E+14 0.00343  5.21416E-02 0.00332 ];
U235_CAPT                 (idx, [1:   4]) = [  1.54673E+15 0.00231  6.33469E-02 0.00226 ];
U238_CAPT                 (idx, [1:   4]) = [  9.24426E+15 0.00109  3.78572E-01 0.00078 ];
PU239_CAPT                (idx, [1:   4]) = [  2.74475E+15 0.00163  1.12421E-01 0.00164 ];
PU240_CAPT                (idx, [1:   4]) = [  1.65363E+15 0.00215  6.77188E-02 0.00200 ];
PU241_CAPT                (idx, [1:   4]) = [  2.58443E+14 0.00536  1.05856E-02 0.00537 ];
XE135_CAPT                (idx, [1:   4]) = [  7.54610E+14 0.00307  3.09075E-02 0.00308 ];
SM149_CAPT                (idx, [1:   4]) = [  2.15253E+14 0.00592  8.81807E-03 0.00599 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000619 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.85926E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000619 5.00786E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3173849 3.17859E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1781518 1.78401E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45252 4.52603E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000619 5.00786E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -2.51457E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.62858E+16 2.2E-05  3.62858E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36933E+16 4.4E-06  1.36933E+16 4.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.44281E+16 0.00049  1.84550E+16 0.00048  5.97310E+15 0.00126 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.81214E+16 0.00031  3.21483E+16 0.00027  5.97310E+15 0.00126 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.84095E+16 0.00053  3.84095E+16 0.00053  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.80403E+18 0.00052  4.79301E+17 0.00051  1.32473E+18 0.00058 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.47721E+14 0.00487 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.84691E+16 0.00032 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.34935E+18 0.00069 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10771E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10771E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.54034E+00 0.00055 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.80967E-01 0.00035 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.40741E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23809E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94124E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96806E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.54225E-01 0.00060 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.45589E-01 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.64991E+00 2.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05140E+02 4.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.45674E-01 0.00061  9.40406E-01 0.00061  5.18295E-03 0.01022 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.44797E-01 0.00032 ];
COL_KEFF                  (idx, [1:   2]) = [  9.44843E-01 0.00053 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.44797E-01 0.00032 ];
ABS_KINF                  (idx, [1:   2]) = [  9.53426E-01 0.00031 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71358E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71370E+01 0.00010 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.25347E-07 0.00373 ];
IMP_EALF                  (idx, [1:   2]) = [  7.22517E-07 0.00172 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.65003E-01 0.00286 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.64660E-01 0.00122 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.83956E-03 0.00609  1.43504E-04 0.03877  1.00615E-03 0.01482  9.00599E-04 0.01517  2.60381E-03 0.00870  9.18022E-04 0.01518  2.67467E-04 0.02813 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.65117E-01 0.01463  9.27786E-03 0.02655  3.09898E-02 0.00043  1.10483E-01 0.00046  3.22376E-01 0.00030  1.31042E+00 0.00145  7.85899E+00 0.01567 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.46189E-03 0.00893  1.36556E-04 0.05827  9.48496E-04 0.02210  8.55614E-04 0.02323  2.42036E-03 0.01319  8.59540E-04 0.02144  2.41322E-04 0.04153 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.53085E-01 0.02038  1.25362E-02 0.00084  3.09853E-02 0.00063  1.10395E-01 0.00063  3.22424E-01 0.00045  1.31137E+00 0.00195  8.62709E+00 0.00891 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.69462E-05 0.00143  2.69358E-05 0.00143  2.88143E-05 0.01608 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.54773E-05 0.00129  2.54675E-05 0.00129  2.72487E-05 0.01608 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.47598E-03 0.01018  1.26818E-04 0.07057  9.70849E-04 0.02392  8.66642E-04 0.02634  2.39402E-03 0.01496  8.70083E-04 0.02396  2.47565E-04 0.04775 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.69319E-01 0.02575  1.25329E-02 0.00123  3.09760E-02 0.00083  1.10356E-01 0.00086  3.22370E-01 0.00053  1.31258E+00 0.00233  8.56436E+00 0.01244 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.65082E-05 0.00352  2.65049E-05 0.00353  2.54000E-05 0.03928 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.50617E-05 0.00342  2.50585E-05 0.00343  2.40310E-05 0.03931 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.40487E-03 0.03529  1.12628E-04 0.21324  8.80433E-04 0.08559  8.96022E-04 0.08429  2.30919E-03 0.05314  9.47567E-04 0.08485  2.59021E-04 0.16312 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.93182E-01 0.07974  1.25788E-02 0.00402  3.09974E-02 0.00199  1.10229E-01 0.00189  3.21944E-01 0.00151  1.31204E+00 0.00591  8.23376E+00 0.03378 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.38435E-03 0.03456  1.12375E-04 0.20857  8.76135E-04 0.08259  8.89183E-04 0.08294  2.30934E-03 0.05146  9.41121E-04 0.08307  2.56194E-04 0.15889 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.88766E-01 0.07866  1.25788E-02 0.00402  3.09978E-02 0.00198  1.10210E-01 0.00187  3.21911E-01 0.00150  1.31252E+00 0.00576  8.24241E+00 0.03360 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.04744E+02 0.03523 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.67944E-05 0.00090 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.53336E-05 0.00060 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.33807E-03 0.00682 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.99278E+02 0.00684 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.74782E-07 0.00079 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.71136E-06 0.00044  2.71119E-06 0.00044  2.74012E-06 0.00579 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.90016E-05 0.00098  3.90229E-05 0.00098  3.52611E-05 0.01017 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.38562E-01 0.00034  6.38676E-01 0.00035  6.30732E-01 0.00951 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04204E+01 0.01497 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.50799E+01 0.00051  3.35008E+01 0.00055 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.82768E+04 0.00278  3.02371E+05 0.00125  6.10465E+05 0.00063  6.50586E+05 0.00061  5.99241E+05 0.00075  6.41355E+05 0.00058  4.35513E+05 0.00067  3.85292E+05 0.00063  2.94641E+05 0.00066  2.41039E+05 0.00065  2.07885E+05 0.00099  1.87454E+05 0.00088  1.72897E+05 0.00078  1.64350E+05 0.00076  1.59975E+05 0.00073  1.38499E+05 0.00088  1.36834E+05 0.00096  1.35413E+05 0.00105  1.33127E+05 0.00096  2.59840E+05 0.00095  2.51062E+05 0.00061  1.81204E+05 0.00086  1.17379E+05 0.00107  1.35716E+05 0.00075  1.29299E+05 0.00085  1.15080E+05 0.00084  1.87076E+05 0.00079  4.32508E+04 0.00167  5.40625E+04 0.00151  4.88470E+04 0.00111  2.86080E+04 0.00204  4.94126E+04 0.00131  3.30579E+04 0.00167  2.73537E+04 0.00184  4.80105E+03 0.00364  4.21907E+03 0.00316  3.71952E+03 0.00463  3.53783E+03 0.00423  3.63070E+03 0.00357  4.01563E+03 0.00434  4.69758E+03 0.00358  4.69776E+03 0.00404  9.18367E+03 0.00322  1.50284E+04 0.00262  1.94339E+04 0.00189  5.20103E+04 0.00144  5.48443E+04 0.00139  5.86501E+04 0.00193  3.81598E+04 0.00186  2.69930E+04 0.00120  2.00575E+04 0.00181  2.36519E+04 0.00198  4.63086E+04 0.00124  6.55036E+04 0.00122  1.30107E+05 0.00116  2.05488E+05 0.00131  3.11363E+05 0.00132  2.00550E+05 0.00141  1.44265E+05 0.00154  1.03966E+05 0.00151  9.36496E+04 0.00162  9.23988E+04 0.00154  7.71965E+04 0.00182  5.23501E+04 0.00179  4.83622E+04 0.00160  4.30764E+04 0.00177  3.64744E+04 0.00180  2.88154E+04 0.00198  1.93015E+04 0.00185  6.84585E+03 0.00229 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.53474E-01 0.00044 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.43372E+18 0.00044  3.70341E+17 0.00132 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38629E-01 8.6E-05  1.55835E+00 0.00041 ];
INF_CAPT                  (idx, [1:   4]) = [  7.75536E-03 0.00057  3.59414E-02 0.00070 ];
INF_ABS                   (idx, [1:   4]) = [  9.57704E-03 0.00050  6.58701E-02 0.00098 ];
INF_FISS                  (idx, [1:   4]) = [  1.82167E-03 0.00048  2.99287E-02 0.00133 ];
INF_NSF                   (idx, [1:   4]) = [  4.86532E-03 0.00047  7.91610E-02 0.00136 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.67080E+00 5.9E-05  2.64498E+00 5.6E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05270E+02 8.1E-06  2.05110E+02 1.0E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.63881E-08 0.00040  2.58182E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29050E-01 9.1E-05  1.49250E+00 0.00047 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44105E-01 0.00017  3.95716E-01 0.00046 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63245E-02 0.00028  9.36028E-02 0.00102 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32475E-03 0.00295  2.81697E-02 0.00235 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03292E-02 0.00144 -8.78652E-03 0.00598 ];
INF_SCATT5                (idx, [1:   4]) = [  1.92103E-04 0.07333  6.62175E-03 0.00632 ];
INF_SCATT6                (idx, [1:   4]) = [  5.12268E-03 0.00274 -1.71191E-02 0.00299 ];
INF_SCATT7                (idx, [1:   4]) = [  7.66534E-04 0.01913  3.90798E-04 0.12288 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29092E-01 9.1E-05  1.49250E+00 0.00047 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44105E-01 0.00017  3.95716E-01 0.00046 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63250E-02 0.00028  9.36028E-02 0.00102 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32499E-03 0.00294  2.81697E-02 0.00235 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03291E-02 0.00144 -8.78652E-03 0.00598 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.92141E-04 0.07309  6.62175E-03 0.00632 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.12266E-03 0.00273 -1.71191E-02 0.00299 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.66735E-04 0.01916  3.90798E-04 0.12288 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12516E-01 0.00024  1.01081E+00 0.00037 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56851E+00 0.00024  3.29769E-01 0.00037 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.53492E-03 0.00051  6.58701E-02 0.00098 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69745E-02 0.00022  6.70148E-02 0.00120 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11655E-01 8.4E-05  1.73949E-02 0.00039  1.16035E-03 0.00422  1.49134E+00 0.00047 ];
INF_S1                    (idx, [1:   8]) = [  2.39069E-01 0.00016  5.03612E-03 0.00102  4.91571E-04 0.00664  3.95225E-01 0.00046 ];
INF_S2                    (idx, [1:   8]) = [  9.78914E-02 0.00027 -1.56691E-03 0.00229  2.68821E-04 0.01054  9.33340E-02 0.00101 ];
INF_S3                    (idx, [1:   8]) = [  9.12077E-03 0.00238 -1.79602E-03 0.00160  9.49028E-05 0.02496  2.80748E-02 0.00233 ];
INF_S4                    (idx, [1:   8]) = [ -9.76275E-03 0.00162 -5.66415E-04 0.00490  8.10292E-08 1.00000 -8.78660E-03 0.00597 ];
INF_S5                    (idx, [1:   8]) = [  1.56755E-04 0.09253  3.53481E-05 0.07060 -3.86256E-05 0.04753  6.66037E-03 0.00615 ];
INF_S6                    (idx, [1:   8]) = [  5.25925E-03 0.00259 -1.36564E-04 0.01939 -4.92665E-05 0.03005 -1.70698E-02 0.00300 ];
INF_S7                    (idx, [1:   8]) = [  9.38587E-04 0.01535 -1.72053E-04 0.01256 -4.34303E-05 0.02662  4.34229E-04 0.11048 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11697E-01 8.5E-05  1.73949E-02 0.00039  1.16035E-03 0.00422  1.49134E+00 0.00047 ];
INF_SP1                   (idx, [1:   8]) = [  2.39069E-01 0.00016  5.03612E-03 0.00102  4.91571E-04 0.00664  3.95225E-01 0.00046 ];
INF_SP2                   (idx, [1:   8]) = [  9.78919E-02 0.00027 -1.56691E-03 0.00229  2.68821E-04 0.01054  9.33340E-02 0.00101 ];
INF_SP3                   (idx, [1:   8]) = [  9.12101E-03 0.00237 -1.79602E-03 0.00160  9.49028E-05 0.02496  2.80748E-02 0.00233 ];
INF_SP4                   (idx, [1:   8]) = [ -9.76268E-03 0.00162 -5.66415E-04 0.00490  8.10292E-08 1.00000 -8.78660E-03 0.00597 ];
INF_SP5                   (idx, [1:   8]) = [  1.56793E-04 0.09226  3.53481E-05 0.07060 -3.86256E-05 0.04753  6.66037E-03 0.00615 ];
INF_SP6                   (idx, [1:   8]) = [  5.25922E-03 0.00258 -1.36564E-04 0.01939 -4.92665E-05 0.03005 -1.70698E-02 0.00300 ];
INF_SP7                   (idx, [1:   8]) = [  9.38788E-04 0.01538 -1.72053E-04 0.01256 -4.34303E-05 0.02662  4.34229E-04 0.11048 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31929E-01 0.00062  1.19284E+00 0.00537 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33567E-01 0.00082  1.30149E+00 0.00636 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33703E-01 0.00075  1.28971E+00 0.00617 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28599E-01 0.00115  1.03039E+00 0.00676 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43723E+00 0.00062  2.79636E-01 0.00531 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42717E+00 0.00082  2.56364E-01 0.00630 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42633E+00 0.00075  2.58696E-01 0.00624 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45820E+00 0.00115  3.23848E-01 0.00662 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.46189E-03 0.00893  1.36556E-04 0.05827  9.48496E-04 0.02210  8.55614E-04 0.02323  2.42036E-03 0.01319  8.59540E-04 0.02144  2.41322E-04 0.04153 ];
LAMBDA                    (idx, [1:  14]) = [  7.53085E-01 0.02038  1.25362E-02 0.00084  3.09853E-02 0.00063  1.10395E-01 0.00063  3.22424E-01 0.00045  1.31137E+00 0.00195  8.62709E+00 0.00891 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0006' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:46 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:15:31 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284846 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.90081E-01  1.00147E+00  1.00332E+00  1.00182E+00  1.00332E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15350E-02 0.00107  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88465E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06064E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06493E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67631E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.50106E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.50016E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.39743E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.18454E-01 0.00108  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000894 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00018E+04 0.00087 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00018E+04 0.00087 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.71507E+02 ;
RUNNING_TIME              (idx, 1)        =  9.47472E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.32900E-01  4.32900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.76700E-01  1.04333E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.31850E+01  3.34017E+00  2.59963E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  8.43150E-01  2.79500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.90833E-02  8.50002E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.47470E+01  1.24838E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97647 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99942E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80847E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.53124E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.72705E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.70304E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.91386E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.33803E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61736E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.59321E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.31051E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.70935E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.55517E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.27227E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.55340E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.88212E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.78678E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.37212E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.49397E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.53889E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.30197E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.35312E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.31051E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.30522E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.98061E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.29085E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.95229E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 15 ;
BURNUP                     (idx, [1:  2])  = [  1.75000E+01  1.75016E+01 ];
BURN_DAYS                 (idx, 1)        =  4.37500E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.62522E-01 0.00109 ];
U235_FISS                 (idx, [1:   4]) = [  6.33974E+15 0.00109  4.63273E-01 0.00088 ];
U238_FISS                 (idx, [1:   4]) = [  1.09373E+15 0.00289  7.99112E-02 0.00270 ];
PU239_FISS                (idx, [1:   4]) = [  5.32981E+15 0.00117  3.89475E-01 0.00099 ];
PU240_FISS                (idx, [1:   4]) = [  8.43062E+12 0.03067  6.15713E-04 0.03061 ];
PU241_FISS                (idx, [1:   4]) = [  8.96962E+14 0.00293  6.55437E-02 0.00284 ];
U235_CAPT                 (idx, [1:   4]) = [  1.42152E+15 0.00232  5.56571E-02 0.00226 ];
U238_CAPT                 (idx, [1:   4]) = [  9.47591E+15 0.00105  3.70986E-01 0.00077 ];
PU239_CAPT                (idx, [1:   4]) = [  2.93705E+15 0.00165  1.14994E-01 0.00157 ];
PU240_CAPT                (idx, [1:   4]) = [  1.88908E+15 0.00210  7.39572E-02 0.00195 ];
PU241_CAPT                (idx, [1:   4]) = [  3.20609E+14 0.00473  1.25520E-02 0.00467 ];
XE135_CAPT                (idx, [1:   4]) = [  7.67816E+14 0.00306  3.00651E-02 0.00307 ];
SM149_CAPT                (idx, [1:   4]) = [  2.25670E+14 0.00603  8.83502E-03 0.00599 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000894 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.87384E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000894 5.00787E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3226646 3.23123E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1728940 1.73133E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45308 4.53152E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000894 5.00787E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -9.31323E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.64994E+16 2.4E-05  3.64994E+16 2.4E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36747E+16 5.0E-06  1.36747E+16 5.0E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.55162E+16 0.00046  1.93961E+16 0.00049  6.12018E+15 0.00122 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.91909E+16 0.00030  3.30707E+16 0.00028  6.12018E+15 0.00122 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.95229E+16 0.00050  3.95229E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.85299E+18 0.00048  4.91946E+17 0.00051  1.36105E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.58235E+14 0.00498 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.95491E+16 0.00030 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.38535E+18 0.00065 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10481E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10481E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.51883E+00 0.00058 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.79819E-01 0.00036 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.36570E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23719E+00 0.00038 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94097E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96821E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.32715E-01 0.00062 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.24262E-01 0.00063 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.66913E+00 2.9E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05419E+02 5.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.24383E-01 0.00064  9.19389E-01 0.00063  4.87263E-03 0.01102 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.24408E-01 0.00031 ];
COL_KEFF                  (idx, [1:   2]) = [  9.23618E-01 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.24408E-01 0.00031 ];
ABS_KINF                  (idx, [1:   2]) = [  9.32869E-01 0.00030 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70964E+01 0.00024 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70970E+01 9.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.54957E-07 0.00405 ];
IMP_EALF                  (idx, [1:   2]) = [  7.51991E-07 0.00170 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.71910E-01 0.00275 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.72018E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.88484E-03 0.00644  1.56966E-04 0.03742  1.04620E-03 0.01457  9.08321E-04 0.01540  2.55945E-03 0.00938  9.28657E-04 0.01586  2.85242E-04 0.02681 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.85408E-01 0.01395  9.77393E-03 0.02393  3.08900E-02 0.00044  1.10476E-01 0.00207  3.22407E-01 0.00032  1.29645E+00 0.00179  8.05582E+00 0.01430 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.28427E-03 0.00900  1.32962E-04 0.05719  9.43084E-04 0.02262  8.09841E-04 0.02352  2.30890E-03 0.01410  8.41692E-04 0.02276  2.47793E-04 0.04232 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.73422E-01 0.02166  1.25663E-02 0.00103  3.08785E-02 0.00066  1.10742E-01 0.00075  3.22389E-01 0.00050  1.29600E+00 0.00245  8.67548E+00 0.00863 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.73887E-05 0.00139  2.73809E-05 0.00139  2.88559E-05 0.01762 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.53127E-05 0.00124  2.53054E-05 0.00124  2.66741E-05 0.01762 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.27590E-03 0.01113  1.40951E-04 0.06423  9.46501E-04 0.02433  8.21763E-04 0.02689  2.26861E-03 0.01639  8.39657E-04 0.02897  2.58420E-04 0.04647 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.95375E-01 0.02513  1.25757E-02 0.00160  3.09039E-02 0.00081  1.10817E-01 0.00097  3.22309E-01 0.00058  1.29334E+00 0.00355  8.74027E+00 0.01059 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.69612E-05 0.00345  2.69569E-05 0.00345  2.67486E-05 0.04536 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.49165E-05 0.00337  2.49125E-05 0.00338  2.47227E-05 0.04535 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.92889E-03 0.03750  1.23904E-04 0.22278  8.49755E-04 0.08722  7.20652E-04 0.08903  2.16946E-03 0.05484  8.51856E-04 0.09042  2.13255E-04 0.16382 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.75682E-01 0.07997  1.27112E-02 0.00649  3.08169E-02 0.00203  1.10653E-01 0.00236  3.22032E-01 0.00164  1.30735E+00 0.00657  8.90371E+00 0.02745 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.88853E-03 0.03650  1.24341E-04 0.22317  8.42828E-04 0.08269  7.26149E-04 0.08840  2.16319E-03 0.05373  8.19816E-04 0.08922  2.12204E-04 0.16626 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.73753E-01 0.07998  1.27112E-02 0.00649  3.08178E-02 0.00202  1.10636E-01 0.00236  3.22128E-01 0.00164  1.30686E+00 0.00664  8.90699E+00 0.02743 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.84685E+02 0.03814 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.71510E-05 0.00090 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.50928E-05 0.00064 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.07687E-03 0.00635 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.87088E+02 0.00645 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.72558E-07 0.00081 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.69272E-06 0.00042  2.69263E-06 0.00042  2.71179E-06 0.00597 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.90405E-05 0.00096  3.90585E-05 0.00096  3.57683E-05 0.01122 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.34408E-01 0.00035  6.34627E-01 0.00035  6.10958E-01 0.01058 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07246E+01 0.01392 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.50016E+01 0.00052  3.33981E+01 0.00053 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.89705E+04 0.00275  3.03035E+05 0.00137  6.10220E+05 0.00077  6.50307E+05 0.00061  5.98243E+05 0.00058  6.40770E+05 0.00070  4.35614E+05 0.00055  3.85285E+05 0.00056  2.95068E+05 0.00074  2.41007E+05 0.00053  2.07919E+05 0.00080  1.87291E+05 0.00078  1.73091E+05 0.00087  1.64540E+05 0.00100  1.60035E+05 0.00074  1.38498E+05 0.00092  1.36686E+05 0.00091  1.35398E+05 0.00094  1.32949E+05 0.00079  2.60040E+05 0.00066  2.51228E+05 0.00082  1.81190E+05 0.00086  1.17636E+05 0.00086  1.35459E+05 0.00079  1.28853E+05 0.00119  1.15135E+05 0.00095  1.86647E+05 0.00059  4.30753E+04 0.00167  5.39474E+04 0.00179  4.88041E+04 0.00125  2.85458E+04 0.00161  4.93103E+04 0.00138  3.30032E+04 0.00170  2.71161E+04 0.00199  4.69862E+03 0.00339  4.04846E+03 0.00275  3.55438E+03 0.00342  3.43668E+03 0.00354  3.47910E+03 0.00354  3.82538E+03 0.00253  4.51605E+03 0.00313  4.62266E+03 0.00459  9.02160E+03 0.00297  1.48644E+04 0.00254  1.92534E+04 0.00201  5.15155E+04 0.00119  5.43608E+04 0.00142  5.79795E+04 0.00158  3.77911E+04 0.00186  2.66607E+04 0.00169  1.97956E+04 0.00188  2.33943E+04 0.00159  4.57509E+04 0.00161  6.45772E+04 0.00166  1.28797E+05 0.00152  2.03740E+05 0.00194  3.09349E+05 0.00197  1.99449E+05 0.00201  1.43586E+05 0.00217  1.03497E+05 0.00224  9.31395E+04 0.00214  9.19701E+04 0.00217  7.70254E+04 0.00208  5.21623E+04 0.00230  4.81905E+04 0.00226  4.29046E+04 0.00219  3.62864E+04 0.00207  2.87380E+04 0.00197  1.92616E+04 0.00183  6.83566E+03 0.00296 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.32066E-01 0.00048 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.47455E+18 0.00052  3.78478E+17 0.00173 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38850E-01 0.00011  1.56103E+00 0.00051 ];
INF_CAPT                  (idx, [1:   4]) = [  7.92175E-03 0.00074  3.65596E-02 0.00103 ];
INF_ABS                   (idx, [1:   4]) = [  9.68944E-03 0.00059  6.58098E-02 0.00133 ];
INF_FISS                  (idx, [1:   4]) = [  1.76769E-03 0.00045  2.92502E-02 0.00172 ];
INF_NSF                   (idx, [1:   4]) = [  4.74648E-03 0.00045  7.79625E-02 0.00178 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.68514E+00 7.5E-05  2.66536E+00 8.2E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05472E+02 1.1E-05  2.05407E+02 1.5E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.61145E-08 0.00036  2.58478E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29155E-01 0.00011  1.49515E+00 0.00059 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44087E-01 0.00024  3.96276E-01 0.00076 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63365E-02 0.00030  9.37528E-02 0.00115 ];
INF_SCATT3                (idx, [1:   4]) = [  7.30079E-03 0.00327  2.81670E-02 0.00230 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03397E-02 0.00198 -8.89379E-03 0.00625 ];
INF_SCATT5                (idx, [1:   4]) = [  1.64245E-04 0.11280  6.67966E-03 0.00776 ];
INF_SCATT6                (idx, [1:   4]) = [  5.13043E-03 0.00324 -1.70796E-02 0.00318 ];
INF_SCATT7                (idx, [1:   4]) = [  7.68303E-04 0.01727  4.31815E-04 0.11263 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29197E-01 0.00011  1.49515E+00 0.00059 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44088E-01 0.00024  3.96276E-01 0.00076 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63367E-02 0.00030  9.37528E-02 0.00115 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.30089E-03 0.00327  2.81670E-02 0.00230 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03393E-02 0.00198 -8.89379E-03 0.00625 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.64143E-04 0.11300  6.67966E-03 0.00776 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.13040E-03 0.00324 -1.70796E-02 0.00318 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.68302E-04 0.01727  4.31815E-04 0.11263 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12575E-01 0.00022  1.01321E+00 0.00043 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56808E+00 0.00022  3.28990E-01 0.00043 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.64723E-03 0.00061  6.58098E-02 0.00133 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69858E-02 0.00021  6.70383E-02 0.00142 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11864E-01 0.00011  1.72908E-02 0.00054  1.16299E-03 0.00458  1.49399E+00 0.00060 ];
INF_S1                    (idx, [1:   8]) = [  2.39088E-01 0.00023  4.99985E-03 0.00095  5.05843E-04 0.00820  3.95771E-01 0.00076 ];
INF_S2                    (idx, [1:   8]) = [  9.79009E-02 0.00028 -1.56442E-03 0.00267  2.74570E-04 0.01021  9.34782E-02 0.00116 ];
INF_S3                    (idx, [1:   8]) = [  9.08761E-03 0.00249 -1.78682E-03 0.00187  9.78660E-05 0.01996  2.80691E-02 0.00232 ];
INF_S4                    (idx, [1:   8]) = [ -9.77940E-03 0.00208 -5.60276E-04 0.00419  2.49077E-06 0.66932 -8.89628E-03 0.00629 ];
INF_S5                    (idx, [1:   8]) = [  1.27681E-04 0.13819  3.65640E-05 0.06140 -4.00044E-05 0.03377  6.71967E-03 0.00773 ];
INF_S6                    (idx, [1:   8]) = [  5.26767E-03 0.00314 -1.37234E-04 0.01762 -5.04204E-05 0.02615 -1.70291E-02 0.00319 ];
INF_S7                    (idx, [1:   8]) = [  9.38484E-04 0.01385 -1.70181E-04 0.01504 -4.32511E-05 0.03175  4.75066E-04 0.10250 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11907E-01 0.00011  1.72908E-02 0.00054  1.16299E-03 0.00458  1.49399E+00 0.00060 ];
INF_SP1                   (idx, [1:   8]) = [  2.39088E-01 0.00023  4.99985E-03 0.00095  5.05843E-04 0.00820  3.95771E-01 0.00076 ];
INF_SP2                   (idx, [1:   8]) = [  9.79012E-02 0.00029 -1.56442E-03 0.00267  2.74570E-04 0.01021  9.34782E-02 0.00116 ];
INF_SP3                   (idx, [1:   8]) = [  9.08772E-03 0.00249 -1.78682E-03 0.00187  9.78660E-05 0.01996  2.80691E-02 0.00232 ];
INF_SP4                   (idx, [1:   8]) = [ -9.77902E-03 0.00207 -5.60276E-04 0.00419  2.49077E-06 0.66932 -8.89628E-03 0.00629 ];
INF_SP5                   (idx, [1:   8]) = [  1.27579E-04 0.13846  3.65640E-05 0.06140 -4.00044E-05 0.03377  6.71967E-03 0.00773 ];
INF_SP6                   (idx, [1:   8]) = [  5.26763E-03 0.00314 -1.37234E-04 0.01762 -5.04204E-05 0.02615 -1.70291E-02 0.00319 ];
INF_SP7                   (idx, [1:   8]) = [  9.38483E-04 0.01385 -1.70181E-04 0.01504 -4.32511E-05 0.03175  4.75066E-04 0.10250 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31999E-01 0.00050  1.21594E+00 0.00807 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33822E-01 0.00105  1.33098E+00 0.01009 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33779E-01 0.00091  1.31838E+00 0.01091 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28487E-01 0.00066  1.04564E+00 0.00626 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43679E+00 0.00050  2.74562E-01 0.00800 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42562E+00 0.00105  2.51040E-01 0.00987 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42588E+00 0.00091  2.53563E-01 0.01096 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45889E+00 0.00066  3.19083E-01 0.00624 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.28427E-03 0.00900  1.32962E-04 0.05719  9.43084E-04 0.02262  8.09841E-04 0.02352  2.30890E-03 0.01410  8.41692E-04 0.02276  2.47793E-04 0.04232 ];
LAMBDA                    (idx, [1:  14]) = [  7.73422E-01 0.02166  1.25663E-02 0.00103  3.08785E-02 0.00066  1.10742E-01 0.00075  3.22389E-01 0.00050  1.29600E+00 0.00245  8.67548E+00 0.00863 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0006' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:46 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:21:32 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284846 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.90660E-01  1.00163E+00  1.00322E+00  1.00163E+00  1.00286E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.3E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15426E-02 0.00114  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88457E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06525E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06954E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67808E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.50682E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.50590E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.38637E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.19234E-01 0.00117  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000976 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00020E+04 0.00089 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00020E+04 0.00089 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.01558E+02 ;
RUNNING_TIME              (idx, 1)        =  1.00764E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.32900E-01  4.32900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.97467E-01  1.00500E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.91251E+01  3.33807E+00  2.60212E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  8.98817E-01  2.71667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  5.25833E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.00764E+02  1.24831E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97755 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00042E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81112E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.58539E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.72005E+04 ;
TOT_SF_RATE               (idx, 1)        =  5.97112E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.96368E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.37424E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62168E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.58259E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.74342E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.78816E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.95529E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.56054E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.88136E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.93211E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.99238E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.39245E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.52199E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.56733E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.95010E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.68615E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.36002E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.28293E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.86264E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.30528E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.05851E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 16 ;
BURNUP                     (idx, [1:  2])  = [  2.00000E+01  2.00019E+01 ];
BURN_DAYS                 (idx, 1)        =  5.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.85287E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  5.81717E+15 0.00119  4.25430E-01 0.00096 ];
U238_FISS                 (idx, [1:   4]) = [  1.12461E+15 0.00271  8.22396E-02 0.00256 ];
PU239_FISS                (idx, [1:   4]) = [  5.62844E+15 0.00107  4.11648E-01 0.00092 ];
PU240_FISS                (idx, [1:   4]) = [  1.06602E+13 0.02851  7.79188E-04 0.02846 ];
PU241_FISS                (idx, [1:   4]) = [  1.07372E+15 0.00269  7.85266E-02 0.00262 ];
U235_CAPT                 (idx, [1:   4]) = [  1.30464E+15 0.00243  4.90413E-02 0.00238 ];
U238_CAPT                 (idx, [1:   4]) = [  9.67992E+15 0.00107  3.63838E-01 0.00076 ];
PU239_CAPT                (idx, [1:   4]) = [  3.09815E+15 0.00160  1.16462E-01 0.00156 ];
PU240_CAPT                (idx, [1:   4]) = [  2.11546E+15 0.00200  7.95139E-02 0.00185 ];
PU241_CAPT                (idx, [1:   4]) = [  3.85300E+14 0.00464  1.44830E-02 0.00460 ];
XE135_CAPT                (idx, [1:   4]) = [  7.72133E+14 0.00332  2.90276E-02 0.00334 ];
SM149_CAPT                (idx, [1:   4]) = [  2.31641E+14 0.00569  8.70687E-03 0.00565 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000976 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.99495E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000976 5.00799E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3272828 3.27753E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1682313 1.68463E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45835 4.58410E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000976 5.00799E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.21541E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.66902E+16 2.4E-05  3.66902E+16 2.4E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36579E+16 5.1E-06  1.36579E+16 5.1E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.66048E+16 0.00045  2.02905E+16 0.00045  6.31423E+15 0.00124 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.02626E+16 0.00030  3.39484E+16 0.00027  6.31423E+15 0.00124 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.05851E+16 0.00051  4.05851E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.90266E+18 0.00050  5.03884E+17 0.00049  1.39878E+18 0.00056 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.72158E+14 0.00524 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.06348E+16 0.00031 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.42495E+18 0.00067 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10192E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10192E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.49919E+00 0.00058 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.77961E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.33146E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23723E+00 0.00039 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94018E-01 3.8E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96794E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.13544E-01 0.00062 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.05169E-01 0.00063 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.68638E+00 2.9E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05672E+02 5.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.05075E-01 0.00063  9.00469E-01 0.00063  4.69975E-03 0.01066 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.04421E-01 0.00031 ];
COL_KEFF                  (idx, [1:   2]) = [  9.04148E-01 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.04421E-01 0.00031 ];
ABS_KINF                  (idx, [1:   2]) = [  9.12791E-01 0.00030 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70577E+01 0.00024 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70637E+01 9.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.84913E-07 0.00413 ];
IMP_EALF                  (idx, [1:   2]) = [  7.77477E-07 0.00169 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.80269E-01 0.00276 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.79335E-01 0.00111 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.85552E-03 0.00649  1.44008E-04 0.04021  1.03171E-03 0.01516  9.40679E-04 0.01575  2.52855E-03 0.00882  9.33266E-04 0.01583  2.77303E-04 0.02798 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.65777E-01 0.01468  9.06659E-03 0.02779  3.08267E-02 0.00046  1.10849E-01 0.00054  3.22839E-01 0.00036  1.29112E+00 0.00179  7.75071E+00 0.01567 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.16716E-03 0.00950  1.22639E-04 0.06092  9.15628E-04 0.02197  8.19815E-04 0.02302  2.21458E-03 0.01358  8.59535E-04 0.02508  2.34961E-04 0.04250 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.51561E-01 0.02192  1.25559E-02 0.00099  3.08221E-02 0.00067  1.10945E-01 0.00079  3.22887E-01 0.00054  1.29102E+00 0.00259  8.29549E+00 0.01117 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.80081E-05 0.00148  2.79961E-05 0.00149  3.04778E-05 0.01701 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.53447E-05 0.00136  2.53339E-05 0.00136  2.75854E-05 0.01705 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.19717E-03 0.01084  1.29508E-04 0.06965  8.97744E-04 0.02592  8.56744E-04 0.02754  2.24395E-03 0.01608  8.27050E-04 0.02662  2.42177E-04 0.04818 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.59653E-01 0.02604  1.25767E-02 0.00172  3.08614E-02 0.00091  1.10796E-01 0.00100  3.22856E-01 0.00068  1.28822E+00 0.00365  8.24282E+00 0.01586 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.76530E-05 0.00351  2.76473E-05 0.00351  2.65082E-05 0.04572 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.50232E-05 0.00345  2.50180E-05 0.00346  2.39854E-05 0.04575 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.05195E-03 0.03584  1.17516E-04 0.26876  1.02914E-03 0.08824  7.30992E-04 0.08961  2.18203E-03 0.05427  7.51430E-04 0.09444  2.40842E-04 0.18246 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.75131E-01 0.08573  1.25845E-02 0.00530  3.08714E-02 0.00201  1.11123E-01 0.00244  3.22961E-01 0.00168  1.27686E+00 0.00938  8.42481E+00 0.03825 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.06349E-03 0.03528  1.18671E-04 0.27849  1.01331E-03 0.08736  7.28758E-04 0.08786  2.19332E-03 0.05306  7.69791E-04 0.09309  2.39629E-04 0.17447 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.61012E-01 0.08334  1.25845E-02 0.00530  3.08757E-02 0.00201  1.11116E-01 0.00244  3.22963E-01 0.00168  1.27748E+00 0.00937  8.44827E+00 0.03777 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.84961E+02 0.03639 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.77840E-05 0.00085 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.51417E-05 0.00058 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.17964E-03 0.00695 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.86521E+02 0.00704 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.74024E-07 0.00078 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.67805E-06 0.00044  2.67777E-06 0.00044  2.73569E-06 0.00572 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.93762E-05 0.00095  3.93951E-05 0.00095  3.59728E-05 0.01099 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.30992E-01 0.00035  6.31269E-01 0.00035  5.96237E-01 0.01006 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05468E+01 0.01560 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.50590E+01 0.00050  3.33706E+01 0.00053 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.91770E+04 0.00326  3.04442E+05 0.00121  6.10637E+05 0.00091  6.51205E+05 0.00061  5.98150E+05 0.00055  6.40148E+05 0.00064  4.35760E+05 0.00073  3.85552E+05 0.00063  2.94753E+05 0.00056  2.40628E+05 0.00076  2.07658E+05 0.00084  1.87202E+05 0.00091  1.72885E+05 0.00097  1.64371E+05 0.00090  1.60020E+05 0.00087  1.38324E+05 0.00108  1.36864E+05 0.00104  1.35521E+05 0.00088  1.33115E+05 0.00095  2.59772E+05 0.00074  2.51241E+05 0.00055  1.81124E+05 0.00086  1.17441E+05 0.00083  1.35399E+05 0.00067  1.28925E+05 0.00101  1.14872E+05 0.00086  1.86248E+05 0.00074  4.30520E+04 0.00185  5.36566E+04 0.00135  4.87253E+04 0.00133  2.84042E+04 0.00216  4.91272E+04 0.00138  3.27869E+04 0.00174  2.67121E+04 0.00169  4.59506E+03 0.00262  3.95795E+03 0.00370  3.47282E+03 0.00398  3.31450E+03 0.00396  3.38638E+03 0.00421  3.72618E+03 0.00317  4.40810E+03 0.00344  4.52549E+03 0.00332  8.94483E+03 0.00231  1.47019E+04 0.00224  1.90505E+04 0.00210  5.11798E+04 0.00104  5.38379E+04 0.00172  5.74219E+04 0.00147  3.73192E+04 0.00148  2.64029E+04 0.00178  1.96184E+04 0.00139  2.31270E+04 0.00160  4.53704E+04 0.00124  6.42774E+04 0.00121  1.28366E+05 0.00129  2.03677E+05 0.00154  3.09728E+05 0.00161  2.00146E+05 0.00174  1.44056E+05 0.00178  1.04024E+05 0.00185  9.37241E+04 0.00155  9.25039E+04 0.00180  7.73858E+04 0.00175  5.25116E+04 0.00196  4.85580E+04 0.00210  4.32931E+04 0.00182  3.66178E+04 0.00204  2.88884E+04 0.00186  1.93769E+04 0.00222  6.90660E+03 0.00247 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.12514E-01 0.00052 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.51365E+18 0.00049  3.89035E+17 0.00144 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39049E-01 0.00012  1.56548E+00 0.00045 ];
INF_CAPT                  (idx, [1:   4]) = [  8.06710E-03 0.00074  3.70031E-02 0.00084 ];
INF_ABS                   (idx, [1:   4]) = [  9.78481E-03 0.00061  6.54326E-02 0.00108 ];
INF_FISS                  (idx, [1:   4]) = [  1.71771E-03 0.00044  2.84295E-02 0.00141 ];
INF_NSF                   (idx, [1:   4]) = [  4.63577E-03 0.00044  7.62893E-02 0.00147 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.69880E+00 6.9E-05  2.68345E+00 7.6E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05665E+02 9.9E-06  2.05673E+02 1.4E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.58488E-08 0.00036  2.59027E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29259E-01 0.00012  1.50003E+00 0.00052 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44206E-01 0.00018  3.97300E-01 0.00061 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63785E-02 0.00038  9.37974E-02 0.00099 ];
INF_SCATT3                (idx, [1:   4]) = [  7.31422E-03 0.00356  2.81386E-02 0.00233 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03501E-02 0.00194 -8.98039E-03 0.00631 ];
INF_SCATT5                (idx, [1:   4]) = [  1.52542E-04 0.09656  6.65123E-03 0.00848 ];
INF_SCATT6                (idx, [1:   4]) = [  5.13271E-03 0.00238 -1.72094E-02 0.00352 ];
INF_SCATT7                (idx, [1:   4]) = [  7.85554E-04 0.01654  4.81632E-04 0.09560 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29302E-01 0.00012  1.50003E+00 0.00052 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44206E-01 0.00018  3.97300E-01 0.00061 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63789E-02 0.00038  9.37974E-02 0.00099 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.31424E-03 0.00356  2.81386E-02 0.00233 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03500E-02 0.00195 -8.98039E-03 0.00631 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.52743E-04 0.09656  6.65123E-03 0.00848 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.13287E-03 0.00238 -1.72094E-02 0.00352 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.85610E-04 0.01648  4.81632E-04 0.09560 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12404E-01 0.00026  1.01674E+00 0.00040 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56934E+00 0.00026  3.27847E-01 0.00040 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.74194E-03 0.00060  6.54326E-02 0.00108 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69908E-02 0.00024  6.65985E-02 0.00129 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12058E-01 0.00011  1.72018E-02 0.00042  1.15021E-03 0.00422  1.49888E+00 0.00052 ];
INF_S1                    (idx, [1:   8]) = [  2.39238E-01 0.00017  4.96799E-03 0.00095  4.97633E-04 0.00646  3.96802E-01 0.00062 ];
INF_S2                    (idx, [1:   8]) = [  9.79362E-02 0.00036 -1.55774E-03 0.00316  2.71083E-04 0.00972  9.35263E-02 0.00100 ];
INF_S3                    (idx, [1:   8]) = [  9.08896E-03 0.00279 -1.77474E-03 0.00189  9.54780E-05 0.02008  2.80432E-02 0.00235 ];
INF_S4                    (idx, [1:   8]) = [ -9.79557E-03 0.00211 -5.54579E-04 0.00564  5.76599E-07 1.00000 -8.98096E-03 0.00635 ];
INF_S5                    (idx, [1:   8]) = [  1.13442E-04 0.12383  3.90995E-05 0.07431 -3.96895E-05 0.03968  6.69092E-03 0.00833 ];
INF_S6                    (idx, [1:   8]) = [  5.26477E-03 0.00213 -1.32059E-04 0.01681 -4.97780E-05 0.02895 -1.71596E-02 0.00354 ];
INF_S7                    (idx, [1:   8]) = [  9.53212E-04 0.01431 -1.67658E-04 0.01280 -4.54990E-05 0.02207  5.27131E-04 0.08724 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12101E-01 0.00011  1.72018E-02 0.00042  1.15021E-03 0.00422  1.49888E+00 0.00052 ];
INF_SP1                   (idx, [1:   8]) = [  2.39238E-01 0.00017  4.96799E-03 0.00095  4.97633E-04 0.00646  3.96802E-01 0.00062 ];
INF_SP2                   (idx, [1:   8]) = [  9.79366E-02 0.00036 -1.55774E-03 0.00316  2.71083E-04 0.00972  9.35263E-02 0.00100 ];
INF_SP3                   (idx, [1:   8]) = [  9.08898E-03 0.00279 -1.77474E-03 0.00189  9.54780E-05 0.02008  2.80432E-02 0.00235 ];
INF_SP4                   (idx, [1:   8]) = [ -9.79540E-03 0.00211 -5.54579E-04 0.00564  5.76599E-07 1.00000 -8.98096E-03 0.00635 ];
INF_SP5                   (idx, [1:   8]) = [  1.13643E-04 0.12383  3.90995E-05 0.07431 -3.96895E-05 0.03968  6.69092E-03 0.00833 ];
INF_SP6                   (idx, [1:   8]) = [  5.26493E-03 0.00213 -1.32059E-04 0.01681 -4.97780E-05 0.02895 -1.71596E-02 0.00354 ];
INF_SP7                   (idx, [1:   8]) = [  9.53267E-04 0.01426 -1.67658E-04 0.01280 -4.54990E-05 0.02207  5.27131E-04 0.08724 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31729E-01 0.00042  1.20813E+00 0.00552 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33386E-01 0.00065  1.32022E+00 0.00702 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33418E-01 0.00086  1.31836E+00 0.00807 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28460E-01 0.00072  1.03503E+00 0.00576 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43847E+00 0.00042  2.76108E-01 0.00546 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42826E+00 0.00065  2.52777E-01 0.00690 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42808E+00 0.00086  2.53240E-01 0.00819 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45906E+00 0.00072  3.22306E-01 0.00572 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.16716E-03 0.00950  1.22639E-04 0.06092  9.15628E-04 0.02197  8.19815E-04 0.02302  2.21458E-03 0.01358  8.59535E-04 0.02508  2.34961E-04 0.04250 ];
LAMBDA                    (idx, [1:  14]) = [  7.51561E-01 0.02192  1.25559E-02 0.00099  3.08221E-02 0.00067  1.10945E-01 0.00079  3.22887E-01 0.00054  1.29102E+00 0.00259  8.29549E+00 0.01117 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0006' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:46 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:27:33 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284846 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.90024E-01  1.00261E+00  1.00233E+00  1.00117E+00  1.00386E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 9.3E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15829E-02 0.00111  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88417E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06405E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06834E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68237E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.50239E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.50147E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.38230E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.20451E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000852 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00017E+04 0.00090 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00017E+04 0.00090 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.31659E+02 ;
RUNNING_TIME              (idx, 1)        =  1.06792E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.32900E-01  4.32900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.18833E-01  1.05500E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.05076E+02  3.33997E+00  2.61088E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  9.53617E-01  2.65167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  5.59666E-02  7.83332E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.06792E+02  1.24861E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97846 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99888E+00 0.00023 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81338E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.60635E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68490E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.31122E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.04020E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.43150E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.56611E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.54172E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  3.76239E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.90446E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.91902E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.07020E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  8.43369E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.99744E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.37665E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.36611E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.52611E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.55314E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.42594E+13 ;
CS137_ACTIVITY            (idx, 1)        =  3.34909E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.37970E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.23244E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.07596E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.32086E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.26187E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 17 ;
BURNUP                     (idx, [1:  2])  = [  2.50000E+01  2.50024E+01 ];
BURN_DAYS                 (idx, 1)        =  6.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.29661E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  4.84120E+15 0.00129  3.55162E-01 0.00107 ];
U238_FISS                 (idx, [1:   4]) = [  1.17906E+15 0.00262  8.64918E-02 0.00245 ];
PU239_FISS                (idx, [1:   4]) = [  6.14796E+15 0.00111  4.51035E-01 0.00088 ];
PU240_FISS                (idx, [1:   4]) = [  1.29217E+13 0.02591  9.48217E-04 0.02595 ];
PU241_FISS                (idx, [1:   4]) = [  1.42463E+15 0.00238  1.04518E-01 0.00230 ];
U235_CAPT                 (idx, [1:   4]) = [  1.08835E+15 0.00294  3.79675E-02 0.00288 ];
U238_CAPT                 (idx, [1:   4]) = [  1.01139E+16 0.00106  3.52807E-01 0.00072 ];
PU239_CAPT                (idx, [1:   4]) = [  3.38156E+15 0.00156  1.17973E-01 0.00151 ];
PU240_CAPT                (idx, [1:   4]) = [  2.50088E+15 0.00189  8.72393E-02 0.00173 ];
PU241_CAPT                (idx, [1:   4]) = [  5.10761E+14 0.00402  1.78197E-02 0.00402 ];
XE135_CAPT                (idx, [1:   4]) = [  7.80181E+14 0.00316  2.72197E-02 0.00317 ];
SM149_CAPT                (idx, [1:   4]) = [  2.43352E+14 0.00617  8.48974E-03 0.00615 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000852 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.13410E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000852 5.00813E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3358005 3.36300E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1596957 1.59924E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45890 4.58987E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000852 5.00813E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -1.09896E-07 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.70497E+16 2.2E-05  3.70497E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36260E+16 4.5E-06  1.36260E+16 4.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.86693E+16 0.00044  2.20554E+16 0.00045  6.61393E+15 0.00115 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.22953E+16 0.00030  3.56813E+16 0.00028  6.61393E+15 0.00115 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.26187E+16 0.00052  4.26187E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.99508E+18 0.00048  5.28110E+17 0.00049  1.46697E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.91302E+14 0.00501 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.26866E+16 0.00031 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.49442E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.09614E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.09614E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.46120E+00 0.00060 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.76162E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.26282E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23571E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94018E-01 3.6E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96783E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.77632E-01 0.00063 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.69576E-01 0.00063 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.71905E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06153E+02 4.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.69769E-01 0.00063  8.65310E-01 0.00063  4.26546E-03 0.01129 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.69414E-01 0.00031 ];
COL_KEFF                  (idx, [1:   2]) = [  8.69446E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.69414E-01 0.00031 ];
ABS_KINF                  (idx, [1:   2]) = [  8.77468E-01 0.00030 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.69892E+01 0.00023 ];
IMP_ALF                   (idx, [1:   2]) = [  1.69963E+01 9.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  8.40181E-07 0.00393 ];
IMP_EALF                  (idx, [1:   2]) = [  8.31642E-07 0.00167 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.94110E-01 0.00276 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.93082E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.91310E-03 0.00632  1.38178E-04 0.04187  1.06423E-03 0.01408  9.40529E-04 0.01583  2.53014E-03 0.00985  9.54358E-04 0.01464  2.85669E-04 0.02812 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.68381E-01 0.01549  8.60233E-03 0.03045  3.06782E-02 0.00043  1.11046E-01 0.00056  3.23021E-01 0.00036  1.26934E+00 0.00216  7.56810E+00 0.01686 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.02940E-03 0.00934  1.14800E-04 0.06193  9.29542E-04 0.02270  7.89363E-04 0.02416  2.16316E-03 0.01506  8.04197E-04 0.02263  2.28333E-04 0.04106 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.43223E-01 0.02127  1.25697E-02 0.00117  3.06790E-02 0.00062  1.10934E-01 0.00083  3.22825E-01 0.00053  1.27152E+00 0.00309  8.13961E+00 0.01246 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.92521E-05 0.00152  2.92408E-05 0.00152  3.15482E-05 0.01872 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.54372E-05 0.00136  2.54274E-05 0.00137  2.74389E-05 0.01871 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.91143E-03 0.01138  1.18344E-04 0.07694  8.81322E-04 0.02579  7.86887E-04 0.03001  2.08349E-03 0.01802  8.12253E-04 0.02784  2.29132E-04 0.05044 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.59592E-01 0.02759  1.25895E-02 0.00203  3.06829E-02 0.00089  1.11007E-01 0.00108  3.22784E-01 0.00071  1.26670E+00 0.00409  8.25690E+00 0.01742 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.86012E-05 0.00356  2.85890E-05 0.00357  2.83339E-05 0.05070 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.48733E-05 0.00354  2.48627E-05 0.00355  2.46285E-05 0.05051 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.75177E-03 0.03859  1.46842E-04 0.21990  7.50426E-04 0.09320  8.84934E-04 0.09318  1.95407E-03 0.05933  8.22022E-04 0.09670  1.93473E-04 0.15586 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.68672E-01 0.08366  1.26107E-02 0.00468  3.06452E-02 0.00211  1.11203E-01 0.00255  3.24241E-01 0.00190  1.27547E+00 0.00929  7.87946E+00 0.04658 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.77285E-03 0.03781  1.44098E-04 0.21869  7.49506E-04 0.09226  8.69364E-04 0.09063  1.96851E-03 0.05802  8.36882E-04 0.09577  2.04485E-04 0.14946 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.77522E-01 0.08286  1.26107E-02 0.00468  3.06489E-02 0.00211  1.11174E-01 0.00254  3.24231E-01 0.00189  1.27761E+00 0.00917  7.86942E+00 0.04684 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.66840E+02 0.03868 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.89752E-05 0.00093 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.51966E-05 0.00068 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.94038E-03 0.00732 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.70565E+02 0.00734 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.72768E-07 0.00077 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.65293E-06 0.00042  2.65262E-06 0.00042  2.71123E-06 0.00615 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.96701E-05 0.00092  3.96920E-05 0.00093  3.55203E-05 0.01137 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.24205E-01 0.00036  6.24633E-01 0.00036  5.64880E-01 0.01062 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05726E+01 0.01455 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.50147E+01 0.00049  3.33237E+01 0.00055 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.03691E+04 0.00345  3.03963E+05 0.00176  6.10340E+05 0.00111  6.50286E+05 0.00066  5.97398E+05 0.00078  6.41018E+05 0.00053  4.35243E+05 0.00080  3.85223E+05 0.00072  2.94631E+05 0.00086  2.40859E+05 0.00080  2.07392E+05 0.00087  1.87343E+05 0.00101  1.73116E+05 0.00122  1.64681E+05 0.00091  1.60326E+05 0.00095  1.38416E+05 0.00082  1.36615E+05 0.00097  1.35800E+05 0.00090  1.33440E+05 0.00093  2.59797E+05 0.00061  2.51295E+05 0.00060  1.81363E+05 0.00073  1.17630E+05 0.00082  1.35523E+05 0.00112  1.29087E+05 0.00100  1.14764E+05 0.00108  1.85460E+05 0.00083  4.30002E+04 0.00138  5.31914E+04 0.00144  4.84221E+04 0.00190  2.83503E+04 0.00198  4.89057E+04 0.00147  3.24831E+04 0.00189  2.61751E+04 0.00189  4.42942E+03 0.00399  3.77222E+03 0.00353  3.28477E+03 0.00394  3.16359E+03 0.00296  3.20095E+03 0.00403  3.53465E+03 0.00296  4.24303E+03 0.00336  4.37362E+03 0.00378  8.71014E+03 0.00198  1.44109E+04 0.00150  1.88733E+04 0.00150  5.06071E+04 0.00166  5.31825E+04 0.00138  5.68727E+04 0.00131  3.69511E+04 0.00162  2.60609E+04 0.00145  1.92623E+04 0.00192  2.27876E+04 0.00173  4.46831E+04 0.00130  6.34253E+04 0.00119  1.27338E+05 0.00140  2.02529E+05 0.00115  3.08804E+05 0.00141  1.99589E+05 0.00165  1.43885E+05 0.00156  1.03779E+05 0.00163  9.35797E+04 0.00170  9.22434E+04 0.00163  7.72832E+04 0.00149  5.23643E+04 0.00160  4.84979E+04 0.00198  4.31814E+04 0.00173  3.66364E+04 0.00216  2.89374E+04 0.00213  1.94257E+04 0.00228  6.86854E+03 0.00285 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.77501E-01 0.00060 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.58851E+18 0.00061  4.06617E+17 0.00133 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39501E-01 0.00013  1.56771E+00 0.00046 ];
INF_CAPT                  (idx, [1:   4]) = [  8.33967E-03 0.00070  3.79315E-02 0.00081 ];
INF_ABS                   (idx, [1:   4]) = [  9.96924E-03 0.00057  6.50813E-02 0.00105 ];
INF_FISS                  (idx, [1:   4]) = [  1.62957E-03 0.00043  2.71498E-02 0.00141 ];
INF_NSF                   (idx, [1:   4]) = [  4.44012E-03 0.00041  7.37858E-02 0.00145 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.72472E+00 8.2E-05  2.71772E+00 6.4E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06028E+02 1.0E-05  2.06183E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.54461E-08 0.00043  2.59390E-06 0.00021 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29522E-01 0.00013  1.50267E+00 0.00053 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44250E-01 0.00021  3.97929E-01 0.00054 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63594E-02 0.00034  9.39321E-02 0.00109 ];
INF_SCATT3                (idx, [1:   4]) = [  7.28313E-03 0.00344  2.80912E-02 0.00218 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03565E-02 0.00168 -9.01089E-03 0.00503 ];
INF_SCATT5                (idx, [1:   4]) = [  1.63713E-04 0.08114  6.67415E-03 0.00621 ];
INF_SCATT6                (idx, [1:   4]) = [  5.15496E-03 0.00246 -1.72520E-02 0.00308 ];
INF_SCATT7                (idx, [1:   4]) = [  7.68344E-04 0.01835  5.51058E-04 0.08512 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29566E-01 0.00013  1.50267E+00 0.00053 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44251E-01 0.00021  3.97929E-01 0.00054 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63594E-02 0.00034  9.39321E-02 0.00109 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.28304E-03 0.00344  2.80912E-02 0.00218 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03563E-02 0.00168 -9.01089E-03 0.00503 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.63578E-04 0.08128  6.67415E-03 0.00621 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.15518E-03 0.00246 -1.72520E-02 0.00308 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.68272E-04 0.01836  5.51058E-04 0.08512 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12527E-01 0.00033  1.01901E+00 0.00042 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56843E+00 0.00033  3.27116E-01 0.00042 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.92560E-03 0.00057  6.50813E-02 0.00105 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70057E-02 0.00023  6.61868E-02 0.00115 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12495E-01 0.00013  1.70274E-02 0.00039  1.14498E-03 0.00521  1.50152E+00 0.00053 ];
INF_S1                    (idx, [1:   8]) = [  2.39333E-01 0.00021  4.91775E-03 0.00079  4.92225E-04 0.00687  3.97437E-01 0.00054 ];
INF_S2                    (idx, [1:   8]) = [  9.79111E-02 0.00033 -1.55174E-03 0.00228  2.69312E-04 0.00892  9.36628E-02 0.00109 ];
INF_S3                    (idx, [1:   8]) = [  9.04743E-03 0.00271 -1.76430E-03 0.00118  9.89756E-05 0.01871  2.79922E-02 0.00220 ];
INF_S4                    (idx, [1:   8]) = [ -9.80906E-03 0.00169 -5.47409E-04 0.00373  2.70192E-07 1.00000 -9.01116E-03 0.00510 ];
INF_S5                    (idx, [1:   8]) = [  1.19020E-04 0.10728  4.46924E-05 0.04764 -3.94178E-05 0.04442  6.71357E-03 0.00622 ];
INF_S6                    (idx, [1:   8]) = [  5.28613E-03 0.00233 -1.31162E-04 0.01557 -4.97382E-05 0.03169 -1.72022E-02 0.00307 ];
INF_S7                    (idx, [1:   8]) = [  9.35779E-04 0.01512 -1.67435E-04 0.00957 -4.50722E-05 0.03884  5.96130E-04 0.07864 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12539E-01 0.00013  1.70274E-02 0.00039  1.14498E-03 0.00521  1.50152E+00 0.00053 ];
INF_SP1                   (idx, [1:   8]) = [  2.39333E-01 0.00021  4.91775E-03 0.00079  4.92225E-04 0.00687  3.97437E-01 0.00054 ];
INF_SP2                   (idx, [1:   8]) = [  9.79111E-02 0.00033 -1.55174E-03 0.00228  2.69312E-04 0.00892  9.36628E-02 0.00109 ];
INF_SP3                   (idx, [1:   8]) = [  9.04734E-03 0.00271 -1.76430E-03 0.00118  9.89756E-05 0.01871  2.79922E-02 0.00220 ];
INF_SP4                   (idx, [1:   8]) = [ -9.80894E-03 0.00169 -5.47409E-04 0.00373  2.70192E-07 1.00000 -9.01116E-03 0.00510 ];
INF_SP5                   (idx, [1:   8]) = [  1.18886E-04 0.10749  4.46924E-05 0.04764 -3.94178E-05 0.04442  6.71357E-03 0.00622 ];
INF_SP6                   (idx, [1:   8]) = [  5.28634E-03 0.00233 -1.31162E-04 0.01557 -4.97382E-05 0.03169 -1.72022E-02 0.00307 ];
INF_SP7                   (idx, [1:   8]) = [  9.35707E-04 0.01513 -1.67435E-04 0.00957 -4.50722E-05 0.03884  5.96130E-04 0.07864 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32054E-01 0.00059  1.21346E+00 0.00943 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33737E-01 0.00081  1.32733E+00 0.01167 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33608E-01 0.00097  1.31108E+00 0.01142 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28892E-01 0.00088  1.04740E+00 0.00898 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43646E+00 0.00059  2.75278E-01 0.00934 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42613E+00 0.00081  2.51957E-01 0.01173 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42693E+00 0.00097  2.55018E-01 0.01110 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45632E+00 0.00088  3.18858E-01 0.00888 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.02940E-03 0.00934  1.14800E-04 0.06193  9.29542E-04 0.02270  7.89363E-04 0.02416  2.16316E-03 0.01506  8.04197E-04 0.02263  2.28333E-04 0.04106 ];
LAMBDA                    (idx, [1:  14]) = [  7.43223E-01 0.02127  1.25697E-02 0.00117  3.06790E-02 0.00062  1.10934E-01 0.00083  3.22825E-01 0.00053  1.27152E+00 0.00309  8.13961E+00 0.01246 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0006' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:46 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:33:35 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284846 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.89014E-01  1.00170E+00  1.00261E+00  1.00307E+00  1.00360E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.1E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16389E-02 0.00106  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88361E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05991E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06421E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68618E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.49354E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.49263E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.38323E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.22169E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001441 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00029E+04 0.00102 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00029E+04 0.00102 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.61756E+02 ;
RUNNING_TIME              (idx, 1)        =  1.12818E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.32900E-01  4.32900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.40133E-01  1.04167E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.11025E+02  3.34003E+00  2.60860E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.00915E+00  2.63000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  5.93833E-02  7.83332E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.12818E+02  1.24862E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97931 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00015E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81538E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.72647E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68147E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.45840E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.14088E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.50708E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.58555E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.53072E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  4.99747E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.06085E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.10457E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.71829E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  8.92896E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.08902E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.72860E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.39904E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.56731E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.59797E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.09720E+13 ;
CS137_ACTIVITY            (idx, 1)        =  4.00774E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.48890E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.20190E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.69637E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.34980E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.46125E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 18 ;
BURNUP                     (idx, [1:  2])  = [  3.00000E+01  3.00029E+01 ];
BURN_DAYS                 (idx, 1)        =  7.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.72456E-01 0.00116 ];
U235_FISS                 (idx, [1:   4]) = [  3.98182E+15 0.00148  2.92565E-01 0.00130 ];
U238_FISS                 (idx, [1:   4]) = [  1.22314E+15 0.00277  8.98567E-02 0.00256 ];
PU239_FISS                (idx, [1:   4]) = [  6.60079E+15 0.00112  4.84987E-01 0.00085 ];
PU240_FISS                (idx, [1:   4]) = [  1.58784E+13 0.02335  1.16616E-03 0.02334 ];
PU241_FISS                (idx, [1:   4]) = [  1.75433E+15 0.00233  1.28902E-01 0.00224 ];
U235_CAPT                 (idx, [1:   4]) = [  8.96787E+14 0.00319  2.92432E-02 0.00315 ];
U238_CAPT                 (idx, [1:   4]) = [  1.05527E+16 0.00108  3.44087E-01 0.00079 ];
PU239_CAPT                (idx, [1:   4]) = [  3.61536E+15 0.00152  1.17901E-01 0.00153 ];
PU240_CAPT                (idx, [1:   4]) = [  2.85392E+15 0.00186  9.30570E-02 0.00172 ];
PU241_CAPT                (idx, [1:   4]) = [  6.25199E+14 0.00360  2.03878E-02 0.00359 ];
XE135_CAPT                (idx, [1:   4]) = [  7.90366E+14 0.00326  2.57758E-02 0.00329 ];
SM149_CAPT                (idx, [1:   4]) = [  2.53342E+14 0.00576  8.26223E-03 0.00577 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001441 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.31022E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001441 5.00831E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3432251 3.43707E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1523408 1.52546E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45782 4.57847E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001441 5.00831E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -9.31323E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.73604E+16 2.3E-05  3.73604E+16 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35983E+16 5.0E-06  1.35983E+16 5.0E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.06668E+16 0.00045  2.37785E+16 0.00046  6.88827E+15 0.00126 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.42651E+16 0.00031  3.73768E+16 0.00029  6.88827E+15 0.00126 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.46125E+16 0.00051  4.46125E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.08542E+18 0.00049  5.52372E+17 0.00047  1.53305E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.08558E+14 0.00516 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.46736E+16 0.00032 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.56046E+18 0.00067 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.09037E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.09037E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.42649E+00 0.00062 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.74903E-01 0.00036 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.20335E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23394E+00 0.00039 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94035E-01 3.7E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96789E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.46061E-01 0.00069 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.38314E-01 0.00069 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.74744E+00 2.8E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06573E+02 5.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.38423E-01 0.00070  8.34253E-01 0.00069  4.06012E-03 0.01154 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.37719E-01 0.00032 ];
COL_KEFF                  (idx, [1:   2]) = [  8.37551E-01 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.37719E-01 0.00032 ];
ABS_KINF                  (idx, [1:   2]) = [  8.45463E-01 0.00032 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.69360E+01 0.00026 ];
IMP_ALF                   (idx, [1:   2]) = [  1.69299E+01 9.4E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  8.86899E-07 0.00434 ];
IMP_EALF                  (idx, [1:   2]) = [  8.88678E-07 0.00160 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.06861E-01 0.00285 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.06925E-01 0.00113 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.95223E-03 0.00633  1.39926E-04 0.04164  1.10375E-03 0.01470  9.18131E-04 0.01669  2.53986E-03 0.00999  9.73734E-04 0.01507  2.76833E-04 0.03180 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.38715E-01 0.01582  8.86461E-03 0.02920  3.06068E-02 0.00044  1.11252E-01 0.00060  3.23112E-01 0.00036  1.24650E+00 0.00240  7.18844E+00 0.01984 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.84695E-03 0.00982  1.27228E-04 0.06500  8.83177E-04 0.02389  7.55511E-04 0.02498  2.06649E-03 0.01536  7.99139E-04 0.02469  2.15402E-04 0.04835 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.23706E-01 0.02371  1.26223E-02 0.00139  3.06114E-02 0.00066  1.11307E-01 0.00088  3.23294E-01 0.00055  1.24595E+00 0.00367  8.00438E+00 0.01408 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.04315E-05 0.00160  3.04122E-05 0.00160  3.45612E-05 0.01940 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.55076E-05 0.00140  2.54914E-05 0.00141  2.89657E-05 0.01933 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.84697E-03 0.01168  1.11656E-04 0.07710  8.78661E-04 0.02768  7.63087E-04 0.02828  2.06314E-03 0.01829  8.01341E-04 0.02977  2.29078E-04 0.05626 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.40799E-01 0.02852  1.26062E-02 0.00215  3.06070E-02 0.00087  1.11272E-01 0.00116  3.23362E-01 0.00076  1.25053E+00 0.00501  8.01037E+00 0.01935 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.97030E-05 0.00359  2.96935E-05 0.00360  2.88900E-05 0.04530 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.48982E-05 0.00354  2.48903E-05 0.00355  2.42035E-05 0.04530 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.66909E-03 0.03822  9.34662E-05 0.27364  8.17137E-04 0.09757  6.26784E-04 0.09806  2.07505E-03 0.05833  8.37556E-04 0.09654  2.19100E-04 0.16941 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.15867E-01 0.07811  1.26899E-02 0.00737  3.05592E-02 0.00213  1.11384E-01 0.00297  3.23638E-01 0.00196  1.27170E+00 0.01001  7.76857E+00 0.04793 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.67219E-03 0.03808  9.45823E-05 0.26151  8.23268E-04 0.09651  6.24473E-04 0.09657  2.05867E-03 0.05767  8.42658E-04 0.09631  2.28536E-04 0.16517 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.23234E-01 0.07768  1.26899E-02 0.00737  3.05587E-02 0.00212  1.11377E-01 0.00296  3.23622E-01 0.00196  1.27111E+00 0.01004  7.78104E+00 0.04754 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.57895E+02 0.03827 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.00206E-05 0.00099 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.51641E-05 0.00073 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.72926E-03 0.00765 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.57583E+02 0.00769 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.70542E-07 0.00080 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.63124E-06 0.00045  2.63112E-06 0.00045  2.65435E-06 0.00621 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.98423E-05 0.00098  3.98564E-05 0.00097  3.71878E-05 0.01138 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.18328E-01 0.00035  6.18858E-01 0.00035  5.42065E-01 0.01064 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.10833E+01 0.01504 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.49263E+01 0.00052  3.32928E+01 0.00054 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.11987E+04 0.00282  3.06219E+05 0.00083  6.10154E+05 0.00106  6.50371E+05 0.00081  5.98048E+05 0.00081  6.41324E+05 0.00061  4.35460E+05 0.00064  3.85369E+05 0.00068  2.94782E+05 0.00062  2.40793E+05 0.00074  2.07783E+05 0.00078  1.87150E+05 0.00095  1.72983E+05 0.00077  1.64343E+05 0.00083  1.60273E+05 0.00084  1.38455E+05 0.00109  1.36954E+05 0.00074  1.35677E+05 0.00120  1.33332E+05 0.00104  2.59832E+05 0.00062  2.51618E+05 0.00087  1.81528E+05 0.00079  1.17749E+05 0.00090  1.35437E+05 0.00090  1.29035E+05 0.00086  1.14577E+05 0.00093  1.84829E+05 0.00080  4.28462E+04 0.00167  5.26419E+04 0.00168  4.81564E+04 0.00177  2.82807E+04 0.00192  4.87143E+04 0.00143  3.19501E+04 0.00156  2.57429E+04 0.00207  4.28815E+03 0.00339  3.61430E+03 0.00402  3.18012E+03 0.00305  3.06063E+03 0.00431  3.10676E+03 0.00395  3.38325E+03 0.00360  4.11496E+03 0.00369  4.22545E+03 0.00319  8.51196E+03 0.00269  1.42056E+04 0.00184  1.85162E+04 0.00269  4.99051E+04 0.00170  5.23813E+04 0.00140  5.62317E+04 0.00139  3.64808E+04 0.00177  2.56725E+04 0.00173  1.90086E+04 0.00180  2.24921E+04 0.00192  4.40141E+04 0.00130  6.26707E+04 0.00164  1.25929E+05 0.00167  2.00875E+05 0.00178  3.06873E+05 0.00189  1.98475E+05 0.00189  1.43301E+05 0.00208  1.03337E+05 0.00188  9.33255E+04 0.00201  9.20685E+04 0.00186  7.70681E+04 0.00214  5.23013E+04 0.00238  4.83849E+04 0.00191  4.30397E+04 0.00233  3.64811E+04 0.00185  2.88628E+04 0.00244  1.93740E+04 0.00230  6.88787E+03 0.00269 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.45291E-01 0.00050 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.66264E+18 0.00042  4.22823E+17 0.00173 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39701E-01 0.00013  1.56839E+00 0.00047 ];
INF_CAPT                  (idx, [1:   4]) = [  8.58608E-03 0.00053  3.87713E-02 0.00107 ];
INF_ABS                   (idx, [1:   4]) = [  1.01404E-02 0.00047  6.48256E-02 0.00134 ];
INF_FISS                  (idx, [1:   4]) = [  1.55436E-03 0.00038  2.60543E-02 0.00175 ];
INF_NSF                   (idx, [1:   4]) = [  4.27129E-03 0.00038  7.15799E-02 0.00181 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.74793E+00 6.8E-05  2.74732E+00 7.6E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06355E+02 1.1E-05  2.06624E+02 1.5E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.50084E-08 0.00046  2.59791E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29563E-01 0.00013  1.50354E+00 0.00056 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44172E-01 0.00026  3.97774E-01 0.00056 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63120E-02 0.00036  9.37615E-02 0.00091 ];
INF_SCATT3                (idx, [1:   4]) = [  7.24453E-03 0.00360  2.80776E-02 0.00217 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03495E-02 0.00195 -9.01984E-03 0.00696 ];
INF_SCATT5                (idx, [1:   4]) = [  1.70210E-04 0.11705  6.77399E-03 0.00977 ];
INF_SCATT6                (idx, [1:   4]) = [  5.13122E-03 0.00300 -1.72956E-02 0.00306 ];
INF_SCATT7                (idx, [1:   4]) = [  7.55393E-04 0.02096  4.98253E-04 0.08980 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29607E-01 0.00013  1.50354E+00 0.00056 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44173E-01 0.00026  3.97774E-01 0.00056 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63120E-02 0.00036  9.37615E-02 0.00091 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.24463E-03 0.00360  2.80776E-02 0.00217 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03493E-02 0.00195 -9.01984E-03 0.00696 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.70132E-04 0.11689  6.77399E-03 0.00977 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.13115E-03 0.00301 -1.72956E-02 0.00306 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.55479E-04 0.02099  4.98253E-04 0.08980 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12483E-01 0.00033  1.02060E+00 0.00043 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56876E+00 0.00033  3.26606E-01 0.00043 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.00958E-02 0.00047  6.48256E-02 0.00134 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70050E-02 0.00027  6.59902E-02 0.00167 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12696E-01 0.00013  1.68664E-02 0.00038  1.13327E-03 0.00472  1.50240E+00 0.00056 ];
INF_S1                    (idx, [1:   8]) = [  2.39310E-01 0.00027  4.86265E-03 0.00083  4.90665E-04 0.00750  3.97284E-01 0.00056 ];
INF_S2                    (idx, [1:   8]) = [  9.78578E-02 0.00035 -1.54585E-03 0.00278  2.67252E-04 0.01156  9.34943E-02 0.00092 ];
INF_S3                    (idx, [1:   8]) = [  8.99137E-03 0.00285 -1.74685E-03 0.00200  9.60997E-05 0.02240  2.79815E-02 0.00216 ];
INF_S4                    (idx, [1:   8]) = [ -9.81667E-03 0.00207 -5.32856E-04 0.00693  6.33540E-07 1.00000 -9.02047E-03 0.00692 ];
INF_S5                    (idx, [1:   8]) = [  1.25892E-04 0.15653  4.43177E-05 0.05706 -3.76999E-05 0.03558  6.81169E-03 0.00971 ];
INF_S6                    (idx, [1:   8]) = [  5.26810E-03 0.00297 -1.36881E-04 0.01951 -4.96781E-05 0.02865 -1.72459E-02 0.00308 ];
INF_S7                    (idx, [1:   8]) = [  9.25629E-04 0.01645 -1.70236E-04 0.01334 -4.61597E-05 0.02838  5.44413E-04 0.08260 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12741E-01 0.00013  1.68664E-02 0.00038  1.13327E-03 0.00472  1.50240E+00 0.00056 ];
INF_SP1                   (idx, [1:   8]) = [  2.39310E-01 0.00027  4.86265E-03 0.00083  4.90665E-04 0.00750  3.97284E-01 0.00056 ];
INF_SP2                   (idx, [1:   8]) = [  9.78578E-02 0.00034 -1.54585E-03 0.00278  2.67252E-04 0.01156  9.34943E-02 0.00092 ];
INF_SP3                   (idx, [1:   8]) = [  8.99148E-03 0.00285 -1.74685E-03 0.00200  9.60997E-05 0.02240  2.79815E-02 0.00216 ];
INF_SP4                   (idx, [1:   8]) = [ -9.81646E-03 0.00207 -5.32856E-04 0.00693  6.33540E-07 1.00000 -9.02047E-03 0.00692 ];
INF_SP5                   (idx, [1:   8]) = [  1.25814E-04 0.15639  4.43177E-05 0.05706 -3.76999E-05 0.03558  6.81169E-03 0.00971 ];
INF_SP6                   (idx, [1:   8]) = [  5.26803E-03 0.00297 -1.36881E-04 0.01951 -4.96781E-05 0.02865 -1.72459E-02 0.00308 ];
INF_SP7                   (idx, [1:   8]) = [  9.25715E-04 0.01647 -1.70236E-04 0.01334 -4.61597E-05 0.02838  5.44413E-04 0.08260 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31816E-01 0.00053  1.22306E+00 0.00800 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33474E-01 0.00094  1.32992E+00 0.00772 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33423E-01 0.00102  1.34719E+00 0.01249 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28628E-01 0.00087  1.04486E+00 0.00842 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43793E+00 0.00053  2.72962E-01 0.00804 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42774E+00 0.00094  2.50999E-01 0.00770 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42806E+00 0.00102  2.48312E-01 0.01194 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45800E+00 0.00087  3.19575E-01 0.00856 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.84695E-03 0.00982  1.27228E-04 0.06500  8.83177E-04 0.02389  7.55511E-04 0.02498  2.06649E-03 0.01536  7.99139E-04 0.02469  2.15402E-04 0.04835 ];
LAMBDA                    (idx, [1:  14]) = [  7.23706E-01 0.02371  1.26223E-02 0.00139  3.06114E-02 0.00066  1.11307E-01 0.00088  3.23294E-01 0.00055  1.24595E+00 0.00367  8.00438E+00 0.01408 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0006' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:46 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:39:36 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284846 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.89909E-01  1.00172E+00  1.00204E+00  1.00182E+00  1.00452E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 9.3E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16795E-02 0.00111  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88321E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05466E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05897E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68974E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.48723E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.48632E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.39624E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.23298E-01 0.00112  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001234 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00025E+04 0.00096 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00025E+04 0.00096 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.91808E+02 ;
RUNNING_TIME              (idx, 1)        =  1.18835E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.32900E-01  4.32900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.61400E-01  1.03000E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.16966E+02  3.34005E+00  2.60115E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.06322E+00  2.61500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  6.27333E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.18835E+02  1.24849E+02 ];
CPU_USAGE                 (idx, 1)        = 4.98008 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00047E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81728E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.86538E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68619E+04 ;
TOT_SF_RATE               (idx, 1)        =  4.13182E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.24347E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.58499E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62188E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.52766E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  6.46566E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.22627E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.52869E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.04183E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  9.36967E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.18445E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.05236E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.43997E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.61078E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.64965E+14 ;
CS134_ACTIVITY            (idx, 1)        =  7.93452E+13 ;
CS137_ACTIVITY            (idx, 1)        =  4.66237E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.61404E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.17867E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.40842E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.38054E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.64411E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 19 ;
BURNUP                     (idx, [1:  2])  = [  3.50000E+01  3.50033E+01 ];
BURN_DAYS                 (idx, 1)        =  8.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  8.11370E-01 0.00113 ];
U235_FISS                 (idx, [1:   4]) = [  3.26031E+15 0.00167  2.40308E-01 0.00153 ];
U238_FISS                 (idx, [1:   4]) = [  1.26885E+15 0.00286  9.35116E-02 0.00269 ];
PU239_FISS                (idx, [1:   4]) = [  6.94549E+15 0.00109  5.11919E-01 0.00081 ];
PU240_FISS                (idx, [1:   4]) = [  1.81745E+13 0.02323  1.33981E-03 0.02323 ];
PU241_FISS                (idx, [1:   4]) = [  2.03008E+15 0.00206  1.49632E-01 0.00196 ];
U235_CAPT                 (idx, [1:   4]) = [  7.21939E+14 0.00379  2.21955E-02 0.00375 ];
U238_CAPT                 (idx, [1:   4]) = [  1.09275E+16 0.00104  3.35940E-01 0.00077 ];
PU239_CAPT                (idx, [1:   4]) = [  3.82347E+15 0.00147  1.17560E-01 0.00149 ];
PU240_CAPT                (idx, [1:   4]) = [  3.16730E+15 0.00180  9.73693E-02 0.00163 ];
PU241_CAPT                (idx, [1:   4]) = [  7.30152E+14 0.00372  2.24482E-02 0.00369 ];
XE135_CAPT                (idx, [1:   4]) = [  8.06604E+14 0.00354  2.47991E-02 0.00352 ];
SM149_CAPT                (idx, [1:   4]) = [  2.61330E+14 0.00616  8.03412E-03 0.00613 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001234 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.29936E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001234 5.00830E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3496871 3.50196E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1458830 1.46080E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 45533 4.55410E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001234 5.00830E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -1.30385E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.76307E+16 2.1E-05  3.76307E+16 2.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35742E+16 4.5E-06  1.35742E+16 4.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.25202E+16 0.00043  2.53818E+16 0.00045  7.13841E+15 0.00124 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.60944E+16 0.00031  3.89560E+16 0.00030  7.13841E+15 0.00124 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.64411E+16 0.00052  4.64411E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.16886E+18 0.00050  5.74637E+17 0.00047  1.59422E+18 0.00056 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.23063E+14 0.00505 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.65174E+16 0.00031 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.62152E+18 0.00066 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.08461E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.08461E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.39103E+00 0.00066 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.74086E-01 0.00036 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.15133E-01 0.00037 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23400E+00 0.00041 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94057E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96816E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.17271E-01 0.00067 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.09828E-01 0.00067 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.77222E+00 2.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06939E+02 4.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.09875E-01 0.00067  8.05975E-01 0.00068  3.85237E-03 0.01222 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.10336E-01 0.00031 ];
COL_KEFF                  (idx, [1:   2]) = [  8.10396E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.10336E-01 0.00031 ];
ABS_KINF                  (idx, [1:   2]) = [  8.17784E-01 0.00031 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.68773E+01 0.00027 ];
IMP_ALF                   (idx, [1:   2]) = [  1.68763E+01 0.00011 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  9.41011E-07 0.00461 ];
IMP_EALF                  (idx, [1:   2]) = [  9.37805E-07 0.00182 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.17964E-01 0.00298 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.18262E-01 0.00119 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.08726E-03 0.00624  1.41179E-04 0.04430  1.14770E-03 0.01507  9.32298E-04 0.01644  2.55957E-03 0.00999  1.01188E-03 0.01588  2.94634E-04 0.03021 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.48032E-01 0.01513  8.23574E-03 0.03289  3.04859E-02 0.00040  1.11513E-01 0.00064  3.23427E-01 0.00041  1.24064E+00 0.00244  7.24640E+00 0.01875 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.76215E-03 0.00950  1.12584E-04 0.06816  8.89740E-04 0.02277  7.22712E-04 0.02424  2.01728E-03 0.01635  7.93943E-04 0.02507  2.25892E-04 0.04455 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.54741E-01 0.02296  1.26658E-02 0.00167  3.04888E-02 0.00060  1.11495E-01 0.00094  3.23488E-01 0.00064  1.24101E+00 0.00367  7.97795E+00 0.01415 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.15900E-05 0.00159  3.15751E-05 0.00159  3.47497E-05 0.01909 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.55780E-05 0.00143  2.55660E-05 0.00143  2.81342E-05 0.01901 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.75704E-03 0.01227  1.17292E-04 0.07673  8.86572E-04 0.02753  7.41954E-04 0.03050  2.04008E-03 0.01920  7.69551E-04 0.03018  2.01592E-04 0.05966 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.02827E-01 0.03127  1.26597E-02 0.00253  3.05072E-02 0.00083  1.11519E-01 0.00130  3.23473E-01 0.00079  1.24295E+00 0.00516  7.92891E+00 0.02341 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.06487E-05 0.00367  3.06417E-05 0.00369  2.81088E-05 0.05082 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.48148E-05 0.00358  2.48091E-05 0.00360  2.27577E-05 0.05082 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.63827E-03 0.04078  7.33822E-05 0.25968  8.23359E-04 0.10086  7.04441E-04 0.10064  1.92085E-03 0.06587  8.96821E-04 0.10103  2.19413E-04 0.18731 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.34048E-01 0.09129  1.27405E-02 0.00797  3.05532E-02 0.00211  1.12028E-01 0.00307  3.24183E-01 0.00222  1.22705E+00 0.01250  7.31229E+00 0.07217 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.60572E-03 0.04035  7.65318E-05 0.25026  7.98974E-04 0.09748  7.02993E-04 0.09705  1.92739E-03 0.06472  8.91647E-04 0.09871  2.08184E-04 0.18322 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.32042E-01 0.09160  1.27406E-02 0.00797  3.05553E-02 0.00211  1.12049E-01 0.00307  3.24185E-01 0.00222  1.22806E+00 0.01246  7.30950E+00 0.07223 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.51557E+02 0.04076 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.11745E-05 0.00097 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.52416E-05 0.00069 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.68582E-03 0.00788 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.50312E+02 0.00780 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.68652E-07 0.00079 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.61182E-06 0.00043  2.61164E-06 0.00043  2.64445E-06 0.00642 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  4.00167E-05 0.00093  4.00334E-05 0.00093  3.66260E-05 0.01189 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.13171E-01 0.00037  6.13824E-01 0.00037  5.16895E-01 0.01038 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07309E+01 0.01505 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.48632E+01 0.00050  3.32371E+01 0.00058 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.17069E+04 0.00301  3.06321E+05 0.00148  6.10943E+05 0.00084  6.51023E+05 0.00073  5.98285E+05 0.00072  6.41844E+05 0.00062  4.35774E+05 0.00070  3.85486E+05 0.00073  2.95011E+05 0.00080  2.41212E+05 0.00094  2.08079E+05 0.00082  1.87469E+05 0.00086  1.72968E+05 0.00074  1.64619E+05 0.00104  1.60330E+05 0.00101  1.38523E+05 0.00100  1.36865E+05 0.00128  1.35592E+05 0.00116  1.33348E+05 0.00078  2.60146E+05 0.00065  2.52117E+05 0.00065  1.81409E+05 0.00101  1.17850E+05 0.00080  1.35299E+05 0.00076  1.29232E+05 0.00066  1.14445E+05 0.00102  1.84277E+05 0.00077  4.27533E+04 0.00152  5.22397E+04 0.00170  4.78804E+04 0.00173  2.81409E+04 0.00210  4.83801E+04 0.00132  3.15988E+04 0.00167  2.53503E+04 0.00191  4.21434E+03 0.00395  3.51900E+03 0.00330  3.10947E+03 0.00360  2.99154E+03 0.00487  3.02043E+03 0.00392  3.27616E+03 0.00335  3.98134E+03 0.00388  4.14350E+03 0.00383  8.37249E+03 0.00385  1.40105E+04 0.00242  1.83013E+04 0.00255  4.92066E+04 0.00155  5.18713E+04 0.00131  5.56991E+04 0.00157  3.61101E+04 0.00167  2.54144E+04 0.00180  1.88350E+04 0.00242  2.22296E+04 0.00169  4.36381E+04 0.00185  6.21161E+04 0.00139  1.25079E+05 0.00142  1.99742E+05 0.00158  3.05064E+05 0.00168  1.97557E+05 0.00156  1.42702E+05 0.00189  1.02937E+05 0.00206  9.28778E+04 0.00208  9.18908E+04 0.00220  7.69931E+04 0.00222  5.21098E+04 0.00206  4.83430E+04 0.00249  4.30467E+04 0.00229  3.64943E+04 0.00193  2.88147E+04 0.00224  1.93305E+04 0.00248  6.87381E+03 0.00266 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.17845E-01 0.00047 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.73101E+18 0.00046  4.37886E+17 0.00165 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39975E-01 0.00010  1.56853E+00 0.00047 ];
INF_CAPT                  (idx, [1:   4]) = [  8.79115E-03 0.00071  3.95192E-02 0.00098 ];
INF_ABS                   (idx, [1:   4]) = [  1.02803E-02 0.00062  6.46376E-02 0.00124 ];
INF_FISS                  (idx, [1:   4]) = [  1.48910E-03 0.00054  2.51184E-02 0.00167 ];
INF_NSF                   (idx, [1:   4]) = [  4.12218E-03 0.00055  6.96575E-02 0.00172 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.76822E+00 6.4E-05  2.77316E+00 6.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06641E+02 9.0E-06  2.07009E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.46292E-08 0.00040  2.60078E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29699E-01 0.00011  1.50388E+00 0.00054 ];
INF_SCATT1                (idx, [1:   4]) = [  2.44118E-01 0.00020  3.97818E-01 0.00056 ];
INF_SCATT2                (idx, [1:   4]) = [  9.63347E-02 0.00018  9.37116E-02 0.00072 ];
INF_SCATT3                (idx, [1:   4]) = [  7.28397E-03 0.00284  2.80990E-02 0.00211 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03569E-02 0.00208 -8.92646E-03 0.00654 ];
INF_SCATT5                (idx, [1:   4]) = [  1.85574E-04 0.08876  6.75283E-03 0.00777 ];
INF_SCATT6                (idx, [1:   4]) = [  5.15888E-03 0.00385 -1.72030E-02 0.00326 ];
INF_SCATT7                (idx, [1:   4]) = [  7.77093E-04 0.01558  5.08717E-04 0.07601 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29743E-01 0.00011  1.50388E+00 0.00054 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.44119E-01 0.00020  3.97818E-01 0.00056 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.63351E-02 0.00018  9.37116E-02 0.00072 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.28418E-03 0.00283  2.80990E-02 0.00211 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03569E-02 0.00208 -8.92646E-03 0.00654 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.85702E-04 0.08852  6.75283E-03 0.00777 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.15903E-03 0.00385 -1.72030E-02 0.00326 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.77217E-04 0.01556  5.08717E-04 0.07601 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12512E-01 0.00034  1.02148E+00 0.00042 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56854E+00 0.00034  3.26324E-01 0.00042 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.02357E-02 0.00062  6.46376E-02 0.00124 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69977E-02 0.00022  6.57701E-02 0.00135 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12977E-01 0.00010  1.67216E-02 0.00055  1.12159E-03 0.00419  1.50276E+00 0.00054 ];
INF_S1                    (idx, [1:   8]) = [  2.39304E-01 0.00019  4.81438E-03 0.00112  4.88099E-04 0.00756  3.97330E-01 0.00056 ];
INF_S2                    (idx, [1:   8]) = [  9.78779E-02 0.00016 -1.54317E-03 0.00201  2.64969E-04 0.01127  9.34466E-02 0.00072 ];
INF_S3                    (idx, [1:   8]) = [  9.01923E-03 0.00217 -1.73525E-03 0.00176  9.44363E-05 0.02278  2.80046E-02 0.00213 ];
INF_S4                    (idx, [1:   8]) = [ -9.82768E-03 0.00207 -5.29194E-04 0.00567  6.49095E-07 1.00000 -8.92711E-03 0.00659 ];
INF_S5                    (idx, [1:   8]) = [  1.38979E-04 0.11613  4.65954E-05 0.04502 -3.77269E-05 0.03946  6.79056E-03 0.00778 ];
INF_S6                    (idx, [1:   8]) = [  5.28804E-03 0.00378 -1.29163E-04 0.02233 -4.97183E-05 0.02399 -1.71533E-02 0.00326 ];
INF_S7                    (idx, [1:   8]) = [  9.38316E-04 0.01354 -1.61223E-04 0.01356 -4.43283E-05 0.03763  5.53045E-04 0.06963 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.13022E-01 0.00010  1.67216E-02 0.00055  1.12159E-03 0.00419  1.50276E+00 0.00054 ];
INF_SP1                   (idx, [1:   8]) = [  2.39304E-01 0.00019  4.81438E-03 0.00112  4.88099E-04 0.00756  3.97330E-01 0.00056 ];
INF_SP2                   (idx, [1:   8]) = [  9.78783E-02 0.00016 -1.54317E-03 0.00201  2.64969E-04 0.01127  9.34466E-02 0.00072 ];
INF_SP3                   (idx, [1:   8]) = [  9.01943E-03 0.00216 -1.73525E-03 0.00176  9.44363E-05 0.02278  2.80046E-02 0.00213 ];
INF_SP4                   (idx, [1:   8]) = [ -9.82766E-03 0.00207 -5.29194E-04 0.00567  6.49095E-07 1.00000 -8.92711E-03 0.00659 ];
INF_SP5                   (idx, [1:   8]) = [  1.39107E-04 0.11579  4.65954E-05 0.04502 -3.77269E-05 0.03946  6.79056E-03 0.00778 ];
INF_SP6                   (idx, [1:   8]) = [  5.28819E-03 0.00378 -1.29163E-04 0.02233 -4.97183E-05 0.02399 -1.71533E-02 0.00326 ];
INF_SP7                   (idx, [1:   8]) = [  9.38440E-04 0.01352 -1.61223E-04 0.01356 -4.43283E-05 0.03763  5.53045E-04 0.06963 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31942E-01 0.00044  1.22148E+00 0.00650 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33464E-01 0.00105  1.32421E+00 0.00828 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33752E-01 0.00070  1.34200E+00 0.00866 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28690E-01 0.00092  1.04809E+00 0.00804 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43715E+00 0.00044  2.73171E-01 0.00655 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42781E+00 0.00105  2.52139E-01 0.00835 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42603E+00 0.00070  2.48840E-01 0.00883 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45761E+00 0.00093  3.18535E-01 0.00808 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.76215E-03 0.00950  1.12584E-04 0.06816  8.89740E-04 0.02277  7.22712E-04 0.02424  2.01728E-03 0.01635  7.93943E-04 0.02507  2.25892E-04 0.04455 ];
LAMBDA                    (idx, [1:  14]) = [  7.54741E-01 0.02296  1.26658E-02 0.00167  3.04888E-02 0.00060  1.11495E-01 0.00094  3.23488E-01 0.00064  1.24101E+00 0.00367  7.97795E+00 0.01415 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0006' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:46 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:45:37 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284846 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.91830E-01  1.00544E+00  1.00484E+00  1.00516E+00  9.92721E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.17541E-02 0.00114  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88246E-01 1.4E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04400E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04835E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69137E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.46557E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.46468E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.40082E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.24169E-01 0.00113  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000915 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00018E+04 0.00104 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00018E+04 0.00104 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  6.21912E+02 ;
RUNNING_TIME              (idx, 1)        =  1.24863E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.32900E-01  4.32900E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.84917E-01  1.28167E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.22914E+02  3.35033E+00  2.59818E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.11873E+00  2.82500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  6.52166E-02  7.83332E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.24863E+02  1.24863E+02 ];
CPU_USAGE                 (idx, 1)        = 4.98074 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00038E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81883E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.56;
MEMSIZE                   (idx, 1)        = 6041.33;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 171.08;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.23;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  1.00005E+17 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.69369E+04 ;
TOT_SF_RATE               (idx, 1)        =  6.39817E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.33781E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.65772E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.66261E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.52788E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  8.16681E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.39178E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  7.19041E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.11279E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  9.76403E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.27899E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.35152E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.47999E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.65118E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.69914E+14 ;
CS134_ACTIVITY            (idx, 1)        =  9.88879E+13 ;
CS137_ACTIVITY            (idx, 1)        =  5.31302E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.73354E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.15965E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.16884E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.40918E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.81503E+12 0.00053  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 20 ;
BURNUP                     (idx, [1:  2])  = [  4.00000E+01  4.00038E+01 ];
BURN_DAYS                 (idx, 1)        =  1.00000E+03 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  8.50850E-01 0.00112 ];
U235_FISS                 (idx, [1:   4]) = [  2.60677E+15 0.00199  1.92512E-01 0.00183 ];
U238_FISS                 (idx, [1:   4]) = [  1.31271E+15 0.00264  9.69355E-02 0.00244 ];
PU239_FISS                (idx, [1:   4]) = [  7.25300E+15 0.00106  5.35666E-01 0.00084 ];
PU240_FISS                (idx, [1:   4]) = [  2.16264E+13 0.02059  1.59623E-03 0.02050 ];
PU241_FISS                (idx, [1:   4]) = [  2.28609E+15 0.00196  1.68835E-01 0.00183 ];
U235_CAPT                 (idx, [1:   4]) = [  5.83535E+14 0.00431  1.70319E-02 0.00424 ];
U238_CAPT                 (idx, [1:   4]) = [  1.13181E+16 0.00104  3.30357E-01 0.00075 ];
PU239_CAPT                (idx, [1:   4]) = [  3.96534E+15 0.00156  1.15757E-01 0.00156 ];
PU240_CAPT                (idx, [1:   4]) = [  3.44703E+15 0.00163  1.00613E-01 0.00146 ];
PU241_CAPT                (idx, [1:   4]) = [  8.18344E+14 0.00347  2.38888E-02 0.00346 ];
XE135_CAPT                (idx, [1:   4]) = [  8.12723E+14 0.00348  2.37254E-02 0.00348 ];
SM149_CAPT                (idx, [1:   4]) = [  2.72440E+14 0.00575  7.95377E-03 0.00578 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000915 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.22585E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000915 5.00823E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3552163 3.55749E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1404184 1.40616E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44568 4.45764E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000915 5.00823E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.35276E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50057E+05 0.0E+00  4.50057E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.78705E+16 2.0E-05  3.78705E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35530E+16 4.1E-06  1.35530E+16 4.1E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.42673E+16 0.00047  2.69727E+16 0.00047  7.29462E+15 0.00129 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.78203E+16 0.00034  4.05257E+16 0.00032  7.29462E+15 0.00129 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.81503E+16 0.00053  4.81503E+16 0.00053  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.24249E+18 0.00053  5.96529E+17 0.00048  1.64597E+18 0.00060 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.29290E+14 0.00490 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.82496E+16 0.00034 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.67076E+18 0.00071 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.07885E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.07885E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.36236E+00 0.00068 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.75102E-01 0.00039 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.08506E-01 0.00037 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23421E+00 0.00042 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94183E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96884E-01 2.7E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  7.92974E-01 0.00072 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  7.85905E-01 0.00072 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.79424E+00 2.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.07263E+02 4.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  7.86054E-01 0.00073  7.82240E-01 0.00072  3.66417E-03 0.01219 ];
IMP_KEFF                  (idx, [1:   2]) = [  7.86235E-01 0.00034 ];
COL_KEFF                  (idx, [1:   2]) = [  7.86616E-01 0.00053 ];
ABS_KEFF                  (idx, [1:   2]) = [  7.86235E-01 0.00034 ];
ABS_KINF                  (idx, [1:   2]) = [  7.93304E-01 0.00034 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.68189E+01 0.00028 ];
IMP_ALF                   (idx, [1:   2]) = [  1.68196E+01 0.00010 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  9.97757E-07 0.00464 ];
IMP_EALF                  (idx, [1:   2]) = [  9.92423E-07 0.00174 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.31256E-01 0.00275 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.29588E-01 0.00109 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.11878E-03 0.00672  1.47288E-04 0.04171  1.14475E-03 0.01536  9.41315E-04 0.01662  2.54265E-03 0.01026  1.05754E-03 0.01652  2.85246E-04 0.02866 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.26651E-01 0.01519  8.76041E-03 0.02991  3.03686E-02 0.00037  1.11859E-01 0.00067  3.23537E-01 0.00045  1.22223E+00 0.00279  6.94713E+00 0.02013 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.61735E-03 0.01057  1.17880E-04 0.06640  8.74342E-04 0.02318  7.05723E-04 0.02698  1.89518E-03 0.01598  7.97952E-04 0.02452  2.26273E-04 0.04423 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.56429E-01 0.02408  1.26546E-02 0.00162  3.03677E-02 0.00056  1.11818E-01 0.00095  3.23846E-01 0.00069  1.22614E+00 0.00400  7.72232E+00 0.01564 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.25315E-05 0.00157  3.25210E-05 0.00157  3.46950E-05 0.01974 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.55657E-05 0.00144  2.55574E-05 0.00144  2.72783E-05 0.01985 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.67314E-03 0.01229  1.15810E-04 0.07858  8.59166E-04 0.03002  7.09081E-04 0.03281  1.93396E-03 0.01811  8.15727E-04 0.03160  2.39398E-04 0.05415 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.75826E-01 0.03081  1.26595E-02 0.00262  3.03925E-02 0.00082  1.11709E-01 0.00135  3.23691E-01 0.00093  1.22587E+00 0.00556  7.78620E+00 0.02200 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  3.17734E-05 0.00390  3.17517E-05 0.00391  3.03810E-05 0.05094 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.49681E-05 0.00381  2.49511E-05 0.00382  2.38723E-05 0.05083 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.34943E-03 0.04463  8.49767E-05 0.28706  7.93566E-04 0.09989  6.52640E-04 0.10097  1.80924E-03 0.07050  7.59609E-04 0.11606  2.49404E-04 0.19495 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.73267E-01 0.09787  1.27519E-02 0.00831  3.04166E-02 0.00200  1.11795E-01 0.00295  3.23833E-01 0.00230  1.22782E+00 0.01372  7.57561E+00 0.05775 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.35029E-03 0.04402  8.39699E-05 0.27804  7.96152E-04 0.09971  6.58720E-04 0.09828  1.80088E-03 0.06967  7.59922E-04 0.11403  2.50648E-04 0.19242 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.88757E-01 0.09651  1.27519E-02 0.00831  3.04188E-02 0.00200  1.11783E-01 0.00295  3.23738E-01 0.00228  1.22720E+00 0.01369  7.55768E+00 0.05820 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.37652E+02 0.04484 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.22030E-05 0.00105 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.53069E-05 0.00078 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.61574E-03 0.00831 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.43374E+02 0.00835 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.63526E-07 0.00080 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.59843E-06 0.00043  2.59828E-06 0.00044  2.63464E-06 0.00636 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.99497E-05 0.00096  3.99679E-05 0.00097  3.61803E-05 0.01252 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.06694E-01 0.00036  6.07412E-01 0.00037  5.01680E-01 0.01087 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.10053E+01 0.01563 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.46468E+01 0.00051  3.31651E+01 0.00062 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.22721E+04 0.00276  3.06217E+05 0.00142  6.10729E+05 0.00081  6.50398E+05 0.00060  5.98737E+05 0.00086  6.41782E+05 0.00045  4.35734E+05 0.00083  3.85916E+05 0.00063  2.95501E+05 0.00105  2.41657E+05 0.00057  2.08153E+05 0.00083  1.87678E+05 0.00076  1.73111E+05 0.00106  1.64887E+05 0.00066  1.60634E+05 0.00107  1.38562E+05 0.00099  1.36999E+05 0.00098  1.35865E+05 0.00086  1.33551E+05 0.00075  2.60182E+05 0.00072  2.51942E+05 0.00072  1.81564E+05 0.00073  1.17931E+05 0.00087  1.35166E+05 0.00103  1.29510E+05 0.00089  1.14142E+05 0.00094  1.83622E+05 0.00065  4.26925E+04 0.00160  5.17950E+04 0.00129  4.74590E+04 0.00114  2.80609E+04 0.00222  4.80284E+04 0.00151  3.10608E+04 0.00186  2.49115E+04 0.00152  4.09369E+03 0.00370  3.41589E+03 0.00367  3.00462E+03 0.00365  2.91198E+03 0.00390  2.93961E+03 0.00413  3.18751E+03 0.00380  3.90675E+03 0.00363  4.07548E+03 0.00319  8.21194E+03 0.00248  1.37634E+04 0.00221  1.81157E+04 0.00258  4.86381E+04 0.00140  5.12302E+04 0.00128  5.48540E+04 0.00151  3.56839E+04 0.00154  2.50846E+04 0.00154  1.85342E+04 0.00223  2.18587E+04 0.00206  4.29112E+04 0.00136  6.11738E+04 0.00183  1.23316E+05 0.00167  1.96649E+05 0.00178  3.01266E+05 0.00168  1.95385E+05 0.00188  1.40960E+05 0.00163  1.01869E+05 0.00197  9.19207E+04 0.00203  9.07957E+04 0.00158  7.60646E+04 0.00176  5.15767E+04 0.00220  4.77585E+04 0.00198  4.26714E+04 0.00198  3.60273E+04 0.00220  2.84876E+04 0.00230  1.91183E+04 0.00288  6.79003E+03 0.00256 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  7.93692E-01 0.00049 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.79425E+18 0.00043  4.48276E+17 0.00162 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.40202E-01 8.8E-05  1.56491E+00 0.00049 ];
INF_CAPT                  (idx, [1:   4]) = [  9.02279E-03 0.00058  4.03329E-02 0.00099 ];
INF_ABS                   (idx, [1:   4]) = [  1.04617E-02 0.00051  6.48127E-02 0.00122 ];
INF_FISS                  (idx, [1:   4]) = [  1.43894E-03 0.00037  2.44798E-02 0.00163 ];
INF_NSF                   (idx, [1:   4]) = [  4.00932E-03 0.00035  6.84485E-02 0.00167 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.78629E+00 6.8E-05  2.79611E+00 5.8E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06892E+02 8.1E-06  2.07350E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.42490E-08 0.00038  2.60211E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29745E-01 9.1E-05  1.50012E+00 0.00057 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43844E-01 0.00019  3.96314E-01 0.00064 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62473E-02 0.00019  9.32582E-02 0.00125 ];
INF_SCATT3                (idx, [1:   4]) = [  7.22270E-03 0.00220  2.79523E-02 0.00243 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03530E-02 0.00172 -9.00805E-03 0.00635 ];
INF_SCATT5                (idx, [1:   4]) = [  1.73154E-04 0.07544  6.62173E-03 0.00969 ];
INF_SCATT6                (idx, [1:   4]) = [  5.12510E-03 0.00283 -1.72154E-02 0.00296 ];
INF_SCATT7                (idx, [1:   4]) = [  7.30053E-04 0.01939  4.68662E-04 0.11950 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29789E-01 9.1E-05  1.50012E+00 0.00057 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43845E-01 0.00019  3.96314E-01 0.00064 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62475E-02 0.00019  9.32582E-02 0.00125 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.22251E-03 0.00220  2.79523E-02 0.00243 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03532E-02 0.00171 -9.00805E-03 0.00635 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.73247E-04 0.07543  6.62173E-03 0.00969 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.12520E-03 0.00283 -1.72154E-02 0.00296 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.29786E-04 0.01942  4.68662E-04 0.11950 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12810E-01 0.00032  1.02052E+00 0.00040 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56635E+00 0.00033  3.26631E-01 0.00040 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.04176E-02 0.00052  6.48127E-02 0.00122 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70082E-02 0.00018  6.59195E-02 0.00137 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.13194E-01 8.9E-05  1.65517E-02 0.00043  1.13804E-03 0.00339  1.49899E+00 0.00057 ];
INF_S1                    (idx, [1:   8]) = [  2.39082E-01 0.00017  4.76202E-03 0.00122  4.91600E-04 0.00639  3.95823E-01 0.00064 ];
INF_S2                    (idx, [1:   8]) = [  9.77746E-02 0.00018 -1.52728E-03 0.00261  2.69117E-04 0.00862  9.29891E-02 0.00125 ];
INF_S3                    (idx, [1:   8]) = [  8.93635E-03 0.00176 -1.71365E-03 0.00192  9.57635E-05 0.01492  2.78566E-02 0.00245 ];
INF_S4                    (idx, [1:   8]) = [ -9.83654E-03 0.00180 -5.16417E-04 0.00714 -2.61118E-07 1.00000 -9.00779E-03 0.00635 ];
INF_S5                    (idx, [1:   8]) = [  1.23357E-04 0.10093  4.97973E-05 0.05308 -3.76741E-05 0.04347  6.65940E-03 0.00963 ];
INF_S6                    (idx, [1:   8]) = [  5.25189E-03 0.00276 -1.26788E-04 0.01666 -4.83421E-05 0.03017 -1.71671E-02 0.00295 ];
INF_S7                    (idx, [1:   8]) = [  8.93547E-04 0.01567 -1.63493E-04 0.01565 -4.30778E-05 0.03383  5.11740E-04 0.10893 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.13238E-01 8.8E-05  1.65517E-02 0.00043  1.13804E-03 0.00339  1.49899E+00 0.00057 ];
INF_SP1                   (idx, [1:   8]) = [  2.39083E-01 0.00017  4.76202E-03 0.00122  4.91600E-04 0.00639  3.95823E-01 0.00064 ];
INF_SP2                   (idx, [1:   8]) = [  9.77748E-02 0.00018 -1.52728E-03 0.00261  2.69117E-04 0.00862  9.29891E-02 0.00125 ];
INF_SP3                   (idx, [1:   8]) = [  8.93616E-03 0.00176 -1.71365E-03 0.00192  9.57635E-05 0.01492  2.78566E-02 0.00245 ];
INF_SP4                   (idx, [1:   8]) = [ -9.83674E-03 0.00180 -5.16417E-04 0.00714 -2.61118E-07 1.00000 -9.00779E-03 0.00635 ];
INF_SP5                   (idx, [1:   8]) = [  1.23450E-04 0.10092  4.97973E-05 0.05308 -3.76741E-05 0.04347  6.65940E-03 0.00963 ];
INF_SP6                   (idx, [1:   8]) = [  5.25199E-03 0.00276 -1.26788E-04 0.01666 -4.83421E-05 0.03017 -1.71671E-02 0.00295 ];
INF_SP7                   (idx, [1:   8]) = [  8.93279E-04 0.01569 -1.63493E-04 0.01565 -4.30778E-05 0.03383  5.11740E-04 0.10893 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31999E-01 0.00051  1.20411E+00 0.00596 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33393E-01 0.00088  1.32148E+00 0.00966 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33947E-01 0.00085  1.30710E+00 0.00879 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28735E-01 0.00083  1.03326E+00 0.00629 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43680E+00 0.00051  2.77067E-01 0.00600 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42823E+00 0.00088  2.52799E-01 0.00951 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42485E+00 0.00085  2.55487E-01 0.00873 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45732E+00 0.00083  3.22915E-01 0.00637 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.61735E-03 0.01057  1.17880E-04 0.06640  8.74342E-04 0.02318  7.05723E-04 0.02698  1.89518E-03 0.01598  7.97952E-04 0.02452  2.26273E-04 0.04423 ];
LAMBDA                    (idx, [1:  14]) = [  7.56429E-01 0.02408  1.26546E-02 0.00162  3.03677E-02 0.00056  1.11818E-01 0.00095  3.23846E-01 0.00069  1.22614E+00 0.00400  7.72232E+00 0.01564 ];

