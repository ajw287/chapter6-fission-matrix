
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0009' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Mon Jul 20 23:44:37 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284850 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00450E+00  9.98461E-01  1.00029E+00  9.99066E-01  9.97683E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15453E-02 0.00101  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88455E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.93714E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.94153E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.71608E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.50995E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.50928E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  9.09573E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.27647E-01 0.00105  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000699 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00014E+04 0.00067 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00014E+04 0.00067 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.70718E+01 ;
RUNNING_TIME              (idx, 1)        =  3.76997E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  4.43100E-01  4.43100E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.28333E-03  1.28333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.32535E+00  3.32535E+00  0.00000E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.76962E+00  0.00000E+00 ];
CPU_USAGE                 (idx, 1)        = 4.52836 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00053E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  8.69950E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.66321E+08 ;
TOT_DECAY_HEAT            (idx, 1)        =  6.59896E-04 ;
TOT_SF_RATE               (idx, 1)        =  7.40202E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  8.66321E+08 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  6.59896E-04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  7.92612E+03 ;
INGESTION_TOXICITY        (idx, 1)        =  4.18447E+01 ;
ACTINIDE_INH_TOX          (idx, 1)        =  7.92612E+03 ;
ACTINIDE_ING_TOX          (idx, 1)        =  4.18447E+01 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  1.13637E+08 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  8.65912E+08 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  3.58418E+08 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.87386E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 0 ;
BURNUP                     (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
BURN_DAYS                 (idx, 1)        =  0.00000E+00 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.67788E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  1.30626E+16 0.00054  9.41785E-01 0.00016 ];
U238_FISS                 (idx, [1:   4]) = [  8.06258E+14 0.00280  5.81221E-02 0.00266 ];
U235_CAPT                 (idx, [1:   4]) = [  2.82569E+15 0.00139  1.91999E-01 0.00124 ];
U238_CAPT                 (idx, [1:   4]) = [  7.38038E+15 0.00101  5.01448E-01 0.00064 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000699 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.32855E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000699 5.00733E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2557141 2.56057E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2410067 2.41326E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 33491 3.34946E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000699 5.00733E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.33299E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 0.0E+00  4.50056E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.41009E+16 1.0E-05  3.41009E+16 1.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38687E+16 1.2E-06  1.38687E+16 1.2E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.47208E+16 0.00047  1.03132E+16 0.00052  4.40768E+15 0.00100 ];
TOT_ABSRATE               (idx, [1:   6]) = [  2.85895E+16 0.00024  2.41818E+16 0.00022  4.40768E+15 0.00100 ];
TOT_SRCRATE               (idx, [1:   6]) = [  2.87386E+16 0.00050  2.87386E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.36723E+18 0.00045  3.75715E+17 0.00044  9.91517E+17 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.92540E+14 0.00579 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  2.87821E+16 0.00025 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.00988E+18 0.00055 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12514E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.81712E+00 0.00035 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.93085E-01 0.00029 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.66148E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24469E+00 0.00031 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95623E-01 3.1E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97667E-01 2.2E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.19486E+00 0.00046 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.18685E+00 0.00046 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.45884E+00 1.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02545E+02 1.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.18677E+00 0.00048  1.17869E+00 0.00046  8.15902E-03 0.00791 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.18657E+00 0.00024 ];
COL_KEFF                  (idx, [1:   2]) = [  1.18674E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.18657E+00 0.00024 ];
ABS_KINF                  (idx, [1:   2]) = [  1.19457E+00 0.00024 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75635E+01 0.00018 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75706E+01 7.6E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.72399E-07 0.00308 ];
IMP_EALF                  (idx, [1:   2]) = [  4.68182E-07 0.00134 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.98585E-01 0.00290 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.97098E-01 0.00112 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.92067E-03 0.00539  1.73478E-04 0.03011  9.27541E-04 0.01368  9.08536E-04 0.01426  2.74861E-03 0.00750  8.73200E-04 0.01327  2.89308E-04 0.02448 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.00699E-01 0.01259  1.09919E-02 0.01653  3.15928E-02 0.00202  1.10148E-01 0.00027  3.20404E-01 0.00021  1.34574E+00 0.00017  8.48157E+00 0.00973 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.95782E-03 0.00764  2.02275E-04 0.04694  1.07860E-03 0.01979  1.05662E-03 0.02033  3.24896E-03 0.01148  1.01736E-03 0.01913  3.54016E-04 0.03603 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.17126E-01 0.01857  1.24908E-02 3.0E-06  3.16449E-02 0.00033  1.10178E-01 0.00040  3.20453E-01 0.00031  1.34611E+00 0.00023  8.85334E+00 0.00182 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.33792E-05 0.00102  2.33684E-05 0.00103  2.50141E-05 0.01104 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.77423E-05 0.00089  2.77295E-05 0.00090  2.96819E-05 0.01101 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.87786E-03 0.00796  1.93821E-04 0.04718  1.07147E-03 0.02010  1.06286E-03 0.02013  3.18964E-03 0.01137  1.02627E-03 0.01907  3.33795E-04 0.03643 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.00302E-01 0.01863  1.24907E-02 3.1E-06  3.16550E-02 0.00035  1.10167E-01 0.00043  3.20524E-01 0.00031  1.34556E+00 0.00026  8.87077E+00 0.00219 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.30843E-05 0.00229  2.30706E-05 0.00230  2.48999E-05 0.02330 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.73937E-05 0.00228  2.73773E-05 0.00229  2.95564E-05 0.02332 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.96393E-03 0.02421  2.12089E-04 0.14709  1.14194E-03 0.05962  1.04091E-03 0.05607  3.11199E-03 0.03578  1.05958E-03 0.06597  3.97423E-04 0.10322 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.37333E-01 0.05368  1.24907E-02 7.2E-06  3.16726E-02 0.00073  1.10035E-01 0.00090  3.20434E-01 0.00095  1.34483E+00 0.00061  8.92759E+00 0.00506 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.98122E-03 0.02261  2.10432E-04 0.14157  1.13347E-03 0.05778  1.04783E-03 0.05486  3.13183E-03 0.03407  1.05669E-03 0.06495  4.00964E-04 0.09924 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.55968E-01 0.05263  1.24907E-02 7.0E-06  3.16662E-02 0.00074  1.10050E-01 0.00090  3.20367E-01 0.00092  1.34486E+00 0.00060  8.92689E+00 0.00504 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -3.02550E+02 0.02439 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.32423E-05 0.00068 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.75798E-05 0.00045 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.89350E-03 0.00488 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.96687E+02 0.00495 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.83785E-07 0.00064 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.88710E-06 0.00042  2.88705E-06 0.00042  2.89681E-06 0.00487 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.85344E-05 0.00079  3.85544E-05 0.00079  3.58695E-05 0.00837 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.64447E-01 0.00031  6.63570E-01 0.00032  8.22397E-01 0.00804 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02512E+01 0.01299 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.50928E+01 0.00043  3.48523E+01 0.00041 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.18428E+04 0.00359  2.91375E+05 0.00172  6.03453E+05 0.00102  6.50555E+05 0.00093  6.00394E+05 0.00074  6.46211E+05 0.00078  4.38849E+05 0.00088  3.88224E+05 0.00065  2.97256E+05 0.00067  2.42978E+05 0.00080  2.09215E+05 0.00073  1.88637E+05 0.00104  1.74382E+05 0.00099  1.65327E+05 0.00096  1.61384E+05 0.00083  1.39263E+05 0.00091  1.37735E+05 0.00104  1.36147E+05 0.00090  1.33687E+05 0.00080  2.60985E+05 0.00066  2.50903E+05 0.00068  1.81157E+05 0.00061  1.16944E+05 0.00096  1.34887E+05 0.00074  1.27278E+05 0.00086  1.16071E+05 0.00106  1.90167E+05 0.00071  4.34147E+04 0.00155  5.45235E+04 0.00144  4.95403E+04 0.00162  2.87469E+04 0.00194  5.00005E+04 0.00169  3.39606E+04 0.00207  2.88797E+04 0.00193  5.51506E+03 0.00310  5.44130E+03 0.00358  5.55042E+03 0.00348  5.75321E+03 0.00318  5.73064E+03 0.00273  5.63575E+03 0.00348  5.82951E+03 0.00431  5.49148E+03 0.00357  1.03649E+04 0.00285  1.65367E+04 0.00208  2.10509E+04 0.00181  5.55100E+04 0.00150  5.83713E+04 0.00151  6.36775E+04 0.00125  4.34453E+04 0.00110  3.23465E+04 0.00137  2.49907E+04 0.00151  2.94551E+04 0.00154  5.58371E+04 0.00148  7.51288E+04 0.00140  1.43420E+05 0.00119  2.19048E+05 0.00110  3.24148E+05 0.00097  2.05642E+05 0.00154  1.46664E+05 0.00126  1.05112E+05 0.00148  9.41294E+04 0.00151  9.24324E+04 0.00143  7.69214E+04 0.00136  5.18698E+04 0.00145  4.78176E+04 0.00155  4.24837E+04 0.00151  3.58631E+04 0.00138  2.81714E+04 0.00176  1.88227E+04 0.00165  6.62348E+03 0.00236 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.19474E+00 0.00049 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.07666E+18 0.00057  2.90608E+17 0.00112 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.36779E-01 0.00012  1.48648E+00 0.00032 ];
INF_CAPT                  (idx, [1:   4]) = [  6.45472E-03 0.00055  2.67439E-02 0.00035 ];
INF_ABS                   (idx, [1:   4]) = [  8.88487E-03 0.00043  6.54705E-02 0.00083 ];
INF_FISS                  (idx, [1:   4]) = [  2.43015E-03 0.00050  3.87266E-02 0.00116 ];
INF_NSF                   (idx, [1:   4]) = [  6.20676E-03 0.00051  9.43650E-02 0.00116 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.55406E+00 4.7E-05  2.43670E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03725E+02 4.4E-06  2.02270E+02 0.0E+00 ];
INF_INVV                  (idx, [1:   4]) = [  5.85859E-08 0.00045  2.52968E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.27888E-01 0.00012  1.42105E+00 0.00037 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42619E-01 0.00017  3.76520E-01 0.00051 ];
INF_SCATT2                (idx, [1:   4]) = [  9.57039E-02 0.00032  8.97697E-02 0.00114 ];
INF_SCATT3                (idx, [1:   4]) = [  7.33024E-03 0.00339  2.69799E-02 0.00269 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02339E-02 0.00162 -8.02875E-03 0.00734 ];
INF_SCATT5                (idx, [1:   4]) = [  1.57711E-04 0.10334  6.19484E-03 0.00889 ];
INF_SCATT6                (idx, [1:   4]) = [  5.03665E-03 0.00327 -1.58779E-02 0.00231 ];
INF_SCATT7                (idx, [1:   4]) = [  7.17421E-04 0.02052  2.84821E-04 0.15409 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.27928E-01 0.00012  1.42105E+00 0.00037 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42619E-01 0.00017  3.76520E-01 0.00051 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.57037E-02 0.00032  8.97697E-02 0.00114 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.33003E-03 0.00339  2.69799E-02 0.00269 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02340E-02 0.00162 -8.02875E-03 0.00734 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.57528E-04 0.10359  6.19484E-03 0.00889 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.03677E-03 0.00328 -1.58779E-02 0.00231 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.17332E-04 0.02058  2.84821E-04 0.15409 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.14156E-01 0.00031  9.58740E-01 0.00026 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55650E+00 0.00031  3.47679E-01 0.00026 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.84575E-03 0.00043  6.54705E-02 0.00083 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69317E-02 0.00021  6.66107E-02 0.00094 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.09847E-01 0.00012  1.80409E-02 0.00031  1.17429E-03 0.00338  1.41987E+00 0.00037 ];
INF_S1                    (idx, [1:   8]) = [  2.37348E-01 0.00018  5.27086E-03 0.00072  4.94716E-04 0.00652  3.76025E-01 0.00051 ];
INF_S2                    (idx, [1:   8]) = [  9.72607E-02 0.00032 -1.55687E-03 0.00288  2.72067E-04 0.00872  8.94976E-02 0.00114 ];
INF_S3                    (idx, [1:   8]) = [  9.18459E-03 0.00261 -1.85435E-03 0.00185  9.74025E-05 0.01765  2.68825E-02 0.00271 ];
INF_S4                    (idx, [1:   8]) = [ -9.61551E-03 0.00168 -6.18352E-04 0.00326 -6.54121E-07 1.00000 -8.02809E-03 0.00740 ];
INF_S5                    (idx, [1:   8]) = [  1.40776E-04 0.11059  1.69348E-05 0.11046 -3.93171E-05 0.04212  6.23416E-03 0.00885 ];
INF_S6                    (idx, [1:   8]) = [  5.18063E-03 0.00322 -1.43977E-04 0.01529 -4.74941E-05 0.02586 -1.58304E-02 0.00233 ];
INF_S7                    (idx, [1:   8]) = [  8.87759E-04 0.01716 -1.70338E-04 0.01188 -4.45409E-05 0.02127  3.29362E-04 0.13270 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.09887E-01 0.00012  1.80409E-02 0.00031  1.17429E-03 0.00338  1.41987E+00 0.00037 ];
INF_SP1                   (idx, [1:   8]) = [  2.37348E-01 0.00018  5.27086E-03 0.00072  4.94716E-04 0.00652  3.76025E-01 0.00051 ];
INF_SP2                   (idx, [1:   8]) = [  9.72605E-02 0.00032 -1.55687E-03 0.00288  2.72067E-04 0.00872  8.94976E-02 0.00114 ];
INF_SP3                   (idx, [1:   8]) = [  9.18439E-03 0.00262 -1.85435E-03 0.00185  9.74025E-05 0.01765  2.68825E-02 0.00271 ];
INF_SP4                   (idx, [1:   8]) = [ -9.61560E-03 0.00168 -6.18352E-04 0.00326 -6.54121E-07 1.00000 -8.02809E-03 0.00740 ];
INF_SP5                   (idx, [1:   8]) = [  1.40593E-04 0.11092  1.69348E-05 0.11046 -3.93171E-05 0.04212  6.23416E-03 0.00885 ];
INF_SP6                   (idx, [1:   8]) = [  5.18075E-03 0.00323 -1.43977E-04 0.01529 -4.74941E-05 0.02586 -1.58304E-02 0.00233 ];
INF_SP7                   (idx, [1:   8]) = [  8.87669E-04 0.01720 -1.70338E-04 0.01188 -4.45409E-05 0.02127  3.29362E-04 0.13270 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32355E-01 0.00064  1.03834E+00 0.00709 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33795E-01 0.00100  1.09847E+00 0.00928 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33596E-01 0.00089  1.09219E+00 0.00797 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29727E-01 0.00078  9.41519E-01 0.00738 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43460E+00 0.00064  3.21414E-01 0.00709 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42579E+00 0.00100  3.04077E-01 0.00925 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42699E+00 0.00090  3.05661E-01 0.00796 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45102E+00 0.00078  3.54505E-01 0.00744 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.95782E-03 0.00764  2.02275E-04 0.04694  1.07860E-03 0.01979  1.05662E-03 0.02033  3.24896E-03 0.01148  1.01736E-03 0.01913  3.54016E-04 0.03603 ];
LAMBDA                    (idx, [1:  14]) = [  8.17126E-01 0.01857  1.24908E-02 3.0E-06  3.16449E-02 0.00033  1.10178E-01 0.00040  3.20453E-01 0.00031  1.34611E+00 0.00023  8.85334E+00 0.00182 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0009' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Mon Jul 20 23:50:30 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284850 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00391E+00  9.98204E-01  9.98168E-01  1.00108E+00  9.98643E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.6E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15299E-02 0.00101  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88470E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.95612E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.96053E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.70760E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.48497E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.48429E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.92624E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.21883E-01 0.00104  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000755 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00015E+04 0.00067 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00015E+04 0.00067 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.64821E+01 ;
RUNNING_TIME              (idx, 1)        =  9.66483E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  4.43100E-01  4.43100E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.46000E-02  6.78333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.11708E+00  3.24303E+00  2.54870E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  7.92167E-02  2.90167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.80000E-03  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.66467E+00  1.22632E+02 ];
CPU_USAGE                 (idx, 1)        = 4.80940 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00054E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.40280E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  7.82311E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.75054E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.40187E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.13808E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  8.07024E+02 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  6.68501E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.66982E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.19702E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  1.73485E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.12443E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  3.30209E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.84580E+06 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  1.40464E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.28454E+11 ;
TE132_ACTIVITY            (idx, 1)        =  2.51385E+14 ;
I131_ACTIVITY             (idx, 1)        =  7.42224E+13 ;
I132_ACTIVITY             (idx, 1)        =  2.43546E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.67853E+07 ;
CS137_ACTIVITY            (idx, 1)        =  1.34696E+11 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  6.51022E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.64737E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.29105E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  8.91792E+16 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.98325E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 1 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E-01  1.00009E-01 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+00 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.80634E-01 0.00109 ];
U235_FISS                 (idx, [1:   4]) = [  1.30112E+16 0.00053  9.38393E-01 0.00017 ];
U238_FISS                 (idx, [1:   4]) = [  8.31661E+14 0.00274  5.99732E-02 0.00260 ];
PU239_FISS                (idx, [1:   4]) = [  2.08796E+13 0.01671  1.50582E-03 0.01670 ];
U235_CAPT                 (idx, [1:   4]) = [  2.84298E+15 0.00134  1.79856E-01 0.00123 ];
U238_CAPT                 (idx, [1:   4]) = [  7.58284E+15 0.00106  4.79667E-01 0.00065 ];
PU239_CAPT                (idx, [1:   4]) = [  1.20702E+13 0.02253  7.63994E-04 0.02257 ];
PU240_CAPT                (idx, [1:   4]) = [  1.44216E+11 0.19940  9.08213E-06 0.19940 ];
XE135_CAPT                (idx, [1:   4]) = [  7.00423E+14 0.00295  4.43152E-02 0.00297 ];
SM149_CAPT                (idx, [1:   4]) = [  1.96259E+13 0.01717  1.24178E-03 0.01719 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000755 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.31968E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000755 5.00732E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2645902 2.64936E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2320912 2.32401E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 33941 3.39455E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000755 5.00732E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.12227E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 0.0E+00  4.50056E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.41182E+16 1.1E-05  3.41182E+16 1.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38674E+16 1.3E-06  1.38674E+16 1.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.57906E+16 0.00045  1.12848E+16 0.00047  4.50576E+15 0.00103 ];
TOT_ABSRATE               (idx, [1:   6]) = [  2.96580E+16 0.00024  2.51523E+16 0.00021  4.50576E+15 0.00103 ];
TOT_SRCRATE               (idx, [1:   6]) = [  2.98325E+16 0.00049  2.98325E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.41240E+18 0.00045  3.87109E+17 0.00044  1.02529E+18 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.02583E+14 0.00560 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  2.98606E+16 0.00025 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.04083E+18 0.00056 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12502E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12502E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.73014E+00 0.00039 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.96292E-01 0.00029 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.66329E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25433E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95576E-01 2.9E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97624E-01 2.2E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.15140E+00 0.00048 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.14358E+00 0.00048 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46032E+00 1.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02563E+02 1.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.14361E+00 0.00049  1.13566E+00 0.00049  7.92047E-03 0.00813 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.14429E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.14380E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.14429E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.15212E+00 0.00024 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74937E+01 0.00018 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74927E+01 7.5E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.06692E-07 0.00322 ];
IMP_EALF                  (idx, [1:   2]) = [  5.06109E-07 0.00131 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.04825E-01 0.00278 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.04355E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.20901E-03 0.00535  1.78146E-04 0.03329  9.84990E-04 0.01370  9.50392E-04 0.01382  2.83554E-03 0.00801  9.45329E-04 0.01380  3.14615E-04 0.02419 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.24418E-01 0.01308  1.06671E-02 0.01851  3.16516E-02 0.00023  1.10160E-01 0.00028  3.20591E-01 0.00021  1.34586E+00 0.00016  8.57742E+00 0.00855 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.09273E-03 0.00790  2.00650E-04 0.05234  1.13069E-03 0.02139  1.08781E-03 0.01928  3.24788E-03 0.01206  1.08011E-03 0.02008  3.45601E-04 0.03481 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.05355E-01 0.01789  1.24907E-02 3.2E-06  3.16557E-02 0.00030  1.10153E-01 0.00040  3.20623E-01 0.00032  1.34589E+00 0.00022  8.86367E+00 0.00190 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.32778E-05 0.00106  2.32659E-05 0.00106  2.50793E-05 0.01101 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.66180E-05 0.00097  2.66044E-05 0.00097  2.86772E-05 0.01100 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.92188E-03 0.00820  2.01002E-04 0.04862  1.09627E-03 0.02038  1.07035E-03 0.02157  3.18465E-03 0.01173  1.03101E-03 0.02097  3.38603E-04 0.03606 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.02148E-01 0.01900  1.24908E-02 4.2E-06  3.16559E-02 0.00035  1.10156E-01 0.00044  3.20750E-01 0.00034  1.34622E+00 0.00025  8.87435E+00 0.00228 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.29373E-05 0.00257  2.29258E-05 0.00259  2.42385E-05 0.02648 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.62284E-05 0.00253  2.62152E-05 0.00254  2.77300E-05 0.02653 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.03453E-03 0.02524  1.86352E-04 0.14108  1.18046E-03 0.05919  1.02772E-03 0.07036  3.21142E-03 0.03587  1.05978E-03 0.06535  3.68801E-04 0.11066 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.37568E-01 0.05857  1.24907E-02 7.4E-06  3.16197E-02 0.00087  1.10339E-01 0.00117  3.20660E-01 0.00099  1.34579E+00 0.00062  8.92510E+00 0.00545 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.97770E-03 0.02484  1.76566E-04 0.14210  1.17078E-03 0.05745  1.04673E-03 0.06757  3.17598E-03 0.03484  1.04843E-03 0.06238  3.59231E-04 0.10913 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.25272E-01 0.05698  1.24907E-02 7.4E-06  3.16222E-02 0.00087  1.10333E-01 0.00115  3.20722E-01 0.00099  1.34590E+00 0.00061  8.92181E+00 0.00542 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -3.07975E+02 0.02552 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.30982E-05 0.00070 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.64122E-05 0.00049 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.96051E-03 0.00461 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -3.01412E+02 0.00464 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.74178E-07 0.00065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.88360E-06 0.00042  2.88317E-06 0.00042  2.94235E-06 0.00493 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.76259E-05 0.00079  3.76410E-05 0.00080  3.55921E-05 0.00795 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.64620E-01 0.00034  6.63893E-01 0.00034  7.92668E-01 0.00868 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.00954E+01 0.01368 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.48429E+01 0.00045  3.42785E+01 0.00041 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.16052E+04 0.00292  2.91527E+05 0.00144  6.03233E+05 0.00090  6.50240E+05 0.00091  6.00389E+05 0.00091  6.46082E+05 0.00074  4.38203E+05 0.00080  3.87851E+05 0.00061  2.96675E+05 0.00084  2.42694E+05 0.00065  2.09220E+05 0.00081  1.88188E+05 0.00079  1.73969E+05 0.00070  1.65517E+05 0.00080  1.61314E+05 0.00074  1.39110E+05 0.00093  1.37679E+05 0.00114  1.36304E+05 0.00103  1.33867E+05 0.00102  2.60730E+05 0.00060  2.51063E+05 0.00057  1.81243E+05 0.00083  1.17050E+05 0.00107  1.34848E+05 0.00084  1.27155E+05 0.00092  1.15842E+05 0.00105  1.90051E+05 0.00083  4.33901E+04 0.00150  5.44287E+04 0.00140  4.94613E+04 0.00156  2.87785E+04 0.00171  5.00244E+04 0.00154  3.39900E+04 0.00147  2.88660E+04 0.00189  5.50017E+03 0.00352  5.47865E+03 0.00358  5.54353E+03 0.00400  5.73153E+03 0.00357  5.70568E+03 0.00395  5.62537E+03 0.00276  5.83351E+03 0.00410  5.43745E+03 0.00377  1.02968E+04 0.00272  1.64966E+04 0.00189  2.10337E+04 0.00182  5.52564E+04 0.00171  5.85138E+04 0.00143  6.35659E+04 0.00106  4.32067E+04 0.00151  3.20730E+04 0.00174  2.47796E+04 0.00155  2.91768E+04 0.00169  5.52026E+04 0.00107  7.40122E+04 0.00126  1.41054E+05 0.00125  2.14701E+05 0.00125  3.16470E+05 0.00115  2.00544E+05 0.00125  1.42770E+05 0.00130  1.02283E+05 0.00149  9.15134E+04 0.00139  8.99478E+04 0.00155  7.48853E+04 0.00144  5.06420E+04 0.00123  4.66208E+04 0.00182  4.13834E+04 0.00176  3.49366E+04 0.00227  2.74604E+04 0.00170  1.83362E+04 0.00181  6.49734E+03 0.00254 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.15161E+00 0.00052 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.11700E+18 0.00046  2.95426E+17 0.00096 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.36964E-01 0.00011  1.49298E+00 0.00037 ];
INF_CAPT                  (idx, [1:   4]) = [  6.46505E-03 0.00056  2.90082E-02 0.00032 ];
INF_ABS                   (idx, [1:   4]) = [  8.88923E-03 0.00049  6.67890E-02 0.00068 ];
INF_FISS                  (idx, [1:   4]) = [  2.42418E-03 0.00055  3.77807E-02 0.00097 ];
INF_NSF                   (idx, [1:   4]) = [  6.19257E-03 0.00056  9.20892E-02 0.00097 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.55450E+00 5.3E-05  2.43746E+00 3.1E-07 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03730E+02 4.7E-06  2.02280E+02 4.9E-08 ];
INF_INVV                  (idx, [1:   4]) = [  5.85498E-08 0.00041  2.52388E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28074E-01 0.00012  1.42614E+00 0.00042 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42802E-01 0.00020  3.78777E-01 0.00047 ];
INF_SCATT2                (idx, [1:   4]) = [  9.57665E-02 0.00033  9.07689E-02 0.00073 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35586E-03 0.00299  2.72623E-02 0.00253 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02402E-02 0.00218 -8.16316E-03 0.00762 ];
INF_SCATT5                (idx, [1:   4]) = [  1.55083E-04 0.11583  6.17934E-03 0.00896 ];
INF_SCATT6                (idx, [1:   4]) = [  5.06895E-03 0.00284 -1.59274E-02 0.00251 ];
INF_SCATT7                (idx, [1:   4]) = [  7.56345E-04 0.02066  2.50472E-04 0.18422 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28113E-01 0.00012  1.42614E+00 0.00042 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42802E-01 0.00020  3.78777E-01 0.00047 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.57666E-02 0.00033  9.07689E-02 0.00073 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35566E-03 0.00300  2.72623E-02 0.00253 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02403E-02 0.00218 -8.16316E-03 0.00762 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.54915E-04 0.11582  6.17934E-03 0.00896 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.06887E-03 0.00284 -1.59274E-02 0.00251 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.56450E-04 0.02063  2.50472E-04 0.18422 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.14153E-01 0.00029  9.60712E-01 0.00032 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55652E+00 0.00029  3.46966E-01 0.00032 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.85013E-03 0.00050  6.67890E-02 0.00068 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69449E-02 0.00021  6.80275E-02 0.00095 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10019E-01 0.00012  1.80548E-02 0.00035  1.19644E-03 0.00517  1.42495E+00 0.00043 ];
INF_S1                    (idx, [1:   8]) = [  2.37529E-01 0.00020  5.27249E-03 0.00107  5.06884E-04 0.00822  3.78270E-01 0.00047 ];
INF_S2                    (idx, [1:   8]) = [  9.73223E-02 0.00032 -1.55580E-03 0.00255  2.77019E-04 0.00832  9.04919E-02 0.00073 ];
INF_S3                    (idx, [1:   8]) = [  9.20904E-03 0.00242 -1.85318E-03 0.00184  9.45031E-05 0.01985  2.71678E-02 0.00252 ];
INF_S4                    (idx, [1:   8]) = [ -9.61965E-03 0.00226 -6.20556E-04 0.00454 -9.81297E-07 1.00000 -8.16218E-03 0.00758 ];
INF_S5                    (idx, [1:   8]) = [  1.38884E-04 0.12778  1.61986E-05 0.16204 -4.06472E-05 0.03574  6.21998E-03 0.00888 ];
INF_S6                    (idx, [1:   8]) = [  5.20834E-03 0.00267 -1.39387E-04 0.02123 -5.06137E-05 0.02838 -1.58768E-02 0.00248 ];
INF_S7                    (idx, [1:   8]) = [  9.21342E-04 0.01659 -1.64997E-04 0.01364 -4.70575E-05 0.03035  2.97529E-04 0.15417 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10059E-01 0.00012  1.80548E-02 0.00035  1.19644E-03 0.00517  1.42495E+00 0.00043 ];
INF_SP1                   (idx, [1:   8]) = [  2.37530E-01 0.00020  5.27249E-03 0.00107  5.06884E-04 0.00822  3.78270E-01 0.00047 ];
INF_SP2                   (idx, [1:   8]) = [  9.73224E-02 0.00032 -1.55580E-03 0.00255  2.77019E-04 0.00832  9.04919E-02 0.00073 ];
INF_SP3                   (idx, [1:   8]) = [  9.20884E-03 0.00242 -1.85318E-03 0.00184  9.45031E-05 0.01985  2.71678E-02 0.00252 ];
INF_SP4                   (idx, [1:   8]) = [ -9.61977E-03 0.00226 -6.20556E-04 0.00454 -9.81297E-07 1.00000 -8.16218E-03 0.00758 ];
INF_SP5                   (idx, [1:   8]) = [  1.38717E-04 0.12780  1.61986E-05 0.16204 -4.06472E-05 0.03574  6.21998E-03 0.00888 ];
INF_SP6                   (idx, [1:   8]) = [  5.20826E-03 0.00267 -1.39387E-04 0.02123 -5.06137E-05 0.02838 -1.58768E-02 0.00248 ];
INF_SP7                   (idx, [1:   8]) = [  9.21447E-04 0.01656 -1.64997E-04 0.01364 -4.70575E-05 0.03035  2.97529E-04 0.15417 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32053E-01 0.00071  1.06356E+00 0.00669 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33201E-01 0.00068  1.12937E+00 0.00871 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33692E-01 0.00110  1.12687E+00 0.00816 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29325E-01 0.00124  9.55203E-01 0.00617 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43647E+00 0.00072  3.13753E-01 0.00675 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42940E+00 0.00068  2.95694E-01 0.00879 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42642E+00 0.00111  2.96284E-01 0.00827 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45360E+00 0.00124  3.49282E-01 0.00611 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.09273E-03 0.00790  2.00650E-04 0.05234  1.13069E-03 0.02139  1.08781E-03 0.01928  3.24788E-03 0.01206  1.08011E-03 0.02008  3.45601E-04 0.03481 ];
LAMBDA                    (idx, [1:  14]) = [  8.05355E-01 0.01789  1.24907E-02 3.2E-06  3.16557E-02 0.00030  1.10153E-01 0.00040  3.20623E-01 0.00032  1.34589E+00 0.00022  8.86367E+00 0.00190 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0009' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Mon Jul 20 23:56:26 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284850 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00199E+00  9.98205E-01  9.98673E-01  1.00178E+00  9.99352E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14927E-02 0.00105  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88507E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.96475E-01 1.0E-04  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.96914E-01 9.9E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.70410E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.48020E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.47951E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.86678E+00 0.00036  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.19288E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000526 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00011E+04 0.00072 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00011E+04 0.00072 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  7.60798E+01 ;
RUNNING_TIME              (idx, 1)        =  1.55911E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.43100E-01  4.43100E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  4.23500E-02  7.03333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.49687E+01  3.28682E+00  2.56483E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.35417E-01  2.73333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.65000E-03  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.55909E+01  1.21370E+02 ];
CPU_USAGE                 (idx, 1)        = 4.87971 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99888E+00 0.00023 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.57336E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.64555E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.82834E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.41388E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.50672E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.05290E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.13880E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.72303E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.62515E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  2.97467E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  8.33099E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.14910E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  1.79205E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  2.35976E+07 ;
SR90_ACTIVITY             (idx, 1)        =  6.38900E+11 ;
TE132_ACTIVITY            (idx, 1)        =  5.62978E+14 ;
I131_ACTIVITY             (idx, 1)        =  2.65217E+14 ;
I132_ACTIVITY             (idx, 1)        =  5.65763E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.85618E+09 ;
CS137_ACTIVITY            (idx, 1)        =  6.73966E+11 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.56132E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.63716E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  6.41550E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.09586E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.01311E+12 0.00047  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 2 ;
BURNUP                     (idx, [1:  2])  = [  5.00000E-01  5.00043E-01 ];
BURN_DAYS                 (idx, 1)        =  1.25000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.80337E-01 0.00111 ];
U235_FISS                 (idx, [1:   4]) = [  1.27486E+16 0.00056  9.20432E-01 0.00019 ];
U238_FISS                 (idx, [1:   4]) = [  8.37508E+14 0.00266  6.04579E-02 0.00249 ];
PU239_FISS                (idx, [1:   4]) = [  2.62319E+14 0.00481  1.89387E-02 0.00477 ];
PU240_FISS                (idx, [1:   4]) = [  1.79667E+10 0.57621  1.30216E-06 0.57623 ];
PU241_FISS                (idx, [1:   4]) = [  1.02555E+11 0.23868  7.37057E-06 0.23868 ];
U235_CAPT                 (idx, [1:   4]) = [  2.79218E+15 0.00145  1.73257E-01 0.00135 ];
U238_CAPT                 (idx, [1:   4]) = [  7.60178E+15 0.00103  4.71654E-01 0.00064 ];
PU239_CAPT                (idx, [1:   4]) = [  1.47275E+14 0.00685  9.13822E-03 0.00681 ];
PU240_CAPT                (idx, [1:   4]) = [  7.06025E+12 0.02840  4.37746E-04 0.02833 ];
PU241_CAPT                (idx, [1:   4]) = [  4.19399E+10 0.37571  2.60695E-06 0.37574 ];
XE135_CAPT                (idx, [1:   4]) = [  7.08944E+14 0.00301  4.39935E-02 0.00301 ];
SM149_CAPT                (idx, [1:   4]) = [  1.19510E+14 0.00692  7.41568E-03 0.00691 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000526 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.41330E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000526 5.00741E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2670600 2.67436E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2295392 2.29851E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 34534 3.45374E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000526 5.00741E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -2.88710E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 0.0E+00  4.50056E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.42088E+16 1.1E-05  3.42088E+16 1.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38605E+16 1.3E-06  1.38605E+16 1.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.61168E+16 0.00044  1.15777E+16 0.00044  4.53904E+15 0.00104 ];
TOT_ABSRATE               (idx, [1:   6]) = [  2.99773E+16 0.00023  2.54383E+16 0.00020  4.53904E+15 0.00104 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.01311E+16 0.00047  3.01311E+16 0.00047  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.42484E+18 0.00044  3.89984E+17 0.00044  1.03486E+18 0.00048 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.08155E+14 0.00569 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.01855E+16 0.00024 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.04984E+18 0.00056 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12456E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12456E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.71238E+00 0.00042 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.97195E-01 0.00029 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.66712E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25533E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95493E-01 3.1E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97589E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.14245E+00 0.00049 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.13456E+00 0.00050 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46807E+00 1.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02664E+02 1.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.13463E+00 0.00049  1.12679E+00 0.00050  7.77460E-03 0.00822 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.13499E+00 0.00024 ];
COL_KEFF                  (idx, [1:   2]) = [  1.13546E+00 0.00047 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.13499E+00 0.00024 ];
ABS_KINF                  (idx, [1:   2]) = [  1.14289E+00 0.00023 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74689E+01 0.00018 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74709E+01 8.1E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.19315E-07 0.00314 ];
IMP_EALF                  (idx, [1:   2]) = [  5.17309E-07 0.00141 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.06295E-01 0.00270 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.06744E-01 0.00120 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.14230E-03 0.00544  1.72245E-04 0.03335  9.74401E-04 0.01377  9.87281E-04 0.01323  2.81586E-03 0.00813  8.92362E-04 0.01438  3.00152E-04 0.02397 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.01204E-01 0.01282  1.07170E-02 0.01821  3.16433E-02 0.00022  1.10153E-01 0.00027  3.20550E-01 0.00021  1.34572E+00 0.00016  8.64649E+00 0.00750 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.91196E-03 0.00836  1.96937E-04 0.04580  1.10483E-03 0.01984  1.12013E-03 0.01980  3.13687E-03 0.01224  1.01756E-03 0.02185  3.35636E-04 0.03751 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.98869E-01 0.01928  1.24907E-02 3.6E-06  3.16415E-02 0.00032  1.10123E-01 0.00037  3.20663E-01 0.00031  1.34562E+00 0.00024  8.87712E+00 0.00206 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.31360E-05 0.00111  2.31240E-05 0.00111  2.47917E-05 0.01102 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.62477E-05 0.00100  2.62340E-05 0.00100  2.81266E-05 0.01100 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.85947E-03 0.00831  1.97062E-04 0.04635  1.08983E-03 0.02026  1.10514E-03 0.01869  3.12224E-03 0.01246  1.00198E-03 0.02166  3.43219E-04 0.03794 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.10397E-01 0.02037  1.24906E-02 4.3E-06  3.16470E-02 0.00035  1.10134E-01 0.00042  3.20580E-01 0.00032  1.34560E+00 0.00025  8.89178E+00 0.00251 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.27518E-05 0.00241  2.27439E-05 0.00242  2.36374E-05 0.02667 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.58122E-05 0.00238  2.58033E-05 0.00239  2.68016E-05 0.02656 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.90944E-03 0.02488  2.12100E-04 0.14254  1.08825E-03 0.05958  1.21740E-03 0.06126  3.10371E-03 0.03777  9.80613E-04 0.06979  3.07370E-04 0.10376 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.89796E-01 0.05758  1.24905E-02 1.1E-05  3.16131E-02 0.00090  1.10089E-01 0.00095  3.21097E-01 0.00103  1.34484E+00 0.00066  8.86378E+00 0.00496 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.91757E-03 0.02449  2.11055E-04 0.13866  1.09425E-03 0.05677  1.22811E-03 0.05967  3.09630E-03 0.03754  9.69265E-04 0.06797  3.18582E-04 0.10154 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.08267E-01 0.05658  1.24905E-02 1.1E-05  3.16130E-02 0.00089  1.10094E-01 0.00095  3.21122E-01 0.00101  1.34488E+00 0.00065  8.86521E+00 0.00496 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -3.04709E+02 0.02516 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.29315E-05 0.00066 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.60156E-05 0.00043 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.00399E-03 0.00481 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -3.05516E+02 0.00488 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.71909E-07 0.00067 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.88016E-06 0.00042  2.87982E-06 0.00042  2.92920E-06 0.00506 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.73948E-05 0.00081  3.74158E-05 0.00081  3.44533E-05 0.00846 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.64954E-01 0.00032  6.64288E-01 0.00032  7.83904E-01 0.00852 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.01887E+01 0.01318 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.47951E+01 0.00043  3.40808E+01 0.00041 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.18213E+04 0.00365  2.92463E+05 0.00160  6.03368E+05 0.00098  6.51685E+05 0.00078  5.99602E+05 0.00052  6.45362E+05 0.00059  4.38598E+05 0.00078  3.87394E+05 0.00058  2.97095E+05 0.00060  2.42803E+05 0.00077  2.09301E+05 0.00086  1.88647E+05 0.00082  1.74097E+05 0.00074  1.65543E+05 0.00084  1.60965E+05 0.00069  1.39085E+05 0.00083  1.37470E+05 0.00089  1.36168E+05 0.00103  1.33704E+05 0.00105  2.61069E+05 0.00065  2.51014E+05 0.00060  1.81159E+05 0.00063  1.17026E+05 0.00090  1.34938E+05 0.00087  1.27280E+05 0.00065  1.15664E+05 0.00111  1.89843E+05 0.00074  4.33608E+04 0.00199  5.43369E+04 0.00142  4.94070E+04 0.00152  2.85653E+04 0.00143  5.00200E+04 0.00166  3.40010E+04 0.00136  2.88334E+04 0.00195  5.49665E+03 0.00302  5.45235E+03 0.00297  5.60222E+03 0.00475  5.75070E+03 0.00325  5.72621E+03 0.00325  5.62429E+03 0.00416  5.79461E+03 0.00331  5.52115E+03 0.00420  1.03320E+04 0.00288  1.65861E+04 0.00249  2.10147E+04 0.00166  5.53500E+04 0.00137  5.82744E+04 0.00143  6.32516E+04 0.00112  4.28803E+04 0.00120  3.17358E+04 0.00159  2.43919E+04 0.00173  2.87685E+04 0.00140  5.44232E+04 0.00149  7.33272E+04 0.00120  1.39841E+05 0.00110  2.13237E+05 0.00119  3.14523E+05 0.00102  1.99035E+05 0.00120  1.42021E+05 0.00119  1.01762E+05 0.00129  9.10783E+04 0.00133  8.96181E+04 0.00138  7.46784E+04 0.00148  5.03931E+04 0.00138  4.64222E+04 0.00136  4.12518E+04 0.00135  3.48321E+04 0.00153  2.73392E+04 0.00143  1.83243E+04 0.00155  6.47403E+03 0.00200 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.14336E+00 0.00045 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.12840E+18 0.00047  2.96471E+17 0.00090 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.36945E-01 9.8E-05  1.49752E+00 0.00023 ];
INF_CAPT                  (idx, [1:   4]) = [  6.47104E-03 0.00077  2.97351E-02 0.00034 ];
INF_ABS                   (idx, [1:   4]) = [  8.87746E-03 0.00063  6.73334E-02 0.00063 ];
INF_FISS                  (idx, [1:   4]) = [  2.40642E-03 0.00042  3.75983E-02 0.00087 ];
INF_NSF                   (idx, [1:   4]) = [  6.15550E-03 0.00040  9.19720E-02 0.00087 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.55795E+00 6.6E-05  2.44617E+00 3.9E-06 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03772E+02 6.9E-06  2.02394E+02 6.2E-07 ];
INF_INVV                  (idx, [1:   4]) = [  5.85328E-08 0.00035  2.52597E-06 9.9E-05 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28067E-01 0.00010  1.43023E+00 0.00027 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42798E-01 0.00016  3.79713E-01 0.00036 ];
INF_SCATT2                (idx, [1:   4]) = [  9.57800E-02 0.00021  9.06872E-02 0.00074 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35021E-03 0.00231  2.72761E-02 0.00187 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02288E-02 0.00180 -8.12735E-03 0.00727 ];
INF_SCATT5                (idx, [1:   4]) = [  1.61839E-04 0.08919  6.15778E-03 0.00830 ];
INF_SCATT6                (idx, [1:   4]) = [  5.03942E-03 0.00398 -1.59990E-02 0.00267 ];
INF_SCATT7                (idx, [1:   4]) = [  7.24869E-04 0.02368  2.84658E-04 0.16688 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28107E-01 0.00010  1.43023E+00 0.00027 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42799E-01 0.00016  3.79713E-01 0.00036 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.57801E-02 0.00021  9.06872E-02 0.00074 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35000E-03 0.00232  2.72761E-02 0.00187 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02290E-02 0.00180 -8.12735E-03 0.00727 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.61872E-04 0.08911  6.15778E-03 0.00830 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.03953E-03 0.00398 -1.59990E-02 0.00267 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.24723E-04 0.02367  2.84658E-04 0.16688 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.14086E-01 0.00035  9.64425E-01 0.00020 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55701E+00 0.00035  3.45629E-01 0.00020 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.83787E-03 0.00063  6.73334E-02 0.00063 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69369E-02 0.00020  6.84919E-02 0.00078 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10008E-01 9.7E-05  1.80592E-02 0.00042  1.19950E-03 0.00333  1.42903E+00 0.00027 ];
INF_S1                    (idx, [1:   8]) = [  2.37522E-01 0.00016  5.27690E-03 0.00097  5.05179E-04 0.00485  3.79208E-01 0.00036 ];
INF_S2                    (idx, [1:   8]) = [  9.73446E-02 0.00022 -1.56455E-03 0.00303  2.77220E-04 0.00836  9.04100E-02 0.00075 ];
INF_S3                    (idx, [1:   8]) = [  9.20552E-03 0.00176 -1.85531E-03 0.00202  9.63460E-05 0.02111  2.71797E-02 0.00187 ];
INF_S4                    (idx, [1:   8]) = [ -9.60922E-03 0.00180 -6.19582E-04 0.00474 -1.36041E-06 0.87776 -8.12599E-03 0.00731 ];
INF_S5                    (idx, [1:   8]) = [  1.46276E-04 0.09411  1.55633E-05 0.21069 -4.12134E-05 0.03969  6.19900E-03 0.00828 ];
INF_S6                    (idx, [1:   8]) = [  5.17967E-03 0.00394 -1.40249E-04 0.02125 -4.95300E-05 0.02945 -1.59494E-02 0.00268 ];
INF_S7                    (idx, [1:   8]) = [  8.92362E-04 0.01866 -1.67493E-04 0.01403 -4.60434E-05 0.03012  3.30701E-04 0.14176 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10048E-01 9.7E-05  1.80592E-02 0.00042  1.19950E-03 0.00333  1.42903E+00 0.00027 ];
INF_SP1                   (idx, [1:   8]) = [  2.37522E-01 0.00016  5.27690E-03 0.00097  5.05179E-04 0.00485  3.79208E-01 0.00036 ];
INF_SP2                   (idx, [1:   8]) = [  9.73447E-02 0.00022 -1.56455E-03 0.00303  2.77220E-04 0.00836  9.04100E-02 0.00075 ];
INF_SP3                   (idx, [1:   8]) = [  9.20531E-03 0.00176 -1.85531E-03 0.00202  9.63460E-05 0.02111  2.71797E-02 0.00187 ];
INF_SP4                   (idx, [1:   8]) = [ -9.60937E-03 0.00180 -6.19582E-04 0.00474 -1.36041E-06 0.87776 -8.12599E-03 0.00731 ];
INF_SP5                   (idx, [1:   8]) = [  1.46308E-04 0.09406  1.55633E-05 0.21069 -4.12134E-05 0.03969  6.19900E-03 0.00828 ];
INF_SP6                   (idx, [1:   8]) = [  5.17978E-03 0.00394 -1.40249E-04 0.02125 -4.95300E-05 0.02945 -1.59494E-02 0.00268 ];
INF_SP7                   (idx, [1:   8]) = [  8.92216E-04 0.01864 -1.67493E-04 0.01403 -4.60434E-05 0.03012  3.30701E-04 0.14176 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32482E-01 0.00071  1.04856E+00 0.00671 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.34047E-01 0.00089  1.10372E+00 0.00718 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33829E-01 0.00101  1.10952E+00 0.00888 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29629E-01 0.00091  9.49801E-01 0.00683 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43382E+00 0.00071  3.18240E-01 0.00672 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42424E+00 0.00089  3.02378E-01 0.00711 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42558E+00 0.00101  3.01001E-01 0.00892 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45165E+00 0.00091  3.51342E-01 0.00681 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.91196E-03 0.00836  1.96937E-04 0.04580  1.10483E-03 0.01984  1.12013E-03 0.01980  3.13687E-03 0.01224  1.01756E-03 0.02185  3.35636E-04 0.03751 ];
LAMBDA                    (idx, [1:  14]) = [  7.98869E-01 0.01928  1.24907E-02 3.6E-06  3.16415E-02 0.00032  1.10123E-01 0.00037  3.20663E-01 0.00031  1.34562E+00 0.00024  8.87712E+00 0.00206 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0009' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:02:20 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284850 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00228E+00  9.98280E-01  9.98180E-01  1.00137E+00  9.99882E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.6E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15300E-02 0.00105  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88470E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.96915E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.97356E-01 1.0E-04  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.70037E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.46376E+01 0.00042  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.46307E+01 0.00042  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.80082E+00 0.00036  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.18220E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000543 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00011E+04 0.00069 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00011E+04 0.00069 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.05553E+02 ;
RUNNING_TIME              (idx, 1)        =  2.14920E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.43100E-01  4.43100E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  6.05333E-02  8.26667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  2.07941E+01  3.26618E+00  2.55920E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.92300E-01  2.83000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  6.35000E-03  8.16667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  2.14919E+01  1.22117E+02 ];
CPU_USAGE                 (idx, 1)        = 4.91127 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99934E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.64977E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.85202E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.84549E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.46126E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.53461E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.07152E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.31740E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.73832E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  3.48778E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.44879E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  9.58759E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.34613E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  2.52902E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  2.81417E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.26751E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.03703E+14 ;
I131_ACTIVITY             (idx, 1)        =  3.59581E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.08671E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.67810E+10 ;
CS137_ACTIVITY            (idx, 1)        =  1.34867E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.75716E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.62009E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.41935E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.13413E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.04008E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 3 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E+00  1.00008E+00 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.81820E-01 0.00101 ];
U235_FISS                 (idx, [1:   4]) = [  1.24054E+16 0.00056  8.95995E-01 0.00023 ];
U238_FISS                 (idx, [1:   4]) = [  8.50090E+14 0.00273  6.13927E-02 0.00261 ];
PU239_FISS                (idx, [1:   4]) = [  5.86181E+14 0.00330  4.23343E-02 0.00321 ];
PU240_FISS                (idx, [1:   4]) = [  7.28691E+10 0.28551  5.29163E-06 0.28550 ];
PU241_FISS                (idx, [1:   4]) = [  1.12574E+12 0.07267  8.11958E-05 0.07265 ];
U235_CAPT                 (idx, [1:   4]) = [  2.71945E+15 0.00156  1.65927E-01 0.00140 ];
U238_CAPT                 (idx, [1:   4]) = [  7.64783E+15 0.00096  4.66626E-01 0.00063 ];
PU239_CAPT                (idx, [1:   4]) = [  3.31825E+14 0.00433  2.02456E-02 0.00426 ];
PU240_CAPT                (idx, [1:   4]) = [  3.03788E+13 0.01445  1.85305E-03 0.01438 ];
PU241_CAPT                (idx, [1:   4]) = [  3.90711E+11 0.12707  2.37670E-05 0.12677 ];
XE135_CAPT                (idx, [1:   4]) = [  7.10357E+14 0.00293  4.33429E-02 0.00287 ];
SM149_CAPT                (idx, [1:   4]) = [  1.48313E+14 0.00678  9.04927E-03 0.00674 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000543 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.44574E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000543 5.00745E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2691694 2.69552E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2274209 2.27728E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 34640 3.46473E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000543 5.00745E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.30854E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 0.0E+00  4.50056E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.43303E+16 1.1E-05  3.43303E+16 1.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38513E+16 1.4E-06  1.38513E+16 1.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.63830E+16 0.00045  1.18494E+16 0.00044  4.53362E+15 0.00102 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.02343E+16 0.00024  2.57007E+16 0.00020  4.53362E+15 0.00102 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.04008E+16 0.00050  3.04008E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.43363E+18 0.00046  3.92390E+17 0.00045  1.04124E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.10684E+14 0.00580 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.04450E+16 0.00025 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.05423E+18 0.00057 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12397E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12397E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.70281E+00 0.00041 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.99144E-01 0.00029 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.65709E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25494E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95493E-01 3.1E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97566E-01 2.2E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.13677E+00 0.00048 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.12889E+00 0.00048 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.47849E+00 1.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02799E+02 1.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.12884E+00 0.00049  1.12125E+00 0.00049  7.64866E-03 0.00843 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.12933E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.12940E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.12933E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.13721E+00 0.00024 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74446E+01 0.00018 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74465E+01 7.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.32157E-07 0.00323 ];
IMP_EALF                  (idx, [1:   2]) = [  5.30024E-07 0.00138 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.10109E-01 0.00280 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.09150E-01 0.00112 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.12089E-03 0.00550  1.76102E-04 0.03070  9.67980E-04 0.01345  9.55811E-04 0.01392  2.79802E-03 0.00788  9.17530E-04 0.01420  3.05440E-04 0.02407 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.12465E-01 0.01237  1.07919E-02 0.01776  3.16037E-02 0.00025  1.10138E-01 0.00028  3.20784E-01 0.00023  1.34533E+00 0.00016  8.72223E+00 0.00660 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.89441E-03 0.00776  1.89715E-04 0.04609  1.09978E-03 0.01919  1.08004E-03 0.02034  3.12337E-03 0.01133  1.04582E-03 0.02094  3.55677E-04 0.03624 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.31206E-01 0.01922  1.24906E-02 4.6E-06  3.15990E-02 0.00035  1.10151E-01 0.00038  3.20728E-01 0.00032  1.34543E+00 0.00023  8.91016E+00 0.00202 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.30181E-05 0.00109  2.30088E-05 0.00110  2.44940E-05 0.01155 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.59807E-05 0.00097  2.59702E-05 0.00098  2.76446E-05 0.01152 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.78637E-03 0.00851  1.96003E-04 0.04706  1.09367E-03 0.01960  1.04357E-03 0.02109  3.07028E-03 0.01194  1.03365E-03 0.02174  3.49185E-04 0.03725 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.28066E-01 0.01962  1.24906E-02 6.5E-06  3.16085E-02 0.00039  1.10136E-01 0.00044  3.20765E-01 0.00035  1.34549E+00 0.00027  8.89939E+00 0.00239 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.26289E-05 0.00239  2.26151E-05 0.00240  2.53153E-05 0.02711 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.55412E-05 0.00233  2.55256E-05 0.00234  2.85957E-05 0.02717 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.34650E-03 0.02689  1.48390E-04 0.15733  9.52436E-04 0.06603  1.02686E-03 0.06185  2.90733E-03 0.03892  9.83082E-04 0.06393  3.28409E-04 0.11722 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.11878E-01 0.05830  1.24904E-02 1.3E-05  3.15392E-02 0.00111  1.09968E-01 0.00095  3.21231E-01 0.00110  1.34442E+00 0.00065  8.82917E+00 0.00519 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.37579E-03 0.02604  1.51729E-04 0.15335  9.66810E-04 0.06540  1.01414E-03 0.06110  2.93868E-03 0.03748  9.71968E-04 0.06147  3.32459E-04 0.11529 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.02621E-01 0.05665  1.24904E-02 1.3E-05  3.15430E-02 0.00109  1.09984E-01 0.00095  3.21227E-01 0.00108  1.34432E+00 0.00065  8.82776E+00 0.00518 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.81076E+02 0.02684 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.28183E-05 0.00071 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.57552E-05 0.00050 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.55895E-03 0.00491 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.87612E+02 0.00508 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.66758E-07 0.00065 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87913E-06 0.00042  2.87900E-06 0.00042  2.89390E-06 0.00527 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.69854E-05 0.00080  3.70077E-05 0.00081  3.38657E-05 0.00863 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.63983E-01 0.00031  6.63301E-01 0.00032  7.86052E-01 0.00844 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.01867E+01 0.01309 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.46307E+01 0.00042  3.39432E+01 0.00041 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.27620E+04 0.00293  2.92214E+05 0.00154  6.04434E+05 0.00084  6.50745E+05 0.00100  5.99690E+05 0.00097  6.44985E+05 0.00055  4.38150E+05 0.00069  3.88085E+05 0.00070  2.96518E+05 0.00081  2.42420E+05 0.00074  2.09116E+05 0.00083  1.88362E+05 0.00071  1.73823E+05 0.00075  1.65610E+05 0.00086  1.60969E+05 0.00076  1.38910E+05 0.00089  1.37450E+05 0.00091  1.36073E+05 0.00101  1.33898E+05 0.00085  2.60858E+05 0.00063  2.51285E+05 0.00054  1.81202E+05 0.00073  1.17194E+05 0.00096  1.35014E+05 0.00093  1.27444E+05 0.00089  1.15597E+05 0.00107  1.89517E+05 0.00074  4.34823E+04 0.00156  5.44116E+04 0.00128  4.95129E+04 0.00171  2.87665E+04 0.00240  5.00611E+04 0.00094  3.38832E+04 0.00177  2.88646E+04 0.00228  5.50529E+03 0.00306  5.44352E+03 0.00334  5.56526E+03 0.00345  5.73440E+03 0.00413  5.69511E+03 0.00407  5.59979E+03 0.00343  5.81641E+03 0.00446  5.45277E+03 0.00435  1.03137E+04 0.00260  1.64949E+04 0.00207  2.09223E+04 0.00193  5.53937E+04 0.00172  5.82117E+04 0.00153  6.30559E+04 0.00136  4.24435E+04 0.00133  3.12318E+04 0.00145  2.39159E+04 0.00148  2.81090E+04 0.00120  5.34353E+04 0.00126  7.21687E+04 0.00121  1.37817E+05 0.00100  2.10182E+05 0.00100  3.10138E+05 0.00093  1.96625E+05 0.00093  1.40023E+05 0.00102  1.00425E+05 0.00117  9.00073E+04 0.00118  8.84374E+04 0.00139  7.37768E+04 0.00145  4.97716E+04 0.00147  4.59355E+04 0.00127  4.07622E+04 0.00140  3.44754E+04 0.00128  2.70813E+04 0.00146  1.80775E+04 0.00166  6.39781E+03 0.00207 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.13728E+00 0.00045 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.13839E+18 0.00045  2.95268E+17 0.00082 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.36979E-01 0.00013  1.50021E+00 0.00029 ];
INF_CAPT                  (idx, [1:   4]) = [  6.51552E-03 0.00055  3.03678E-02 0.00031 ];
INF_ABS                   (idx, [1:   4]) = [  8.90629E-03 0.00044  6.80673E-02 0.00058 ];
INF_FISS                  (idx, [1:   4]) = [  2.39077E-03 0.00041  3.76996E-02 0.00081 ];
INF_NSF                   (idx, [1:   4]) = [  6.12607E-03 0.00041  9.26648E-02 0.00081 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.56238E+00 4.8E-05  2.45798E+00 7.1E-06 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03827E+02 5.9E-06  2.02548E+02 1.1E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.85259E-08 0.00047  2.52632E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28074E-01 0.00014  1.43214E+00 0.00033 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42834E-01 0.00016  3.80525E-01 0.00040 ];
INF_SCATT2                (idx, [1:   4]) = [  9.57539E-02 0.00030  9.10808E-02 0.00067 ];
INF_SCATT3                (idx, [1:   4]) = [  7.31336E-03 0.00348  2.75052E-02 0.00233 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02557E-02 0.00226 -8.06064E-03 0.00652 ];
INF_SCATT5                (idx, [1:   4]) = [  1.12417E-04 0.12274  6.25856E-03 0.00676 ];
INF_SCATT6                (idx, [1:   4]) = [  5.05072E-03 0.00299 -1.60012E-02 0.00274 ];
INF_SCATT7                (idx, [1:   4]) = [  7.37178E-04 0.02167  2.74363E-04 0.13771 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28114E-01 0.00014  1.43214E+00 0.00033 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42835E-01 0.00016  3.80525E-01 0.00040 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.57543E-02 0.00030  9.10808E-02 0.00067 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.31332E-03 0.00348  2.75052E-02 0.00233 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02556E-02 0.00226 -8.06064E-03 0.00652 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.12459E-04 0.12279  6.25856E-03 0.00676 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.05085E-03 0.00300 -1.60012E-02 0.00274 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.37125E-04 0.02167  2.74363E-04 0.13771 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13980E-01 0.00029  9.66335E-01 0.00027 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55778E+00 0.00029  3.44946E-01 0.00027 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.86653E-03 0.00043  6.80673E-02 0.00058 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69415E-02 0.00021  6.92898E-02 0.00073 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10038E-01 0.00013  1.80364E-02 0.00035  1.22270E-03 0.00388  1.43092E+00 0.00033 ];
INF_S1                    (idx, [1:   8]) = [  2.37561E-01 0.00016  5.27317E-03 0.00076  5.19834E-04 0.00804  3.80005E-01 0.00040 ];
INF_S2                    (idx, [1:   8]) = [  9.73078E-02 0.00030 -1.55390E-03 0.00272  2.84367E-04 0.00887  9.07964E-02 0.00068 ];
INF_S3                    (idx, [1:   8]) = [  9.16556E-03 0.00263 -1.85221E-03 0.00174  1.05626E-04 0.02083  2.73995E-02 0.00235 ];
INF_S4                    (idx, [1:   8]) = [ -9.63426E-03 0.00229 -6.21399E-04 0.00462  3.01567E-06 0.73843 -8.06366E-03 0.00653 ];
INF_S5                    (idx, [1:   8]) = [  1.03256E-04 0.12943  9.16144E-06 0.30688 -3.93182E-05 0.04621  6.29788E-03 0.00668 ];
INF_S6                    (idx, [1:   8]) = [  5.19466E-03 0.00264 -1.43936E-04 0.01797 -5.22793E-05 0.01928 -1.59490E-02 0.00275 ];
INF_S7                    (idx, [1:   8]) = [  9.06834E-04 0.01737 -1.69656E-04 0.01489 -4.83094E-05 0.02147  3.22673E-04 0.11824 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10078E-01 0.00013  1.80364E-02 0.00035  1.22270E-03 0.00388  1.43092E+00 0.00033 ];
INF_SP1                   (idx, [1:   8]) = [  2.37562E-01 0.00016  5.27317E-03 0.00076  5.19834E-04 0.00804  3.80005E-01 0.00040 ];
INF_SP2                   (idx, [1:   8]) = [  9.73082E-02 0.00030 -1.55390E-03 0.00272  2.84367E-04 0.00887  9.07964E-02 0.00068 ];
INF_SP3                   (idx, [1:   8]) = [  9.16553E-03 0.00263 -1.85221E-03 0.00174  1.05626E-04 0.02083  2.73995E-02 0.00235 ];
INF_SP4                   (idx, [1:   8]) = [ -9.63421E-03 0.00229 -6.21399E-04 0.00462  3.01567E-06 0.73843 -8.06366E-03 0.00653 ];
INF_SP5                   (idx, [1:   8]) = [  1.03298E-04 0.12955  9.16144E-06 0.30688 -3.93182E-05 0.04621  6.29788E-03 0.00668 ];
INF_SP6                   (idx, [1:   8]) = [  5.19479E-03 0.00264 -1.43936E-04 0.01797 -5.22793E-05 0.01928 -1.59490E-02 0.00275 ];
INF_SP7                   (idx, [1:   8]) = [  9.06781E-04 0.01737 -1.69656E-04 0.01489 -4.83094E-05 0.02147  3.22673E-04 0.11824 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31943E-01 0.00062  1.06033E+00 0.00619 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33119E-01 0.00079  1.11681E+00 0.00708 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33171E-01 0.00101  1.12758E+00 0.00722 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29583E-01 0.00094  9.55938E-01 0.00733 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43715E+00 0.00062  3.14656E-01 0.00619 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42991E+00 0.00079  2.98827E-01 0.00704 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42960E+00 0.00101  2.95993E-01 0.00733 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45194E+00 0.00094  3.49147E-01 0.00732 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.89441E-03 0.00776  1.89715E-04 0.04609  1.09978E-03 0.01919  1.08004E-03 0.02034  3.12337E-03 0.01133  1.04582E-03 0.02094  3.55677E-04 0.03624 ];
LAMBDA                    (idx, [1:  14]) = [  8.31206E-01 0.01922  1.24906E-02 4.6E-06  3.15990E-02 0.00035  1.10151E-01 0.00038  3.20728E-01 0.00032  1.34543E+00 0.00023  8.91016E+00 0.00202 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0009' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:08:12 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284850 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00103E+00  9.98363E-01  9.99373E-01  1.00154E+00  9.99694E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15262E-02 0.00102  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88474E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.98372E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.98811E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69515E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.44743E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.44672E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.68046E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.15137E-01 0.00107  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000620 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00012E+04 0.00075 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00012E+04 0.00075 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.34849E+02 ;
RUNNING_TIME              (idx, 1)        =  2.73577E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.43100E-01  4.43100E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  7.84667E-02  8.15000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  2.65858E+01  3.24965E+00  2.54203E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  2.47750E-01  2.68833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  9.00000E-03  9.16668E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  2.73575E+01  1.21410E+02 ];
CPU_USAGE                 (idx, 1)        = 4.92912 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00027E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.69280E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.01378E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.84706E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.66340E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.54541E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.07883E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.46835E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.73915E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  4.66887E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.80850E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.21246E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.40585E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  3.45641E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.16791E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.49457E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.11603E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.06886E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.18123E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.57453E+11 ;
CS137_ACTIVITY            (idx, 1)        =  2.69896E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.87578E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.58625E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.09760E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.16056E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.09397E+12 0.00047  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 4 ;
BURNUP                     (idx, [1:  2])  = [  2.00000E+00  2.00017E+00 ];
BURN_DAYS                 (idx, 1)        =  5.00000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.86313E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  1.18104E+16 0.00063  8.53221E-01 0.00028 ];
U238_FISS                 (idx, [1:   4]) = [  8.64597E+14 0.00276  6.24560E-02 0.00264 ];
PU239_FISS                (idx, [1:   4]) = [  1.15528E+15 0.00222  8.34658E-02 0.00219 ];
PU240_FISS                (idx, [1:   4]) = [  2.97303E+11 0.14987  2.14961E-05 0.14977 ];
PU241_FISS                (idx, [1:   4]) = [  8.14732E+12 0.02836  5.88788E-04 0.02840 ];
U235_CAPT                 (idx, [1:   4]) = [  2.60334E+15 0.00149  1.53820E-01 0.00138 ];
U238_CAPT                 (idx, [1:   4]) = [  7.72357E+15 0.00099  4.56326E-01 0.00066 ];
PU239_CAPT                (idx, [1:   4]) = [  6.45784E+14 0.00301  3.81623E-02 0.00305 ];
PU240_CAPT                (idx, [1:   4]) = [  1.14899E+14 0.00752  6.78747E-03 0.00745 ];
PU241_CAPT                (idx, [1:   4]) = [  2.76583E+12 0.04783  1.63596E-04 0.04788 ];
XE135_CAPT                (idx, [1:   4]) = [  7.10779E+14 0.00304  4.19982E-02 0.00301 ];
SM149_CAPT                (idx, [1:   4]) = [  1.57192E+14 0.00632  9.28752E-03 0.00629 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000620 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.36341E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000620 5.00736E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2731433 2.73514E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2234010 2.23703E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 35177 3.51911E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000620 5.00736E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.33299E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 0.0E+00  4.50056E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.45425E+16 1.2E-05  3.45425E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38350E+16 1.7E-06  1.38350E+16 1.7E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.69144E+16 0.00045  1.23403E+16 0.00046  4.57419E+15 0.00106 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.07495E+16 0.00024  2.61753E+16 0.00022  4.57419E+15 0.00106 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.09397E+16 0.00047  3.09397E+16 0.00047  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.45398E+18 0.00045  3.97119E+17 0.00043  1.05686E+18 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.17789E+14 0.00554 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.09673E+16 0.00025 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.06785E+18 0.00057 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12281E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12281E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.69057E+00 0.00041 ];
SIX_FF_F                  (idx, [1:   2]) = [  8.00268E-01 0.00030 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.62854E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25463E+00 0.00033 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95383E-01 3.1E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97568E-01 2.2E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.12506E+00 0.00049 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.11714E+00 0.00050 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.49674E+00 1.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03037E+02 1.7E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.11709E+00 0.00051  1.10982E+00 0.00050  7.32250E-03 0.00814 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.11714E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.11657E+00 0.00047 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.11714E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.12507E+00 0.00024 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74060E+01 0.00018 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74089E+01 7.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.53119E-07 0.00321 ];
IMP_EALF                  (idx, [1:   2]) = [  5.50361E-07 0.00135 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.13285E-01 0.00281 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.12818E-01 0.00110 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.98850E-03 0.00577  1.65958E-04 0.03257  9.56643E-04 0.01380  9.45725E-04 0.01352  2.74210E-03 0.00851  8.78738E-04 0.01587  2.99336E-04 0.02508 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.12051E-01 0.01294  1.05669E-02 0.01910  3.15304E-02 0.00029  1.10153E-01 0.00032  3.21026E-01 0.00023  1.34434E+00 0.00073  8.61573E+00 0.00860 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.65441E-03 0.00770  1.89120E-04 0.04985  1.05518E-03 0.01985  1.06520E-03 0.01893  3.02899E-03 0.01186  9.79496E-04 0.02103  3.36430E-04 0.03665 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.17839E-01 0.01895  1.24905E-02 7.2E-06  3.15185E-02 0.00042  1.10158E-01 0.00045  3.21188E-01 0.00034  1.34423E+00 0.00075  8.92608E+00 0.00225 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.27818E-05 0.00108  2.27727E-05 0.00109  2.41660E-05 0.01138 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.54460E-05 0.00096  2.54359E-05 0.00097  2.69942E-05 0.01138 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.55116E-03 0.00825  1.81057E-04 0.04937  1.04253E-03 0.02111  1.03260E-03 0.02071  3.00478E-03 0.01218  9.59207E-04 0.02331  3.30980E-04 0.03668 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.22315E-01 0.02020  1.24905E-02 9.6E-06  3.15000E-02 0.00051  1.10132E-01 0.00050  3.21018E-01 0.00035  1.34524E+00 0.00027  8.93065E+00 0.00278 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.23674E-05 0.00242  2.23627E-05 0.00243  2.36908E-05 0.03454 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.49832E-05 0.00237  2.49779E-05 0.00237  2.64504E-05 0.03442 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.54465E-03 0.02697  2.05799E-04 0.16468  9.92978E-04 0.06623  9.87276E-04 0.06742  3.04939E-03 0.03803  9.95281E-04 0.07491  3.13924E-04 0.12148 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.75498E-01 0.06239  1.24907E-02 8.7E-06  3.14784E-02 0.00121  1.10229E-01 0.00125  3.20860E-01 0.00102  1.34615E+00 0.00064  8.83980E+00 0.00527 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.54087E-03 0.02657  2.05757E-04 0.16283  1.00452E-03 0.06453  9.89868E-04 0.06464  3.05376E-03 0.03762  9.94011E-04 0.07347  2.92955E-04 0.11738 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.56143E-01 0.05886  1.24907E-02 8.6E-06  3.14801E-02 0.00121  1.10205E-01 0.00123  3.20891E-01 0.00102  1.34604E+00 0.00064  8.84404E+00 0.00532 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.93949E+02 0.02721 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.25632E-05 0.00065 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.52018E-05 0.00040 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.55560E-03 0.00515 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.90653E+02 0.00525 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.61141E-07 0.00068 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.86653E-06 0.00039  2.86618E-06 0.00039  2.91832E-06 0.00545 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.66177E-05 0.00083  3.66352E-05 0.00083  3.42058E-05 0.00888 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.61068E-01 0.00032  6.60499E-01 0.00033  7.68415E-01 0.00899 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04136E+01 0.01274 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.44672E+01 0.00044  3.36744E+01 0.00041 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.31058E+04 0.00285  2.93871E+05 0.00137  6.04557E+05 0.00091  6.50999E+05 0.00080  5.99986E+05 0.00071  6.45168E+05 0.00064  4.37759E+05 0.00059  3.87261E+05 0.00072  2.96638E+05 0.00054  2.42781E+05 0.00076  2.08810E+05 0.00084  1.88381E+05 0.00078  1.74026E+05 0.00068  1.65315E+05 0.00080  1.61103E+05 0.00055  1.39179E+05 0.00097  1.37269E+05 0.00085  1.36039E+05 0.00081  1.33529E+05 0.00096  2.60594E+05 0.00062  2.51188E+05 0.00067  1.81163E+05 0.00090  1.16941E+05 0.00098  1.34935E+05 0.00093  1.27644E+05 0.00097  1.15422E+05 0.00119  1.89014E+05 0.00097  4.33402E+04 0.00160  5.43852E+04 0.00142  4.93866E+04 0.00143  2.87438E+04 0.00195  4.98842E+04 0.00138  3.38148E+04 0.00168  2.88006E+04 0.00168  5.42492E+03 0.00300  5.36554E+03 0.00394  5.36553E+03 0.00397  5.53280E+03 0.00242  5.47732E+03 0.00341  5.45657E+03 0.00390  5.72273E+03 0.00365  5.40865E+03 0.00388  1.01899E+04 0.00239  1.62567E+04 0.00277  2.07371E+04 0.00236  5.51375E+04 0.00172  5.78048E+04 0.00113  6.23432E+04 0.00115  4.16463E+04 0.00143  3.04444E+04 0.00143  2.30490E+04 0.00136  2.71187E+04 0.00161  5.17798E+04 0.00115  7.02160E+04 0.00122  1.34890E+05 0.00103  2.06407E+05 0.00106  3.05270E+05 0.00122  1.93980E+05 0.00118  1.38412E+05 0.00143  9.91236E+04 0.00135  8.88977E+04 0.00134  8.73589E+04 0.00109  7.28346E+04 0.00146  4.92323E+04 0.00140  4.54668E+04 0.00143  4.03787E+04 0.00193  3.41275E+04 0.00171  2.68345E+04 0.00168  1.78847E+04 0.00182  6.34686E+03 0.00154 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.12448E+00 0.00045 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.15824E+18 0.00044  2.95759E+17 0.00099 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.36974E-01 0.00010  1.50774E+00 0.00032 ];
INF_CAPT                  (idx, [1:   4]) = [  6.61744E-03 0.00054  3.12774E-02 0.00041 ];
INF_ABS                   (idx, [1:   4]) = [  8.97093E-03 0.00046  6.88445E-02 0.00071 ];
INF_FISS                  (idx, [1:   4]) = [  2.35349E-03 0.00048  3.75672E-02 0.00096 ];
INF_NSF                   (idx, [1:   4]) = [  6.04955E-03 0.00050  9.31161E-02 0.00097 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.57046E+00 6.0E-05  2.47866E+00 1.6E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03929E+02 5.4E-06  2.02819E+02 2.7E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.82838E-08 0.00039  2.53053E-06 0.00014 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.27992E-01 0.00011  1.43889E+00 0.00037 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42828E-01 0.00020  3.82436E-01 0.00048 ];
INF_SCATT2                (idx, [1:   4]) = [  9.57969E-02 0.00039  9.12468E-02 0.00106 ];
INF_SCATT3                (idx, [1:   4]) = [  7.33528E-03 0.00342  2.74312E-02 0.00290 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02404E-02 0.00188 -8.17865E-03 0.00787 ];
INF_SCATT5                (idx, [1:   4]) = [  1.41911E-04 0.12754  6.22362E-03 0.00600 ];
INF_SCATT6                (idx, [1:   4]) = [  5.02912E-03 0.00317 -1.61084E-02 0.00319 ];
INF_SCATT7                (idx, [1:   4]) = [  7.15124E-04 0.02098  2.57281E-04 0.20405 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28031E-01 0.00011  1.43889E+00 0.00037 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42829E-01 0.00020  3.82436E-01 0.00048 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.57971E-02 0.00039  9.12468E-02 0.00106 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.33497E-03 0.00341  2.74312E-02 0.00290 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02403E-02 0.00187 -8.17865E-03 0.00787 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.41886E-04 0.12740  6.22362E-03 0.00600 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.02908E-03 0.00316 -1.61084E-02 0.00319 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.15384E-04 0.02097  2.57281E-04 0.20405 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13794E-01 0.00023  9.72159E-01 0.00028 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55913E+00 0.00023  3.42880E-01 0.00028 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.93159E-03 0.00048  6.88445E-02 0.00071 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69417E-02 0.00024  7.00810E-02 0.00081 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10032E-01 0.00011  1.79597E-02 0.00032  1.22615E-03 0.00402  1.43766E+00 0.00037 ];
INF_S1                    (idx, [1:   8]) = [  2.37584E-01 0.00020  5.24386E-03 0.00093  5.15945E-04 0.00724  3.81920E-01 0.00048 ];
INF_S2                    (idx, [1:   8]) = [  9.73513E-02 0.00038 -1.55433E-03 0.00238  2.80815E-04 0.01130  9.09660E-02 0.00106 ];
INF_S3                    (idx, [1:   8]) = [  9.18604E-03 0.00271 -1.85077E-03 0.00214  9.88049E-05 0.02436  2.73324E-02 0.00288 ];
INF_S4                    (idx, [1:   8]) = [ -9.62545E-03 0.00193 -6.14978E-04 0.00524 -2.39742E-06 0.83229 -8.17626E-03 0.00789 ];
INF_S5                    (idx, [1:   8]) = [  1.21898E-04 0.14663  2.00134E-05 0.17581 -4.23712E-05 0.03738  6.26599E-03 0.00588 ];
INF_S6                    (idx, [1:   8]) = [  5.17099E-03 0.00302 -1.41873E-04 0.01640 -5.18952E-05 0.02820 -1.60565E-02 0.00320 ];
INF_S7                    (idx, [1:   8]) = [  8.84458E-04 0.01677 -1.69334E-04 0.01052 -4.52625E-05 0.03302  3.02543E-04 0.17431 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10071E-01 0.00011  1.79597E-02 0.00032  1.22615E-03 0.00402  1.43766E+00 0.00037 ];
INF_SP1                   (idx, [1:   8]) = [  2.37585E-01 0.00020  5.24386E-03 0.00093  5.15945E-04 0.00724  3.81920E-01 0.00048 ];
INF_SP2                   (idx, [1:   8]) = [  9.73514E-02 0.00038 -1.55433E-03 0.00238  2.80815E-04 0.01130  9.09660E-02 0.00106 ];
INF_SP3                   (idx, [1:   8]) = [  9.18574E-03 0.00270 -1.85077E-03 0.00214  9.88049E-05 0.02436  2.73324E-02 0.00288 ];
INF_SP4                   (idx, [1:   8]) = [ -9.62531E-03 0.00193 -6.14978E-04 0.00524 -2.39742E-06 0.83229 -8.17626E-03 0.00789 ];
INF_SP5                   (idx, [1:   8]) = [  1.21873E-04 0.14642  2.00134E-05 0.17581 -4.23712E-05 0.03738  6.26599E-03 0.00588 ];
INF_SP6                   (idx, [1:   8]) = [  5.17096E-03 0.00301 -1.41873E-04 0.01640 -5.18952E-05 0.02820 -1.60565E-02 0.00320 ];
INF_SP7                   (idx, [1:   8]) = [  8.84718E-04 0.01676 -1.69334E-04 0.01052 -4.52625E-05 0.03302  3.02543E-04 0.17431 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32121E-01 0.00059  1.08017E+00 0.00658 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33688E-01 0.00102  1.14437E+00 0.00870 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33457E-01 0.00090  1.15052E+00 0.00776 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29283E-01 0.00107  9.67928E-01 0.00701 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43605E+00 0.00059  3.08912E-01 0.00653 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42644E+00 0.00102  2.91810E-01 0.00868 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42785E+00 0.00090  2.90140E-01 0.00770 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45385E+00 0.00107  3.44786E-01 0.00703 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.65441E-03 0.00770  1.89120E-04 0.04985  1.05518E-03 0.01985  1.06520E-03 0.01893  3.02899E-03 0.01186  9.79496E-04 0.02103  3.36430E-04 0.03665 ];
LAMBDA                    (idx, [1:  14]) = [  8.17839E-01 0.01895  1.24905E-02 7.2E-06  3.15185E-02 0.00042  1.10158E-01 0.00045  3.21188E-01 0.00034  1.34423E+00 0.00075  8.92608E+00 0.00225 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0009' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:14:03 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284850 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00199E+00  9.96518E-01  9.98722E-01  1.00240E+00  1.00037E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15045E-02 0.00115  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88495E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.99695E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.00134E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68865E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.44072E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.44001E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.59226E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.12122E-01 0.00120  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000964 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00019E+04 0.00073 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00019E+04 0.00073 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.64073E+02 ;
RUNNING_TIME              (idx, 1)        =  3.32092E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.43100E-01  4.43100E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  9.79333E-02  9.20000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.23612E+01  3.25058E+00  2.52483E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  3.04017E-01  2.82333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.08333E-02  9.00002E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.32090E+01  1.20949E+02 ];
CPU_USAGE                 (idx, 1)        = 4.94061 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99839E+00 0.00021 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.72035E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.09254E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.83556E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.07357E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.56337E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.09126E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.52915E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.72641E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  5.62576E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.96926E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.54372E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.49305E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.08204E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.31995E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.68520E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.14545E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.16106E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.22195E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.43556E+11 ;
CS137_ACTIVITY            (idx, 1)        =  4.04885E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.92756E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.55472E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.06594E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.17498E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.14115E+12 0.00046  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 5 ;
BURNUP                     (idx, [1:  2])  = [  3.00000E+00  3.00025E+00 ];
BURN_DAYS                 (idx, 1)        =  7.50000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.92084E-01 0.00104 ];
U235_FISS                 (idx, [1:   4]) = [  1.12832E+16 0.00059  8.16882E-01 0.00034 ];
U238_FISS                 (idx, [1:   4]) = [  8.73069E+14 0.00276  6.31981E-02 0.00259 ];
PU239_FISS                (idx, [1:   4]) = [  1.62820E+15 0.00202  1.17874E-01 0.00193 ];
PU240_FISS                (idx, [1:   4]) = [  5.14946E+11 0.11080  3.73007E-05 0.11082 ];
PU241_FISS                (idx, [1:   4]) = [  2.36751E+13 0.01633  1.71379E-03 0.01632 ];
U235_CAPT                 (idx, [1:   4]) = [  2.49657E+15 0.00155  1.43317E-01 0.00148 ];
U238_CAPT                 (idx, [1:   4]) = [  7.77597E+15 0.00100  4.46349E-01 0.00068 ];
PU239_CAPT                (idx, [1:   4]) = [  9.16426E+14 0.00250  5.26062E-02 0.00243 ];
PU240_CAPT                (idx, [1:   4]) = [  2.20718E+14 0.00532  1.26696E-02 0.00528 ];
PU241_CAPT                (idx, [1:   4]) = [  8.54309E+12 0.02791  4.90507E-04 0.02787 ];
XE135_CAPT                (idx, [1:   4]) = [  7.12265E+14 0.00302  4.08889E-02 0.00300 ];
SM149_CAPT                (idx, [1:   4]) = [  1.63864E+14 0.00616  9.40747E-03 0.00618 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000964 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.51690E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000964 5.00752E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2769404 2.77296E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2195775 2.19876E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 35785 3.57905E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000964 5.00752E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -2.32831E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 0.0E+00  4.50056E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.47275E+16 1.3E-05  3.47275E+16 1.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38208E+16 2.0E-06  1.38208E+16 2.0E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.74332E+16 0.00043  1.28062E+16 0.00041  4.62704E+15 0.00110 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.12540E+16 0.00024  2.66270E+16 0.00020  4.62704E+15 0.00110 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.14115E+16 0.00046  3.14115E+16 0.00046  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.47381E+18 0.00044  4.01732E+17 0.00043  1.07208E+18 0.00049 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.24880E+14 0.00546 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.14789E+16 0.00025 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.08205E+18 0.00057 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12164E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12164E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.67800E+00 0.00042 ];
SIX_FF_F                  (idx, [1:   2]) = [  8.01048E-01 0.00030 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.60686E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25327E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95347E-01 3.1E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97484E-01 2.2E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.11292E+00 0.00050 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.10496E+00 0.00050 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.51270E+00 1.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03247E+02 2.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.10501E+00 0.00052  1.09794E+00 0.00050  7.01312E-03 0.00824 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.10490E+00 0.00024 ];
COL_KEFF                  (idx, [1:   2]) = [  1.10568E+00 0.00046 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.10490E+00 0.00024 ];
ABS_KINF                  (idx, [1:   2]) = [  1.11286E+00 0.00024 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73798E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73747E+01 8.3E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.67909E-07 0.00335 ];
IMP_EALF                  (idx, [1:   2]) = [  5.69549E-07 0.00145 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.16212E-01 0.00292 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.17074E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.89403E-03 0.00583  1.64608E-04 0.03173  9.58207E-04 0.01350  9.11091E-04 0.01415  2.70554E-03 0.00793  8.77050E-04 0.01446  2.77534E-04 0.02526 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.88476E-01 0.01309  1.08417E-02 0.01746  3.14829E-02 0.00033  1.10157E-01 0.00033  3.20871E-01 0.00022  1.34338E+00 0.00039  8.41620E+00 0.01134 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.38098E-03 0.00817  1.90064E-04 0.04896  1.04624E-03 0.01996  9.84011E-04 0.02054  2.91463E-03 0.01209  9.54445E-04 0.02217  2.91582E-04 0.03872 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.73583E-01 0.01947  1.24905E-02 7.1E-06  3.14811E-02 0.00046  1.10136E-01 0.00043  3.20765E-01 0.00032  1.34359E+00 0.00038  8.93427E+00 0.00260 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.26928E-05 0.00113  2.26838E-05 0.00113  2.41754E-05 0.01168 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.50723E-05 0.00100  2.50624E-05 0.00100  2.67116E-05 0.01167 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.33505E-03 0.00838  1.80622E-04 0.04945  1.03204E-03 0.02084  9.84674E-04 0.02151  2.89284E-03 0.01197  9.59502E-04 0.02300  2.85363E-04 0.04097 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.74725E-01 0.02073  1.24904E-02 1.0E-05  3.14830E-02 0.00056  1.10094E-01 0.00052  3.20716E-01 0.00036  1.34311E+00 0.00084  8.93464E+00 0.00361 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.22053E-05 0.00263  2.21996E-05 0.00264  2.33974E-05 0.03383 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.45333E-05 0.00256  2.45269E-05 0.00257  2.58619E-05 0.03388 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.20878E-03 0.02806  2.04310E-04 0.14940  1.01195E-03 0.07114  9.64383E-04 0.06874  2.76212E-03 0.04016  9.85366E-04 0.06867  2.80649E-04 0.12941 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.78222E-01 0.06408  1.24906E-02 1.4E-05  3.15196E-02 0.00116  1.10117E-01 0.00129  3.20656E-01 0.00101  1.34580E+00 0.00063  8.86447E+00 0.00580 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.16281E-03 0.02750  1.95444E-04 0.14734  1.01653E-03 0.06862  9.52960E-04 0.06637  2.72683E-03 0.03950  9.81626E-04 0.06684  2.89426E-04 0.12801 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.93622E-01 0.06457  1.24907E-02 1.4E-05  3.15230E-02 0.00114  1.10100E-01 0.00128  3.20641E-01 0.00100  1.34562E+00 0.00063  8.86560E+00 0.00578 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.80979E+02 0.02838 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.25092E-05 0.00072 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.48695E-05 0.00049 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.30866E-03 0.00480 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.80286E+02 0.00477 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.57681E-07 0.00071 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.85190E-06 0.00043  2.85171E-06 0.00043  2.88047E-06 0.00520 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.64345E-05 0.00089  3.64551E-05 0.00089  3.33499E-05 0.00868 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.58911E-01 0.00032  6.58414E-01 0.00032  7.56499E-01 0.00942 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03151E+01 0.01369 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.44001E+01 0.00046  3.35114E+01 0.00045 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.40290E+04 0.00400  2.95279E+05 0.00164  6.07104E+05 0.00100  6.51683E+05 0.00091  5.99924E+05 0.00065  6.45169E+05 0.00053  4.38179E+05 0.00064  3.87452E+05 0.00079  2.96569E+05 0.00066  2.42322E+05 0.00074  2.08750E+05 0.00096  1.88398E+05 0.00076  1.73996E+05 0.00098  1.65164E+05 0.00108  1.61070E+05 0.00100  1.39067E+05 0.00103  1.37414E+05 0.00085  1.36227E+05 0.00064  1.33733E+05 0.00090  2.60680E+05 0.00080  2.50978E+05 0.00080  1.81135E+05 0.00105  1.17204E+05 0.00087  1.35022E+05 0.00111  1.27464E+05 0.00119  1.15303E+05 0.00117  1.89001E+05 0.00065  4.33843E+04 0.00177  5.42607E+04 0.00146  4.92978E+04 0.00165  2.86161E+04 0.00184  4.97540E+04 0.00150  3.38631E+04 0.00195  2.86617E+04 0.00188  5.42359E+03 0.00397  5.24967E+03 0.00420  5.18463E+03 0.00353  5.25126E+03 0.00336  5.28388E+03 0.00296  5.33094E+03 0.00326  5.63349E+03 0.00373  5.38292E+03 0.00372  1.01720E+04 0.00364  1.62477E+04 0.00168  2.07676E+04 0.00208  5.48314E+04 0.00158  5.74065E+04 0.00153  6.18081E+04 0.00154  4.09801E+04 0.00169  2.98325E+04 0.00165  2.25213E+04 0.00163  2.64335E+04 0.00156  5.03767E+04 0.00126  6.90307E+04 0.00110  1.33053E+05 0.00108  2.04064E+05 0.00114  3.02678E+05 0.00127  1.92296E+05 0.00134  1.37261E+05 0.00124  9.84221E+04 0.00123  8.84344E+04 0.00144  8.69838E+04 0.00184  7.24327E+04 0.00149  4.89604E+04 0.00130  4.52166E+04 0.00153  4.01341E+04 0.00158  3.39725E+04 0.00172  2.67297E+04 0.00197  1.78585E+04 0.00203  6.30203E+03 0.00248 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.11365E+00 0.00036 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.17650E+18 0.00036  2.97339E+17 0.00110 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.36898E-01 0.00011  1.51481E+00 0.00036 ];
INF_CAPT                  (idx, [1:   4]) = [  6.72292E-03 0.00062  3.20328E-02 0.00046 ];
INF_ABS                   (idx, [1:   4]) = [  9.03953E-03 0.00052  6.93539E-02 0.00081 ];
INF_FISS                  (idx, [1:   4]) = [  2.31661E-03 0.00056  3.73211E-02 0.00111 ];
INF_NSF                   (idx, [1:   4]) = [  5.97361E-03 0.00056  9.31726E-02 0.00112 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.57860E+00 5.3E-05  2.49652E+00 2.0E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04031E+02 6.2E-06  2.03054E+02 3.2E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.80833E-08 0.00041  2.53460E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.27863E-01 0.00011  1.44551E+00 0.00043 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42735E-01 0.00015  3.84318E-01 0.00051 ];
INF_SCATT2                (idx, [1:   4]) = [  9.57753E-02 0.00033  9.16587E-02 0.00065 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35265E-03 0.00366  2.75038E-02 0.00223 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02163E-02 0.00266 -8.28206E-03 0.00828 ];
INF_SCATT5                (idx, [1:   4]) = [  1.80279E-04 0.10089  6.35124E-03 0.00810 ];
INF_SCATT6                (idx, [1:   4]) = [  5.06546E-03 0.00279 -1.61915E-02 0.00235 ];
INF_SCATT7                (idx, [1:   4]) = [  7.49382E-04 0.01847  3.10384E-04 0.15814 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.27903E-01 0.00011  1.44551E+00 0.00043 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42735E-01 0.00015  3.84318E-01 0.00051 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.57758E-02 0.00033  9.16587E-02 0.00065 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35284E-03 0.00366  2.75038E-02 0.00223 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02164E-02 0.00265 -8.28206E-03 0.00828 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.80539E-04 0.10072  6.35124E-03 0.00810 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.06536E-03 0.00279 -1.61915E-02 0.00235 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.49434E-04 0.01848  3.10384E-04 0.15814 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13611E-01 0.00027  9.77408E-01 0.00034 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56047E+00 0.00027  3.41039E-01 0.00034 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.99939E-03 0.00051  6.93539E-02 0.00081 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69249E-02 0.00020  7.05247E-02 0.00103 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.09973E-01 0.00011  1.78902E-02 0.00031  1.22655E-03 0.00423  1.44428E+00 0.00043 ];
INF_S1                    (idx, [1:   8]) = [  2.37520E-01 0.00016  5.21519E-03 0.00085  5.18580E-04 0.00711  3.83799E-01 0.00051 ];
INF_S2                    (idx, [1:   8]) = [  9.73303E-02 0.00033 -1.55495E-03 0.00252  2.87444E-04 0.00948  9.13712E-02 0.00065 ];
INF_S3                    (idx, [1:   8]) = [  9.19148E-03 0.00289 -1.83883E-03 0.00204  1.01918E-04 0.02501  2.74019E-02 0.00225 ];
INF_S4                    (idx, [1:   8]) = [ -9.60939E-03 0.00285 -6.06920E-04 0.00444  2.20642E-06 0.94466 -8.28427E-03 0.00832 ];
INF_S5                    (idx, [1:   8]) = [  1.59875E-04 0.11526  2.04042E-05 0.11163 -4.01254E-05 0.04098  6.39137E-03 0.00808 ];
INF_S6                    (idx, [1:   8]) = [  5.20698E-03 0.00278 -1.41525E-04 0.01823 -5.42542E-05 0.02544 -1.61373E-02 0.00234 ];
INF_S7                    (idx, [1:   8]) = [  9.18619E-04 0.01443 -1.69238E-04 0.01227 -4.71228E-05 0.02551  3.57507E-04 0.13605 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10013E-01 0.00011  1.78902E-02 0.00031  1.22655E-03 0.00423  1.44428E+00 0.00043 ];
INF_SP1                   (idx, [1:   8]) = [  2.37520E-01 0.00016  5.21519E-03 0.00085  5.18580E-04 0.00711  3.83799E-01 0.00051 ];
INF_SP2                   (idx, [1:   8]) = [  9.73307E-02 0.00033 -1.55495E-03 0.00252  2.87444E-04 0.00948  9.13712E-02 0.00065 ];
INF_SP3                   (idx, [1:   8]) = [  9.19167E-03 0.00289 -1.83883E-03 0.00204  1.01918E-04 0.02501  2.74019E-02 0.00225 ];
INF_SP4                   (idx, [1:   8]) = [ -9.60945E-03 0.00285 -6.06920E-04 0.00444  2.20642E-06 0.94466 -8.28427E-03 0.00832 ];
INF_SP5                   (idx, [1:   8]) = [  1.60135E-04 0.11508  2.04042E-05 0.11163 -4.01254E-05 0.04098  6.39137E-03 0.00808 ];
INF_SP6                   (idx, [1:   8]) = [  5.20689E-03 0.00279 -1.41525E-04 0.01823 -5.42542E-05 0.02544 -1.61373E-02 0.00234 ];
INF_SP7                   (idx, [1:   8]) = [  9.18672E-04 0.01444 -1.69238E-04 0.01227 -4.71228E-05 0.02551  3.57507E-04 0.13605 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31858E-01 0.00073  1.07633E+00 0.00523 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33190E-01 0.00100  1.13968E+00 0.00703 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33307E-01 0.00104  1.14618E+00 0.00576 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29137E-01 0.00127  9.64580E-01 0.00544 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43768E+00 0.00073  3.09897E-01 0.00518 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42949E+00 0.00100  2.92823E-01 0.00697 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42877E+00 0.00104  2.91049E-01 0.00567 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45479E+00 0.00128  3.45819E-01 0.00544 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.38098E-03 0.00817  1.90064E-04 0.04896  1.04624E-03 0.01996  9.84011E-04 0.02054  2.91463E-03 0.01209  9.54445E-04 0.02217  2.91582E-04 0.03872 ];
LAMBDA                    (idx, [1:  14]) = [  7.73583E-01 0.01947  1.24905E-02 7.1E-06  3.14811E-02 0.00046  1.10136E-01 0.00043  3.20765E-01 0.00032  1.34359E+00 0.00038  8.93427E+00 0.00260 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0009' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:19:54 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284850 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00191E+00  1.00040E+00  9.99576E-01  9.98549E-01  9.99563E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14563E-02 0.00113  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88544E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.01153E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.01590E-01 1.0E-04  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68182E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.43583E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.43510E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.50210E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.08662E-01 0.00120  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000844 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00017E+04 0.00075 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00017E+04 0.00075 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.93328E+02 ;
RUNNING_TIME              (idx, 1)        =  3.90661E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.43100E-01  4.43100E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.18333E-01  9.86667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.81420E+01  3.23513E+00  2.54562E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  3.59233E-01  2.74000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.33500E-02  8.49998E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.90660E+01  1.21256E+02 ];
CPU_USAGE                 (idx, 1)        = 4.94874 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99941E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.73972E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.14536E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.82436E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.88594E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.57872E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.10188E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.56662E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.71415E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  6.52073E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.07495E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.95296E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.57077E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.56777E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.41787E+07 ;
SR90_ACTIVITY             (idx, 1)        =  4.84383E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.17158E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.20454E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.25767E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.20093E+12 ;
CS137_ACTIVITY            (idx, 1)        =  5.39818E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.96120E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.52811E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  7.63312E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.18504E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.19785E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 6 ;
BURNUP                     (idx, [1:  2])  = [  4.00000E+00  4.00034E+00 ];
BURN_DAYS                 (idx, 1)        =  1.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.01206E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  1.08369E+16 0.00068  7.84748E-01 0.00035 ];
U238_FISS                 (idx, [1:   4]) = [  8.92151E+14 0.00287  6.45974E-02 0.00273 ];
PU239_FISS                (idx, [1:   4]) = [  2.02782E+15 0.00168  1.46846E-01 0.00160 ];
PU240_FISS                (idx, [1:   4]) = [  9.66112E+11 0.08323  6.99665E-05 0.08319 ];
PU241_FISS                (idx, [1:   4]) = [  4.68395E+13 0.01160  3.39153E-03 0.01156 ];
U235_CAPT                 (idx, [1:   4]) = [  2.40403E+15 0.00160  1.33700E-01 0.00154 ];
U238_CAPT                 (idx, [1:   4]) = [  7.86569E+15 0.00101  4.37408E-01 0.00069 ];
PU239_CAPT                (idx, [1:   4]) = [  1.13834E+15 0.00228  6.33070E-02 0.00222 ];
PU240_CAPT                (idx, [1:   4]) = [  3.38887E+14 0.00448  1.88446E-02 0.00440 ];
PU241_CAPT                (idx, [1:   4]) = [  1.76580E+13 0.02007  9.82297E-04 0.02008 ];
XE135_CAPT                (idx, [1:   4]) = [  7.15630E+14 0.00286  3.97998E-02 0.00283 ];
SM149_CAPT                (idx, [1:   4]) = [  1.68609E+14 0.00623  9.37778E-03 0.00624 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000844 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.61951E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000844 5.00762E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2807690 2.81156E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2156370 2.15927E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 36784 3.67931E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000844 5.00762E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.98492E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 0.0E+00  4.50056E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.48884E+16 1.4E-05  3.48884E+16 1.4E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38082E+16 2.2E-06  1.38082E+16 2.2E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.79766E+16 0.00045  1.32704E+16 0.00048  4.70615E+15 0.00108 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.17848E+16 0.00026  2.70786E+16 0.00023  4.70615E+15 0.00108 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.19785E+16 0.00051  3.19785E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.49697E+18 0.00046  4.06657E+17 0.00046  1.09031E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.35374E+14 0.00575 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.20201E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.10002E+18 0.00058 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12048E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12048E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.66741E+00 0.00044 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.99684E-01 0.00030 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.57715E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25345E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95183E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97446E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.09921E+00 0.00051 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.09112E+00 0.00051 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.52664E+00 1.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03432E+02 2.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.09118E+00 0.00053  1.08411E+00 0.00052  7.01757E-03 0.00793 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.09127E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.09114E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.09127E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.09936E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73438E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73446E+01 8.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.88796E-07 0.00341 ];
IMP_EALF                  (idx, [1:   2]) = [  5.86976E-07 0.00151 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.21285E-01 0.00298 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.20836E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.95485E-03 0.00554  1.65139E-04 0.03336  9.74979E-04 0.01430  9.49709E-04 0.01317  2.69182E-03 0.00813  8.92984E-04 0.01440  2.80216E-04 0.02569 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.90429E-01 0.01315  1.02692E-02 0.02083  3.14449E-02 0.00034  1.10171E-01 0.00032  3.21133E-01 0.00025  1.34245E+00 0.00041  8.45165E+00 0.01110 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.46168E-03 0.00780  1.71952E-04 0.05048  1.05125E-03 0.02108  1.02723E-03 0.01933  2.91714E-03 0.01206  9.97384E-04 0.02173  2.96721E-04 0.03545 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.88551E-01 0.01812  1.24946E-02 0.00024  3.14336E-02 0.00049  1.10165E-01 0.00044  3.21214E-01 0.00036  1.34179E+00 0.00059  8.93915E+00 0.00320 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.27548E-05 0.00118  2.27460E-05 0.00119  2.41361E-05 0.01244 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.48264E-05 0.00108  2.48167E-05 0.00109  2.63346E-05 0.01244 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.42630E-03 0.00809  1.65013E-04 0.05600  1.06221E-03 0.02187  1.01691E-03 0.02202  2.91201E-03 0.01243  9.63800E-04 0.02255  3.06357E-04 0.03979 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.94448E-01 0.02084  1.24903E-02 1.2E-05  3.14341E-02 0.00058  1.10198E-01 0.00054  3.21059E-01 0.00038  1.34192E+00 0.00076  8.96197E+00 0.00291 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.22184E-05 0.00279  2.22148E-05 0.00279  2.24411E-05 0.02789 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.42401E-05 0.00272  2.42361E-05 0.00271  2.44889E-05 0.02790 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.50084E-03 0.02800  1.29597E-04 0.19140  1.06936E-03 0.07168  1.06755E-03 0.07299  3.03709E-03 0.04024  8.29895E-04 0.07647  3.67347E-04 0.13291 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.33409E-01 0.06855  1.24904E-02 1.8E-05  3.13969E-02 0.00135  1.10482E-01 0.00137  3.21381E-01 0.00112  1.34432E+00 0.00072  8.89276E+00 0.00571 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.42681E-03 0.02746  1.34089E-04 0.18189  1.06877E-03 0.06899  1.03874E-03 0.07230  2.99145E-03 0.03973  8.38071E-04 0.07318  3.55694E-04 0.12899 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.33444E-01 0.06735  1.24904E-02 1.8E-05  3.14001E-02 0.00133  1.10471E-01 0.00136  3.21377E-01 0.00110  1.34442E+00 0.00071  8.89246E+00 0.00570 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.95062E+02 0.02862 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.25000E-05 0.00072 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.45480E-05 0.00048 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.31269E-03 0.00488 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.80647E+02 0.00493 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.55937E-07 0.00072 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.83308E-06 0.00044  2.83287E-06 0.00044  2.86781E-06 0.00509 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.64029E-05 0.00085  3.64228E-05 0.00086  3.34755E-05 0.00907 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.55869E-01 0.00031  6.55405E-01 0.00032  7.44512E-01 0.00859 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04436E+01 0.01298 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.43510E+01 0.00045  3.33530E+01 0.00045 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.38377E+04 0.00416  2.95726E+05 0.00181  6.06650E+05 0.00100  6.51355E+05 0.00084  6.00159E+05 0.00073  6.44169E+05 0.00062  4.37316E+05 0.00058  3.87885E+05 0.00077  2.96299E+05 0.00098  2.41881E+05 0.00071  2.08583E+05 0.00074  1.88150E+05 0.00117  1.73641E+05 0.00082  1.65270E+05 0.00073  1.60893E+05 0.00101  1.38991E+05 0.00087  1.37346E+05 0.00072  1.35816E+05 0.00086  1.33404E+05 0.00091  2.60673E+05 0.00074  2.50954E+05 0.00078  1.81031E+05 0.00072  1.16987E+05 0.00102  1.34987E+05 0.00111  1.27564E+05 0.00079  1.15093E+05 0.00102  1.88289E+05 0.00077  4.31358E+04 0.00204  5.42998E+04 0.00134  4.92335E+04 0.00161  2.85830E+04 0.00191  4.95770E+04 0.00149  3.36181E+04 0.00175  2.85382E+04 0.00190  5.32702E+03 0.00365  5.15197E+03 0.00278  5.01917E+03 0.00380  5.01361E+03 0.00385  5.03038E+03 0.00331  5.18215E+03 0.00344  5.55909E+03 0.00352  5.26440E+03 0.00407  1.00714E+04 0.00322  1.61201E+04 0.00183  2.06012E+04 0.00168  5.43639E+04 0.00146  5.69777E+04 0.00151  6.11713E+04 0.00134  4.05274E+04 0.00147  2.92524E+04 0.00151  2.20435E+04 0.00153  2.58338E+04 0.00183  4.95316E+04 0.00143  6.79288E+04 0.00143  1.31510E+05 0.00127  2.02687E+05 0.00108  3.00658E+05 0.00127  1.91385E+05 0.00139  1.36722E+05 0.00146  9.81325E+04 0.00139  8.80192E+04 0.00141  8.67037E+04 0.00139  7.22350E+04 0.00157  4.88139E+04 0.00197  4.50774E+04 0.00147  4.00722E+04 0.00194  3.38655E+04 0.00187  2.66524E+04 0.00168  1.78358E+04 0.00201  6.30962E+03 0.00266 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.09922E+00 0.00059 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.19642E+18 0.00051  3.00588E+17 0.00110 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37113E-01 0.00013  1.52180E+00 0.00032 ];
INF_CAPT                  (idx, [1:   4]) = [  6.83382E-03 0.00077  3.26077E-02 0.00054 ];
INF_ABS                   (idx, [1:   4]) = [  9.11179E-03 0.00064  6.94847E-02 0.00085 ];
INF_FISS                  (idx, [1:   4]) = [  2.27797E-03 0.00055  3.68770E-02 0.00113 ];
INF_NSF                   (idx, [1:   4]) = [  5.89108E-03 0.00055  9.26359E-02 0.00114 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.58611E+00 4.4E-05  2.51202E+00 2.4E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04128E+02 5.5E-06  2.03260E+02 4.0E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.78379E-08 0.00039  2.53862E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.27991E-01 0.00014  1.45234E+00 0.00037 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42896E-01 0.00020  3.86103E-01 0.00044 ];
INF_SCATT2                (idx, [1:   4]) = [  9.58335E-02 0.00030  9.20106E-02 0.00064 ];
INF_SCATT3                (idx, [1:   4]) = [  7.33337E-03 0.00334  2.76062E-02 0.00259 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02250E-02 0.00198 -8.35920E-03 0.00780 ];
INF_SCATT5                (idx, [1:   4]) = [  1.48818E-04 0.11660  6.37808E-03 0.00816 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07346E-03 0.00344 -1.63642E-02 0.00240 ];
INF_SCATT7                (idx, [1:   4]) = [  7.30227E-04 0.01920  2.68220E-04 0.17143 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28031E-01 0.00014  1.45234E+00 0.00037 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42897E-01 0.00020  3.86103E-01 0.00044 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.58337E-02 0.00030  9.20106E-02 0.00064 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.33335E-03 0.00335  2.76062E-02 0.00259 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02250E-02 0.00198 -8.35920E-03 0.00780 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.48782E-04 0.11733  6.37808E-03 0.00816 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07340E-03 0.00345 -1.63642E-02 0.00240 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.30029E-04 0.01921  2.68220E-04 0.17143 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13590E-01 0.00024  9.82372E-01 0.00030 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56063E+00 0.00024  3.39315E-01 0.00030 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.07106E-03 0.00064  6.94847E-02 0.00085 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69518E-02 0.00027  7.06971E-02 0.00092 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10161E-01 0.00013  1.78296E-02 0.00046  1.23874E-03 0.00407  1.45110E+00 0.00038 ];
INF_S1                    (idx, [1:   8]) = [  2.37701E-01 0.00020  5.19485E-03 0.00077  5.28573E-04 0.00660  3.85575E-01 0.00044 ];
INF_S2                    (idx, [1:   8]) = [  9.73913E-02 0.00029 -1.55782E-03 0.00242  2.90843E-04 0.00832  9.17198E-02 0.00064 ];
INF_S3                    (idx, [1:   8]) = [  9.16750E-03 0.00268 -1.83413E-03 0.00168  1.04569E-04 0.01993  2.75017E-02 0.00262 ];
INF_S4                    (idx, [1:   8]) = [ -9.62315E-03 0.00209 -6.01844E-04 0.00566  2.89927E-06 0.57160 -8.36210E-03 0.00785 ];
INF_S5                    (idx, [1:   8]) = [  1.27606E-04 0.12873  2.12124E-05 0.14534 -4.29693E-05 0.03257  6.42105E-03 0.00806 ];
INF_S6                    (idx, [1:   8]) = [  5.21579E-03 0.00332 -1.42328E-04 0.02115 -5.35598E-05 0.02664 -1.63106E-02 0.00239 ];
INF_S7                    (idx, [1:   8]) = [  9.01846E-04 0.01580 -1.71619E-04 0.01281 -4.69665E-05 0.03290  3.15187E-04 0.14677 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10202E-01 0.00013  1.78296E-02 0.00046  1.23874E-03 0.00407  1.45110E+00 0.00038 ];
INF_SP1                   (idx, [1:   8]) = [  2.37702E-01 0.00020  5.19485E-03 0.00077  5.28573E-04 0.00660  3.85575E-01 0.00044 ];
INF_SP2                   (idx, [1:   8]) = [  9.73915E-02 0.00029 -1.55782E-03 0.00242  2.90843E-04 0.00832  9.17198E-02 0.00064 ];
INF_SP3                   (idx, [1:   8]) = [  9.16748E-03 0.00269 -1.83413E-03 0.00168  1.04569E-04 0.01993  2.75017E-02 0.00262 ];
INF_SP4                   (idx, [1:   8]) = [ -9.62315E-03 0.00210 -6.01844E-04 0.00566  2.89927E-06 0.57160 -8.36210E-03 0.00785 ];
INF_SP5                   (idx, [1:   8]) = [  1.27569E-04 0.12949  2.12124E-05 0.14534 -4.29693E-05 0.03257  6.42105E-03 0.00806 ];
INF_SP6                   (idx, [1:   8]) = [  5.21572E-03 0.00333 -1.42328E-04 0.02115 -5.35598E-05 0.02664 -1.63106E-02 0.00239 ];
INF_SP7                   (idx, [1:   8]) = [  9.01647E-04 0.01581 -1.71619E-04 0.01281 -4.69665E-05 0.03290  3.15187E-04 0.14677 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31928E-01 0.00064  1.10024E+00 0.00848 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33369E-01 0.00105  1.17240E+00 0.00893 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33546E-01 0.00084  1.17541E+00 0.01049 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28938E-01 0.00105  9.78412E-01 0.00846 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43724E+00 0.00064  3.03475E-01 0.00828 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42839E+00 0.00105  2.84860E-01 0.00892 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42729E+00 0.00084  2.84312E-01 0.01014 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45604E+00 0.00105  3.41253E-01 0.00817 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.46168E-03 0.00780  1.71952E-04 0.05048  1.05125E-03 0.02108  1.02723E-03 0.01933  2.91714E-03 0.01206  9.97384E-04 0.02173  2.96721E-04 0.03545 ];
LAMBDA                    (idx, [1:  14]) = [  7.88551E-01 0.01812  1.24946E-02 0.00024  3.14336E-02 0.00049  1.10165E-01 0.00044  3.21214E-01 0.00036  1.34179E+00 0.00059  8.93915E+00 0.00320 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0009' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:25:45 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284850 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00129E+00  9.99809E-01  9.99727E-01  9.98695E-01  1.00048E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14559E-02 0.00115  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88544E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.01970E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.02407E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67637E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.42745E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.42670E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.43772E+00 0.00036  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.07089E-01 0.00120  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000511 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00010E+04 0.00078 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00010E+04 0.00078 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.22476E+02 ;
RUNNING_TIME              (idx, 1)        =  4.49021E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.43100E-01  4.43100E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.37850E-01  9.08333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  4.39025E+01  3.23695E+00  2.52357E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  4.14567E-01  2.70500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.52167E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.49020E+01  1.20705E+02 ];
CPU_USAGE                 (idx, 1)        = 4.95469 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00068E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75386E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.18800E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.81451E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.04085E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.59564E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.11362E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.59234E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.70312E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  7.41116E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.15729E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.44307E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.65622E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.96809E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.49167E+07 ;
SR90_ACTIVITY             (idx, 1)        =  5.97389E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.19467E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.23802E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.28906E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.91927E+12 ;
CS137_ACTIVITY            (idx, 1)        =  6.74671E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.99038E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.50510E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.12382E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.19378E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.24781E+12 0.00047  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 7 ;
BURNUP                     (idx, [1:  2])  = [  5.00000E+00  5.00042E+00 ];
BURN_DAYS                 (idx, 1)        =  1.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.10314E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  1.04318E+16 0.00068  7.55240E-01 0.00038 ];
U238_FISS                 (idx, [1:   4]) = [  9.07411E+14 0.00267  6.56838E-02 0.00248 ];
PU239_FISS                (idx, [1:   4]) = [  2.38697E+15 0.00156  1.72810E-01 0.00143 ];
PU240_FISS                (idx, [1:   4]) = [  1.29549E+12 0.07086  9.38112E-05 0.07079 ];
PU241_FISS                (idx, [1:   4]) = [  7.97675E+13 0.00866  5.77503E-03 0.00863 ];
U235_CAPT                 (idx, [1:   4]) = [  2.31897E+15 0.00171  1.25550E-01 0.00165 ];
U238_CAPT                 (idx, [1:   4]) = [  7.95165E+15 0.00098  4.30471E-01 0.00066 ];
PU239_CAPT                (idx, [1:   4]) = [  1.33330E+15 0.00211  7.21884E-02 0.00210 ];
PU240_CAPT                (idx, [1:   4]) = [  4.58431E+14 0.00375  2.48181E-02 0.00369 ];
PU241_CAPT                (idx, [1:   4]) = [  2.80761E+13 0.01585  1.51954E-03 0.01580 ];
XE135_CAPT                (idx, [1:   4]) = [  7.17425E+14 0.00303  3.88391E-02 0.00295 ];
SM149_CAPT                (idx, [1:   4]) = [  1.72008E+14 0.00585  9.31419E-03 0.00589 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000511 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.55376E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000511 5.00755E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2839537 2.84366E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2123618 2.12654E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 37356 3.73565E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000511 5.00755E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -1.22003E-07 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 0.0E+00  4.50056E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.50347E+16 1.5E-05  3.50347E+16 1.5E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37967E+16 2.6E-06  1.37967E+16 2.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.84805E+16 0.00044  1.37249E+16 0.00044  4.75557E+15 0.00116 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.22772E+16 0.00025  2.75216E+16 0.00022  4.75557E+15 0.00116 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.24781E+16 0.00047  3.24781E+16 0.00047  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.51763E+18 0.00045  4.11695E+17 0.00043  1.10594E+18 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.42661E+14 0.00540 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.25198E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.11447E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11931E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11931E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.65781E+00 0.00045 ];
SIX_FF_F                  (idx, [1:   2]) = [  8.00163E-01 0.00030 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.54940E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25264E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95149E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97367E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.08821E+00 0.00054 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.08008E+00 0.00054 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.53935E+00 1.8E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03601E+02 2.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07977E+00 0.00055  1.07341E+00 0.00055  6.67391E-03 0.00827 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07902E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07883E+00 0.00046 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07902E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.08715E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73177E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73165E+01 8.1E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.04280E-07 0.00333 ];
IMP_EALF                  (idx, [1:   2]) = [  6.03649E-07 0.00140 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.25346E-01 0.00276 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.24611E-01 0.00117 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.85413E-03 0.00577  1.65322E-04 0.03348  9.86421E-04 0.01357  9.14605E-04 0.01481  2.64021E-03 0.00823  8.67016E-04 0.01478  2.80562E-04 0.02581 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.95718E-01 0.01354  1.04927E-02 0.01954  3.14314E-02 0.00034  1.10194E-01 0.00036  3.21285E-01 0.00026  1.34116E+00 0.00049  8.60813E+00 0.00932 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.27128E-03 0.00817  1.76019E-04 0.05013  1.09366E-03 0.02013  9.62702E-04 0.02092  2.83243E-03 0.01201  9.07133E-04 0.02197  2.99331E-04 0.03776 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.89354E-01 0.01934  1.24913E-02 0.00010  3.14332E-02 0.00047  1.10190E-01 0.00048  3.21343E-01 0.00039  1.34123E+00 0.00062  8.97071E+00 0.00307 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.28000E-05 0.00123  2.27867E-05 0.00123  2.50009E-05 0.01266 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.46146E-05 0.00106  2.46002E-05 0.00107  2.69919E-05 0.01266 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.16908E-03 0.00842  1.77702E-04 0.05028  1.04125E-03 0.02152  9.79893E-04 0.02276  2.78610E-03 0.01279  8.86615E-04 0.02423  2.97513E-04 0.04071 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.91362E-01 0.02164  1.24922E-02 0.00016  3.14464E-02 0.00056  1.10199E-01 0.00059  3.21300E-01 0.00043  1.33947E+00 0.00105  8.94519E+00 0.00421 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.22018E-05 0.00287  2.21832E-05 0.00289  2.44065E-05 0.03497 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.39686E-05 0.00280  2.39485E-05 0.00282  2.63520E-05 0.03493 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.23499E-03 0.02912  1.63047E-04 0.17783  1.04231E-03 0.07684  1.01165E-03 0.07056  2.79675E-03 0.04280  8.51289E-04 0.07557  3.69941E-04 0.11202 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  9.44196E-01 0.07323  1.25031E-02 0.00102  3.13768E-02 0.00139  1.10354E-01 0.00141  3.22008E-01 0.00124  1.34075E+00 0.00194  9.12831E+00 0.00712 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.23645E-03 0.02888  1.64140E-04 0.17719  1.04525E-03 0.07436  1.00964E-03 0.06878  2.81362E-03 0.04241  8.34324E-04 0.07297  3.69475E-04 0.10872 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  9.41987E-01 0.07062  1.25039E-02 0.00109  3.13726E-02 0.00138  1.10348E-01 0.00140  3.21961E-01 0.00123  1.34059E+00 0.00193  9.12869E+00 0.00712 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.82497E+02 0.02939 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.25443E-05 0.00073 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.43390E-05 0.00048 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.10608E-03 0.00618 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.70983E+02 0.00630 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.52580E-07 0.00074 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.81946E-06 0.00041  2.81939E-06 0.00042  2.83021E-06 0.00524 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.62620E-05 0.00090  3.62791E-05 0.00090  3.36241E-05 0.00946 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.53125E-01 0.00033  6.52679E-01 0.00034  7.41398E-01 0.00933 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06709E+01 0.01303 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.42670E+01 0.00046  3.32392E+01 0.00043 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.46258E+04 0.00358  2.96877E+05 0.00155  6.07167E+05 0.00066  6.51972E+05 0.00089  5.99868E+05 0.00067  6.43670E+05 0.00051  4.37724E+05 0.00080  3.87754E+05 0.00060  2.96151E+05 0.00066  2.42228E+05 0.00080  2.08789E+05 0.00079  1.87970E+05 0.00079  1.73701E+05 0.00074  1.65075E+05 0.00100  1.60572E+05 0.00073  1.39080E+05 0.00097  1.37454E+05 0.00093  1.35830E+05 0.00107  1.33514E+05 0.00087  2.60578E+05 0.00073  2.50853E+05 0.00073  1.80971E+05 0.00093  1.17030E+05 0.00082  1.35035E+05 0.00090  1.27725E+05 0.00081  1.15077E+05 0.00108  1.87989E+05 0.00069  4.32565E+04 0.00126  5.42369E+04 0.00153  4.92368E+04 0.00139  2.86119E+04 0.00201  4.96659E+04 0.00174  3.36319E+04 0.00152  2.83133E+04 0.00148  5.29810E+03 0.00436  5.03241E+03 0.00346  4.84344E+03 0.00407  4.77220E+03 0.00324  4.83848E+03 0.00375  5.04062E+03 0.00417  5.44589E+03 0.00352  5.18592E+03 0.00395  9.93993E+03 0.00305  1.60327E+04 0.00287  2.03714E+04 0.00199  5.40826E+04 0.00169  5.67507E+04 0.00128  6.07944E+04 0.00111  4.00490E+04 0.00167  2.87329E+04 0.00151  2.15298E+04 0.00169  2.53024E+04 0.00127  4.85217E+04 0.00128  6.68530E+04 0.00116  1.29630E+05 0.00103  2.00312E+05 0.00124  2.98328E+05 0.00119  1.90063E+05 0.00129  1.35728E+05 0.00132  9.74287E+04 0.00127  8.74539E+04 0.00140  8.60897E+04 0.00157  7.17805E+04 0.00143  4.85870E+04 0.00128  4.47414E+04 0.00155  3.98032E+04 0.00133  3.36833E+04 0.00162  2.64855E+04 0.00163  1.77135E+04 0.00181  6.26230E+03 0.00180 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.08695E+00 0.00047 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.21519E+18 0.00051  3.02471E+17 0.00093 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37171E-01 9.3E-05  1.52648E+00 0.00027 ];
INF_CAPT                  (idx, [1:   4]) = [  6.94894E-03 0.00060  3.31833E-02 0.00046 ];
INF_ABS                   (idx, [1:   4]) = [  9.19247E-03 0.00046  6.97886E-02 0.00069 ];
INF_FISS                  (idx, [1:   4]) = [  2.24353E-03 0.00038  3.66053E-02 0.00092 ];
INF_NSF                   (idx, [1:   4]) = [  5.81816E-03 0.00039  9.24674E-02 0.00093 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.59331E+00 6.3E-05  2.52607E+00 3.0E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04221E+02 6.3E-06  2.03449E+02 5.0E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.76357E-08 0.00048  2.54115E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.27977E-01 0.00010  1.45668E+00 0.00032 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43002E-01 0.00019  3.87425E-01 0.00051 ];
INF_SCATT2                (idx, [1:   4]) = [  9.58882E-02 0.00027  9.23720E-02 0.00085 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35233E-03 0.00246  2.77640E-02 0.00261 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02627E-02 0.00163 -8.37060E-03 0.00712 ];
INF_SCATT5                (idx, [1:   4]) = [  1.63252E-04 0.11368  6.41229E-03 0.00824 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08310E-03 0.00385 -1.63812E-02 0.00306 ];
INF_SCATT7                (idx, [1:   4]) = [  7.64422E-04 0.02497  3.58947E-04 0.12006 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28018E-01 0.00010  1.45668E+00 0.00032 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43002E-01 0.00019  3.87425E-01 0.00051 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.58881E-02 0.00027  9.23720E-02 0.00085 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35234E-03 0.00246  2.77640E-02 0.00261 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02625E-02 0.00163 -8.37060E-03 0.00712 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.63276E-04 0.11375  6.41229E-03 0.00824 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08322E-03 0.00385 -1.63812E-02 0.00306 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.64260E-04 0.02499  3.58947E-04 0.12006 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13370E-01 0.00029  9.85952E-01 0.00027 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56224E+00 0.00029  3.38083E-01 0.00027 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.15209E-03 0.00044  6.97886E-02 0.00069 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69499E-02 0.00015  7.10554E-02 0.00094 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10221E-01 9.6E-05  1.77561E-02 0.00040  1.25494E-03 0.00379  1.45543E+00 0.00032 ];
INF_S1                    (idx, [1:   8]) = [  2.37828E-01 0.00019  5.17443E-03 0.00053  5.36288E-04 0.00694  3.86888E-01 0.00051 ];
INF_S2                    (idx, [1:   8]) = [  9.74378E-02 0.00026 -1.54960E-03 0.00235  2.92764E-04 0.00800  9.20792E-02 0.00085 ];
INF_S3                    (idx, [1:   8]) = [  9.18311E-03 0.00202 -1.83077E-03 0.00184  1.04618E-04 0.02302  2.76594E-02 0.00259 ];
INF_S4                    (idx, [1:   8]) = [ -9.66242E-03 0.00164 -6.00295E-04 0.00498  9.93964E-07 1.00000 -8.37159E-03 0.00707 ];
INF_S5                    (idx, [1:   8]) = [  1.40026E-04 0.12752  2.32268E-05 0.09758 -4.30004E-05 0.03911  6.45529E-03 0.00815 ];
INF_S6                    (idx, [1:   8]) = [  5.22345E-03 0.00376 -1.40348E-04 0.01735 -5.22160E-05 0.03405 -1.63290E-02 0.00310 ];
INF_S7                    (idx, [1:   8]) = [  9.33963E-04 0.02038 -1.69541E-04 0.01213 -4.66317E-05 0.02684  4.05579E-04 0.10668 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10261E-01 9.6E-05  1.77561E-02 0.00040  1.25494E-03 0.00379  1.45543E+00 0.00032 ];
INF_SP1                   (idx, [1:   8]) = [  2.37827E-01 0.00019  5.17443E-03 0.00053  5.36288E-04 0.00694  3.86888E-01 0.00051 ];
INF_SP2                   (idx, [1:   8]) = [  9.74377E-02 0.00027 -1.54960E-03 0.00235  2.92764E-04 0.00800  9.20792E-02 0.00085 ];
INF_SP3                   (idx, [1:   8]) = [  9.18312E-03 0.00201 -1.83077E-03 0.00184  1.04618E-04 0.02302  2.76594E-02 0.00259 ];
INF_SP4                   (idx, [1:   8]) = [ -9.66224E-03 0.00165 -6.00295E-04 0.00498  9.93964E-07 1.00000 -8.37159E-03 0.00707 ];
INF_SP5                   (idx, [1:   8]) = [  1.40049E-04 0.12760  2.32268E-05 0.09758 -4.30004E-05 0.03911  6.45529E-03 0.00815 ];
INF_SP6                   (idx, [1:   8]) = [  5.22357E-03 0.00376 -1.40348E-04 0.01735 -5.22160E-05 0.03405 -1.63290E-02 0.00310 ];
INF_SP7                   (idx, [1:   8]) = [  9.33801E-04 0.02039 -1.69541E-04 0.01213 -4.66317E-05 0.02684  4.05579E-04 0.10668 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31914E-01 0.00057  1.12538E+00 0.00726 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.32960E-01 0.00085  1.20552E+00 0.00772 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33534E-01 0.00082  1.20085E+00 0.01061 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29302E-01 0.00096  9.97926E-01 0.00766 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43732E+00 0.00057  2.96565E-01 0.00713 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.43089E+00 0.00085  2.76897E-01 0.00761 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42737E+00 0.00082  2.78305E-01 0.01023 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45372E+00 0.00096  3.34494E-01 0.00762 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.27128E-03 0.00817  1.76019E-04 0.05013  1.09366E-03 0.02013  9.62702E-04 0.02092  2.83243E-03 0.01201  9.07133E-04 0.02197  2.99331E-04 0.03776 ];
LAMBDA                    (idx, [1:  14]) = [  7.89354E-01 0.01934  1.24913E-02 0.00010  3.14332E-02 0.00047  1.10190E-01 0.00048  3.21343E-01 0.00039  1.34123E+00 0.00062  8.97071E+00 0.00307 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0009' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:31:35 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284850 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00111E+00  1.00046E+00  9.98384E-01  9.99155E-01  1.00088E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14354E-02 0.00115  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88565E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.02701E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.03138E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67207E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.42554E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.42479E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.39416E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.05467E-01 0.00120  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000518 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00010E+04 0.00079 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00010E+04 0.00079 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.51622E+02 ;
RUNNING_TIME              (idx, 1)        =  5.07376E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.43100E-01  4.43100E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.57367E-01  9.71667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  4.96651E+01  3.24858E+00  2.51400E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  4.67300E-01  2.61333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.76667E-02  9.00002E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  5.07375E+01  1.20609E+02 ];
CPU_USAGE                 (idx, 1)        = 4.95929 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00103E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76488E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.22620E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.80593E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.30555E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.61534E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.12732E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61083E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.69317E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  8.32015E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.22652E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.01117E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.75437E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.30898E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.55108E+07 ;
SR90_ACTIVITY             (idx, 1)        =  7.07754E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.21596E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.26756E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.31787E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.79372E+12 ;
CS137_ACTIVITY            (idx, 1)        =  8.09432E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.01926E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.48469E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.64203E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.20223E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.29561E+12 0.00048  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 8 ;
BURNUP                     (idx, [1:  2])  = [  6.00000E+00  6.00051E+00 ];
BURN_DAYS                 (idx, 1)        =  1.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.18622E-01 0.00110 ];
U235_FISS                 (idx, [1:   4]) = [  1.00369E+16 0.00074  7.28156E-01 0.00041 ];
U238_FISS                 (idx, [1:   4]) = [  9.16915E+14 0.00278  6.65130E-02 0.00264 ];
PU239_FISS                (idx, [1:   4]) = [  2.70558E+15 0.00145  1.96295E-01 0.00140 ];
PU240_FISS                (idx, [1:   4]) = [  1.85487E+12 0.05908  1.34432E-04 0.05905 ];
PU241_FISS                (idx, [1:   4]) = [  1.16457E+14 0.00798  8.44860E-03 0.00796 ];
U235_CAPT                 (idx, [1:   4]) = [  2.24145E+15 0.00170  1.18152E-01 0.00163 ];
U238_CAPT                 (idx, [1:   4]) = [  8.01102E+15 0.00106  4.22242E-01 0.00075 ];
PU239_CAPT                (idx, [1:   4]) = [  1.51052E+15 0.00207  7.96218E-02 0.00200 ];
PU240_CAPT                (idx, [1:   4]) = [  5.75457E+14 0.00343  3.03310E-02 0.00335 ];
PU241_CAPT                (idx, [1:   4]) = [  4.21151E+13 0.01244  2.21958E-03 0.01240 ];
XE135_CAPT                (idx, [1:   4]) = [  7.17787E+14 0.00298  3.78400E-02 0.00301 ];
SM149_CAPT                (idx, [1:   4]) = [  1.80243E+14 0.00639  9.50153E-03 0.00640 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000518 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.68610E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000518 5.00769E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2874104 2.87834E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2088421 2.09135E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 37993 3.79972E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000518 5.00769E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -7.07805E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 0.0E+00  4.50056E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.51698E+16 1.5E-05  3.51698E+16 1.5E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37859E+16 2.6E-06  1.37859E+16 2.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.89697E+16 0.00047  1.41429E+16 0.00045  4.82671E+15 0.00115 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.27556E+16 0.00027  2.79289E+16 0.00023  4.82671E+15 0.00115 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.29561E+16 0.00048  3.29561E+16 0.00048  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.53824E+18 0.00046  4.16501E+17 0.00042  1.12174E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.50471E+14 0.00558 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.30060E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.13026E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11815E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11815E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.64666E+00 0.00048 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.99016E-01 0.00030 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.52916E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25182E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95037E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97351E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.07530E+00 0.00053 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06713E+00 0.00054 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.55114E+00 1.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03761E+02 2.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06693E+00 0.00055  1.06059E+00 0.00054  6.54449E-03 0.00898 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06724E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06730E+00 0.00048 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06724E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.07541E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72934E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72921E+01 7.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.19142E-07 0.00333 ];
IMP_EALF                  (idx, [1:   2]) = [  6.18551E-07 0.00137 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.27950E-01 0.00284 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.28318E-01 0.00108 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.83190E-03 0.00595  1.61564E-04 0.03384  9.75311E-04 0.01436  8.98181E-04 0.01493  2.65078E-03 0.00835  8.65964E-04 0.01462  2.80103E-04 0.02625 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.95679E-01 0.01345  1.03703E-02 0.02026  3.13749E-02 0.00038  1.10137E-01 0.00035  3.21367E-01 0.00025  1.33907E+00 0.00065  8.60275E+00 0.00914 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.11287E-03 0.00846  1.63204E-04 0.04863  1.04147E-03 0.02075  9.36994E-04 0.02173  2.78683E-03 0.01237  9.07948E-04 0.02125  2.76416E-04 0.04101 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.70041E-01 0.02045  1.24957E-02 0.00028  3.13821E-02 0.00052  1.10190E-01 0.00050  3.21257E-01 0.00036  1.33880E+00 0.00083  8.97966E+00 0.00308 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.28994E-05 0.00120  2.28888E-05 0.00120  2.44567E-05 0.01284 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.44278E-05 0.00103  2.44164E-05 0.00103  2.60928E-05 0.01284 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.13744E-03 0.00901  1.72584E-04 0.05213  1.01795E-03 0.02194  9.48109E-04 0.02312  2.80539E-03 0.01368  8.96447E-04 0.02380  2.96957E-04 0.04115 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.97241E-01 0.02173  1.24978E-02 0.00041  3.13952E-02 0.00059  1.10192E-01 0.00063  3.21371E-01 0.00040  1.33880E+00 0.00096  8.96399E+00 0.00375 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.22801E-05 0.00270  2.22709E-05 0.00271  2.26762E-05 0.03472 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.37669E-05 0.00262  2.37571E-05 0.00263  2.41840E-05 0.03459 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.01925E-03 0.03077  2.15369E-04 0.17362  9.45120E-04 0.07085  1.00632E-03 0.07513  2.64712E-03 0.04435  9.03567E-04 0.07619  3.01758E-04 0.14070 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.94894E-01 0.06706  1.24892E-02 3.9E-05  3.13758E-02 0.00143  1.10280E-01 0.00151  3.21852E-01 0.00127  1.33914E+00 0.00226  9.04867E+00 0.00873 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.96925E-03 0.02961  2.13873E-04 0.16341  9.43311E-04 0.07053  9.78041E-04 0.07517  2.62581E-03 0.04303  9.08300E-04 0.07397  2.99919E-04 0.13494 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.04407E-01 0.06701  1.24892E-02 3.9E-05  3.13740E-02 0.00143  1.10276E-01 0.00150  3.21806E-01 0.00126  1.33890E+00 0.00228  9.03791E+00 0.00953 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.71043E+02 0.03098 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.26230E-05 0.00077 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.41332E-05 0.00050 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.10144E-03 0.00622 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.69787E+02 0.00629 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.51664E-07 0.00071 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.80374E-06 0.00043  2.80372E-06 0.00043  2.80298E-06 0.00504 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.62805E-05 0.00090  3.62958E-05 0.00090  3.38491E-05 0.01070 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.51017E-01 0.00033  6.50649E-01 0.00033  7.27334E-01 0.00957 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06085E+01 0.01301 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.42479E+01 0.00045  3.31360E+01 0.00044 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.52087E+04 0.00339  2.97490E+05 0.00147  6.08380E+05 0.00085  6.51840E+05 0.00058  5.99754E+05 0.00080  6.44328E+05 0.00076  4.37317E+05 0.00067  3.86710E+05 0.00088  2.96125E+05 0.00077  2.41736E+05 0.00068  2.08599E+05 0.00080  1.87961E+05 0.00084  1.73749E+05 0.00109  1.64774E+05 0.00066  1.60877E+05 0.00075  1.38996E+05 0.00083  1.37132E+05 0.00103  1.35878E+05 0.00092  1.33499E+05 0.00089  2.60512E+05 0.00079  2.50902E+05 0.00071  1.80807E+05 0.00084  1.17093E+05 0.00098  1.34923E+05 0.00083  1.27568E+05 0.00075  1.14884E+05 0.00094  1.87710E+05 0.00101  4.31158E+04 0.00163  5.42249E+04 0.00116  4.91175E+04 0.00154  2.85200E+04 0.00206  4.95633E+04 0.00123  3.35915E+04 0.00130  2.82190E+04 0.00182  5.20893E+03 0.00359  4.90996E+03 0.00397  4.66202E+03 0.00351  4.58349E+03 0.00366  4.64500E+03 0.00259  4.87564E+03 0.00464  5.35161E+03 0.00385  5.12280E+03 0.00387  9.83805E+03 0.00246  1.58511E+04 0.00223  2.02538E+04 0.00151  5.37991E+04 0.00127  5.65399E+04 0.00158  6.05383E+04 0.00139  3.96587E+04 0.00154  2.83344E+04 0.00194  2.12292E+04 0.00170  2.49778E+04 0.00164  4.78166E+04 0.00167  6.61422E+04 0.00111  1.28846E+05 0.00117  1.99155E+05 0.00124  2.96891E+05 0.00142  1.89415E+05 0.00118  1.35538E+05 0.00135  9.74463E+04 0.00144  8.73730E+04 0.00167  8.60673E+04 0.00169  7.17228E+04 0.00178  4.85434E+04 0.00188  4.47972E+04 0.00170  3.98665E+04 0.00173  3.36457E+04 0.00182  2.64985E+04 0.00155  1.77379E+04 0.00171  6.28391E+03 0.00224 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.07547E+00 0.00054 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.23259E+18 0.00044  3.05679E+17 0.00124 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37244E-01 9.2E-05  1.53125E+00 0.00040 ];
INF_CAPT                  (idx, [1:   4]) = [  7.04510E-03 0.00058  3.36523E-02 0.00056 ];
INF_ABS                   (idx, [1:   4]) = [  9.25448E-03 0.00048  6.98491E-02 0.00092 ];
INF_FISS                  (idx, [1:   4]) = [  2.20938E-03 0.00041  3.61968E-02 0.00125 ];
INF_NSF                   (idx, [1:   4]) = [  5.74522E-03 0.00042  9.19045E-02 0.00126 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.60037E+00 4.6E-05  2.53902E+00 2.6E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04315E+02 4.9E-06  2.03624E+02 4.3E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.74158E-08 0.00027  2.54475E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.27993E-01 9.2E-05  1.46140E+00 0.00046 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43046E-01 0.00019  3.88653E-01 0.00052 ];
INF_SCATT2                (idx, [1:   4]) = [  9.58829E-02 0.00034  9.25767E-02 0.00088 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34511E-03 0.00429  2.77725E-02 0.00211 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02426E-02 0.00223 -8.47144E-03 0.00681 ];
INF_SCATT5                (idx, [1:   4]) = [  1.53339E-04 0.13586  6.36735E-03 0.00886 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07138E-03 0.00465 -1.65037E-02 0.00278 ];
INF_SCATT7                (idx, [1:   4]) = [  7.56496E-04 0.02326  3.41102E-04 0.10344 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28034E-01 9.2E-05  1.46140E+00 0.00046 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43047E-01 0.00019  3.88653E-01 0.00052 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.58830E-02 0.00034  9.25767E-02 0.00088 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34505E-03 0.00428  2.77725E-02 0.00211 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02427E-02 0.00223 -8.47144E-03 0.00681 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.53147E-04 0.13620  6.36735E-03 0.00886 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07123E-03 0.00466 -1.65037E-02 0.00278 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.56395E-04 0.02329  3.41102E-04 0.10344 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13233E-01 0.00026  9.89333E-01 0.00037 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56324E+00 0.00026  3.36928E-01 0.00037 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.21338E-03 0.00047  6.98491E-02 0.00092 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69519E-02 0.00027  7.10945E-02 0.00098 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10292E-01 8.7E-05  1.77013E-02 0.00044  1.23508E-03 0.00434  1.46016E+00 0.00047 ];
INF_S1                    (idx, [1:   8]) = [  2.37893E-01 0.00019  5.15327E-03 0.00098  5.26817E-04 0.00796  3.88126E-01 0.00052 ];
INF_S2                    (idx, [1:   8]) = [  9.74366E-02 0.00031 -1.55376E-03 0.00243  2.85833E-04 0.00956  9.22909E-02 0.00087 ];
INF_S3                    (idx, [1:   8]) = [  9.16713E-03 0.00335 -1.82203E-03 0.00197  1.06621E-04 0.02144  2.76659E-02 0.00212 ];
INF_S4                    (idx, [1:   8]) = [ -9.64913E-03 0.00242 -5.93507E-04 0.00509  2.33456E-06 0.94093 -8.47377E-03 0.00690 ];
INF_S5                    (idx, [1:   8]) = [  1.28920E-04 0.16012  2.44193E-05 0.10781 -3.96732E-05 0.04608  6.40703E-03 0.00886 ];
INF_S6                    (idx, [1:   8]) = [  5.21173E-03 0.00428 -1.40350E-04 0.01871 -5.38258E-05 0.03192 -1.64499E-02 0.00277 ];
INF_S7                    (idx, [1:   8]) = [  9.28437E-04 0.01881 -1.71941E-04 0.01204 -4.93483E-05 0.03294  3.90450E-04 0.09026 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10333E-01 8.7E-05  1.77013E-02 0.00044  1.23508E-03 0.00434  1.46016E+00 0.00047 ];
INF_SP1                   (idx, [1:   8]) = [  2.37893E-01 0.00019  5.15327E-03 0.00098  5.26817E-04 0.00796  3.88126E-01 0.00052 ];
INF_SP2                   (idx, [1:   8]) = [  9.74368E-02 0.00031 -1.55376E-03 0.00243  2.85833E-04 0.00956  9.22909E-02 0.00087 ];
INF_SP3                   (idx, [1:   8]) = [  9.16708E-03 0.00335 -1.82203E-03 0.00197  1.06621E-04 0.02144  2.76659E-02 0.00212 ];
INF_SP4                   (idx, [1:   8]) = [ -9.64917E-03 0.00242 -5.93507E-04 0.00509  2.33456E-06 0.94093 -8.47377E-03 0.00690 ];
INF_SP5                   (idx, [1:   8]) = [  1.28728E-04 0.16052  2.44193E-05 0.10781 -3.96732E-05 0.04608  6.40703E-03 0.00886 ];
INF_SP6                   (idx, [1:   8]) = [  5.21158E-03 0.00429 -1.40350E-04 0.01871 -5.38258E-05 0.03192 -1.64499E-02 0.00277 ];
INF_SP7                   (idx, [1:   8]) = [  9.28336E-04 0.01884 -1.71941E-04 0.01204 -4.93483E-05 0.03294  3.90450E-04 0.09026 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31800E-01 0.00059  1.12073E+00 0.00440 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33294E-01 0.00101  1.20334E+00 0.00728 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33420E-01 0.00087  1.20603E+00 0.00655 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28757E-01 0.00097  9.84825E-01 0.00471 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43803E+00 0.00059  2.97565E-01 0.00447 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42885E+00 0.00101  2.77368E-01 0.00746 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42807E+00 0.00087  2.76679E-01 0.00666 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45718E+00 0.00098  3.38649E-01 0.00469 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.11287E-03 0.00846  1.63204E-04 0.04863  1.04147E-03 0.02075  9.36994E-04 0.02173  2.78683E-03 0.01237  9.07948E-04 0.02125  2.76416E-04 0.04101 ];
LAMBDA                    (idx, [1:  14]) = [  7.70041E-01 0.02045  1.24957E-02 0.00028  3.13821E-02 0.00052  1.10190E-01 0.00050  3.21257E-01 0.00036  1.33880E+00 0.00083  8.97966E+00 0.00308 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0009' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:37:24 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284850 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00106E+00  9.99968E-01  9.98708E-01  9.97970E-01  1.00229E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 9.3E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14494E-02 0.00111  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88551E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.03225E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.03661E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67093E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.42152E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.42075E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.35654E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.05240E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000674 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00013E+04 0.00080 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00013E+04 0.00080 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.80715E+02 ;
RUNNING_TIME              (idx, 1)        =  5.65623E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.43100E-01  4.43100E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.75967E-01  8.83333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  5.54171E+01  3.23495E+00  2.51707E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.20783E-01  2.58667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.94667E-02  8.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  5.65622E+01  1.20748E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96294 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00083E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.77356E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.25651E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.79807E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.73175E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.63209E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.13897E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62440E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.68415E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  9.25794E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.28476E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.65192E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.84138E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.60602E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.60062E+07 ;
SR90_ACTIVITY             (idx, 1)        =  8.15678E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.23546E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.29417E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.34415E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.81967E+12 ;
CS137_ACTIVITY            (idx, 1)        =  9.44099E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.04230E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.46629E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.37532E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.20922E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.34466E+12 0.00048  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 9 ;
BURNUP                     (idx, [1:  2])  = [  7.00000E+00  7.00059E+00 ];
BURN_DAYS                 (idx, 1)        =  1.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.29094E-01 0.00101 ];
U233_FISS                 (idx, [1:   4]) = [  6.91688E+09 1.00000  4.96278E-07 1.00000 ];
U235_FISS                 (idx, [1:   4]) = [  9.68340E+15 0.00074  7.02808E-01 0.00046 ];
U238_FISS                 (idx, [1:   4]) = [  9.33832E+14 0.00280  6.77641E-02 0.00261 ];
PU239_FISS                (idx, [1:   4]) = [  2.99186E+15 0.00152  2.17143E-01 0.00139 ];
PU240_FISS                (idx, [1:   4]) = [  2.42845E+12 0.04890  1.76266E-04 0.04885 ];
PU241_FISS                (idx, [1:   4]) = [  1.60073E+14 0.00655  1.16174E-02 0.00650 ];
U235_CAPT                 (idx, [1:   4]) = [  2.16450E+15 0.00186  1.11200E-01 0.00175 ];
U238_CAPT                 (idx, [1:   4]) = [  8.10425E+15 0.00101  4.16340E-01 0.00069 ];
PU239_CAPT                (idx, [1:   4]) = [  1.67180E+15 0.00202  8.58906E-02 0.00194 ];
PU240_CAPT                (idx, [1:   4]) = [  6.96852E+14 0.00312  3.58042E-02 0.00313 ];
PU241_CAPT                (idx, [1:   4]) = [  5.70828E+13 0.01097  2.93332E-03 0.01100 ];
XE135_CAPT                (idx, [1:   4]) = [  7.21151E+14 0.00310  3.70539E-02 0.00312 ];
SM149_CAPT                (idx, [1:   4]) = [  1.83149E+14 0.00622  9.41009E-03 0.00621 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000674 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.79630E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000674 5.00780E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2905601 2.90982E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2056929 2.05982E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 38144 3.81575E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000674 5.00780E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -9.22009E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 0.0E+00  4.50056E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.52977E+16 1.8E-05  3.52977E+16 1.8E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37756E+16 3.0E-06  1.37756E+16 3.0E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.94772E+16 0.00046  1.45900E+16 0.00046  4.88720E+15 0.00114 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.32529E+16 0.00027  2.83657E+16 0.00024  4.88720E+15 0.00114 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.34466E+16 0.00048  3.34466E+16 0.00048  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.55944E+18 0.00047  4.21851E+17 0.00046  1.13759E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.55267E+14 0.00487 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.35081E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.14574E+18 0.00061 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11699E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11699E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.63700E+00 0.00048 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.98827E-01 0.00030 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.49995E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25154E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95041E-01 3.1E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97315E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.06371E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.05559E+00 0.00054 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.56233E+00 2.0E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03913E+02 3.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.05557E+00 0.00057  1.04937E+00 0.00055  6.22506E-03 0.00889 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.05508E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.05547E+00 0.00048 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.05508E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.06319E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72694E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72691E+01 8.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.34421E-07 0.00356 ];
IMP_EALF                  (idx, [1:   2]) = [  6.33011E-07 0.00152 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.31600E-01 0.00278 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.31762E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.74408E-03 0.00573  1.60797E-04 0.03384  9.96646E-04 0.01362  9.06510E-04 0.01524  2.54330E-03 0.00867  8.68551E-04 0.01456  2.68277E-04 0.02674 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.83216E-01 0.01403  1.01470E-02 0.02154  3.13166E-02 0.00037  1.10150E-01 0.00037  3.21492E-01 0.00025  1.33610E+00 0.00076  8.46269E+00 0.01062 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.94227E-03 0.00855  1.62806E-04 0.05151  1.02498E-03 0.02149  9.35187E-04 0.02147  2.62062E-03 0.01321  9.15349E-04 0.02082  2.83331E-04 0.03978 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.97734E-01 0.02079  1.24940E-02 0.00023  3.13219E-02 0.00052  1.10160E-01 0.00053  3.21385E-01 0.00037  1.33551E+00 0.00118  8.89860E+00 0.00447 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.30287E-05 0.00131  2.30232E-05 0.00131  2.40016E-05 0.01351 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.43042E-05 0.00115  2.42984E-05 0.00116  2.53309E-05 0.01348 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.89131E-03 0.00894  1.67485E-04 0.05541  9.93741E-04 0.02291  9.13504E-04 0.02395  2.61712E-03 0.01344  9.25208E-04 0.02200  2.74247E-04 0.04395 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.89073E-01 0.02268  1.24898E-02 1.4E-05  3.13534E-02 0.00063  1.10087E-01 0.00059  3.21397E-01 0.00040  1.33571E+00 0.00111  8.90532E+00 0.00568 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.24201E-05 0.00298  2.24189E-05 0.00299  2.17289E-05 0.03198 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.36624E-05 0.00293  2.36612E-05 0.00294  2.29275E-05 0.03198 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.92931E-03 0.02948  1.67752E-04 0.18872  9.84916E-04 0.07423  9.10839E-04 0.07522  2.59115E-03 0.04542  9.16581E-04 0.07832  3.58063E-04 0.11549 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.68074E-01 0.06227  1.24894E-02 3.5E-05  3.13452E-02 0.00156  1.10212E-01 0.00146  3.21347E-01 0.00123  1.33226E+00 0.00382  8.77740E+00 0.01421 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.98077E-03 0.02879  1.71088E-04 0.18117  9.78692E-04 0.07218  9.39558E-04 0.07362  2.61062E-03 0.04364  9.10738E-04 0.07814  3.70071E-04 0.11367 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.63280E-01 0.06148  1.24894E-02 3.5E-05  3.13435E-02 0.00155  1.10182E-01 0.00144  3.21322E-01 0.00121  1.33232E+00 0.00380  8.77734E+00 0.01420 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.66543E+02 0.02987 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.27431E-05 0.00078 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.40031E-05 0.00053 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.81819E-03 0.00535 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.55886E+02 0.00537 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.49798E-07 0.00073 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.79210E-06 0.00041  2.79202E-06 0.00042  2.80214E-06 0.00554 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.62779E-05 0.00086  3.62978E-05 0.00086  3.30859E-05 0.00981 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.48156E-01 0.00033  6.47875E-01 0.00034  7.08421E-01 0.00914 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08644E+01 0.01436 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.42075E+01 0.00047  3.30728E+01 0.00046 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.55777E+04 0.00364  2.97786E+05 0.00144  6.08818E+05 0.00099  6.51025E+05 0.00072  5.99706E+05 0.00077  6.43686E+05 0.00061  4.37688E+05 0.00064  3.87316E+05 0.00072  2.96345E+05 0.00073  2.41865E+05 0.00062  2.08657E+05 0.00075  1.87994E+05 0.00090  1.73600E+05 0.00096  1.65031E+05 0.00091  1.60665E+05 0.00101  1.39017E+05 0.00087  1.37135E+05 0.00095  1.35887E+05 0.00076  1.33639E+05 0.00089  2.60676E+05 0.00055  2.51097E+05 0.00079  1.81090E+05 0.00068  1.17223E+05 0.00097  1.35135E+05 0.00090  1.28011E+05 0.00080  1.14997E+05 0.00095  1.87351E+05 0.00070  4.32307E+04 0.00139  5.41596E+04 0.00150  4.90721E+04 0.00157  2.85064E+04 0.00173  4.97193E+04 0.00165  3.35170E+04 0.00160  2.81140E+04 0.00155  5.16047E+03 0.00407  4.81937E+03 0.00392  4.52411E+03 0.00301  4.40458E+03 0.00317  4.46250E+03 0.00329  4.77403E+03 0.00322  5.25922E+03 0.00359  5.12503E+03 0.00499  9.74989E+03 0.00266  1.57337E+04 0.00266  2.01265E+04 0.00167  5.35668E+04 0.00144  5.60923E+04 0.00169  6.00554E+04 0.00126  3.91897E+04 0.00156  2.79333E+04 0.00123  2.08505E+04 0.00172  2.45318E+04 0.00160  4.72191E+04 0.00145  6.54020E+04 0.00118  1.27499E+05 0.00097  1.97999E+05 0.00144  2.95529E+05 0.00117  1.88517E+05 0.00150  1.34893E+05 0.00120  9.69315E+04 0.00154  8.71585E+04 0.00163  8.57983E+04 0.00147  7.15518E+04 0.00171  4.83610E+04 0.00172  4.46907E+04 0.00191  3.97349E+04 0.00173  3.35838E+04 0.00210  2.65232E+04 0.00203  1.77222E+04 0.00202  6.29076E+03 0.00253 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.06358E+00 0.00050 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.25108E+18 0.00057  3.08387E+17 0.00118 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37398E-01 0.00012  1.53518E+00 0.00037 ];
INF_CAPT                  (idx, [1:   4]) = [  7.15974E-03 0.00070  3.41155E-02 0.00061 ];
INF_ABS                   (idx, [1:   4]) = [  9.33736E-03 0.00060  6.99573E-02 0.00092 ];
INF_FISS                  (idx, [1:   4]) = [  2.17762E-03 0.00054  3.58418E-02 0.00122 ];
INF_NSF                   (idx, [1:   4]) = [  5.67736E-03 0.00054  9.14425E-02 0.00125 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.60714E+00 6.2E-05  2.55128E+00 3.8E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04403E+02 7.6E-06  2.03792E+02 6.4E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.72721E-08 0.00046  2.54803E-06 0.00023 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28071E-01 0.00012  1.46522E+00 0.00043 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43057E-01 0.00020  3.89625E-01 0.00051 ];
INF_SCATT2                (idx, [1:   4]) = [  9.59153E-02 0.00028  9.26657E-02 0.00078 ];
INF_SCATT3                (idx, [1:   4]) = [  7.33122E-03 0.00256  2.78630E-02 0.00259 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02374E-02 0.00142 -8.56547E-03 0.00856 ];
INF_SCATT5                (idx, [1:   4]) = [  1.85561E-04 0.08755  6.39961E-03 0.00974 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07152E-03 0.00274 -1.65504E-02 0.00311 ];
INF_SCATT7                (idx, [1:   4]) = [  7.51109E-04 0.01773  3.46412E-04 0.13251 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28112E-01 0.00012  1.46522E+00 0.00043 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43058E-01 0.00020  3.89625E-01 0.00051 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.59153E-02 0.00028  9.26657E-02 0.00078 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.33104E-03 0.00256  2.78630E-02 0.00259 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02373E-02 0.00142 -8.56547E-03 0.00856 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.85598E-04 0.08737  6.39961E-03 0.00974 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07147E-03 0.00274 -1.65504E-02 0.00311 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.51108E-04 0.01767  3.46412E-04 0.13251 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13232E-01 0.00033  9.92404E-01 0.00032 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56325E+00 0.00033  3.35886E-01 0.00032 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.29567E-03 0.00060  6.99573E-02 0.00092 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69499E-02 0.00024  7.12034E-02 0.00096 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10448E-01 0.00012  1.76225E-02 0.00048  1.24359E-03 0.00391  1.46397E+00 0.00043 ];
INF_S1                    (idx, [1:   8]) = [  2.37930E-01 0.00019  5.12759E-03 0.00090  5.33589E-04 0.00661  3.89091E-01 0.00051 ];
INF_S2                    (idx, [1:   8]) = [  9.74674E-02 0.00027 -1.55207E-03 0.00224  2.92091E-04 0.00735  9.23737E-02 0.00079 ];
INF_S3                    (idx, [1:   8]) = [  9.15153E-03 0.00189 -1.82031E-03 0.00182  1.05207E-04 0.01467  2.77578E-02 0.00260 ];
INF_S4                    (idx, [1:   8]) = [ -9.65044E-03 0.00150 -5.86946E-04 0.00425  1.90563E-06 0.68483 -8.56737E-03 0.00856 ];
INF_S5                    (idx, [1:   8]) = [  1.55922E-04 0.10760  2.96389E-05 0.09208 -4.19565E-05 0.03545  6.44156E-03 0.00965 ];
INF_S6                    (idx, [1:   8]) = [  5.20922E-03 0.00274 -1.37705E-04 0.01695 -5.23587E-05 0.02597 -1.64980E-02 0.00314 ];
INF_S7                    (idx, [1:   8]) = [  9.18376E-04 0.01495 -1.67267E-04 0.01444 -4.72895E-05 0.03643  3.93701E-04 0.11649 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10490E-01 0.00012  1.76225E-02 0.00048  1.24359E-03 0.00391  1.46397E+00 0.00043 ];
INF_SP1                   (idx, [1:   8]) = [  2.37931E-01 0.00019  5.12759E-03 0.00090  5.33589E-04 0.00661  3.89091E-01 0.00051 ];
INF_SP2                   (idx, [1:   8]) = [  9.74674E-02 0.00027 -1.55207E-03 0.00224  2.92091E-04 0.00735  9.23737E-02 0.00079 ];
INF_SP3                   (idx, [1:   8]) = [  9.15135E-03 0.00190 -1.82031E-03 0.00182  1.05207E-04 0.01467  2.77578E-02 0.00260 ];
INF_SP4                   (idx, [1:   8]) = [ -9.65037E-03 0.00150 -5.86946E-04 0.00425  1.90563E-06 0.68483 -8.56737E-03 0.00856 ];
INF_SP5                   (idx, [1:   8]) = [  1.55959E-04 0.10743  2.96389E-05 0.09208 -4.19565E-05 0.03545  6.44156E-03 0.00965 ];
INF_SP6                   (idx, [1:   8]) = [  5.20917E-03 0.00275 -1.37705E-04 0.01695 -5.23587E-05 0.02597 -1.64980E-02 0.00314 ];
INF_SP7                   (idx, [1:   8]) = [  9.18375E-04 0.01490 -1.67267E-04 0.01444 -4.72895E-05 0.03643  3.93701E-04 0.11649 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31537E-01 0.00066  1.13557E+00 0.00833 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33076E-01 0.00081  1.21545E+00 0.01049 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33067E-01 0.00110  1.22878E+00 0.01035 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28537E-01 0.00111  9.96087E-01 0.00779 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43967E+00 0.00066  2.94016E-01 0.00814 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.43017E+00 0.00081  2.74972E-01 0.01052 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.43024E+00 0.00111  2.71954E-01 0.01013 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45859E+00 0.00111  3.35122E-01 0.00767 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.94227E-03 0.00855  1.62806E-04 0.05151  1.02498E-03 0.02149  9.35187E-04 0.02147  2.62062E-03 0.01321  9.15349E-04 0.02082  2.83331E-04 0.03978 ];
LAMBDA                    (idx, [1:  14]) = [  7.97734E-01 0.02079  1.24940E-02 0.00023  3.13219E-02 0.00052  1.10160E-01 0.00053  3.21385E-01 0.00037  1.33551E+00 0.00118  8.89860E+00 0.00447 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0009' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:43:14 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284850 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00140E+00  9.99943E-01  9.99909E-01  9.98114E-01  1.00064E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.1E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14188E-02 0.00114  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88581E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.03799E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04235E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.66885E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.41856E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.41777E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.31894E+00 0.00036  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.03254E-01 0.00119  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000787 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00016E+04 0.00077 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00016E+04 0.00077 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.09881E+02 ;
RUNNING_TIME              (idx, 1)        =  6.24016E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.43100E-01  4.43100E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.95967E-01  1.01167E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.11808E+01  3.23460E+00  2.52910E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.75967E-01  2.77833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.19667E-02  9.16668E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  6.24014E+01  1.20800E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96591 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00005E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78072E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.28813E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.79098E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.37824E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.65417E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.15440E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.63394E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.67551E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.02387E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.33808E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.36930E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.95167E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.86940E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.64291E+07 ;
SR90_ACTIVITY             (idx, 1)        =  9.21261E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.25380E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.31879E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.36890E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.99563E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.07866E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.06931E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.44896E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.38609E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.21709E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.39405E+12 0.00047  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 10 ;
BURNUP                     (idx, [1:  2])  = [  8.00000E+00  8.00068E+00 ];
BURN_DAYS                 (idx, 1)        =  2.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.39304E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  9.35970E+15 0.00076  6.79840E-01 0.00050 ];
U238_FISS                 (idx, [1:   4]) = [  9.43818E+14 0.00269  6.85497E-02 0.00258 ];
PU239_FISS                (idx, [1:   4]) = [  3.24647E+15 0.00144  2.35812E-01 0.00135 ];
PU240_FISS                (idx, [1:   4]) = [  2.94733E+12 0.04769  2.14078E-04 0.04769 ];
PU241_FISS                (idx, [1:   4]) = [  2.07161E+14 0.00583  1.50471E-02 0.00580 ];
U235_CAPT                 (idx, [1:   4]) = [  2.09281E+15 0.00184  1.04858E-01 0.00173 ];
U238_CAPT                 (idx, [1:   4]) = [  8.19932E+15 0.00103  4.10808E-01 0.00074 ];
PU239_CAPT                (idx, [1:   4]) = [  1.81379E+15 0.00193  9.08779E-02 0.00183 ];
PU240_CAPT                (idx, [1:   4]) = [  8.07635E+14 0.00295  4.04631E-02 0.00283 ];
PU241_CAPT                (idx, [1:   4]) = [  7.42349E+13 0.01016  3.71906E-03 0.01012 ];
XE135_CAPT                (idx, [1:   4]) = [  7.21657E+14 0.00296  3.61584E-02 0.00290 ];
SM149_CAPT                (idx, [1:   4]) = [  1.87476E+14 0.00583  9.39387E-03 0.00582 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000787 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.73348E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000787 5.00773E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2936011 2.94020E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2025529 2.02829E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 39247 3.92496E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000787 5.00773E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -7.91624E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 0.0E+00  4.50056E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.54138E+16 1.8E-05  3.54138E+16 1.8E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37662E+16 3.1E-06  1.37662E+16 3.1E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.99503E+16 0.00044  1.49919E+16 0.00042  4.95845E+15 0.00117 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.37165E+16 0.00026  2.87580E+16 0.00022  4.95845E+15 0.00117 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.39405E+16 0.00047  3.39405E+16 0.00047  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.58030E+18 0.00047  4.26365E+17 0.00046  1.15394E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.66457E+14 0.00522 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.39830E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.16167E+18 0.00062 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11582E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11582E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.62765E+00 0.00048 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.97736E-01 0.00030 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.47617E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25095E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94888E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97248E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.05184E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04359E+00 0.00056 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.57252E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04053E+02 3.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04376E+00 0.00057  1.03743E+00 0.00056  6.15799E-03 0.00907 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.04375E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04353E+00 0.00047 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.04375E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.05201E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72520E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72501E+01 8.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.45514E-07 0.00350 ];
IMP_EALF                  (idx, [1:   2]) = [  6.45128E-07 0.00153 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.35004E-01 0.00269 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.34642E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.79178E-03 0.00591  1.62456E-04 0.03268  9.64136E-04 0.01281  9.24158E-04 0.01431  2.60414E-03 0.00878  8.72874E-04 0.01432  2.64020E-04 0.02796 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.74455E-01 0.01432  1.05005E-02 0.01954  3.13169E-02 0.00038  1.10221E-01 0.00037  3.21456E-01 0.00025  1.33380E+00 0.00077  8.28632E+00 0.01316 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.92140E-03 0.00833  1.69405E-04 0.05013  9.91743E-04 0.01989  9.59932E-04 0.02108  2.64172E-03 0.01296  8.73467E-04 0.02205  2.85133E-04 0.03815 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.01013E-01 0.02038  1.25061E-02 0.00050  3.13073E-02 0.00055  1.10159E-01 0.00047  3.21519E-01 0.00038  1.33492E+00 0.00097  8.94988E+00 0.00427 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.32063E-05 0.00126  2.31990E-05 0.00127  2.46144E-05 0.01299 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.42184E-05 0.00116  2.42108E-05 0.00116  2.56873E-05 0.01298 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.90490E-03 0.00927  1.67754E-04 0.05349  9.97755E-04 0.02065  9.56873E-04 0.02164  2.62192E-03 0.01365  8.78694E-04 0.02479  2.81909E-04 0.04228 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.84882E-01 0.02218  1.25004E-02 0.00052  3.13061E-02 0.00067  1.10230E-01 0.00066  3.21449E-01 0.00043  1.33465E+00 0.00122  8.95587E+00 0.00545 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.27145E-05 0.00285  2.27110E-05 0.00286  2.26806E-05 0.02987 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.37052E-05 0.00281  2.37016E-05 0.00282  2.36679E-05 0.02984 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.70974E-03 0.02924  1.97739E-04 0.16283  9.40870E-04 0.07479  9.49518E-04 0.07236  2.54041E-03 0.04587  7.87459E-04 0.07617  2.93745E-04 0.15393 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.84416E-01 0.07395  1.25069E-02 0.00143  3.12528E-02 0.00160  1.10234E-01 0.00149  3.21855E-01 0.00125  1.33107E+00 0.00378  8.92362E+00 0.01106 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.75904E-03 0.02876  1.94672E-04 0.15897  9.70178E-04 0.07198  9.51619E-04 0.07106  2.52972E-03 0.04572  8.17102E-04 0.07434  2.95753E-04 0.15001 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.86647E-01 0.07263  1.25069E-02 0.00143  3.12493E-02 0.00159  1.10236E-01 0.00148  3.21889E-01 0.00124  1.33083E+00 0.00384  8.92224E+00 0.01106 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.52275E+02 0.02934 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.29511E-05 0.00080 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.39517E-05 0.00057 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.89723E-03 0.00586 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.57072E+02 0.00597 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.49062E-07 0.00073 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.78133E-06 0.00041  2.78123E-06 0.00042  2.79835E-06 0.00513 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.63071E-05 0.00091  3.63294E-05 0.00091  3.27113E-05 0.01062 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.45729E-01 0.00032  6.45461E-01 0.00033  7.04585E-01 0.00929 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05310E+01 0.01367 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.41777E+01 0.00046  3.30134E+01 0.00047 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.59477E+04 0.00318  2.98297E+05 0.00127  6.08513E+05 0.00105  6.52712E+05 0.00074  5.99234E+05 0.00082  6.43385E+05 0.00068  4.36938E+05 0.00058  3.86393E+05 0.00080  2.95958E+05 0.00076  2.41829E+05 0.00073  2.08214E+05 0.00085  1.87979E+05 0.00082  1.73168E+05 0.00095  1.64890E+05 0.00080  1.60660E+05 0.00109  1.38884E+05 0.00092  1.37043E+05 0.00103  1.35704E+05 0.00088  1.33376E+05 0.00085  2.60247E+05 0.00074  2.50576E+05 0.00089  1.81259E+05 0.00054  1.17312E+05 0.00127  1.35014E+05 0.00085  1.27809E+05 0.00098  1.14754E+05 0.00090  1.87374E+05 0.00081  4.30956E+04 0.00126  5.40909E+04 0.00147  4.90536E+04 0.00131  2.86161E+04 0.00238  4.93391E+04 0.00133  3.34262E+04 0.00189  2.80353E+04 0.00154  5.08337E+03 0.00456  4.76175E+03 0.00241  4.38140E+03 0.00378  4.24800E+03 0.00404  4.33820E+03 0.00301  4.65139E+03 0.00359  5.17282E+03 0.00385  5.01948E+03 0.00407  9.71191E+03 0.00271  1.55761E+04 0.00257  2.01158E+04 0.00242  5.32182E+04 0.00157  5.57386E+04 0.00201  5.95691E+04 0.00135  3.89364E+04 0.00146  2.76345E+04 0.00167  2.06212E+04 0.00183  2.41312E+04 0.00121  4.66041E+04 0.00149  6.47310E+04 0.00121  1.26779E+05 0.00125  1.97088E+05 0.00124  2.94407E+05 0.00114  1.88024E+05 0.00133  1.34797E+05 0.00131  9.68954E+04 0.00136  8.69356E+04 0.00145  8.56851E+04 0.00138  7.15060E+04 0.00156  4.83987E+04 0.00157  4.46622E+04 0.00161  3.96917E+04 0.00172  3.35849E+04 0.00152  2.64862E+04 0.00115  1.77162E+04 0.00152  6.30299E+03 0.00226 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.05178E+00 0.00046 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.26859E+18 0.00043  3.11731E+17 0.00115 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37468E-01 0.00011  1.53932E+00 0.00034 ];
INF_CAPT                  (idx, [1:   4]) = [  7.24921E-03 0.00063  3.45009E-02 0.00053 ];
INF_ABS                   (idx, [1:   4]) = [  9.39260E-03 0.00055  6.99447E-02 0.00085 ];
INF_FISS                  (idx, [1:   4]) = [  2.14338E-03 0.00051  3.54438E-02 0.00116 ];
INF_NSF                   (idx, [1:   4]) = [  5.60197E-03 0.00053  9.08215E-02 0.00117 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.61361E+00 5.1E-05  2.56241E+00 3.2E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04491E+02 4.9E-06  2.03945E+02 5.5E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.71072E-08 0.00041  2.55120E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28073E-01 0.00011  1.46936E+00 0.00039 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43133E-01 0.00017  3.90573E-01 0.00036 ];
INF_SCATT2                (idx, [1:   4]) = [  9.59024E-02 0.00031  9.29042E-02 0.00092 ];
INF_SCATT3                (idx, [1:   4]) = [  7.28486E-03 0.00279  2.79490E-02 0.00271 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02768E-02 0.00173 -8.45965E-03 0.00673 ];
INF_SCATT5                (idx, [1:   4]) = [  2.01133E-04 0.08497  6.38949E-03 0.00951 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10165E-03 0.00284 -1.67069E-02 0.00230 ];
INF_SCATT7                (idx, [1:   4]) = [  7.24914E-04 0.01854  3.09732E-04 0.15076 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28114E-01 0.00011  1.46936E+00 0.00039 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43134E-01 0.00017  3.90573E-01 0.00036 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.59027E-02 0.00031  9.29042E-02 0.00092 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.28492E-03 0.00280  2.79490E-02 0.00271 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02768E-02 0.00173 -8.45965E-03 0.00673 ];
INF_SCATTP5               (idx, [1:   4]) = [  2.00868E-04 0.08513  6.38949E-03 0.00951 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10188E-03 0.00284 -1.67069E-02 0.00230 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.24951E-04 0.01848  3.09732E-04 0.15076 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13046E-01 0.00029  9.95396E-01 0.00036 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56461E+00 0.00029  3.34876E-01 0.00036 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.35122E-03 0.00054  6.99447E-02 0.00085 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69628E-02 0.00022  7.11950E-02 0.00089 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10505E-01 0.00011  1.75676E-02 0.00036  1.23607E-03 0.00532  1.46813E+00 0.00039 ];
INF_S1                    (idx, [1:   8]) = [  2.38028E-01 0.00016  5.10457E-03 0.00083  5.31997E-04 0.00834  3.90041E-01 0.00036 ];
INF_S2                    (idx, [1:   8]) = [  9.74587E-02 0.00030 -1.55629E-03 0.00224  2.89236E-04 0.01147  9.26149E-02 0.00092 ];
INF_S3                    (idx, [1:   8]) = [  9.09686E-03 0.00224 -1.81200E-03 0.00140  1.03402E-04 0.01978  2.78456E-02 0.00271 ];
INF_S4                    (idx, [1:   8]) = [ -9.69315E-03 0.00187 -5.83651E-04 0.00478  2.13441E-06 0.80621 -8.46178E-03 0.00670 ];
INF_S5                    (idx, [1:   8]) = [  1.69528E-04 0.10618  3.16051E-05 0.07667 -4.22279E-05 0.03430  6.43172E-03 0.00947 ];
INF_S6                    (idx, [1:   8]) = [  5.23893E-03 0.00258 -1.37276E-04 0.01763 -5.21389E-05 0.02443 -1.66548E-02 0.00231 ];
INF_S7                    (idx, [1:   8]) = [  8.97779E-04 0.01445 -1.72865E-04 0.01626 -4.79506E-05 0.02817  3.57682E-04 0.13064 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10546E-01 0.00011  1.75676E-02 0.00036  1.23607E-03 0.00532  1.46813E+00 0.00039 ];
INF_SP1                   (idx, [1:   8]) = [  2.38029E-01 0.00016  5.10457E-03 0.00083  5.31997E-04 0.00834  3.90041E-01 0.00036 ];
INF_SP2                   (idx, [1:   8]) = [  9.74590E-02 0.00030 -1.55629E-03 0.00224  2.89236E-04 0.01147  9.26149E-02 0.00092 ];
INF_SP3                   (idx, [1:   8]) = [  9.09691E-03 0.00224 -1.81200E-03 0.00140  1.03402E-04 0.01978  2.78456E-02 0.00271 ];
INF_SP4                   (idx, [1:   8]) = [ -9.69314E-03 0.00187 -5.83651E-04 0.00478  2.13441E-06 0.80621 -8.46178E-03 0.00670 ];
INF_SP5                   (idx, [1:   8]) = [  1.69263E-04 0.10638  3.16051E-05 0.07667 -4.22279E-05 0.03430  6.43172E-03 0.00947 ];
INF_SP6                   (idx, [1:   8]) = [  5.23916E-03 0.00257 -1.37276E-04 0.01763 -5.21389E-05 0.02443 -1.66548E-02 0.00231 ];
INF_SP7                   (idx, [1:   8]) = [  8.97816E-04 0.01439 -1.72865E-04 0.01626 -4.79506E-05 0.02817  3.57682E-04 0.13064 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31702E-01 0.00063  1.14879E+00 0.00653 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33292E-01 0.00085  1.23558E+00 0.00824 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33054E-01 0.00118  1.23694E+00 0.00713 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28822E-01 0.00077  1.00742E+00 0.00762 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43864E+00 0.00063  2.90454E-01 0.00647 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42885E+00 0.00085  2.70218E-01 0.00821 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.43033E+00 0.00118  2.69810E-01 0.00710 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45676E+00 0.00077  3.31333E-01 0.00750 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.92140E-03 0.00833  1.69405E-04 0.05013  9.91743E-04 0.01989  9.59932E-04 0.02108  2.64172E-03 0.01296  8.73467E-04 0.02205  2.85133E-04 0.03815 ];
LAMBDA                    (idx, [1:  14]) = [  8.01013E-01 0.02038  1.25061E-02 0.00050  3.13073E-02 0.00055  1.10159E-01 0.00047  3.21519E-01 0.00038  1.33492E+00 0.00097  8.94988E+00 0.00427 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0009' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:49:05 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284850 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00006E+00  9.99201E-01  9.99749E-01  9.99981E-01  1.00101E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.0E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13933E-02 0.00113  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88607E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04134E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04568E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.66774E+00 0.00020  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.41703E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.41625E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.29763E+00 0.00036  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.02037E-01 0.00115  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000704 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00014E+04 0.00079 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00014E+04 0.00079 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.39032E+02 ;
RUNNING_TIME              (idx, 1)        =  6.82383E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.43100E-01  4.43100E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.15917E-01  9.48333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.69425E+01  3.23048E+00  2.53122E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  6.30633E-01  2.66667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.38333E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  6.82382E+01  1.20781E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96836 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99869E+00 0.00021 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78656E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.31347E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.78448E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.30455E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.67212E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.16696E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.64133E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.66776E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.12633E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.38445E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.15660E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.04628E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.10670E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.67983E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.02467E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.27046E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.34114E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.39133E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.31282E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.21311E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.09052E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.43333E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  4.73021E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.22355E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.44079E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 11 ;
BURNUP                     (idx, [1:  2])  = [  9.00000E+00  9.00077E+00 ];
BURN_DAYS                 (idx, 1)        =  2.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.48783E-01 0.00104 ];
U235_FISS                 (idx, [1:   4]) = [  9.03322E+15 0.00078  6.56740E-01 0.00052 ];
U238_FISS                 (idx, [1:   4]) = [  9.59050E+14 0.00281  6.97131E-02 0.00261 ];
PU239_FISS                (idx, [1:   4]) = [  3.48895E+15 0.00135  2.53655E-01 0.00120 ];
PU240_FISS                (idx, [1:   4]) = [  3.42606E+12 0.04686  2.48823E-04 0.04674 ];
PU241_FISS                (idx, [1:   4]) = [  2.61837E+14 0.00492  1.90360E-02 0.00488 ];
U235_CAPT                 (idx, [1:   4]) = [  2.03095E+15 0.00180  9.93883E-02 0.00173 ];
U238_CAPT                 (idx, [1:   4]) = [  8.28083E+15 0.00101  4.05211E-01 0.00071 ];
PU239_CAPT                (idx, [1:   4]) = [  1.95228E+15 0.00185  9.55380E-02 0.00178 ];
PU240_CAPT                (idx, [1:   4]) = [  9.19852E+14 0.00286  4.50119E-02 0.00277 ];
PU241_CAPT                (idx, [1:   4]) = [  9.23273E+13 0.00863  4.51838E-03 0.00862 ];
XE135_CAPT                (idx, [1:   4]) = [  7.24758E+14 0.00310  3.54664E-02 0.00303 ];
SM149_CAPT                (idx, [1:   4]) = [  1.91416E+14 0.00611  9.36703E-03 0.00607 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000704 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.70798E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000704 5.00771E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2965347 2.96955E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1996066 1.99886E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 39291 3.93040E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000704 5.00771E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.19095E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 0.0E+00  4.50056E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.55281E+16 1.7E-05  3.55281E+16 1.7E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37568E+16 3.1E-06  1.37568E+16 3.1E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.04255E+16 0.00044  1.54027E+16 0.00045  5.02283E+15 0.00114 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.41823E+16 0.00026  2.91595E+16 0.00024  5.02283E+15 0.00114 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.44079E+16 0.00050  3.44079E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.60117E+18 0.00046  4.31734E+17 0.00046  1.16944E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.70492E+14 0.00530 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.44528E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.17709E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11466E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11466E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.61719E+00 0.00051 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.97367E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.45079E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25122E+00 0.00039 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94897E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97228E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.04071E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.03252E+00 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.58258E+00 2.0E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04192E+02 3.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03225E+00 0.00056  1.02651E+00 0.00055  6.01690E-03 0.00967 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.03285E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03268E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.03285E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.04104E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72242E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72293E+01 8.5E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.63929E-07 0.00367 ];
IMP_EALF                  (idx, [1:   2]) = [  6.58660E-07 0.00147 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.39099E-01 0.00286 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.38327E-01 0.00112 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.80675E-03 0.00622  1.59728E-04 0.03506  9.71894E-04 0.01455  8.88471E-04 0.01488  2.62254E-03 0.00895  8.75725E-04 0.01540  2.88398E-04 0.02445 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.08958E-01 0.01302  1.00476E-02 0.02210  3.12554E-02 0.00043  1.10179E-01 0.00036  3.21587E-01 0.00027  1.33093E+00 0.00091  8.53892E+00 0.00986 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.89147E-03 0.00896  1.69823E-04 0.05181  1.00578E-03 0.02150  8.99857E-04 0.02179  2.63274E-03 0.01353  8.99010E-04 0.02376  2.84263E-04 0.03805 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.98078E-01 0.02010  1.24971E-02 0.00033  3.12480E-02 0.00057  1.10128E-01 0.00053  3.21604E-01 0.00037  1.32853E+00 0.00142  8.90522E+00 0.00448 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.33667E-05 0.00131  2.33558E-05 0.00131  2.53326E-05 0.01386 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.41162E-05 0.00115  2.41049E-05 0.00115  2.61477E-05 0.01386 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.82876E-03 0.00976  1.71117E-04 0.05532  9.85161E-04 0.02364  9.14705E-04 0.02341  2.58944E-03 0.01523  8.81480E-04 0.02555  2.86859E-04 0.04272 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.00553E-01 0.02243  1.25038E-02 0.00058  3.12500E-02 0.00070  1.10189E-01 0.00062  3.21713E-01 0.00048  1.32890E+00 0.00179  8.89470E+00 0.00654 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.27609E-05 0.00295  2.27447E-05 0.00296  2.41109E-05 0.03492 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.34907E-05 0.00288  2.34739E-05 0.00288  2.48990E-05 0.03495 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.51233E-03 0.03231  1.74244E-04 0.18749  8.85756E-04 0.07464  7.79965E-04 0.08079  2.55561E-03 0.04768  8.54443E-04 0.08444  2.62317E-04 0.16358 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.14299E-01 0.07177  1.24898E-02 3.8E-05  3.11988E-02 0.00174  1.10038E-01 0.00164  3.21903E-01 0.00132  1.32428E+00 0.00450  8.82826E+00 0.02044 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.50462E-03 0.03199  1.72687E-04 0.18346  8.64036E-04 0.07242  7.52852E-04 0.07922  2.56033E-03 0.04629  8.83180E-04 0.08223  2.71537E-04 0.15842 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.35142E-01 0.06936  1.24898E-02 3.8E-05  3.12059E-02 0.00173  1.10039E-01 0.00164  3.21811E-01 0.00130  1.32427E+00 0.00450  8.82677E+00 0.02042 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.42661E+02 0.03239 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.31009E-05 0.00077 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.38423E-05 0.00054 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.72236E-03 0.00619 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.47711E+02 0.00614 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.48022E-07 0.00075 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.76946E-06 0.00043  2.76930E-06 0.00043  2.79591E-06 0.00555 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.63765E-05 0.00090  3.63951E-05 0.00091  3.33327E-05 0.01077 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.43210E-01 0.00037  6.43002E-01 0.00036  6.91765E-01 0.00969 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04678E+01 0.01380 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.41625E+01 0.00046  3.29385E+01 0.00047 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.64054E+04 0.00336  2.98739E+05 0.00147  6.09656E+05 0.00082  6.52170E+05 0.00075  6.00218E+05 0.00058  6.43970E+05 0.00063  4.36689E+05 0.00069  3.86780E+05 0.00077  2.95827E+05 0.00081  2.41546E+05 0.00082  2.08577E+05 0.00088  1.87646E+05 0.00091  1.73619E+05 0.00087  1.64960E+05 0.00072  1.61031E+05 0.00081  1.38788E+05 0.00103  1.37085E+05 0.00086  1.35870E+05 0.00084  1.33325E+05 0.00097  2.60427E+05 0.00070  2.51053E+05 0.00069  1.80870E+05 0.00085  1.16966E+05 0.00097  1.35157E+05 0.00077  1.27806E+05 0.00093  1.14937E+05 0.00101  1.86790E+05 0.00056  4.30833E+04 0.00153  5.40719E+04 0.00195  4.89563E+04 0.00132  2.84928E+04 0.00146  4.94976E+04 0.00205  3.32671E+04 0.00146  2.77541E+04 0.00193  5.03870E+03 0.00413  4.62443E+03 0.00347  4.23607E+03 0.00425  4.11439E+03 0.00386  4.21508E+03 0.00292  4.54224E+03 0.00396  5.05663E+03 0.00362  4.94306E+03 0.00361  9.58443E+03 0.00321  1.55093E+04 0.00249  1.99488E+04 0.00192  5.29946E+04 0.00156  5.54998E+04 0.00132  5.91605E+04 0.00136  3.84242E+04 0.00162  2.73118E+04 0.00161  2.03072E+04 0.00179  2.38714E+04 0.00199  4.61049E+04 0.00175  6.41411E+04 0.00167  1.26019E+05 0.00112  1.95985E+05 0.00137  2.93499E+05 0.00140  1.87755E+05 0.00151  1.34480E+05 0.00156  9.67303E+04 0.00174  8.69942E+04 0.00150  8.56411E+04 0.00160  7.14912E+04 0.00143  4.84251E+04 0.00166  4.46634E+04 0.00165  3.98022E+04 0.00162  3.35767E+04 0.00139  2.65121E+04 0.00184  1.76996E+04 0.00202  6.27344E+03 0.00277 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.04086E+00 0.00050 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.28631E+18 0.00050  3.14895E+17 0.00133 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37461E-01 0.00010  1.54264E+00 0.00039 ];
INF_CAPT                  (idx, [1:   4]) = [  7.33927E-03 0.00058  3.48882E-02 0.00064 ];
INF_ABS                   (idx, [1:   4]) = [  9.45050E-03 0.00051  6.99575E-02 0.00095 ];
INF_FISS                  (idx, [1:   4]) = [  2.11123E-03 0.00049  3.50694E-02 0.00127 ];
INF_NSF                   (idx, [1:   4]) = [  5.53225E-03 0.00051  9.02434E-02 0.00128 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.62039E+00 5.4E-05  2.57328E+00 3.3E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04582E+02 5.6E-06  2.04096E+02 5.7E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.68849E-08 0.00046  2.55438E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28001E-01 0.00011  1.47271E+00 0.00045 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43145E-01 0.00019  3.91447E-01 0.00057 ];
INF_SCATT2                (idx, [1:   4]) = [  9.59820E-02 0.00028  9.29055E-02 0.00106 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34922E-03 0.00279  2.78783E-02 0.00262 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02295E-02 0.00170 -8.59417E-03 0.00668 ];
INF_SCATT5                (idx, [1:   4]) = [  2.04274E-04 0.09902  6.48904E-03 0.00949 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10608E-03 0.00340 -1.67921E-02 0.00216 ];
INF_SCATT7                (idx, [1:   4]) = [  7.74749E-04 0.02188  3.68973E-04 0.09652 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28042E-01 0.00011  1.47271E+00 0.00045 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43145E-01 0.00019  3.91447E-01 0.00057 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.59824E-02 0.00028  9.29055E-02 0.00106 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34935E-03 0.00279  2.78783E-02 0.00262 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02295E-02 0.00170 -8.59417E-03 0.00668 ];
INF_SCATTP5               (idx, [1:   4]) = [  2.04162E-04 0.09911  6.48904E-03 0.00949 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10598E-03 0.00340 -1.67921E-02 0.00216 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.74873E-04 0.02191  3.68973E-04 0.09652 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12950E-01 0.00027  9.98119E-01 0.00030 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56531E+00 0.00027  3.33962E-01 0.00030 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.40927E-03 0.00050  6.99575E-02 0.00095 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69561E-02 0.00020  7.11684E-02 0.00109 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10505E-01 0.00011  1.74956E-02 0.00047  1.23398E-03 0.00500  1.47147E+00 0.00045 ];
INF_S1                    (idx, [1:   8]) = [  2.38064E-01 0.00018  5.08037E-03 0.00084  5.23148E-04 0.00653  3.90924E-01 0.00057 ];
INF_S2                    (idx, [1:   8]) = [  9.75339E-02 0.00027 -1.55192E-03 0.00204  2.83103E-04 0.00967  9.26224E-02 0.00107 ];
INF_S3                    (idx, [1:   8]) = [  9.15052E-03 0.00225 -1.80130E-03 0.00114  1.01763E-04 0.02191  2.77765E-02 0.00266 ];
INF_S4                    (idx, [1:   8]) = [ -9.65190E-03 0.00185 -5.77582E-04 0.00459  1.85151E-06 1.00000 -8.59602E-03 0.00659 ];
INF_S5                    (idx, [1:   8]) = [  1.75744E-04 0.11020  2.85297E-05 0.08248 -4.10042E-05 0.03763  6.53005E-03 0.00939 ];
INF_S6                    (idx, [1:   8]) = [  5.24766E-03 0.00335 -1.41575E-04 0.01672 -5.22358E-05 0.03766 -1.67399E-02 0.00211 ];
INF_S7                    (idx, [1:   8]) = [  9.44921E-04 0.01849 -1.70172E-04 0.01640 -4.55393E-05 0.02935  4.14512E-04 0.08556 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10546E-01 0.00011  1.74956E-02 0.00047  1.23398E-03 0.00500  1.47147E+00 0.00045 ];
INF_SP1                   (idx, [1:   8]) = [  2.38065E-01 0.00018  5.08037E-03 0.00084  5.23148E-04 0.00653  3.90924E-01 0.00057 ];
INF_SP2                   (idx, [1:   8]) = [  9.75343E-02 0.00027 -1.55192E-03 0.00204  2.83103E-04 0.00967  9.26224E-02 0.00107 ];
INF_SP3                   (idx, [1:   8]) = [  9.15065E-03 0.00225 -1.80130E-03 0.00114  1.01763E-04 0.02191  2.77765E-02 0.00266 ];
INF_SP4                   (idx, [1:   8]) = [ -9.65188E-03 0.00185 -5.77582E-04 0.00459  1.85151E-06 1.00000 -8.59602E-03 0.00659 ];
INF_SP5                   (idx, [1:   8]) = [  1.75632E-04 0.11033  2.85297E-05 0.08248 -4.10042E-05 0.03763  6.53005E-03 0.00939 ];
INF_SP6                   (idx, [1:   8]) = [  5.24755E-03 0.00334 -1.41575E-04 0.01672 -5.22358E-05 0.03766 -1.67399E-02 0.00211 ];
INF_SP7                   (idx, [1:   8]) = [  9.45045E-04 0.01851 -1.70172E-04 0.01640 -4.55393E-05 0.02935  4.14512E-04 0.08556 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31729E-01 0.00059  1.14716E+00 0.00866 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33342E-01 0.00071  1.23322E+00 0.01167 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33005E-01 0.00100  1.23739E+00 0.01069 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28901E-01 0.00084  1.00559E+00 0.00803 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43847E+00 0.00059  2.91094E-01 0.00864 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42853E+00 0.00071  2.71158E-01 0.01139 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.43062E+00 0.00100  2.70137E-01 0.01087 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45626E+00 0.00084  3.31986E-01 0.00790 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.89147E-03 0.00896  1.69823E-04 0.05181  1.00578E-03 0.02150  8.99857E-04 0.02179  2.63274E-03 0.01353  8.99010E-04 0.02376  2.84263E-04 0.03805 ];
LAMBDA                    (idx, [1:  14]) = [  7.98078E-01 0.02010  1.24971E-02 0.00033  3.12480E-02 0.00057  1.10128E-01 0.00053  3.21604E-01 0.00037  1.32853E+00 0.00142  8.90522E+00 0.00448 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0009' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 00:54:56 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284850 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00112E+00  1.00123E+00  9.98313E-01  9.98743E-01  1.00060E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.3E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.13908E-02 0.00111  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88609E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04321E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04757E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.66670E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.41416E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.41337E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.28069E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.01027E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000711 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00014E+04 0.00082 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00014E+04 0.00082 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.68262E+02 ;
RUNNING_TIME              (idx, 1)        =  7.40904E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.43100E-01  4.43100E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.36400E-01  1.01167E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.27186E+01  3.22782E+00  2.54832E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  6.85867E-01  2.71000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.55667E-02  8.00002E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.40902E+01  1.20916E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97044 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99962E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79154E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.33687E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.77844E+04 ;
TOT_SF_RATE               (idx, 1)        =  4.57692E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.69023E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.17966E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.64661E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.66045E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.23347E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.42688E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  6.01152E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.14241E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.32322E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.71264E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.12599E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.28584E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.36175E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.41211E+14 ;
CS134_ACTIVITY            (idx, 1)        =  7.77224E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.34745E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.11061E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.41892E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  6.46321E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.22974E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.48851E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 12 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E+01  1.00009E+01 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.58772E-01 0.00105 ];
U235_FISS                 (idx, [1:   4]) = [  8.74588E+15 0.00086  6.35799E-01 0.00057 ];
U238_FISS                 (idx, [1:   4]) = [  9.71410E+14 0.00284  7.06094E-02 0.00267 ];
PU239_FISS                (idx, [1:   4]) = [  3.71227E+15 0.00132  2.69882E-01 0.00122 ];
PU240_FISS                (idx, [1:   4]) = [  3.60828E+12 0.04496  2.62357E-04 0.04505 ];
PU241_FISS                (idx, [1:   4]) = [  3.13528E+14 0.00470  2.27937E-02 0.00468 ];
U235_CAPT                 (idx, [1:   4]) = [  1.96435E+15 0.00186  9.39524E-02 0.00184 ];
U238_CAPT                 (idx, [1:   4]) = [  8.37614E+15 0.00101  4.00574E-01 0.00070 ];
PU239_CAPT                (idx, [1:   4]) = [  2.06900E+15 0.00181  9.89551E-02 0.00176 ];
PU240_CAPT                (idx, [1:   4]) = [  1.02660E+15 0.00269  4.90939E-02 0.00258 ];
PU241_CAPT                (idx, [1:   4]) = [  1.13340E+14 0.00812  5.42090E-03 0.00811 ];
XE135_CAPT                (idx, [1:   4]) = [  7.30749E+14 0.00302  3.49516E-02 0.00303 ];
SM149_CAPT                (idx, [1:   4]) = [  1.96731E+14 0.00605  9.40910E-03 0.00603 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000711 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.83321E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000711 5.00783E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2992577 2.99693E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1968895 1.97166E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 39239 3.92501E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000711 5.00783E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -2.70084E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 0.0E+00  4.50056E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.56358E+16 1.9E-05  3.56358E+16 1.9E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37479E+16 3.5E-06  1.37479E+16 3.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.08948E+16 0.00045  1.58077E+16 0.00046  5.08710E+15 0.00115 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.46427E+16 0.00027  2.95556E+16 0.00024  5.08710E+15 0.00115 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.48851E+16 0.00049  3.48851E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.62164E+18 0.00047  4.36879E+17 0.00046  1.18476E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.73885E+14 0.00528 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.49166E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.19244E+18 0.00061 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11350E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11350E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.60871E+00 0.00047 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.96682E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.42871E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25042E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94877E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97259E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.03019E+00 0.00057 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.02210E+00 0.00057 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.59208E+00 2.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04324E+02 3.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.02212E+00 0.00058  1.01629E+00 0.00057  5.81129E-03 0.00980 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.02225E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.02164E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.02225E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.03034E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72073E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72101E+01 8.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.75240E-07 0.00367 ];
IMP_EALF                  (idx, [1:   2]) = [  6.71471E-07 0.00151 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.41544E-01 0.00291 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.41725E-01 0.00113 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.74623E-03 0.00636  1.51315E-04 0.03524  9.87289E-04 0.01408  8.98512E-04 0.01483  2.56169E-03 0.00895  8.75042E-04 0.01573  2.72377E-04 0.02632 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.86873E-01 0.01367  9.85337E-03 0.02322  3.12388E-02 0.00039  1.10290E-01 0.00041  3.21782E-01 0.00028  1.32829E+00 0.00095  8.29319E+00 0.01225 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.80754E-03 0.00880  1.47207E-04 0.05488  9.76409E-04 0.02020  9.04646E-04 0.02244  2.61891E-03 0.01275  8.87588E-04 0.02269  2.72788E-04 0.03970 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.87381E-01 0.02027  1.25062E-02 0.00050  3.12359E-02 0.00056  1.10250E-01 0.00055  3.21894E-01 0.00041  1.32836E+00 0.00126  8.81290E+00 0.00550 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.35563E-05 0.00128  2.35451E-05 0.00129  2.56220E-05 0.01419 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.40725E-05 0.00108  2.40611E-05 0.00109  2.61784E-05 0.01414 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.68501E-03 0.00998  1.52815E-04 0.05942  9.60557E-04 0.02371  8.85677E-04 0.02483  2.56517E-03 0.01440  8.50182E-04 0.02388  2.70613E-04 0.04183 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.83665E-01 0.02150  1.24977E-02 0.00034  3.12240E-02 0.00073  1.10290E-01 0.00073  3.21904E-01 0.00050  1.32509E+00 0.00190  8.81383E+00 0.00679 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.29704E-05 0.00308  2.29580E-05 0.00308  2.38584E-05 0.03368 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.34722E-05 0.00295  2.34596E-05 0.00295  2.43797E-05 0.03367 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.47607E-03 0.03068  1.77421E-04 0.17092  1.01252E-03 0.07334  9.02584E-04 0.07917  2.40357E-03 0.04699  7.50222E-04 0.08227  2.29751E-04 0.14010 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.56380E-01 0.07673  1.24927E-02 0.00023  3.12968E-02 0.00157  1.10483E-01 0.00174  3.22283E-01 0.00138  1.32964E+00 0.00411  9.05238E+00 0.00842 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.49756E-03 0.03040  1.87807E-04 0.16556  9.88539E-04 0.07146  9.20560E-04 0.07732  2.41657E-03 0.04675  7.57912E-04 0.08042  2.26176E-04 0.14227 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.48477E-01 0.07690  1.24950E-02 0.00041  3.12888E-02 0.00156  1.10468E-01 0.00173  3.22297E-01 0.00136  1.32987E+00 0.00406  9.05010E+00 0.00840 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.39915E+02 0.03073 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.32707E-05 0.00084 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.37813E-05 0.00057 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.60547E-03 0.00666 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.41001E+02 0.00678 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.47143E-07 0.00073 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.76097E-06 0.00044  2.76068E-06 0.00045  2.81285E-06 0.00568 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.63937E-05 0.00089  3.64117E-05 0.00090  3.34394E-05 0.01018 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.41008E-01 0.00035  6.40839E-01 0.00036  6.84846E-01 0.01013 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06213E+01 0.01360 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.41337E+01 0.00046  3.29092E+01 0.00044 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.64365E+04 0.00288  3.00168E+05 0.00140  6.08730E+05 0.00082  6.51414E+05 0.00064  5.99029E+05 0.00079  6.42582E+05 0.00057  4.37278E+05 0.00084  3.86865E+05 0.00072  2.95817E+05 0.00072  2.42025E+05 0.00077  2.08238E+05 0.00094  1.87892E+05 0.00106  1.73777E+05 0.00099  1.65051E+05 0.00057  1.60469E+05 0.00073  1.38710E+05 0.00079  1.36710E+05 0.00087  1.36043E+05 0.00101  1.33453E+05 0.00114  2.60264E+05 0.00086  2.51080E+05 0.00087  1.81164E+05 0.00078  1.17227E+05 0.00097  1.34990E+05 0.00073  1.28116E+05 0.00084  1.14739E+05 0.00107  1.86434E+05 0.00061  4.30405E+04 0.00148  5.39655E+04 0.00134  4.90437E+04 0.00175  2.85015E+04 0.00182  4.93811E+04 0.00183  3.32528E+04 0.00200  2.77576E+04 0.00185  4.99826E+03 0.00502  4.55551E+03 0.00359  4.13115E+03 0.00314  3.99242E+03 0.00310  4.08749E+03 0.00313  4.42227E+03 0.00453  5.04591E+03 0.00357  4.92302E+03 0.00492  9.52357E+03 0.00265  1.54034E+04 0.00177  1.98126E+04 0.00219  5.26476E+04 0.00178  5.53442E+04 0.00152  5.90226E+04 0.00119  3.82556E+04 0.00151  2.71378E+04 0.00177  2.01391E+04 0.00193  2.36617E+04 0.00155  4.56461E+04 0.00152  6.36991E+04 0.00145  1.24969E+05 0.00155  1.95077E+05 0.00144  2.92453E+05 0.00161  1.87389E+05 0.00152  1.34266E+05 0.00157  9.65761E+04 0.00166  8.67538E+04 0.00181  8.54747E+04 0.00194  7.14320E+04 0.00211  4.83507E+04 0.00204  4.46837E+04 0.00188  3.97066E+04 0.00223  3.35867E+04 0.00196  2.64741E+04 0.00212  1.76789E+04 0.00203  6.27041E+03 0.00246 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.02973E+00 0.00045 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.30351E+18 0.00046  3.18160E+17 0.00147 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37688E-01 8.9E-05  1.54506E+00 0.00043 ];
INF_CAPT                  (idx, [1:   4]) = [  7.43116E-03 0.00064  3.52316E-02 0.00076 ];
INF_ABS                   (idx, [1:   4]) = [  9.51213E-03 0.00053  6.99233E-02 0.00110 ];
INF_FISS                  (idx, [1:   4]) = [  2.08096E-03 0.00044  3.46917E-02 0.00145 ];
INF_NSF                   (idx, [1:   4]) = [  5.46665E-03 0.00044  8.96264E-02 0.00148 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.62698E+00 6.5E-05  2.58351E+00 5.2E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04669E+02 6.4E-06  2.04239E+02 9.1E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.67590E-08 0.00038  2.55639E-06 0.00021 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28163E-01 9.2E-05  1.47512E+00 0.00050 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43186E-01 0.00018  3.92146E-01 0.00052 ];
INF_SCATT2                (idx, [1:   4]) = [  9.59538E-02 0.00025  9.31993E-02 0.00086 ];
INF_SCATT3                (idx, [1:   4]) = [  7.29789E-03 0.00265  2.81525E-02 0.00217 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02618E-02 0.00202 -8.47154E-03 0.00610 ];
INF_SCATT5                (idx, [1:   4]) = [  1.52575E-04 0.10140  6.52799E-03 0.00856 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07081E-03 0.00311 -1.68086E-02 0.00263 ];
INF_SCATT7                (idx, [1:   4]) = [  7.18074E-04 0.02091  3.92005E-04 0.11161 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28205E-01 9.2E-05  1.47512E+00 0.00050 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43187E-01 0.00018  3.92146E-01 0.00052 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.59541E-02 0.00025  9.31993E-02 0.00086 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.29753E-03 0.00264  2.81525E-02 0.00217 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02620E-02 0.00202 -8.47154E-03 0.00610 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.52684E-04 0.10089  6.52799E-03 0.00856 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07081E-03 0.00310 -1.68086E-02 0.00263 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.18084E-04 0.02089  3.92005E-04 0.11161 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13060E-01 0.00032  9.99926E-01 0.00038 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56451E+00 0.00032  3.33359E-01 0.00038 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.47020E-03 0.00054  6.99233E-02 0.00110 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69687E-02 0.00020  7.11732E-02 0.00128 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10719E-01 9.0E-05  1.74442E-02 0.00034  1.23531E-03 0.00467  1.47388E+00 0.00050 ];
INF_S1                    (idx, [1:   8]) = [  2.38120E-01 0.00018  5.06611E-03 0.00085  5.26691E-04 0.00493  3.91619E-01 0.00052 ];
INF_S2                    (idx, [1:   8]) = [  9.75040E-02 0.00024 -1.55016E-03 0.00337  2.85234E-04 0.00756  9.29141E-02 0.00087 ];
INF_S3                    (idx, [1:   8]) = [  9.10075E-03 0.00212 -1.80286E-03 0.00186  1.00651E-04 0.01987  2.80518E-02 0.00217 ];
INF_S4                    (idx, [1:   8]) = [ -9.68216E-03 0.00216 -5.79689E-04 0.00482  5.41786E-07 1.00000 -8.47208E-03 0.00608 ];
INF_S5                    (idx, [1:   8]) = [  1.23725E-04 0.12714  2.88492E-05 0.09499 -3.90174E-05 0.04033  6.56700E-03 0.00847 ];
INF_S6                    (idx, [1:   8]) = [  5.20971E-03 0.00288 -1.38901E-04 0.01841 -5.00111E-05 0.03371 -1.67586E-02 0.00259 ];
INF_S7                    (idx, [1:   8]) = [  8.84369E-04 0.01716 -1.66295E-04 0.01390 -4.49104E-05 0.03818  4.36916E-04 0.09883 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10761E-01 9.0E-05  1.74442E-02 0.00034  1.23531E-03 0.00467  1.47388E+00 0.00050 ];
INF_SP1                   (idx, [1:   8]) = [  2.38121E-01 0.00018  5.06611E-03 0.00085  5.26691E-04 0.00493  3.91619E-01 0.00052 ];
INF_SP2                   (idx, [1:   8]) = [  9.75042E-02 0.00024 -1.55016E-03 0.00337  2.85234E-04 0.00756  9.29141E-02 0.00087 ];
INF_SP3                   (idx, [1:   8]) = [  9.10039E-03 0.00211 -1.80286E-03 0.00186  1.00651E-04 0.01987  2.80518E-02 0.00217 ];
INF_SP4                   (idx, [1:   8]) = [ -9.68228E-03 0.00216 -5.79689E-04 0.00482  5.41786E-07 1.00000 -8.47208E-03 0.00608 ];
INF_SP5                   (idx, [1:   8]) = [  1.23835E-04 0.12644  2.88492E-05 0.09499 -3.90174E-05 0.04033  6.56700E-03 0.00847 ];
INF_SP6                   (idx, [1:   8]) = [  5.20971E-03 0.00287 -1.38901E-04 0.01841 -5.00111E-05 0.03371 -1.67586E-02 0.00259 ];
INF_SP7                   (idx, [1:   8]) = [  8.84380E-04 0.01714 -1.66295E-04 0.01390 -4.49104E-05 0.03818  4.36916E-04 0.09883 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31924E-01 0.00084  1.15520E+00 0.00684 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33485E-01 0.00104  1.23491E+00 0.00715 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33539E-01 0.00089  1.25685E+00 0.00810 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28820E-01 0.00119  1.00926E+00 0.00761 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43727E+00 0.00084  2.88879E-01 0.00696 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42768E+00 0.00104  2.70251E-01 0.00706 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42734E+00 0.00089  2.65642E-01 0.00831 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45680E+00 0.00119  3.30745E-01 0.00779 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.80754E-03 0.00880  1.47207E-04 0.05488  9.76409E-04 0.02020  9.04646E-04 0.02244  2.61891E-03 0.01275  8.87588E-04 0.02269  2.72788E-04 0.03970 ];
LAMBDA                    (idx, [1:  14]) = [  7.87381E-01 0.02027  1.25062E-02 0.00050  3.12359E-02 0.00056  1.10250E-01 0.00055  3.21894E-01 0.00041  1.32836E+00 0.00126  8.81290E+00 0.00550 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0009' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:00:46 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284850 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00025E+00  1.00082E+00  1.00021E+00  9.98438E-01  1.00028E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15405E-02 0.00107  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88460E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05108E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05543E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.66870E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.41447E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.41365E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.23993E+00 0.00036  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.07226E-01 0.00108  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000722 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00014E+04 0.00076 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00014E+04 0.00076 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.97405E+02 ;
RUNNING_TIME              (idx, 1)        =  7.99261E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.43100E-01  4.43100E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.57400E-01  9.70000E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.84786E+01  3.21052E+00  2.54947E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  7.39917E-01  2.54833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.81667E-02  7.99998E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.99259E+01  1.20859E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97216 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99846E+00 0.00022 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79558E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.34541E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.75140E+04 ;
TOT_SF_RATE               (idx, 1)        =  9.73480E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.72491E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.20418E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62047E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.63096E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.52523E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.50478E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  8.46252E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.34395E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.78977E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.77039E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.37085E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.29008E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.39035E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.42792E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.19932E+13 ;
CS137_ACTIVITY            (idx, 1)        =  1.68285E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.11754E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.37829E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.28074E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.23822E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.59895E+12 0.00048  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 13 ;
BURNUP                     (idx, [1:  2])  = [  1.25000E+01  1.25011E+01 ];
BURN_DAYS                 (idx, 1)        =  3.12500E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.81681E-01 0.00110 ];
U235_FISS                 (idx, [1:   4]) = [  8.06147E+15 0.00088  5.86946E-01 0.00064 ];
U238_FISS                 (idx, [1:   4]) = [  9.99636E+14 0.00275  7.27749E-02 0.00261 ];
PU239_FISS                (idx, [1:   4]) = [  4.19714E+15 0.00126  3.05593E-01 0.00113 ];
PU240_FISS                (idx, [1:   4]) = [  5.15168E+12 0.03644  3.75113E-04 0.03642 ];
PU241_FISS                (idx, [1:   4]) = [  4.60034E+14 0.00390  3.34972E-02 0.00390 ];
U235_CAPT                 (idx, [1:   4]) = [  1.81477E+15 0.00203  8.24370E-02 0.00202 ];
U238_CAPT                 (idx, [1:   4]) = [  8.57647E+15 0.00105  3.89547E-01 0.00078 ];
PU239_CAPT                (idx, [1:   4]) = [  2.33105E+15 0.00171  1.05887E-01 0.00168 ];
PU240_CAPT                (idx, [1:   4]) = [  1.27836E+15 0.00246  5.80630E-02 0.00235 ];
PU241_CAPT                (idx, [1:   4]) = [  1.67672E+14 0.00653  7.61626E-03 0.00650 ];
XE135_CAPT                (idx, [1:   4]) = [  7.36390E+14 0.00305  3.34496E-02 0.00302 ];
SM149_CAPT                (idx, [1:   4]) = [  2.04629E+14 0.00590  9.29458E-03 0.00586 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000722 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.96450E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000722 5.00796E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3054133 3.05864E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1905483 1.90821E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 41106 4.11136E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000722 5.00796E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.72529E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 0.0E+00  4.50056E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.58825E+16 2.0E-05  3.58825E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37272E+16 3.6E-06  1.37272E+16 3.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.20105E+16 0.00045  1.67544E+16 0.00043  5.25604E+15 0.00119 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.57377E+16 0.00028  3.04816E+16 0.00024  5.25604E+15 0.00119 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.59895E+16 0.00048  3.59895E+16 0.00048  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.67114E+18 0.00047  4.48834E+17 0.00044  1.22230E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.95951E+14 0.00490 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.60336E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.23033E+18 0.00063 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11060E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11060E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.58723E+00 0.00052 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.94840E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.38493E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24873E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94649E-01 3.3E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97113E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.00579E+00 0.00054 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.97520E-01 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.61397E+00 2.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04632E+02 3.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.97515E-01 0.00056  9.91991E-01 0.00054  5.52887E-03 0.00984 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.97421E-01 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  9.97141E-01 0.00048 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.97421E-01 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.00569E+00 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71672E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71668E+01 9.0E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.02978E-07 0.00379 ];
IMP_EALF                  (idx, [1:   2]) = [  7.01194E-07 0.00155 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.48807E-01 0.00278 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.49599E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.71329E-03 0.00608  1.54945E-04 0.03648  9.73984E-04 0.01428  8.93051E-04 0.01542  2.52679E-03 0.00895  8.89481E-04 0.01423  2.75035E-04 0.02700 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.96262E-01 0.01407  9.82598E-03 0.02351  3.11321E-02 0.00043  1.10348E-01 0.00044  3.21795E-01 0.00028  1.31941E+00 0.00137  8.25305E+00 0.01317 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.63765E-03 0.00919  1.55398E-04 0.05465  9.41961E-04 0.02188  9.00889E-04 0.02175  2.49104E-03 0.01395  8.72166E-04 0.02302  2.76195E-04 0.03806 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.06721E-01 0.02038  1.25249E-02 0.00071  3.11311E-02 0.00062  1.10341E-01 0.00061  3.21781E-01 0.00041  1.31967E+00 0.00180  8.86006E+00 0.00624 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.40703E-05 0.00137  2.40585E-05 0.00138  2.62342E-05 0.01569 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.40062E-05 0.00122  2.39945E-05 0.00122  2.61675E-05 0.01567 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.54419E-03 0.00989  1.55847E-04 0.05773  9.22990E-04 0.02442  8.71142E-04 0.02390  2.44801E-03 0.01524  8.67837E-04 0.02470  2.78366E-04 0.04527 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.15259E-01 0.02429  1.25333E-02 0.00111  3.11286E-02 0.00077  1.10388E-01 0.00075  3.21940E-01 0.00050  1.31994E+00 0.00230  8.78325E+00 0.00891 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.33985E-05 0.00326  2.33887E-05 0.00327  2.40088E-05 0.03489 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.33370E-05 0.00322  2.33273E-05 0.00323  2.39423E-05 0.03481 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.57130E-03 0.03395  1.55402E-04 0.17894  9.40543E-04 0.08392  9.49880E-04 0.07733  2.33713E-03 0.05009  9.34992E-04 0.08289  2.53352E-04 0.14216 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.44195E-01 0.06946  1.25307E-02 0.00234  3.10589E-02 0.00184  1.10610E-01 0.00179  3.21922E-01 0.00145  1.31253E+00 0.00626  8.67309E+00 0.02375 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.54666E-03 0.03351  1.58368E-04 0.17385  9.42879E-04 0.08069  9.62351E-04 0.07446  2.30959E-03 0.04911  9.10996E-04 0.08121  2.62469E-04 0.14108 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.35434E-01 0.06765  1.25307E-02 0.00234  3.10604E-02 0.00183  1.10616E-01 0.00179  3.21929E-01 0.00144  1.31330E+00 0.00614  8.67041E+00 0.02386 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.38545E+02 0.03382 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.37650E-05 0.00084 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.37020E-05 0.00059 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.53050E-03 0.00544 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.32818E+02 0.00553 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.46477E-07 0.00075 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.73536E-06 0.00042  2.73520E-06 0.00042  2.76855E-06 0.00543 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.65810E-05 0.00088  3.66003E-05 0.00089  3.32376E-05 0.01141 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.36580E-01 0.00032  6.36485E-01 0.00032  6.66184E-01 0.00997 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05618E+01 0.01403 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.41365E+01 0.00046  3.28283E+01 0.00050 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.69706E+04 0.00346  3.00710E+05 0.00130  6.10105E+05 0.00083  6.52302E+05 0.00063  5.98761E+05 0.00065  6.43024E+05 0.00067  4.36209E+05 0.00069  3.86472E+05 0.00069  2.95763E+05 0.00075  2.41675E+05 0.00073  2.08138E+05 0.00065  1.87699E+05 0.00076  1.73353E+05 0.00085  1.64596E+05 0.00097  1.60408E+05 0.00082  1.38933E+05 0.00078  1.37210E+05 0.00099  1.35730E+05 0.00117  1.33198E+05 0.00113  2.60596E+05 0.00085  2.51313E+05 0.00068  1.80959E+05 0.00078  1.17104E+05 0.00094  1.35031E+05 0.00086  1.28178E+05 0.00093  1.14745E+05 0.00095  1.86634E+05 0.00085  4.30352E+04 0.00148  5.37789E+04 0.00164  4.89267E+04 0.00145  2.84347E+04 0.00140  4.92099E+04 0.00150  3.30243E+04 0.00182  2.74049E+04 0.00155  4.87574E+03 0.00294  4.36138E+03 0.00324  3.90965E+03 0.00426  3.74067E+03 0.00387  3.81168E+03 0.00250  4.18932E+03 0.00335  4.81749E+03 0.00470  4.78014E+03 0.00301  9.32146E+03 0.00248  1.51592E+04 0.00221  1.96012E+04 0.00210  5.21886E+04 0.00123  5.47555E+04 0.00136  5.83892E+04 0.00120  3.77447E+04 0.00169  2.66365E+04 0.00171  1.97048E+04 0.00195  2.31529E+04 0.00178  4.49546E+04 0.00137  6.28965E+04 0.00116  1.24063E+05 0.00092  1.93960E+05 0.00116  2.91648E+05 0.00101  1.86817E+05 0.00127  1.34259E+05 0.00134  9.65812E+04 0.00122  8.68329E+04 0.00138  8.55666E+04 0.00144  7.14387E+04 0.00138  4.83079E+04 0.00124  4.47436E+04 0.00158  3.97676E+04 0.00158  3.36624E+04 0.00141  2.66014E+04 0.00149  1.77503E+04 0.00160  6.31782E+03 0.00198 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.00541E+00 0.00041 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.34428E+18 0.00040  3.26884E+17 0.00106 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37955E-01 9.1E-05  1.55158E+00 0.00032 ];
INF_CAPT                  (idx, [1:   4]) = [  7.62686E-03 0.00053  3.59727E-02 0.00057 ];
INF_ABS                   (idx, [1:   4]) = [  9.63898E-03 0.00045  6.96977E-02 0.00080 ];
INF_FISS                  (idx, [1:   4]) = [  2.01212E-03 0.00042  3.37251E-02 0.00105 ];
INF_NSF                   (idx, [1:   4]) = [  5.31674E-03 0.00043  8.79212E-02 0.00108 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.64236E+00 5.8E-05  2.60700E+00 4.5E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04881E+02 5.1E-06  2.04571E+02 8.1E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.64218E-08 0.00042  2.56191E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28313E-01 9.6E-05  1.48186E+00 0.00037 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43352E-01 0.00017  3.93909E-01 0.00045 ];
INF_SCATT2                (idx, [1:   4]) = [  9.60084E-02 0.00027  9.34410E-02 0.00101 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32286E-03 0.00322  2.80769E-02 0.00252 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02898E-02 0.00196 -8.65978E-03 0.00638 ];
INF_SCATT5                (idx, [1:   4]) = [  1.61739E-04 0.11972  6.54680E-03 0.00899 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08041E-03 0.00353 -1.68841E-02 0.00271 ];
INF_SCATT7                (idx, [1:   4]) = [  7.50569E-04 0.01694  3.45275E-04 0.16219 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28356E-01 9.6E-05  1.48186E+00 0.00037 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43352E-01 0.00017  3.93909E-01 0.00045 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.60083E-02 0.00027  9.34410E-02 0.00101 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32296E-03 0.00322  2.80769E-02 0.00252 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02899E-02 0.00196 -8.65978E-03 0.00638 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.61983E-04 0.11977  6.54680E-03 0.00899 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08024E-03 0.00354 -1.68841E-02 0.00271 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.50791E-04 0.01692  3.45275E-04 0.16219 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12943E-01 0.00024  1.00464E+00 0.00030 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56536E+00 0.00024  3.31795E-01 0.00030 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.59633E-03 0.00044  6.96977E-02 0.00080 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69726E-02 0.00018  7.09573E-02 0.00079 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10982E-01 9.0E-05  1.73311E-02 0.00039  1.23701E-03 0.00469  1.48062E+00 0.00037 ];
INF_S1                    (idx, [1:   8]) = [  2.38328E-01 0.00017  5.02407E-03 0.00100  5.28625E-04 0.00932  3.93380E-01 0.00045 ];
INF_S2                    (idx, [1:   8]) = [  9.75611E-02 0.00027 -1.55279E-03 0.00187  2.88637E-04 0.01089  9.31524E-02 0.00102 ];
INF_S3                    (idx, [1:   8]) = [  9.11372E-03 0.00253 -1.79086E-03 0.00180  1.03413E-04 0.02037  2.79735E-02 0.00253 ];
INF_S4                    (idx, [1:   8]) = [ -9.72015E-03 0.00205 -5.69644E-04 0.00558  1.43931E-06 1.00000 -8.66122E-03 0.00630 ];
INF_S5                    (idx, [1:   8]) = [  1.27765E-04 0.14935  3.39738E-05 0.07837 -4.03678E-05 0.03827  6.58717E-03 0.00885 ];
INF_S6                    (idx, [1:   8]) = [  5.21550E-03 0.00332 -1.35088E-04 0.02061 -5.15277E-05 0.02638 -1.68325E-02 0.00269 ];
INF_S7                    (idx, [1:   8]) = [  9.18314E-04 0.01409 -1.67745E-04 0.01638 -4.85520E-05 0.03110  3.93827E-04 0.14205 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11025E-01 9.0E-05  1.73311E-02 0.00039  1.23701E-03 0.00469  1.48062E+00 0.00037 ];
INF_SP1                   (idx, [1:   8]) = [  2.38328E-01 0.00017  5.02407E-03 0.00100  5.28625E-04 0.00932  3.93380E-01 0.00045 ];
INF_SP2                   (idx, [1:   8]) = [  9.75611E-02 0.00027 -1.55279E-03 0.00187  2.88637E-04 0.01089  9.31524E-02 0.00102 ];
INF_SP3                   (idx, [1:   8]) = [  9.11382E-03 0.00253 -1.79086E-03 0.00180  1.03413E-04 0.02037  2.79735E-02 0.00253 ];
INF_SP4                   (idx, [1:   8]) = [ -9.72025E-03 0.00205 -5.69644E-04 0.00558  1.43931E-06 1.00000 -8.66122E-03 0.00630 ];
INF_SP5                   (idx, [1:   8]) = [  1.28009E-04 0.14938  3.39738E-05 0.07837 -4.03678E-05 0.03827  6.58717E-03 0.00885 ];
INF_SP6                   (idx, [1:   8]) = [  5.21533E-03 0.00333 -1.35088E-04 0.02061 -5.15277E-05 0.02638 -1.68325E-02 0.00269 ];
INF_SP7                   (idx, [1:   8]) = [  9.18537E-04 0.01408 -1.67745E-04 0.01638 -4.85520E-05 0.03110  3.93827E-04 0.14205 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31716E-01 0.00044  1.18037E+00 0.00551 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33492E-01 0.00078  1.27613E+00 0.00825 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33018E-01 0.00072  1.28642E+00 0.00886 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28704E-01 0.00082  1.02142E+00 0.00538 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43855E+00 0.00044  2.82605E-01 0.00555 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42762E+00 0.00077  2.61628E-01 0.00817 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.43052E+00 0.00071  2.59617E-01 0.00907 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45751E+00 0.00082  3.26570E-01 0.00536 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.63765E-03 0.00919  1.55398E-04 0.05465  9.41961E-04 0.02188  9.00889E-04 0.02175  2.49104E-03 0.01395  8.72166E-04 0.02302  2.76195E-04 0.03806 ];
LAMBDA                    (idx, [1:  14]) = [  8.06721E-01 0.02038  1.25249E-02 0.00071  3.11311E-02 0.00062  1.10341E-01 0.00061  3.21781E-01 0.00041  1.31967E+00 0.00180  8.86006E+00 0.00624 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0009' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:06:37 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284850 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99852E-01  9.99342E-01  1.00036E+00  9.99096E-01  1.00135E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15654E-02 0.00113  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88435E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05723E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06158E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.66976E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.41882E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.41799E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.21803E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.08484E-01 0.00118  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000644 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00013E+04 0.00085 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00013E+04 0.00085 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.26612E+02 ;
RUNNING_TIME              (idx, 1)        =  8.57740E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.43100E-01  4.43100E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.79100E-01  1.06500E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  8.42501E+01  3.22617E+00  2.54537E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  7.93983E-01  2.72667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.14500E-02  7.83332E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  8.57738E+01  1.20764E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97368 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99860E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79926E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.39409E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.73949E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.86096E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.77265E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.23813E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62141E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.61564E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.85521E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.58853E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.13571E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.60652E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.19500E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.82788E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.60443E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.31420E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.42547E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.46212E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.69736E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.01734E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.16234E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.35007E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.24839E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.25234E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.70586E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 14 ;
BURNUP                     (idx, [1:  2])  = [  1.50000E+01  1.50013E+01 ];
BURN_DAYS                 (idx, 1)        =  3.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.06184E-01 0.00115 ];
U235_FISS                 (idx, [1:   4]) = [  7.42938E+15 0.00092  5.42038E-01 0.00071 ];
U238_FISS                 (idx, [1:   4]) = [  1.02511E+15 0.00293  7.47784E-02 0.00277 ];
PU239_FISS                (idx, [1:   4]) = [  4.60716E+15 0.00126  3.36126E-01 0.00108 ];
PU240_FISS                (idx, [1:   4]) = [  6.73344E+12 0.03348  4.91323E-04 0.03347 ];
PU241_FISS                (idx, [1:   4]) = [  6.25257E+14 0.00357  4.56195E-02 0.00354 ];
U235_CAPT                 (idx, [1:   4]) = [  1.66910E+15 0.00202  7.22508E-02 0.00199 ];
U238_CAPT                 (idx, [1:   4]) = [  8.79705E+15 0.00103  3.80762E-01 0.00075 ];
PU239_CAPT                (idx, [1:   4]) = [  2.55308E+15 0.00164  1.10517E-01 0.00162 ];
PU240_CAPT                (idx, [1:   4]) = [  1.51875E+15 0.00241  6.57344E-02 0.00228 ];
PU241_CAPT                (idx, [1:   4]) = [  2.22182E+14 0.00627  9.61755E-03 0.00625 ];
XE135_CAPT                (idx, [1:   4]) = [  7.40305E+14 0.00320  3.20427E-02 0.00313 ];
SM149_CAPT                (idx, [1:   4]) = [  2.12520E+14 0.00611  9.19960E-03 0.00610 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000644 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.04608E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000644 5.00805E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3112408 3.11709E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1846690 1.84940E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 41546 4.15561E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000644 5.00805E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -9.03383E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 0.0E+00  4.50056E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.61063E+16 2.2E-05  3.61063E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37081E+16 4.3E-06  1.37081E+16 4.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.31111E+16 0.00046  1.76770E+16 0.00046  5.43402E+15 0.00124 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.68192E+16 0.00029  3.13851E+16 0.00026  5.43402E+15 0.00124 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.70586E+16 0.00050  3.70586E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.72008E+18 0.00049  4.60716E+17 0.00045  1.25937E+18 0.00056 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.08063E+14 0.00507 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.71272E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.26853E+18 0.00066 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10770E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10770E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.56575E+00 0.00052 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.92934E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.34669E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24692E+00 0.00038 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94574E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97099E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.82452E-01 0.00059 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.74287E-01 0.00059 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.63393E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04917E+02 4.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.74181E-01 0.00061  9.68988E-01 0.00060  5.29892E-03 0.00998 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.74095E-01 0.00030 ];
COL_KEFF                  (idx, [1:   2]) = [  9.74422E-01 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.74095E-01 0.00030 ];
ABS_KINF                  (idx, [1:   2]) = [  9.82256E-01 0.00029 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71319E+01 0.00024 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71322E+01 9.6E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.28686E-07 0.00408 ];
IMP_EALF                  (idx, [1:   2]) = [  7.25906E-07 0.00165 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.56156E-01 0.00289 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.56677E-01 0.00120 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.73728E-03 0.00615  1.57095E-04 0.03606  9.83655E-04 0.01511  9.07788E-04 0.01450  2.52146E-03 0.00906  8.97413E-04 0.01584  2.69867E-04 0.02804 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.77577E-01 0.01422  9.66211E-03 0.02433  3.10556E-02 0.00044  1.10504E-01 0.00048  3.22211E-01 0.00032  1.30945E+00 0.00241  8.02866E+00 0.01474 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.45817E-03 0.00873  1.45531E-04 0.05433  9.42006E-04 0.02278  8.52483E-04 0.02228  2.41584E-03 0.01321  8.39419E-04 0.02378  2.62893E-04 0.04128 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.83709E-01 0.02101  1.25209E-02 0.00064  3.10489E-02 0.00064  1.10543E-01 0.00068  3.22276E-01 0.00046  1.31212E+00 0.00193  8.70978E+00 0.00769 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.46365E-05 0.00141  2.46258E-05 0.00142  2.66131E-05 0.01823 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.39955E-05 0.00124  2.39850E-05 0.00124  2.59261E-05 0.01823 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.44372E-03 0.01008  1.45776E-04 0.06217  9.54150E-04 0.02502  8.48769E-04 0.02526  2.38492E-03 0.01487  8.39442E-04 0.02561  2.70665E-04 0.04701 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.98583E-01 0.02437  1.25287E-02 0.00109  3.10508E-02 0.00081  1.10528E-01 0.00088  3.22355E-01 0.00058  1.31094E+00 0.00274  8.75804E+00 0.00966 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.39687E-05 0.00314  2.39615E-05 0.00316  2.41993E-05 0.03977 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.33468E-05 0.00312  2.33398E-05 0.00314  2.35727E-05 0.03980 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.41005E-03 0.03315  1.35796E-04 0.23570  8.40025E-04 0.08906  7.86522E-04 0.09066  2.50617E-03 0.04831  8.69229E-04 0.08353  2.72302E-04 0.16410 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.98741E-01 0.07245  1.25433E-02 0.00301  3.10802E-02 0.00194  1.10469E-01 0.00203  3.22111E-01 0.00150  1.30382E+00 0.00665  8.75097E+00 0.02357 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.45429E-03 0.03241  1.48208E-04 0.21597  8.38700E-04 0.08806  7.93399E-04 0.08747  2.51064E-03 0.04698  8.90951E-04 0.08173  2.72400E-04 0.16438 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.08169E-01 0.07234  1.25433E-02 0.00301  3.10841E-02 0.00192  1.10465E-01 0.00203  3.22132E-01 0.00150  1.30348E+00 0.00670  8.74790E+00 0.02334 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.27139E+02 0.03342 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.43449E-05 0.00092 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.37117E-05 0.00066 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.37568E-03 0.00619 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.20916E+02 0.00627 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.47441E-07 0.00077 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.71753E-06 0.00043  2.71758E-06 0.00043  2.70741E-06 0.00605 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.68666E-05 0.00092  3.68871E-05 0.00093  3.33387E-05 0.01094 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.32741E-01 0.00035  6.32750E-01 0.00035  6.43250E-01 0.00950 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03761E+01 0.01441 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.41799E+01 0.00048  3.28122E+01 0.00051 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.79100E+04 0.00334  3.01112E+05 0.00131  6.10036E+05 0.00106  6.52405E+05 0.00076  5.98655E+05 0.00079  6.42852E+05 0.00068  4.36375E+05 0.00066  3.85954E+05 0.00059  2.95678E+05 0.00059  2.41237E+05 0.00068  2.07984E+05 0.00076  1.87728E+05 0.00072  1.73238E+05 0.00077  1.64953E+05 0.00091  1.60589E+05 0.00111  1.38766E+05 0.00107  1.36990E+05 0.00086  1.35803E+05 0.00094  1.33309E+05 0.00079  2.59968E+05 0.00051  2.51394E+05 0.00051  1.81346E+05 0.00077  1.17115E+05 0.00083  1.35121E+05 0.00099  1.28386E+05 0.00094  1.14733E+05 0.00101  1.85886E+05 0.00052  4.29478E+04 0.00166  5.37033E+04 0.00118  4.85993E+04 0.00161  2.84708E+04 0.00200  4.90894E+04 0.00127  3.29071E+04 0.00159  2.71209E+04 0.00178  4.75669E+03 0.00347  4.21146E+03 0.00272  3.70225E+03 0.00327  3.55681E+03 0.00422  3.64906E+03 0.00414  4.03316E+03 0.00350  4.65766E+03 0.00392  4.70577E+03 0.00407  9.20678E+03 0.00214  1.50004E+04 0.00273  1.94316E+04 0.00178  5.17506E+04 0.00149  5.42679E+04 0.00144  5.77491E+04 0.00133  3.72693E+04 0.00164  2.62845E+04 0.00151  1.94142E+04 0.00186  2.28724E+04 0.00178  4.43373E+04 0.00149  6.23755E+04 0.00193  1.23451E+05 0.00188  1.93541E+05 0.00172  2.91991E+05 0.00181  1.87427E+05 0.00193  1.34699E+05 0.00199  9.69493E+04 0.00198  8.72805E+04 0.00216  8.59195E+04 0.00183  7.18499E+04 0.00206  4.86701E+04 0.00204  4.50123E+04 0.00227  4.00877E+04 0.00178  3.38046E+04 0.00205  2.66969E+04 0.00211  1.78791E+04 0.00212  6.34046E+03 0.00248 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.82588E-01 0.00071 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.38359E+18 0.00061  3.36525E+17 0.00183 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38195E-01 0.00011  1.55678E+00 0.00051 ];
INF_CAPT                  (idx, [1:   4]) = [  7.80388E-03 0.00073  3.65952E-02 0.00105 ];
INF_ABS                   (idx, [1:   4]) = [  9.74836E-03 0.00062  6.93422E-02 0.00143 ];
INF_FISS                  (idx, [1:   4]) = [  1.94448E-03 0.00049  3.27471E-02 0.00187 ];
INF_NSF                   (idx, [1:   4]) = [  5.16668E-03 0.00049  8.60684E-02 0.00192 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.65711E+00 6.2E-05  2.62827E+00 5.8E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05086E+02 7.7E-06  2.04876E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.61439E-08 0.00035  2.56750E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28448E-01 0.00011  1.48747E+00 0.00060 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43441E-01 0.00023  3.95296E-01 0.00069 ];
INF_SCATT2                (idx, [1:   4]) = [  9.60574E-02 0.00033  9.37619E-02 0.00106 ];
INF_SCATT3                (idx, [1:   4]) = [  7.29416E-03 0.00243  2.81386E-02 0.00294 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02942E-02 0.00177 -8.70833E-03 0.00851 ];
INF_SCATT5                (idx, [1:   4]) = [  1.72572E-04 0.10085  6.63549E-03 0.00870 ];
INF_SCATT6                (idx, [1:   4]) = [  5.11509E-03 0.00323 -1.69472E-02 0.00330 ];
INF_SCATT7                (idx, [1:   4]) = [  7.63794E-04 0.02293  4.48310E-04 0.08210 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28491E-01 0.00011  1.48747E+00 0.00060 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43442E-01 0.00023  3.95296E-01 0.00069 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.60575E-02 0.00033  9.37619E-02 0.00106 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.29410E-03 0.00242  2.81386E-02 0.00294 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02943E-02 0.00177 -8.70833E-03 0.00851 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.72459E-04 0.10115  6.63549E-03 0.00870 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.11515E-03 0.00323 -1.69472E-02 0.00330 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.63874E-04 0.02289  4.48310E-04 0.08210 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12825E-01 0.00035  1.00879E+00 0.00041 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56623E+00 0.00035  3.30429E-01 0.00041 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.70526E-03 0.00062  6.93422E-02 0.00143 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69776E-02 0.00023  7.05282E-02 0.00145 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11217E-01 0.00011  1.72307E-02 0.00045  1.21451E-03 0.00532  1.48625E+00 0.00060 ];
INF_S1                    (idx, [1:   8]) = [  2.38455E-01 0.00022  4.98656E-03 0.00093  5.21960E-04 0.00816  3.94774E-01 0.00069 ];
INF_S2                    (idx, [1:   8]) = [  9.76025E-02 0.00032 -1.54509E-03 0.00171  2.82455E-04 0.01060  9.34794E-02 0.00106 ];
INF_S3                    (idx, [1:   8]) = [  9.07093E-03 0.00189 -1.77677E-03 0.00159  1.02752E-04 0.01700  2.80358E-02 0.00296 ];
INF_S4                    (idx, [1:   8]) = [ -9.72824E-03 0.00180 -5.65930E-04 0.00436  3.06638E-07 1.00000 -8.70863E-03 0.00849 ];
INF_S5                    (idx, [1:   8]) = [  1.39051E-04 0.12364  3.35216E-05 0.06149 -4.02201E-05 0.03734  6.67571E-03 0.00870 ];
INF_S6                    (idx, [1:   8]) = [  5.24773E-03 0.00313 -1.32640E-04 0.02078 -5.08096E-05 0.02600 -1.68964E-02 0.00331 ];
INF_S7                    (idx, [1:   8]) = [  9.28089E-04 0.01914 -1.64295E-04 0.01355 -4.73941E-05 0.02703  4.95704E-04 0.07421 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11261E-01 0.00011  1.72307E-02 0.00045  1.21451E-03 0.00532  1.48625E+00 0.00060 ];
INF_SP1                   (idx, [1:   8]) = [  2.38455E-01 0.00022  4.98656E-03 0.00093  5.21960E-04 0.00816  3.94774E-01 0.00069 ];
INF_SP2                   (idx, [1:   8]) = [  9.76025E-02 0.00032 -1.54509E-03 0.00171  2.82455E-04 0.01060  9.34794E-02 0.00106 ];
INF_SP3                   (idx, [1:   8]) = [  9.07087E-03 0.00188 -1.77677E-03 0.00159  1.02752E-04 0.01700  2.80358E-02 0.00296 ];
INF_SP4                   (idx, [1:   8]) = [ -9.72835E-03 0.00180 -5.65930E-04 0.00436  3.06638E-07 1.00000 -8.70863E-03 0.00849 ];
INF_SP5                   (idx, [1:   8]) = [  1.38937E-04 0.12400  3.35216E-05 0.06149 -4.02201E-05 0.03734  6.67571E-03 0.00870 ];
INF_SP6                   (idx, [1:   8]) = [  5.24779E-03 0.00313 -1.32640E-04 0.02078 -5.08096E-05 0.02600 -1.68964E-02 0.00331 ];
INF_SP7                   (idx, [1:   8]) = [  9.28168E-04 0.01911 -1.64295E-04 0.01355 -4.73941E-05 0.02703  4.95704E-04 0.07421 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31598E-01 0.00049  1.17601E+00 0.00812 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33485E-01 0.00083  1.27595E+00 0.00998 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33035E-01 0.00106  1.27072E+00 0.01025 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28356E-01 0.00093  1.02143E+00 0.00787 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43928E+00 0.00049  2.83893E-01 0.00810 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42767E+00 0.00083  2.61852E-01 0.00972 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.43044E+00 0.00106  2.63002E-01 0.01059 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45974E+00 0.00092  3.26825E-01 0.00788 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.45817E-03 0.00873  1.45531E-04 0.05433  9.42006E-04 0.02278  8.52483E-04 0.02228  2.41584E-03 0.01321  8.39419E-04 0.02378  2.62893E-04 0.04128 ];
LAMBDA                    (idx, [1:  14]) = [  7.83709E-01 0.02101  1.25209E-02 0.00064  3.10489E-02 0.00064  1.10543E-01 0.00068  3.22276E-01 0.00046  1.31212E+00 0.00193  8.70978E+00 0.00769 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0009' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:12:26 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284850 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00072E+00  9.99949E-01  9.99689E-01  9.99178E-01  1.00047E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15893E-02 0.00111  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88411E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06147E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06580E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67235E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.42442E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.42357E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.20920E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.10164E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000790 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00016E+04 0.00088 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00016E+04 0.00088 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.55689E+02 ;
RUNNING_TIME              (idx, 1)        =  9.15963E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.43100E-01  4.43100E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.01517E-01  1.07333E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  8.99951E+01  3.21767E+00  2.52728E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  8.48367E-01  2.76333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.40000E-02  8.16667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.15962E+01  1.20747E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97497 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99987E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80238E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.44800E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.73062E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.24355E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.82360E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.27466E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62438E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.60312E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.22705E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.66992E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.47159E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.88984E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.55457E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.88093E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.82759E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.33761E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.45763E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.49465E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.26796E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.35094E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.21265E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.32516E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.59090E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.26722E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.81205E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 15 ;
BURNUP                     (idx, [1:  2])  = [  1.75000E+01  1.75015E+01 ];
BURN_DAYS                 (idx, 1)        =  4.37500E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.26665E-01 0.00109 ];
U235_FISS                 (idx, [1:   4]) = [  6.85774E+15 0.00103  5.01085E-01 0.00080 ];
U238_FISS                 (idx, [1:   4]) = [  1.05589E+15 0.00266  7.71465E-02 0.00251 ];
PU239_FISS                (idx, [1:   4]) = [  4.96040E+15 0.00119  3.62453E-01 0.00102 ];
PU240_FISS                (idx, [1:   4]) = [  8.39510E+12 0.02975  6.13256E-04 0.02973 ];
PU241_FISS                (idx, [1:   4]) = [  7.87663E+14 0.00308  5.75525E-02 0.00300 ];
U235_CAPT                 (idx, [1:   4]) = [  1.55213E+15 0.00216  6.42208E-02 0.00212 ];
U238_CAPT                 (idx, [1:   4]) = [  8.99182E+15 0.00103  3.72012E-01 0.00071 ];
PU239_CAPT                (idx, [1:   4]) = [  2.74955E+15 0.00171  1.13768E-01 0.00168 ];
PU240_CAPT                (idx, [1:   4]) = [  1.72470E+15 0.00220  7.13545E-02 0.00207 ];
PU241_CAPT                (idx, [1:   4]) = [  2.83605E+14 0.00520  1.17353E-02 0.00521 ];
XE135_CAPT                (idx, [1:   4]) = [  7.50448E+14 0.00298  3.10500E-02 0.00294 ];
SM149_CAPT                (idx, [1:   4]) = [  2.19463E+14 0.00611  9.08207E-03 0.00615 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000790 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.02193E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000790 5.00802E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3165529 3.17016E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1792559 1.79515E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 42702 4.27078E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000790 5.00802E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.98492E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 0.0E+00  4.50056E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.63123E+16 2.0E-05  3.63123E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36904E+16 4.1E-06  1.36904E+16 4.1E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.41695E+16 0.00044  1.85535E+16 0.00044  5.61604E+15 0.00120 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.78599E+16 0.00028  3.22439E+16 0.00025  5.61604E+15 0.00120 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.81205E+16 0.00049  3.81205E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.76906E+18 0.00049  4.72668E+17 0.00047  1.29639E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.25676E+14 0.00508 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.81856E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.30701E+18 0.00064 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10480E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10480E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.54441E+00 0.00056 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.90373E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.31706E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24578E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94448E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96994E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.60546E-01 0.00061 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.52341E-01 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.65240E+00 2.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05183E+02 4.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.52453E-01 0.00061  9.47286E-01 0.00061  5.05500E-03 0.01045 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.52518E-01 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  9.52682E-01 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.52518E-01 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  9.60722E-01 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70966E+01 0.00021 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70982E+01 9.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.54158E-07 0.00357 ];
IMP_EALF                  (idx, [1:   2]) = [  7.51062E-07 0.00167 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.64087E-01 0.00280 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.64126E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.76782E-03 0.00645  1.56263E-04 0.03823  9.96140E-04 0.01502  9.09901E-04 0.01578  2.54420E-03 0.00966  8.83494E-04 0.01575  2.77818E-04 0.02866 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.76663E-01 0.01470  9.37501E-03 0.02599  3.09538E-02 0.00043  1.10564E-01 0.00048  3.22181E-01 0.00031  1.29979E+00 0.00183  7.96270E+00 0.01467 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.35733E-03 0.00913  1.44360E-04 0.05423  9.22838E-04 0.02146  8.42411E-04 0.02188  2.36782E-03 0.01420  8.19930E-04 0.02439  2.59966E-04 0.04314 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.83104E-01 0.02308  1.25312E-02 0.00077  3.09758E-02 0.00063  1.10607E-01 0.00066  3.22289E-01 0.00046  1.29918E+00 0.00250  8.62617E+00 0.00858 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.52725E-05 0.00134  2.52633E-05 0.00134  2.68388E-05 0.01539 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.40671E-05 0.00124  2.40583E-05 0.00124  2.55563E-05 0.01540 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.31226E-03 0.01067  1.55931E-04 0.06223  9.08359E-04 0.02557  8.25453E-04 0.02774  2.36591E-03 0.01594  7.91857E-04 0.02740  2.64750E-04 0.04632 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.90245E-01 0.02600  1.25243E-02 0.00099  3.09803E-02 0.00083  1.10599E-01 0.00091  3.22394E-01 0.00057  1.29606E+00 0.00343  8.57297E+00 0.01115 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.46915E-05 0.00314  2.46759E-05 0.00315  2.56028E-05 0.04642 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.35120E-05 0.00305  2.34973E-05 0.00306  2.43589E-05 0.04619 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.36905E-03 0.03586  1.29001E-04 0.23453  9.53748E-04 0.08492  8.66735E-04 0.08847  2.34770E-03 0.05391  8.03677E-04 0.08478  2.68194E-04 0.15674 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.53015E-01 0.08138  1.24893E-02 6.7E-05  3.09924E-02 0.00190  1.10733E-01 0.00209  3.22755E-01 0.00159  1.30264E+00 0.00688  8.11547E+00 0.03361 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.33971E-03 0.03524  1.35732E-04 0.22893  9.44874E-04 0.08098  8.59489E-04 0.08508  2.34100E-03 0.05292  7.85736E-04 0.08271  2.72886E-04 0.15267 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.62096E-01 0.08005  1.24893E-02 6.7E-05  3.09865E-02 0.00190  1.10717E-01 0.00208  3.22734E-01 0.00157  1.30322E+00 0.00680  8.11478E+00 0.03361 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.19171E+02 0.03632 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.49990E-05 0.00084 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.38056E-05 0.00054 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.32622E-03 0.00640 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.13113E+02 0.00643 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.48838E-07 0.00078 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.69598E-06 0.00046  2.69589E-06 0.00045  2.71245E-06 0.00603 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.71584E-05 0.00095  3.71761E-05 0.00096  3.41040E-05 0.01140 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.29766E-01 0.00035  6.29851E-01 0.00036  6.27435E-01 0.00980 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06270E+01 0.01478 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.42357E+01 0.00048  3.27956E+01 0.00049 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.89845E+04 0.00270  3.02088E+05 0.00121  6.10221E+05 0.00088  6.51346E+05 0.00069  5.99259E+05 0.00086  6.42473E+05 0.00064  4.36132E+05 0.00082  3.85863E+05 0.00087  2.95225E+05 0.00087  2.41545E+05 0.00085  2.08153E+05 0.00081  1.87709E+05 0.00055  1.73230E+05 0.00079  1.64461E+05 0.00085  1.60399E+05 0.00080  1.38334E+05 0.00117  1.37013E+05 0.00063  1.35937E+05 0.00118  1.33307E+05 0.00094  2.60583E+05 0.00073  2.51037E+05 0.00073  1.81206E+05 0.00076  1.17403E+05 0.00118  1.35055E+05 0.00093  1.28492E+05 0.00074  1.14286E+05 0.00090  1.85350E+05 0.00074  4.29223E+04 0.00181  5.36186E+04 0.00110  4.85608E+04 0.00180  2.83069E+04 0.00184  4.89587E+04 0.00127  3.27192E+04 0.00172  2.68469E+04 0.00194  4.63393E+03 0.00461  4.10236E+03 0.00342  3.58992E+03 0.00409  3.41544E+03 0.00400  3.49630E+03 0.00436  3.86402E+03 0.00390  4.54225E+03 0.00308  4.62282E+03 0.00400  8.97199E+03 0.00342  1.48126E+04 0.00206  1.91400E+04 0.00244  5.12598E+04 0.00122  5.38416E+04 0.00168  5.73482E+04 0.00163  3.69989E+04 0.00146  2.60439E+04 0.00153  1.92768E+04 0.00123  2.26783E+04 0.00143  4.40859E+04 0.00158  6.19122E+04 0.00144  1.23032E+05 0.00143  1.93462E+05 0.00148  2.92442E+05 0.00123  1.88018E+05 0.00127  1.35153E+05 0.00144  9.72412E+04 0.00145  8.76016E+04 0.00157  8.65065E+04 0.00148  7.22416E+04 0.00154  4.89285E+04 0.00153  4.53230E+04 0.00175  4.03786E+04 0.00167  3.41241E+04 0.00147  2.69949E+04 0.00191  1.80313E+04 0.00194  6.38837E+03 0.00151 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.60889E-01 0.00045 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.42256E+18 0.00048  3.46526E+17 0.00132 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38467E-01 0.00011  1.56128E+00 0.00034 ];
INF_CAPT                  (idx, [1:   4]) = [  7.94997E-03 0.00047  3.71158E-02 0.00080 ];
INF_ABS                   (idx, [1:   4]) = [  9.83219E-03 0.00044  6.89024E-02 0.00105 ];
INF_FISS                  (idx, [1:   4]) = [  1.88222E-03 0.00059  3.17866E-02 0.00135 ];
INF_NSF                   (idx, [1:   4]) = [  5.02803E-03 0.00061  8.41643E-02 0.00139 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.67133E+00 6.0E-05  2.64779E+00 5.8E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05281E+02 7.2E-06  2.05159E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.58433E-08 0.00043  2.57282E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28639E-01 0.00012  1.49238E+00 0.00040 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43570E-01 0.00020  3.96545E-01 0.00053 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61184E-02 0.00031  9.38058E-02 0.00079 ];
INF_SCATT3                (idx, [1:   4]) = [  7.28750E-03 0.00369  2.81467E-02 0.00218 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03280E-02 0.00198 -8.72391E-03 0.00698 ];
INF_SCATT5                (idx, [1:   4]) = [  1.67408E-04 0.07972  6.65447E-03 0.00789 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10708E-03 0.00277 -1.70491E-02 0.00315 ];
INF_SCATT7                (idx, [1:   4]) = [  7.67528E-04 0.01652  4.44028E-04 0.09189 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28682E-01 0.00012  1.49238E+00 0.00040 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43571E-01 0.00020  3.96545E-01 0.00053 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61184E-02 0.00031  9.38058E-02 0.00079 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.28788E-03 0.00368  2.81467E-02 0.00218 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03279E-02 0.00198 -8.72391E-03 0.00698 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.67513E-04 0.07971  6.65447E-03 0.00789 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10692E-03 0.00277 -1.70491E-02 0.00315 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.67667E-04 0.01651  4.44028E-04 0.09189 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12705E-01 0.00024  1.01212E+00 0.00029 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56712E+00 0.00024  3.29343E-01 0.00029 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.78919E-03 0.00043  6.89024E-02 0.00105 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69854E-02 0.00020  7.01081E-02 0.00109 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11481E-01 0.00011  1.71575E-02 0.00044  1.20707E-03 0.00370  1.49118E+00 0.00040 ];
INF_S1                    (idx, [1:   8]) = [  2.38603E-01 0.00021  4.96768E-03 0.00072  5.20828E-04 0.00721  3.96024E-01 0.00053 ];
INF_S2                    (idx, [1:   8]) = [  9.76712E-02 0.00032 -1.55283E-03 0.00262  2.81977E-04 0.00898  9.35238E-02 0.00080 ];
INF_S3                    (idx, [1:   8]) = [  9.06737E-03 0.00310 -1.77987E-03 0.00213  1.01039E-04 0.02068  2.80457E-02 0.00215 ];
INF_S4                    (idx, [1:   8]) = [ -9.76726E-03 0.00217 -5.60740E-04 0.00407  4.27363E-06 0.44585 -8.72818E-03 0.00700 ];
INF_S5                    (idx, [1:   8]) = [  1.35183E-04 0.10437  3.22244E-05 0.08415 -3.91342E-05 0.05234  6.69361E-03 0.00791 ];
INF_S6                    (idx, [1:   8]) = [  5.24097E-03 0.00274 -1.33888E-04 0.01681 -5.10891E-05 0.03527 -1.69980E-02 0.00315 ];
INF_S7                    (idx, [1:   8]) = [  9.31049E-04 0.01304 -1.63521E-04 0.00995 -4.79702E-05 0.02776  4.91998E-04 0.08339 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11524E-01 0.00011  1.71575E-02 0.00044  1.20707E-03 0.00370  1.49118E+00 0.00040 ];
INF_SP1                   (idx, [1:   8]) = [  2.38604E-01 0.00021  4.96768E-03 0.00072  5.20828E-04 0.00721  3.96024E-01 0.00053 ];
INF_SP2                   (idx, [1:   8]) = [  9.76712E-02 0.00032 -1.55283E-03 0.00262  2.81977E-04 0.00898  9.35238E-02 0.00080 ];
INF_SP3                   (idx, [1:   8]) = [  9.06774E-03 0.00309 -1.77987E-03 0.00213  1.01039E-04 0.02068  2.80457E-02 0.00215 ];
INF_SP4                   (idx, [1:   8]) = [ -9.76715E-03 0.00217 -5.60740E-04 0.00407  4.27363E-06 0.44585 -8.72818E-03 0.00700 ];
INF_SP5                   (idx, [1:   8]) = [  1.35288E-04 0.10433  3.22244E-05 0.08415 -3.91342E-05 0.05234  6.69361E-03 0.00791 ];
INF_SP6                   (idx, [1:   8]) = [  5.24080E-03 0.00273 -1.33888E-04 0.01681 -5.10891E-05 0.03527 -1.69980E-02 0.00315 ];
INF_SP7                   (idx, [1:   8]) = [  9.31188E-04 0.01304 -1.63521E-04 0.00995 -4.79702E-05 0.02776  4.91998E-04 0.08339 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31583E-01 0.00069  1.19681E+00 0.00593 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33100E-01 0.00104  1.29179E+00 0.00841 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33071E-01 0.00086  1.31140E+00 0.00859 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28641E-01 0.00093  1.03239E+00 0.00613 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43939E+00 0.00068  2.78747E-01 0.00578 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.43004E+00 0.00104  2.58455E-01 0.00797 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.43020E+00 0.00086  2.54623E-01 0.00842 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45792E+00 0.00093  3.23164E-01 0.00606 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.35733E-03 0.00913  1.44360E-04 0.05423  9.22838E-04 0.02146  8.42411E-04 0.02188  2.36782E-03 0.01420  8.19930E-04 0.02439  2.59966E-04 0.04314 ];
LAMBDA                    (idx, [1:  14]) = [  7.83104E-01 0.02308  1.25312E-02 0.00077  3.09758E-02 0.00063  1.10607E-01 0.00066  3.22289E-01 0.00046  1.29918E+00 0.00250  8.62617E+00 0.00858 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0009' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:18:18 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284850 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00195E+00  9.99985E-01  9.99110E-01  9.98645E-01  1.00031E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 9.3E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16114E-02 0.00112  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88389E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06402E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06836E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67383E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.42680E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.42594E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.20135E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.11129E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000975 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00020E+04 0.00089 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00020E+04 0.00089 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.84963E+02 ;
RUNNING_TIME              (idx, 1)        =  9.74578E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.43100E-01  4.43100E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.22933E-01  1.03833E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.57815E+01  3.25453E+00  2.53185E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  9.01367E-01  2.62500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.66000E-02  9.16668E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.74576E+01  1.20749E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97614 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99941E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80517E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.50276E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.72363E+04 ;
TOT_SF_RATE               (idx, 1)        =  5.24473E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.87424E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.31136E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62849E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.59246E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.64334E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.74911E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.85539E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.18036E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.87954E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.93108E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.04107E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.35918E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.48672E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.52445E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.90499E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.68362E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.26372E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.30291E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.32336E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.28192E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.92204E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 16 ;
BURNUP                     (idx, [1:  2])  = [  2.00000E+01  2.00018E+01 ];
BURN_DAYS                 (idx, 1)        =  5.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.51361E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  6.32488E+15 0.00103  4.62534E-01 0.00083 ];
U238_FISS                 (idx, [1:   4]) = [  1.08295E+15 0.00271  7.91846E-02 0.00254 ];
PU239_FISS                (idx, [1:   4]) = [  5.28461E+15 0.00114  3.86458E-01 0.00096 ];
PU240_FISS                (idx, [1:   4]) = [  9.50161E+12 0.02874  6.94665E-04 0.02872 ];
PU241_FISS                (idx, [1:   4]) = [  9.53635E+14 0.00277  6.97395E-02 0.00272 ];
U235_CAPT                 (idx, [1:   4]) = [  1.42838E+15 0.00228  5.65277E-02 0.00223 ];
U238_CAPT                 (idx, [1:   4]) = [  9.24418E+15 0.00106  3.65803E-01 0.00076 ];
PU239_CAPT                (idx, [1:   4]) = [  2.92430E+15 0.00154  1.15729E-01 0.00149 ];
PU240_CAPT                (idx, [1:   4]) = [  1.93552E+15 0.00205  7.65891E-02 0.00189 ];
PU241_CAPT                (idx, [1:   4]) = [  3.41764E+14 0.00478  1.35265E-02 0.00480 ];
XE135_CAPT                (idx, [1:   4]) = [  7.62362E+14 0.00328  3.01722E-02 0.00329 ];
SM149_CAPT                (idx, [1:   4]) = [  2.26279E+14 0.00578  8.95483E-03 0.00574 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000975 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.94372E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000975 5.00794E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3216879 3.22151E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1741059 1.74339E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 43037 4.30464E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000975 5.00794E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.79865E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 0.0E+00  4.50056E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.65046E+16 2.2E-05  3.65046E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36735E+16 4.5E-06  1.36735E+16 4.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.52608E+16 0.00045  1.94677E+16 0.00046  5.79308E+15 0.00125 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.89343E+16 0.00029  3.31413E+16 0.00027  5.79308E+15 0.00125 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.92204E+16 0.00050  3.92204E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.81858E+18 0.00049  4.85200E+17 0.00047  1.33338E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.37697E+14 0.00508 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.92720E+16 0.00030 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.34558E+18 0.00066 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10190E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10190E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.52468E+00 0.00055 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.88505E-01 0.00035 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.28051E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24379E+00 0.00038 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94404E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96970E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.39052E-01 0.00061 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.30967E-01 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.66973E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05436E+02 4.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.30905E-01 0.00062  9.26095E-01 0.00061  4.87150E-03 0.01041 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.31061E-01 0.00030 ];
COL_KEFF                  (idx, [1:   2]) = [  9.30873E-01 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.31061E-01 0.00030 ];
ABS_KINF                  (idx, [1:   2]) = [  9.39147E-01 0.00030 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70686E+01 0.00023 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70661E+01 9.5E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.76026E-07 0.00390 ];
IMP_EALF                  (idx, [1:   2]) = [  7.75564E-07 0.00162 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.70588E-01 0.00281 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.70633E-01 0.00110 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.80659E-03 0.00632  1.54609E-04 0.03814  1.05386E-03 0.01544  9.02904E-04 0.01523  2.51611E-03 0.00954  9.07828E-04 0.01479  2.71285E-04 0.02857 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.70113E-01 0.01495  9.50187E-03 0.02531  3.08866E-02 0.00047  1.10719E-01 0.00054  3.22512E-01 0.00034  1.29426E+00 0.00183  7.90285E+00 0.01585 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.24835E-03 0.00912  1.32793E-04 0.05885  9.49548E-04 0.02233  7.89346E-04 0.02365  2.29576E-03 0.01426  8.26912E-04 0.02430  2.53999E-04 0.04166 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.91269E-01 0.02254  1.25379E-02 0.00087  3.08757E-02 0.00065  1.10668E-01 0.00074  3.22582E-01 0.00052  1.29786E+00 0.00236  8.67610E+00 0.00923 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.58410E-05 0.00135  2.58285E-05 0.00135  2.81286E-05 0.01564 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.40511E-05 0.00122  2.40395E-05 0.00122  2.61790E-05 0.01561 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.24908E-03 0.01046  1.41808E-04 0.06371  9.62301E-04 0.02589  7.99581E-04 0.02706  2.26319E-03 0.01508  8.28953E-04 0.02696  2.53244E-04 0.04996 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.85466E-01 0.02700  1.25328E-02 0.00119  3.08964E-02 0.00083  1.10721E-01 0.00095  3.22102E-01 0.00058  1.29523E+00 0.00341  8.66348E+00 0.01229 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.52986E-05 0.00332  2.52779E-05 0.00334  2.51606E-05 0.04051 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.35458E-05 0.00326  2.35265E-05 0.00328  2.34222E-05 0.04047 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.98578E-03 0.03815  1.23941E-04 0.23663  8.58653E-04 0.09245  7.28809E-04 0.08350  2.24611E-03 0.05428  7.81682E-04 0.09745  2.46590E-04 0.17224 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.15749E-01 0.08801  1.24896E-02 4.3E-05  3.08661E-02 0.00196  1.10928E-01 0.00220  3.21993E-01 0.00164  1.29272E+00 0.00816  8.65515E+00 0.03042 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.99294E-03 0.03717  1.23310E-04 0.23678  8.75913E-04 0.09201  7.50783E-04 0.08306  2.24680E-03 0.05237  7.41520E-04 0.09526  2.54620E-04 0.16316 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.18000E-01 0.08617  1.24896E-02 4.3E-05  3.08685E-02 0.00196  1.10946E-01 0.00221  3.21945E-01 0.00163  1.29199E+00 0.00820  8.56094E+00 0.03218 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.98845E+02 0.03834 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.55501E-05 0.00082 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.37801E-05 0.00054 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.16799E-03 0.00692 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.02353E+02 0.00697 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.49850E-07 0.00081 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.68064E-06 0.00043  2.68050E-06 0.00043  2.70422E-06 0.00568 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.74304E-05 0.00095  3.74507E-05 0.00095  3.37142E-05 0.01071 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.26114E-01 0.00036  6.26306E-01 0.00036  6.05510E-01 0.00987 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08353E+01 0.01469 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.42594E+01 0.00050  3.27670E+01 0.00051 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.85738E+04 0.00298  3.02365E+05 0.00100  6.09519E+05 0.00103  6.51509E+05 0.00082  5.98781E+05 0.00057  6.41581E+05 0.00055  4.35179E+05 0.00075  3.85772E+05 0.00088  2.95312E+05 0.00084  2.41313E+05 0.00065  2.08037E+05 0.00109  1.87752E+05 0.00073  1.73044E+05 0.00083  1.64521E+05 0.00078  1.60516E+05 0.00100  1.38450E+05 0.00074  1.36932E+05 0.00096  1.35802E+05 0.00098  1.33255E+05 0.00107  2.60028E+05 0.00071  2.51330E+05 0.00069  1.80756E+05 0.00082  1.17253E+05 0.00103  1.35056E+05 0.00086  1.28385E+05 0.00092  1.14183E+05 0.00111  1.85185E+05 0.00074  4.28313E+04 0.00193  5.33948E+04 0.00159  4.83357E+04 0.00180  2.83531E+04 0.00144  4.89175E+04 0.00106  3.26584E+04 0.00195  2.66179E+04 0.00205  4.56761E+03 0.00397  3.95204E+03 0.00318  3.46806E+03 0.00421  3.30477E+03 0.00395  3.39262E+03 0.00347  3.74538E+03 0.00353  4.42775E+03 0.00379  4.47583E+03 0.00287  8.89082E+03 0.00273  1.46916E+04 0.00265  1.90341E+04 0.00237  5.08471E+04 0.00203  5.33600E+04 0.00144  5.68215E+04 0.00108  3.66946E+04 0.00188  2.57566E+04 0.00142  1.89681E+04 0.00181  2.23699E+04 0.00153  4.35825E+04 0.00158  6.15354E+04 0.00147  1.22646E+05 0.00149  1.93184E+05 0.00163  2.92652E+05 0.00168  1.88344E+05 0.00168  1.35549E+05 0.00188  9.76589E+04 0.00207  8.78940E+04 0.00196  8.67975E+04 0.00183  7.26108E+04 0.00203  4.92166E+04 0.00193  4.54754E+04 0.00208  4.04929E+04 0.00209  3.42744E+04 0.00195  2.70648E+04 0.00186  1.81587E+04 0.00203  6.42508E+03 0.00250 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.38957E-01 0.00051 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.46219E+18 0.00051  3.56422E+17 0.00151 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38834E-01 0.00013  1.56478E+00 0.00042 ];
INF_CAPT                  (idx, [1:   4]) = [  8.11154E-03 0.00068  3.76006E-02 0.00088 ];
INF_ABS                   (idx, [1:   4]) = [  9.93571E-03 0.00058  6.84867E-02 0.00115 ];
INF_FISS                  (idx, [1:   4]) = [  1.82417E-03 0.00049  3.08861E-02 0.00148 ];
INF_NSF                   (idx, [1:   4]) = [  4.89749E-03 0.00049  8.23450E-02 0.00154 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.68478E+00 5.9E-05  2.66608E+00 6.3E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05474E+02 7.5E-06  2.05426E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.56491E-08 0.00048  2.57743E-06 0.00020 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28901E-01 0.00014  1.49626E+00 0.00049 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43729E-01 0.00028  3.97387E-01 0.00055 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61869E-02 0.00038  9.40907E-02 0.00073 ];
INF_SCATT3                (idx, [1:   4]) = [  7.29709E-03 0.00353  2.82451E-02 0.00231 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03108E-02 0.00205 -8.90035E-03 0.00629 ];
INF_SCATT5                (idx, [1:   4]) = [  1.78813E-04 0.08441  6.51768E-03 0.00896 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10916E-03 0.00257 -1.71996E-02 0.00304 ];
INF_SCATT7                (idx, [1:   4]) = [  7.65238E-04 0.01996  3.71556E-04 0.12576 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28943E-01 0.00014  1.49626E+00 0.00049 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43730E-01 0.00028  3.97387E-01 0.00055 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61873E-02 0.00038  9.40907E-02 0.00073 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.29725E-03 0.00353  2.82451E-02 0.00231 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03109E-02 0.00206 -8.90035E-03 0.00629 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.79017E-04 0.08429  6.51768E-03 0.00896 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10908E-03 0.00257 -1.71996E-02 0.00304 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.65348E-04 0.01990  3.71556E-04 0.12576 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12769E-01 0.00027  1.01499E+00 0.00038 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56665E+00 0.00027  3.28411E-01 0.00038 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.89309E-03 0.00059  6.84867E-02 0.00115 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70072E-02 0.00022  6.97147E-02 0.00114 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11827E-01 0.00013  1.70737E-02 0.00056  1.19682E-03 0.00448  1.49506E+00 0.00049 ];
INF_S1                    (idx, [1:   8]) = [  2.38792E-01 0.00028  4.93700E-03 0.00096  5.17842E-04 0.00609  3.96869E-01 0.00055 ];
INF_S2                    (idx, [1:   8]) = [  9.77385E-02 0.00037 -1.55153E-03 0.00300  2.80419E-04 0.00868  9.38103E-02 0.00073 ];
INF_S3                    (idx, [1:   8]) = [  9.06321E-03 0.00274 -1.76613E-03 0.00168  1.00290E-04 0.02455  2.81448E-02 0.00229 ];
INF_S4                    (idx, [1:   8]) = [ -9.76341E-03 0.00203 -5.47424E-04 0.00601  2.27939E-06 0.77684 -8.90263E-03 0.00624 ];
INF_S5                    (idx, [1:   8]) = [  1.42245E-04 0.10162  3.65676E-05 0.10653 -4.18049E-05 0.03372  6.55949E-03 0.00887 ];
INF_S6                    (idx, [1:   8]) = [  5.24475E-03 0.00235 -1.35591E-04 0.01855 -5.14307E-05 0.03544 -1.71482E-02 0.00304 ];
INF_S7                    (idx, [1:   8]) = [  9.29439E-04 0.01580 -1.64201E-04 0.01331 -4.77893E-05 0.03706  4.19345E-04 0.11209 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11870E-01 0.00013  1.70737E-02 0.00056  1.19682E-03 0.00448  1.49506E+00 0.00049 ];
INF_SP1                   (idx, [1:   8]) = [  2.38793E-01 0.00028  4.93700E-03 0.00096  5.17842E-04 0.00609  3.96869E-01 0.00055 ];
INF_SP2                   (idx, [1:   8]) = [  9.77388E-02 0.00037 -1.55153E-03 0.00300  2.80419E-04 0.00868  9.38103E-02 0.00073 ];
INF_SP3                   (idx, [1:   8]) = [  9.06338E-03 0.00274 -1.76613E-03 0.00168  1.00290E-04 0.02455  2.81448E-02 0.00229 ];
INF_SP4                   (idx, [1:   8]) = [ -9.76352E-03 0.00204 -5.47424E-04 0.00601  2.27939E-06 0.77684 -8.90263E-03 0.00624 ];
INF_SP5                   (idx, [1:   8]) = [  1.42450E-04 0.10148  3.65676E-05 0.10653 -4.18049E-05 0.03372  6.55949E-03 0.00887 ];
INF_SP6                   (idx, [1:   8]) = [  5.24467E-03 0.00235 -1.35591E-04 0.01855 -5.14307E-05 0.03544 -1.71482E-02 0.00304 ];
INF_SP7                   (idx, [1:   8]) = [  9.29549E-04 0.01574 -1.64201E-04 0.01331 -4.77893E-05 0.03706  4.19345E-04 0.11209 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31629E-01 0.00069  1.20945E+00 0.00672 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33128E-01 0.00083  1.31275E+00 0.00895 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33295E-01 0.00095  1.30588E+00 0.01037 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28535E-01 0.00120  1.05126E+00 0.00647 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43910E+00 0.00069  2.75906E-01 0.00672 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42985E+00 0.00083  2.54396E-01 0.00871 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42884E+00 0.00095  2.55926E-01 0.01053 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45862E+00 0.00120  3.17396E-01 0.00640 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.24835E-03 0.00912  1.32793E-04 0.05885  9.49548E-04 0.02233  7.89346E-04 0.02365  2.29576E-03 0.01426  8.26912E-04 0.02430  2.53999E-04 0.04166 ];
LAMBDA                    (idx, [1:  14]) = [  7.91269E-01 0.02254  1.25379E-02 0.00087  3.08757E-02 0.00065  1.10668E-01 0.00074  3.22582E-01 0.00052  1.29786E+00 0.00236  8.67610E+00 0.00923 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0009' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:24:12 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284850 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00130E+00  9.99556E-01  9.99000E-01  9.99071E-01  1.00107E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16368E-02 0.00111  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88363E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06551E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06984E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67849E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.43506E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.43418E+01 0.00051  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.21323E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.13490E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001183 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00024E+04 0.00096 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00024E+04 0.00096 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.14443E+02 ;
RUNNING_TIME              (idx, 1)        =  1.03361E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.43100E-01  4.43100E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.44967E-01  1.08667E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.01607E+02  3.26212E+00  2.56388E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  9.55883E-01  2.72333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.83833E-02  8.49998E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.03361E+02  1.21045E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97715 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99949E+00 0.00021 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80775E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.50586E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68231E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.15647E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.94961E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.36766E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.55622E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.54551E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  3.62066E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.85766E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.77879E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.68057E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  8.41874E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.98961E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.44106E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.32225E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.48424E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.49979E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.35275E+13 ;
CS137_ACTIVITY            (idx, 1)        =  3.34551E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.26998E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.24885E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  9.89473E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.29563E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.12772E+12 0.00052  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 17 ;
BURNUP                     (idx, [1:  2])  = [  2.50000E+01  2.50023E+01 ];
BURN_DAYS                 (idx, 1)        =  6.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.96841E-01 0.00113 ];
U235_FISS                 (idx, [1:   4]) = [  5.35694E+15 0.00121  3.92680E-01 0.00104 ];
U238_FISS                 (idx, [1:   4]) = [  1.13768E+15 0.00270  8.33830E-02 0.00252 ];
PU239_FISS                (idx, [1:   4]) = [  5.81843E+15 0.00110  4.26510E-01 0.00092 ];
PU240_FISS                (idx, [1:   4]) = [  1.23732E+13 0.02511  9.07586E-04 0.02520 ];
PU241_FISS                (idx, [1:   4]) = [  1.29147E+15 0.00255  9.46665E-02 0.00246 ];
U235_CAPT                 (idx, [1:   4]) = [  1.20538E+15 0.00264  4.40932E-02 0.00258 ];
U238_CAPT                 (idx, [1:   4]) = [  9.68705E+15 0.00112  3.54329E-01 0.00080 ];
PU239_CAPT                (idx, [1:   4]) = [  3.21093E+15 0.00153  1.17462E-01 0.00147 ];
PU240_CAPT                (idx, [1:   4]) = [  2.32505E+15 0.00206  8.50431E-02 0.00188 ];
PU241_CAPT                (idx, [1:   4]) = [  4.59182E+14 0.00442  1.67982E-02 0.00441 ];
XE135_CAPT                (idx, [1:   4]) = [  7.67238E+14 0.00302  2.80653E-02 0.00295 ];
SM149_CAPT                (idx, [1:   4]) = [  2.41439E+14 0.00605  8.83156E-03 0.00600 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001183 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.15504E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001183 5.00816E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3306783 3.31146E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1650328 1.65262E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44072 4.40779E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001183 5.00816E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.09782E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 0.0E+00  4.50056E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.68592E+16 2.3E-05  3.68592E+16 2.3E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36422E+16 4.6E-06  1.36422E+16 4.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.73283E+16 0.00047  2.11941E+16 0.00047  6.13418E+15 0.00125 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.09705E+16 0.00031  3.48363E+16 0.00029  6.13418E+15 0.00125 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.12772E+16 0.00052  4.12772E+16 0.00052  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.91461E+18 0.00051  5.09270E+17 0.00048  1.40534E+18 0.00057 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.63922E+14 0.00509 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.13344E+16 0.00032 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.41962E+18 0.00068 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.09612E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.09612E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.48542E+00 0.00060 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.84875E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.22201E-01 0.00037 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24215E+00 0.00038 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94256E-01 3.7E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96910E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.00992E-01 0.00066 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.93049E-01 0.00066 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.70185E+00 2.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05907E+02 4.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.93118E-01 0.00068  8.88474E-01 0.00066  4.57491E-03 0.01059 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.93230E-01 0.00032 ];
COL_KEFF                  (idx, [1:   2]) = [  8.93091E-01 0.00052 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.93230E-01 0.00032 ];
ABS_KINF                  (idx, [1:   2]) = [  9.01175E-01 0.00032 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70008E+01 0.00023 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70053E+01 0.00010 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  8.30648E-07 0.00402 ];
IMP_EALF                  (idx, [1:   2]) = [  8.24225E-07 0.00172 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.84728E-01 0.00278 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.84322E-01 0.00120 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.82228E-03 0.00613  1.44866E-04 0.03712  1.05229E-03 0.01459  9.15517E-04 0.01569  2.54194E-03 0.00934  9.01852E-04 0.01580  2.65824E-04 0.02896 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.42406E-01 0.01529  9.38568E-03 0.02614  3.07678E-02 0.00046  1.11005E-01 0.00055  3.22595E-01 0.00036  1.27872E+00 0.00205  7.54146E+00 0.01712 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.09090E-03 0.00919  1.33684E-04 0.05883  9.07250E-04 0.02302  8.02154E-04 0.02359  2.22309E-03 0.01382  7.95674E-04 0.02339  2.29040E-04 0.04431 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.41184E-01 0.02252  1.25833E-02 0.00118  3.07684E-02 0.00067  1.11077E-01 0.00078  3.22576E-01 0.00052  1.27536E+00 0.00311  8.20581E+00 0.01219 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.71976E-05 0.00145  2.71835E-05 0.00146  3.00716E-05 0.01948 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.42854E-05 0.00130  2.42728E-05 0.00131  2.68455E-05 0.01940 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.10855E-03 0.01069  1.25804E-04 0.07037  9.06434E-04 0.02563  8.23935E-04 0.02727  2.19483E-03 0.01633  8.17473E-04 0.02679  2.40072E-04 0.05167 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.56178E-01 0.02700  1.25797E-02 0.00178  3.07360E-02 0.00085  1.10945E-01 0.00107  3.22792E-01 0.00066  1.28334E+00 0.00376  8.30477E+00 0.01613 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.65135E-05 0.00352  2.64996E-05 0.00353  2.69475E-05 0.05327 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.36746E-05 0.00345  2.36623E-05 0.00347  2.40588E-05 0.05352 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.90893E-03 0.04119  1.52684E-04 0.23323  8.52650E-04 0.08630  8.05912E-04 0.10127  2.08895E-03 0.06098  8.14751E-04 0.10482  1.93989E-04 0.15307 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.11770E-01 0.08365  1.25555E-02 0.00373  3.06994E-02 0.00196  1.11149E-01 0.00234  3.22433E-01 0.00178  1.27961E+00 0.00914  8.20094E+00 0.03874 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.87087E-03 0.03995  1.48748E-04 0.22599  8.38638E-04 0.08537  8.19414E-04 0.09489  2.09640E-03 0.05977  7.81063E-04 0.10211  1.86617E-04 0.15197 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.11245E-01 0.08190  1.25555E-02 0.00373  3.06977E-02 0.00195  1.11158E-01 0.00234  3.22424E-01 0.00177  1.28097E+00 0.00894  8.20010E+00 0.03874 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.86243E+02 0.04149 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.68651E-05 0.00093 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.39884E-05 0.00065 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.96893E-03 0.00724 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.85034E+02 0.00729 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.52092E-07 0.00080 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.65624E-06 0.00045  2.65608E-06 0.00045  2.68587E-06 0.00633 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.79957E-05 0.00095  3.80113E-05 0.00096  3.50456E-05 0.01209 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.20266E-01 0.00037  6.20555E-01 0.00037  5.81635E-01 0.00993 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06148E+01 0.01435 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.43418E+01 0.00051  3.28134E+01 0.00052 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.93884E+04 0.00356  3.03465E+05 0.00142  6.10437E+05 0.00099  6.51008E+05 0.00062  5.98834E+05 0.00068  6.41869E+05 0.00063  4.36086E+05 0.00075  3.85856E+05 0.00081  2.95484E+05 0.00081  2.41236E+05 0.00072  2.08121E+05 0.00073  1.87386E+05 0.00075  1.72959E+05 0.00102  1.64495E+05 0.00075  1.60241E+05 0.00082  1.38485E+05 0.00089  1.36955E+05 0.00090  1.35646E+05 0.00088  1.33305E+05 0.00083  2.60278E+05 0.00069  2.51193E+05 0.00071  1.81176E+05 0.00066  1.17322E+05 0.00095  1.34942E+05 0.00081  1.28798E+05 0.00102  1.14107E+05 0.00083  1.84256E+05 0.00064  4.25588E+04 0.00141  5.28203E+04 0.00127  4.81322E+04 0.00185  2.82335E+04 0.00193  4.87150E+04 0.00146  3.21120E+04 0.00150  2.60988E+04 0.00211  4.43669E+03 0.00470  3.75918E+03 0.00385  3.31436E+03 0.00392  3.21203E+03 0.00288  3.18808E+03 0.00327  3.57787E+03 0.00461  4.24621E+03 0.00392  4.34880E+03 0.00388  8.71338E+03 0.00260  1.43764E+04 0.00231  1.87082E+04 0.00284  5.02865E+04 0.00121  5.26813E+04 0.00167  5.61806E+04 0.00168  3.62165E+04 0.00142  2.54065E+04 0.00157  1.87200E+04 0.00201  2.20995E+04 0.00189  4.31328E+04 0.00162  6.12459E+04 0.00188  1.22234E+05 0.00188  1.93399E+05 0.00199  2.93932E+05 0.00191  1.89589E+05 0.00195  1.36746E+05 0.00187  9.85302E+04 0.00228  8.86809E+04 0.00236  8.74825E+04 0.00200  7.33305E+04 0.00226  4.96619E+04 0.00223  4.58729E+04 0.00207  4.08876E+04 0.00230  3.46440E+04 0.00241  2.73550E+04 0.00227  1.83140E+04 0.00265  6.50100E+03 0.00326 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.01034E-01 0.00053 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.53844E+18 0.00058  3.76207E+17 0.00171 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39235E-01 0.00011  1.56842E+00 0.00046 ];
INF_CAPT                  (idx, [1:   4]) = [  8.36680E-03 0.00063  3.84317E-02 0.00100 ];
INF_ABS                   (idx, [1:   4]) = [  1.00878E-02 0.00054  6.76632E-02 0.00128 ];
INF_FISS                  (idx, [1:   4]) = [  1.72099E-03 0.00049  2.92315E-02 0.00167 ];
INF_NSF                   (idx, [1:   4]) = [  4.66497E-03 0.00054  7.89175E-02 0.00172 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.71063E+00 9.2E-05  2.69973E+00 6.4E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05837E+02 1.1E-05  2.05923E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.51921E-08 0.00036  2.58423E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29144E-01 0.00012  1.50075E+00 0.00054 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43844E-01 0.00018  3.98246E-01 0.00059 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62570E-02 0.00030  9.40840E-02 0.00132 ];
INF_SCATT3                (idx, [1:   4]) = [  7.28134E-03 0.00309  2.81729E-02 0.00310 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02992E-02 0.00208 -8.87519E-03 0.00666 ];
INF_SCATT5                (idx, [1:   4]) = [  1.82543E-04 0.10347  6.64784E-03 0.00831 ];
INF_SCATT6                (idx, [1:   4]) = [  5.13936E-03 0.00367 -1.72149E-02 0.00346 ];
INF_SCATT7                (idx, [1:   4]) = [  7.64717E-04 0.02662  4.34906E-04 0.10233 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29188E-01 0.00012  1.50075E+00 0.00054 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43844E-01 0.00018  3.98246E-01 0.00059 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62571E-02 0.00030  9.40840E-02 0.00132 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.28142E-03 0.00310  2.81729E-02 0.00310 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02991E-02 0.00208 -8.87519E-03 0.00666 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.82589E-04 0.10348  6.64784E-03 0.00831 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.13928E-03 0.00368 -1.72149E-02 0.00346 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.64622E-04 0.02665  4.34906E-04 0.10233 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12702E-01 0.00034  1.01843E+00 0.00041 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56714E+00 0.00034  3.27304E-01 0.00041 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.00440E-02 0.00054  6.76632E-02 0.00128 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70113E-02 0.00016  6.88593E-02 0.00146 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12224E-01 0.00011  1.69203E-02 0.00049  1.19007E-03 0.00400  1.49956E+00 0.00055 ];
INF_S1                    (idx, [1:   8]) = [  2.38958E-01 0.00018  4.88579E-03 0.00086  5.15862E-04 0.00852  3.97730E-01 0.00059 ];
INF_S2                    (idx, [1:   8]) = [  9.78008E-02 0.00029 -1.54383E-03 0.00250  2.84551E-04 0.00986  9.37994E-02 0.00133 ];
INF_S3                    (idx, [1:   8]) = [  9.03230E-03 0.00242 -1.75095E-03 0.00185  1.02791E-04 0.02418  2.80702E-02 0.00310 ];
INF_S4                    (idx, [1:   8]) = [ -9.76028E-03 0.00221 -5.38898E-04 0.00419  4.29826E-06 0.52733 -8.87948E-03 0.00659 ];
INF_S5                    (idx, [1:   8]) = [  1.44084E-04 0.13036  3.84585E-05 0.06231 -3.89663E-05 0.04667  6.68681E-03 0.00829 ];
INF_S6                    (idx, [1:   8]) = [  5.27268E-03 0.00355 -1.33321E-04 0.01248 -5.12355E-05 0.02836 -1.71637E-02 0.00345 ];
INF_S7                    (idx, [1:   8]) = [  9.27111E-04 0.02166 -1.62394E-04 0.01664 -4.55860E-05 0.01946  4.80492E-04 0.09255 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12268E-01 0.00011  1.69203E-02 0.00049  1.19007E-03 0.00400  1.49956E+00 0.00055 ];
INF_SP1                   (idx, [1:   8]) = [  2.38958E-01 0.00018  4.88579E-03 0.00086  5.15862E-04 0.00852  3.97730E-01 0.00059 ];
INF_SP2                   (idx, [1:   8]) = [  9.78009E-02 0.00029 -1.54383E-03 0.00250  2.84551E-04 0.00986  9.37994E-02 0.00133 ];
INF_SP3                   (idx, [1:   8]) = [  9.03237E-03 0.00242 -1.75095E-03 0.00185  1.02791E-04 0.02418  2.80702E-02 0.00310 ];
INF_SP4                   (idx, [1:   8]) = [ -9.76020E-03 0.00221 -5.38898E-04 0.00419  4.29826E-06 0.52733 -8.87948E-03 0.00659 ];
INF_SP5                   (idx, [1:   8]) = [  1.44130E-04 0.13038  3.84585E-05 0.06231 -3.89663E-05 0.04667  6.68681E-03 0.00829 ];
INF_SP6                   (idx, [1:   8]) = [  5.27260E-03 0.00355 -1.33321E-04 0.01248 -5.12355E-05 0.02836 -1.71637E-02 0.00345 ];
INF_SP7                   (idx, [1:   8]) = [  9.27015E-04 0.02169 -1.62394E-04 0.01664 -4.55860E-05 0.01946  4.80492E-04 0.09255 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31891E-01 0.00057  1.21946E+00 0.00855 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33548E-01 0.00087  1.33816E+00 0.01024 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33650E-01 0.00079  1.32343E+00 0.00966 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28557E-01 0.00091  1.04575E+00 0.00871 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43746E+00 0.00057  2.73831E-01 0.00863 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42728E+00 0.00087  2.49729E-01 0.01030 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42666E+00 0.00079  2.52432E-01 0.00962 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45846E+00 0.00092  3.19331E-01 0.00872 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.09090E-03 0.00919  1.33684E-04 0.05883  9.07250E-04 0.02302  8.02154E-04 0.02359  2.22309E-03 0.01382  7.95674E-04 0.02339  2.29040E-04 0.04431 ];
LAMBDA                    (idx, [1:  14]) = [  7.41184E-01 0.02252  1.25833E-02 0.00118  3.07684E-02 0.00067  1.11077E-01 0.00078  3.22576E-01 0.00052  1.27536E+00 0.00311  8.20581E+00 0.01219 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0009' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:30:09 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284850 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00173E+00  1.00033E+00  9.99701E-01  9.98274E-01  9.99965E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16665E-02 0.00111  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88334E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06353E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06785E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68178E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.44102E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.44014E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.23808E+00 0.00040  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.15934E-01 0.00113  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001089 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00022E+04 0.00090 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00022E+04 0.00090 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.44126E+02 ;
RUNNING_TIME              (idx, 1)        =  1.09305E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.43100E-01  4.43100E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.68033E-01  1.13667E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.07470E+02  3.28335E+00  2.57920E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.01328E+00  2.88833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.10333E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.09304E+02  1.21148E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97807 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00063E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81009E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.62944E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.67869E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.17895E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.05453E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.44603E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.57488E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.53405E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  4.80995E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.01504E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.91923E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.34087E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  8.90716E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.08095E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.80837E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.35830E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.52845E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.54829E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.00717E+13 ;
CS137_ACTIVITY            (idx, 1)        =  4.00329E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.38477E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.21756E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.58036E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.32576E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.32478E+12 0.00053  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 18 ;
BURNUP                     (idx, [1:  2])  = [  3.00000E+01  3.00028E+01 ];
BURN_DAYS                 (idx, 1)        =  7.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.37397E-01 0.00111 ];
U235_FISS                 (idx, [1:   4]) = [  4.49997E+15 0.00140  3.30447E-01 0.00127 ];
U238_FISS                 (idx, [1:   4]) = [  1.18334E+15 0.00288  8.68804E-02 0.00269 ];
PU239_FISS                (idx, [1:   4]) = [  6.28325E+15 0.00112  4.61385E-01 0.00087 ];
PU240_FISS                (idx, [1:   4]) = [  1.45158E+13 0.02417  1.06613E-03 0.02415 ];
PU241_FISS                (idx, [1:   4]) = [  1.60423E+15 0.00234  1.17803E-01 0.00226 ];
U233_CAPT                 (idx, [1:   4]) = [  8.82078E+09 1.00000  3.02480E-07 1.00000 ];
U235_CAPT                 (idx, [1:   4]) = [  1.01162E+15 0.00295  3.45105E-02 0.00292 ];
U238_CAPT                 (idx, [1:   4]) = [  1.00953E+16 0.00111  3.44350E-01 0.00077 ];
PU239_CAPT                (idx, [1:   4]) = [  3.45325E+15 0.00147  1.17804E-01 0.00141 ];
PU240_CAPT                (idx, [1:   4]) = [  2.66802E+15 0.00175  9.10128E-02 0.00165 ];
PU241_CAPT                (idx, [1:   4]) = [  5.78637E+14 0.00389  1.97391E-02 0.00385 ];
XE135_CAPT                (idx, [1:   4]) = [  7.81503E+14 0.00330  2.66605E-02 0.00328 ];
SM149_CAPT                (idx, [1:   4]) = [  2.47695E+14 0.00601  8.45089E-03 0.00603 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001089 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.09290E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001089 5.00809E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3384356 3.38919E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1572368 1.57453E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44365 4.43737E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001089 5.00809E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.12227E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 0.0E+00  4.50056E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.71756E+16 2.1E-05  3.71756E+16 2.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36142E+16 4.3E-06  1.36142E+16 4.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.93290E+16 0.00045  2.28766E+16 0.00046  6.45248E+15 0.00121 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.29432E+16 0.00031  3.64907E+16 0.00029  6.45248E+15 0.00121 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.32478E+16 0.00053  4.32478E+16 0.00053  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.00675E+18 0.00051  5.33276E+17 0.00050  1.47347E+18 0.00056 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.83818E+14 0.00515 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.33270E+16 0.00031 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.48998E+18 0.00066 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.09035E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.09035E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.44960E+00 0.00059 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.82312E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.17361E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23927E+00 0.00039 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94208E-01 3.8E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96899E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.67560E-01 0.00065 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.59859E-01 0.00065 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.73065E+00 2.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06331E+02 4.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.59910E-01 0.00066  8.55538E-01 0.00065  4.32154E-03 0.01125 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.59470E-01 0.00031 ];
COL_KEFF                  (idx, [1:   2]) = [  8.59715E-01 0.00053 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.59470E-01 0.00031 ];
ABS_KINF                  (idx, [1:   2]) = [  8.67163E-01 0.00031 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.69536E+01 0.00025 ];
IMP_ALF                   (idx, [1:   2]) = [  1.69488E+01 1.0E-04 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  8.71339E-07 0.00429 ];
IMP_EALF                  (idx, [1:   2]) = [  8.72101E-07 0.00170 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.96385E-01 0.00292 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.97376E-01 0.00112 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.93575E-03 0.00675  1.48315E-04 0.03879  1.08265E-03 0.01518  9.15081E-04 0.01667  2.54180E-03 0.00973  9.66296E-04 0.01569  2.81615E-04 0.02912 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.51625E-01 0.01530  9.08422E-03 0.02794  3.06629E-02 0.00044  1.11132E-01 0.00061  3.22820E-01 0.00037  1.25688E+00 0.00229  7.31794E+00 0.01852 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.99791E-03 0.00964  1.31199E-04 0.05726  9.09050E-04 0.02274  7.77867E-04 0.02499  2.13977E-03 0.01465  8.08637E-04 0.02382  2.31389E-04 0.04697 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.37487E-01 0.02425  1.26076E-02 0.00132  3.06777E-02 0.00066  1.11188E-01 0.00088  3.22685E-01 0.00056  1.25725E+00 0.00338  8.06630E+00 0.01353 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.85234E-05 0.00158  2.85127E-05 0.00158  3.03939E-05 0.01732 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.45220E-05 0.00142  2.45127E-05 0.00142  2.61393E-05 0.01736 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.04609E-03 0.01126  1.36904E-04 0.06810  9.42112E-04 0.02631  8.09282E-04 0.02870  2.15394E-03 0.01736  7.70485E-04 0.02934  2.33366E-04 0.05354 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.21225E-01 0.02892  1.26058E-02 0.00195  3.06938E-02 0.00090  1.11216E-01 0.00115  3.23013E-01 0.00069  1.26110E+00 0.00459  7.96342E+00 0.01994 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.76821E-05 0.00357  2.76671E-05 0.00359  2.71505E-05 0.04200 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.37993E-05 0.00351  2.37862E-05 0.00353  2.33749E-05 0.04212 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.87650E-03 0.03772  1.43194E-04 0.20576  8.20165E-04 0.09082  8.81297E-04 0.09239  2.05135E-03 0.05981  7.86244E-04 0.09079  1.94247E-04 0.16066 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.63983E-01 0.08996  1.25838E-02 0.00424  3.06274E-02 0.00211  1.11796E-01 0.00259  3.23099E-01 0.00192  1.26101E+00 0.01093  8.05009E+00 0.04440 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.92451E-03 0.03701  1.43734E-04 0.20382  8.29359E-04 0.08876  8.89258E-04 0.09121  2.05688E-03 0.05878  8.02993E-04 0.08936  2.02282E-04 0.15914 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.64985E-01 0.08884  1.25838E-02 0.00424  3.06291E-02 0.00211  1.11789E-01 0.00259  3.23102E-01 0.00191  1.26047E+00 0.01092  8.03923E+00 0.04454 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.77407E+02 0.03788 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.81153E-05 0.00097 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.41715E-05 0.00072 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.00992E-03 0.00671 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.78238E+02 0.00672 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.53787E-07 0.00078 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.63229E-06 0.00044  2.63214E-06 0.00044  2.65786E-06 0.00608 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.84644E-05 0.00093  3.84822E-05 0.00093  3.50809E-05 0.01165 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.15466E-01 0.00035  6.15914E-01 0.00036  5.53498E-01 0.01064 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.08038E+01 0.01380 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.44014E+01 0.00049  3.28673E+01 0.00060 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.05358E+04 0.00327  3.04641E+05 0.00120  6.10246E+05 0.00113  6.51362E+05 0.00080  5.99092E+05 0.00078  6.41813E+05 0.00077  4.36429E+05 0.00062  3.85419E+05 0.00089  2.95445E+05 0.00089  2.41221E+05 0.00071  2.07991E+05 0.00077  1.87620E+05 0.00116  1.73454E+05 0.00077  1.64628E+05 0.00091  1.60396E+05 0.00077  1.38747E+05 0.00088  1.36763E+05 0.00114  1.35902E+05 0.00082  1.33314E+05 0.00096  2.60052E+05 0.00060  2.51504E+05 0.00065  1.81164E+05 0.00086  1.17288E+05 0.00077  1.35023E+05 0.00078  1.28765E+05 0.00076  1.14206E+05 0.00100  1.83671E+05 0.00087  4.26641E+04 0.00147  5.26567E+04 0.00157  4.78953E+04 0.00140  2.81948E+04 0.00175  4.84795E+04 0.00119  3.18023E+04 0.00158  2.56568E+04 0.00205  4.30422E+03 0.00342  3.63718E+03 0.00343  3.14984E+03 0.00425  3.05921E+03 0.00383  3.09905E+03 0.00372  3.39814E+03 0.00248  4.10774E+03 0.00465  4.26054E+03 0.00396  8.48568E+03 0.00263  1.41320E+04 0.00215  1.85149E+04 0.00186  4.95895E+04 0.00171  5.21672E+04 0.00170  5.56308E+04 0.00111  3.58929E+04 0.00159  2.52328E+04 0.00160  1.85846E+04 0.00126  2.19164E+04 0.00132  4.29115E+04 0.00142  6.08343E+04 0.00153  1.21797E+05 0.00130  1.93572E+05 0.00133  2.94886E+05 0.00157  1.90561E+05 0.00166  1.37125E+05 0.00159  9.89545E+04 0.00202  8.93556E+04 0.00187  8.82087E+04 0.00183  7.38383E+04 0.00176  5.00099E+04 0.00171  4.63403E+04 0.00176  4.12227E+04 0.00173  3.48727E+04 0.00207  2.76569E+04 0.00194  1.84909E+04 0.00147  6.57932E+03 0.00220 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.67413E-01 0.00050 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.61180E+18 0.00043  3.94990E+17 0.00136 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39574E-01 0.00010  1.57047E+00 0.00035 ];
INF_CAPT                  (idx, [1:   4]) = [  8.59276E-03 0.00080  3.91938E-02 0.00079 ];
INF_ABS                   (idx, [1:   4]) = [  1.02238E-02 0.00068  6.70113E-02 0.00102 ];
INF_FISS                  (idx, [1:   4]) = [  1.63101E-03 0.00052  2.78175E-02 0.00134 ];
INF_NSF                   (idx, [1:   4]) = [  4.45966E-03 0.00053  7.59356E-02 0.00138 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.73429E+00 7.0E-05  2.72978E+00 5.3E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06169E+02 8.4E-06  2.06370E+02 1.0E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.48207E-08 0.00037  2.59001E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29355E-01 0.00011  1.50347E+00 0.00042 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43873E-01 0.00022  3.98496E-01 0.00057 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62367E-02 0.00035  9.39731E-02 0.00097 ];
INF_SCATT3                (idx, [1:   4]) = [  7.27042E-03 0.00380  2.80394E-02 0.00251 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03334E-02 0.00211 -8.98826E-03 0.00845 ];
INF_SCATT5                (idx, [1:   4]) = [  1.84264E-04 0.08928  6.65652E-03 0.00580 ];
INF_SCATT6                (idx, [1:   4]) = [  5.11244E-03 0.00286 -1.73337E-02 0.00276 ];
INF_SCATT7                (idx, [1:   4]) = [  7.42397E-04 0.02415  5.60673E-04 0.07588 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29398E-01 0.00011  1.50347E+00 0.00042 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43873E-01 0.00022  3.98496E-01 0.00057 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62369E-02 0.00035  9.39731E-02 0.00097 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.27035E-03 0.00380  2.80394E-02 0.00251 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03335E-02 0.00210 -8.98826E-03 0.00845 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.84317E-04 0.08924  6.65652E-03 0.00580 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.11223E-03 0.00287 -1.73337E-02 0.00276 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.42342E-04 0.02415  5.60673E-04 0.07588 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12699E-01 0.00023  1.02090E+00 0.00031 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56716E+00 0.00023  3.26511E-01 0.00031 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.01803E-02 0.00068  6.70113E-02 0.00102 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70062E-02 0.00025  6.81652E-02 0.00113 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12568E-01 1.0E-04  1.67869E-02 0.00053  1.16765E-03 0.00338  1.50230E+00 0.00042 ];
INF_S1                    (idx, [1:   8]) = [  2.39039E-01 0.00022  4.83419E-03 0.00094  5.04250E-04 0.00752  3.97992E-01 0.00057 ];
INF_S2                    (idx, [1:   8]) = [  9.77760E-02 0.00034 -1.53930E-03 0.00278  2.75435E-04 0.01044  9.36977E-02 0.00098 ];
INF_S3                    (idx, [1:   8]) = [  9.00581E-03 0.00304 -1.73539E-03 0.00233  9.69982E-05 0.02552  2.79424E-02 0.00252 ];
INF_S4                    (idx, [1:   8]) = [ -9.80037E-03 0.00221 -5.33068E-04 0.00497 -2.03288E-07 1.00000 -8.98806E-03 0.00849 ];
INF_S5                    (idx, [1:   8]) = [  1.41079E-04 0.11574  4.31850E-05 0.05519 -4.02258E-05 0.03368  6.69675E-03 0.00576 ];
INF_S6                    (idx, [1:   8]) = [  5.24451E-03 0.00269 -1.32069E-04 0.01685 -5.01359E-05 0.02678 -1.72835E-02 0.00276 ];
INF_S7                    (idx, [1:   8]) = [  9.05951E-04 0.01887 -1.63554E-04 0.01403 -4.48019E-05 0.03659  6.05475E-04 0.06968 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12611E-01 0.00010  1.67869E-02 0.00053  1.16765E-03 0.00338  1.50230E+00 0.00042 ];
INF_SP1                   (idx, [1:   8]) = [  2.39039E-01 0.00022  4.83419E-03 0.00094  5.04250E-04 0.00752  3.97992E-01 0.00057 ];
INF_SP2                   (idx, [1:   8]) = [  9.77762E-02 0.00034 -1.53930E-03 0.00278  2.75435E-04 0.01044  9.36977E-02 0.00098 ];
INF_SP3                   (idx, [1:   8]) = [  9.00574E-03 0.00303 -1.73539E-03 0.00233  9.69982E-05 0.02552  2.79424E-02 0.00252 ];
INF_SP4                   (idx, [1:   8]) = [ -9.80039E-03 0.00220 -5.33068E-04 0.00497 -2.03288E-07 1.00000 -8.98806E-03 0.00849 ];
INF_SP5                   (idx, [1:   8]) = [  1.41132E-04 0.11568  4.31850E-05 0.05519 -4.02258E-05 0.03368  6.69675E-03 0.00576 ];
INF_SP6                   (idx, [1:   8]) = [  5.24430E-03 0.00269 -1.32069E-04 0.01685 -5.01359E-05 0.02678 -1.72835E-02 0.00276 ];
INF_SP7                   (idx, [1:   8]) = [  9.05896E-04 0.01887 -1.63554E-04 0.01403 -4.48019E-05 0.03659  6.05475E-04 0.06968 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31865E-01 0.00069  1.21149E+00 0.00683 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33596E-01 0.00089  1.32265E+00 0.00937 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33428E-01 0.00109  1.31483E+00 0.00767 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28648E-01 0.00099  1.04315E+00 0.00708 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43764E+00 0.00069  2.75454E-01 0.00685 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42699E+00 0.00089  2.52550E-01 0.00937 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42803E+00 0.00109  2.53867E-01 0.00750 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45788E+00 0.00099  3.19944E-01 0.00733 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.99791E-03 0.00964  1.31199E-04 0.05726  9.09050E-04 0.02274  7.77867E-04 0.02499  2.13977E-03 0.01465  8.08637E-04 0.02382  2.31389E-04 0.04697 ];
LAMBDA                    (idx, [1:  14]) = [  7.37487E-01 0.02425  1.26076E-02 0.00132  3.06777E-02 0.00066  1.11188E-01 0.00088  3.22685E-01 0.00056  1.25725E+00 0.00338  8.06630E+00 0.01353 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0009' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:36:05 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284850 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00170E+00  1.00064E+00  9.99150E-01  9.97923E-01  1.00059E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16965E-02 0.00118  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88303E-01 1.4E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06210E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06645E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68404E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.44315E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.44226E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.25043E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.17058E-01 0.00116  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001194 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00024E+04 0.00100 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00024E+04 0.00100 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.73820E+02 ;
RUNNING_TIME              (idx, 1)        =  1.15250E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.43100E-01  4.43100E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.90400E-01  1.10500E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.13337E+02  3.29143E+00  2.57573E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.06870E+00  2.74167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.28000E-02  8.50002E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.15250E+02  1.21186E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97891 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00095E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81223E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.75803E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.67983E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.67754E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.15594E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.52297E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.60206E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.52749E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  6.21984E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.17530E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.28593E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.00284E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  9.33907E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.17246E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.14666E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.39542E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.57042E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.59650E+14 ;
CS134_ACTIVITY            (idx, 1)        =  7.81669E+13 ;
CS137_ACTIVITY            (idx, 1)        =  4.65697E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.50255E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.19161E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.26922E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.35542E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.52220E+12 0.00055  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 19 ;
BURNUP                     (idx, [1:  2])  = [  3.50000E+01  3.50033E+01 ];
BURN_DAYS                 (idx, 1)        =  8.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.81205E-01 0.00109 ];
U235_FISS                 (idx, [1:   4]) = [  3.73623E+15 0.00162  2.75152E-01 0.00141 ];
U238_FISS                 (idx, [1:   4]) = [  1.22990E+15 0.00272  9.05657E-02 0.00252 ];
PU239_FISS                (idx, [1:   4]) = [  6.65520E+15 0.00108  4.90144E-01 0.00086 ];
PU240_FISS                (idx, [1:   4]) = [  1.76765E+13 0.02259  1.30187E-03 0.02257 ];
PU241_FISS                (idx, [1:   4]) = [  1.89550E+15 0.00211  1.39599E-01 0.00199 ];
U235_CAPT                 (idx, [1:   4]) = [  8.40654E+14 0.00312  2.68460E-02 0.00305 ];
U238_CAPT                 (idx, [1:   4]) = [  1.05471E+16 0.00109  3.36797E-01 0.00075 ];
PU239_CAPT                (idx, [1:   4]) = [  3.66462E+15 0.00152  1.17035E-01 0.00147 ];
PU240_CAPT                (idx, [1:   4]) = [  2.99087E+15 0.00169  9.55084E-02 0.00153 ];
PU241_CAPT                (idx, [1:   4]) = [  6.77256E+14 0.00358  2.16294E-02 0.00357 ];
XE135_CAPT                (idx, [1:   4]) = [  7.91820E+14 0.00323  2.52873E-02 0.00320 ];
SM149_CAPT                (idx, [1:   4]) = [  2.58322E+14 0.00622  8.25026E-03 0.00621 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001194 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.30168E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001194 5.00830E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3457289 3.46228E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1499291 1.50140E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44614 4.46182E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001194 5.00830E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.93601E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 0.0E+00  4.50056E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.74560E+16 2.2E-05  3.74560E+16 2.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35893E+16 4.6E-06  1.35893E+16 4.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.13034E+16 0.00044  2.45450E+16 0.00047  6.75837E+15 0.00127 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.48926E+16 0.00031  3.81343E+16 0.00030  6.75837E+15 0.00127 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.52220E+16 0.00055  4.52220E+16 0.00055  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.09825E+18 0.00052  5.57537E+17 0.00051  1.54071E+18 0.00058 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.03601E+14 0.00520 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.52962E+16 0.00032 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.55897E+18 0.00068 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.08459E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.08459E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.41248E+00 0.00061 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.79999E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.11673E-01 0.00040 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23926E+00 0.00041 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94189E-01 3.7E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96869E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.35066E-01 0.00071 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.27613E-01 0.00071 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.75629E+00 2.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06709E+02 4.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.27509E-01 0.00071  8.23536E-01 0.00071  4.07786E-03 0.01146 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.28318E-01 0.00032 ];
COL_KEFF                  (idx, [1:   2]) = [  8.28394E-01 0.00055 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.28318E-01 0.00032 ];
ABS_KINF                  (idx, [1:   2]) = [  8.35774E-01 0.00031 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.68928E+01 0.00026 ];
IMP_ALF                   (idx, [1:   2]) = [  1.68917E+01 0.00011 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  9.26251E-07 0.00445 ];
IMP_EALF                  (idx, [1:   2]) = [  9.23505E-07 0.00183 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.09212E-01 0.00286 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.10144E-01 0.00121 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.08218E-03 0.00595  1.40279E-04 0.04001  1.11724E-03 0.01430  9.48631E-04 0.01667  2.59074E-03 0.00971  1.00593E-03 0.01542  2.79368E-04 0.02995 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.29101E-01 0.01550  8.93971E-03 0.02878  3.05581E-02 0.00042  1.10959E-01 0.00290  3.22817E-01 0.00040  1.24531E+00 0.00285  6.96752E+00 0.02038 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.93719E-03 0.00956  1.12947E-04 0.06262  8.92262E-04 0.02229  7.48764E-04 0.02453  2.13730E-03 0.01528  8.17003E-04 0.02402  2.28920E-04 0.04470 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.36580E-01 0.02343  1.26252E-02 0.00148  3.05648E-02 0.00064  1.11422E-01 0.00091  3.23174E-01 0.00059  1.24351E+00 0.00375  7.93079E+00 0.01393 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.98781E-05 0.00158  2.98651E-05 0.00159  3.23220E-05 0.01920 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.47181E-05 0.00140  2.47073E-05 0.00141  2.67399E-05 0.01927 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.93263E-03 0.01156  1.12341E-04 0.07810  8.94175E-04 0.02765  7.79969E-04 0.03006  2.09409E-03 0.01892  8.17035E-04 0.02806  2.35028E-04 0.05351 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.49230E-01 0.02987  1.26199E-02 0.00228  3.05668E-02 0.00085  1.11314E-01 0.00117  3.23455E-01 0.00085  1.24052E+00 0.00516  7.95101E+00 0.01996 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.89772E-05 0.00374  2.89642E-05 0.00377  2.68260E-05 0.04744 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.39730E-05 0.00368  2.39622E-05 0.00370  2.22129E-05 0.04745 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.91233E-03 0.04088  7.27145E-05 0.34914  9.67211E-04 0.08766  7.20882E-04 0.10841  2.11512E-03 0.06498  8.49676E-04 0.09372  1.86725E-04 0.18932 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  6.38297E-01 0.08175  1.24863E-02 0.00011  3.04897E-02 0.00185  1.11072E-01 0.00312  3.23256E-01 0.00211  1.22080E+00 0.01245  7.19032E+00 0.06468 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.95850E-03 0.04019  7.05909E-05 0.36146  9.75902E-04 0.08627  7.11869E-04 0.10889  2.15488E-03 0.06371  8.49820E-04 0.09287  1.95436E-04 0.18856 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  6.41934E-01 0.08201  1.24863E-02 0.00011  3.04875E-02 0.00184  1.11050E-01 0.00311  3.23282E-01 0.00211  1.22045E+00 0.01245  7.19032E+00 0.06468 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.71251E+02 0.04146 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.94729E-05 0.00102 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.43829E-05 0.00073 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.00926E-03 0.00773 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.69963E+02 0.00767 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.54797E-07 0.00083 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.61336E-06 0.00043  2.61335E-06 0.00043  2.61488E-06 0.00665 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.89064E-05 0.00100  3.89254E-05 0.00100  3.52042E-05 0.01157 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.09832E-01 0.00040  6.10404E-01 0.00040  5.26034E-01 0.00986 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07423E+01 0.01384 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.44226E+01 0.00053  3.28675E+01 0.00059 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.08611E+04 0.00221  3.05520E+05 0.00130  6.10174E+05 0.00082  6.51612E+05 0.00072  5.99050E+05 0.00065  6.42341E+05 0.00055  4.36253E+05 0.00078  3.85949E+05 0.00072  2.95566E+05 0.00080  2.41560E+05 0.00101  2.08002E+05 0.00077  1.87158E+05 0.00053  1.73324E+05 0.00077  1.64879E+05 0.00095  1.60467E+05 0.00076  1.38643E+05 0.00099  1.36948E+05 0.00080  1.35993E+05 0.00116  1.33820E+05 0.00099  2.59929E+05 0.00066  2.51758E+05 0.00074  1.81091E+05 0.00077  1.17553E+05 0.00112  1.35098E+05 0.00113  1.28798E+05 0.00084  1.13997E+05 0.00097  1.83028E+05 0.00071  4.25916E+04 0.00129  5.21609E+04 0.00117  4.74965E+04 0.00172  2.80089E+04 0.00203  4.83220E+04 0.00155  3.14272E+04 0.00201  2.51764E+04 0.00178  4.16867E+03 0.00350  3.51135E+03 0.00473  3.08448E+03 0.00493  2.98398E+03 0.00431  3.01163E+03 0.00369  3.30758E+03 0.00355  3.99240E+03 0.00360  4.14096E+03 0.00317  8.34306E+03 0.00227  1.39608E+04 0.00218  1.83076E+04 0.00265  4.90001E+04 0.00144  5.14460E+04 0.00129  5.50307E+04 0.00147  3.54817E+04 0.00168  2.49396E+04 0.00207  1.84226E+04 0.00174  2.16791E+04 0.00213  4.24543E+04 0.00173  6.04559E+04 0.00202  1.21467E+05 0.00204  1.93369E+05 0.00191  2.95252E+05 0.00199  1.91333E+05 0.00200  1.37891E+05 0.00211  9.94838E+04 0.00231  8.97845E+04 0.00203  8.85478E+04 0.00208  7.40948E+04 0.00209  5.02768E+04 0.00228  4.65330E+04 0.00229  4.15185E+04 0.00224  3.51274E+04 0.00226  2.77646E+04 0.00249  1.86211E+04 0.00223  6.62807E+03 0.00230 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.35853E-01 0.00053 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.68506E+18 0.00053  4.13236E+17 0.00164 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39825E-01 8.1E-05  1.57142E+00 0.00045 ];
INF_CAPT                  (idx, [1:   4]) = [  8.81024E-03 0.00075  3.98318E-02 0.00096 ];
INF_ABS                   (idx, [1:   4]) = [  1.03661E-02 0.00067  6.63789E-02 0.00120 ];
INF_FISS                  (idx, [1:   4]) = [  1.55586E-03 0.00043  2.65471E-02 0.00159 ];
INF_NSF                   (idx, [1:   4]) = [  4.28733E-03 0.00045  7.31760E-02 0.00163 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.75560E+00 7.0E-05  2.75646E+00 6.2E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06467E+02 9.4E-06  2.06767E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.44521E-08 0.00039  2.59523E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29451E-01 9.2E-05  1.50508E+00 0.00053 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43827E-01 0.00017  3.98569E-01 0.00063 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62329E-02 0.00036  9.38150E-02 0.00096 ];
INF_SCATT3                (idx, [1:   4]) = [  7.25594E-03 0.00365  2.80283E-02 0.00281 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03359E-02 0.00247 -9.10631E-03 0.00672 ];
INF_SCATT5                (idx, [1:   4]) = [  2.19190E-04 0.08135  6.73361E-03 0.00805 ];
INF_SCATT6                (idx, [1:   4]) = [  5.16471E-03 0.00333 -1.72199E-02 0.00283 ];
INF_SCATT7                (idx, [1:   4]) = [  7.88695E-04 0.02178  6.03970E-04 0.07989 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29496E-01 9.3E-05  1.50508E+00 0.00053 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43827E-01 0.00017  3.98569E-01 0.00063 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62334E-02 0.00036  9.38150E-02 0.00096 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.25619E-03 0.00364  2.80283E-02 0.00281 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03353E-02 0.00247 -9.10631E-03 0.00672 ];
INF_SCATTP5               (idx, [1:   4]) = [  2.19102E-04 0.08142  6.73361E-03 0.00805 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.16456E-03 0.00334 -1.72199E-02 0.00283 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.88649E-04 0.02184  6.03970E-04 0.07989 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12697E-01 0.00029  1.02285E+00 0.00041 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56717E+00 0.00029  3.25890E-01 0.00041 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.03215E-02 0.00071  6.63789E-02 0.00120 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70094E-02 0.00017  6.75007E-02 0.00151 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12815E-01 8.3E-05  1.66362E-02 0.00055  1.15677E-03 0.00529  1.50392E+00 0.00053 ];
INF_S1                    (idx, [1:   8]) = [  2.39032E-01 0.00017  4.79417E-03 0.00120  4.96596E-04 0.00712  3.98072E-01 0.00064 ];
INF_S2                    (idx, [1:   8]) = [  9.77649E-02 0.00036 -1.53198E-03 0.00273  2.73296E-04 0.00882  9.35417E-02 0.00096 ];
INF_S3                    (idx, [1:   8]) = [  8.98097E-03 0.00282 -1.72503E-03 0.00170  9.56696E-05 0.01964  2.79327E-02 0.00281 ];
INF_S4                    (idx, [1:   8]) = [ -9.80850E-03 0.00252 -5.27403E-04 0.00651 -1.12380E-06 1.00000 -9.10519E-03 0.00667 ];
INF_S5                    (idx, [1:   8]) = [  1.70971E-04 0.10566  4.82189E-05 0.06410 -4.16716E-05 0.04476  6.77528E-03 0.00802 ];
INF_S6                    (idx, [1:   8]) = [  5.29282E-03 0.00321 -1.28109E-04 0.01824 -5.19349E-05 0.02426 -1.71679E-02 0.00282 ];
INF_S7                    (idx, [1:   8]) = [  9.54325E-04 0.01783 -1.65630E-04 0.01565 -4.62963E-05 0.03112  6.50266E-04 0.07434 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12860E-01 8.3E-05  1.66362E-02 0.00055  1.15677E-03 0.00529  1.50392E+00 0.00053 ];
INF_SP1                   (idx, [1:   8]) = [  2.39033E-01 0.00017  4.79417E-03 0.00120  4.96596E-04 0.00712  3.98072E-01 0.00064 ];
INF_SP2                   (idx, [1:   8]) = [  9.77654E-02 0.00036 -1.53198E-03 0.00273  2.73296E-04 0.00882  9.35417E-02 0.00096 ];
INF_SP3                   (idx, [1:   8]) = [  8.98122E-03 0.00282 -1.72503E-03 0.00170  9.56696E-05 0.01964  2.79327E-02 0.00281 ];
INF_SP4                   (idx, [1:   8]) = [ -9.80791E-03 0.00252 -5.27403E-04 0.00651 -1.12380E-06 1.00000 -9.10519E-03 0.00667 ];
INF_SP5                   (idx, [1:   8]) = [  1.70883E-04 0.10574  4.82189E-05 0.06410 -4.16716E-05 0.04476  6.77528E-03 0.00802 ];
INF_SP6                   (idx, [1:   8]) = [  5.29267E-03 0.00321 -1.28109E-04 0.01824 -5.19349E-05 0.02426 -1.71679E-02 0.00282 ];
INF_SP7                   (idx, [1:   8]) = [  9.54279E-04 0.01788 -1.65630E-04 0.01565 -4.62963E-05 0.03112  6.50266E-04 0.07434 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31916E-01 0.00059  1.21610E+00 0.00672 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33318E-01 0.00101  1.33164E+00 0.00921 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33800E-01 0.00100  1.31884E+00 0.00910 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28709E-01 0.00089  1.04543E+00 0.00595 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43731E+00 0.00059  2.74398E-01 0.00675 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42870E+00 0.00101  2.50825E-01 0.00917 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42576E+00 0.00100  2.53251E-01 0.00913 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45748E+00 0.00089  3.19118E-01 0.00595 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.93719E-03 0.00956  1.12947E-04 0.06262  8.92262E-04 0.02229  7.48764E-04 0.02453  2.13730E-03 0.01528  8.17003E-04 0.02402  2.28920E-04 0.04470 ];
LAMBDA                    (idx, [1:  14]) = [  7.36580E-01 0.02343  1.26252E-02 0.00148  3.05648E-02 0.00064  1.11422E-01 0.00091  3.23174E-01 0.00059  1.24351E+00 0.00375  7.93079E+00 0.01393 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0009' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Mon Jul 20 23:40:50 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:42:04 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595284850 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00209E+00  9.99849E-01  1.00075E+00  9.97583E-01  9.99728E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.17069E-02 0.00111  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88293E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05719E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06153E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68737E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.44040E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.43951E+01 0.00052  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.26989E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.17839E-01 0.00112  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001059 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00021E+04 0.00098 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00021E+04 0.00098 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  6.03628E+02 ;
RUNNING_TIME              (idx, 1)        =  1.21219E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.43100E-01  4.43100E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  4.13433E-01  1.11000E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.19227E+02  3.30125E+00  2.58867E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.12382E+00  2.72833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.62833E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.21219E+02  1.21219E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97966 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00072E+00 0.00021 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.81414E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6134.15;
MEMSIZE                   (idx, 1)        = 6040.54;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 170.29;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.61;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.89527E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68598E+04 ;
TOT_SF_RATE               (idx, 1)        =  5.71388E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.25662E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.60011E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.63862E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.52593E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  7.85270E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.34082E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  6.88001E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.07558E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  9.72689E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.26524E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.45918E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.43547E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.61177E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.64653E+14 ;
CS134_ACTIVITY            (idx, 1)        =  9.76326E+13 ;
CS137_ACTIVITY            (idx, 1)        =  5.30649E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.62742E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.17089E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.01486E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.38550E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.70250E+12 0.00054  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 20 ;
BURNUP                     (idx, [1:  2])  = [  4.00000E+01  4.00038E+01 ];
BURN_DAYS                 (idx, 1)        =  1.00000E+03 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  8.18145E-01 0.00115 ];
U235_FISS                 (idx, [1:   4]) = [  3.05660E+15 0.00176  2.25241E-01 0.00158 ];
U238_FISS                 (idx, [1:   4]) = [  1.28317E+15 0.00275  9.45463E-02 0.00255 ];
PU239_FISS                (idx, [1:   4]) = [  7.00701E+15 0.00105  5.16373E-01 0.00083 ];
PU240_FISS                (idx, [1:   4]) = [  2.00462E+13 0.02192  1.47702E-03 0.02186 ];
PU241_FISS                (idx, [1:   4]) = [  2.14680E+15 0.00215  1.58198E-01 0.00201 ];
U235_CAPT                 (idx, [1:   4]) = [  6.89765E+14 0.00404  2.08293E-02 0.00401 ];
U238_CAPT                 (idx, [1:   4]) = [  1.09233E+16 0.00111  3.29832E-01 0.00078 ];
PU239_CAPT                (idx, [1:   4]) = [  3.83469E+15 0.00158  1.15812E-01 0.00163 ];
PU240_CAPT                (idx, [1:   4]) = [  3.26680E+15 0.00173  9.86442E-02 0.00157 ];
PU241_CAPT                (idx, [1:   4]) = [  7.71314E+14 0.00323  2.32956E-02 0.00328 ];
XE135_CAPT                (idx, [1:   4]) = [  8.02200E+14 0.00342  2.42256E-02 0.00340 ];
SM149_CAPT                (idx, [1:   4]) = [  2.67902E+14 0.00549  8.09091E-03 0.00551 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001059 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.27353E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001059 5.00827E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3515806 3.52108E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1441017 1.44295E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44236 4.42437E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001059 5.00827E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -2.60770E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50056E+05 0.0E+00  4.50056E+05 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.77092E+16 2.0E-05  3.77092E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35671E+16 4.1E-06  1.35671E+16 4.1E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.31148E+16 0.00047  2.60989E+16 0.00047  7.01594E+15 0.00128 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.66819E+16 0.00033  3.96659E+16 0.00031  7.01594E+15 0.00128 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.70250E+16 0.00054  4.70250E+16 0.00054  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.18051E+18 0.00052  5.79442E+17 0.00049  1.60107E+18 0.00057 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.16155E+14 0.00531 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.70980E+16 0.00034 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.61983E+18 0.00068 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.07883E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.07883E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.38203E+00 0.00064 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.78448E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.07127E-01 0.00039 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.23909E+00 0.00039 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94208E-01 3.7E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96925E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.09258E-01 0.00069 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.02097E-01 0.00070 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.77946E+00 2.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.07047E+02 4.1E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.02025E-01 0.00070  7.98345E-01 0.00070  3.75251E-03 0.01230 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.02024E-01 0.00034 ];
COL_KEFF                  (idx, [1:   2]) = [  8.02011E-01 0.00054 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.02024E-01 0.00034 ];
ABS_KINF                  (idx, [1:   2]) = [  8.09184E-01 0.00033 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.68292E+01 0.00026 ];
IMP_ALF                   (idx, [1:   2]) = [  1.68353E+01 0.00010 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  9.87054E-07 0.00449 ];
IMP_EALF                  (idx, [1:   2]) = [  9.76946E-07 0.00175 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.23252E-01 0.00286 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.22490E-01 0.00111 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.10114E-03 0.00647  1.28783E-04 0.04518  1.13146E-03 0.01388  9.53664E-04 0.01652  2.55346E-03 0.00932  1.04004E-03 0.01539  2.93738E-04 0.02838 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.46516E-01 0.01488  8.08871E-03 0.03376  3.04378E-02 0.00041  1.11762E-01 0.00065  3.23392E-01 0.00042  1.23475E+00 0.00269  7.07889E+00 0.01958 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.74638E-03 0.01013  1.04008E-04 0.06769  9.09258E-04 0.02306  7.19485E-04 0.02570  1.97699E-03 0.01531  8.07965E-04 0.02581  2.28672E-04 0.04689 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.41806E-01 0.02350  1.26808E-02 0.00176  3.04304E-02 0.00060  1.11766E-01 0.00094  3.23422E-01 0.00064  1.22838E+00 0.00399  7.81372E+00 0.01527 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.09490E-05 0.00160  3.09302E-05 0.00160  3.45755E-05 0.01773 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.48151E-05 0.00139  2.48000E-05 0.00139  2.77207E-05 0.01769 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.66374E-03 0.01248  1.03112E-04 0.07898  8.92476E-04 0.02789  6.91220E-04 0.03067  1.93232E-03 0.01892  8.13532E-04 0.02911  2.31077E-04 0.05356 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.59455E-01 0.02998  1.26560E-02 0.00264  3.04326E-02 0.00077  1.11760E-01 0.00134  3.23402E-01 0.00082  1.23071E+00 0.00533  7.76877E+00 0.02275 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.99669E-05 0.00384  2.99462E-05 0.00385  2.85593E-05 0.04647 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.40295E-05 0.00380  2.40128E-05 0.00381  2.29077E-05 0.04644 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.40015E-03 0.04357  1.05027E-04 0.28931  9.18874E-04 0.09180  7.92756E-04 0.10608  1.51850E-03 0.06958  8.22534E-04 0.09874  2.42461E-04 0.17595 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.50462E-01 0.09251  1.26908E-02 0.00736  3.05254E-02 0.00196  1.12013E-01 0.00300  3.23545E-01 0.00244  1.20437E+00 0.01342  7.91602E+00 0.05221 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.45371E-03 0.04266  9.97718E-05 0.26314  9.28000E-04 0.09066  8.18008E-04 0.10413  1.56576E-03 0.06845  8.05673E-04 0.09714  2.36488E-04 0.17331 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.48695E-01 0.09212  1.26908E-02 0.00736  3.05250E-02 0.00195  1.12015E-01 0.00300  3.23544E-01 0.00244  1.20529E+00 0.01334  7.91602E+00 0.05221 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.47808E+02 0.04421 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  3.05461E-05 0.00106 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.44927E-05 0.00079 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.61396E-03 0.00785 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.51051E+02 0.00779 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.54458E-07 0.00084 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.59979E-06 0.00045  2.59968E-06 0.00045  2.62438E-06 0.00627 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.91728E-05 0.00097  3.91906E-05 0.00098  3.55085E-05 0.01118 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.05325E-01 0.00039  6.05999E-01 0.00039  5.06304E-01 0.01030 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06252E+01 0.01416 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.43951E+01 0.00052  3.28634E+01 0.00058 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.22976E+04 0.00384  3.07375E+05 0.00125  6.10008E+05 0.00060  6.50217E+05 0.00068  5.99039E+05 0.00061  6.41652E+05 0.00073  4.35965E+05 0.00071  3.86040E+05 0.00067  2.95244E+05 0.00069  2.41606E+05 0.00074  2.08160E+05 0.00090  1.87468E+05 0.00078  1.73277E+05 0.00066  1.65017E+05 0.00083  1.60640E+05 0.00089  1.38776E+05 0.00078  1.37054E+05 0.00086  1.35782E+05 0.00088  1.33290E+05 0.00093  2.60266E+05 0.00075  2.52026E+05 0.00074  1.81232E+05 0.00099  1.17430E+05 0.00088  1.34657E+05 0.00101  1.28973E+05 0.00103  1.13891E+05 0.00114  1.82578E+05 0.00069  4.24892E+04 0.00158  5.17257E+04 0.00157  4.73060E+04 0.00189  2.79354E+04 0.00183  4.79746E+04 0.00126  3.10529E+04 0.00163  2.48624E+04 0.00203  4.11971E+03 0.00415  3.43690E+03 0.00390  3.00760E+03 0.00454  2.93091E+03 0.00334  2.95790E+03 0.00441  3.22206E+03 0.00406  3.89052E+03 0.00363  4.03146E+03 0.00370  8.23177E+03 0.00337  1.38087E+04 0.00248  1.81052E+04 0.00223  4.85450E+04 0.00150  5.10114E+04 0.00149  5.45418E+04 0.00149  3.54350E+04 0.00159  2.48123E+04 0.00187  1.82522E+04 0.00200  2.15621E+04 0.00192  4.22841E+04 0.00143  6.02271E+04 0.00159  1.20890E+05 0.00147  1.92853E+05 0.00158  2.95033E+05 0.00175  1.91040E+05 0.00188  1.37753E+05 0.00157  9.95118E+04 0.00197  8.98025E+04 0.00215  8.85701E+04 0.00211  7.42208E+04 0.00194  5.03304E+04 0.00223  4.66073E+04 0.00211  4.15360E+04 0.00187  3.50902E+04 0.00194  2.77860E+04 0.00215  1.86653E+04 0.00228  6.61388E+03 0.00303 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.09171E-01 0.00052 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.75158E+18 0.00051  4.28969E+17 0.00169 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.40092E-01 9.9E-05  1.57083E+00 0.00043 ];
INF_CAPT                  (idx, [1:   4]) = [  8.99781E-03 0.00072  4.04612E-02 0.00102 ];
INF_ABS                   (idx, [1:   4]) = [  1.04916E-02 0.00064  6.59950E-02 0.00128 ];
INF_FISS                  (idx, [1:   4]) = [  1.49376E-03 0.00036  2.55338E-02 0.00170 ];
INF_NSF                   (idx, [1:   4]) = [  4.14527E-03 0.00035  7.09973E-02 0.00175 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.77505E+00 6.6E-05  2.78052E+00 5.8E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06732E+02 8.5E-06  2.07123E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.41557E-08 0.00045  2.59749E-06 0.00020 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29593E-01 0.00010  1.50486E+00 0.00050 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43754E-01 0.00022  3.98474E-01 0.00060 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62486E-02 0.00030  9.39267E-02 0.00094 ];
INF_SCATT3                (idx, [1:   4]) = [  7.23586E-03 0.00263  2.82277E-02 0.00285 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03337E-02 0.00171 -9.03510E-03 0.00692 ];
INF_SCATT5                (idx, [1:   4]) = [  1.87093E-04 0.08441  6.73222E-03 0.00979 ];
INF_SCATT6                (idx, [1:   4]) = [  5.15334E-03 0.00258 -1.72786E-02 0.00286 ];
INF_SCATT7                (idx, [1:   4]) = [  7.66518E-04 0.01461  5.58623E-04 0.08675 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29638E-01 0.00010  1.50486E+00 0.00050 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43755E-01 0.00022  3.98474E-01 0.00060 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62488E-02 0.00031  9.39267E-02 0.00094 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.23554E-03 0.00263  2.82277E-02 0.00285 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03339E-02 0.00171 -9.03510E-03 0.00692 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.86646E-04 0.08484  6.73222E-03 0.00979 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.15321E-03 0.00259 -1.72786E-02 0.00286 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.66511E-04 0.01462  5.58623E-04 0.08675 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12789E-01 0.00030  1.02288E+00 0.00036 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56650E+00 0.00030  3.25879E-01 0.00036 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.04472E-02 0.00064  6.59950E-02 0.00128 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70174E-02 0.00019  6.71180E-02 0.00128 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.13075E-01 9.9E-05  1.65188E-02 0.00058  1.14874E-03 0.00416  1.50371E+00 0.00050 ];
INF_S1                    (idx, [1:   8]) = [  2.38997E-01 0.00021  4.75749E-03 0.00124  4.99422E-04 0.00530  3.97975E-01 0.00060 ];
INF_S2                    (idx, [1:   8]) = [  9.77717E-02 0.00029 -1.52314E-03 0.00298  2.70130E-04 0.01118  9.36566E-02 0.00096 ];
INF_S3                    (idx, [1:   8]) = [  8.94819E-03 0.00213 -1.71234E-03 0.00214  9.57787E-05 0.02233  2.81319E-02 0.00288 ];
INF_S4                    (idx, [1:   8]) = [ -9.81377E-03 0.00184 -5.19962E-04 0.00604  2.82718E-08 1.00000 -9.03513E-03 0.00693 ];
INF_S5                    (idx, [1:   8]) = [  1.40597E-04 0.10900  4.64953E-05 0.05395 -3.75953E-05 0.03840  6.76981E-03 0.00975 ];
INF_S6                    (idx, [1:   8]) = [  5.28163E-03 0.00245 -1.28296E-04 0.01799 -5.07971E-05 0.02873 -1.72278E-02 0.00288 ];
INF_S7                    (idx, [1:   8]) = [  9.32741E-04 0.01201 -1.66222E-04 0.01536 -4.34958E-05 0.02646  6.02118E-04 0.08094 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.13119E-01 9.9E-05  1.65188E-02 0.00058  1.14874E-03 0.00416  1.50371E+00 0.00050 ];
INF_SP1                   (idx, [1:   8]) = [  2.38998E-01 0.00021  4.75749E-03 0.00124  4.99422E-04 0.00530  3.97975E-01 0.00060 ];
INF_SP2                   (idx, [1:   8]) = [  9.77719E-02 0.00029 -1.52314E-03 0.00298  2.70130E-04 0.01118  9.36566E-02 0.00096 ];
INF_SP3                   (idx, [1:   8]) = [  8.94787E-03 0.00214 -1.71234E-03 0.00214  9.57787E-05 0.02233  2.81319E-02 0.00288 ];
INF_SP4                   (idx, [1:   8]) = [ -9.81391E-03 0.00184 -5.19962E-04 0.00604  2.82718E-08 1.00000 -9.03513E-03 0.00693 ];
INF_SP5                   (idx, [1:   8]) = [  1.40151E-04 0.10965  4.64953E-05 0.05395 -3.75953E-05 0.03840  6.76981E-03 0.00975 ];
INF_SP6                   (idx, [1:   8]) = [  5.28151E-03 0.00246 -1.28296E-04 0.01799 -5.07971E-05 0.02873 -1.72278E-02 0.00288 ];
INF_SP7                   (idx, [1:   8]) = [  9.32734E-04 0.01202 -1.66222E-04 0.01536 -4.34958E-05 0.02646  6.02118E-04 0.08094 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32166E-01 0.00064  1.21738E+00 0.00743 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33934E-01 0.00093  1.32419E+00 0.01019 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33802E-01 0.00102  1.33081E+00 0.00916 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28844E-01 0.00099  1.04556E+00 0.00722 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43577E+00 0.00064  2.74178E-01 0.00751 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42493E+00 0.00093  2.52356E-01 0.01024 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42574E+00 0.00101  2.50975E-01 0.00908 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45663E+00 0.00099  3.19203E-01 0.00715 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.74638E-03 0.01013  1.04008E-04 0.06769  9.09258E-04 0.02306  7.19485E-04 0.02570  1.97699E-03 0.01531  8.07965E-04 0.02581  2.28672E-04 0.04689 ];
LAMBDA                    (idx, [1:  14]) = [  7.41806E-01 0.02350  1.26808E-02 0.00176  3.04304E-02 0.00060  1.11766E-01 0.00094  3.23422E-01 0.00064  1.22838E+00 0.00399  7.81372E+00 0.01527 ];

