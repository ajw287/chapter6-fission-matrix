
% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0010' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:42:04 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:45:46 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292124 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00150E+00  9.97342E-01  1.00442E+00  9.98283E-01  9.98449E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16661E-02 0.00103  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88334E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.92368E-01 9.8E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.92816E-01 9.7E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.71450E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.43000E+01 0.00040  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.42939E+01 0.00040  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.96156E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.22223E-01 0.00104  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000457 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00009E+04 0.00065 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00009E+04 0.00065 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.66195E+01 ;
RUNNING_TIME              (idx, 1)        =  3.70667E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  4.78167E-01  4.78167E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.41667E-03  1.41667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.22687E+00  3.22687E+00  0.00000E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.70633E+00  0.00000E+00 ];
CPU_USAGE                 (idx, 1)        = 4.48368 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99855E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  8.58593E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6133.74;
MEMSIZE                   (idx, 1)        = 6039.76;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 169.50;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.98;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.66995E+08 ;
TOT_DECAY_HEAT            (idx, 1)        =  6.60401E-04 ;
TOT_SF_RATE               (idx, 1)        =  7.39530E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  8.66995E+08 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  6.60401E-04 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  0.00000E+00 ;
INHALATION_TOXICITY       (idx, 1)        =  7.93191E+03 ;
INGESTION_TOXICITY        (idx, 1)        =  4.18766E+01 ;
ACTINIDE_INH_TOX          (idx, 1)        =  7.93191E+03 ;
ACTINIDE_ING_TOX          (idx, 1)        =  4.18766E+01 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  0.00000E+00 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  0.00000E+00 ;
SR90_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
TE132_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
I131_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
I132_ACTIVITY             (idx, 1)        =  0.00000E+00 ;
CS134_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
CS137_ACTIVITY            (idx, 1)        =  0.00000E+00 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  1.14588E+08 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  0.00000E+00 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  8.66574E+08 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  3.59686E+08 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.82582E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 0 ;
BURNUP                     (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
BURN_DAYS                 (idx, 1)        =  0.00000E+00 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.55477E-01 0.00104 ];
U235_FISS                 (idx, [1:   4]) = [  1.30805E+16 0.00054  9.43108E-01 0.00016 ];
U238_FISS                 (idx, [1:   4]) = [  7.87648E+14 0.00276  5.67837E-02 0.00263 ];
U235_CAPT                 (idx, [1:   4]) = [  2.84955E+15 0.00140  1.99842E-01 0.00125 ];
U238_CAPT                 (idx, [1:   4]) = [  7.20559E+15 0.00102  5.05311E-01 0.00067 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000457 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.50927E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000457 5.00751E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2519417 2.52302E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2450770 2.45420E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 30270 3.02870E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000457 5.00751E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.07336E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50055E+05 2.0E-09  4.50055E+05 2.0E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.40987E+16 1.0E-05  3.40987E+16 1.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38689E+16 1.2E-06  1.38689E+16 1.2E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.42609E+16 0.00048  1.01552E+16 0.00048  4.10569E+15 0.00101 ];
TOT_ABSRATE               (idx, [1:   6]) = [  2.81298E+16 0.00024  2.40241E+16 0.00020  4.10569E+15 0.00101 ];
TOT_SRCRATE               (idx, [1:   6]) = [  2.82582E+16 0.00050  2.82582E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.33169E+18 0.00047  3.68971E+17 0.00045  9.62715E+17 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.71186E+14 0.00600 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  2.83010E+16 0.00025 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  9.70430E+17 0.00056 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12514E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.82877E+00 0.00035 ];
SIX_FF_F                  (idx, [1:   2]) = [  8.04109E-01 0.00030 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.61191E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24877E+00 0.00032 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.96077E-01 2.8E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97857E-01 2.1E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.21411E+00 0.00046 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.20676E+00 0.00046 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.45864E+00 1.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02541E+02 1.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.20677E+00 0.00046  1.19843E+00 0.00046  8.32431E-03 0.00737 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.20668E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.20683E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.20668E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.21403E+00 0.00024 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.75529E+01 0.00018 ];
IMP_ALF                   (idx, [1:   2]) = [  1.75466E+01 7.6E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  4.77497E-07 0.00313 ];
IMP_EALF                  (idx, [1:   2]) = [  4.79568E-07 0.00134 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  1.94534E-01 0.00282 ];
IMP_AFGE                  (idx, [1:   2]) = [  1.95307E-01 0.00111 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.88790E-03 0.00515  1.75116E-04 0.03160  9.36554E-04 0.01312  9.31470E-04 0.01303  2.68969E-03 0.00770  8.70409E-04 0.01417  2.84667E-04 0.02434 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.95995E-01 0.01218  1.09419E-02 0.01684  3.16599E-02 0.00021  1.10110E-01 0.00028  3.20371E-01 0.00021  1.34607E+00 0.00016  8.62108E+00 0.00776 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.03168E-03 0.00767  2.15927E-04 0.04727  1.13416E-03 0.01985  1.11473E-03 0.02010  3.18961E-03 0.01120  1.02845E-03 0.02109  3.48803E-04 0.03505 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.04781E-01 0.01841  1.24907E-02 2.6E-06  3.16721E-02 0.00028  1.10053E-01 0.00038  3.20384E-01 0.00032  1.34591E+00 0.00023  8.85803E+00 0.00192 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.21265E-05 0.00099  2.21159E-05 0.00100  2.37323E-05 0.01069 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.66989E-05 0.00089  2.66861E-05 0.00090  2.86341E-05 0.01066 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.89008E-03 0.00736  2.07757E-04 0.04554  1.10908E-03 0.01827  1.10628E-03 0.01922  3.13545E-03 0.01113  1.00605E-03 0.02052  3.25452E-04 0.03453 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.82976E-01 0.01781  1.24907E-02 2.8E-06  3.16602E-02 0.00032  1.10071E-01 0.00043  3.20386E-01 0.00031  1.34545E+00 0.00026  8.84728E+00 0.00223 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.17232E-05 0.00230  2.17133E-05 0.00232  2.31884E-05 0.02495 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.62122E-05 0.00226  2.62002E-05 0.00228  2.79856E-05 0.02499 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.96438E-03 0.02273  2.52008E-04 0.13640  1.12493E-03 0.05823  1.12373E-03 0.05316  3.14317E-03 0.03383  1.03641E-03 0.06380  2.84126E-04 0.11297 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.44966E-01 0.05901  1.24908E-02 6.9E-06  3.16466E-02 0.00081  1.09962E-01 0.00086  3.20537E-01 0.00092  1.34614E+00 0.00057  8.90943E+00 0.00527 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.90533E-03 0.02237  2.41873E-04 0.12879  1.12569E-03 0.05709  1.12904E-03 0.05131  3.12893E-03 0.03302  9.86186E-04 0.06052  2.93603E-04 0.11131 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.51393E-01 0.05750  1.24908E-02 6.9E-06  3.16508E-02 0.00080  1.09955E-01 0.00085  3.20561E-01 0.00089  1.34609E+00 0.00057  8.90836E+00 0.00526 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -3.21647E+02 0.02290 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.19297E-05 0.00065 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.64611E-05 0.00042 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.94598E-03 0.00431 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -3.16838E+02 0.00441 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.60236E-07 0.00063 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.88831E-06 0.00043  2.88803E-06 0.00043  2.92870E-06 0.00481 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.67281E-05 0.00078  3.67445E-05 0.00079  3.45043E-05 0.00844 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.59768E-01 0.00033  6.58826E-01 0.00033  8.29019E-01 0.00811 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03923E+01 0.01345 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.42939E+01 0.00040  3.43261E+01 0.00039 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.20421E+04 0.00337  2.91415E+05 0.00126  6.03798E+05 0.00082  6.51262E+05 0.00071  6.00722E+05 0.00078  6.46574E+05 0.00077  4.40144E+05 0.00067  3.89772E+05 0.00053  2.97683E+05 0.00087  2.43281E+05 0.00083  2.09733E+05 0.00098  1.89252E+05 0.00085  1.74586E+05 0.00075  1.65948E+05 0.00103  1.61914E+05 0.00092  1.39410E+05 0.00104  1.37785E+05 0.00084  1.36442E+05 0.00087  1.33881E+05 0.00082  2.61483E+05 0.00068  2.51288E+05 0.00083  1.81471E+05 0.00065  1.17208E+05 0.00071  1.35025E+05 0.00107  1.27283E+05 0.00087  1.15562E+05 0.00104  1.89173E+05 0.00109  4.34779E+04 0.00139  5.44437E+04 0.00149  4.93865E+04 0.00173  2.85982E+04 0.00214  4.98610E+04 0.00151  3.38740E+04 0.00169  2.87101E+04 0.00228  5.48414E+03 0.00409  5.43535E+03 0.00332  5.51076E+03 0.00440  5.75283E+03 0.00290  5.66143E+03 0.00326  5.61176E+03 0.00420  5.79198E+03 0.00364  5.46883E+03 0.00382  1.03243E+04 0.00254  1.64412E+04 0.00231  2.09016E+04 0.00245  5.52388E+04 0.00147  5.81541E+04 0.00148  6.31013E+04 0.00103  4.26565E+04 0.00179  3.16553E+04 0.00169  2.43681E+04 0.00167  2.86215E+04 0.00127  5.39126E+04 0.00149  7.24505E+04 0.00128  1.37438E+05 0.00128  2.08843E+05 0.00118  3.07323E+05 0.00118  1.94391E+05 0.00147  1.38408E+05 0.00111  9.89878E+04 0.00125  8.85955E+04 0.00144  8.68873E+04 0.00138  7.22228E+04 0.00130  4.87207E+04 0.00158  4.49001E+04 0.00132  3.98259E+04 0.00164  3.36093E+04 0.00157  2.63486E+04 0.00173  1.75920E+04 0.00180  6.19389E+03 0.00168 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.21419E+00 0.00050 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.05970E+18 0.00050  2.72015E+17 0.00111 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.36367E-01 1.0E-04  1.47809E+00 0.00033 ];
INF_CAPT                  (idx, [1:   4]) = [  6.50545E-03 0.00059  2.70857E-02 0.00033 ];
INF_ABS                   (idx, [1:   4]) = [  9.01992E-03 0.00048  6.82832E-02 0.00081 ];
INF_FISS                  (idx, [1:   4]) = [  2.51446E-03 0.00040  4.11975E-02 0.00112 ];
INF_NSF                   (idx, [1:   4]) = [  6.41412E-03 0.00041  1.00386E-01 0.00112 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.55089E+00 4.6E-05  2.43670E+00 0.0E+00 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03680E+02 4.7E-06  2.02270E+02 4.7E-09 ];
INF_INVV                  (idx, [1:   4]) = [  5.83544E-08 0.00050  2.51553E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.27347E-01 0.00011  1.40982E+00 0.00039 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42022E-01 0.00019  3.74304E-01 0.00047 ];
INF_SCATT2                (idx, [1:   4]) = [  9.54909E-02 0.00024  8.96322E-02 0.00089 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32457E-03 0.00215  2.69331E-02 0.00196 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.01697E-02 0.00151 -8.01395E-03 0.00670 ];
INF_SCATT5                (idx, [1:   4]) = [  1.41392E-04 0.14516  6.02464E-03 0.00664 ];
INF_SCATT6                (idx, [1:   4]) = [  5.02695E-03 0.00347 -1.56535E-02 0.00269 ];
INF_SCATT7                (idx, [1:   4]) = [  7.33224E-04 0.02324  1.78467E-04 0.22729 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.27387E-01 0.00011  1.40982E+00 0.00039 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42023E-01 0.00019  3.74304E-01 0.00047 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.54911E-02 0.00024  8.96322E-02 0.00089 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32434E-03 0.00215  2.69331E-02 0.00196 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.01694E-02 0.00150 -8.01395E-03 0.00670 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.41561E-04 0.14540  6.02464E-03 0.00664 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.02714E-03 0.00347 -1.56535E-02 0.00269 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.33148E-04 0.02324  1.78467E-04 0.22729 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.14359E-01 0.00024  9.53353E-01 0.00030 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55503E+00 0.00024  3.49644E-01 0.00030 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.97987E-03 0.00049  6.82832E-02 0.00081 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69153E-02 0.00022  6.94946E-02 0.00086 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 2.0E-07  1.97197E-07 1.00000 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  9.99972E-01 2.8E-05  2.80813E-05 1.00000 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.09452E-01 0.00010  1.78952E-02 0.00043  1.22167E-03 0.00523  1.40860E+00 0.00039 ];
INF_S1                    (idx, [1:   8]) = [  2.36791E-01 0.00019  5.23123E-03 0.00082  5.14853E-04 0.00817  3.73789E-01 0.00047 ];
INF_S2                    (idx, [1:   8]) = [  9.70365E-02 0.00025 -1.54563E-03 0.00209  2.76631E-04 0.01032  8.93556E-02 0.00089 ];
INF_S3                    (idx, [1:   8]) = [  9.16361E-03 0.00165 -1.83904E-03 0.00205  9.96892E-05 0.02373  2.68334E-02 0.00195 ];
INF_S4                    (idx, [1:   8]) = [ -9.56011E-03 0.00156 -6.09578E-04 0.00610 -8.00277E-07 1.00000 -8.01315E-03 0.00673 ];
INF_S5                    (idx, [1:   8]) = [  1.24442E-04 0.16325  1.69502E-05 0.16723 -4.23490E-05 0.03797  6.06699E-03 0.00658 ];
INF_S6                    (idx, [1:   8]) = [  5.16982E-03 0.00323 -1.42862E-04 0.01409 -5.11409E-05 0.02903 -1.56023E-02 0.00271 ];
INF_S7                    (idx, [1:   8]) = [  9.02580E-04 0.01913 -1.69356E-04 0.01297 -4.72208E-05 0.02769  2.25688E-04 0.17927 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.09492E-01 0.00010  1.78952E-02 0.00043  1.22167E-03 0.00523  1.40860E+00 0.00039 ];
INF_SP1                   (idx, [1:   8]) = [  2.36791E-01 0.00019  5.23123E-03 0.00082  5.14853E-04 0.00817  3.73789E-01 0.00047 ];
INF_SP2                   (idx, [1:   8]) = [  9.70367E-02 0.00025 -1.54563E-03 0.00209  2.76631E-04 0.01032  8.93556E-02 0.00089 ];
INF_SP3                   (idx, [1:   8]) = [  9.16338E-03 0.00166 -1.83904E-03 0.00205  9.96892E-05 0.02373  2.68334E-02 0.00195 ];
INF_SP4                   (idx, [1:   8]) = [ -9.55980E-03 0.00156 -6.09578E-04 0.00610 -8.00277E-07 1.00000 -8.01315E-03 0.00673 ];
INF_SP5                   (idx, [1:   8]) = [  1.24611E-04 0.16348  1.69502E-05 0.16723 -4.23490E-05 0.03797  6.06699E-03 0.00658 ];
INF_SP6                   (idx, [1:   8]) = [  5.17000E-03 0.00323 -1.42862E-04 0.01409 -5.11409E-05 0.02903 -1.56023E-02 0.00271 ];
INF_SP7                   (idx, [1:   8]) = [  9.02504E-04 0.01913 -1.69356E-04 0.01297 -4.72208E-05 0.02769  2.25688E-04 0.17927 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31876E-01 0.00051  1.03008E+00 0.00594 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.32927E-01 0.00071  1.09083E+00 0.00693 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33366E-01 0.00097  1.08493E+00 0.00823 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29382E-01 0.00076  9.31995E-01 0.00591 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43756E+00 0.00051  3.23870E-01 0.00587 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.43108E+00 0.00071  3.05922E-01 0.00676 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42840E+00 0.00097  3.07730E-01 0.00806 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45320E+00 0.00076  3.57957E-01 0.00595 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.03168E-03 0.00767  2.15927E-04 0.04727  1.13416E-03 0.01985  1.11473E-03 0.02010  3.18961E-03 0.01120  1.02845E-03 0.02109  3.48803E-04 0.03505 ];
LAMBDA                    (idx, [1:  14]) = [  8.04781E-01 0.01841  1.24907E-02 2.6E-06  3.16721E-02 0.00028  1.10053E-01 0.00038  3.20384E-01 0.00032  1.34591E+00 0.00023  8.85803E+00 0.00192 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0010' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:42:04 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:51:35 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292124 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99708E-01  1.00031E+00  1.00296E+00  9.98058E-01  9.98958E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 6.6E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15932E-02 0.00111  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88407E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.94539E-01 9.6E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.94985E-01 9.6E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.70389E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.41105E+01 0.00039  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.41042E+01 0.00039  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.79461E+00 0.00034  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.15303E-01 0.00113  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000798 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00016E+04 0.00074 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00016E+04 0.00074 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.56637E+01 ;
RUNNING_TIME              (idx, 1)        =  9.52110E+00 ;
INIT_TIME                 (idx, [1:  2])  = [  4.78167E-01  4.78167E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.57000E-02  7.21667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  8.97062E+00  3.22312E+00  2.52063E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.60333E-02  2.93167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.36667E-03  7.99998E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.52093E+00  1.20112E+02 ];
CPU_USAGE                 (idx, 1)        = 4.79605 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99994E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.35207E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6133.74;
MEMSIZE                   (idx, 1)        = 6039.76;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 169.50;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.98;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  7.79674E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.74823E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.39515E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.11225E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  7.88718E+02 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  6.68447E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.66933E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.18773E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  1.72742E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.03107E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  3.22712E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.84621E+06 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  1.40471E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.28504E+11 ;
TE132_ACTIVITY            (idx, 1)        =  2.51368E+14 ;
I131_ACTIVITY             (idx, 1)        =  7.42161E+13 ;
I132_ACTIVITY             (idx, 1)        =  2.43529E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.80048E+07 ;
CS137_ACTIVITY            (idx, 1)        =  1.34707E+11 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  6.48414E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.64625E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.28211E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  8.86027E+16 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.93226E+12 0.00048  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 1 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E-01  1.00009E-01 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+00 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.67921E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  1.30221E+16 0.00056  9.39419E-01 0.00016 ];
U238_FISS                 (idx, [1:   4]) = [  8.17588E+14 0.00263  5.89715E-02 0.00245 ];
PU239_FISS                (idx, [1:   4]) = [  2.05136E+13 0.01639  1.47913E-03 0.01629 ];
U235_CAPT                 (idx, [1:   4]) = [  2.85872E+15 0.00142  1.86626E-01 0.00130 ];
U238_CAPT                 (idx, [1:   4]) = [  7.39476E+15 0.00101  4.82723E-01 0.00065 ];
PU239_CAPT                (idx, [1:   4]) = [  1.18708E+13 0.02253  7.75563E-04 0.02264 ];
PU240_CAPT                (idx, [1:   4]) = [  1.23439E+11 0.21384  8.05322E-06 0.21383 ];
XE135_CAPT                (idx, [1:   4]) = [  6.94689E+14 0.00283  4.53549E-02 0.00282 ];
SM149_CAPT                (idx, [1:   4]) = [  1.97665E+13 0.01685  1.29059E-03 0.01684 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000798 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.31323E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000798 5.00731E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2608531 2.61203E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2360782 2.36379E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 31485 3.14907E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000798 5.00731E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.07336E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50055E+05 2.0E-09  4.50055E+05 2.0E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.41151E+16 1.1E-05  3.41151E+16 1.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38677E+16 1.3E-06  1.38677E+16 1.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.53266E+16 0.00047  1.11159E+16 0.00050  4.21067E+15 0.00100 ];
TOT_ABSRATE               (idx, [1:   6]) = [  2.91942E+16 0.00025  2.49836E+16 0.00022  4.21067E+15 0.00100 ];
TOT_SRCRATE               (idx, [1:   6]) = [  2.93226E+16 0.00048  2.93226E+16 0.00048  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.37600E+18 0.00046  3.80343E+17 0.00045  9.95660E+17 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.84718E+14 0.00588 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  2.93790E+16 0.00025 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.00138E+18 0.00056 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12502E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12502E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.74120E+00 0.00040 ];
SIX_FF_F                  (idx, [1:   2]) = [  8.06774E-01 0.00029 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.61604E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25935E+00 0.00032 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95912E-01 2.8E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97781E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.17037E+00 0.00050 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.16300E+00 0.00050 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46005E+00 1.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02559E+02 1.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.16287E+00 0.00051  1.15483E+00 0.00050  8.16758E-03 0.00796 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.16296E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.16358E+00 0.00048 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.16296E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.17033E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74708E+01 0.00017 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74707E+01 8.2E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.18154E-07 0.00291 ];
IMP_EALF                  (idx, [1:   2]) = [  5.17392E-07 0.00144 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.01957E-01 0.00257 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.02204E-01 0.00118 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.11113E-03 0.00566  1.75338E-04 0.03029  9.61460E-04 0.01435  9.55641E-04 0.01348  2.81550E-03 0.00808  9.04473E-04 0.01451  2.98711E-04 0.02468 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.02590E-01 0.01272  1.07420E-02 0.01806  3.16642E-02 0.00021  1.10110E-01 0.00026  3.20427E-01 0.00021  1.34614E+00 0.00017  8.60926E+00 0.00831 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.04581E-03 0.00777  2.07998E-04 0.04344  1.11515E-03 0.02047  1.11096E-03 0.02056  3.23470E-03 0.01100  1.02312E-03 0.02102  3.53890E-04 0.03438 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.13121E-01 0.01845  1.24907E-02 2.9E-06  3.16658E-02 0.00029  1.10075E-01 0.00036  3.20312E-01 0.00030  1.34597E+00 0.00024  8.88981E+00 0.00202 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.20295E-05 0.00110  2.20198E-05 0.00110  2.34737E-05 0.01042 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.56136E-05 0.00094  2.56023E-05 0.00094  2.72944E-05 0.01040 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.02030E-03 0.00797  2.10064E-04 0.04441  1.08822E-03 0.02034  1.10815E-03 0.02017  3.24107E-03 0.01122  1.03694E-03 0.02080  3.35855E-04 0.03547 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.94928E-01 0.01868  1.24907E-02 3.3E-06  3.16642E-02 0.00034  1.10111E-01 0.00041  3.20418E-01 0.00033  1.34564E+00 0.00027  8.89608E+00 0.00247 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.15237E-05 0.00244  2.15155E-05 0.00244  2.21570E-05 0.02527 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.50254E-05 0.00237  2.50159E-05 0.00236  2.57706E-05 0.02533 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.93781E-03 0.02582  2.60261E-04 0.13146  1.09289E-03 0.06025  1.05414E-03 0.06825  3.21109E-03 0.03622  1.02516E-03 0.06319  2.94279E-04 0.10678 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.37492E-01 0.05448  1.24906E-02 4.4E-06  3.16563E-02 0.00079  1.10252E-01 0.00108  3.20235E-01 0.00090  1.34613E+00 0.00059  8.97915E+00 0.00573 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.98531E-03 0.02489  2.66528E-04 0.13026  1.07815E-03 0.05915  1.04798E-03 0.06532  3.28519E-03 0.03463  1.01496E-03 0.06220  2.92497E-04 0.10337 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.31855E-01 0.05206  1.24906E-02 4.8E-06  3.16550E-02 0.00078  1.10267E-01 0.00109  3.20300E-01 0.00090  1.34617E+00 0.00059  8.97731E+00 0.00567 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -3.23181E+02 0.02582 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.17882E-05 0.00068 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.53333E-05 0.00043 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.04496E-03 0.00435 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -3.23405E+02 0.00439 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.52360E-07 0.00062 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.88711E-06 0.00042  2.88685E-06 0.00043  2.92216E-06 0.00473 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.59669E-05 0.00075  3.59860E-05 0.00075  3.34160E-05 0.00845 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.60110E-01 0.00031  6.59318E-01 0.00032  8.03045E-01 0.00909 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03936E+01 0.01310 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.41042E+01 0.00039  3.37586E+01 0.00040 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.15516E+04 0.00389  2.91237E+05 0.00192  6.03118E+05 0.00116  6.51823E+05 0.00091  6.00264E+05 0.00068  6.46737E+05 0.00072  4.38847E+05 0.00069  3.88868E+05 0.00084  2.97467E+05 0.00070  2.43235E+05 0.00070  2.09631E+05 0.00102  1.88931E+05 0.00059  1.74504E+05 0.00097  1.65967E+05 0.00083  1.61477E+05 0.00074  1.39611E+05 0.00074  1.37704E+05 0.00111  1.36262E+05 0.00096  1.34166E+05 0.00098  2.61365E+05 0.00078  2.51394E+05 0.00059  1.81086E+05 0.00092  1.17035E+05 0.00081  1.34802E+05 0.00081  1.27223E+05 0.00085  1.15524E+05 0.00128  1.89524E+05 0.00081  4.32878E+04 0.00142  5.43060E+04 0.00171  4.93685E+04 0.00148  2.85711E+04 0.00187  4.97856E+04 0.00141  3.38752E+04 0.00172  2.87224E+04 0.00165  5.44180E+03 0.00352  5.41423E+03 0.00414  5.55760E+03 0.00378  5.78257E+03 0.00461  5.68503E+03 0.00343  5.60136E+03 0.00399  5.84118E+03 0.00347  5.48669E+03 0.00393  1.03059E+04 0.00243  1.64374E+04 0.00229  2.09362E+04 0.00210  5.51596E+04 0.00123  5.81219E+04 0.00161  6.28979E+04 0.00131  4.25563E+04 0.00136  3.15229E+04 0.00127  2.41554E+04 0.00208  2.84018E+04 0.00210  5.35161E+04 0.00157  7.14858E+04 0.00137  1.35494E+05 0.00131  2.05285E+05 0.00126  3.00592E+05 0.00121  1.89902E+05 0.00140  1.35104E+05 0.00138  9.67261E+04 0.00136  8.65426E+04 0.00131  8.50005E+04 0.00149  7.05835E+04 0.00153  4.77070E+04 0.00142  4.39735E+04 0.00147  3.89098E+04 0.00159  3.28671E+04 0.00139  2.58222E+04 0.00173  1.72495E+04 0.00165  6.09025E+03 0.00227 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.17095E+00 0.00066 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.09885E+18 0.00065  2.77172E+17 0.00121 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.36482E-01 0.00012  1.48603E+00 0.00034 ];
INF_CAPT                  (idx, [1:   4]) = [  6.51085E-03 0.00065  2.94862E-02 0.00049 ];
INF_ABS                   (idx, [1:   4]) = [  9.01623E-03 0.00056  6.95930E-02 0.00093 ];
INF_FISS                  (idx, [1:   4]) = [  2.50538E-03 0.00053  4.01068E-02 0.00125 ];
INF_NSF                   (idx, [1:   4]) = [  6.39198E-03 0.00053  9.77579E-02 0.00125 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.55131E+00 6.0E-05  2.43744E+00 2.7E-07 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03686E+02 5.3E-06  2.02280E+02 4.3E-08 ];
INF_INVV                  (idx, [1:   4]) = [  5.83724E-08 0.00035  2.51086E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.27469E-01 0.00013  1.41648E+00 0.00038 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42174E-01 0.00016  3.76688E-01 0.00049 ];
INF_SCATT2                (idx, [1:   4]) = [  9.55266E-02 0.00027  9.02069E-02 0.00083 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32958E-03 0.00247  2.71668E-02 0.00284 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.01945E-02 0.00137 -7.91935E-03 0.00774 ];
INF_SCATT5                (idx, [1:   4]) = [  1.63594E-04 0.08791  5.99790E-03 0.00921 ];
INF_SCATT6                (idx, [1:   4]) = [  5.03314E-03 0.00292 -1.57718E-02 0.00287 ];
INF_SCATT7                (idx, [1:   4]) = [  7.20535E-04 0.02187  2.08450E-04 0.20290 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.27508E-01 0.00013  1.41648E+00 0.00038 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42174E-01 0.00016  3.76688E-01 0.00049 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.55266E-02 0.00027  9.02069E-02 0.00083 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32976E-03 0.00248  2.71668E-02 0.00284 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.01945E-02 0.00137 -7.91935E-03 0.00774 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.63595E-04 0.08803  5.99790E-03 0.00921 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.03292E-03 0.00292 -1.57718E-02 0.00287 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.20510E-04 0.02184  2.08450E-04 0.20290 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.14355E-01 0.00033  9.56552E-01 0.00029 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55505E+00 0.00033  3.48475E-01 0.00029 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.97720E-03 0.00057  6.95930E-02 0.00093 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69265E-02 0.00021  7.07888E-02 0.00090 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.09555E-01 0.00013  1.79143E-02 0.00046  1.23591E-03 0.00343  1.41524E+00 0.00038 ];
INF_S1                    (idx, [1:   8]) = [  2.36936E-01 0.00015  5.23769E-03 0.00083  5.22015E-04 0.00597  3.76166E-01 0.00049 ];
INF_S2                    (idx, [1:   8]) = [  9.70732E-02 0.00027 -1.54655E-03 0.00247  2.83847E-04 0.00929  8.99231E-02 0.00083 ];
INF_S3                    (idx, [1:   8]) = [  9.17059E-03 0.00185 -1.84101E-03 0.00173  1.02452E-04 0.01725  2.70644E-02 0.00286 ];
INF_S4                    (idx, [1:   8]) = [ -9.57808E-03 0.00145 -6.16386E-04 0.00514 -8.09612E-07 1.00000 -7.91854E-03 0.00774 ];
INF_S5                    (idx, [1:   8]) = [  1.49313E-04 0.09263  1.42812E-05 0.23801 -4.05875E-05 0.04937  6.03849E-03 0.00915 ];
INF_S6                    (idx, [1:   8]) = [  5.17514E-03 0.00257 -1.42000E-04 0.02133 -5.16474E-05 0.02693 -1.57201E-02 0.00285 ];
INF_S7                    (idx, [1:   8]) = [  8.89492E-04 0.01670 -1.68956E-04 0.01379 -4.79136E-05 0.02364  2.56364E-04 0.16546 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.09594E-01 0.00013  1.79143E-02 0.00046  1.23591E-03 0.00343  1.41524E+00 0.00038 ];
INF_SP1                   (idx, [1:   8]) = [  2.36937E-01 0.00015  5.23769E-03 0.00083  5.22015E-04 0.00597  3.76166E-01 0.00049 ];
INF_SP2                   (idx, [1:   8]) = [  9.70732E-02 0.00027 -1.54655E-03 0.00247  2.83847E-04 0.00929  8.99231E-02 0.00083 ];
INF_SP3                   (idx, [1:   8]) = [  9.17077E-03 0.00186 -1.84101E-03 0.00173  1.02452E-04 0.01725  2.70644E-02 0.00286 ];
INF_SP4                   (idx, [1:   8]) = [ -9.57816E-03 0.00145 -6.16386E-04 0.00514 -8.09612E-07 1.00000 -7.91854E-03 0.00774 ];
INF_SP5                   (idx, [1:   8]) = [  1.49314E-04 0.09280  1.42812E-05 0.23801 -4.05875E-05 0.04937  6.03849E-03 0.00915 ];
INF_SP6                   (idx, [1:   8]) = [  5.17492E-03 0.00257 -1.42000E-04 0.02133 -5.16474E-05 0.02693 -1.57201E-02 0.00285 ];
INF_SP7                   (idx, [1:   8]) = [  8.89466E-04 0.01667 -1.68956E-04 0.01379 -4.79136E-05 0.02364  2.56364E-04 0.16546 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31856E-01 0.00079  1.03334E+00 0.00678 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33200E-01 0.00102  1.08715E+00 0.00815 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33212E-01 0.00110  1.08840E+00 0.00802 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29209E-01 0.00097  9.40574E-01 0.00816 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43770E+00 0.00079  3.22925E-01 0.00661 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42943E+00 0.00102  3.07099E-01 0.00811 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42936E+00 0.00110  3.06718E-01 0.00778 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45431E+00 0.00097  3.54956E-01 0.00810 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.04581E-03 0.00777  2.07998E-04 0.04344  1.11515E-03 0.02047  1.11096E-03 0.02056  3.23470E-03 0.01100  1.02312E-03 0.02102  3.53890E-04 0.03438 ];
LAMBDA                    (idx, [1:  14]) = [  8.13121E-01 0.01845  1.24907E-02 2.9E-06  3.16658E-02 0.00029  1.10075E-01 0.00036  3.20312E-01 0.00030  1.34597E+00 0.00024  8.88981E+00 0.00202 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0010' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:42:04 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 01:57:24 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292124 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.98165E-01  1.00074E+00  1.00294E+00  9.97253E-01  1.00090E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15992E-02 0.00104  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88401E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.95335E-01 9.9E-05  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.95781E-01 9.8E-05  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.70025E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.40417E+01 0.00042  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.40354E+01 0.00042  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.73399E+00 0.00036  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.14000E-01 0.00105  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001001 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00020E+04 0.00070 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00020E+04 0.00070 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  7.46606E+01 ;
RUNNING_TIME              (idx, 1)        =  1.53266E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.78167E-01  4.78167E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.29833E-02  8.31667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.47021E+01  3.21390E+00  2.51760E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.12167E-01  2.72167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  5.16667E-03  8.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.53265E+01  1.19939E+02 ];
CPU_USAGE                 (idx, 1)        = 4.87131 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99980E+00 0.00022 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.53890E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6133.74;
MEMSIZE                   (idx, 1)        = 6039.76;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 169.50;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.98;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.60867E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.82503E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.40683E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.47167E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.02842E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.13699E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.72217E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.60590E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  2.96014E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  8.13869E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.00619E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  1.79203E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  2.35952E+07 ;
SR90_ACTIVITY             (idx, 1)        =  6.39241E+11 ;
TE132_ACTIVITY            (idx, 1)        =  5.62855E+14 ;
I131_ACTIVITY             (idx, 1)        =  2.65173E+14 ;
I132_ACTIVITY             (idx, 1)        =  5.65626E+14 ;
CS134_ACTIVITY            (idx, 1)        =  7.06257E+09 ;
CS137_ACTIVITY            (idx, 1)        =  6.74012E+11 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.51997E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.63615E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  6.28833E+09 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.08674E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.97001E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 2 ;
BURNUP                     (idx, [1:  2])  = [  5.00000E-01  5.00044E-01 ];
BURN_DAYS                 (idx, 1)        =  1.25000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.69051E-01 0.00111 ];
U235_FISS                 (idx, [1:   4]) = [  1.27947E+16 0.00059  9.22047E-01 0.00019 ];
U238_FISS                 (idx, [1:   4]) = [  8.26110E+14 0.00273  5.95272E-02 0.00260 ];
PU239_FISS                (idx, [1:   4]) = [  2.53177E+14 0.00456  1.82435E-02 0.00449 ];
PU240_FISS                (idx, [1:   4]) = [  1.77543E+10 0.57624  1.28123E-06 0.57623 ];
PU241_FISS                (idx, [1:   4]) = [  1.42050E+11 0.20787  1.02491E-05 0.20801 ];
U235_CAPT                 (idx, [1:   4]) = [  2.81655E+15 0.00144  1.79647E-01 0.00134 ];
U238_CAPT                 (idx, [1:   4]) = [  7.45096E+15 0.00101  4.75204E-01 0.00065 ];
PU239_CAPT                (idx, [1:   4]) = [  1.42734E+14 0.00622  9.10360E-03 0.00619 ];
PU240_CAPT                (idx, [1:   4]) = [  6.86836E+12 0.02803  4.37843E-04 0.02798 ];
PU241_CAPT                (idx, [1:   4]) = [  2.95415E+10 0.44546  1.88656E-06 0.44547 ];
XE135_CAPT                (idx, [1:   4]) = [  6.99975E+14 0.00275  4.46488E-02 0.00275 ];
SM149_CAPT                (idx, [1:   4]) = [  1.18728E+14 0.00740  7.57246E-03 0.00736 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001001 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.38651E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001001 5.00739E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2636119 2.63952E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2333196 2.33618E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 31686 3.16861E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001001 5.00739E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.21541E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50055E+05 2.0E-09  4.50055E+05 2.0E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.42033E+16 1.1E-05  3.42033E+16 1.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38610E+16 1.3E-06  1.38610E+16 1.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.56815E+16 0.00044  1.14322E+16 0.00045  4.24930E+15 0.00102 ];
TOT_ABSRATE               (idx, [1:   6]) = [  2.95425E+16 0.00023  2.52932E+16 0.00020  4.24930E+15 0.00102 ];
TOT_SRCRATE               (idx, [1:   6]) = [  2.97001E+16 0.00049  2.97001E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.39127E+18 0.00044  3.83979E+17 0.00043  1.00729E+18 0.00049 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.88248E+14 0.00603 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  2.97307E+16 0.00024 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.01221E+18 0.00055 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12455E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12455E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.72369E+00 0.00038 ];
SIX_FF_F                  (idx, [1:   2]) = [  8.07482E-01 0.00029 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.61296E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.26063E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95885E-01 2.9E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97768E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.16025E+00 0.00048 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.15290E+00 0.00049 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.46759E+00 1.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02657E+02 1.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.15295E+00 0.00050  1.14471E+00 0.00050  8.18580E-03 0.00759 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.15218E+00 0.00024 ];
COL_KEFF                  (idx, [1:   2]) = [  1.15176E+00 0.00048 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.15218E+00 0.00024 ];
ABS_KINF                  (idx, [1:   2]) = [  1.15953E+00 0.00023 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74495E+01 0.00018 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74471E+01 7.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.29541E-07 0.00321 ];
IMP_EALF                  (idx, [1:   2]) = [  5.29732E-07 0.00136 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.04180E-01 0.00271 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.04958E-01 0.00112 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.16711E-03 0.00563  1.65292E-04 0.03023  9.85988E-04 0.01365  9.76596E-04 0.01318  2.83494E-03 0.00829  9.10316E-04 0.01392  2.93986E-04 0.02618 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.92266E-01 0.01334  1.08669E-02 0.01730  3.16357E-02 0.00024  1.10123E-01 0.00027  3.20576E-01 0.00022  1.34588E+00 0.00014  8.49104E+00 0.00974 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  7.07659E-03 0.00762  1.96093E-04 0.04355  1.17252E-03 0.01958  1.07787E-03 0.02003  3.21831E-03 0.01166  1.07309E-03 0.01979  3.38702E-04 0.03715 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.96301E-01 0.01890  1.24907E-02 4.1E-06  3.16294E-02 0.00032  1.10154E-01 0.00038  3.20482E-01 0.00031  1.34606E+00 0.00023  8.87982E+00 0.00197 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.19367E-05 0.00110  2.19255E-05 0.00111  2.35759E-05 0.01122 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.52885E-05 0.00096  2.52755E-05 0.00096  2.71818E-05 0.01125 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  7.09496E-03 0.00774  2.04182E-04 0.04449  1.13547E-03 0.01920  1.07734E-03 0.02111  3.27722E-03 0.01201  1.05294E-03 0.02055  3.47801E-04 0.03502 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.06517E-01 0.01920  1.24907E-02 3.7E-06  3.16237E-02 0.00039  1.10166E-01 0.00045  3.20584E-01 0.00033  1.34618E+00 0.00024  8.88372E+00 0.00236 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.13453E-05 0.00246  2.13332E-05 0.00246  2.32218E-05 0.02765 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.46077E-05 0.00244  2.45939E-05 0.00244  2.67603E-05 0.02759 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  7.15100E-03 0.02474  1.62677E-04 0.13318  1.19853E-03 0.05694  1.09878E-03 0.06216  3.41044E-03 0.03600  9.48893E-04 0.06463  3.31675E-04 0.10679 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.77976E-01 0.05576  1.24908E-02 7.8E-06  3.16688E-02 0.00074  1.09864E-01 0.00080  3.20841E-01 0.00097  1.34641E+00 0.00058  8.86378E+00 0.00482 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  7.21410E-03 0.02384  1.82501E-04 0.13344  1.21104E-03 0.05587  1.11220E-03 0.06081  3.39686E-03 0.03478  9.70534E-04 0.06188  3.40972E-04 0.10000 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.81258E-01 0.05238  1.24908E-02 7.8E-06  3.16691E-02 0.00073  1.09871E-01 0.00080  3.20798E-01 0.00093  1.34636E+00 0.00058  8.86411E+00 0.00482 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -3.36195E+02 0.02492 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.16732E-05 0.00062 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.49853E-05 0.00041 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  7.21190E-03 0.00472 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -3.32817E+02 0.00475 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.49733E-07 0.00066 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.88336E-06 0.00042  2.88312E-06 0.00042  2.92000E-06 0.00495 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.57449E-05 0.00080  3.57640E-05 0.00080  3.30862E-05 0.00865 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.59794E-01 0.00033  6.59019E-01 0.00033  7.97138E-01 0.00883 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.02430E+01 0.01260 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.40354E+01 0.00042  3.36029E+01 0.00040 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.20508E+04 0.00283  2.91276E+05 0.00136  6.02988E+05 0.00077  6.51322E+05 0.00081  6.00365E+05 0.00063  6.46554E+05 0.00064  4.39227E+05 0.00064  3.88973E+05 0.00062  2.97776E+05 0.00093  2.43260E+05 0.00058  2.09489E+05 0.00081  1.89096E+05 0.00104  1.74490E+05 0.00095  1.65793E+05 0.00092  1.61574E+05 0.00090  1.39509E+05 0.00097  1.37388E+05 0.00081  1.36322E+05 0.00118  1.33602E+05 0.00110  2.61227E+05 0.00086  2.51146E+05 0.00072  1.81282E+05 0.00086  1.17062E+05 0.00127  1.34690E+05 0.00070  1.27001E+05 0.00076  1.15399E+05 0.00078  1.89112E+05 0.00062  4.31373E+04 0.00164  5.42926E+04 0.00158  4.95576E+04 0.00181  2.86776E+04 0.00164  4.97334E+04 0.00179  3.38679E+04 0.00238  2.86698E+04 0.00220  5.46935E+03 0.00382  5.42748E+03 0.00347  5.55764E+03 0.00356  5.73000E+03 0.00344  5.68632E+03 0.00432  5.58391E+03 0.00442  5.76828E+03 0.00391  5.43852E+03 0.00370  1.02639E+04 0.00301  1.63981E+04 0.00240  2.08830E+04 0.00222  5.51384E+04 0.00131  5.79734E+04 0.00094  6.26175E+04 0.00145  4.22286E+04 0.00149  3.11851E+04 0.00191  2.38732E+04 0.00177  2.79533E+04 0.00135  5.26485E+04 0.00125  7.05867E+04 0.00110  1.34114E+05 0.00112  2.03301E+05 0.00118  2.98247E+05 0.00127  1.88554E+05 0.00119  1.34105E+05 0.00122  9.59592E+04 0.00150  8.60241E+04 0.00141  8.44569E+04 0.00156  7.02653E+04 0.00135  4.73958E+04 0.00143  4.37085E+04 0.00140  3.88122E+04 0.00149  3.27579E+04 0.00139  2.56778E+04 0.00184  1.71880E+04 0.00188  6.08796E+03 0.00224 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.15910E+00 0.00048 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.11275E+18 0.00048  2.78545E+17 0.00097 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.36527E-01 9.5E-05  1.49066E+00 0.00031 ];
INF_CAPT                  (idx, [1:   4]) = [  6.52822E-03 0.00052  3.02212E-02 0.00037 ];
INF_ABS                   (idx, [1:   4]) = [  9.01351E-03 0.00039  7.00615E-02 0.00071 ];
INF_FISS                  (idx, [1:   4]) = [  2.48529E-03 0.00040  3.98403E-02 0.00098 ];
INF_NSF                   (idx, [1:   4]) = [  6.34929E-03 0.00040  9.74442E-02 0.00098 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.55475E+00 4.1E-05  2.44587E+00 4.1E-06 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03727E+02 4.1E-06  2.02390E+02 6.5E-07 ];
INF_INVV                  (idx, [1:   4]) = [  5.83295E-08 0.00044  2.51292E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.27504E-01 0.00010  1.42059E+00 0.00036 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42177E-01 0.00019  3.78093E-01 0.00042 ];
INF_SCATT2                (idx, [1:   4]) = [  9.55270E-02 0.00036  9.07051E-02 0.00095 ];
INF_SCATT3                (idx, [1:   4]) = [  7.33534E-03 0.00298  2.72369E-02 0.00236 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02181E-02 0.00227 -7.99467E-03 0.00785 ];
INF_SCATT5                (idx, [1:   4]) = [  1.53997E-04 0.13847  6.07353E-03 0.00721 ];
INF_SCATT6                (idx, [1:   4]) = [  5.03119E-03 0.00384 -1.58018E-02 0.00375 ];
INF_SCATT7                (idx, [1:   4]) = [  7.32481E-04 0.01953  3.08694E-04 0.15583 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.27544E-01 0.00010  1.42059E+00 0.00036 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42177E-01 0.00019  3.78093E-01 0.00042 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.55272E-02 0.00036  9.07051E-02 0.00095 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.33548E-03 0.00298  2.72369E-02 0.00236 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02178E-02 0.00226 -7.99467E-03 0.00785 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.53998E-04 0.13859  6.07353E-03 0.00721 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.03117E-03 0.00383 -1.58018E-02 0.00375 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.32396E-04 0.01950  3.08694E-04 0.15583 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.14325E-01 0.00031  9.59501E-01 0.00028 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55528E+00 0.00031  3.47403E-01 0.00028 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.97408E-03 0.00039  7.00615E-02 0.00071 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69344E-02 0.00021  7.13202E-02 0.00083 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.09592E-01 9.7E-05  1.79120E-02 0.00039  1.25470E-03 0.00364  1.41934E+00 0.00036 ];
INF_S1                    (idx, [1:   8]) = [  2.36945E-01 0.00018  5.23126E-03 0.00105  5.28556E-04 0.00705  3.77564E-01 0.00042 ];
INF_S2                    (idx, [1:   8]) = [  9.70729E-02 0.00034 -1.54587E-03 0.00267  2.89942E-04 0.00958  9.04151E-02 0.00096 ];
INF_S3                    (idx, [1:   8]) = [  9.17997E-03 0.00240 -1.84463E-03 0.00170  1.04989E-04 0.01770  2.71319E-02 0.00236 ];
INF_S4                    (idx, [1:   8]) = [ -9.59693E-03 0.00231 -6.21123E-04 0.00432 -4.73556E-07 1.00000 -7.99420E-03 0.00784 ];
INF_S5                    (idx, [1:   8]) = [  1.38626E-04 0.14702  1.53710E-05 0.16818 -4.05652E-05 0.04678  6.11410E-03 0.00717 ];
INF_S6                    (idx, [1:   8]) = [  5.17178E-03 0.00370 -1.40589E-04 0.01185 -5.11443E-05 0.03542 -1.57506E-02 0.00375 ];
INF_S7                    (idx, [1:   8]) = [  8.98416E-04 0.01654 -1.65935E-04 0.01100 -4.79547E-05 0.02135  3.56649E-04 0.13551 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.09632E-01 9.6E-05  1.79120E-02 0.00039  1.25470E-03 0.00364  1.41934E+00 0.00036 ];
INF_SP1                   (idx, [1:   8]) = [  2.36946E-01 0.00018  5.23126E-03 0.00105  5.28556E-04 0.00705  3.77564E-01 0.00042 ];
INF_SP2                   (idx, [1:   8]) = [  9.70731E-02 0.00034 -1.54587E-03 0.00267  2.89942E-04 0.00958  9.04151E-02 0.00096 ];
INF_SP3                   (idx, [1:   8]) = [  9.18011E-03 0.00240 -1.84463E-03 0.00170  1.04989E-04 0.01770  2.71319E-02 0.00236 ];
INF_SP4                   (idx, [1:   8]) = [ -9.59671E-03 0.00231 -6.21123E-04 0.00432 -4.73556E-07 1.00000 -7.99420E-03 0.00784 ];
INF_SP5                   (idx, [1:   8]) = [  1.38627E-04 0.14714  1.53710E-05 0.16818 -4.05652E-05 0.04678  6.11410E-03 0.00717 ];
INF_SP6                   (idx, [1:   8]) = [  5.17176E-03 0.00370 -1.40589E-04 0.01185 -5.11443E-05 0.03542 -1.57506E-02 0.00375 ];
INF_SP7                   (idx, [1:   8]) = [  8.98330E-04 0.01651 -1.65935E-04 0.01100 -4.79547E-05 0.02135  3.56649E-04 0.13551 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32071E-01 0.00052  1.04820E+00 0.00584 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33647E-01 0.00087  1.11637E+00 0.00678 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33095E-01 0.00087  1.10183E+00 0.00731 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29521E-01 0.00091  9.45051E-01 0.00556 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43635E+00 0.00052  3.18272E-01 0.00594 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42668E+00 0.00087  2.98921E-01 0.00685 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.43006E+00 0.00087  3.02912E-01 0.00724 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45233E+00 0.00091  3.52982E-01 0.00568 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  7.07659E-03 0.00762  1.96093E-04 0.04355  1.17252E-03 0.01958  1.07787E-03 0.02003  3.21831E-03 0.01166  1.07309E-03 0.01979  3.38702E-04 0.03715 ];
LAMBDA                    (idx, [1:  14]) = [  7.96301E-01 0.01890  1.24907E-02 4.1E-06  3.16294E-02 0.00032  1.10154E-01 0.00038  3.20482E-01 0.00031  1.34606E+00 0.00023  8.87982E+00 0.00197 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0010' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:42:04 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:03:11 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292124 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99299E-01  1.00043E+00  1.00235E+00  9.98863E-01  9.99055E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.1E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16265E-02 0.00117  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88373E-01 1.4E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.96010E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.96457E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69739E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.39546E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.39483E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.67535E+00 0.00036  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.13171E-01 0.00123  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000649 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00013E+04 0.00066 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00013E+04 0.00066 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.03614E+02 ;
RUNNING_TIME              (idx, 1)        =  2.11231E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.78167E-01  4.78167E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  5.02167E-02  7.56667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  2.04260E+01  3.21240E+00  2.51145E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.67117E-01  2.68000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  7.75000E-03  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  2.11230E+01  1.19622E+02 ];
CPU_USAGE                 (idx, 1)        = 4.90526 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00064E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.62342E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6133.74;
MEMSIZE                   (idx, 1)        = 6039.76;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 169.50;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.98;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.81660E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.84236E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.45278E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.50131E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.04828E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.31527E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.73751E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  3.46726E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.43452E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  9.38245E+06 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.20855E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  2.52901E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  2.81367E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.26849E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.03430E+14 ;
I131_ACTIVITY             (idx, 1)        =  3.59438E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.08359E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.83120E+10 ;
CS137_ACTIVITY            (idx, 1)        =  1.34870E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.71723E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.61944E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.38974E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.12534E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  2.99466E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 3 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E+00  1.00009E+00 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.70443E-01 0.00107 ];
U235_FISS                 (idx, [1:   4]) = [  1.24416E+16 0.00057  8.98081E-01 0.00022 ];
U238_FISS                 (idx, [1:   4]) = [  8.40577E+14 0.00278  6.06666E-02 0.00262 ];
PU239_FISS                (idx, [1:   4]) = [  5.67602E+14 0.00314  4.09715E-02 0.00310 ];
PU240_FISS                (idx, [1:   4]) = [  6.07681E+10 0.31342  4.36820E-06 0.31341 ];
PU241_FISS                (idx, [1:   4]) = [  1.09444E+12 0.07085  7.89538E-05 0.07087 ];
U235_CAPT                 (idx, [1:   4]) = [  2.73982E+15 0.00143  1.71797E-01 0.00133 ];
U238_CAPT                 (idx, [1:   4]) = [  7.47822E+15 0.00100  4.68883E-01 0.00069 ];
PU239_CAPT                (idx, [1:   4]) = [  3.20509E+14 0.00424  2.00960E-02 0.00418 ];
PU240_CAPT                (idx, [1:   4]) = [  3.05179E+13 0.01414  1.91321E-03 0.01410 ];
PU241_CAPT                (idx, [1:   4]) = [  3.56995E+11 0.13008  2.24510E-05 0.13018 ];
XE135_CAPT                (idx, [1:   4]) = [  7.05989E+14 0.00291  4.42680E-02 0.00287 ];
SM149_CAPT                (idx, [1:   4]) = [  1.47886E+14 0.00616  9.27334E-03 0.00614 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000649 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.52019E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000649 5.00752E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2659112 2.66284E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2309996 2.31314E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 31541 3.15465E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000649 5.00752E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.84288E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50055E+05 2.0E-09  4.50055E+05 2.0E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.43206E+16 1.2E-05  3.43206E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38521E+16 1.5E-06  1.38521E+16 1.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.59542E+16 0.00046  1.16922E+16 0.00045  4.26202E+15 0.00113 ];
TOT_ABSRATE               (idx, [1:   6]) = [  2.98063E+16 0.00024  2.55443E+16 0.00021  4.26202E+15 0.00113 ];
TOT_SRCRATE               (idx, [1:   6]) = [  2.99466E+16 0.00049  2.99466E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.40076E+18 0.00047  3.86356E+17 0.00046  1.01441E+18 0.00051 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.88970E+14 0.00632 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  2.99953E+16 0.00025 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.01803E+18 0.00059 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12397E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12397E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.71282E+00 0.00039 ];
SIX_FF_F                  (idx, [1:   2]) = [  8.08295E-01 0.00029 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.60855E-01 0.00031 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.26089E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95924E-01 3.0E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97757E-01 2.2E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.15356E+00 0.00047 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.14628E+00 0.00047 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.47766E+00 1.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.02788E+02 1.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.14631E+00 0.00047  1.13847E+00 0.00048  7.80961E-03 0.00821 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.14594E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.14620E+00 0.00048 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.14594E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.15321E+00 0.00024 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.74193E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.74232E+01 8.8E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.45883E-07 0.00327 ];
IMP_EALF                  (idx, [1:   2]) = [  5.42626E-07 0.00153 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.08026E-01 0.00290 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.07286E-01 0.00125 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.00421E-03 0.00529  1.72683E-04 0.03245  9.57894E-04 0.01356  9.27508E-04 0.01290  2.74932E-03 0.00780  8.95547E-04 0.01366  3.01259E-04 0.02403 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.16559E-01 0.01268  1.07669E-02 0.01791  3.16140E-02 0.00025  1.10121E-01 0.00029  3.20632E-01 0.00021  1.34545E+00 0.00017  8.55040E+00 0.00954 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.79854E-03 0.00797  1.91485E-04 0.05022  1.08143E-03 0.01976  1.06502E-03 0.01916  3.11455E-03 0.01182  9.96870E-04 0.02037  3.49178E-04 0.03678 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.24403E-01 0.01924  1.24906E-02 4.5E-06  3.16107E-02 0.00034  1.10180E-01 0.00042  3.20570E-01 0.00029  1.34562E+00 0.00023  8.91868E+00 0.00215 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.17715E-05 0.00113  2.17635E-05 0.00114  2.29545E-05 0.01090 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.49537E-05 0.00100  2.49445E-05 0.00101  2.63085E-05 0.01085 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.80487E-03 0.00833  2.02014E-04 0.04910  1.07911E-03 0.01999  1.04305E-03 0.02033  3.12026E-03 0.01233  1.00919E-03 0.02036  3.51240E-04 0.03791 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.28840E-01 0.02044  1.24906E-02 6.2E-06  3.16205E-02 0.00039  1.10142E-01 0.00046  3.20645E-01 0.00033  1.34541E+00 0.00027  8.91272E+00 0.00248 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.10858E-05 0.00244  2.10799E-05 0.00243  2.16673E-05 0.02565 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.41671E-05 0.00235  2.41602E-05 0.00234  2.48480E-05 0.02569 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.78361E-03 0.02525  2.21945E-04 0.15233  1.05313E-03 0.06698  1.02031E-03 0.06129  3.04521E-03 0.03544  1.05671E-03 0.06384  3.86301E-04 0.10817 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.98977E-01 0.05878  1.24903E-02 1.8E-05  3.16215E-02 0.00093  1.10259E-01 0.00112  3.20634E-01 0.00097  1.34557E+00 0.00060  8.90182E+00 0.00516 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.80439E-03 0.02443  2.25487E-04 0.15143  1.05311E-03 0.06260  1.01716E-03 0.05946  3.04558E-03 0.03476  1.07897E-03 0.06085  3.84075E-04 0.10718 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.95729E-01 0.05745  1.24903E-02 1.8E-05  3.16218E-02 0.00092  1.10262E-01 0.00112  3.20641E-01 0.00096  1.34546E+00 0.00059  8.90649E+00 0.00518 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -3.22243E+02 0.02525 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.15196E-05 0.00070 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.46651E-05 0.00047 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.84602E-03 0.00500 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -3.18148E+02 0.00498 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.46306E-07 0.00071 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.87937E-06 0.00044  2.87898E-06 0.00044  2.93486E-06 0.00491 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.54681E-05 0.00085  3.54860E-05 0.00085  3.29212E-05 0.00862 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.59401E-01 0.00031  6.58726E-01 0.00031  7.81675E-01 0.00848 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03982E+01 0.01398 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.39483E+01 0.00044  3.34279E+01 0.00042 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.25338E+04 0.00372  2.92384E+05 0.00159  6.05029E+05 0.00072  6.51521E+05 0.00097  6.00735E+05 0.00061  6.47069E+05 0.00068  4.38561E+05 0.00080  3.88900E+05 0.00060  2.97490E+05 0.00062  2.43127E+05 0.00063  2.09432E+05 0.00104  1.89246E+05 0.00081  1.74175E+05 0.00077  1.65535E+05 0.00079  1.61254E+05 0.00078  1.39476E+05 0.00088  1.37781E+05 0.00086  1.36119E+05 0.00095  1.33829E+05 0.00082  2.61041E+05 0.00090  2.51440E+05 0.00053  1.81277E+05 0.00080  1.17092E+05 0.00113  1.34722E+05 0.00106  1.27137E+05 0.00092  1.15163E+05 0.00098  1.89236E+05 0.00071  4.33941E+04 0.00124  5.42592E+04 0.00162  4.92764E+04 0.00183  2.87030E+04 0.00224  4.97955E+04 0.00168  3.37590E+04 0.00155  2.86697E+04 0.00183  5.46317E+03 0.00289  5.42052E+03 0.00341  5.48323E+03 0.00276  5.66356E+03 0.00355  5.61890E+03 0.00340  5.56250E+03 0.00308  5.80484E+03 0.00300  5.41600E+03 0.00494  1.02054E+04 0.00343  1.63216E+04 0.00237  2.09436E+04 0.00248  5.50811E+04 0.00135  5.78723E+04 0.00151  6.25182E+04 0.00145  4.18117E+04 0.00158  3.06722E+04 0.00163  2.33536E+04 0.00162  2.73639E+04 0.00161  5.17339E+04 0.00141  6.97768E+04 0.00139  1.32578E+05 0.00134  2.01224E+05 0.00132  2.95678E+05 0.00163  1.86826E+05 0.00127  1.33142E+05 0.00154  9.52950E+04 0.00156  8.53378E+04 0.00166  8.38658E+04 0.00165  6.98402E+04 0.00142  4.71091E+04 0.00172  4.34470E+04 0.00193  3.85538E+04 0.00200  3.25135E+04 0.00173  2.55372E+04 0.00203  1.70491E+04 0.00229  6.01299E+03 0.00300 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.15347E+00 0.00046 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.12242E+18 0.00047  2.78371E+17 0.00137 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.36417E-01 9.2E-05  1.49426E+00 0.00044 ];
INF_CAPT                  (idx, [1:   4]) = [  6.56505E-03 0.00061  3.08450E-02 0.00054 ];
INF_ABS                   (idx, [1:   4]) = [  9.03359E-03 0.00055  7.06602E-02 0.00100 ];
INF_FISS                  (idx, [1:   4]) = [  2.46853E-03 0.00063  3.98152E-02 0.00136 ];
INF_NSF                   (idx, [1:   4]) = [  6.31726E-03 0.00062  9.78373E-02 0.00137 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.55911E+00 5.1E-05  2.45729E+00 1.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03782E+02 5.7E-06  2.02539E+02 1.7E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.82652E-08 0.00039  2.51425E-06 0.00021 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.27385E-01 9.5E-05  1.42362E+00 0.00051 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42171E-01 0.00017  3.78824E-01 0.00059 ];
INF_SCATT2                (idx, [1:   4]) = [  9.55371E-02 0.00026  9.07252E-02 0.00076 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34174E-03 0.00328  2.72807E-02 0.00220 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.01878E-02 0.00208 -8.00396E-03 0.00757 ];
INF_SCATT5                (idx, [1:   4]) = [  1.89937E-04 0.10123  6.18907E-03 0.00977 ];
INF_SCATT6                (idx, [1:   4]) = [  5.06289E-03 0.00198 -1.58803E-02 0.00295 ];
INF_SCATT7                (idx, [1:   4]) = [  7.38902E-04 0.01721  2.30257E-04 0.17376 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.27426E-01 9.5E-05  1.42362E+00 0.00051 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42172E-01 0.00017  3.78824E-01 0.00059 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.55374E-02 0.00026  9.07252E-02 0.00076 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34195E-03 0.00327  2.72807E-02 0.00220 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.01874E-02 0.00208 -8.00396E-03 0.00757 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.89898E-04 0.10126  6.18907E-03 0.00977 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.06300E-03 0.00199 -1.58803E-02 0.00295 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.38756E-04 0.01720  2.30257E-04 0.17376 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.14090E-01 0.00027  9.62584E-01 0.00038 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55698E+00 0.00027  3.46291E-01 0.00038 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  8.99346E-03 0.00055  7.06602E-02 0.00100 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69253E-02 0.00020  7.19114E-02 0.00108 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.09492E-01 9.4E-05  1.78937E-02 0.00036  1.26417E-03 0.00531  1.42235E+00 0.00051 ];
INF_S1                    (idx, [1:   8]) = [  2.36944E-01 0.00017  5.22732E-03 0.00114  5.35192E-04 0.00634  3.78289E-01 0.00059 ];
INF_S2                    (idx, [1:   8]) = [  9.70845E-02 0.00025 -1.54733E-03 0.00268  2.94451E-04 0.00797  9.04307E-02 0.00077 ];
INF_S3                    (idx, [1:   8]) = [  9.18103E-03 0.00251 -1.83929E-03 0.00203  1.05386E-04 0.01682  2.71753E-02 0.00219 ];
INF_S4                    (idx, [1:   8]) = [ -9.57729E-03 0.00232 -6.10494E-04 0.00441  1.81943E-06 0.94972 -8.00577E-03 0.00755 ];
INF_S5                    (idx, [1:   8]) = [  1.73361E-04 0.11076  1.65765E-05 0.14946 -4.16397E-05 0.03377  6.23071E-03 0.00966 ];
INF_S6                    (idx, [1:   8]) = [  5.20510E-03 0.00195 -1.42211E-04 0.01037 -5.25320E-05 0.03136 -1.58278E-02 0.00300 ];
INF_S7                    (idx, [1:   8]) = [  9.07567E-04 0.01352 -1.68665E-04 0.01115 -4.92725E-05 0.02385  2.79529E-04 0.14234 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.09532E-01 9.3E-05  1.78937E-02 0.00036  1.26417E-03 0.00531  1.42235E+00 0.00051 ];
INF_SP1                   (idx, [1:   8]) = [  2.36944E-01 0.00017  5.22732E-03 0.00114  5.35192E-04 0.00634  3.78289E-01 0.00059 ];
INF_SP2                   (idx, [1:   8]) = [  9.70848E-02 0.00025 -1.54733E-03 0.00268  2.94451E-04 0.00797  9.04307E-02 0.00077 ];
INF_SP3                   (idx, [1:   8]) = [  9.18124E-03 0.00251 -1.83929E-03 0.00203  1.05386E-04 0.01682  2.71753E-02 0.00219 ];
INF_SP4                   (idx, [1:   8]) = [ -9.57688E-03 0.00232 -6.10494E-04 0.00441  1.81943E-06 0.94972 -8.00577E-03 0.00755 ];
INF_SP5                   (idx, [1:   8]) = [  1.73322E-04 0.11079  1.65765E-05 0.14946 -4.16397E-05 0.03377  6.23071E-03 0.00966 ];
INF_SP6                   (idx, [1:   8]) = [  5.20521E-03 0.00196 -1.42211E-04 0.01037 -5.25320E-05 0.03136 -1.58278E-02 0.00300 ];
INF_SP7                   (idx, [1:   8]) = [  9.07421E-04 0.01350 -1.68665E-04 0.01115 -4.92725E-05 0.02385  2.79529E-04 0.14234 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31649E-01 0.00059  1.05184E+00 0.00613 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.32874E-01 0.00104  1.10435E+00 0.00774 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.32933E-01 0.00100  1.11652E+00 0.00699 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29190E-01 0.00098  9.52282E-01 0.00669 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43897E+00 0.00059  3.17191E-01 0.00612 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.43143E+00 0.00104  3.02270E-01 0.00776 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.43106E+00 0.00099  2.98895E-01 0.00698 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45443E+00 0.00098  3.50408E-01 0.00662 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.79854E-03 0.00797  1.91485E-04 0.05022  1.08143E-03 0.01976  1.06502E-03 0.01916  3.11455E-03 0.01182  9.96870E-04 0.02037  3.49178E-04 0.03678 ];
LAMBDA                    (idx, [1:  14]) = [  8.24403E-01 0.01924  1.24906E-02 4.5E-06  3.16107E-02 0.00034  1.10180E-01 0.00042  3.20570E-01 0.00029  1.34562E+00 0.00023  8.91868E+00 0.00215 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0010' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:42:04 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:08:57 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292124 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99085E-01  1.00112E+00  1.00317E+00  9.97059E-01  9.99571E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.0E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16099E-02 0.00113  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88390E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.97357E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.97804E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.69192E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.38610E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.38545E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.57957E+00 0.00036  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.10266E-01 0.00113  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000682 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00014E+04 0.00073 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00014E+04 0.00073 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.32418E+02 ;
RUNNING_TIME              (idx, 1)        =  2.68897E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.78167E-01  4.78167E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  6.91500E-02  9.46667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  2.61200E+01  3.20840E+00  2.48568E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  2.20117E-01  2.69333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.02000E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  2.68896E+01  1.19224E+02 ];
CPU_USAGE                 (idx, 1)        = 4.92449 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00042E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.67123E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6133.74;
MEMSIZE                   (idx, 1)        = 6039.76;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 169.50;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.98;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  8.97907E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.84415E+04 ;
TOT_SF_RATE               (idx, 1)        =  7.64947E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.51350E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.05656E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.46554E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.73847E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  4.64519E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.79418E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.18876E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.27428E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  3.45643E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.16676E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.49779E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.11017E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.06505E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.17446E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.64402E+11 ;
CS137_ACTIVITY            (idx, 1)        =  2.69889E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.83671E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.58654E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.03671E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.15199E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.04519E+12 0.00048  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 4 ;
BURNUP                     (idx, [1:  2])  = [  2.00000E+00  2.00017E+00 ];
BURN_DAYS                 (idx, 1)        =  5.00000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.75471E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  1.18449E+16 0.00058  8.57213E-01 0.00026 ];
U238_FISS                 (idx, [1:   4]) = [  8.48725E+14 0.00273  6.14135E-02 0.00257 ];
PU239_FISS                (idx, [1:   4]) = [  1.11238E+15 0.00229  8.05026E-02 0.00222 ];
PU240_FISS                (idx, [1:   4]) = [  2.62663E+11 0.14967  1.90431E-05 0.14970 ];
PU241_FISS                (idx, [1:   4]) = [  8.33830E+12 0.02766  6.03346E-04 0.02764 ];
U235_CAPT                 (idx, [1:   4]) = [  2.62236E+15 0.00156  1.59124E-01 0.00147 ];
U238_CAPT                 (idx, [1:   4]) = [  7.55034E+15 0.00102  4.58117E-01 0.00068 ];
PU239_CAPT                (idx, [1:   4]) = [  6.31053E+14 0.00299  3.82919E-02 0.00294 ];
PU240_CAPT                (idx, [1:   4]) = [  1.13077E+14 0.00716  6.86017E-03 0.00709 ];
PU241_CAPT                (idx, [1:   4]) = [  3.05014E+12 0.04371  1.84767E-04 0.04362 ];
XE135_CAPT                (idx, [1:   4]) = [  7.09541E+14 0.00307  4.30563E-02 0.00305 ];
SM149_CAPT                (idx, [1:   4]) = [  1.56985E+14 0.00619  9.52489E-03 0.00614 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000682 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.42645E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000682 5.00743E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2702337 2.70599E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2265838 2.26892E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 32507 3.25194E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000682 5.00743E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.25963E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50055E+05 2.0E-09  4.50055E+05 2.0E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.45269E+16 1.2E-05  3.45269E+16 1.2E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38363E+16 1.6E-06  1.38363E+16 1.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.64846E+16 0.00044  1.21729E+16 0.00046  4.31165E+15 0.00113 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.03208E+16 0.00024  2.60092E+16 0.00022  4.31165E+15 0.00113 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.04519E+16 0.00048  3.04519E+16 0.00048  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.42072E+18 0.00045  3.90829E+17 0.00044  1.02989E+18 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  1.98079E+14 0.00565 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.05189E+16 0.00025 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.03235E+18 0.00059 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12281E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12281E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.69794E+00 0.00042 ];
SIX_FF_F                  (idx, [1:   2]) = [  8.08954E-01 0.00029 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.58967E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25932E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95782E-01 2.9E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97704E-01 2.2E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.13977E+00 0.00049 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.13236E+00 0.00049 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.49540E+00 1.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03019E+02 1.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.13252E+00 0.00051  1.12501E+00 0.00049  7.34480E-03 0.00799 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.13306E+00 0.00025 ];
COL_KEFF                  (idx, [1:   2]) = [  1.13395E+00 0.00048 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.13306E+00 0.00025 ];
ABS_KINF                  (idx, [1:   2]) = [  1.14047E+00 0.00024 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73897E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73890E+01 7.4E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.62240E-07 0.00323 ];
IMP_EALF                  (idx, [1:   2]) = [  5.61377E-07 0.00129 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.10786E-01 0.00285 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.11046E-01 0.00110 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.89015E-03 0.00545  1.67759E-04 0.03194  9.47668E-04 0.01316  9.23250E-04 0.01424  2.67565E-03 0.00809  8.90192E-04 0.01452  2.85631E-04 0.02515 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.05205E-01 0.01300  1.06419E-02 0.01866  3.15434E-02 0.00028  1.10113E-01 0.00030  3.20819E-01 0.00023  1.34498E+00 0.00021  8.58170E+00 0.00933 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.61365E-03 0.00768  1.88871E-04 0.05066  1.08247E-03 0.02019  1.02861E-03 0.02079  2.97520E-03 0.01211  9.95421E-04 0.02081  3.43073E-04 0.03713 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.36564E-01 0.01998  1.24905E-02 5.3E-06  3.15385E-02 0.00041  1.10107E-01 0.00041  3.20958E-01 0.00033  1.34524E+00 0.00024  8.93242E+00 0.00221 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.16932E-05 0.00114  2.16825E-05 0.00115  2.33628E-05 0.01198 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.45646E-05 0.00101  2.45525E-05 0.00102  2.64557E-05 0.01196 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.47468E-03 0.00801  1.88026E-04 0.04970  1.01888E-03 0.02064  1.01129E-03 0.02097  2.97370E-03 0.01271  9.65249E-04 0.02120  3.17533E-04 0.03902 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.12394E-01 0.02038  1.24905E-02 7.3E-06  3.15296E-02 0.00049  1.10028E-01 0.00047  3.20937E-01 0.00035  1.34532E+00 0.00028  8.95760E+00 0.00283 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.11922E-05 0.00246  2.11785E-05 0.00247  2.33292E-05 0.03012 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.39972E-05 0.00240  2.39817E-05 0.00241  2.64056E-05 0.03004 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.23450E-03 0.02756  1.35183E-04 0.16148  1.06127E-03 0.06998  9.91228E-04 0.06541  2.69977E-03 0.04100  9.81286E-04 0.07298  3.65773E-04 0.11343 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.84854E-01 0.06538  1.24904E-02 1.9E-05  3.15288E-02 0.00113  1.10133E-01 0.00118  3.21144E-01 0.00107  1.34502E+00 0.00066  8.91940E+00 0.00584 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.22272E-03 0.02681  1.34109E-04 0.16270  1.06039E-03 0.06831  9.57468E-04 0.06354  2.73066E-03 0.03979  9.87555E-04 0.07031  3.52538E-04 0.11134 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.81882E-01 0.06529  1.24904E-02 1.9E-05  3.15323E-02 0.00111  1.10141E-01 0.00118  3.21113E-01 0.00105  1.34496E+00 0.00066  8.92076E+00 0.00587 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.95235E+02 0.02781 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.14544E-05 0.00066 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.42942E-05 0.00039 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.40223E-03 0.00518 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.98499E+02 0.00525 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.42457E-07 0.00071 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.86765E-06 0.00041  2.86739E-06 0.00042  2.90491E-06 0.00512 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.52120E-05 0.00087  3.52300E-05 0.00087  3.25837E-05 0.00842 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.57426E-01 0.00033  6.56803E-01 0.00033  7.73699E-01 0.00835 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04064E+01 0.01276 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.38545E+01 0.00046  3.32321E+01 0.00044 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.30936E+04 0.00283  2.93233E+05 0.00136  6.05128E+05 0.00096  6.51636E+05 0.00064  6.00211E+05 0.00066  6.46963E+05 0.00055  4.38807E+05 0.00054  3.88408E+05 0.00078  2.97281E+05 0.00091  2.42838E+05 0.00086  2.09542E+05 0.00098  1.88677E+05 0.00052  1.74679E+05 0.00105  1.65811E+05 0.00090  1.61500E+05 0.00083  1.39466E+05 0.00084  1.37550E+05 0.00101  1.36342E+05 0.00090  1.33778E+05 0.00106  2.60747E+05 0.00070  2.51471E+05 0.00077  1.81051E+05 0.00081  1.17021E+05 0.00132  1.34958E+05 0.00093  1.27297E+05 0.00107  1.15236E+05 0.00144  1.88531E+05 0.00083  4.32901E+04 0.00140  5.41918E+04 0.00165  4.92382E+04 0.00163  2.84668E+04 0.00159  4.97866E+04 0.00155  3.37733E+04 0.00150  2.86687E+04 0.00172  5.40709E+03 0.00366  5.33792E+03 0.00331  5.37179E+03 0.00431  5.44215E+03 0.00223  5.42931E+03 0.00354  5.44038E+03 0.00370  5.71327E+03 0.00343  5.36951E+03 0.00393  1.01595E+04 0.00237  1.63099E+04 0.00267  2.08096E+04 0.00190  5.48977E+04 0.00151  5.74757E+04 0.00135  6.18218E+04 0.00092  4.12032E+04 0.00161  2.99578E+04 0.00143  2.25816E+04 0.00147  2.65751E+04 0.00226  5.03560E+04 0.00134  6.81900E+04 0.00139  1.30377E+05 0.00138  1.98500E+05 0.00119  2.92281E+05 0.00137  1.84834E+05 0.00149  1.31897E+05 0.00134  9.45519E+04 0.00146  8.46099E+04 0.00133  8.31847E+04 0.00159  6.93453E+04 0.00157  4.68245E+04 0.00174  4.31486E+04 0.00188  3.82879E+04 0.00173  3.23384E+04 0.00175  2.54566E+04 0.00177  1.69704E+04 0.00204  5.97645E+03 0.00258 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.14137E+00 0.00042 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.14109E+18 0.00045  2.79663E+17 0.00133 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.36640E-01 8.0E-05  1.50187E+00 0.00040 ];
INF_CAPT                  (idx, [1:   4]) = [  6.66331E-03 0.00079  3.17599E-02 0.00058 ];
INF_ABS                   (idx, [1:   4]) = [  9.08999E-03 0.00065  7.13404E-02 0.00100 ];
INF_FISS                  (idx, [1:   4]) = [  2.42669E-03 0.00048  3.95805E-02 0.00134 ];
INF_NSF                   (idx, [1:   4]) = [  6.23035E-03 0.00047  9.80557E-02 0.00135 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.56743E+00 5.1E-05  2.47737E+00 1.3E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03887E+02 5.2E-06  2.02802E+02 2.1E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.81002E-08 0.00042  2.51888E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.27558E-01 8.5E-05  1.43058E+00 0.00046 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42290E-01 0.00019  3.80612E-01 0.00053 ];
INF_SCATT2                (idx, [1:   4]) = [  9.55877E-02 0.00035  9.11287E-02 0.00099 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34348E-03 0.00363  2.74792E-02 0.00224 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.01704E-02 0.00201 -8.13121E-03 0.00937 ];
INF_SCATT5                (idx, [1:   4]) = [  1.94412E-04 0.09603  6.18945E-03 0.00992 ];
INF_SCATT6                (idx, [1:   4]) = [  5.06610E-03 0.00333 -1.59893E-02 0.00361 ];
INF_SCATT7                (idx, [1:   4]) = [  7.38147E-04 0.02005  2.59585E-04 0.19639 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.27598E-01 8.5E-05  1.43058E+00 0.00046 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42291E-01 0.00019  3.80612E-01 0.00053 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.55875E-02 0.00035  9.11287E-02 0.00099 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34344E-03 0.00363  2.74792E-02 0.00224 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.01705E-02 0.00201 -8.13121E-03 0.00937 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.94416E-04 0.09610  6.18945E-03 0.00992 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.06599E-03 0.00334 -1.59893E-02 0.00361 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.38168E-04 0.01998  2.59585E-04 0.19639 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.14124E-01 0.00029  9.68597E-01 0.00036 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55673E+00 0.00029  3.44142E-01 0.00036 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.05036E-03 0.00066  7.13404E-02 0.00100 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69290E-02 0.00019  7.25747E-02 0.00096 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.09711E-01 8.2E-05  1.78475E-02 0.00039  1.28590E-03 0.00451  1.42930E+00 0.00046 ];
INF_S1                    (idx, [1:   8]) = [  2.37085E-01 0.00019  5.20451E-03 0.00099  5.44848E-04 0.00523  3.80067E-01 0.00053 ];
INF_S2                    (idx, [1:   8]) = [  9.71296E-02 0.00035 -1.54188E-03 0.00222  2.97607E-04 0.00605  9.08311E-02 0.00099 ];
INF_S3                    (idx, [1:   8]) = [  9.17648E-03 0.00288 -1.83300E-03 0.00149  1.08553E-04 0.01832  2.73707E-02 0.00225 ];
INF_S4                    (idx, [1:   8]) = [ -9.56177E-03 0.00214 -6.08619E-04 0.00310  2.00661E-06 0.94920 -8.13321E-03 0.00933 ];
INF_S5                    (idx, [1:   8]) = [  1.80598E-04 0.10482  1.38133E-05 0.14967 -4.15797E-05 0.04254  6.23103E-03 0.00984 ];
INF_S6                    (idx, [1:   8]) = [  5.21273E-03 0.00317 -1.46626E-04 0.01385 -5.16987E-05 0.02841 -1.59376E-02 0.00362 ];
INF_S7                    (idx, [1:   8]) = [  9.05752E-04 0.01626 -1.67605E-04 0.01749 -4.96238E-05 0.02762  3.09209E-04 0.16635 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.09750E-01 8.1E-05  1.78475E-02 0.00039  1.28590E-03 0.00451  1.42930E+00 0.00046 ];
INF_SP1                   (idx, [1:   8]) = [  2.37086E-01 0.00018  5.20451E-03 0.00099  5.44848E-04 0.00523  3.80067E-01 0.00053 ];
INF_SP2                   (idx, [1:   8]) = [  9.71294E-02 0.00035 -1.54188E-03 0.00222  2.97607E-04 0.00605  9.08311E-02 0.00099 ];
INF_SP3                   (idx, [1:   8]) = [  9.17644E-03 0.00288 -1.83300E-03 0.00149  1.08553E-04 0.01832  2.73707E-02 0.00225 ];
INF_SP4                   (idx, [1:   8]) = [ -9.56184E-03 0.00214 -6.08619E-04 0.00310  2.00661E-06 0.94920 -8.13321E-03 0.00933 ];
INF_SP5                   (idx, [1:   8]) = [  1.80602E-04 0.10488  1.38133E-05 0.14967 -4.15797E-05 0.04254  6.23103E-03 0.00984 ];
INF_SP6                   (idx, [1:   8]) = [  5.21262E-03 0.00317 -1.46626E-04 0.01385 -5.16987E-05 0.02841 -1.59376E-02 0.00362 ];
INF_SP7                   (idx, [1:   8]) = [  9.05774E-04 0.01620 -1.67605E-04 0.01749 -4.96238E-05 0.02762  3.09209E-04 0.16635 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31800E-01 0.00079  1.06053E+00 0.00683 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33141E-01 0.00092  1.11752E+00 0.00767 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.32894E-01 0.00103  1.13576E+00 0.00880 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29412E-01 0.00128  9.50158E-01 0.00714 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43804E+00 0.00079  3.14668E-01 0.00697 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42978E+00 0.00092  2.98700E-01 0.00767 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.43130E+00 0.00103  2.94049E-01 0.00904 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45305E+00 0.00128  3.51256E-01 0.00727 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.61365E-03 0.00768  1.88871E-04 0.05066  1.08247E-03 0.02019  1.02861E-03 0.02079  2.97520E-03 0.01211  9.95421E-04 0.02081  3.43073E-04 0.03713 ];
LAMBDA                    (idx, [1:  14]) = [  8.36564E-01 0.01998  1.24905E-02 5.3E-06  3.15385E-02 0.00041  1.10107E-01 0.00041  3.20958E-01 0.00033  1.34524E+00 0.00024  8.93242E+00 0.00221 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0010' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:42:04 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:14:42 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292124 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99644E-01  9.99491E-01  1.00333E+00  9.97891E-01  9.99639E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15362E-02 0.00111  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88464E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.98901E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  7.99344E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68381E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.38003E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.37936E+01 0.00043  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.48261E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.05851E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000598 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00012E+04 0.00072 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00012E+04 0.00072 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.61139E+02 ;
RUNNING_TIME              (idx, 1)        =  3.26401E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.78167E-01  4.78167E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  8.79000E-02  8.81667E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.17973E+01  3.18828E+00  2.48898E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  2.73867E-01  2.62667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.28667E-02  8.00002E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.26400E+01  1.19153E+02 ];
CPU_USAGE                 (idx, 1)        = 4.93684 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00136E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.70200E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6133.74;
MEMSIZE                   (idx, 1)        = 6039.76;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 169.50;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.98;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.05919E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.83356E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.05453E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.53107E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.06870E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.52810E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.72666E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  5.59801E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  3.95507E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.51534E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.36007E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.08267E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.31906E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.69174E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.13908E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.15601E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.21432E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.60858E+11 ;
CS137_ACTIVITY            (idx, 1)        =  4.04868E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.88933E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.55608E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  4.97918E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.16645E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.10283E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 5 ;
BURNUP                     (idx, [1:  2])  = [  3.00000E+00  3.00026E+00 ];
BURN_DAYS                 (idx, 1)        =  7.50000E+01 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.83028E-01 0.00101 ];
U235_FISS                 (idx, [1:   4]) = [  1.13572E+16 0.00065  8.21491E-01 0.00031 ];
U238_FISS                 (idx, [1:   4]) = [  8.67760E+14 0.00273  6.27595E-02 0.00257 ];
PU239_FISS                (idx, [1:   4]) = [  1.57250E+15 0.00197  1.13750E-01 0.00195 ];
PU240_FISS                (idx, [1:   4]) = [  5.58469E+11 0.10156  4.03421E-05 0.10164 ];
PU241_FISS                (idx, [1:   4]) = [  2.28442E+13 0.01649  1.65234E-03 0.01649 ];
U235_CAPT                 (idx, [1:   4]) = [  2.52558E+15 0.00154  1.48213E-01 0.00143 ];
U238_CAPT                 (idx, [1:   4]) = [  7.63606E+15 0.00099  4.48095E-01 0.00066 ];
PU239_CAPT                (idx, [1:   4]) = [  8.80405E+14 0.00259  5.16635E-02 0.00248 ];
PU240_CAPT                (idx, [1:   4]) = [  2.19054E+14 0.00520  1.28555E-02 0.00518 ];
PU241_CAPT                (idx, [1:   4]) = [  8.16198E+12 0.02704  4.78958E-04 0.02703 ];
XE135_CAPT                (idx, [1:   4]) = [  7.07408E+14 0.00300  4.15203E-02 0.00304 ];
SM149_CAPT                (idx, [1:   4]) = [  1.64009E+14 0.00629  9.62542E-03 0.00628 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000598 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.46608E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000598 5.00747E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2742167 2.74597E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2224853 2.22791E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 33578 3.35906E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000598 5.00747E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -8.94070E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50055E+05 2.0E-09  4.50055E+05 2.0E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.47037E+16 1.4E-05  3.47037E+16 1.4E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38226E+16 2.0E-06  1.38226E+16 2.0E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.70338E+16 0.00045  1.26505E+16 0.00045  4.38324E+15 0.00112 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.08564E+16 0.00025  2.64731E+16 0.00022  4.38324E+15 0.00112 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.10283E+16 0.00049  3.10283E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.44502E+18 0.00048  3.96418E+17 0.00045  1.04860E+18 0.00053 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.08476E+14 0.00574 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.10648E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.05001E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12164E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12164E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.68811E+00 0.00042 ];
SIX_FF_F                  (idx, [1:   2]) = [  8.08228E-01 0.00030 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.56047E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25839E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95622E-01 3.0E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97650E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.12631E+00 0.00050 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.11874E+00 0.00050 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.51065E+00 1.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03220E+02 2.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.11870E+00 0.00051  1.11149E+00 0.00050  7.24932E-03 0.00867 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.11886E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.11859E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.11886E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.12643E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73563E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73533E+01 8.5E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.81314E-07 0.00326 ];
IMP_EALF                  (idx, [1:   2]) = [  5.81847E-07 0.00148 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.15335E-01 0.00276 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.15465E-01 0.00114 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.90185E-03 0.00587  1.75353E-04 0.03149  9.47199E-04 0.01320  9.20753E-04 0.01385  2.67845E-03 0.00856  8.86802E-04 0.01445  2.93294E-04 0.02305 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.13769E-01 0.01225  1.07450E-02 0.01806  3.15177E-02 0.00030  1.10167E-01 0.00032  3.20999E-01 0.00024  1.34376E+00 0.00032  8.74956E+00 0.00669 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.46707E-03 0.00839  1.88997E-04 0.04595  1.02767E-03 0.02013  9.91662E-04 0.02072  2.94614E-03 0.01224  9.83589E-04 0.02129  3.29008E-04 0.03367 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.27070E-01 0.01806  1.24946E-02 0.00025  3.15167E-02 0.00042  1.10188E-01 0.00044  3.21167E-01 0.00036  1.34396E+00 0.00038  8.91698E+00 0.00280 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.17165E-05 0.00119  2.17092E-05 0.00120  2.28981E-05 0.01213 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.42909E-05 0.00106  2.42826E-05 0.00107  2.56205E-05 0.01220 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.47380E-03 0.00884  1.96201E-04 0.04993  1.02511E-03 0.02091  9.87340E-04 0.02162  2.93726E-03 0.01305  9.99331E-04 0.02047  3.28554E-04 0.03409 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.31334E-01 0.01829  1.24979E-02 0.00043  3.15212E-02 0.00049  1.10225E-01 0.00053  3.21042E-01 0.00035  1.34381E+00 0.00054  8.91977E+00 0.00319 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.11572E-05 0.00248  2.11498E-05 0.00249  2.25161E-05 0.02946 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.36665E-05 0.00246  2.36583E-05 0.00247  2.51759E-05 0.02939 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.53323E-03 0.02696  2.11259E-04 0.14003  1.02137E-03 0.06501  9.41841E-04 0.06704  3.04474E-03 0.04048  9.97565E-04 0.06672  3.16447E-04 0.11143 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.36303E-01 0.06514  1.24901E-02 2.7E-05  3.15102E-02 0.00116  1.10609E-01 0.00145  3.21233E-01 0.00107  1.34303E+00 0.00148  8.94979E+00 0.00590 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.48297E-03 0.02667  2.09946E-04 0.13786  1.03208E-03 0.06462  9.07772E-04 0.06412  3.03012E-03 0.03989  9.99251E-04 0.06550  3.03802E-04 0.10812 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.20098E-01 0.06339  1.24901E-02 2.8E-05  3.15117E-02 0.00116  1.10626E-01 0.00145  3.21163E-01 0.00105  1.34297E+00 0.00147  8.94892E+00 0.00587 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -3.10074E+02 0.02718 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.14598E-05 0.00066 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.40038E-05 0.00042 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.56474E-03 0.00598 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -3.05998E+02 0.00604 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.39580E-07 0.00070 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.85443E-06 0.00044  2.85418E-06 0.00044  2.89418E-06 0.00512 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.51144E-05 0.00086  3.51363E-05 0.00087  3.18792E-05 0.00943 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.54424E-01 0.00033  6.53842E-01 0.00033  7.66741E-01 0.00947 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03790E+01 0.01310 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.37936E+01 0.00043  3.30999E+01 0.00043 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.35804E+04 0.00284  2.95270E+05 0.00150  6.06604E+05 0.00088  6.52547E+05 0.00078  6.01247E+05 0.00070  6.45493E+05 0.00068  4.38606E+05 0.00087  3.88410E+05 0.00062  2.97133E+05 0.00065  2.42741E+05 0.00092  2.09318E+05 0.00103  1.88591E+05 0.00088  1.74324E+05 0.00095  1.65426E+05 0.00099  1.61414E+05 0.00076  1.39417E+05 0.00105  1.37554E+05 0.00100  1.36205E+05 0.00110  1.33668E+05 0.00081  2.60913E+05 0.00063  2.51828E+05 0.00073  1.81249E+05 0.00085  1.17048E+05 0.00110  1.34764E+05 0.00100  1.27312E+05 0.00122  1.15081E+05 0.00081  1.88120E+05 0.00077  4.31292E+04 0.00151  5.41335E+04 0.00151  4.91974E+04 0.00169  2.84925E+04 0.00154  4.95500E+04 0.00150  3.38189E+04 0.00192  2.85302E+04 0.00160  5.35807E+03 0.00384  5.23650E+03 0.00379  5.17825E+03 0.00314  5.22429E+03 0.00264  5.24909E+03 0.00334  5.32511E+03 0.00388  5.63735E+03 0.00384  5.33254E+03 0.00511  1.01051E+04 0.00308  1.61762E+04 0.00230  2.06279E+04 0.00162  5.46034E+04 0.00127  5.70865E+04 0.00144  6.12646E+04 0.00116  4.05522E+04 0.00192  2.91603E+04 0.00175  2.20501E+04 0.00182  2.58002E+04 0.00156  4.91016E+04 0.00152  6.68447E+04 0.00165  1.28579E+05 0.00173  1.96249E+05 0.00153  2.89921E+05 0.00172  1.83752E+05 0.00169  1.31059E+05 0.00193  9.39515E+04 0.00191  8.42064E+04 0.00190  8.27684E+04 0.00191  6.90228E+04 0.00168  4.65985E+04 0.00227  4.29525E+04 0.00215  3.81843E+04 0.00231  3.22187E+04 0.00241  2.53384E+04 0.00184  1.69473E+04 0.00243  6.00058E+03 0.00280 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.12616E+00 0.00049 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.16275E+18 0.00054  2.82298E+17 0.00162 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.36546E-01 0.00012  1.50960E+00 0.00045 ];
INF_CAPT                  (idx, [1:   4]) = [  6.76844E-03 0.00082  3.24648E-02 0.00070 ];
INF_ABS                   (idx, [1:   4]) = [  9.15354E-03 0.00070  7.16136E-02 0.00118 ];
INF_FISS                  (idx, [1:   4]) = [  2.38509E-03 0.00054  3.91488E-02 0.00159 ];
INF_NSF                   (idx, [1:   4]) = [  6.14236E-03 0.00053  9.76537E-02 0.00160 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.57532E+00 4.7E-05  2.49442E+00 2.5E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.03988E+02 6.3E-06  2.03027E+02 4.0E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.78748E-08 0.00044  2.52377E-06 0.00021 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.27390E-01 0.00013  1.43797E+00 0.00052 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42274E-01 0.00018  3.82704E-01 0.00059 ];
INF_SCATT2                (idx, [1:   4]) = [  9.56339E-02 0.00039  9.13276E-02 0.00102 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34045E-03 0.00353  2.74268E-02 0.00325 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.01940E-02 0.00181 -8.27493E-03 0.00938 ];
INF_SCATT5                (idx, [1:   4]) = [  1.55685E-04 0.09917  6.18067E-03 0.00842 ];
INF_SCATT6                (idx, [1:   4]) = [  5.04599E-03 0.00391 -1.61001E-02 0.00328 ];
INF_SCATT7                (idx, [1:   4]) = [  7.51772E-04 0.01828  3.19728E-04 0.12311 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.27430E-01 0.00013  1.43797E+00 0.00052 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42275E-01 0.00018  3.82704E-01 0.00059 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.56344E-02 0.00040  9.13276E-02 0.00102 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34056E-03 0.00352  2.74268E-02 0.00325 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.01943E-02 0.00181 -8.27493E-03 0.00938 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.55676E-04 0.09923  6.18067E-03 0.00842 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.04600E-03 0.00392 -1.61001E-02 0.00328 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.51948E-04 0.01831  3.19728E-04 0.12311 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13817E-01 0.00033  9.73987E-01 0.00036 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.55897E+00 0.00033  3.42237E-01 0.00036 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.11369E-03 0.00071  7.16136E-02 0.00118 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69183E-02 0.00022  7.29039E-02 0.00121 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.09628E-01 0.00013  1.77616E-02 0.00048  1.27687E-03 0.00497  1.43670E+00 0.00053 ];
INF_S1                    (idx, [1:   8]) = [  2.37096E-01 0.00018  5.17836E-03 0.00091  5.36216E-04 0.00651  3.82168E-01 0.00060 ];
INF_S2                    (idx, [1:   8]) = [  9.71770E-02 0.00038 -1.54309E-03 0.00250  2.90548E-04 0.01185  9.10370E-02 0.00102 ];
INF_S3                    (idx, [1:   8]) = [  9.16781E-03 0.00273 -1.82736E-03 0.00163  1.05499E-04 0.02422  2.73213E-02 0.00326 ];
INF_S4                    (idx, [1:   8]) = [ -9.58757E-03 0.00197 -6.06466E-04 0.00540  6.88572E-07 1.00000 -8.27561E-03 0.00948 ];
INF_S5                    (idx, [1:   8]) = [  1.38286E-04 0.11195  1.73992E-05 0.15023 -4.35558E-05 0.03978  6.22422E-03 0.00831 ];
INF_S6                    (idx, [1:   8]) = [  5.18797E-03 0.00378 -1.41974E-04 0.01372 -5.29662E-05 0.02524 -1.60471E-02 0.00330 ];
INF_S7                    (idx, [1:   8]) = [  9.19505E-04 0.01511 -1.67733E-04 0.01281 -4.89223E-05 0.02921  3.68650E-04 0.10628 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.09668E-01 0.00013  1.77616E-02 0.00048  1.27687E-03 0.00497  1.43670E+00 0.00053 ];
INF_SP1                   (idx, [1:   8]) = [  2.37096E-01 0.00018  5.17836E-03 0.00091  5.36216E-04 0.00651  3.82168E-01 0.00060 ];
INF_SP2                   (idx, [1:   8]) = [  9.71774E-02 0.00038 -1.54309E-03 0.00250  2.90548E-04 0.01185  9.10370E-02 0.00102 ];
INF_SP3                   (idx, [1:   8]) = [  9.16791E-03 0.00273 -1.82736E-03 0.00163  1.05499E-04 0.02422  2.73213E-02 0.00326 ];
INF_SP4                   (idx, [1:   8]) = [ -9.58781E-03 0.00196 -6.06466E-04 0.00540  6.88572E-07 1.00000 -8.27561E-03 0.00948 ];
INF_SP5                   (idx, [1:   8]) = [  1.38277E-04 0.11203  1.73992E-05 0.15023 -4.35558E-05 0.03978  6.22422E-03 0.00831 ];
INF_SP6                   (idx, [1:   8]) = [  5.18798E-03 0.00379 -1.41974E-04 0.01372 -5.29662E-05 0.02524 -1.60471E-02 0.00330 ];
INF_SP7                   (idx, [1:   8]) = [  9.19681E-04 0.01513 -1.67733E-04 0.01281 -4.89223E-05 0.02921  3.68650E-04 0.10628 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31831E-01 0.00046  1.08636E+00 0.00652 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33126E-01 0.00070  1.14723E+00 0.00876 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33536E-01 0.00106  1.16058E+00 0.00876 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28898E-01 0.00094  9.73894E-01 0.00638 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43784E+00 0.00046  3.07150E-01 0.00658 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42986E+00 0.00069  2.91089E-01 0.00875 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42737E+00 0.00105  2.87756E-01 0.00900 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45629E+00 0.00094  3.42605E-01 0.00641 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.46707E-03 0.00839  1.88997E-04 0.04595  1.02767E-03 0.02013  9.91662E-04 0.02072  2.94614E-03 0.01224  9.83589E-04 0.02129  3.29008E-04 0.03367 ];
LAMBDA                    (idx, [1:  14]) = [  8.27070E-01 0.01806  1.24946E-02 0.00025  3.15167E-02 0.00042  1.10188E-01 0.00044  3.21167E-01 0.00036  1.34396E+00 0.00038  8.91698E+00 0.00280 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0010' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:42:04 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:20:30 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292124 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00237E+00  1.00016E+00  1.00155E+00  9.95804E-01  1.00012E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15644E-02 0.00106  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88436E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  7.99934E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.00378E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67910E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.37186E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.37118E+01 0.00044  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.40752E+00 0.00034  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.05011E-01 0.00109  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000746 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00015E+04 0.00071 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00015E+04 0.00071 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  1.90045E+02 ;
RUNNING_TIME              (idx, 1)        =  3.84278E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.78167E-01  4.78167E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.07800E-01  1.00667E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  3.75096E+01  3.20092E+00  2.51138E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  3.28733E-01  2.70000E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.62500E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  3.84277E+01  1.19261E+02 ];
CPU_USAGE                 (idx, 1)        = 4.94552 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99843E+00 0.00020 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.72363E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6133.74;
MEMSIZE                   (idx, 1)        = 6039.76;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 169.50;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.98;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.11624E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.82336E+04 ;
TOT_SF_RATE               (idx, 1)        =  8.86414E+03 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.54932E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.08135E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.56690E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.71521E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  6.49028E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.06206E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.92143E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.44987E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.56885E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.41708E+07 ;
SR90_ACTIVITY             (idx, 1)        =  4.85455E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.16435E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.19815E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.24882E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.23047E+12 ;
CS137_ACTIVITY            (idx, 1)        =  5.39774E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.92723E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.53061E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  7.52796E+10 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.17734E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.15570E+12 0.00047  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 6 ;
BURNUP                     (idx, [1:  2])  = [  4.00000E+00  4.00035E+00 ];
BURN_DAYS                 (idx, 1)        =  1.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  4.91798E-01 0.00106 ];
U233_FISS                 (idx, [1:   4]) = [  6.35647E+09 1.00000  4.60723E-07 1.00000 ];
U235_FISS                 (idx, [1:   4]) = [  1.09232E+16 0.00064  7.90116E-01 0.00036 ];
U238_FISS                 (idx, [1:   4]) = [  8.84089E+14 0.00260  6.39436E-02 0.00247 ];
PU239_FISS                (idx, [1:   4]) = [  1.96550E+15 0.00185  1.42169E-01 0.00174 ];
PU240_FISS                (idx, [1:   4]) = [  8.86381E+11 0.08303  6.40625E-05 0.08305 ];
PU241_FISS                (idx, [1:   4]) = [  4.62289E+13 0.01170  3.34388E-03 0.01169 ];
U235_CAPT                 (idx, [1:   4]) = [  2.43295E+15 0.00156  1.38530E-01 0.00148 ];
U238_CAPT                 (idx, [1:   4]) = [  7.72738E+15 0.00100  4.39955E-01 0.00067 ];
PU239_CAPT                (idx, [1:   4]) = [  1.10638E+15 0.00238  6.29966E-02 0.00235 ];
PU240_CAPT                (idx, [1:   4]) = [  3.32854E+14 0.00444  1.89517E-02 0.00440 ];
PU241_CAPT                (idx, [1:   4]) = [  1.68415E+13 0.01993  9.59081E-04 0.01993 ];
XE135_CAPT                (idx, [1:   4]) = [  7.10253E+14 0.00302  4.04383E-02 0.00293 ];
SM149_CAPT                (idx, [1:   4]) = [  1.68097E+14 0.00616  9.57234E-03 0.00618 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000746 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.59924E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000746 5.00760E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2778889 2.78278E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2187603 2.19055E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 34254 3.42629E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000746 5.00760E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.86733E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50055E+05 2.0E-09  4.50055E+05 2.0E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.48624E+16 1.4E-05  3.48624E+16 1.4E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.38102E+16 2.2E-06  1.38102E+16 2.2E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.75631E+16 0.00046  1.31224E+16 0.00043  4.44069E+15 0.00114 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.13733E+16 0.00025  2.69326E+16 0.00021  4.44069E+15 0.00114 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.15570E+16 0.00047  3.15570E+16 0.00047  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.46615E+18 0.00046  4.01500E+17 0.00042  1.06465E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.16261E+14 0.00568 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.15896E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.06535E+18 0.00061 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.12047E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.12047E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.67632E+00 0.00043 ];
SIX_FF_F                  (idx, [1:   2]) = [  8.08292E-01 0.00030 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.52890E-01 0.00032 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25884E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95509E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97628E-01 2.1E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.11356E+00 0.00051 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.10593E+00 0.00051 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.52439E+00 1.6E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03402E+02 2.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.10599E+00 0.00052  1.09897E+00 0.00051  6.95612E-03 0.00885 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.10532E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.10487E+00 0.00047 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.10532E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.11295E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.73252E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73256E+01 8.1E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  5.99698E-07 0.00322 ];
IMP_EALF                  (idx, [1:   2]) = [  5.98169E-07 0.00141 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.19180E-01 0.00274 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.19203E-01 0.00117 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.86977E-03 0.00581  1.68177E-04 0.03366  9.56764E-04 0.01421  9.08279E-04 0.01413  2.66392E-03 0.00835  8.87814E-04 0.01480  2.84814E-04 0.02542 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.06213E-01 0.01316  1.07434E-02 0.01806  3.14684E-02 0.00032  1.10153E-01 0.00032  3.21024E-01 0.00023  1.34246E+00 0.00038  8.60185E+00 0.00894 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.31932E-03 0.00845  1.79133E-04 0.04975  1.05271E-03 0.01974  9.63456E-04 0.02161  2.86495E-03 0.01257  9.55802E-04 0.02201  3.03269E-04 0.03527 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.04922E-01 0.01854  1.24918E-02 0.00012  3.14661E-02 0.00044  1.10134E-01 0.00042  3.20957E-01 0.00034  1.34261E+00 0.00053  8.93439E+00 0.00268 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.17576E-05 0.00116  2.17493E-05 0.00116  2.30758E-05 0.01250 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.40602E-05 0.00102  2.40510E-05 0.00102  2.55159E-05 0.01244 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.27861E-03 0.00898  1.67681E-04 0.04966  1.03943E-03 0.02258  9.51451E-04 0.02160  2.86143E-03 0.01299  9.57766E-04 0.02198  3.00856E-04 0.03811 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.09040E-01 0.02071  1.24921E-02 0.00016  3.14571E-02 0.00054  1.10175E-01 0.00058  3.21150E-01 0.00040  1.34280E+00 0.00060  8.92871E+00 0.00404 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.10780E-05 0.00263  2.10668E-05 0.00265  2.23975E-05 0.02944 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.33079E-05 0.00254  2.32956E-05 0.00257  2.47631E-05 0.02943 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.33398E-03 0.02851  1.47493E-04 0.16770  1.04994E-03 0.07171  9.05822E-04 0.07523  2.94892E-03 0.03892  9.36571E-04 0.06631  3.45229E-04 0.12192 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.25279E-01 0.06390  1.24900E-02 2.9E-05  3.13810E-02 0.00140  1.10026E-01 0.00123  3.21299E-01 0.00113  1.34420E+00 0.00132  8.95540E+00 0.00873 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.32475E-03 0.02707  1.45563E-04 0.16208  1.04591E-03 0.06978  9.14224E-04 0.07141  2.93881E-03 0.03799  9.41632E-04 0.06263  3.38607E-04 0.11776 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.25467E-01 0.06359  1.24900E-02 2.9E-05  3.13755E-02 0.00141  1.10033E-01 0.00121  3.21358E-01 0.00112  1.34424E+00 0.00127  8.95800E+00 0.00874 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -3.00742E+02 0.02853 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.14832E-05 0.00073 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.37569E-05 0.00049 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.28400E-03 0.00591 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.92599E+02 0.00596 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.36989E-07 0.00071 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.83752E-06 0.00040  2.83720E-06 0.00041  2.88280E-06 0.00532 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.50208E-05 0.00088  3.50401E-05 0.00088  3.21835E-05 0.00963 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.51237E-01 0.00032  6.50751E-01 0.00032  7.46324E-01 0.00917 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.04121E+01 0.01391 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.37118E+01 0.00044  3.29579E+01 0.00045 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.46596E+04 0.00331  2.95120E+05 0.00141  6.06839E+05 0.00089  6.52208E+05 0.00065  6.00850E+05 0.00072  6.45622E+05 0.00058  4.37901E+05 0.00063  3.88060E+05 0.00064  2.96747E+05 0.00078  2.42843E+05 0.00072  2.09220E+05 0.00077  1.88352E+05 0.00068  1.74096E+05 0.00101  1.65618E+05 0.00087  1.61245E+05 0.00084  1.39286E+05 0.00088  1.37550E+05 0.00076  1.36276E+05 0.00086  1.33588E+05 0.00102  2.60662E+05 0.00067  2.51131E+05 0.00060  1.80826E+05 0.00077  1.16854E+05 0.00100  1.34860E+05 0.00085  1.27392E+05 0.00078  1.14798E+05 0.00112  1.87784E+05 0.00081  4.31756E+04 0.00166  5.42041E+04 0.00156  4.93092E+04 0.00172  2.84496E+04 0.00201  4.95218E+04 0.00103  3.34909E+04 0.00144  2.84242E+04 0.00193  5.32390E+03 0.00323  5.10935E+03 0.00343  5.01357E+03 0.00373  4.97711E+03 0.00386  5.04440E+03 0.00333  5.16737E+03 0.00403  5.52345E+03 0.00355  5.25465E+03 0.00356  9.98880E+03 0.00246  1.60509E+04 0.00162  2.05123E+04 0.00226  5.41236E+04 0.00124  5.67038E+04 0.00131  6.07624E+04 0.00100  3.98616E+04 0.00154  2.86209E+04 0.00195  2.15424E+04 0.00200  2.51788E+04 0.00147  4.81098E+04 0.00162  6.56693E+04 0.00139  1.26721E+05 0.00128  1.94124E+05 0.00145  2.87561E+05 0.00142  1.82508E+05 0.00173  1.30259E+05 0.00170  9.33421E+04 0.00169  8.37667E+04 0.00177  8.23106E+04 0.00166  6.86281E+04 0.00172  4.64044E+04 0.00197  4.27628E+04 0.00164  3.80044E+04 0.00192  3.21698E+04 0.00216  2.52968E+04 0.00201  1.68773E+04 0.00201  5.96995E+03 0.00246 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.11249E+00 0.00045 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.18166E+18 0.00041  2.84512E+17 0.00148 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.36673E-01 0.00012  1.51556E+00 0.00041 ];
INF_CAPT                  (idx, [1:   4]) = [  6.88872E-03 0.00057  3.31230E-02 0.00067 ];
INF_ABS                   (idx, [1:   4]) = [  9.23406E-03 0.00049  7.19293E-02 0.00109 ];
INF_FISS                  (idx, [1:   4]) = [  2.34534E-03 0.00042  3.88063E-02 0.00146 ];
INF_NSF                   (idx, [1:   4]) = [  6.05746E-03 0.00041  9.73938E-02 0.00148 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.58276E+00 5.0E-05  2.50974E+00 2.5E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04084E+02 4.7E-06  2.03231E+02 4.1E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.76607E-08 0.00037  2.52790E-06 0.00020 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.27426E-01 0.00013  1.44364E+00 0.00048 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42391E-01 0.00022  3.84383E-01 0.00055 ];
INF_SCATT2                (idx, [1:   4]) = [  9.56566E-02 0.00033  9.18648E-02 0.00121 ];
INF_SCATT3                (idx, [1:   4]) = [  7.35054E-03 0.00309  2.77288E-02 0.00264 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.01926E-02 0.00148 -8.20729E-03 0.00771 ];
INF_SCATT5                (idx, [1:   4]) = [  1.83999E-04 0.09047  6.13761E-03 0.00977 ];
INF_SCATT6                (idx, [1:   4]) = [  5.03485E-03 0.00340 -1.62479E-02 0.00331 ];
INF_SCATT7                (idx, [1:   4]) = [  7.25864E-04 0.02378  2.38909E-04 0.22223 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.27466E-01 0.00013  1.44364E+00 0.00048 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42393E-01 0.00022  3.84383E-01 0.00055 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.56571E-02 0.00033  9.18648E-02 0.00121 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.35063E-03 0.00309  2.77288E-02 0.00264 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.01928E-02 0.00148 -8.20729E-03 0.00771 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.83948E-04 0.09022  6.13761E-03 0.00977 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.03479E-03 0.00340 -1.62479E-02 0.00331 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.25832E-04 0.02375  2.38909E-04 0.22223 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13676E-01 0.00028  9.78350E-01 0.00040 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56000E+00 0.00028  3.40711E-01 0.00040 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.19347E-03 0.00047  7.19293E-02 0.00109 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69363E-02 0.00018  7.32103E-02 0.00117 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.09736E-01 0.00012  1.76895E-02 0.00045  1.28770E-03 0.00383  1.44235E+00 0.00048 ];
INF_S1                    (idx, [1:   8]) = [  2.37241E-01 0.00023  5.15092E-03 0.00081  5.42244E-04 0.00785  3.83840E-01 0.00055 ];
INF_S2                    (idx, [1:   8]) = [  9.72005E-02 0.00033 -1.54385E-03 0.00248  2.92914E-04 0.00920  9.15719E-02 0.00122 ];
INF_S3                    (idx, [1:   8]) = [  9.16487E-03 0.00247 -1.81433E-03 0.00147  1.03056E-04 0.02325  2.76258E-02 0.00265 ];
INF_S4                    (idx, [1:   8]) = [ -9.60199E-03 0.00162 -5.90560E-04 0.00401 -9.15363E-07 1.00000 -8.20637E-03 0.00767 ];
INF_S5                    (idx, [1:   8]) = [  1.58322E-04 0.10619  2.56773E-05 0.12264 -4.40495E-05 0.03793  6.18166E-03 0.00971 ];
INF_S6                    (idx, [1:   8]) = [  5.17817E-03 0.00319 -1.43328E-04 0.01553 -5.49772E-05 0.03319 -1.61930E-02 0.00333 ];
INF_S7                    (idx, [1:   8]) = [  9.00549E-04 0.01910 -1.74684E-04 0.01507 -5.23379E-05 0.02701  2.91247E-04 0.18164 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.09777E-01 0.00012  1.76895E-02 0.00045  1.28770E-03 0.00383  1.44235E+00 0.00048 ];
INF_SP1                   (idx, [1:   8]) = [  2.37242E-01 0.00023  5.15092E-03 0.00081  5.42244E-04 0.00785  3.83840E-01 0.00055 ];
INF_SP2                   (idx, [1:   8]) = [  9.72010E-02 0.00033 -1.54385E-03 0.00248  2.92914E-04 0.00920  9.15719E-02 0.00122 ];
INF_SP3                   (idx, [1:   8]) = [  9.16496E-03 0.00248 -1.81433E-03 0.00147  1.03056E-04 0.02325  2.76258E-02 0.00265 ];
INF_SP4                   (idx, [1:   8]) = [ -9.60228E-03 0.00162 -5.90560E-04 0.00401 -9.15363E-07 1.00000 -8.20637E-03 0.00767 ];
INF_SP5                   (idx, [1:   8]) = [  1.58271E-04 0.10595  2.56773E-05 0.12264 -4.40495E-05 0.03793  6.18166E-03 0.00971 ];
INF_SP6                   (idx, [1:   8]) = [  5.17812E-03 0.00319 -1.43328E-04 0.01553 -5.49772E-05 0.03319 -1.61930E-02 0.00333 ];
INF_SP7                   (idx, [1:   8]) = [  9.00516E-04 0.01908 -1.74684E-04 0.01507 -5.23379E-05 0.02701  2.91247E-04 0.18164 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31805E-01 0.00067  1.08617E+00 0.00634 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33337E-01 0.00085  1.15413E+00 0.00856 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33015E-01 0.00111  1.15851E+00 0.00768 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29119E-01 0.00095  9.69698E-01 0.00652 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43800E+00 0.00067  3.07184E-01 0.00635 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42857E+00 0.00085  2.89327E-01 0.00855 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.43056E+00 0.00111  2.88128E-01 0.00759 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45488E+00 0.00095  3.44098E-01 0.00647 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.31932E-03 0.00845  1.79133E-04 0.04975  1.05271E-03 0.01974  9.63456E-04 0.02161  2.86495E-03 0.01257  9.55802E-04 0.02201  3.03269E-04 0.03527 ];
LAMBDA                    (idx, [1:  14]) = [  8.04922E-01 0.01854  1.24918E-02 0.00012  3.14661E-02 0.00044  1.10134E-01 0.00042  3.20957E-01 0.00034  1.34261E+00 0.00053  8.93439E+00 0.00268 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0010' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:42:04 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:26:15 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292124 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00291E+00  9.99365E-01  1.00117E+00  9.96391E-01  1.00017E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15434E-02 0.00116  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88457E-01 1.4E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.01099E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.01541E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67433E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.37234E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.37164E+01 0.00045  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.34762E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.03791E-01 0.00119  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000339 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00007E+04 0.00071 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00007E+04 0.00071 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.18764E+02 ;
RUNNING_TIME              (idx, 1)        =  4.41776E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.78167E-01  4.78167E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.27150E-01  9.13333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  4.31861E+01  3.18315E+00  2.49333E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  3.82333E-01  2.58167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  1.96333E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.41774E+01  1.19111E+02 ];
CPU_USAGE                 (idx, 1)        = 4.95192 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99904E+00 0.00022 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.73963E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6133.74;
MEMSIZE                   (idx, 1)        = 6039.76;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 169.50;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.98;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.16432E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.81446E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.03956E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.57028E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.09591E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.59401E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.70485E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  7.37734E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.14643E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.40761E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.55178E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  4.96973E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.49125E+07 ;
SR90_ACTIVITY             (idx, 1)        =  5.98937E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.18757E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.23116E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.28019E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.96423E+12 ;
CS137_ACTIVITY            (idx, 1)        =  6.74603E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.96207E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.50829E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.11278E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.18721E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.20682E+12 0.00048  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 7 ;
BURNUP                     (idx, [1:  2])  = [  5.00000E+00  5.00043E+00 ];
BURN_DAYS                 (idx, 1)        =  1.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.02135E-01 0.00111 ];
U235_FISS                 (idx, [1:   4]) = [  1.05077E+16 0.00068  7.62220E-01 0.00039 ];
U238_FISS                 (idx, [1:   4]) = [  8.96534E+14 0.00281  6.50241E-02 0.00265 ];
PU239_FISS                (idx, [1:   4]) = [  2.29776E+15 0.00165  1.66674E-01 0.00153 ];
PU240_FISS                (idx, [1:   4]) = [  1.22968E+12 0.07520  8.91866E-05 0.07512 ];
PU241_FISS                (idx, [1:   4]) = [  7.69706E+13 0.00939  5.58359E-03 0.00938 ];
U235_CAPT                 (idx, [1:   4]) = [  2.34438E+15 0.00175  1.29487E-01 0.00164 ];
U238_CAPT                 (idx, [1:   4]) = [  7.81253E+15 0.00103  4.31487E-01 0.00070 ];
PU239_CAPT                (idx, [1:   4]) = [  1.30084E+15 0.00211  7.18557E-02 0.00211 ];
PU240_CAPT                (idx, [1:   4]) = [  4.50281E+14 0.00367  2.48690E-02 0.00359 ];
PU241_CAPT                (idx, [1:   4]) = [  2.79625E+13 0.01502  1.54446E-03 0.01500 ];
XE135_CAPT                (idx, [1:   4]) = [  7.15042E+14 0.00281  3.94979E-02 0.00282 ];
SM149_CAPT                (idx, [1:   4]) = [  1.74354E+14 0.00577  9.63034E-03 0.00574 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000339 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.61365E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000339 5.00761E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2818742 2.82293E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2146451 2.14953E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 35146 3.51562E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000339 5.00761E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.23986E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50055E+05 2.0E-09  4.50055E+05 2.0E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.50056E+16 1.5E-05  3.50056E+16 1.5E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37989E+16 2.5E-06  1.37989E+16 2.5E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.80915E+16 0.00044  1.35706E+16 0.00043  4.52093E+15 0.00115 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.18905E+16 0.00025  2.73696E+16 0.00021  4.52093E+15 0.00115 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.20682E+16 0.00048  3.20682E+16 0.00048  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.48834E+18 0.00046  4.06462E+17 0.00044  1.08187E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.25515E+14 0.00571 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.21160E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.08274E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11931E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11931E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.66286E+00 0.00043 ];
SIX_FF_F                  (idx, [1:   2]) = [  8.06794E-01 0.00028 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.50758E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25798E+00 0.00035 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95437E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97520E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.09820E+00 0.00051 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.09048E+00 0.00051 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.53683E+00 1.7E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03568E+02 2.5E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.09062E+00 0.00053  1.08376E+00 0.00052  6.72013E-03 0.00865 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.09168E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.09172E+00 0.00048 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.09168E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.09941E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72993E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.73005E+01 8.1E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.15669E-07 0.00346 ];
IMP_EALF                  (idx, [1:   2]) = [  6.13393E-07 0.00141 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.23615E-01 0.00286 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.22819E-01 0.00109 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.78330E-03 0.00547  1.59696E-04 0.03329  9.57543E-04 0.01423  9.19821E-04 0.01428  2.60459E-03 0.00814  8.73850E-04 0.01374  2.67796E-04 0.02730 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.82896E-01 0.01375  1.03686E-02 0.02026  3.14188E-02 0.00033  1.09874E-01 0.00203  3.21389E-01 0.00024  1.34111E+00 0.00050  8.33736E+00 0.01210 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.20370E-03 0.00844  1.74666E-04 0.04899  1.03392E-03 0.02161  9.80404E-04 0.02084  2.79212E-03 0.01223  9.41662E-04 0.02106  2.80926E-04 0.03963 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.74655E-01 0.01939  1.24931E-02 0.00019  3.14317E-02 0.00046  1.10125E-01 0.00047  3.21507E-01 0.00037  1.34119E+00 0.00061  8.90990E+00 0.00406 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.18859E-05 0.00119  2.18762E-05 0.00119  2.34280E-05 0.01247 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.38656E-05 0.00105  2.38551E-05 0.00105  2.55445E-05 0.01245 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.17035E-03 0.00886  1.79042E-04 0.05198  9.79614E-04 0.02252  9.79764E-04 0.02117  2.79884E-03 0.01264  9.47782E-04 0.02184  2.85310E-04 0.04106 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.86810E-01 0.02093  1.24922E-02 0.00016  3.14261E-02 0.00058  1.10016E-01 0.00051  3.21396E-01 0.00039  1.33804E+00 0.00115  8.95624E+00 0.00453 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.12253E-05 0.00273  2.12071E-05 0.00273  2.37229E-05 0.03376 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.31453E-05 0.00267  2.31255E-05 0.00268  2.58756E-05 0.03379 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.17826E-03 0.02873  1.69882E-04 0.15901  9.90856E-04 0.07568  1.02574E-03 0.07212  2.80947E-03 0.04143  9.65082E-04 0.06926  2.17218E-04 0.13809 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  6.89235E-01 0.05931  1.24898E-02 3.7E-05  3.13621E-02 0.00148  1.09885E-01 0.00118  3.21511E-01 0.00114  1.33664E+00 0.00267  9.01275E+00 0.00774 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.22274E-03 0.02802  1.69891E-04 0.15907  9.78624E-04 0.07447  1.01956E-03 0.07063  2.86793E-03 0.04063  9.64222E-04 0.06880  2.22509E-04 0.13440 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  6.92217E-01 0.05953  1.24898E-02 3.7E-05  3.13615E-02 0.00148  1.09880E-01 0.00117  3.21578E-01 0.00112  1.33668E+00 0.00271  9.01882E+00 0.00777 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.92531E+02 0.02877 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.15903E-05 0.00074 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.35432E-05 0.00048 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  6.14091E-03 0.00561 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.84507E+02 0.00566 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.36373E-07 0.00074 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.82248E-06 0.00042  2.82218E-06 0.00042  2.87014E-06 0.00532 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.50605E-05 0.00093  3.50799E-05 0.00094  3.20867E-05 0.01011 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.49114E-01 0.00035  6.48692E-01 0.00035  7.33343E-01 0.00912 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05324E+01 0.01285 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.37164E+01 0.00045  3.28564E+01 0.00043 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.42683E+04 0.00325  2.96236E+05 0.00153  6.07550E+05 0.00059  6.52805E+05 0.00088  6.00024E+05 0.00070  6.45017E+05 0.00052  4.37627E+05 0.00089  3.87547E+05 0.00075  2.97083E+05 0.00065  2.42872E+05 0.00082  2.09087E+05 0.00063  1.88608E+05 0.00083  1.74199E+05 0.00074  1.65196E+05 0.00094  1.60949E+05 0.00087  1.39411E+05 0.00077  1.37415E+05 0.00075  1.36126E+05 0.00106  1.33915E+05 0.00114  2.60781E+05 0.00074  2.51144E+05 0.00073  1.81077E+05 0.00078  1.16961E+05 0.00076  1.34632E+05 0.00075  1.27377E+05 0.00051  1.14716E+05 0.00103  1.87387E+05 0.00075  4.31307E+04 0.00157  5.41209E+04 0.00129  4.91424E+04 0.00136  2.84531E+04 0.00185  4.94977E+04 0.00149  3.35082E+04 0.00205  2.82289E+04 0.00203  5.25310E+03 0.00310  5.02110E+03 0.00359  4.81137E+03 0.00312  4.74729E+03 0.00370  4.82557E+03 0.00320  4.98601E+03 0.00304  5.42123E+03 0.00442  5.16129E+03 0.00427  9.92676E+03 0.00334  1.58990E+04 0.00262  2.03029E+04 0.00210  5.39185E+04 0.00115  5.65095E+04 0.00129  6.03903E+04 0.00122  3.93610E+04 0.00122  2.81759E+04 0.00174  2.10937E+04 0.00151  2.47659E+04 0.00132  4.72776E+04 0.00145  6.50251E+04 0.00160  1.25574E+05 0.00102  1.92990E+05 0.00123  2.86367E+05 0.00102  1.82099E+05 0.00137  1.30232E+05 0.00128  9.33690E+04 0.00148  8.37625E+04 0.00125  8.25104E+04 0.00153  6.87874E+04 0.00144  4.64259E+04 0.00158  4.29100E+04 0.00155  3.81035E+04 0.00144  3.21819E+04 0.00148  2.52837E+04 0.00192  1.69432E+04 0.00182  5.98517E+03 0.00241 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.09946E+00 0.00053 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.20037E+18 0.00047  2.87993E+17 0.00121 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.36856E-01 0.00011  1.52204E+00 0.00037 ];
INF_CAPT                  (idx, [1:   4]) = [  7.00108E-03 0.00065  3.36417E-02 0.00060 ];
INF_ABS                   (idx, [1:   4]) = [  9.30765E-03 0.00055  7.19486E-02 0.00095 ];
INF_FISS                  (idx, [1:   4]) = [  2.30657E-03 0.00048  3.83069E-02 0.00127 ];
INF_NSF                   (idx, [1:   4]) = [  5.97399E-03 0.00046  9.66671E-02 0.00128 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.58999E+00 4.5E-05  2.52349E+00 2.5E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04177E+02 4.7E-06  2.03415E+02 4.1E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.74481E-08 0.00041  2.53269E-06 0.00015 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.27550E-01 0.00011  1.45009E+00 0.00043 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42484E-01 0.00022  3.86093E-01 0.00053 ];
INF_SCATT2                (idx, [1:   4]) = [  9.56745E-02 0.00037  9.21191E-02 0.00090 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32007E-03 0.00404  2.77681E-02 0.00260 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02248E-02 0.00170 -8.23411E-03 0.00619 ];
INF_SCATT5                (idx, [1:   4]) = [  1.88504E-04 0.10928  6.43101E-03 0.00794 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07505E-03 0.00398 -1.62722E-02 0.00279 ];
INF_SCATT7                (idx, [1:   4]) = [  7.56197E-04 0.02327  2.89538E-04 0.15958 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.27591E-01 0.00011  1.45009E+00 0.00043 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42485E-01 0.00022  3.86093E-01 0.00053 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.56750E-02 0.00037  9.21191E-02 0.00090 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32003E-03 0.00404  2.77681E-02 0.00260 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02247E-02 0.00170 -8.23411E-03 0.00619 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.88453E-04 0.10929  6.43101E-03 0.00794 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07507E-03 0.00398 -1.62722E-02 0.00279 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.56094E-04 0.02329  2.89538E-04 0.15958 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13601E-01 0.00028  9.83060E-01 0.00031 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56055E+00 0.00028  3.39078E-01 0.00031 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.26697E-03 0.00054  7.19486E-02 0.00095 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69397E-02 0.00021  7.32287E-02 0.00093 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.09916E-01 0.00011  1.76346E-02 0.00041  1.27417E-03 0.00446  1.44881E+00 0.00043 ];
INF_S1                    (idx, [1:   8]) = [  2.37359E-01 0.00022  5.12574E-03 0.00118  5.39261E-04 0.00657  3.85554E-01 0.00053 ];
INF_S2                    (idx, [1:   8]) = [  9.72211E-02 0.00036 -1.54651E-03 0.00271  2.95094E-04 0.00927  9.18240E-02 0.00091 ];
INF_S3                    (idx, [1:   8]) = [  9.13448E-03 0.00303 -1.81441E-03 0.00224  1.06519E-04 0.01987  2.76616E-02 0.00264 ];
INF_S4                    (idx, [1:   8]) = [ -9.63017E-03 0.00182 -5.94646E-04 0.00393  2.16772E-06 0.82781 -8.23628E-03 0.00614 ];
INF_S5                    (idx, [1:   8]) = [  1.64815E-04 0.12746  2.36889E-05 0.08558 -4.24036E-05 0.05009  6.47341E-03 0.00788 ];
INF_S6                    (idx, [1:   8]) = [  5.21378E-03 0.00391 -1.38729E-04 0.01723 -5.54866E-05 0.02834 -1.62167E-02 0.00279 ];
INF_S7                    (idx, [1:   8]) = [  9.23346E-04 0.01930 -1.67149E-04 0.01121 -4.86500E-05 0.03861  3.38188E-04 0.13573 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.09956E-01 0.00011  1.76346E-02 0.00041  1.27417E-03 0.00446  1.44881E+00 0.00043 ];
INF_SP1                   (idx, [1:   8]) = [  2.37359E-01 0.00022  5.12574E-03 0.00118  5.39261E-04 0.00657  3.85554E-01 0.00053 ];
INF_SP2                   (idx, [1:   8]) = [  9.72215E-02 0.00036 -1.54651E-03 0.00271  2.95094E-04 0.00927  9.18240E-02 0.00091 ];
INF_SP3                   (idx, [1:   8]) = [  9.13444E-03 0.00303 -1.81441E-03 0.00224  1.06519E-04 0.01987  2.76616E-02 0.00264 ];
INF_SP4                   (idx, [1:   8]) = [ -9.63002E-03 0.00182 -5.94646E-04 0.00393  2.16772E-06 0.82781 -8.23628E-03 0.00614 ];
INF_SP5                   (idx, [1:   8]) = [  1.64764E-04 0.12746  2.36889E-05 0.08558 -4.24036E-05 0.05009  6.47341E-03 0.00788 ];
INF_SP6                   (idx, [1:   8]) = [  5.21379E-03 0.00391 -1.38729E-04 0.01723 -5.54866E-05 0.02834 -1.62167E-02 0.00279 ];
INF_SP7                   (idx, [1:   8]) = [  9.23244E-04 0.01933 -1.67149E-04 0.01121 -4.86500E-05 0.03861  3.38188E-04 0.13573 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31637E-01 0.00063  1.10467E+00 0.00724 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33188E-01 0.00088  1.17412E+00 0.00938 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.32734E-01 0.00091  1.18126E+00 0.00882 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29039E-01 0.00082  9.84070E-01 0.00746 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43905E+00 0.00063  3.02123E-01 0.00711 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42949E+00 0.00088  2.84487E-01 0.00919 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.43228E+00 0.00091  2.82708E-01 0.00876 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45538E+00 0.00082  3.39173E-01 0.00731 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.20370E-03 0.00844  1.74666E-04 0.04899  1.03392E-03 0.02161  9.80404E-04 0.02084  2.79212E-03 0.01223  9.41662E-04 0.02106  2.80926E-04 0.03963 ];
LAMBDA                    (idx, [1:  14]) = [  7.74655E-01 0.01939  1.24931E-02 0.00019  3.14317E-02 0.00046  1.10125E-01 0.00047  3.21507E-01 0.00037  1.34119E+00 0.00061  8.90990E+00 0.00406 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0010' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:42:04 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:32:03 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292124 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00095E+00  9.99680E-01  1.00213E+00  9.97507E-01  9.99736E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.9E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15196E-02 0.00112  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88480E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.01965E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.02407E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67142E+00 0.00020  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.37544E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.37473E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.30967E+00 0.00036  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.02330E-01 0.00118  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000725 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00015E+04 0.00080 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00015E+04 0.00080 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.47729E+02 ;
RUNNING_TIME              (idx, 1)        =  4.99774E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.78167E-01  4.78167E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.47650E-01  1.02333E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  4.89088E+01  3.20520E+00  2.51752E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  4.38383E-01  2.75500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.22500E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  4.99770E+01  1.19292E+02 ];
CPU_USAGE                 (idx, 1)        = 4.95682 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99999E+00 0.00022 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.75187E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6133.74;
MEMSIZE                   (idx, 1)        = 6039.76;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 169.50;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.98;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.20182E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.80627E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.30557E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.58843E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.10852E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.61336E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.69540E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  8.27910E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.21524E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.96801E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.64367E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.31109E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.55088E+07 ;
SR90_ACTIVITY             (idx, 1)        =  7.09860E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.20832E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.25991E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.30816E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.85220E+12 ;
CS137_ACTIVITY            (idx, 1)        =  8.09339E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  7.98983E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.48845E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.62908E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.19532E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.25788E+12 0.00047  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 8 ;
BURNUP                     (idx, [1:  2])  = [  6.00000E+00  6.00052E+00 ];
BURN_DAYS                 (idx, 1)        =  1.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.10480E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  1.01414E+16 0.00069  7.35109E-01 0.00041 ];
U238_FISS                 (idx, [1:   4]) = [  9.08801E+14 0.00269  6.58674E-02 0.00253 ];
PU239_FISS                (idx, [1:   4]) = [  2.62346E+15 0.00153  1.90164E-01 0.00142 ];
PU240_FISS                (idx, [1:   4]) = [  1.73363E+12 0.05973  1.25772E-04 0.05973 ];
PU241_FISS                (idx, [1:   4]) = [  1.14278E+14 0.00735  8.28255E-03 0.00729 ];
U235_CAPT                 (idx, [1:   4]) = [  2.26821E+15 0.00168  1.21944E-01 0.00158 ];
U238_CAPT                 (idx, [1:   4]) = [  7.88781E+15 0.00103  4.24040E-01 0.00068 ];
PU239_CAPT                (idx, [1:   4]) = [  1.46996E+15 0.00210  7.90339E-02 0.00208 ];
PU240_CAPT                (idx, [1:   4]) = [  5.65766E+14 0.00356  3.04154E-02 0.00349 ];
PU241_CAPT                (idx, [1:   4]) = [  4.12041E+13 0.01295  2.21497E-03 0.01292 ];
XE135_CAPT                (idx, [1:   4]) = [  7.12669E+14 0.00293  3.83163E-02 0.00290 ];
SM149_CAPT                (idx, [1:   4]) = [  1.78052E+14 0.00610  9.57435E-03 0.00614 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000725 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.77686E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000725 5.00778E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2850609 2.85471E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2114446 2.11739E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 35670 3.56731E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000725 5.00778E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -3.81842E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50055E+05 2.0E-09  4.50055E+05 2.0E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.51377E+16 1.6E-05  3.51377E+16 1.6E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37884E+16 2.7E-06  1.37884E+16 2.7E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.86037E+16 0.00045  1.39961E+16 0.00045  4.60763E+15 0.00119 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.23921E+16 0.00026  2.77845E+16 0.00023  4.60763E+15 0.00119 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.25788E+16 0.00047  3.25788E+16 0.00047  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.51127E+18 0.00046  4.11469E+17 0.00046  1.09980E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.32457E+14 0.00554 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.26246E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.10101E+18 0.00061 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11815E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11815E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.65474E+00 0.00047 ];
SIX_FF_F                  (idx, [1:   2]) = [  8.05639E-01 0.00030 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.48533E-01 0.00033 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25724E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95338E-01 3.1E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97516E-01 2.2E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.08691E+00 0.00053 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.07916E+00 0.00054 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.54834E+00 1.8E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03723E+02 2.7E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.07900E+00 0.00055  1.07258E+00 0.00054  6.58168E-03 0.00893 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.07873E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.07866E+00 0.00047 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.07873E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.08649E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72767E+01 0.00019 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72765E+01 8.7E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.29608E-07 0.00337 ];
IMP_EALF                  (idx, [1:   2]) = [  6.28338E-07 0.00151 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.25960E-01 0.00277 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.26607E-01 0.00118 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.77293E-03 0.00612  1.69011E-04 0.03155  9.54767E-04 0.01434  8.95926E-04 0.01452  2.61720E-03 0.00867  8.60825E-04 0.01553  2.75205E-04 0.02515 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.93778E-01 0.01295  1.06186E-02 0.01881  3.13620E-02 0.00037  1.10196E-01 0.00038  3.21297E-01 0.00026  1.33846E+00 0.00060  8.47066E+00 0.01095 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.06259E-03 0.00890  1.73754E-04 0.04614  1.01998E-03 0.02085  9.30539E-04 0.02167  2.75756E-03 0.01306  8.97016E-04 0.02410  2.83744E-04 0.03978 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.84308E-01 0.02004  1.24929E-02 0.00017  3.13691E-02 0.00051  1.10229E-01 0.00055  3.21291E-01 0.00036  1.33988E+00 0.00060  8.94477E+00 0.00370 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.19874E-05 0.00126  2.19812E-05 0.00126  2.30803E-05 0.01325 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.37210E-05 0.00114  2.37143E-05 0.00114  2.49012E-05 0.01324 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.12078E-03 0.00906  1.82160E-04 0.05076  1.02223E-03 0.02127  9.31758E-04 0.02193  2.76061E-03 0.01341  9.28579E-04 0.02372  2.95445E-04 0.04065 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.00790E-01 0.02113  1.24956E-02 0.00035  3.13800E-02 0.00061  1.10255E-01 0.00068  3.21432E-01 0.00041  1.33962E+00 0.00087  8.97600E+00 0.00394 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.12436E-05 0.00280  2.12375E-05 0.00281  2.24644E-05 0.03210 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.29189E-05 0.00276  2.29122E-05 0.00277  2.42409E-05 0.03206 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.67392E-03 0.02960  1.79859E-04 0.16665  9.21147E-04 0.07330  8.82940E-04 0.08102  2.47527E-03 0.04339  8.99258E-04 0.07899  3.15443E-04 0.12386 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.63595E-01 0.07252  1.25072E-02 0.00141  3.13192E-02 0.00149  1.10322E-01 0.00159  3.21344E-01 0.00121  1.33610E+00 0.00314  9.14924E+00 0.00818 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.70313E-03 0.02911  1.91753E-04 0.15938  9.13775E-04 0.07069  8.88211E-04 0.07971  2.49083E-03 0.04233  8.98177E-04 0.07571  3.20388E-04 0.12423 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.48321E-01 0.07045  1.25072E-02 0.00141  3.13228E-02 0.00148  1.10313E-01 0.00159  3.21357E-01 0.00120  1.33619E+00 0.00313  9.15954E+00 0.00772 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.67581E+02 0.02958 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.16817E-05 0.00080 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.33910E-05 0.00057 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.92401E-03 0.00517 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.73349E+02 0.00528 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.36435E-07 0.00076 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.80757E-06 0.00042  2.80740E-06 0.00042  2.82881E-06 0.00552 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.51757E-05 0.00093  3.51942E-05 0.00093  3.23128E-05 0.01067 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.46833E-01 0.00033  6.46420E-01 0.00033  7.33274E-01 0.01016 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06167E+01 0.01318 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.37473E+01 0.00049  3.27972E+01 0.00046 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.52532E+04 0.00346  2.96803E+05 0.00124  6.08370E+05 0.00099  6.51546E+05 0.00093  6.00472E+05 0.00060  6.44655E+05 0.00062  4.37325E+05 0.00083  3.87801E+05 0.00058  2.96614E+05 0.00073  2.42179E+05 0.00076  2.09104E+05 0.00086  1.88440E+05 0.00079  1.73922E+05 0.00106  1.65458E+05 0.00095  1.61286E+05 0.00084  1.39101E+05 0.00104  1.37419E+05 0.00073  1.36266E+05 0.00113  1.33821E+05 0.00106  2.60749E+05 0.00062  2.51146E+05 0.00066  1.81188E+05 0.00084  1.17034E+05 0.00088  1.34841E+05 0.00098  1.27573E+05 0.00077  1.14648E+05 0.00093  1.87127E+05 0.00095  4.31377E+04 0.00169  5.40090E+04 0.00173  4.90740E+04 0.00190  2.84403E+04 0.00175  4.94846E+04 0.00132  3.34361E+04 0.00145  2.81587E+04 0.00245  5.18310E+03 0.00365  4.93098E+03 0.00429  4.64682E+03 0.00299  4.57410E+03 0.00300  4.63300E+03 0.00312  4.87435E+03 0.00423  5.32562E+03 0.00384  5.10545E+03 0.00472  9.78383E+03 0.00304  1.57394E+04 0.00229  2.02690E+04 0.00191  5.35432E+04 0.00176  5.61061E+04 0.00112  6.00215E+04 0.00125  3.91020E+04 0.00151  2.78180E+04 0.00155  2.08762E+04 0.00218  2.43938E+04 0.00147  4.67404E+04 0.00130  6.44514E+04 0.00116  1.25159E+05 0.00124  1.92698E+05 0.00139  2.86431E+05 0.00126  1.82339E+05 0.00135  1.30100E+05 0.00155  9.33810E+04 0.00136  8.37865E+04 0.00161  8.25819E+04 0.00175  6.88980E+04 0.00179  4.65382E+04 0.00173  4.29995E+04 0.00152  3.82391E+04 0.00194  3.22071E+04 0.00194  2.54246E+04 0.00160  1.70111E+04 0.00185  6.02027E+03 0.00183 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.08641E+00 0.00050 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.21918E+18 0.00049  2.92122E+17 0.00125 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37059E-01 0.00013  1.52766E+00 0.00035 ];
INF_CAPT                  (idx, [1:   4]) = [  7.09909E-03 0.00069  3.40597E-02 0.00063 ];
INF_ABS                   (idx, [1:   4]) = [  9.36706E-03 0.00055  7.18017E-02 0.00096 ];
INF_FISS                  (idx, [1:   4]) = [  2.26797E-03 0.00060  3.77420E-02 0.00126 ];
INF_NSF                   (idx, [1:   4]) = [  5.89016E-03 0.00059  9.57179E-02 0.00129 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.59711E+00 5.9E-05  2.53611E+00 3.0E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04269E+02 6.5E-06  2.03586E+02 5.1E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.72662E-08 0.00048  2.53641E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.27693E-01 0.00013  1.45585E+00 0.00042 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42650E-01 0.00023  3.87680E-01 0.00055 ];
INF_SCATT2                (idx, [1:   4]) = [  9.57923E-02 0.00034  9.25384E-02 0.00119 ];
INF_SCATT3                (idx, [1:   4]) = [  7.34063E-03 0.00238  2.77378E-02 0.00240 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.01932E-02 0.00179 -8.45406E-03 0.00688 ];
INF_SCATT5                (idx, [1:   4]) = [  1.86928E-04 0.11490  6.33083E-03 0.00620 ];
INF_SCATT6                (idx, [1:   4]) = [  5.06324E-03 0.00329 -1.64145E-02 0.00328 ];
INF_SCATT7                (idx, [1:   4]) = [  7.53524E-04 0.01807  2.96267E-04 0.17841 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.27734E-01 0.00013  1.45585E+00 0.00042 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42651E-01 0.00023  3.87680E-01 0.00055 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.57923E-02 0.00034  9.25384E-02 0.00119 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.34047E-03 0.00238  2.77378E-02 0.00240 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.01933E-02 0.00179 -8.45406E-03 0.00688 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.86727E-04 0.11518  6.33083E-03 0.00620 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.06324E-03 0.00329 -1.64145E-02 0.00328 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.53667E-04 0.01812  2.96267E-04 0.17841 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13503E-01 0.00034  9.86767E-01 0.00030 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56127E+00 0.00034  3.37804E-01 0.00030 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.32549E-03 0.00055  7.18017E-02 0.00096 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69422E-02 0.00021  7.30791E-02 0.00100 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10117E-01 0.00013  1.75758E-02 0.00044  1.26576E-03 0.00409  1.45458E+00 0.00042 ];
INF_S1                    (idx, [1:   8]) = [  2.37533E-01 0.00023  5.11676E-03 0.00105  5.39419E-04 0.00619  3.87141E-01 0.00055 ];
INF_S2                    (idx, [1:   8]) = [  9.73358E-02 0.00032 -1.54354E-03 0.00326  2.96205E-04 0.00875  9.22422E-02 0.00119 ];
INF_S3                    (idx, [1:   8]) = [  9.15118E-03 0.00187 -1.81055E-03 0.00232  1.05272E-04 0.02225  2.76325E-02 0.00239 ];
INF_S4                    (idx, [1:   8]) = [ -9.60489E-03 0.00185 -5.88302E-04 0.00484 -7.43935E-07 1.00000 -8.45332E-03 0.00689 ];
INF_S5                    (idx, [1:   8]) = [  1.62145E-04 0.13244  2.47830E-05 0.08572 -4.30697E-05 0.03110  6.37390E-03 0.00613 ];
INF_S6                    (idx, [1:   8]) = [  5.20472E-03 0.00308 -1.41472E-04 0.01995 -5.64558E-05 0.02512 -1.63580E-02 0.00330 ];
INF_S7                    (idx, [1:   8]) = [  9.18114E-04 0.01518 -1.64590E-04 0.01428 -5.01779E-05 0.03128  3.46444E-04 0.15311 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10158E-01 0.00013  1.75758E-02 0.00044  1.26576E-03 0.00409  1.45458E+00 0.00042 ];
INF_SP1                   (idx, [1:   8]) = [  2.37534E-01 0.00023  5.11676E-03 0.00105  5.39419E-04 0.00619  3.87141E-01 0.00055 ];
INF_SP2                   (idx, [1:   8]) = [  9.73359E-02 0.00032 -1.54354E-03 0.00326  2.96205E-04 0.00875  9.22422E-02 0.00119 ];
INF_SP3                   (idx, [1:   8]) = [  9.15101E-03 0.00188 -1.81055E-03 0.00232  1.05272E-04 0.02225  2.76325E-02 0.00239 ];
INF_SP4                   (idx, [1:   8]) = [ -9.60500E-03 0.00185 -5.88302E-04 0.00484 -7.43935E-07 1.00000 -8.45332E-03 0.00689 ];
INF_SP5                   (idx, [1:   8]) = [  1.61944E-04 0.13276  2.47830E-05 0.08572 -4.30697E-05 0.03110  6.37390E-03 0.00613 ];
INF_SP6                   (idx, [1:   8]) = [  5.20471E-03 0.00308 -1.41472E-04 0.01995 -5.64558E-05 0.02512 -1.63580E-02 0.00330 ];
INF_SP7                   (idx, [1:   8]) = [  9.18257E-04 0.01522 -1.64590E-04 0.01428 -5.01779E-05 0.03128  3.46444E-04 0.15311 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31562E-01 0.00069  1.12095E+00 0.00737 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.32942E-01 0.00112  1.20345E+00 0.00890 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33030E-01 0.00099  1.19568E+00 0.01047 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28775E-01 0.00103  9.92368E-01 0.00653 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43952E+00 0.00069  2.97750E-01 0.00730 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.43101E+00 0.00112  2.77503E-01 0.00881 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.43047E+00 0.00099  2.79504E-01 0.01029 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45707E+00 0.00103  3.36241E-01 0.00654 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.06259E-03 0.00890  1.73754E-04 0.04614  1.01998E-03 0.02085  9.30539E-04 0.02167  2.75756E-03 0.01306  8.97016E-04 0.02410  2.83744E-04 0.03978 ];
LAMBDA                    (idx, [1:  14]) = [  7.84308E-01 0.02004  1.24929E-02 0.00017  3.13691E-02 0.00051  1.10229E-01 0.00055  3.21291E-01 0.00036  1.33988E+00 0.00060  8.94477E+00 0.00370 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0010' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:42:04 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:37:50 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292124 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00009E+00  1.00099E+00  1.00313E+00  9.97202E-01  9.98593E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.5E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14945E-02 0.00111  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88506E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.02451E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.02892E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.66932E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.37243E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.37171E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.27691E+00 0.00036  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.00775E-01 0.00115  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000663 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00013E+04 0.00079 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00013E+04 0.00079 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  2.76665E+02 ;
RUNNING_TIME              (idx, 1)        =  5.57704E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.78167E-01  4.78167E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.67783E-01  9.48333E-03 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  5.46281E+01  3.19877E+00  2.52050E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  4.91600E-01  2.52500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.48167E-02  7.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  5.57703E+01  1.19517E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96078 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99934E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76162E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6133.74;
MEMSIZE                   (idx, 1)        = 6039.76;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 169.50;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.98;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.23367E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.79859E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.73482E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.60663E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.12119E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62702E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.68645E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  9.21009E+07 ;
INGESTION_TOXICITY        (idx, 1)        =  4.27418E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.60171E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.73661E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.60838E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.60052E+07 ;
SR90_ACTIVITY             (idx, 1)        =  8.18369E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.22747E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.28606E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.33402E+14 ;
CS134_ACTIVITY            (idx, 1)        =  3.89212E+12 ;
CS137_ACTIVITY            (idx, 1)        =  9.43976E+12 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.01461E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.47019E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.35988E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.20270E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.30479E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 9 ;
BURNUP                     (idx, [1:  2])  = [  7.00000E+00  7.00061E+00 ];
BURN_DAYS                 (idx, 1)        =  1.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.19906E-01 0.00112 ];
U235_FISS                 (idx, [1:   4]) = [  9.79657E+15 0.00072  7.10624E-01 0.00044 ];
U238_FISS                 (idx, [1:   4]) = [  9.23534E+14 0.00258  6.69858E-02 0.00245 ];
PU239_FISS                (idx, [1:   4]) = [  2.90105E+15 0.00139  2.10444E-01 0.00132 ];
PU240_FISS                (idx, [1:   4]) = [  2.38726E+12 0.05449  1.73014E-04 0.05436 ];
PU241_FISS                (idx, [1:   4]) = [  1.55440E+14 0.00667  1.12753E-02 0.00664 ];
U235_CAPT                 (idx, [1:   4]) = [  2.19255E+15 0.00178  1.14952E-01 0.00168 ];
U238_CAPT                 (idx, [1:   4]) = [  7.96926E+15 0.00108  4.17788E-01 0.00073 ];
PU239_CAPT                (idx, [1:   4]) = [  1.62482E+15 0.00200  8.51921E-02 0.00196 ];
PU240_CAPT                (idx, [1:   4]) = [  6.77063E+14 0.00320  3.54941E-02 0.00307 ];
PU241_CAPT                (idx, [1:   4]) = [  5.62135E+13 0.01095  2.94818E-03 0.01098 ];
XE135_CAPT                (idx, [1:   4]) = [  7.17576E+14 0.00289  3.76236E-02 0.00286 ];
SM149_CAPT                (idx, [1:   4]) = [  1.82901E+14 0.00647  9.58985E-03 0.00646 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000663 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.77035E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000663 5.00777E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2881550 2.88577E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2082968 2.08585E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 36145 3.61459E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000663 5.00777E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -5.58794E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50055E+05 2.0E-09  4.50055E+05 2.0E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.52614E+16 1.7E-05  3.52614E+16 1.7E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37785E+16 2.8E-06  1.37785E+16 2.8E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.90777E+16 0.00046  1.44090E+16 0.00047  4.66868E+15 0.00119 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.28562E+16 0.00027  2.81875E+16 0.00024  4.66868E+15 0.00119 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.30479E+16 0.00049  3.30479E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.53140E+18 0.00049  4.16389E+17 0.00046  1.11501E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.38930E+14 0.00539 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.30952E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.11588E+18 0.00063 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11698E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11698E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.64479E+00 0.00045 ];
SIX_FF_F                  (idx, [1:   2]) = [  8.05089E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.46398E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25636E+00 0.00034 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95316E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97443E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.07533E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.06756E+00 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.55916E+00 1.9E-05 ];
FISSE                     (idx, [1:   2]) = [  2.03870E+02 2.8E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.06746E+00 0.00057  1.06114E+00 0.00055  6.42192E-03 0.00913 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.06716E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.06710E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.06716E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.07493E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72568E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72563E+01 8.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.42315E-07 0.00342 ];
IMP_EALF                  (idx, [1:   2]) = [  6.41155E-07 0.00154 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.29624E-01 0.00271 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.30177E-01 0.00115 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.75467E-03 0.00585  1.60387E-04 0.03326  9.65732E-04 0.01419  8.87264E-04 0.01501  2.58954E-03 0.00841  8.63483E-04 0.01495  2.88264E-04 0.02658 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.11783E-01 0.01382  1.02988E-02 0.02069  3.13483E-02 0.00037  1.10202E-01 0.00038  3.21364E-01 0.00025  1.33784E+00 0.00067  8.44457E+00 0.01142 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  6.01186E-03 0.00868  1.63255E-04 0.04966  1.01628E-03 0.02011  9.09405E-04 0.02218  2.72263E-03 0.01280  9.01118E-04 0.02213  2.99169E-04 0.03741 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  8.06673E-01 0.01927  1.25003E-02 0.00037  3.13332E-02 0.00052  1.10186E-01 0.00051  3.21422E-01 0.00038  1.33593E+00 0.00101  8.90076E+00 0.00469 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.21986E-05 0.00121  2.21909E-05 0.00122  2.35094E-05 0.01310 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.36923E-05 0.00108  2.36840E-05 0.00108  2.50969E-05 0.01312 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  6.00969E-03 0.00924  1.67265E-04 0.05477  1.02619E-03 0.02137  9.27642E-04 0.02391  2.68816E-03 0.01343  9.05470E-04 0.02338  2.94964E-04 0.03993 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.07606E-01 0.02150  1.25014E-02 0.00055  3.13366E-02 0.00062  1.10225E-01 0.00062  3.21415E-01 0.00040  1.33685E+00 0.00108  8.92989E+00 0.00547 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.15708E-05 0.00297  2.15654E-05 0.00298  2.17015E-05 0.03158 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.30216E-05 0.00290  2.30158E-05 0.00291  2.31664E-05 0.03164 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.65101E-03 0.02994  1.62949E-04 0.17906  9.86249E-04 0.07037  8.47223E-04 0.07202  2.60474E-03 0.04522  8.06778E-04 0.07893  2.43069E-04 0.14176 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.41032E-01 0.07361  1.24893E-02 3.8E-05  3.12867E-02 0.00151  1.10120E-01 0.00148  3.21919E-01 0.00129  1.33275E+00 0.00339  8.92000E+00 0.01401 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.67449E-03 0.02872  1.65605E-04 0.18130  9.84010E-04 0.06733  8.67935E-04 0.06938  2.62955E-03 0.04400  7.80627E-04 0.07584  2.46755E-04 0.14290 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.49410E-01 0.07364  1.24893E-02 3.9E-05  3.12898E-02 0.00150  1.10115E-01 0.00147  3.21881E-01 0.00127  1.33316E+00 0.00333  8.91665E+00 0.01392 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.63658E+02 0.03044 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.19144E-05 0.00080 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.33888E-05 0.00055 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.98494E-03 0.00560 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.73228E+02 0.00569 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.35276E-07 0.00074 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.79302E-06 0.00043  2.79293E-06 0.00043  2.80760E-06 0.00538 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.51842E-05 0.00092  3.52044E-05 0.00092  3.20667E-05 0.01027 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.44738E-01 0.00035  6.44388E-01 0.00035  7.17224E-01 0.00922 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05336E+01 0.01391 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.37171E+01 0.00046  3.27288E+01 0.00045 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.56808E+04 0.00262  2.97593E+05 0.00153  6.09185E+05 0.00114  6.51638E+05 0.00083  6.00769E+05 0.00068  6.44132E+05 0.00066  4.37508E+05 0.00058  3.88180E+05 0.00052  2.96303E+05 0.00074  2.42315E+05 0.00086  2.08886E+05 0.00077  1.88320E+05 0.00071  1.73925E+05 0.00104  1.64949E+05 0.00085  1.61165E+05 0.00067  1.38963E+05 0.00083  1.37279E+05 0.00126  1.36103E+05 0.00103  1.33531E+05 0.00080  2.60924E+05 0.00085  2.51435E+05 0.00071  1.80864E+05 0.00068  1.17012E+05 0.00121  1.34897E+05 0.00094  1.27545E+05 0.00091  1.14416E+05 0.00110  1.86838E+05 0.00051  4.29989E+04 0.00147  5.39377E+04 0.00113  4.90100E+04 0.00135  2.84295E+04 0.00231  4.94593E+04 0.00190  3.33150E+04 0.00177  2.80007E+04 0.00184  5.11267E+03 0.00342  4.79069E+03 0.00319  4.47799E+03 0.00298  4.40969E+03 0.00391  4.48356E+03 0.00308  4.72654E+03 0.00418  5.24098E+03 0.00438  5.03103E+03 0.00292  9.70887E+03 0.00240  1.57047E+04 0.00251  2.01131E+04 0.00203  5.34314E+04 0.00110  5.59552E+04 0.00106  5.95271E+04 0.00129  3.86513E+04 0.00139  2.75021E+04 0.00177  2.05087E+04 0.00164  2.39951E+04 0.00152  4.60861E+04 0.00166  6.38210E+04 0.00147  1.23976E+05 0.00122  1.91684E+05 0.00147  2.85039E+05 0.00147  1.81734E+05 0.00165  1.29931E+05 0.00161  9.34469E+04 0.00151  8.38589E+04 0.00179  8.24680E+04 0.00157  6.87730E+04 0.00181  4.64518E+04 0.00163  4.29134E+04 0.00166  3.80965E+04 0.00192  3.22526E+04 0.00207  2.54569E+04 0.00162  1.70109E+04 0.00221  6.00945E+03 0.00251 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.07487E+00 0.00060 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.23644E+18 0.00066  2.94978E+17 0.00145 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37073E-01 0.00011  1.53173E+00 0.00042 ];
INF_CAPT                  (idx, [1:   4]) = [  7.19322E-03 0.00081  3.45270E-02 0.00081 ];
INF_ABS                   (idx, [1:   4]) = [  9.42437E-03 0.00065  7.18923E-02 0.00120 ];
INF_FISS                  (idx, [1:   4]) = [  2.23115E-03 0.00046  3.73653E-02 0.00156 ];
INF_NSF                   (idx, [1:   4]) = [  5.81014E-03 0.00047  9.52034E-02 0.00159 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.60410E+00 5.9E-05  2.54791E+00 4.2E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04361E+02 5.2E-06  2.03747E+02 7.1E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.70816E-08 0.00044  2.53983E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.27649E-01 0.00011  1.45983E+00 0.00049 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42632E-01 0.00019  3.88705E-01 0.00057 ];
INF_SCATT2                (idx, [1:   4]) = [  9.57556E-02 0.00031  9.26735E-02 0.00083 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32460E-03 0.00281  2.78504E-02 0.00256 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02137E-02 0.00164 -8.41193E-03 0.00705 ];
INF_SCATT5                (idx, [1:   4]) = [  1.70734E-04 0.12772  6.49091E-03 0.00881 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07038E-03 0.00344 -1.64268E-02 0.00253 ];
INF_SCATT7                (idx, [1:   4]) = [  7.57525E-04 0.02152  3.19616E-04 0.14027 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.27690E-01 0.00011  1.45983E+00 0.00049 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42633E-01 0.00019  3.88705E-01 0.00057 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.57556E-02 0.00031  9.26735E-02 0.00083 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32445E-03 0.00281  2.78504E-02 0.00256 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02136E-02 0.00163 -8.41193E-03 0.00705 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.70821E-04 0.12749  6.49091E-03 0.00881 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07039E-03 0.00344 -1.64268E-02 0.00253 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.57520E-04 0.02152  3.19616E-04 0.14027 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13371E-01 0.00036  9.90021E-01 0.00034 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56223E+00 0.00036  3.36694E-01 0.00034 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.38283E-03 0.00065  7.18923E-02 0.00120 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69492E-02 0.00025  7.31754E-02 0.00115 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10124E-01 0.00011  1.75248E-02 0.00043  1.27699E-03 0.00385  1.45855E+00 0.00050 ];
INF_S1                    (idx, [1:   8]) = [  2.37532E-01 0.00019  5.09967E-03 0.00091  5.46217E-04 0.00706  3.88159E-01 0.00057 ];
INF_S2                    (idx, [1:   8]) = [  9.73012E-02 0.00031 -1.54564E-03 0.00207  2.97186E-04 0.00900  9.23763E-02 0.00083 ];
INF_S3                    (idx, [1:   8]) = [  9.12707E-03 0.00224 -1.80247E-03 0.00133  1.06517E-04 0.01709  2.77438E-02 0.00257 ];
INF_S4                    (idx, [1:   8]) = [ -9.62722E-03 0.00168 -5.86439E-04 0.00452  7.61175E-07 1.00000 -8.41269E-03 0.00708 ];
INF_S5                    (idx, [1:   8]) = [  1.45141E-04 0.14585  2.55933E-05 0.08968 -4.26576E-05 0.03352  6.53356E-03 0.00882 ];
INF_S6                    (idx, [1:   8]) = [  5.20870E-03 0.00330 -1.38312E-04 0.01460 -5.47786E-05 0.02950 -1.63720E-02 0.00248 ];
INF_S7                    (idx, [1:   8]) = [  9.28403E-04 0.01751 -1.70879E-04 0.01262 -5.01750E-05 0.02930  3.69791E-04 0.12008 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10165E-01 0.00011  1.75248E-02 0.00043  1.27699E-03 0.00385  1.45855E+00 0.00050 ];
INF_SP1                   (idx, [1:   8]) = [  2.37533E-01 0.00019  5.09967E-03 0.00091  5.46217E-04 0.00706  3.88159E-01 0.00057 ];
INF_SP2                   (idx, [1:   8]) = [  9.73012E-02 0.00031 -1.54564E-03 0.00207  2.97186E-04 0.00900  9.23763E-02 0.00083 ];
INF_SP3                   (idx, [1:   8]) = [  9.12692E-03 0.00224 -1.80247E-03 0.00133  1.06517E-04 0.01709  2.77438E-02 0.00257 ];
INF_SP4                   (idx, [1:   8]) = [ -9.62717E-03 0.00167 -5.86439E-04 0.00452  7.61175E-07 1.00000 -8.41269E-03 0.00708 ];
INF_SP5                   (idx, [1:   8]) = [  1.45228E-04 0.14556  2.55933E-05 0.08968 -4.26576E-05 0.03352  6.53356E-03 0.00882 ];
INF_SP6                   (idx, [1:   8]) = [  5.20870E-03 0.00330 -1.38312E-04 0.01460 -5.47786E-05 0.02950 -1.63720E-02 0.00248 ];
INF_SP7                   (idx, [1:   8]) = [  9.28398E-04 0.01751 -1.70879E-04 0.01262 -5.01750E-05 0.02930  3.69791E-04 0.12008 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31642E-01 0.00060  1.13116E+00 0.00758 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33044E-01 0.00098  1.20935E+00 0.00865 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33166E-01 0.00075  1.21200E+00 0.00896 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28777E-01 0.00089  1.00094E+00 0.00840 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43901E+00 0.00060  2.95099E-01 0.00777 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.43038E+00 0.00098  2.76143E-01 0.00896 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42961E+00 0.00076  2.75562E-01 0.00903 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45705E+00 0.00088  3.33592E-01 0.00851 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  6.01186E-03 0.00868  1.63255E-04 0.04966  1.01628E-03 0.02011  9.09405E-04 0.02218  2.72263E-03 0.01280  9.01118E-04 0.02213  2.99169E-04 0.03741 ];
LAMBDA                    (idx, [1:  14]) = [  8.06673E-01 0.01927  1.25003E-02 0.00037  3.13332E-02 0.00052  1.10186E-01 0.00051  3.21422E-01 0.00038  1.33593E+00 0.00101  8.90076E+00 0.00469 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0010' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:42:04 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:43:35 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292124 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99335E-01  1.00060E+00  1.00362E+00  9.96521E-01  9.99932E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.5E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14927E-02 0.00116  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88507E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.02968E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.03409E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.66694E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.37301E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.37227E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.25133E+00 0.00036  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.00435E-01 0.00121  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000859 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00017E+04 0.00084 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00017E+04 0.00084 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.05337E+02 ;
RUNNING_TIME              (idx, 1)        =  6.15111E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.78167E-01  4.78167E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  1.88733E-01  1.04667E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.02943E+01  3.18288E+00  2.48338E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.44700E-01  2.70833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.72833E-02  8.49998E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  6.15110E+01  1.19084E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96393 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00020E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.76936E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6133.74;
MEMSIZE                   (idx, 1)        = 6039.76;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 169.50;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.98;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.26171E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.79167E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.38204E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.62459E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.13372E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.63709E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.67827E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.01779E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.32584E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  4.30590E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.82995E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  5.87199E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.64285E+07 ;
SR90_ACTIVITY             (idx, 1)        =  9.24616E+12 ;
TE132_ACTIVITY            (idx, 1)        =  6.24507E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.30988E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.35774E+14 ;
CS134_ACTIVITY            (idx, 1)        =  5.08082E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.07850E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.03729E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.45368E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.36171E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.20952E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.35403E+12 0.00048  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 10 ;
BURNUP                     (idx, [1:  2])  = [  8.00000E+00  8.00070E+00 ];
BURN_DAYS                 (idx, 1)        =  2.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.30261E-01 0.00109 ];
U233_FISS                 (idx, [1:   4]) = [  6.88573E+09 1.00000  4.89476E-07 1.00000 ];
U235_FISS                 (idx, [1:   4]) = [  9.47240E+15 0.00073  6.87737E-01 0.00045 ];
U238_FISS                 (idx, [1:   4]) = [  9.34364E+14 0.00268  6.78307E-02 0.00253 ];
PU239_FISS                (idx, [1:   4]) = [  3.15484E+15 0.00137  2.29057E-01 0.00126 ];
PU240_FISS                (idx, [1:   4]) = [  2.98852E+12 0.04935  2.16954E-04 0.04937 ];
PU241_FISS                (idx, [1:   4]) = [  2.01140E+14 0.00592  1.46030E-02 0.00587 ];
U235_CAPT                 (idx, [1:   4]) = [  2.12584E+15 0.00168  1.08631E-01 0.00162 ];
U238_CAPT                 (idx, [1:   4]) = [  8.06652E+15 0.00104  4.12163E-01 0.00071 ];
PU239_CAPT                (idx, [1:   4]) = [  1.76430E+15 0.00181  9.01597E-02 0.00179 ];
PU240_CAPT                (idx, [1:   4]) = [  7.87165E+14 0.00289  4.02212E-02 0.00280 ];
PU241_CAPT                (idx, [1:   4]) = [  7.20038E+13 0.00970  3.67914E-03 0.00968 ];
XE135_CAPT                (idx, [1:   4]) = [  7.17592E+14 0.00306  3.66711E-02 0.00306 ];
SM149_CAPT                (idx, [1:   4]) = [  1.87014E+14 0.00605  9.55483E-03 0.00597 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000859 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.76707E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000859 5.00777E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2913221 2.91742E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2050650 2.05335E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 36988 3.69949E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000859 5.00777E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.65661E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50055E+05 2.0E-09  4.50055E+05 2.0E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.53765E+16 1.6E-05  3.53765E+16 1.6E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37691E+16 2.9E-06  1.37691E+16 2.9E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  1.95779E+16 0.00046  1.48330E+16 0.00047  4.74493E+15 0.00114 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.33471E+16 0.00027  2.86021E+16 0.00024  4.74493E+15 0.00114 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.35403E+16 0.00048  3.35403E+16 0.00048  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.55286E+18 0.00047  4.21291E+17 0.00045  1.13156E+18 0.00052 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.48208E+14 0.00549 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.35953E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.13268E+18 0.00060 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11582E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11582E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.63580E+00 0.00047 ];
SIX_FF_F                  (idx, [1:   2]) = [  8.03958E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.43982E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25519E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95167E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97421E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.06296E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.05510E+00 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.56926E+00 1.9E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04009E+02 2.9E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.05487E+00 0.00058  1.04886E+00 0.00056  6.23403E-03 0.00882 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.05470E+00 0.00028 ];
COL_KEFF                  (idx, [1:   2]) = [  1.05487E+00 0.00047 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.05470E+00 0.00028 ];
ABS_KINF                  (idx, [1:   2]) = [  1.06256E+00 0.00027 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72386E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72395E+01 9.0E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.54110E-07 0.00340 ];
IMP_EALF                  (idx, [1:   2]) = [  6.52055E-07 0.00155 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.33123E-01 0.00281 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.33061E-01 0.00122 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.76593E-03 0.00575  1.59119E-04 0.03560  9.68588E-04 0.01438  8.83237E-04 0.01433  2.59838E-03 0.00856  8.71247E-04 0.01528  2.85353E-04 0.02626 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  8.10560E-01 0.01402  1.00461E-02 0.02210  3.12928E-02 0.00040  1.10190E-01 0.00039  3.21534E-01 0.00025  1.33564E+00 0.00077  8.38438E+00 0.01157 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.98496E-03 0.00829  1.57235E-04 0.05167  1.01920E-03 0.02051  9.32571E-04 0.02124  2.68063E-03 0.01238  9.05933E-04 0.02140  2.89390E-04 0.03927 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.95566E-01 0.02003  1.24959E-02 0.00029  3.12840E-02 0.00055  1.10213E-01 0.00056  3.21566E-01 0.00038  1.33648E+00 0.00095  8.89124E+00 0.00446 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.23851E-05 0.00124  2.23776E-05 0.00124  2.37485E-05 0.01368 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.36096E-05 0.00110  2.36016E-05 0.00110  2.50523E-05 0.01367 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.88062E-03 0.00888  1.57009E-04 0.05645  9.60038E-04 0.02306  9.13795E-04 0.02236  2.63959E-03 0.01320  9.02966E-04 0.02358  3.07223E-04 0.03946 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.35574E-01 0.02076  1.25034E-02 0.00058  3.13089E-02 0.00068  1.10297E-01 0.00066  3.21581E-01 0.00043  1.33794E+00 0.00113  8.89770E+00 0.00583 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.17261E-05 0.00280  2.17169E-05 0.00280  2.24871E-05 0.03139 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.29143E-05 0.00273  2.29047E-05 0.00274  2.37036E-05 0.03135 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.60490E-03 0.02975  1.55327E-04 0.18137  9.38844E-04 0.07498  9.25213E-04 0.08104  2.44936E-03 0.04613  8.83450E-04 0.07426  2.52707E-04 0.13885 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.14654E-01 0.06646  1.24898E-02 3.4E-05  3.12786E-02 0.00163  1.10718E-01 0.00165  3.21706E-01 0.00122  1.33830E+00 0.00242  8.89254E+00 0.01064 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.61707E-03 0.02843  1.64786E-04 0.17989  9.12729E-04 0.07443  9.13457E-04 0.07668  2.46839E-03 0.04477  8.93861E-04 0.07064  2.63846E-04 0.13310 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.29658E-01 0.06402  1.24898E-02 3.4E-05  3.12745E-02 0.00163  1.10743E-01 0.00166  3.21702E-01 0.00120  1.33847E+00 0.00233  8.89297E+00 0.01063 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.58807E+02 0.02979 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.21148E-05 0.00078 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.33245E-05 0.00053 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.77720E-03 0.00572 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.61257E+02 0.00570 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.35201E-07 0.00075 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.78290E-06 0.00044  2.78282E-06 0.00044  2.79631E-06 0.00560 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.52952E-05 0.00089  3.53132E-05 0.00090  3.24782E-05 0.01060 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.42243E-01 0.00034  6.41928E-01 0.00034  7.10308E-01 0.00973 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03024E+01 0.01361 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.37227E+01 0.00046  3.26991E+01 0.00043 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.56919E+04 0.00320  2.98167E+05 0.00142  6.08119E+05 0.00069  6.51667E+05 0.00072  6.00452E+05 0.00098  6.44498E+05 0.00067  4.37524E+05 0.00079  3.87367E+05 0.00069  2.96267E+05 0.00047  2.42412E+05 0.00075  2.08857E+05 0.00063  1.88253E+05 0.00075  1.73790E+05 0.00089  1.65077E+05 0.00094  1.60968E+05 0.00072  1.38958E+05 0.00092  1.37128E+05 0.00081  1.36074E+05 0.00089  1.33469E+05 0.00084  2.60672E+05 0.00053  2.51381E+05 0.00074  1.80734E+05 0.00089  1.16937E+05 0.00094  1.35050E+05 0.00126  1.27476E+05 0.00089  1.14503E+05 0.00100  1.86501E+05 0.00074  4.29704E+04 0.00177  5.39263E+04 0.00125  4.88671E+04 0.00170  2.85069E+04 0.00199  4.92741E+04 0.00164  3.33122E+04 0.00168  2.79028E+04 0.00161  5.09074E+03 0.00402  4.74461E+03 0.00320  4.39959E+03 0.00368  4.23751E+03 0.00391  4.32166E+03 0.00429  4.62019E+03 0.00330  5.13996E+03 0.00365  4.99866E+03 0.00367  9.63202E+03 0.00249  1.56042E+04 0.00290  2.00148E+04 0.00138  5.29291E+04 0.00143  5.55401E+04 0.00111  5.90792E+04 0.00150  3.84095E+04 0.00108  2.71854E+04 0.00191  2.02555E+04 0.00148  2.37184E+04 0.00183  4.57491E+04 0.00126  6.32788E+04 0.00103  1.23410E+05 0.00102  1.91064E+05 0.00104  2.84796E+05 0.00109  1.81671E+05 0.00156  1.29940E+05 0.00148  9.33394E+04 0.00123  8.38538E+04 0.00133  8.24544E+04 0.00127  6.88704E+04 0.00126  4.66169E+04 0.00156  4.29862E+04 0.00156  3.81933E+04 0.00163  3.23477E+04 0.00183  2.54812E+04 0.00167  1.70276E+04 0.00242  6.04380E+03 0.00180 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.06273E+00 0.00049 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.25415E+18 0.00053  2.98727E+17 0.00102 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37318E-01 0.00011  1.53591E+00 0.00028 ];
INF_CAPT                  (idx, [1:   4]) = [  7.29810E-03 0.00074  3.49006E-02 0.00051 ];
INF_ABS                   (idx, [1:   4]) = [  9.49336E-03 0.00066  7.17827E-02 0.00074 ];
INF_FISS                  (idx, [1:   4]) = [  2.19526E-03 0.00063  3.68821E-02 0.00097 ];
INF_NSF                   (idx, [1:   4]) = [  5.73083E-03 0.00062  9.43792E-02 0.00099 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.61055E+00 5.1E-05  2.55894E+00 3.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04448E+02 4.8E-06  2.03899E+02 5.4E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.69143E-08 0.00037  2.54332E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.27825E-01 0.00011  1.46414E+00 0.00033 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42806E-01 0.00019  3.89717E-01 0.00033 ];
INF_SCATT2                (idx, [1:   4]) = [  9.58355E-02 0.00033  9.28313E-02 0.00098 ];
INF_SCATT3                (idx, [1:   4]) = [  7.36960E-03 0.00330  2.79442E-02 0.00232 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.01586E-02 0.00223 -8.48794E-03 0.00746 ];
INF_SCATT5                (idx, [1:   4]) = [  1.99388E-04 0.08861  6.24955E-03 0.00816 ];
INF_SCATT6                (idx, [1:   4]) = [  5.10743E-03 0.00299 -1.65551E-02 0.00227 ];
INF_SCATT7                (idx, [1:   4]) = [  7.51810E-04 0.01990  3.43956E-04 0.13101 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.27867E-01 0.00011  1.46414E+00 0.00033 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42806E-01 0.00019  3.89717E-01 0.00033 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.58360E-02 0.00033  9.28313E-02 0.00098 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.36953E-03 0.00331  2.79442E-02 0.00232 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.01587E-02 0.00223 -8.48794E-03 0.00746 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.99158E-04 0.08892  6.24955E-03 0.00816 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.10757E-03 0.00298 -1.65551E-02 0.00227 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.51735E-04 0.01992  3.43956E-04 0.13101 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13323E-01 0.00023  9.93118E-01 0.00029 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56258E+00 0.00023  3.35644E-01 0.00029 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.45182E-03 0.00065  7.17827E-02 0.00074 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69568E-02 0.00019  7.30346E-02 0.00086 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10361E-01 0.00011  1.74645E-02 0.00041  1.26398E-03 0.00449  1.46287E+00 0.00033 ];
INF_S1                    (idx, [1:   8]) = [  2.37737E-01 0.00019  5.06884E-03 0.00109  5.36076E-04 0.00745  3.89181E-01 0.00033 ];
INF_S2                    (idx, [1:   8]) = [  9.73853E-02 0.00031 -1.54980E-03 0.00321  2.91465E-04 0.01058  9.25398E-02 0.00099 ];
INF_S3                    (idx, [1:   8]) = [  9.17403E-03 0.00254 -1.80443E-03 0.00225  1.03277E-04 0.01949  2.78409E-02 0.00231 ];
INF_S4                    (idx, [1:   8]) = [ -9.58109E-03 0.00225 -5.77525E-04 0.00585  4.08776E-06 0.43981 -8.49203E-03 0.00749 ];
INF_S5                    (idx, [1:   8]) = [  1.68982E-04 0.10499  3.04052E-05 0.08865 -4.17662E-05 0.04241  6.29132E-03 0.00822 ];
INF_S6                    (idx, [1:   8]) = [  5.24274E-03 0.00274 -1.35302E-04 0.01396 -5.26423E-05 0.03222 -1.65025E-02 0.00229 ];
INF_S7                    (idx, [1:   8]) = [  9.19428E-04 0.01583 -1.67618E-04 0.01275 -4.61258E-05 0.02973  3.90081E-04 0.11680 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10402E-01 0.00011  1.74645E-02 0.00041  1.26398E-03 0.00449  1.46287E+00 0.00033 ];
INF_SP1                   (idx, [1:   8]) = [  2.37738E-01 0.00019  5.06884E-03 0.00109  5.36076E-04 0.00745  3.89181E-01 0.00033 ];
INF_SP2                   (idx, [1:   8]) = [  9.73858E-02 0.00031 -1.54980E-03 0.00321  2.91465E-04 0.01058  9.25398E-02 0.00099 ];
INF_SP3                   (idx, [1:   8]) = [  9.17395E-03 0.00254 -1.80443E-03 0.00225  1.03277E-04 0.01949  2.78409E-02 0.00231 ];
INF_SP4                   (idx, [1:   8]) = [ -9.58121E-03 0.00225 -5.77525E-04 0.00585  4.08776E-06 0.43981 -8.49203E-03 0.00749 ];
INF_SP5                   (idx, [1:   8]) = [  1.68753E-04 0.10535  3.04052E-05 0.08865 -4.17662E-05 0.04241  6.29132E-03 0.00822 ];
INF_SP6                   (idx, [1:   8]) = [  5.24287E-03 0.00274 -1.35302E-04 0.01396 -5.26423E-05 0.03222 -1.65025E-02 0.00229 ];
INF_SP7                   (idx, [1:   8]) = [  9.19353E-04 0.01585 -1.67618E-04 0.01275 -4.61258E-05 0.02973  3.90081E-04 0.11680 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31333E-01 0.00062  1.13317E+00 0.00727 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.32666E-01 0.00095  1.21746E+00 0.00976 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.32724E-01 0.00084  1.21356E+00 0.00783 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28669E-01 0.00131  9.99099E-01 0.00755 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.44094E+00 0.00062  2.94521E-01 0.00705 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.43270E+00 0.00095  2.74409E-01 0.00956 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.43234E+00 0.00084  2.75065E-01 0.00759 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45777E+00 0.00131  3.34091E-01 0.00755 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.98496E-03 0.00829  1.57235E-04 0.05167  1.01920E-03 0.02051  9.32571E-04 0.02124  2.68063E-03 0.01238  9.05933E-04 0.02140  2.89390E-04 0.03927 ];
LAMBDA                    (idx, [1:  14]) = [  7.95566E-01 0.02003  1.24959E-02 0.00029  3.12840E-02 0.00055  1.10213E-01 0.00056  3.21566E-01 0.00038  1.33648E+00 0.00095  8.89124E+00 0.00446 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0010' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:42:04 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:49:21 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292124 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.98444E-01  1.00084E+00  1.00311E+00  9.96839E-01  1.00077E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 2.1E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14758E-02 0.00112  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88524E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.03115E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.03556E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.66679E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.36997E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.36923E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.23610E+00 0.00034  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  3.99387E-01 0.00114  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000756 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00015E+04 0.00080 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00015E+04 0.00080 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.34180E+02 ;
RUNNING_TIME              (idx, 1)        =  6.72859E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.78167E-01  4.78167E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.10417E-01  1.07167E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  6.59920E+01  3.19938E+00  2.49827E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  5.99800E-01  2.72333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  2.91000E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  6.72857E+01  1.19099E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96657 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00011E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.77590E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6133.74;
MEMSIZE                   (idx, 1)        = 6039.76;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 169.50;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.98;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.28959E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.78552E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.30668E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.64487E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.14794E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.64469E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.67069E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.11932E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.37319E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.08373E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  6.93347E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.10942E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.67984E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.02873E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.26141E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.33169E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.37976E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.41537E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.21291E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.06126E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.43833E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  4.68974E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.21656E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.39547E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 11 ;
BURNUP                     (idx, [1:  2])  = [  9.00000E+00  9.00079E+00 ];
BURN_DAYS                 (idx, 1)        =  2.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.39745E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  9.14598E+15 0.00078  6.64972E-01 0.00052 ];
U238_FISS                 (idx, [1:   4]) = [  9.45772E+14 0.00285  6.87538E-02 0.00268 ];
PU239_FISS                (idx, [1:   4]) = [  3.39986E+15 0.00143  2.47188E-01 0.00129 ];
PU240_FISS                (idx, [1:   4]) = [  3.39724E+12 0.04612  2.47140E-04 0.04617 ];
PU241_FISS                (idx, [1:   4]) = [  2.50434E+14 0.00516  1.82081E-02 0.00512 ];
U235_CAPT                 (idx, [1:   4]) = [  2.05752E+15 0.00182  1.02874E-01 0.00173 ];
U238_CAPT                 (idx, [1:   4]) = [  8.14794E+15 0.00108  4.07358E-01 0.00073 ];
PU239_CAPT                (idx, [1:   4]) = [  1.89691E+15 0.00182  9.48483E-02 0.00178 ];
PU240_CAPT                (idx, [1:   4]) = [  8.92130E+14 0.00275  4.46055E-02 0.00269 ];
PU241_CAPT                (idx, [1:   4]) = [  9.16431E+13 0.00863  4.58275E-03 0.00865 ];
XE135_CAPT                (idx, [1:   4]) = [  7.22949E+14 0.00298  3.61503E-02 0.00300 ];
SM149_CAPT                (idx, [1:   4]) = [  1.89851E+14 0.00610  9.49273E-03 0.00609 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000756 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.87927E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000756 5.00788E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2940929 2.94522E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 2022619 2.02545E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 37208 3.72096E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000756 5.00788E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -2.32831E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50055E+05 2.0E-09  4.50055E+05 2.0E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.54877E+16 1.9E-05  3.54877E+16 1.9E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37600E+16 3.2E-06  1.37600E+16 3.2E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.00150E+16 0.00047  1.52198E+16 0.00047  4.79525E+15 0.00124 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.37750E+16 0.00028  2.89798E+16 0.00024  4.79525E+15 0.00124 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.39547E+16 0.00050  3.39547E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.57101E+18 0.00048  4.26028E+17 0.00046  1.14498E+18 0.00055 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.52708E+14 0.00544 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.40277E+16 0.00028 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.14568E+18 0.00064 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11466E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11466E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.62551E+00 0.00049 ];
SIX_FF_F                  (idx, [1:   2]) = [  8.04410E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.41975E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25411E+00 0.00036 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95177E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97368E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.05267E+00 0.00056 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.04483E+00 0.00056 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.57905E+00 2.2E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04144E+02 3.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.04465E+00 0.00058  1.03869E+00 0.00056  6.14499E-03 0.00924 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.04458E+00 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  1.04528E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.04458E+00 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  1.05241E+00 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72198E+01 0.00020 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72207E+01 9.1E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.66648E-07 0.00352 ];
IMP_EALF                  (idx, [1:   2]) = [  6.64400E-07 0.00157 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.36279E-01 0.00281 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.36090E-01 0.00118 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.75371E-03 0.00589  1.60757E-04 0.03342  9.87788E-04 0.01399  9.11435E-04 0.01480  2.56481E-03 0.00888  8.50514E-04 0.01469  2.78404E-04 0.02699 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.94485E-01 0.01405  1.03072E-02 0.02069  3.12674E-02 0.00040  1.10198E-01 0.00038  3.21575E-01 0.00027  1.33348E+00 0.00085  8.43533E+00 0.01121 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.97868E-03 0.00869  1.65268E-04 0.05031  1.02871E-03 0.02109  9.40511E-04 0.02156  2.67451E-03 0.01279  8.82584E-04 0.02198  2.87104E-04 0.03790 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.97055E-01 0.02023  1.25091E-02 0.00046  3.12508E-02 0.00055  1.10193E-01 0.00056  3.21656E-01 0.00038  1.33174E+00 0.00125  8.89189E+00 0.00496 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.25638E-05 0.00136  2.25566E-05 0.00137  2.37705E-05 0.01343 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.35668E-05 0.00119  2.35593E-05 0.00119  2.48300E-05 0.01341 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.86900E-03 0.00927  1.74655E-04 0.05284  1.00718E-03 0.02292  9.16861E-04 0.02432  2.63081E-03 0.01418  8.56285E-04 0.02518  2.83204E-04 0.04330 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.90999E-01 0.02230  1.25117E-02 0.00065  3.12584E-02 0.00068  1.10130E-01 0.00065  3.21655E-01 0.00045  1.33218E+00 0.00163  8.92298E+00 0.00597 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.18105E-05 0.00286  2.17983E-05 0.00289  2.30557E-05 0.03264 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.27811E-05 0.00282  2.27685E-05 0.00285  2.40869E-05 0.03266 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.82800E-03 0.03033  1.60934E-04 0.18347  9.39044E-04 0.07501  1.03002E-03 0.07424  2.51201E-03 0.04678  8.30102E-04 0.08038  3.55893E-04 0.11961 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.81712E-01 0.07068  1.25260E-02 0.00210  3.12346E-02 0.00166  1.10317E-01 0.00159  3.21976E-01 0.00134  1.33635E+00 0.00255  9.02931E+00 0.01240 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.81380E-03 0.02964  1.63954E-04 0.17730  9.54630E-04 0.07290  9.97701E-04 0.07181  2.48938E-03 0.04656  8.46940E-04 0.07759  3.61196E-04 0.11848 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.74524E-01 0.06852  1.25262E-02 0.00211  3.12354E-02 0.00165  1.10306E-01 0.00158  3.22009E-01 0.00133  1.33648E+00 0.00253  9.03755E+00 0.01232 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.68259E+02 0.03035 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.22511E-05 0.00079 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.32408E-05 0.00054 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.87745E-03 0.00601 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.64287E+02 0.00612 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.33990E-07 0.00082 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.77335E-06 0.00042  2.77327E-06 0.00042  2.78834E-06 0.00548 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.52919E-05 0.00098  3.53076E-05 0.00099  3.28370E-05 0.01083 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.40282E-01 0.00036  6.39991E-01 0.00036  7.02143E-01 0.00899 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07307E+01 0.01356 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.36923E+01 0.00049  3.26552E+01 0.00048 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.60381E+04 0.00311  2.98882E+05 0.00174  6.08652E+05 0.00082  6.51986E+05 0.00092  6.00032E+05 0.00073  6.44198E+05 0.00075  4.38019E+05 0.00068  3.87448E+05 0.00077  2.96538E+05 0.00078  2.42072E+05 0.00098  2.08859E+05 0.00086  1.88368E+05 0.00088  1.73756E+05 0.00092  1.64919E+05 0.00087  1.60945E+05 0.00097  1.39181E+05 0.00091  1.37139E+05 0.00078  1.35931E+05 0.00077  1.33544E+05 0.00094  2.60672E+05 0.00092  2.51279E+05 0.00053  1.81221E+05 0.00080  1.17118E+05 0.00112  1.35014E+05 0.00092  1.27841E+05 0.00075  1.14488E+05 0.00092  1.86280E+05 0.00088  4.29434E+04 0.00138  5.38513E+04 0.00151  4.88482E+04 0.00126  2.84010E+04 0.00179  4.93600E+04 0.00179  3.32320E+04 0.00143  2.78115E+04 0.00148  5.05338E+03 0.00405  4.62728E+03 0.00335  4.22905E+03 0.00292  4.07647E+03 0.00324  4.19319E+03 0.00396  4.50388E+03 0.00372  5.06977E+03 0.00345  4.96769E+03 0.00354  9.58657E+03 0.00316  1.54913E+04 0.00192  1.98607E+04 0.00193  5.29217E+04 0.00120  5.53011E+04 0.00108  5.88674E+04 0.00139  3.81377E+04 0.00155  2.70113E+04 0.00149  2.00187E+04 0.00183  2.34563E+04 0.00161  4.51867E+04 0.00125  6.26992E+04 0.00146  1.22528E+05 0.00159  1.90090E+05 0.00171  2.83611E+05 0.00172  1.81187E+05 0.00186  1.29679E+05 0.00185  9.32349E+04 0.00188  8.36405E+04 0.00174  8.23978E+04 0.00176  6.87616E+04 0.00191  4.65080E+04 0.00190  4.29137E+04 0.00212  3.82386E+04 0.00214  3.23123E+04 0.00192  2.54549E+04 0.00179  1.70187E+04 0.00228  6.03891E+03 0.00255 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.05312E+00 0.00044 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.26981E+18 0.00048  3.01228E+17 0.00162 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37366E-01 0.00013  1.53819E+00 0.00042 ];
INF_CAPT                  (idx, [1:   4]) = [  7.38484E-03 0.00054  3.53183E-02 0.00088 ];
INF_ABS                   (idx, [1:   4]) = [  9.54887E-03 0.00047  7.18834E-02 0.00125 ];
INF_FISS                  (idx, [1:   4]) = [  2.16404E-03 0.00039  3.65651E-02 0.00161 ];
INF_NSF                   (idx, [1:   4]) = [  5.66321E-03 0.00041  9.39572E-02 0.00165 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.61697E+00 7.9E-05  2.56958E+00 4.2E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04535E+02 6.8E-06  2.04046E+02 7.3E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.67775E-08 0.00035  2.54580E-06 0.00016 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.27822E-01 0.00014  1.46636E+00 0.00050 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42769E-01 0.00019  3.90398E-01 0.00062 ];
INF_SCATT2                (idx, [1:   4]) = [  9.57899E-02 0.00027  9.31904E-02 0.00109 ];
INF_SCATT3                (idx, [1:   4]) = [  7.31056E-03 0.00352  2.79442E-02 0.00241 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02404E-02 0.00201 -8.44159E-03 0.00713 ];
INF_SCATT5                (idx, [1:   4]) = [  1.84021E-04 0.10405  6.42838E-03 0.00955 ];
INF_SCATT6                (idx, [1:   4]) = [  5.09926E-03 0.00288 -1.66331E-02 0.00311 ];
INF_SCATT7                (idx, [1:   4]) = [  7.73733E-04 0.01784  3.39550E-04 0.15399 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.27864E-01 0.00014  1.46636E+00 0.00050 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42770E-01 0.00019  3.90398E-01 0.00062 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.57899E-02 0.00027  9.31904E-02 0.00109 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.31040E-03 0.00352  2.79442E-02 0.00241 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02401E-02 0.00201 -8.44159E-03 0.00713 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.84090E-04 0.10408  6.42838E-03 0.00955 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.09929E-03 0.00288 -1.66331E-02 0.00311 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.73689E-04 0.01789  3.39550E-04 0.15399 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13326E-01 0.00040  9.94794E-01 0.00035 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56256E+00 0.00040  3.35079E-01 0.00035 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.50673E-03 0.00047  7.18834E-02 0.00125 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69557E-02 0.00022  7.31040E-02 0.00125 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10411E-01 0.00013  1.74112E-02 0.00051  1.27526E-03 0.00315  1.46508E+00 0.00050 ];
INF_S1                    (idx, [1:   8]) = [  2.37714E-01 0.00018  5.05507E-03 0.00123  5.48839E-04 0.00382  3.89849E-01 0.00062 ];
INF_S2                    (idx, [1:   8]) = [  9.73318E-02 0.00027 -1.54189E-03 0.00208  2.97216E-04 0.00834  9.28931E-02 0.00109 ];
INF_S3                    (idx, [1:   8]) = [  9.10293E-03 0.00268 -1.79238E-03 0.00249  1.06214E-04 0.01846  2.78380E-02 0.00242 ];
INF_S4                    (idx, [1:   8]) = [ -9.66392E-03 0.00220 -5.76481E-04 0.00384  5.73257E-09 1.00000 -8.44160E-03 0.00713 ];
INF_S5                    (idx, [1:   8]) = [  1.56472E-04 0.11941  2.75490E-05 0.09589 -4.20262E-05 0.04013  6.47041E-03 0.00947 ];
INF_S6                    (idx, [1:   8]) = [  5.23483E-03 0.00281 -1.35577E-04 0.01508 -5.41782E-05 0.03060 -1.65789E-02 0.00312 ];
INF_S7                    (idx, [1:   8]) = [  9.40633E-04 0.01477 -1.66900E-04 0.01013 -4.67240E-05 0.02333  3.86274E-04 0.13534 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10453E-01 0.00013  1.74112E-02 0.00051  1.27526E-03 0.00315  1.46508E+00 0.00050 ];
INF_SP1                   (idx, [1:   8]) = [  2.37715E-01 0.00018  5.05507E-03 0.00123  5.48839E-04 0.00382  3.89849E-01 0.00062 ];
INF_SP2                   (idx, [1:   8]) = [  9.73318E-02 0.00027 -1.54189E-03 0.00208  2.97216E-04 0.00834  9.28931E-02 0.00109 ];
INF_SP3                   (idx, [1:   8]) = [  9.10278E-03 0.00268 -1.79238E-03 0.00249  1.06214E-04 0.01846  2.78380E-02 0.00242 ];
INF_SP4                   (idx, [1:   8]) = [ -9.66367E-03 0.00220 -5.76481E-04 0.00384  5.73257E-09 1.00000 -8.44160E-03 0.00713 ];
INF_SP5                   (idx, [1:   8]) = [  1.56541E-04 0.11947  2.75490E-05 0.09589 -4.20262E-05 0.04013  6.47041E-03 0.00947 ];
INF_SP6                   (idx, [1:   8]) = [  5.23487E-03 0.00281 -1.35577E-04 0.01508 -5.41782E-05 0.03060 -1.65789E-02 0.00312 ];
INF_SP7                   (idx, [1:   8]) = [  9.40589E-04 0.01481 -1.66900E-04 0.01013 -4.67240E-05 0.02333  3.86274E-04 0.13534 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31806E-01 0.00060  1.13484E+00 0.00711 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33194E-01 0.00093  1.22110E+00 0.00942 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33077E-01 0.00094  1.22278E+00 0.00932 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.29202E-01 0.00099  9.94476E-01 0.00641 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43799E+00 0.00060  2.94064E-01 0.00673 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42945E+00 0.00093  2.73548E-01 0.00927 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.43017E+00 0.00094  2.73148E-01 0.00897 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45435E+00 0.00099  3.35495E-01 0.00603 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.97868E-03 0.00869  1.65268E-04 0.05031  1.02871E-03 0.02109  9.40511E-04 0.02156  2.67451E-03 0.01279  8.82584E-04 0.02198  2.87104E-04 0.03790 ];
LAMBDA                    (idx, [1:  14]) = [  7.97055E-01 0.02023  1.25091E-02 0.00046  3.12508E-02 0.00055  1.10193E-01 0.00056  3.21656E-01 0.00038  1.33174E+00 0.00125  8.89189E+00 0.00496 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0010' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:42:04 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 02:55:09 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292124 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.98563E-01  1.00078E+00  1.00209E+00  9.97734E-01  1.00084E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.14851E-02 0.00107  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88515E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.03961E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.04401E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.66574E+00 0.00021  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.37748E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.37672E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.21031E+00 0.00036  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.00206E-01 0.00107  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000629 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00013E+04 0.00078 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00013E+04 0.00078 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.63142E+02 ;
RUNNING_TIME              (idx, 1)        =  7.30845E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.78167E-01  4.78167E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.31817E-01  1.06500E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.17152E+01  3.21128E+00  2.51193E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  6.53383E-01  2.66500E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.16667E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.30843E+01  1.19372E+02 ];
CPU_USAGE                 (idx, 1)        = 4.96880 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99850E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78152E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6133.74;
MEMSIZE                   (idx, 1)        = 6039.76;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 169.50;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.98;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.31528E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.77986E+04 ;
TOT_SF_RATE               (idx, 1)        =  4.58075E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.66454E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.16173E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.65071E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.66366E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.22571E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.41664E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.93088E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.03592E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.32622E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.71305E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.13078E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.27737E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.35250E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.40111E+14 ;
CS134_ACTIVITY            (idx, 1)        =  7.89102E+12 ;
CS137_ACTIVITY            (idx, 1)        =  1.34721E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.08379E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.42402E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  6.40325E+11 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.22323E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.44602E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 12 ;
BURNUP                     (idx, [1:  2])  = [  1.00000E+01  1.00009E+01 ];
BURN_DAYS                 (idx, 1)        =  2.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.50470E-01 0.00108 ];
U235_FISS                 (idx, [1:   4]) = [  8.86360E+15 0.00074  6.44724E-01 0.00051 ];
U238_FISS                 (idx, [1:   4]) = [  9.59862E+14 0.00283  6.98057E-02 0.00263 ];
PU239_FISS                (idx, [1:   4]) = [  3.60688E+15 0.00128  2.62356E-01 0.00114 ];
PU240_FISS                (idx, [1:   4]) = [  3.81686E+12 0.04294  2.77669E-04 0.04293 ];
PU241_FISS                (idx, [1:   4]) = [  3.04660E+14 0.00473  2.21605E-02 0.00469 ];
U235_CAPT                 (idx, [1:   4]) = [  1.99078E+15 0.00185  9.71021E-02 0.00179 ];
U238_CAPT                 (idx, [1:   4]) = [  8.24385E+15 0.00105  4.02062E-01 0.00069 ];
PU239_CAPT                (idx, [1:   4]) = [  2.01346E+15 0.00178  9.82068E-02 0.00170 ];
PU240_CAPT                (idx, [1:   4]) = [  1.00296E+15 0.00264  4.89190E-02 0.00257 ];
PU241_CAPT                (idx, [1:   4]) = [  1.10145E+14 0.00813  5.37247E-03 0.00813 ];
XE135_CAPT                (idx, [1:   4]) = [  7.19973E+14 0.00295  3.51198E-02 0.00296 ];
SM149_CAPT                (idx, [1:   4]) = [  1.94242E+14 0.00590  9.47372E-03 0.00586 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000629 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.77413E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000629 5.00777E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 2970494 2.97483E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1992075 1.99488E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 38060 3.80650E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000629 5.00777E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -1.39698E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50055E+05 2.0E-09  4.50055E+05 2.0E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.55908E+16 1.8E-05  3.55908E+16 1.8E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37515E+16 3.2E-06  1.37515E+16 3.2E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.05131E+16 0.00044  1.56150E+16 0.00045  4.89818E+15 0.00119 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.42646E+16 0.00026  2.93664E+16 0.00024  4.89818E+15 0.00119 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.44602E+16 0.00050  3.44602E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.59425E+18 0.00049  4.31285E+17 0.00047  1.16297E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.62388E+14 0.00512 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.45270E+16 0.00027 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.16528E+18 0.00063 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11350E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11350E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.61793E+00 0.00048 ];
SIX_FF_F                  (idx, [1:   2]) = [  8.01986E-01 0.00030 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.39990E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25310E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.95007E-01 3.1E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97367E-01 2.3E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.04053E+00 0.00055 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.03261E+00 0.00055 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.58814E+00 2.1E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04270E+02 3.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.03275E+00 0.00057  1.02657E+00 0.00055  6.04155E-03 0.00926 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.03246E+00 0.00027 ];
COL_KEFF                  (idx, [1:   2]) = [  1.03294E+00 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.03246E+00 0.00027 ];
ABS_KINF                  (idx, [1:   2]) = [  1.04038E+00 0.00026 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.72058E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.72013E+01 9.1E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  6.76320E-07 0.00377 ];
IMP_EALF                  (idx, [1:   2]) = [  6.77438E-07 0.00158 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.39205E-01 0.00292 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.39545E-01 0.00117 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.79332E-03 0.00623  1.57918E-04 0.03451  9.86444E-04 0.01424  9.11949E-04 0.01542  2.60676E-03 0.00908  8.76874E-04 0.01455  2.53371E-04 0.02725 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.58942E-01 0.01362  1.01801E-02 0.02140  3.12283E-02 0.00039  1.10242E-01 0.00039  3.21693E-01 0.00029  1.32777E+00 0.00096  8.20505E+00 0.01370 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.91055E-03 0.00930  1.65117E-04 0.05229  1.01509E-03 0.02136  9.46507E-04 0.02241  2.65969E-03 0.01294  8.82305E-04 0.02143  2.41831E-04 0.04111 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.37453E-01 0.02046  1.25043E-02 0.00044  3.12203E-02 0.00055  1.10235E-01 0.00054  3.21688E-01 0.00041  1.32602E+00 0.00146  8.89285E+00 0.00542 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.28256E-05 0.00132  2.28170E-05 0.00132  2.42417E-05 0.01355 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.35699E-05 0.00122  2.35609E-05 0.00122  2.50403E-05 0.01357 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.86204E-03 0.00932  1.70375E-04 0.05724  1.00803E-03 0.02332  9.33484E-04 0.02297  2.63972E-03 0.01378  8.65733E-04 0.02366  2.44693E-04 0.04423 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.39809E-01 0.02266  1.25032E-02 0.00063  3.12075E-02 0.00070  1.10183E-01 0.00066  3.21727E-01 0.00047  1.32465E+00 0.00193  8.92601E+00 0.00644 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.21624E-05 0.00303  2.21543E-05 0.00304  2.29341E-05 0.03511 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.28838E-05 0.00296  2.28754E-05 0.00296  2.36853E-05 0.03511 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  6.03238E-03 0.03028  2.34861E-04 0.19675  9.85721E-04 0.07563  8.69305E-04 0.07961  2.90085E-03 0.04323  8.29524E-04 0.07754  2.12119E-04 0.13391 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.12518E-01 0.07333  1.24893E-02 4.0E-05  3.11832E-02 0.00170  1.10502E-01 0.00178  3.21765E-01 0.00126  1.32357E+00 0.00452  8.86830E+00 0.01727 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  6.02325E-03 0.02931  2.28341E-04 0.18981  9.90052E-04 0.07354  8.77253E-04 0.07715  2.87122E-03 0.04235  8.50272E-04 0.07537  2.06110E-04 0.12835 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.19277E-01 0.07227  1.24893E-02 4.0E-05  3.11860E-02 0.00170  1.10478E-01 0.00177  3.21817E-01 0.00125  1.32330E+00 0.00454  8.86830E+00 0.01727 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.72990E+02 0.03064 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.25320E-05 0.00084 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.32662E-05 0.00062 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.91548E-03 0.00507 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.62608E+02 0.00510 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.35729E-07 0.00078 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.76412E-06 0.00046  2.76408E-06 0.00046  2.76982E-06 0.00527 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.55396E-05 0.00095  3.55587E-05 0.00096  3.24728E-05 0.00993 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.38186E-01 0.00034  6.37947E-01 0.00034  6.92350E-01 0.00982 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07211E+01 0.01325 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.37672E+01 0.00048  3.26567E+01 0.00049 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.62932E+04 0.00357  2.98997E+05 0.00138  6.08676E+05 0.00112  6.51586E+05 0.00072  5.99373E+05 0.00069  6.43980E+05 0.00059  4.37414E+05 0.00074  3.87324E+05 0.00064  2.96437E+05 0.00074  2.42162E+05 0.00065  2.08547E+05 0.00086  1.87906E+05 0.00076  1.73835E+05 0.00101  1.64952E+05 0.00082  1.60628E+05 0.00066  1.38887E+05 0.00101  1.37266E+05 0.00109  1.35864E+05 0.00120  1.33577E+05 0.00082  2.60683E+05 0.00072  2.51348E+05 0.00059  1.81125E+05 0.00090  1.17063E+05 0.00102  1.34971E+05 0.00077  1.27901E+05 0.00087  1.14386E+05 0.00091  1.86087E+05 0.00086  4.30093E+04 0.00120  5.39556E+04 0.00155  4.87660E+04 0.00164  2.83742E+04 0.00224  4.93401E+04 0.00113  3.31890E+04 0.00136  2.76456E+04 0.00228  5.00921E+03 0.00318  4.60952E+03 0.00359  4.15137E+03 0.00332  4.01793E+03 0.00315  4.08295E+03 0.00466  4.45072E+03 0.00317  4.99016E+03 0.00384  4.89737E+03 0.00338  9.45978E+03 0.00308  1.53786E+04 0.00259  1.98099E+04 0.00197  5.25379E+04 0.00146  5.51117E+04 0.00150  5.84395E+04 0.00116  3.78862E+04 0.00190  2.68188E+04 0.00168  1.99289E+04 0.00142  2.33278E+04 0.00190  4.50119E+04 0.00111  6.24980E+04 0.00154  1.22432E+05 0.00148  1.90008E+05 0.00149  2.84576E+05 0.00169  1.81894E+05 0.00176  1.30278E+05 0.00166  9.36498E+04 0.00180  8.41289E+04 0.00194  8.29322E+04 0.00181  6.90930E+04 0.00171  4.67897E+04 0.00191  4.32319E+04 0.00168  3.84668E+04 0.00177  3.25362E+04 0.00179  2.56381E+04 0.00194  1.71809E+04 0.00210  6.08259E+03 0.00199 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.04086E+00 0.00045 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.28794E+18 0.00051  3.06340E+17 0.00136 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37597E-01 0.00012  1.54374E+00 0.00033 ];
INF_CAPT                  (idx, [1:   4]) = [  7.47067E-03 0.00067  3.55571E-02 0.00070 ];
INF_ABS                   (idx, [1:   4]) = [  9.60313E-03 0.00056  7.14882E-02 0.00101 ];
INF_FISS                  (idx, [1:   4]) = [  2.13246E-03 0.00056  3.59311E-02 0.00133 ];
INF_NSF                   (idx, [1:   4]) = [  5.59375E-03 0.00056  9.26810E-02 0.00135 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.62314E+00 3.9E-05  2.57941E+00 3.2E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04619E+02 4.5E-06  2.04184E+02 5.5E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.66591E-08 0.00039  2.55001E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.27996E-01 0.00012  1.47229E+00 0.00040 ];
INF_SCATT1                (idx, [1:   4]) = [  2.42958E-01 0.00018  3.91885E-01 0.00054 ];
INF_SCATT2                (idx, [1:   4]) = [  9.58623E-02 0.00032  9.32947E-02 0.00101 ];
INF_SCATT3                (idx, [1:   4]) = [  7.29745E-03 0.00308  2.80085E-02 0.00282 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02637E-02 0.00209 -8.57572E-03 0.00597 ];
INF_SCATT5                (idx, [1:   4]) = [  1.67645E-04 0.08550  6.47086E-03 0.00826 ];
INF_SCATT6                (idx, [1:   4]) = [  5.07629E-03 0.00247 -1.67287E-02 0.00341 ];
INF_SCATT7                (idx, [1:   4]) = [  7.46801E-04 0.02253  2.74708E-04 0.20917 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28038E-01 0.00012  1.47229E+00 0.00040 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.42959E-01 0.00018  3.91885E-01 0.00054 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.58627E-02 0.00032  9.32947E-02 0.00101 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.29753E-03 0.00307  2.80085E-02 0.00282 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02636E-02 0.00209 -8.57572E-03 0.00597 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.67773E-04 0.08515  6.47086E-03 0.00826 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.07636E-03 0.00248 -1.67287E-02 0.00341 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.46808E-04 0.02250  2.74708E-04 0.20917 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13246E-01 0.00027  9.98590E-01 0.00029 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56314E+00 0.00028  3.33805E-01 0.00029 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.56153E-03 0.00055  7.14882E-02 0.00101 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69647E-02 0.00025  7.27108E-02 0.00114 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10632E-01 0.00012  1.73640E-02 0.00042  1.26389E-03 0.00461  1.47103E+00 0.00040 ];
INF_S1                    (idx, [1:   8]) = [  2.37909E-01 0.00019  5.04979E-03 0.00103  5.40104E-04 0.00772  3.91345E-01 0.00054 ];
INF_S2                    (idx, [1:   8]) = [  9.73982E-02 0.00032 -1.53585E-03 0.00269  2.97216E-04 0.01057  9.29974E-02 0.00100 ];
INF_S3                    (idx, [1:   8]) = [  9.08572E-03 0.00241 -1.78827E-03 0.00137  1.05471E-04 0.02161  2.79031E-02 0.00279 ];
INF_S4                    (idx, [1:   8]) = [ -9.68855E-03 0.00227 -5.75104E-04 0.00446 -9.60665E-07 1.00000 -8.57476E-03 0.00597 ];
INF_S5                    (idx, [1:   8]) = [  1.38756E-04 0.10538  2.88891E-05 0.09824 -4.45022E-05 0.03630  6.51536E-03 0.00818 ];
INF_S6                    (idx, [1:   8]) = [  5.21516E-03 0.00263 -1.38870E-04 0.01731 -5.61999E-05 0.03209 -1.66725E-02 0.00345 ];
INF_S7                    (idx, [1:   8]) = [  9.13640E-04 0.01903 -1.66839E-04 0.01633 -4.94648E-05 0.03260  3.24172E-04 0.17543 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10674E-01 0.00012  1.73640E-02 0.00042  1.26389E-03 0.00461  1.47103E+00 0.00040 ];
INF_SP1                   (idx, [1:   8]) = [  2.37909E-01 0.00019  5.04979E-03 0.00103  5.40104E-04 0.00772  3.91345E-01 0.00054 ];
INF_SP2                   (idx, [1:   8]) = [  9.73985E-02 0.00032 -1.53585E-03 0.00269  2.97216E-04 0.01057  9.29974E-02 0.00100 ];
INF_SP3                   (idx, [1:   8]) = [  9.08580E-03 0.00241 -1.78827E-03 0.00137  1.05471E-04 0.02161  2.79031E-02 0.00279 ];
INF_SP4                   (idx, [1:   8]) = [ -9.68854E-03 0.00227 -5.75104E-04 0.00446 -9.60665E-07 1.00000 -8.57476E-03 0.00597 ];
INF_SP5                   (idx, [1:   8]) = [  1.38884E-04 0.10496  2.88891E-05 0.09824 -4.45022E-05 0.03630  6.51536E-03 0.00818 ];
INF_SP6                   (idx, [1:   8]) = [  5.21523E-03 0.00263 -1.38870E-04 0.01731 -5.61999E-05 0.03209 -1.66725E-02 0.00345 ];
INF_SP7                   (idx, [1:   8]) = [  9.13648E-04 0.01901 -1.66839E-04 0.01633 -4.94648E-05 0.03260  3.24172E-04 0.17543 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31608E-01 0.00056  1.13971E+00 0.00729 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.32971E-01 0.00085  1.21784E+00 0.01069 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33217E-01 0.00090  1.22154E+00 0.00754 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28700E-01 0.00102  1.00930E+00 0.00840 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43923E+00 0.00056  2.92851E-01 0.00739 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.43082E+00 0.00085  2.74482E-01 0.01100 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42931E+00 0.00090  2.73255E-01 0.00759 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45755E+00 0.00102  3.30818E-01 0.00835 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.91055E-03 0.00930  1.65117E-04 0.05229  1.01509E-03 0.02136  9.46507E-04 0.02241  2.65969E-03 0.01294  8.82305E-04 0.02143  2.41831E-04 0.04111 ];
LAMBDA                    (idx, [1:  14]) = [  7.37453E-01 0.02046  1.25043E-02 0.00044  3.12203E-02 0.00055  1.10235E-01 0.00054  3.21688E-01 0.00041  1.32602E+00 0.00146  8.89285E+00 0.00542 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0010' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:42:04 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:00:58 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292124 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  1.00014E+00  1.00035E+00  1.00223E+00  9.98164E-01  9.99120E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.3E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.15819E-02 0.00102  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88418E-01 1.2E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.04583E-01 0.00010  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05022E-01 0.00010  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.66823E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.37949E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.37871E+01 0.00046  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.18277E+00 0.00034  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.04325E-01 0.00106  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000944 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00019E+04 0.00081 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00019E+04 0.00081 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  3.92221E+02 ;
RUNNING_TIME              (idx, 1)        =  7.89065E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.78167E-01  4.78167E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.54483E-01  1.09667E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  7.74582E+01  3.22552E+00  2.51752E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  7.09417E-01  2.69667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.35000E-02  9.16668E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  7.89062E+01  1.19539E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97070 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99965E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.78634E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6133.74;
MEMSIZE                   (idx, 1)        = 6039.76;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 169.50;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.98;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.32465E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.75297E+04 ;
TOT_SF_RATE               (idx, 1)        =  9.71493E+04 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.70045E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.18708E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62417E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.63424E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.51528E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.49491E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  8.36014E+07 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.24251E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  6.79264E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.77066E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.37762E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.28030E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.38002E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.41541E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.21269E+13 ;
CS137_ACTIVITY            (idx, 1)        =  1.68254E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.09178E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.38406E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.26413E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.23195E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.55729E+12 0.00049  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 13 ;
BURNUP                     (idx, [1:  2])  = [  1.25000E+01  1.25011E+01 ];
BURN_DAYS                 (idx, 1)        =  3.12500E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.73599E-01 0.00111 ];
U235_FISS                 (idx, [1:   4]) = [  8.17717E+15 0.00086  5.96204E-01 0.00063 ];
U238_FISS                 (idx, [1:   4]) = [  9.90721E+14 0.00285  7.22289E-02 0.00273 ];
PU239_FISS                (idx, [1:   4]) = [  4.08524E+15 0.00132  2.97852E-01 0.00115 ];
PU240_FISS                (idx, [1:   4]) = [  5.19387E+12 0.03595  3.78579E-04 0.03593 ];
PU241_FISS                (idx, [1:   4]) = [  4.46566E+14 0.00399  3.25598E-02 0.00396 ];
U235_CAPT                 (idx, [1:   4]) = [  1.84689E+15 0.00197  8.53744E-02 0.00189 ];
U238_CAPT                 (idx, [1:   4]) = [  8.45019E+15 0.00106  3.90593E-01 0.00074 ];
PU239_CAPT                (idx, [1:   4]) = [  2.27666E+15 0.00172  1.05250E-01 0.00173 ];
PU240_CAPT                (idx, [1:   4]) = [  1.24606E+15 0.00250  5.75970E-02 0.00238 ];
PU241_CAPT                (idx, [1:   4]) = [  1.62458E+14 0.00620  7.51088E-03 0.00622 ];
XE135_CAPT                (idx, [1:   4]) = [  7.32577E+14 0.00311  3.38676E-02 0.00312 ];
SM149_CAPT                (idx, [1:   4]) = [  2.03048E+14 0.00601  9.38704E-03 0.00600 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000944 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.72448E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000944 5.00772E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3036472 3.04067E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1925323 1.92789E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 39149 3.91616E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000944 5.00772E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 1.86265E-09 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50055E+05 2.0E-09  4.50055E+05 2.0E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.58337E+16 1.9E-05  3.58337E+16 1.9E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37311E+16 3.7E-06  1.37311E+16 3.7E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.16194E+16 0.00041  1.65510E+16 0.00043  5.06833E+15 0.00112 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.53505E+16 0.00025  3.02822E+16 0.00024  5.06833E+15 0.00112 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.55729E+16 0.00049  3.55729E+16 0.00049  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.64450E+18 0.00045  4.43390E+17 0.00045  1.20111E+18 0.00050 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.78657E+14 0.00556 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.56292E+16 0.00026 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.20357E+18 0.00058 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.11059E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.11059E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.59354E+00 0.00049 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.99737E-01 0.00031 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.36123E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25120E+00 0.00039 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94866E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97288E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  1.01425E+00 0.00057 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  1.00631E+00 0.00057 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.60967E+00 2.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04573E+02 3.7E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  1.00625E+00 0.00058  1.00067E+00 0.00058  5.63342E-03 0.00927 ];
IMP_KEFF                  (idx, [1:   2]) = [  1.00736E+00 0.00026 ];
COL_KEFF                  (idx, [1:   2]) = [  1.00745E+00 0.00049 ];
ABS_KEFF                  (idx, [1:   2]) = [  1.00736E+00 0.00026 ];
ABS_KINF                  (idx, [1:   2]) = [  1.01531E+00 0.00025 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71617E+01 0.00023 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71616E+01 8.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.07031E-07 0.00390 ];
IMP_EALF                  (idx, [1:   2]) = [  7.04836E-07 0.00153 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.47955E-01 0.00297 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.47167E-01 0.00112 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.68471E-03 0.00587  1.54074E-04 0.03478  9.48591E-04 0.01402  8.98685E-04 0.01479  2.52503E-03 0.00877  8.86008E-04 0.01466  2.72323E-04 0.02681 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.92422E-01 0.01405  1.00791E-02 0.02197  3.11446E-02 0.00042  1.10442E-01 0.00044  3.21688E-01 0.00027  1.31875E+00 0.00128  8.19609E+00 0.01338 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.58655E-03 0.00845  1.52688E-04 0.05259  9.22222E-04 0.02078  8.90154E-04 0.02199  2.50714E-03 0.01311  8.55472E-04 0.02219  2.58870E-04 0.04147 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.75589E-01 0.02137  1.25062E-02 0.00047  3.11429E-02 0.00060  1.10437E-01 0.00062  3.21779E-01 0.00042  1.31725E+00 0.00192  8.80940E+00 0.00675 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.33792E-05 0.00133  2.33738E-05 0.00134  2.43427E-05 0.01438 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.35213E-05 0.00119  2.35159E-05 0.00119  2.44876E-05 0.01434 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.59621E-03 0.00944  1.43719E-04 0.05834  9.36667E-04 0.02343  8.80823E-04 0.02388  2.47777E-03 0.01530  8.97446E-04 0.02397  2.59784E-04 0.04717 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.83790E-01 0.02449  1.25086E-02 0.00073  3.11228E-02 0.00077  1.10354E-01 0.00072  3.21819E-01 0.00050  1.31513E+00 0.00251  8.82206E+00 0.00823 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.26171E-05 0.00308  2.26093E-05 0.00309  2.39144E-05 0.03653 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.27558E-05 0.00305  2.27479E-05 0.00306  2.40667E-05 0.03657 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.78340E-03 0.03210  1.20504E-04 0.18813  9.49614E-04 0.08029  9.00842E-04 0.07635  2.70151E-03 0.05095  9.17854E-04 0.07870  1.93082E-04 0.14875 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.24719E-01 0.07391  1.25165E-02 0.00216  3.12160E-02 0.00169  1.10465E-01 0.00174  3.22030E-01 0.00141  1.30143E+00 0.00683  8.83759E+00 0.02009 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.77305E-03 0.03059  1.27314E-04 0.18135  9.61249E-04 0.07915  8.98177E-04 0.07278  2.67813E-03 0.04895  9.17562E-04 0.07658  1.90624E-04 0.14010 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.26576E-01 0.07202  1.25165E-02 0.00216  3.12060E-02 0.00168  1.10477E-01 0.00174  3.22025E-01 0.00139  1.30146E+00 0.00682  8.83854E+00 0.02009 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.58586E+02 0.03286 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.30628E-05 0.00080 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.32033E-05 0.00057 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.70370E-03 0.00554 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.47380E+02 0.00558 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.35555E-07 0.00076 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.73871E-06 0.00044  2.73871E-06 0.00044  2.73315E-06 0.00541 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.57496E-05 0.00089  3.57722E-05 0.00089  3.19586E-05 0.01153 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.34296E-01 0.00036  6.34177E-01 0.00036  6.67116E-01 0.00911 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.03797E+01 0.01340 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.37871E+01 0.00046  3.26058E+01 0.00050 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.69125E+04 0.00352  3.00650E+05 0.00119  6.09666E+05 0.00113  6.52317E+05 0.00076  5.99953E+05 0.00066  6.43415E+05 0.00067  4.37278E+05 0.00076  3.86969E+05 0.00082  2.95761E+05 0.00092  2.41794E+05 0.00075  2.08551E+05 0.00066  1.88082E+05 0.00088  1.73713E+05 0.00122  1.64949E+05 0.00088  1.60745E+05 0.00078  1.38901E+05 0.00092  1.37306E+05 0.00097  1.35840E+05 0.00116  1.33546E+05 0.00091  2.60607E+05 0.00056  2.51147E+05 0.00061  1.80962E+05 0.00087  1.16874E+05 0.00110  1.35074E+05 0.00077  1.27844E+05 0.00068  1.14320E+05 0.00122  1.85722E+05 0.00087  4.28629E+04 0.00192  5.39205E+04 0.00156  4.86652E+04 0.00146  2.84149E+04 0.00193  4.92468E+04 0.00166  3.30231E+04 0.00159  2.73246E+04 0.00176  4.87092E+03 0.00332  4.32696E+03 0.00398  3.92260E+03 0.00292  3.76396E+03 0.00426  3.83066E+03 0.00333  4.19812E+03 0.00320  4.83028E+03 0.00347  4.78788E+03 0.00319  9.34742E+03 0.00292  1.51179E+04 0.00226  1.95432E+04 0.00227  5.19745E+04 0.00182  5.45771E+04 0.00118  5.79105E+04 0.00132  3.73438E+04 0.00164  2.62728E+04 0.00170  1.94645E+04 0.00163  2.28389E+04 0.00210  4.41078E+04 0.00141  6.16608E+04 0.00133  1.21477E+05 0.00154  1.89455E+05 0.00170  2.84306E+05 0.00157  1.81887E+05 0.00162  1.30650E+05 0.00182  9.38340E+04 0.00198  8.44003E+04 0.00186  8.30962E+04 0.00201  6.93783E+04 0.00197  4.69431E+04 0.00219  4.33593E+04 0.00212  3.85758E+04 0.00201  3.25850E+04 0.00203  2.57235E+04 0.00194  1.71967E+04 0.00225  6.09302E+03 0.00230 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  1.01540E+00 0.00044 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.32916E+18 0.00048  3.15375E+17 0.00119 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.37799E-01 0.00013  1.54983E+00 0.00034 ];
INF_CAPT                  (idx, [1:   4]) = [  7.65657E-03 0.00064  3.62862E-02 0.00062 ];
INF_ABS                   (idx, [1:   4]) = [  9.71247E-03 0.00056  7.11666E-02 0.00087 ];
INF_FISS                  (idx, [1:   4]) = [  2.05590E-03 0.00047  3.48804E-02 0.00113 ];
INF_NSF                   (idx, [1:   4]) = [  5.42466E-03 0.00046  9.07757E-02 0.00116 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.63858E+00 5.0E-05  2.60248E+00 5.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.04830E+02 6.0E-06  2.04509E+02 9.0E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.62974E-08 0.00049  2.55551E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28095E-01 0.00014  1.47864E+00 0.00040 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43085E-01 0.00025  3.93439E-01 0.00052 ];
INF_SCATT2                (idx, [1:   4]) = [  9.59376E-02 0.00031  9.34390E-02 0.00099 ];
INF_SCATT3                (idx, [1:   4]) = [  7.32103E-03 0.00332  2.81467E-02 0.00286 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02725E-02 0.00176 -8.55624E-03 0.00840 ];
INF_SCATT5                (idx, [1:   4]) = [  1.84092E-04 0.10565  6.53581E-03 0.00753 ];
INF_SCATT6                (idx, [1:   4]) = [  5.09693E-03 0.00337 -1.67432E-02 0.00301 ];
INF_SCATT7                (idx, [1:   4]) = [  7.53549E-04 0.02350  3.95029E-04 0.09670 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28136E-01 0.00014  1.47864E+00 0.00040 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43086E-01 0.00025  3.93439E-01 0.00052 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.59381E-02 0.00031  9.34390E-02 0.00099 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.32102E-03 0.00332  2.81467E-02 0.00286 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02728E-02 0.00176 -8.55624E-03 0.00840 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.84072E-04 0.10548  6.53581E-03 0.00753 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.09704E-03 0.00338 -1.67432E-02 0.00301 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.53416E-04 0.02348  3.95029E-04 0.09670 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.13038E-01 0.00031  1.00328E+00 0.00033 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56467E+00 0.00031  3.32246E-01 0.00033 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.67112E-03 0.00057  7.11666E-02 0.00087 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69662E-02 0.00020  7.24510E-02 0.00110 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.10833E-01 0.00013  1.72625E-02 0.00057  1.25928E-03 0.00496  1.47738E+00 0.00040 ];
INF_S1                    (idx, [1:   8]) = [  2.38075E-01 0.00025  5.01010E-03 0.00078  5.41624E-04 0.00686  3.92897E-01 0.00053 ];
INF_S2                    (idx, [1:   8]) = [  9.74787E-02 0.00030 -1.54110E-03 0.00273  2.95132E-04 0.00828  9.31439E-02 0.00100 ];
INF_S3                    (idx, [1:   8]) = [  9.10428E-03 0.00254 -1.78325E-03 0.00161  1.07537E-04 0.02253  2.80392E-02 0.00286 ];
INF_S4                    (idx, [1:   8]) = [ -9.70183E-03 0.00181 -5.70712E-04 0.00420  1.56451E-06 1.00000 -8.55780E-03 0.00840 ];
INF_S5                    (idx, [1:   8]) = [  1.52541E-04 0.12432  3.15510E-05 0.08267 -4.11043E-05 0.03270  6.57692E-03 0.00750 ];
INF_S6                    (idx, [1:   8]) = [  5.23316E-03 0.00315 -1.36228E-04 0.01968 -5.30934E-05 0.03145 -1.66901E-02 0.00305 ];
INF_S7                    (idx, [1:   8]) = [  9.19950E-04 0.01921 -1.66401E-04 0.01040 -4.96697E-05 0.02972  4.44699E-04 0.08595 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.10874E-01 0.00013  1.72625E-02 0.00057  1.25928E-03 0.00496  1.47738E+00 0.00040 ];
INF_SP1                   (idx, [1:   8]) = [  2.38076E-01 0.00025  5.01010E-03 0.00078  5.41624E-04 0.00686  3.92897E-01 0.00053 ];
INF_SP2                   (idx, [1:   8]) = [  9.74792E-02 0.00031 -1.54110E-03 0.00273  2.95132E-04 0.00828  9.31439E-02 0.00100 ];
INF_SP3                   (idx, [1:   8]) = [  9.10427E-03 0.00254 -1.78325E-03 0.00161  1.07537E-04 0.02253  2.80392E-02 0.00286 ];
INF_SP4                   (idx, [1:   8]) = [ -9.70210E-03 0.00181 -5.70712E-04 0.00420  1.56451E-06 1.00000 -8.55780E-03 0.00840 ];
INF_SP5                   (idx, [1:   8]) = [  1.52521E-04 0.12411  3.15510E-05 0.08267 -4.11043E-05 0.03270  6.57692E-03 0.00750 ];
INF_SP6                   (idx, [1:   8]) = [  5.23327E-03 0.00315 -1.36228E-04 0.01968 -5.30934E-05 0.03145 -1.66901E-02 0.00305 ];
INF_SP7                   (idx, [1:   8]) = [  9.19817E-04 0.01919 -1.66401E-04 0.01040 -4.96697E-05 0.02972  4.44699E-04 0.08595 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31552E-01 0.00064  1.17459E+00 0.00632 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33045E-01 0.00106  1.27045E+00 0.00880 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.32990E-01 0.00087  1.27775E+00 0.00874 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28685E-01 0.00104  1.01756E+00 0.00698 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43957E+00 0.00064  2.84060E-01 0.00636 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.43038E+00 0.00106  2.62866E-01 0.00887 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.43070E+00 0.00087  2.61355E-01 0.00876 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45764E+00 0.00103  3.27958E-01 0.00687 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.58655E-03 0.00845  1.52688E-04 0.05259  9.22222E-04 0.02078  8.90154E-04 0.02199  2.50714E-03 0.01311  8.55472E-04 0.02219  2.58870E-04 0.04147 ];
LAMBDA                    (idx, [1:  14]) = [  7.75589E-01 0.02137  1.25062E-02 0.00047  3.11429E-02 0.00060  1.10437E-01 0.00062  3.21779E-01 0.00042  1.31725E+00 0.00192  8.80940E+00 0.00675 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0010' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:42:04 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:06:47 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292124 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99151E-01  1.00089E+00  1.00278E+00  9.96603E-01  1.00058E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16251E-02 0.00111  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88375E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05031E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.05470E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67140E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.38338E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.38258E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.16875E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.06445E-01 0.00115  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000775 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00015E+04 0.00082 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00015E+04 0.00082 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.21288E+02 ;
RUNNING_TIME              (idx, 1)        =  8.47262E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.78167E-01  4.78167E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.76067E-01  1.04333E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  8.32015E+01  3.21182E+00  2.53145E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  7.63883E-01  2.63833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.53667E-02  9.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  8.47261E+01  1.19710E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97235 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99980E+00 0.00018 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79049E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6133.74;
MEMSIZE                   (idx, 1)        = 6039.76;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 169.50;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.98;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.37088E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.74059E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.85319E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.74692E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.22016E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62394E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.61854E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  1.84257E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.57787E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.12286E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.49946E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.19713E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.82792E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.61329E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.30349E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.41442E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.44862E+14 ;
CS134_ACTIVITY            (idx, 1)        =  1.71404E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.01693E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.13430E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.35562E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.21340E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.24563E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.66624E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 14 ;
BURNUP                     (idx, [1:  2])  = [  1.50000E+01  1.50014E+01 ];
BURN_DAYS                 (idx, 1)        =  3.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  5.95584E-01 0.00105 ];
U235_FISS                 (idx, [1:   4]) = [  7.58752E+15 0.00093  5.53477E-01 0.00066 ];
U238_FISS                 (idx, [1:   4]) = [  1.01702E+15 0.00281  7.41814E-02 0.00267 ];
PU239_FISS                (idx, [1:   4]) = [  4.48572E+15 0.00114  3.27231E-01 0.00104 ];
PU240_FISS                (idx, [1:   4]) = [  6.52372E+12 0.03224  4.75784E-04 0.03223 ];
PU241_FISS                (idx, [1:   4]) = [  5.99005E+14 0.00349  4.36925E-02 0.00339 ];
U235_CAPT                 (idx, [1:   4]) = [  1.71806E+15 0.00205  7.56273E-02 0.00194 ];
U238_CAPT                 (idx, [1:   4]) = [  8.65909E+15 0.00105  3.81152E-01 0.00075 ];
PU239_CAPT                (idx, [1:   4]) = [  2.49460E+15 0.00166  1.09816E-01 0.00161 ];
PU240_CAPT                (idx, [1:   4]) = [  1.47249E+15 0.00238  6.48136E-02 0.00224 ];
PU241_CAPT                (idx, [1:   4]) = [  2.16545E+14 0.00556  9.53408E-03 0.00560 ];
XE135_CAPT                (idx, [1:   4]) = [  7.38572E+14 0.00320  3.25141E-02 0.00320 ];
SM149_CAPT                (idx, [1:   4]) = [  2.11753E+14 0.00590  9.32331E-03 0.00594 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000775 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.98719E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000775 5.00799E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3093671 3.09818E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1866974 1.86967E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 40130 4.01347E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000775 5.00799E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -4.47035E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50055E+05 2.0E-09  4.50055E+05 2.0E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.60532E+16 1.9E-05  3.60532E+16 1.9E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.37124E+16 3.6E-06  1.37124E+16 3.6E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.27053E+16 0.00044  1.74599E+16 0.00045  5.24540E+15 0.00119 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.64177E+16 0.00028  3.11723E+16 0.00025  5.24540E+15 0.00119 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.66624E+16 0.00050  3.66624E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.69395E+18 0.00048  4.55403E+17 0.00046  1.23855E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  2.94332E+14 0.00533 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.67120E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.24191E+18 0.00063 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10769E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10769E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.57246E+00 0.00051 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.97164E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.32308E-01 0.00037 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.25047E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94774E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97184E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.91046E-01 0.00057 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.83092E-01 0.00057 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.62923E+00 2.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.04852E+02 3.6E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.83047E-01 0.00058  9.77649E-01 0.00057  5.44326E-03 0.01062 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.83649E-01 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  9.83506E-01 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.83649E-01 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  9.91609E-01 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71277E+01 0.00023 ];
IMP_ALF                   (idx, [1:   2]) = [  1.71272E+01 9.1E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.31463E-07 0.00389 ];
IMP_EALF                  (idx, [1:   2]) = [  7.29574E-07 0.00157 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.54526E-01 0.00297 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.54246E-01 0.00117 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.73329E-03 0.00640  1.52918E-04 0.03733  9.77273E-04 0.01487  9.06928E-04 0.01558  2.52042E-03 0.00941  8.98842E-04 0.01519  2.76902E-04 0.02694 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.91061E-01 0.01408  9.61788E-03 0.02461  3.09937E-02 0.00205  1.10376E-01 0.00048  3.22093E-01 0.00030  1.31618E+00 0.00126  8.10402E+00 0.01357 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.49093E-03 0.00906  1.53315E-04 0.05402  9.34804E-04 0.02168  8.68606E-04 0.02477  2.39963E-03 0.01385  8.71389E-04 0.02165  2.63185E-04 0.03904 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.90169E-01 0.02112  1.25298E-02 0.00076  3.10487E-02 0.00063  1.10454E-01 0.00064  3.21894E-01 0.00044  1.31135E+00 0.00205  8.65347E+00 0.00790 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.39055E-05 0.00131  2.38980E-05 0.00132  2.52793E-05 0.01379 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.34963E-05 0.00118  2.34889E-05 0.00118  2.48424E-05 0.01375 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.53465E-03 0.01072  1.49533E-04 0.06270  9.45135E-04 0.02494  8.74865E-04 0.02650  2.41838E-03 0.01554  8.69924E-04 0.02508  2.76813E-04 0.04312 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  8.08286E-01 0.02392  1.25445E-02 0.00126  3.10488E-02 0.00080  1.10501E-01 0.00082  3.22269E-01 0.00054  1.30865E+00 0.00280  8.65399E+00 0.01004 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.32284E-05 0.00322  2.32220E-05 0.00323  2.32355E-05 0.03793 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.28305E-05 0.00316  2.28240E-05 0.00317  2.28568E-05 0.03807 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.35723E-03 0.03128  1.32476E-04 0.18778  9.18155E-04 0.08579  8.43881E-04 0.07902  2.35015E-03 0.05096  8.26539E-04 0.07971  2.86033E-04 0.14140 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.92325E-01 0.08036  1.25895E-02 0.00387  3.10048E-02 0.00190  1.10721E-01 0.00194  3.22679E-01 0.00150  1.30701E+00 0.00646  8.50080E+00 0.02465 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.38075E-03 0.03135  1.42529E-04 0.18020  9.06265E-04 0.08323  8.64966E-04 0.07734  2.35300E-03 0.04995  8.24623E-04 0.07961  2.89364E-04 0.13881 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.77841E-01 0.07847  1.25895E-02 0.00387  3.10152E-02 0.00189  1.10714E-01 0.00193  3.22627E-01 0.00151  1.30736E+00 0.00644  8.50734E+00 0.02440 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.31515E+02 0.03151 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.35760E-05 0.00083 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.31725E-05 0.00061 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.46346E-03 0.00614 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.31865E+02 0.00627 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.36689E-07 0.00077 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.71659E-06 0.00044  2.71652E-06 0.00044  2.72792E-06 0.00562 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.60322E-05 0.00091  3.60486E-05 0.00090  3.31084E-05 0.01122 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.30504E-01 0.00037  6.30480E-01 0.00037  6.49328E-01 0.01044 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.05411E+01 0.01503 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.38258E+01 0.00048  3.25773E+01 0.00049 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.72076E+04 0.00326  3.01524E+05 0.00133  6.09695E+05 0.00101  6.51863E+05 0.00083  5.99410E+05 0.00084  6.43678E+05 0.00072  4.36664E+05 0.00062  3.86737E+05 0.00056  2.95771E+05 0.00075  2.41608E+05 0.00052  2.08451E+05 0.00068  1.87779E+05 0.00085  1.73757E+05 0.00075  1.64912E+05 0.00104  1.60484E+05 0.00065  1.38690E+05 0.00118  1.37022E+05 0.00063  1.35673E+05 0.00078  1.33557E+05 0.00083  2.60599E+05 0.00071  2.51437E+05 0.00071  1.80838E+05 0.00082  1.17293E+05 0.00073  1.35105E+05 0.00085  1.27957E+05 0.00069  1.14418E+05 0.00097  1.85327E+05 0.00068  4.28801E+04 0.00183  5.37166E+04 0.00140  4.85356E+04 0.00160  2.82188E+04 0.00186  4.90140E+04 0.00134  3.27855E+04 0.00195  2.70314E+04 0.00190  4.76409E+03 0.00381  4.22715E+03 0.00377  3.71856E+03 0.00320  3.56448E+03 0.00471  3.66048E+03 0.00455  4.02483E+03 0.00455  4.66655E+03 0.00354  4.65646E+03 0.00342  9.15772E+03 0.00208  1.49939E+04 0.00183  1.93833E+04 0.00200  5.15823E+04 0.00153  5.41525E+04 0.00109  5.74530E+04 0.00143  3.69561E+04 0.00116  2.59942E+04 0.00126  1.91723E+04 0.00125  2.24968E+04 0.00141  4.36283E+04 0.00128  6.11583E+04 0.00175  1.20835E+05 0.00162  1.88997E+05 0.00158  2.84575E+05 0.00141  1.82556E+05 0.00156  1.30772E+05 0.00162  9.42100E+04 0.00186  8.47542E+04 0.00184  8.34870E+04 0.00189  6.97488E+04 0.00184  4.72855E+04 0.00168  4.35955E+04 0.00197  3.88716E+04 0.00205  3.28114E+04 0.00223  2.59193E+04 0.00210  1.73444E+04 0.00201  6.17597E+03 0.00232 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.91465E-01 0.00036 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.36898E+18 0.00035  3.25004E+17 0.00148 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38069E-01 0.00010  1.55483E+00 0.00042 ];
INF_CAPT                  (idx, [1:   4]) = [  7.82888E-03 0.00069  3.68888E-02 0.00076 ];
INF_ABS                   (idx, [1:   4]) = [  9.81551E-03 0.00060  7.07193E-02 0.00110 ];
INF_FISS                  (idx, [1:   4]) = [  1.98663E-03 0.00051  3.38305E-02 0.00147 ];
INF_NSF                   (idx, [1:   4]) = [  5.27066E-03 0.00053  8.87487E-02 0.00149 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.65306E+00 6.4E-05  2.62333E+00 3.6E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05031E+02 6.5E-06  2.04808E+02 6.6E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.60196E-08 0.00046  2.56151E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28258E-01 0.00011  1.48407E+00 0.00049 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43203E-01 0.00017  3.94676E-01 0.00054 ];
INF_SCATT2                (idx, [1:   4]) = [  9.59632E-02 0.00025  9.36049E-02 0.00110 ];
INF_SCATT3                (idx, [1:   4]) = [  7.28130E-03 0.00296  2.81088E-02 0.00253 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02983E-02 0.00150 -8.76035E-03 0.00832 ];
INF_SCATT5                (idx, [1:   4]) = [  1.63650E-04 0.07999  6.65713E-03 0.00824 ];
INF_SCATT6                (idx, [1:   4]) = [  5.08485E-03 0.00377 -1.67926E-02 0.00296 ];
INF_SCATT7                (idx, [1:   4]) = [  7.41884E-04 0.01940  3.38004E-04 0.11469 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28301E-01 0.00011  1.48407E+00 0.00049 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43203E-01 0.00017  3.94676E-01 0.00054 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.59633E-02 0.00025  9.36049E-02 0.00110 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.28127E-03 0.00296  2.81088E-02 0.00253 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02983E-02 0.00150 -8.76035E-03 0.00832 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.63499E-04 0.08028  6.65713E-03 0.00824 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.08517E-03 0.00377 -1.67926E-02 0.00296 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.41836E-04 0.01942  3.38004E-04 0.11469 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12979E-01 0.00028  1.00719E+00 0.00036 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56510E+00 0.00028  3.30954E-01 0.00036 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.77273E-03 0.00059  7.07193E-02 0.00110 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69785E-02 0.00023  7.20006E-02 0.00129 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11090E-01 0.00010  1.71677E-02 0.00039  1.24195E-03 0.00449  1.48283E+00 0.00049 ];
INF_S1                    (idx, [1:   8]) = [  2.38234E-01 0.00017  4.96901E-03 0.00092  5.33179E-04 0.00711  3.94143E-01 0.00055 ];
INF_S2                    (idx, [1:   8]) = [  9.75066E-02 0.00024 -1.54346E-03 0.00238  2.87479E-04 0.00920  9.33174E-02 0.00110 ];
INF_S3                    (idx, [1:   8]) = [  9.05586E-03 0.00228 -1.77456E-03 0.00158  1.00814E-04 0.02354  2.80080E-02 0.00256 ];
INF_S4                    (idx, [1:   8]) = [ -9.73604E-03 0.00169 -5.62270E-04 0.00468  3.59363E-07 1.00000 -8.76071E-03 0.00832 ];
INF_S5                    (idx, [1:   8]) = [  1.26615E-04 0.09677  3.70353E-05 0.06391 -4.43773E-05 0.03316  6.70150E-03 0.00810 ];
INF_S6                    (idx, [1:   8]) = [  5.21529E-03 0.00351 -1.30433E-04 0.01986 -5.75808E-05 0.02291 -1.67351E-02 0.00296 ];
INF_S7                    (idx, [1:   8]) = [  9.06002E-04 0.01668 -1.64118E-04 0.01513 -5.11488E-05 0.03076  3.89152E-04 0.09977 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11133E-01 0.00010  1.71677E-02 0.00039  1.24195E-03 0.00449  1.48283E+00 0.00049 ];
INF_SP1                   (idx, [1:   8]) = [  2.38234E-01 0.00017  4.96901E-03 0.00092  5.33179E-04 0.00711  3.94143E-01 0.00055 ];
INF_SP2                   (idx, [1:   8]) = [  9.75067E-02 0.00024 -1.54346E-03 0.00238  2.87479E-04 0.00920  9.33174E-02 0.00110 ];
INF_SP3                   (idx, [1:   8]) = [  9.05583E-03 0.00228 -1.77456E-03 0.00158  1.00814E-04 0.02354  2.80080E-02 0.00256 ];
INF_SP4                   (idx, [1:   8]) = [ -9.73608E-03 0.00169 -5.62270E-04 0.00468  3.59363E-07 1.00000 -8.76071E-03 0.00832 ];
INF_SP5                   (idx, [1:   8]) = [  1.26464E-04 0.09713  3.70353E-05 0.06391 -4.43773E-05 0.03316  6.70150E-03 0.00810 ];
INF_SP6                   (idx, [1:   8]) = [  5.21560E-03 0.00351 -1.30433E-04 0.01986 -5.75808E-05 0.02291 -1.67351E-02 0.00296 ];
INF_SP7                   (idx, [1:   8]) = [  9.05954E-04 0.01669 -1.64118E-04 0.01513 -5.11488E-05 0.03076  3.89152E-04 0.09977 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31545E-01 0.00065  1.18954E+00 0.00475 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33072E-01 0.00076  1.29085E+00 0.00828 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.32921E-01 0.00111  1.29184E+00 0.00727 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28702E-01 0.00089  1.02923E+00 0.00559 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43962E+00 0.00065  2.80374E-01 0.00482 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.43019E+00 0.00076  2.58658E-01 0.00838 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.43114E+00 0.00111  2.58353E-01 0.00718 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45753E+00 0.00089  3.24110E-01 0.00562 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.49093E-03 0.00906  1.53315E-04 0.05402  9.34804E-04 0.02168  8.68606E-04 0.02477  2.39963E-03 0.01385  8.71389E-04 0.02165  2.63185E-04 0.03904 ];
LAMBDA                    (idx, [1:  14]) = [  7.90169E-01 0.02112  1.25298E-02 0.00076  3.10487E-02 0.00063  1.10454E-01 0.00064  3.21894E-01 0.00044  1.31135E+00 0.00205  8.65347E+00 0.00790 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0010' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:42:04 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:12:37 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292124 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99119E-01  1.00143E+00  1.00257E+00  9.96456E-01  1.00042E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.7E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16507E-02 0.00110  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88349E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05720E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06157E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67371E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.39573E+01 0.00048  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.39490E+01 0.00047  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.16256E+00 0.00037  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.09011E-01 0.00112  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5000717 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00014E+04 0.00085 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00014E+04 0.00085 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.50391E+02 ;
RUNNING_TIME              (idx, 1)        =  9.05533E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.78167E-01  4.78167E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  2.99183E-01  1.13333E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  8.89505E+01  3.23377E+00  2.51518E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  8.18467E-01  2.67333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  3.78500E-02  8.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.05531E+01  1.19569E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97377 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99997E+00 0.00022 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79408E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6133.74;
MEMSIZE                   (idx, 1)        = 6039.76;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 169.50;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.98;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.42143E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.73146E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.21599E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.79531E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.25489E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.62609E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.60594E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.21061E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.65793E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.45502E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  7.77183E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.55590E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.88075E+07 ;
SR90_ACTIVITY             (idx, 1)        =  1.83878E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.32584E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.44563E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.47985E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.28487E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.35039E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.18128E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.33104E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  3.52426E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.25975E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.77117E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 15 ;
BURNUP                     (idx, [1:  2])  = [  1.75000E+01  1.75016E+01 ];
BURN_DAYS                 (idx, 1)        =  4.37500E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.18201E-01 0.00106 ];
U235_FISS                 (idx, [1:   4]) = [  7.02743E+15 0.00093  5.13310E-01 0.00072 ];
U238_FISS                 (idx, [1:   4]) = [  1.03971E+15 0.00272  7.59345E-02 0.00255 ];
PU239_FISS                (idx, [1:   4]) = [  4.84568E+15 0.00117  3.53945E-01 0.00101 ];
PU240_FISS                (idx, [1:   4]) = [  7.80863E+12 0.03116  5.70715E-04 0.03120 ];
PU241_FISS                (idx, [1:   4]) = [  7.53719E+14 0.00321  5.50507E-02 0.00312 ];
U235_CAPT                 (idx, [1:   4]) = [  1.58403E+15 0.00219  6.66444E-02 0.00211 ];
U238_CAPT                 (idx, [1:   4]) = [  8.87763E+15 0.00104  3.73487E-01 0.00073 ];
PU239_CAPT                (idx, [1:   4]) = [  2.69360E+15 0.00176  1.13323E-01 0.00162 ];
PU240_CAPT                (idx, [1:   4]) = [  1.68294E+15 0.00221  7.07988E-02 0.00204 ];
PU241_CAPT                (idx, [1:   4]) = [  2.71101E+14 0.00511  1.14075E-02 0.00514 ];
XE135_CAPT                (idx, [1:   4]) = [  7.38388E+14 0.00314  3.10698E-02 0.00316 ];
SM149_CAPT                (idx, [1:   4]) = [  2.20686E+14 0.00571  9.28395E-03 0.00565 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5000717 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.15838E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5000717 5.00816E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3146601 3.15137E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1812606 1.81527E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 41510 4.15134E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5000717 5.00816E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.14673E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50055E+05 2.0E-09  4.50055E+05 2.0E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.62537E+16 2.1E-05  3.62537E+16 2.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36951E+16 4.2E-06  1.36951E+16 4.2E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.37853E+16 0.00044  1.83352E+16 0.00045  5.45013E+15 0.00116 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.74805E+16 0.00028  3.20304E+16 0.00026  5.45013E+15 0.00116 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.77117E+16 0.00051  3.77117E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.74316E+18 0.00049  4.66914E+17 0.00048  1.27624E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.13150E+14 0.00544 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.77936E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.28218E+18 0.00063 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10480E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10480E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.55240E+00 0.00055 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.94733E-01 0.00032 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.29301E-01 0.00035 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24825E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94595E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97086E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.69070E-01 0.00061 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.61025E-01 0.00062 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.64720E+00 2.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05111E+02 4.2E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.60862E-01 0.00062  9.55937E-01 0.00062  5.08759E-03 0.01037 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.60818E-01 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  9.61462E-01 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.60818E-01 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  9.68857E-01 0.00028 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.71000E+01 0.00022 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70992E+01 9.2E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.51844E-07 0.00381 ];
IMP_EALF                  (idx, [1:   2]) = [  7.50285E-07 0.00159 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.59731E-01 0.00288 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.60742E-01 0.00111 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.71937E-03 0.00601  1.45933E-04 0.04040  9.94780E-04 0.01451  8.97617E-04 0.01440  2.49870E-03 0.00913  9.09268E-04 0.01474  2.73074E-04 0.02791 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.83237E-01 0.01453  9.51071E-03 0.02516  3.10095E-02 0.00045  1.10527E-01 0.00045  3.22116E-01 0.00032  1.30196E+00 0.00254  8.03631E+00 0.01429 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.32399E-03 0.00930  1.33542E-04 0.05831  9.33910E-04 0.02121  8.46303E-04 0.02273  2.32211E-03 0.01440  8.29919E-04 0.02218  2.58212E-04 0.04400 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.83154E-01 0.02228  1.25137E-02 0.00058  3.09850E-02 0.00062  1.10516E-01 0.00067  3.22107E-01 0.00047  1.30675E+00 0.00209  8.69194E+00 0.00803 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.46278E-05 0.00142  2.46174E-05 0.00142  2.66941E-05 0.01720 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.36592E-05 0.00126  2.36492E-05 0.00127  2.56383E-05 0.01717 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.29124E-03 0.01051  1.43011E-04 0.05917  8.95765E-04 0.02412  8.28559E-04 0.02526  2.31394E-03 0.01490  8.60324E-04 0.02421  2.49641E-04 0.04917 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.81348E-01 0.02591  1.25252E-02 0.00104  3.09711E-02 0.00082  1.10413E-01 0.00084  3.22119E-01 0.00059  1.30196E+00 0.00298  8.71713E+00 0.01016 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.38976E-05 0.00330  2.38845E-05 0.00330  2.52395E-05 0.03751 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.29564E-05 0.00320  2.29439E-05 0.00321  2.42279E-05 0.03745 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.35622E-03 0.03607  1.39042E-04 0.18651  9.44303E-04 0.08260  7.97168E-04 0.08991  2.26623E-03 0.05274  9.45122E-04 0.07984  2.64348E-04 0.15011 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.51985E-01 0.08002  1.25748E-02 0.00329  3.08872E-02 0.00198  1.10461E-01 0.00192  3.22436E-01 0.00151  1.31085E+00 0.00610  8.28068E+00 0.02920 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.34523E-03 0.03485  1.43311E-04 0.17463  9.37620E-04 0.08162  7.82212E-04 0.08921  2.25890E-03 0.05167  9.46464E-04 0.08088  2.76725E-04 0.15160 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.42990E-01 0.07820  1.25748E-02 0.00329  3.08890E-02 0.00197  1.10468E-01 0.00192  3.22418E-01 0.00148  1.31056E+00 0.00600  8.29292E+00 0.02907 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.24191E+02 0.03596 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.42977E-05 0.00090 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.33422E-05 0.00064 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.39638E-03 0.00677 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.22138E+02 0.00675 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.40196E-07 0.00077 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.69633E-06 0.00043  2.69622E-06 0.00043  2.71684E-06 0.00602 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.65223E-05 0.00093  3.65404E-05 0.00093  3.32290E-05 0.01131 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.27429E-01 0.00035  6.27496E-01 0.00035  6.26900E-01 0.00983 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06686E+01 0.01443 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.39490E+01 0.00047  3.26167E+01 0.00051 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.81356E+04 0.00323  3.01632E+05 0.00191  6.10590E+05 0.00098  6.51483E+05 0.00070  5.98826E+05 0.00067  6.42634E+05 0.00065  4.36029E+05 0.00089  3.85971E+05 0.00061  2.95829E+05 0.00066  2.41670E+05 0.00083  2.08444E+05 0.00076  1.87740E+05 0.00088  1.73342E+05 0.00083  1.64811E+05 0.00096  1.60615E+05 0.00115  1.38713E+05 0.00079  1.36811E+05 0.00100  1.35821E+05 0.00092  1.33523E+05 0.00082  2.60350E+05 0.00073  2.51488E+05 0.00076  1.80795E+05 0.00076  1.17092E+05 0.00104  1.35004E+05 0.00085  1.28141E+05 0.00086  1.14088E+05 0.00110  1.84817E+05 0.00091  4.27296E+04 0.00138  5.32698E+04 0.00145  4.85480E+04 0.00184  2.84140E+04 0.00247  4.87667E+04 0.00126  3.27084E+04 0.00163  2.67138E+04 0.00172  4.68179E+03 0.00355  4.06760E+03 0.00377  3.60824E+03 0.00316  3.41204E+03 0.00327  3.49332E+03 0.00325  3.86985E+03 0.00479  4.55581E+03 0.00248  4.56634E+03 0.00291  9.06149E+03 0.00348  1.48028E+04 0.00215  1.91063E+04 0.00186  5.11890E+04 0.00155  5.37568E+04 0.00176  5.70539E+04 0.00135  3.67307E+04 0.00165  2.56943E+04 0.00186  1.90887E+04 0.00176  2.23553E+04 0.00178  4.34286E+04 0.00162  6.09926E+04 0.00183  1.20856E+05 0.00163  1.89902E+05 0.00154  2.86301E+05 0.00186  1.84024E+05 0.00208  1.32138E+05 0.00178  9.51604E+04 0.00191  8.56159E+04 0.00185  8.45070E+04 0.00170  7.05813E+04 0.00214  4.77856E+04 0.00199  4.41847E+04 0.00195  3.93564E+04 0.00210  3.32355E+04 0.00222  2.62520E+04 0.00213  1.76269E+04 0.00248  6.23801E+03 0.00209 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.69511E-01 0.00046 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.40702E+18 0.00044  3.36175E+17 0.00162 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38405E-01 0.00011  1.56067E+00 0.00043 ];
INF_CAPT                  (idx, [1:   4]) = [  7.98347E-03 0.00060  3.73437E-02 0.00090 ];
INF_ABS                   (idx, [1:   4]) = [  9.90286E-03 0.00056  7.00555E-02 0.00120 ];
INF_FISS                  (idx, [1:   4]) = [  1.91939E-03 0.00047  3.27118E-02 0.00155 ];
INF_NSF                   (idx, [1:   4]) = [  5.11872E-03 0.00048  8.64368E-02 0.00160 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.66685E+00 4.9E-05  2.64237E+00 6.2E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05225E+02 6.5E-06  2.05083E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.57587E-08 0.00030  2.56788E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28508E-01 0.00012  1.49066E+00 0.00051 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43421E-01 0.00022  3.96398E-01 0.00054 ];
INF_SCATT2                (idx, [1:   4]) = [  9.60090E-02 0.00036  9.40123E-02 0.00086 ];
INF_SCATT3                (idx, [1:   4]) = [  7.22471E-03 0.00325  2.81556E-02 0.00180 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03194E-02 0.00201 -8.81131E-03 0.00584 ];
INF_SCATT5                (idx, [1:   4]) = [  1.48892E-04 0.09440  6.54155E-03 0.00875 ];
INF_SCATT6                (idx, [1:   4]) = [  5.09775E-03 0.00337 -1.69568E-02 0.00272 ];
INF_SCATT7                (idx, [1:   4]) = [  7.54180E-04 0.02166  4.23741E-04 0.10678 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28552E-01 0.00012  1.49066E+00 0.00051 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43421E-01 0.00021  3.96398E-01 0.00054 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.60092E-02 0.00036  9.40123E-02 0.00086 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.22448E-03 0.00326  2.81556E-02 0.00180 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03195E-02 0.00202 -8.81131E-03 0.00584 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.48632E-04 0.09443  6.54155E-03 0.00875 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.09787E-03 0.00337 -1.69568E-02 0.00272 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.54098E-04 0.02168  4.23741E-04 0.10678 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12815E-01 0.00035  1.01138E+00 0.00039 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56631E+00 0.00035  3.29584E-01 0.00039 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.85913E-03 0.00056  7.00555E-02 0.00120 ];
INF_REMXS                 (idx, [1:   4]) = [  2.69953E-02 0.00020  7.12415E-02 0.00136 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11410E-01 0.00012  1.70984E-02 0.00041  1.22984E-03 0.00431  1.48943E+00 0.00051 ];
INF_S1                    (idx, [1:   8]) = [  2.38468E-01 0.00021  4.95238E-03 0.00101  5.29235E-04 0.00651  3.95868E-01 0.00054 ];
INF_S2                    (idx, [1:   8]) = [  9.75578E-02 0.00035 -1.54881E-03 0.00203  2.83489E-04 0.00863  9.37288E-02 0.00086 ];
INF_S3                    (idx, [1:   8]) = [  8.99828E-03 0.00264 -1.77357E-03 0.00192  1.01447E-04 0.02535  2.80541E-02 0.00180 ];
INF_S4                    (idx, [1:   8]) = [ -9.76622E-03 0.00217 -5.53139E-04 0.00475 -1.04515E-07 1.00000 -8.81121E-03 0.00583 ];
INF_S5                    (idx, [1:   8]) = [  1.09551E-04 0.13574  3.93411E-05 0.06271 -4.00632E-05 0.04755  6.58162E-03 0.00868 ];
INF_S6                    (idx, [1:   8]) = [  5.22895E-03 0.00317 -1.31205E-04 0.01891 -5.13388E-05 0.03331 -1.69055E-02 0.00271 ];
INF_S7                    (idx, [1:   8]) = [  9.23236E-04 0.01733 -1.69056E-04 0.01408 -4.83788E-05 0.03292  4.72119E-04 0.09647 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11454E-01 0.00012  1.70984E-02 0.00041  1.22984E-03 0.00431  1.48943E+00 0.00051 ];
INF_SP1                   (idx, [1:   8]) = [  2.38468E-01 0.00021  4.95238E-03 0.00101  5.29235E-04 0.00651  3.95868E-01 0.00054 ];
INF_SP2                   (idx, [1:   8]) = [  9.75581E-02 0.00035 -1.54881E-03 0.00203  2.83489E-04 0.00863  9.37288E-02 0.00086 ];
INF_SP3                   (idx, [1:   8]) = [  8.99804E-03 0.00265 -1.77357E-03 0.00192  1.01447E-04 0.02535  2.80541E-02 0.00180 ];
INF_SP4                   (idx, [1:   8]) = [ -9.76636E-03 0.00217 -5.53139E-04 0.00475 -1.04515E-07 1.00000 -8.81121E-03 0.00583 ];
INF_SP5                   (idx, [1:   8]) = [  1.09291E-04 0.13582  3.93411E-05 0.06271 -4.00632E-05 0.04755  6.58162E-03 0.00868 ];
INF_SP6                   (idx, [1:   8]) = [  5.22908E-03 0.00317 -1.31205E-04 0.01891 -5.13388E-05 0.03331 -1.69055E-02 0.00271 ];
INF_SP7                   (idx, [1:   8]) = [  9.23154E-04 0.01736 -1.69056E-04 0.01408 -4.83788E-05 0.03292  4.72119E-04 0.09647 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31752E-01 0.00069  1.18383E+00 0.00693 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33440E-01 0.00085  1.28078E+00 0.00968 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33355E-01 0.00113  1.28432E+00 0.00899 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28537E-01 0.00105  1.02695E+00 0.00529 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43834E+00 0.00069  2.81894E-01 0.00687 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42794E+00 0.00084  2.60825E-01 0.00937 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42848E+00 0.00113  2.60051E-01 0.00908 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45859E+00 0.00104  3.24805E-01 0.00529 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.32399E-03 0.00930  1.33542E-04 0.05831  9.33910E-04 0.02121  8.46303E-04 0.02273  2.32211E-03 0.01440  8.29919E-04 0.02218  2.58212E-04 0.04400 ];
LAMBDA                    (idx, [1:  14]) = [  7.83154E-01 0.02228  1.25137E-02 0.00058  3.09850E-02 0.00062  1.10516E-01 0.00067  3.22107E-01 0.00047  1.30675E+00 0.00209  8.69194E+00 0.00803 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0010' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:42:04 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:18:28 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292124 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.98870E-01  1.00059E+00  1.00384E+00  9.97407E-01  9.99285E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16485E-02 0.00116  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88352E-01 1.4E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05895E-01 0.00011  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06332E-01 0.00011  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.67475E+00 0.00024  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.39820E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.39736E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.15931E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.09108E-01 0.00115  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001253 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00025E+04 0.00091 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00025E+04 0.00091 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  4.79596E+02 ;
RUNNING_TIME              (idx, 1)        =  9.64011E+01 ;
INIT_TIME                 (idx, [1:  2])  = [  4.78167E-01  4.78167E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.23050E-01  1.12000E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  9.47193E+01  3.24548E+00  2.52332E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  8.73050E-01  2.70667E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.04667E-02  9.16668E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  9.64009E+01  1.19735E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97501 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99963E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.79732E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6133.74;
MEMSIZE                   (idx, 1)        = 6039.76;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 169.50;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.98;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.47708E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.72471E+04 ;
TOT_SF_RATE               (idx, 1)        =  5.17937E+05 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.84659E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.29201E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.63047E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.59548E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  2.62330E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.73744E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  1.83530E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.06444E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  7.88002E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.93100E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.05472E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.34781E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.47477E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.50999E+14 ;
CS134_ACTIVITY            (idx, 1)        =  2.91936E+13 ;
CS137_ACTIVITY            (idx, 1)        =  2.68291E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.23348E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.30906E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  5.21438E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.27464E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  3.88149E+12 0.00050  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 16 ;
BURNUP                     (idx, [1:  2])  = [  2.00000E+01  2.00018E+01 ];
BURN_DAYS                 (idx, 1)        =  5.00000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.40703E-01 0.00103 ];
U235_FISS                 (idx, [1:   4]) = [  6.49758E+15 0.00105  4.74299E-01 0.00085 ];
U238_FISS                 (idx, [1:   4]) = [  1.07451E+15 0.00278  7.84247E-02 0.00262 ];
PU239_FISS                (idx, [1:   4]) = [  5.18038E+15 0.00115  3.78144E-01 0.00096 ];
PU240_FISS                (idx, [1:   4]) = [  9.14635E+12 0.02897  6.67119E-04 0.02892 ];
PU241_FISS                (idx, [1:   4]) = [  9.18609E+14 0.00296  6.70491E-02 0.00284 ];
U235_CAPT                 (idx, [1:   4]) = [  1.46609E+15 0.00235  5.90006E-02 0.00232 ];
U238_CAPT                 (idx, [1:   4]) = [  9.10419E+15 0.00103  3.66352E-01 0.00075 ];
PU239_CAPT                (idx, [1:   4]) = [  2.86502E+15 0.00161  1.15298E-01 0.00155 ];
PU240_CAPT                (idx, [1:   4]) = [  1.89209E+15 0.00208  7.61384E-02 0.00197 ];
PU241_CAPT                (idx, [1:   4]) = [  3.32130E+14 0.00527  1.33655E-02 0.00524 ];
XE135_CAPT                (idx, [1:   4]) = [  7.51197E+14 0.00316  3.02301E-02 0.00312 ];
SM149_CAPT                (idx, [1:   4]) = [  2.24475E+14 0.00605  9.03499E-03 0.00609 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001253 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 7.88708E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001253 5.00789E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3196837 3.20110E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1762449 1.76481E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 41967 4.19759E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001253 5.00789E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -9.03383E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50055E+05 2.0E-09  4.50055E+05 2.0E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.64501E+16 2.1E-05  3.64501E+16 2.1E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36781E+16 4.3E-06  1.36781E+16 4.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.48551E+16 0.00044  1.92309E+16 0.00045  5.62419E+15 0.00124 ];
TOT_ABSRATE               (idx, [1:   6]) = [  3.85332E+16 0.00028  3.29090E+16 0.00026  5.62419E+15 0.00124 ];
TOT_SRCRATE               (idx, [1:   6]) = [  3.88149E+16 0.00050  3.88149E+16 0.00050  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.79356E+18 0.00048  4.79670E+17 0.00048  1.31389E+18 0.00054 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.25882E+14 0.00518 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  3.88591E+16 0.00029 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.32057E+18 0.00065 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.10190E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.10190E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.53283E+00 0.00055 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.92888E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.25841E-01 0.00034 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24712E+00 0.00037 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94535E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97054E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.48517E-01 0.00062 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.40553E-01 0.00061 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.66486E+00 2.5E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05367E+02 4.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.40486E-01 0.00062  9.35700E-01 0.00062  4.85346E-03 0.01078 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.39547E-01 0.00029 ];
COL_KEFF                  (idx, [1:   2]) = [  9.39191E-01 0.00050 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.39547E-01 0.00029 ];
ABS_KINF                  (idx, [1:   2]) = [  9.47504E-01 0.00029 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70620E+01 0.00023 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70630E+01 9.5E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  7.81179E-07 0.00394 ];
IMP_EALF                  (idx, [1:   2]) = [  7.77955E-07 0.00163 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.68428E-01 0.00288 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.68420E-01 0.00122 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.72262E-03 0.00648  1.43906E-04 0.03665  1.02429E-03 0.01467  9.16450E-04 0.01545  2.44877E-03 0.00942  9.01901E-04 0.01489  2.87307E-04 0.02560 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.97748E-01 0.01397  9.38036E-03 0.02600  3.09221E-02 0.00046  1.10668E-01 0.00052  3.22506E-01 0.00034  1.29968E+00 0.00169  8.05573E+00 0.01333 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.21820E-03 0.00925  1.34729E-04 0.05659  9.36685E-04 0.02081  8.31799E-04 0.02366  2.23683E-03 0.01395  8.13791E-04 0.02410  2.64365E-04 0.04210 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.97152E-01 0.02258  1.25344E-02 0.00081  3.09272E-02 0.00065  1.10603E-01 0.00069  3.22667E-01 0.00052  1.30325E+00 0.00223  8.50060E+00 0.00916 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.51844E-05 0.00151  2.51741E-05 0.00151  2.73661E-05 0.01771 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.36802E-05 0.00133  2.36705E-05 0.00133  2.57379E-05 0.01770 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.15162E-03 0.01100  1.45316E-04 0.06034  9.14726E-04 0.02469  8.27809E-04 0.02567  2.19944E-03 0.01594  8.17327E-04 0.02786  2.47001E-04 0.04685 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.79002E-01 0.02601  1.25307E-02 0.00108  3.09344E-02 0.00085  1.10571E-01 0.00089  3.22649E-01 0.00062  1.30283E+00 0.00290  8.54771E+00 0.01315 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.45627E-05 0.00337  2.45495E-05 0.00336  2.49790E-05 0.04323 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.30956E-05 0.00329  2.30833E-05 0.00329  2.34689E-05 0.04314 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.04873E-03 0.03559  1.44425E-04 0.19748  9.36226E-04 0.08037  7.17007E-04 0.09213  2.16008E-03 0.05400  8.46422E-04 0.09125  2.44561E-04 0.15378 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.71549E-01 0.08320  1.25500E-02 0.00310  3.08747E-02 0.00195  1.10828E-01 0.00221  3.22649E-01 0.00165  1.28821E+00 0.00825  8.43319E+00 0.02989 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.00073E-03 0.03503  1.38755E-04 0.19261  9.30526E-04 0.07895  7.08350E-04 0.09074  2.14416E-03 0.05309  8.36690E-04 0.08829  2.42245E-04 0.14896 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.72222E-01 0.08216  1.25485E-02 0.00309  3.08751E-02 0.00195  1.10828E-01 0.00220  3.22658E-01 0.00164  1.28796E+00 0.00823  8.43277E+00 0.02990 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -2.06609E+02 0.03589 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.48358E-05 0.00091 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.33532E-05 0.00066 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.08565E-03 0.00731 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -2.04916E+02 0.00744 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.41053E-07 0.00081 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.68043E-06 0.00045  2.68012E-06 0.00045  2.73501E-06 0.00601 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.67837E-05 0.00096  3.68020E-05 0.00096  3.34620E-05 0.01131 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.23991E-01 0.00034  6.24138E-01 0.00034  6.10459E-01 0.00985 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.09047E+01 0.01427 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.39736E+01 0.00049  3.25729E+01 0.00053 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.86508E+04 0.00303  3.03243E+05 0.00118  6.11173E+05 0.00072  6.51597E+05 0.00060  5.98807E+05 0.00048  6.42418E+05 0.00072  4.35757E+05 0.00083  3.86559E+05 0.00102  2.95320E+05 0.00074  2.40968E+05 0.00056  2.08012E+05 0.00096  1.87422E+05 0.00087  1.73187E+05 0.00107  1.64816E+05 0.00110  1.60563E+05 0.00107  1.38646E+05 0.00115  1.37204E+05 0.00076  1.35483E+05 0.00095  1.33387E+05 0.00095  2.60116E+05 0.00067  2.51196E+05 0.00086  1.81005E+05 0.00078  1.17181E+05 0.00107  1.35010E+05 0.00075  1.28205E+05 0.00098  1.14058E+05 0.00088  1.84591E+05 0.00055  4.26814E+04 0.00171  5.31521E+04 0.00168  4.82713E+04 0.00130  2.83137E+04 0.00201  4.87539E+04 0.00146  3.25180E+04 0.00174  2.63950E+04 0.00172  4.57169E+03 0.00345  3.95263E+03 0.00353  3.44377E+03 0.00329  3.31635E+03 0.00322  3.37959E+03 0.00431  3.74484E+03 0.00455  4.42001E+03 0.00233  4.46687E+03 0.00383  8.84666E+03 0.00265  1.45944E+04 0.00266  1.90083E+04 0.00146  5.06898E+04 0.00164  5.32915E+04 0.00123  5.64291E+04 0.00129  3.63202E+04 0.00183  2.54107E+04 0.00165  1.88311E+04 0.00170  2.20848E+04 0.00166  4.30658E+04 0.00170  6.05857E+04 0.00178  1.20488E+05 0.00178  1.89403E+05 0.00168  2.86480E+05 0.00148  1.84479E+05 0.00156  1.32665E+05 0.00181  9.55884E+04 0.00182  8.60289E+04 0.00187  8.48179E+04 0.00179  7.08600E+04 0.00201  4.80648E+04 0.00189  4.43960E+04 0.00174  3.95633E+04 0.00217  3.34759E+04 0.00165  2.64135E+04 0.00216  1.76852E+04 0.00174  6.26566E+03 0.00279 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.47142E-01 0.00055 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.44762E+18 0.00058  3.45976E+17 0.00153 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38487E-01 9.8E-05  1.56387E+00 0.00040 ];
INF_CAPT                  (idx, [1:   4]) = [  8.12619E-03 0.00061  3.78437E-02 0.00091 ];
INF_ABS                   (idx, [1:   4]) = [  9.98608E-03 0.00053  6.96027E-02 0.00118 ];
INF_FISS                  (idx, [1:   4]) = [  1.85989E-03 0.00050  3.17590E-02 0.00152 ];
INF_NSF                   (idx, [1:   4]) = [  4.98594E-03 0.00052  8.45096E-02 0.00155 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.68077E+00 6.8E-05  2.66096E+00 5.7E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05420E+02 8.9E-06  2.05354E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.54878E-08 0.00046  2.57288E-06 0.00017 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28497E-01 0.00011  1.49424E+00 0.00048 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43409E-01 0.00023  3.97096E-01 0.00058 ];
INF_SCATT2                (idx, [1:   4]) = [  9.60490E-02 0.00033  9.40476E-02 0.00101 ];
INF_SCATT3                (idx, [1:   4]) = [  7.24154E-03 0.00245  2.82407E-02 0.00263 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.02939E-02 0.00181 -8.85251E-03 0.00680 ];
INF_SCATT5                (idx, [1:   4]) = [  2.08715E-04 0.07138  6.64826E-03 0.00831 ];
INF_SCATT6                (idx, [1:   4]) = [  5.12109E-03 0.00325 -1.70967E-02 0.00307 ];
INF_SCATT7                (idx, [1:   4]) = [  7.73712E-04 0.02253  3.88254E-04 0.12572 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28539E-01 0.00011  1.49424E+00 0.00048 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43410E-01 0.00023  3.97096E-01 0.00058 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.60495E-02 0.00033  9.40476E-02 0.00101 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.24159E-03 0.00246  2.82407E-02 0.00263 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.02940E-02 0.00181 -8.85251E-03 0.00680 ];
INF_SCATTP5               (idx, [1:   4]) = [  2.08540E-04 0.07140  6.64826E-03 0.00831 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.12095E-03 0.00325 -1.70967E-02 0.00307 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.73568E-04 0.02252  3.88254E-04 0.12572 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12560E-01 0.00027  1.01418E+00 0.00033 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56819E+00 0.00027  3.28674E-01 0.00033 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  9.94378E-03 0.00054  6.96027E-02 0.00118 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70004E-02 0.00021  7.08470E-02 0.00123 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11487E-01 1.0E-04  1.70101E-02 0.00053  1.22082E-03 0.00573  1.49302E+00 0.00048 ];
INF_S1                    (idx, [1:   8]) = [  2.38492E-01 0.00023  4.91745E-03 0.00087  5.27272E-04 0.00871  3.96569E-01 0.00058 ];
INF_S2                    (idx, [1:   8]) = [  9.75931E-02 0.00032 -1.54404E-03 0.00258  2.89064E-04 0.00788  9.37586E-02 0.00101 ];
INF_S3                    (idx, [1:   8]) = [  9.00409E-03 0.00195 -1.76254E-03 0.00157  1.00994E-04 0.02358  2.81397E-02 0.00264 ];
INF_S4                    (idx, [1:   8]) = [ -9.74308E-03 0.00190 -5.50867E-04 0.00314  1.50463E-06 1.00000 -8.85402E-03 0.00681 ];
INF_S5                    (idx, [1:   8]) = [  1.65290E-04 0.08561  4.34254E-05 0.06099 -4.14202E-05 0.03889  6.68968E-03 0.00831 ];
INF_S6                    (idx, [1:   8]) = [  5.25179E-03 0.00316 -1.30698E-04 0.01854 -5.01763E-05 0.02714 -1.70465E-02 0.00307 ];
INF_S7                    (idx, [1:   8]) = [  9.40284E-04 0.01827 -1.66572E-04 0.01158 -4.82423E-05 0.03100  4.36496E-04 0.11088 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.11529E-01 0.00010  1.70101E-02 0.00053  1.22082E-03 0.00573  1.49302E+00 0.00048 ];
INF_SP1                   (idx, [1:   8]) = [  2.38493E-01 0.00023  4.91745E-03 0.00087  5.27272E-04 0.00871  3.96569E-01 0.00058 ];
INF_SP2                   (idx, [1:   8]) = [  9.75935E-02 0.00032 -1.54404E-03 0.00258  2.89064E-04 0.00788  9.37586E-02 0.00101 ];
INF_SP3                   (idx, [1:   8]) = [  9.00413E-03 0.00195 -1.76254E-03 0.00157  1.00994E-04 0.02358  2.81397E-02 0.00264 ];
INF_SP4                   (idx, [1:   8]) = [ -9.74310E-03 0.00190 -5.50867E-04 0.00314  1.50463E-06 1.00000 -8.85402E-03 0.00681 ];
INF_SP5                   (idx, [1:   8]) = [  1.65115E-04 0.08568  4.34254E-05 0.06099 -4.14202E-05 0.03889  6.68968E-03 0.00831 ];
INF_SP6                   (idx, [1:   8]) = [  5.25165E-03 0.00316 -1.30698E-04 0.01854 -5.01763E-05 0.02714 -1.70465E-02 0.00307 ];
INF_SP7                   (idx, [1:   8]) = [  9.40140E-04 0.01827 -1.66572E-04 0.01158 -4.82423E-05 0.03100  4.36496E-04 0.11088 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31477E-01 0.00051  1.21278E+00 0.00748 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33038E-01 0.00086  1.31269E+00 0.00872 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33147E-01 0.00083  1.32844E+00 0.01124 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28321E-01 0.00100  1.04442E+00 0.00723 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.44004E+00 0.00051  2.75224E-01 0.00755 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.43041E+00 0.00086  2.54406E-01 0.00893 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42974E+00 0.00083  2.51710E-01 0.01165 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45997E+00 0.00100  3.19556E-01 0.00719 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.21820E-03 0.00925  1.34729E-04 0.05659  9.36685E-04 0.02081  8.31799E-04 0.02366  2.23683E-03 0.01395  8.13791E-04 0.02410  2.64365E-04 0.04210 ];
LAMBDA                    (idx, [1:  14]) = [  7.97152E-01 0.02258  1.25344E-02 0.00081  3.09272E-02 0.00065  1.10603E-01 0.00069  3.22667E-01 0.00052  1.30325E+00 0.00223  8.50060E+00 0.00916 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0010' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:42:04 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:24:19 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292124 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.98197E-01  9.99949E-01  1.00359E+00  9.97074E-01  1.00119E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 6.6E-10  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16806E-02 0.00109  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88319E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06253E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06689E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68038E+00 0.00022  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.41200E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.41114E+01 0.00050  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.17360E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.12160E-01 0.00112  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001678 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00034E+04 0.00098 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00034E+04 0.00098 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.08823E+02 ;
RUNNING_TIME              (idx, 1)        =  1.02253E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.78167E-01  4.78167E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.45883E-01  1.13667E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.00494E+02  3.24115E+00  2.53317E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  9.27217E-01  2.61833E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.29500E-02  7.83336E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.02253E+02  1.19814E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97613 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00019E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80020E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6133.74;
MEMSIZE                   (idx, 1)        = 6039.76;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 169.50;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.98;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.48013E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68285E+04 ;
TOT_SF_RATE               (idx, 1)        =  1.13683E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  1.92347E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.34930E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.55663E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.54789E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  3.59276E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  4.84627E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  2.75106E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  8.56996E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  8.41709E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  3.98928E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.45962E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.31087E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.47263E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.48551E+14 ;
CS134_ACTIVITY            (idx, 1)        =  4.36717E+13 ;
CS137_ACTIVITY            (idx, 1)        =  3.34460E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.24064E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.25460E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  9.68914E+12 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.28863E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.08375E+12 0.00051  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 17 ;
BURNUP                     (idx, [1:  2])  = [  2.50000E+01  2.50023E+01 ];
BURN_DAYS                 (idx, 1)        =  6.25000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  6.85753E-01 0.00103 ];
U235_FISS                 (idx, [1:   4]) = [  5.52447E+15 0.00111  4.04996E-01 0.00093 ];
U238_FISS                 (idx, [1:   4]) = [  1.12495E+15 0.00270  8.24586E-02 0.00253 ];
PU239_FISS                (idx, [1:   4]) = [  5.70951E+15 0.00113  4.18556E-01 0.00093 ];
PU240_FISS                (idx, [1:   4]) = [  1.16196E+13 0.02871  8.51726E-04 0.02870 ];
PU241_FISS                (idx, [1:   4]) = [  1.24586E+15 0.00244  9.13341E-02 0.00237 ];
U235_CAPT                 (idx, [1:   4]) = [  1.25022E+15 0.00266  4.64620E-02 0.00261 ];
U238_CAPT                 (idx, [1:   4]) = [  9.54685E+15 0.00105  3.54758E-01 0.00074 ];
PU239_CAPT                (idx, [1:   4]) = [  3.15835E+15 0.00154  1.17370E-01 0.00143 ];
PU240_CAPT                (idx, [1:   4]) = [  2.26706E+15 0.00204  8.42434E-02 0.00190 ];
PU241_CAPT                (idx, [1:   4]) = [  4.43923E+14 0.00438  1.64977E-02 0.00437 ];
XE135_CAPT                (idx, [1:   4]) = [  7.60633E+14 0.00318  2.82687E-02 0.00318 ];
SM149_CAPT                (idx, [1:   4]) = [  2.38796E+14 0.00588  8.87304E-03 0.00580 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001678 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.01626E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001678 5.00802E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3290480 3.29472E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1668164 1.67026E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 43034 4.30351E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001678 5.00802E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -8.47504E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50055E+05 2.0E-09  4.50055E+05 2.0E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.67996E+16 2.0E-05  3.67996E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36473E+16 4.0E-06  1.36473E+16 4.0E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.69102E+16 0.00047  2.09338E+16 0.00046  5.97644E+15 0.00126 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.05575E+16 0.00031  3.45811E+16 0.00028  5.97644E+15 0.00126 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.08375E+16 0.00051  4.08375E+16 0.00051  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.88881E+18 0.00049  5.03544E+17 0.00046  1.38527E+18 0.00056 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.51528E+14 0.00480 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.09091E+16 0.00032 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.39505E+18 0.00067 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.09612E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.09612E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.49296E+00 0.00059 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.88494E-01 0.00033 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.20237E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24448E+00 0.00038 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94374E-01 3.2E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.97002E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  9.08569E-01 0.00066 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  9.00749E-01 0.00066 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.69648E+00 2.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.05830E+02 4.0E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  9.00565E-01 0.00066  8.96184E-01 0.00066  4.56526E-03 0.01067 ];
IMP_KEFF                  (idx, [1:   2]) = [  9.01035E-01 0.00032 ];
COL_KEFF                  (idx, [1:   2]) = [  9.01241E-01 0.00051 ];
ABS_KEFF                  (idx, [1:   2]) = [  9.01035E-01 0.00032 ];
ABS_KINF                  (idx, [1:   2]) = [  9.08855E-01 0.00031 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.70062E+01 0.00024 ];
IMP_ALF                   (idx, [1:   2]) = [  1.70045E+01 9.9E-05 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  8.26256E-07 0.00404 ];
IMP_EALF                  (idx, [1:   2]) = [  8.24880E-07 0.00168 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.82100E-01 0.00283 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.82085E-01 0.00113 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.83861E-03 0.00626  1.42690E-04 0.03984  1.05826E-03 0.01571  8.94433E-04 0.01570  2.53490E-03 0.00933  9.44127E-04 0.01539  2.64203E-04 0.02997 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.45957E-01 0.01555  8.94340E-03 0.02849  3.07989E-02 0.00044  1.10914E-01 0.00054  3.22538E-01 0.00033  1.27618E+00 0.00198  7.53059E+00 0.01750 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  5.10277E-03 0.00938  1.12424E-04 0.06226  9.33268E-04 0.02286  7.69080E-04 0.02636  2.21842E-03 0.01375  8.30991E-04 0.02367  2.38581E-04 0.04443 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.61684E-01 0.02300  1.25612E-02 0.00106  3.07817E-02 0.00063  1.11003E-01 0.00077  3.22627E-01 0.00049  1.27684E+00 0.00299  8.24110E+00 0.01208 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.65275E-05 0.00156  2.65138E-05 0.00156  2.92108E-05 0.01702 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.38849E-05 0.00143  2.38725E-05 0.00143  2.63005E-05 0.01700 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  5.06997E-03 0.01074  1.25758E-04 0.07056  8.95942E-04 0.02674  7.66714E-04 0.02766  2.21657E-03 0.01580  8.33038E-04 0.02580  2.31950E-04 0.05117 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.48232E-01 0.02751  1.25711E-02 0.00171  3.07761E-02 0.00088  1.10958E-01 0.00108  3.22650E-01 0.00062  1.27757E+00 0.00394  8.16293E+00 0.01730 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.56569E-05 0.00358  2.56525E-05 0.00360  2.37611E-05 0.03875 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.30989E-05 0.00348  2.30948E-05 0.00349  2.13915E-05 0.03880 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  5.03199E-03 0.03650  1.18728E-04 0.22027  8.05056E-04 0.08887  8.03729E-04 0.08842  2.30969E-03 0.05479  7.62449E-04 0.08746  2.32338E-04 0.16241 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.61392E-01 0.08583  1.26474E-02 0.00527  3.07734E-02 0.00211  1.10843E-01 0.00226  3.21614E-01 0.00164  1.27575E+00 0.00961  8.22332E+00 0.03779 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  5.04256E-03 0.03607  1.25228E-04 0.21835  7.83121E-04 0.08688  8.04925E-04 0.08393  2.33378E-03 0.05461  7.51342E-04 0.08695  2.44158E-04 0.16356 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.69020E-01 0.08528  1.26474E-02 0.00527  3.07657E-02 0.00210  1.10846E-01 0.00225  3.21715E-01 0.00164  1.27530E+00 0.00963  8.23697E+00 0.03757 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.97059E+02 0.03658 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.61568E-05 0.00092 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.35505E-05 0.00062 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  5.11065E-03 0.00675 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.95470E+02 0.00681 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.44685E-07 0.00082 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.65713E-06 0.00041  2.65703E-06 0.00042  2.67242E-06 0.00579 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.74432E-05 0.00099  3.74583E-05 0.00099  3.45709E-05 0.01144 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.18344E-01 0.00036  6.18620E-01 0.00036  5.82306E-01 0.01003 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.06753E+01 0.01389 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.41114E+01 0.00050  3.26295E+01 0.00055 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  7.96732E+04 0.00250  3.05042E+05 0.00152  6.11132E+05 0.00066  6.51897E+05 0.00075  5.98063E+05 0.00062  6.42034E+05 0.00067  4.36171E+05 0.00056  3.85694E+05 0.00063  2.95354E+05 0.00058  2.41219E+05 0.00075  2.08474E+05 0.00086  1.87598E+05 0.00078  1.72859E+05 0.00097  1.64569E+05 0.00080  1.60513E+05 0.00088  1.38490E+05 0.00074  1.37108E+05 0.00094  1.35877E+05 0.00101  1.33336E+05 0.00076  2.60018E+05 0.00068  2.51470E+05 0.00081  1.81115E+05 0.00072  1.17521E+05 0.00077  1.34951E+05 0.00084  1.28393E+05 0.00109  1.14076E+05 0.00086  1.83905E+05 0.00072  4.26059E+04 0.00147  5.28216E+04 0.00135  4.81889E+04 0.00172  2.82532E+04 0.00206  4.86506E+04 0.00126  3.20682E+04 0.00134  2.60266E+04 0.00196  4.43525E+03 0.00276  3.78248E+03 0.00416  3.29253E+03 0.00392  3.17021E+03 0.00319  3.21370E+03 0.00380  3.54598E+03 0.00445  4.28981E+03 0.00268  4.37014E+03 0.00403  8.62084E+03 0.00294  1.43078E+04 0.00251  1.86474E+04 0.00238  5.01646E+04 0.00156  5.26159E+04 0.00136  5.58468E+04 0.00114  3.60306E+04 0.00147  2.52132E+04 0.00185  1.85121E+04 0.00174  2.19035E+04 0.00162  4.26775E+04 0.00159  6.04271E+04 0.00149  1.20461E+05 0.00164  1.90501E+05 0.00168  2.88923E+05 0.00161  1.86351E+05 0.00172  1.33926E+05 0.00175  9.65907E+04 0.00183  8.70347E+04 0.00172  8.58080E+04 0.00187  7.18964E+04 0.00184  4.86936E+04 0.00185  4.50132E+04 0.00207  4.00569E+04 0.00183  3.39127E+04 0.00212  2.68091E+04 0.00198  1.79561E+04 0.00225  6.38514E+03 0.00269 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  9.09065E-01 0.00052 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.52263E+18 0.00052  3.66221E+17 0.00162 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.38970E-01 0.00011  1.56851E+00 0.00044 ];
INF_CAPT                  (idx, [1:   4]) = [  8.38242E-03 0.00073  3.86340E-02 0.00090 ];
INF_ABS                   (idx, [1:   4]) = [  1.01355E-02 0.00062  6.86170E-02 0.00121 ];
INF_FISS                  (idx, [1:   4]) = [  1.75310E-03 0.00045  2.99829E-02 0.00162 ];
INF_NSF                   (idx, [1:   4]) = [  4.74446E-03 0.00045  8.07765E-02 0.00165 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.70633E+00 4.4E-05  2.69408E+00 4.1E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.05779E+02 5.9E-06  2.05842E+02 7.9E-06 ];
INF_INVV                  (idx, [1:   4]) = [  5.51086E-08 0.00032  2.58034E-06 0.00019 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.28829E-01 0.00011  1.49994E+00 0.00051 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43591E-01 0.00018  3.98361E-01 0.00059 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61551E-02 0.00021  9.41978E-02 0.00092 ];
INF_SCATT3                (idx, [1:   4]) = [  7.26314E-03 0.00266  2.82517E-02 0.00237 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03049E-02 0.00173 -8.84145E-03 0.00636 ];
INF_SCATT5                (idx, [1:   4]) = [  1.82514E-04 0.07726  6.63269E-03 0.00742 ];
INF_SCATT6                (idx, [1:   4]) = [  5.11052E-03 0.00335 -1.71852E-02 0.00272 ];
INF_SCATT7                (idx, [1:   4]) = [  7.45683E-04 0.02351  4.93599E-04 0.11628 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.28872E-01 0.00011  1.49994E+00 0.00051 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43591E-01 0.00018  3.98361E-01 0.00059 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61553E-02 0.00021  9.41978E-02 0.00092 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.26330E-03 0.00266  2.82517E-02 0.00237 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03050E-02 0.00173 -8.84145E-03 0.00636 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.82489E-04 0.07731  6.63269E-03 0.00742 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.11066E-03 0.00336 -1.71852E-02 0.00272 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.45648E-04 0.02349  4.93599E-04 0.11628 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12616E-01 0.00028  1.01814E+00 0.00039 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56778E+00 0.00028  3.27396E-01 0.00039 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.00925E-02 0.00062  6.86170E-02 0.00121 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70005E-02 0.00017  6.97681E-02 0.00127 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.11969E-01 0.00011  1.68597E-02 0.00040  1.19730E-03 0.00513  1.49874E+00 0.00051 ];
INF_S1                    (idx, [1:   8]) = [  2.38714E-01 0.00018  4.87706E-03 0.00102  5.10837E-04 0.00739  3.97850E-01 0.00060 ];
INF_S2                    (idx, [1:   8]) = [  9.76860E-02 0.00022 -1.53094E-03 0.00224  2.81842E-04 0.00898  9.39160E-02 0.00092 ];
INF_S3                    (idx, [1:   8]) = [  9.01400E-03 0.00200 -1.75086E-03 0.00130  1.01403E-04 0.02035  2.81503E-02 0.00239 ];
INF_S4                    (idx, [1:   8]) = [ -9.75984E-03 0.00181 -5.45098E-04 0.00421  1.23456E-06 1.00000 -8.84269E-03 0.00640 ];
INF_S5                    (idx, [1:   8]) = [  1.39188E-04 0.10425  4.33259E-05 0.05529 -4.13604E-05 0.03550  6.67405E-03 0.00737 ];
INF_S6                    (idx, [1:   8]) = [  5.24097E-03 0.00332 -1.30452E-04 0.01684 -5.16094E-05 0.02826 -1.71336E-02 0.00274 ];
INF_S7                    (idx, [1:   8]) = [  9.10218E-04 0.01943 -1.64535E-04 0.01164 -4.83498E-05 0.02975  5.41949E-04 0.10577 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12012E-01 0.00011  1.68597E-02 0.00040  1.19730E-03 0.00513  1.49874E+00 0.00051 ];
INF_SP1                   (idx, [1:   8]) = [  2.38714E-01 0.00018  4.87706E-03 0.00102  5.10837E-04 0.00739  3.97850E-01 0.00060 ];
INF_SP2                   (idx, [1:   8]) = [  9.76862E-02 0.00022 -1.53094E-03 0.00224  2.81842E-04 0.00898  9.39160E-02 0.00092 ];
INF_SP3                   (idx, [1:   8]) = [  9.01416E-03 0.00200 -1.75086E-03 0.00130  1.01403E-04 0.02035  2.81503E-02 0.00239 ];
INF_SP4                   (idx, [1:   8]) = [ -9.75987E-03 0.00181 -5.45098E-04 0.00421  1.23456E-06 1.00000 -8.84269E-03 0.00640 ];
INF_SP5                   (idx, [1:   8]) = [  1.39163E-04 0.10432  4.33259E-05 0.05529 -4.13604E-05 0.03550  6.67405E-03 0.00737 ];
INF_SP6                   (idx, [1:   8]) = [  5.24111E-03 0.00333 -1.30452E-04 0.01684 -5.16094E-05 0.02826 -1.71336E-02 0.00274 ];
INF_SP7                   (idx, [1:   8]) = [  9.10183E-04 0.01940 -1.64535E-04 0.01164 -4.83498E-05 0.02975  5.41949E-04 0.10577 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31784E-01 0.00052  1.20312E+00 0.00804 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33606E-01 0.00081  1.29646E+00 0.00889 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33178E-01 0.00099  1.31218E+00 0.00899 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28644E-01 0.00110  1.04263E+00 0.00921 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43813E+00 0.00052  2.77490E-01 0.00808 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42693E+00 0.00081  2.57603E-01 0.00897 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42956E+00 0.00099  2.54524E-01 0.00898 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45791E+00 0.00110  3.20344E-01 0.00907 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  5.10277E-03 0.00938  1.12424E-04 0.06226  9.33268E-04 0.02286  7.69080E-04 0.02636  2.21842E-03 0.01375  8.30991E-04 0.02367  2.38581E-04 0.04443 ];
LAMBDA                    (idx, [1:  14]) = [  7.61684E-01 0.02300  1.25612E-02 0.00106  3.07817E-02 0.00063  1.11003E-01 0.00077  3.22627E-01 0.00049  1.27684E+00 0.00299  8.24110E+00 0.01208 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0010' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:42:04 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:30:11 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292124 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99648E-01  1.00038E+00  1.00280E+00  9.97439E-01  9.99729E-01  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 1.5E-09  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.16932E-02 0.00114  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88307E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.06303E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06739E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68348E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.42170E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.42083E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.19419E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.13877E-01 0.00115  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001255 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00025E+04 0.00095 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00025E+04 0.00095 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.38151E+02 ;
RUNNING_TIME              (idx, 1)        =  1.08125E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.78167E-01  4.78167E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.70467E-01  1.22000E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.06287E+02  3.24420E+00  2.54882E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  9.81467E-01  2.53333E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.55167E-02  8.33333E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.08125E+02  1.19855E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97712 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  4.99998E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80284E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6133.74;
MEMSIZE                   (idx, 1)        = 6039.76;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 169.50;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.98;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.59580E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.67745E+04 ;
TOT_SF_RATE               (idx, 1)        =  2.13299E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.02511E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.42529E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.57066E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.53488E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  4.76926E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.00025E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  3.87911E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.21603E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  8.90155E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.07865E+07 ;
SR90_ACTIVITY             (idx, 1)        =  2.83191E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.34360E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.51467E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.53059E+14 ;
CS134_ACTIVITY            (idx, 1)        =  6.01395E+13 ;
CS137_ACTIVITY            (idx, 1)        =  4.00198E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.34841E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.22261E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  1.54856E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.31748E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.28685E+12 0.00054  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 18 ;
BURNUP                     (idx, [1:  2])  = [  3.00000E+01  3.00028E+01 ];
BURN_DAYS                 (idx, 1)        =  7.50000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.28445E-01 0.00111 ];
U235_FISS                 (idx, [1:   4]) = [  4.66483E+15 0.00129  3.42437E-01 0.00112 ];
U238_FISS                 (idx, [1:   4]) = [  1.17800E+15 0.00270  8.64606E-02 0.00249 ];
PU239_FISS                (idx, [1:   4]) = [  6.17723E+15 0.00105  4.53465E-01 0.00086 ];
PU240_FISS                (idx, [1:   4]) = [  1.45508E+13 0.02525  1.06819E-03 0.02528 ];
PU241_FISS                (idx, [1:   4]) = [  1.55545E+15 0.00232  1.14178E-01 0.00220 ];
U233_CAPT                 (idx, [1:   4]) = [  8.91888E+09 1.00000  3.06937E-07 1.00000 ];
U235_CAPT                 (idx, [1:   4]) = [  1.05896E+15 0.00301  3.65897E-02 0.00296 ];
U238_CAPT                 (idx, [1:   4]) = [  9.98326E+15 0.00106  3.44925E-01 0.00077 ];
PU239_CAPT                (idx, [1:   4]) = [  3.40180E+15 0.00151  1.17548E-01 0.00149 ];
PU240_CAPT                (idx, [1:   4]) = [  2.61413E+15 0.00192  9.03199E-02 0.00179 ];
PU241_CAPT                (idx, [1:   4]) = [  5.57387E+14 0.00401  1.92623E-02 0.00406 ];
XE135_CAPT                (idx, [1:   4]) = [  7.72225E+14 0.00333  2.66807E-02 0.00324 ];
SM149_CAPT                (idx, [1:   4]) = [  2.48078E+14 0.00624  8.57033E-03 0.00618 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001255 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.15149E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001255 5.00815E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3370895 3.37569E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1586880 1.58898E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 43480 4.34777E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001255 5.00815E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.33299E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50055E+05 2.0E-09  4.50055E+05 2.0E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.71161E+16 2.0E-05  3.71161E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.36194E+16 4.4E-06  1.36194E+16 4.4E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  2.89494E+16 0.00047  2.26316E+16 0.00047  6.31777E+15 0.00131 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.25687E+16 0.00032  3.62509E+16 0.00029  6.31777E+15 0.00131 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.28685E+16 0.00054  4.28685E+16 0.00054  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  1.98331E+18 0.00055  5.27569E+17 0.00051  1.45574E+18 0.00062 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.72838E+14 0.00517 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.29415E+16 0.00033 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.46864E+18 0.00072 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.09035E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.09035E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.45572E+00 0.00057 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.85020E-01 0.00036 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.15460E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24230E+00 0.00040 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94356E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96931E-01 2.5E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.73676E-01 0.00065 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.66079E-01 0.00065 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.72525E+00 2.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06252E+02 4.4E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.65924E-01 0.00066  8.61781E-01 0.00066  4.29790E-03 0.01067 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.65808E-01 0.00033 ];
COL_KEFF                  (idx, [1:   2]) = [  8.65938E-01 0.00054 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.65808E-01 0.00033 ];
ABS_KINF                  (idx, [1:   2]) = [  8.73402E-01 0.00032 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.69455E+01 0.00025 ];
IMP_ALF                   (idx, [1:   2]) = [  1.69499E+01 0.00010 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  8.78168E-07 0.00419 ];
IMP_EALF                  (idx, [1:   2]) = [  8.71220E-07 0.00175 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  2.96511E-01 0.00287 ];
IMP_AFGE                  (idx, [1:   2]) = [  2.95083E-01 0.00117 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.92891E-03 0.00631  1.44040E-04 0.04134  1.10047E-03 0.01451  9.29585E-04 0.01603  2.52040E-03 0.00968  9.50206E-04 0.01520  2.84204E-04 0.02970 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.57851E-01 0.01570  8.79128E-03 0.02947  3.06772E-02 0.00043  1.10928E-01 0.00209  3.22897E-01 0.00035  1.25987E+00 0.00233  7.53120E+00 0.01700 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.98464E-03 0.00958  1.19716E-04 0.06317  8.96670E-04 0.02207  8.09631E-04 0.02380  2.13957E-03 0.01497  7.76809E-04 0.02490  2.42244E-04 0.04367 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.59386E-01 0.02326  1.25899E-02 0.00124  3.06626E-02 0.00063  1.11107E-01 0.00083  3.23170E-01 0.00056  1.25576E+00 0.00359  8.27661E+00 0.01179 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.79001E-05 0.00153  2.78880E-05 0.00153  3.06797E-05 0.01991 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.41539E-05 0.00136  2.41434E-05 0.00137  2.65679E-05 0.01990 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.96318E-03 0.01083  1.06821E-04 0.07877  8.99701E-04 0.02577  7.97422E-04 0.02781  2.14297E-03 0.01719  7.88484E-04 0.02724  2.27776E-04 0.05303 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.48311E-01 0.02847  1.25937E-02 0.00204  3.06618E-02 0.00087  1.11032E-01 0.00104  3.23091E-01 0.00071  1.25729E+00 0.00481  8.31660E+00 0.01666 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.70686E-05 0.00363  2.70591E-05 0.00364  2.65158E-05 0.04543 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.34360E-05 0.00362  2.34278E-05 0.00363  2.29489E-05 0.04538 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.79561E-03 0.03626  9.85121E-05 0.25381  8.13400E-04 0.09532  7.17790E-04 0.09578  2.03694E-03 0.05870  8.66500E-04 0.08349  2.62463E-04 0.17059 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  7.64179E-01 0.07961  1.25296E-02 0.00328  3.06896E-02 0.00215  1.11081E-01 0.00257  3.22689E-01 0.00192  1.26304E+00 0.00971  8.33840E+00 0.04110 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.82857E-03 0.03559  1.03270E-04 0.25397  8.13571E-04 0.09167  7.42661E-04 0.09333  2.04444E-03 0.05685  8.63442E-04 0.08282  2.61188E-04 0.17086 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  7.58016E-01 0.07898  1.25297E-02 0.00328  3.06921E-02 0.00215  1.11090E-01 0.00257  3.22726E-01 0.00190  1.26263E+00 0.00974  8.33560E+00 0.04111 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.77451E+02 0.03616 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.75402E-05 0.00094 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.38423E-05 0.00065 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.88509E-03 0.00726 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.77350E+02 0.00714 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.47945E-07 0.00082 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.63392E-06 0.00047  2.63381E-06 0.00046  2.65345E-06 0.00629 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.80065E-05 0.00098  3.80249E-05 0.00098  3.44910E-05 0.01196 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.13647E-01 0.00036  6.14067E-01 0.00036  5.54593E-01 0.01023 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07335E+01 0.01432 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.42083E+01 0.00049  3.26901E+01 0.00051 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.06601E+04 0.00263  3.04404E+05 0.00131  6.09836E+05 0.00098  6.50861E+05 0.00090  5.98866E+05 0.00064  6.41861E+05 0.00069  4.35577E+05 0.00064  3.86302E+05 0.00060  2.95205E+05 0.00083  2.41266E+05 0.00080  2.08263E+05 0.00072  1.87484E+05 0.00082  1.73365E+05 0.00087  1.64792E+05 0.00092  1.60483E+05 0.00097  1.38585E+05 0.00092  1.36898E+05 0.00105  1.35861E+05 0.00107  1.33496E+05 0.00066  2.60107E+05 0.00081  2.50882E+05 0.00057  1.81358E+05 0.00089  1.17235E+05 0.00084  1.34798E+05 0.00086  1.28768E+05 0.00104  1.13851E+05 0.00074  1.83420E+05 0.00094  4.24102E+04 0.00159  5.25159E+04 0.00091  4.78810E+04 0.00166  2.80954E+04 0.00243  4.84673E+04 0.00204  3.18834E+04 0.00198  2.55743E+04 0.00150  4.31012E+03 0.00297  3.61149E+03 0.00397  3.17301E+03 0.00332  3.04434E+03 0.00364  3.10847E+03 0.00307  3.40997E+03 0.00316  4.10558E+03 0.00299  4.22590E+03 0.00362  8.46783E+03 0.00247  1.41035E+04 0.00197  1.83942E+04 0.00203  4.95214E+04 0.00145  5.20123E+04 0.00140  5.54234E+04 0.00117  3.56997E+04 0.00138  2.50430E+04 0.00176  1.84284E+04 0.00262  2.17313E+04 0.00190  4.24267E+04 0.00194  6.00885E+04 0.00183  1.20459E+05 0.00164  1.91244E+05 0.00178  2.90651E+05 0.00181  1.87613E+05 0.00181  1.35076E+05 0.00198  9.74105E+04 0.00223  8.78575E+04 0.00177  8.68064E+04 0.00190  7.26954E+04 0.00243  4.92391E+04 0.00253  4.55734E+04 0.00216  4.06805E+04 0.00227  3.43434E+04 0.00198  2.71360E+04 0.00206  1.81850E+04 0.00252  6.45465E+03 0.00231 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.73534E-01 0.00055 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.59702E+18 0.00063  3.86305E+17 0.00177 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39527E-01 0.00011  1.57129E+00 0.00047 ];
INF_CAPT                  (idx, [1:   4]) = [  8.61786E-03 0.00067  3.93172E-02 0.00106 ];
INF_ABS                   (idx, [1:   4]) = [  1.02767E-02 0.00059  6.77219E-02 0.00134 ];
INF_FISS                  (idx, [1:   4]) = [  1.65881E-03 0.00048  2.84046E-02 0.00174 ];
INF_NSF                   (idx, [1:   4]) = [  4.52874E-03 0.00049  7.73764E-02 0.00178 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.73011E+00 5.9E-05  2.72407E+00 5.6E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06109E+02 8.7E-06  2.06287E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.47584E-08 0.00047  2.58696E-06 0.00018 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29251E-01 0.00012  1.50358E+00 0.00054 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43754E-01 0.00023  3.98926E-01 0.00067 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62016E-02 0.00035  9.41919E-02 0.00083 ];
INF_SCATT3                (idx, [1:   4]) = [  7.26080E-03 0.00300  2.82076E-02 0.00179 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03098E-02 0.00233 -8.89835E-03 0.00577 ];
INF_SCATT5                (idx, [1:   4]) = [  1.94090E-04 0.09489  6.79373E-03 0.00851 ];
INF_SCATT6                (idx, [1:   4]) = [  5.14500E-03 0.00382 -1.71735E-02 0.00300 ];
INF_SCATT7                (idx, [1:   4]) = [  7.80874E-04 0.01998  5.08351E-04 0.08567 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29295E-01 0.00012  1.50358E+00 0.00054 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43755E-01 0.00023  3.98926E-01 0.00067 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62017E-02 0.00035  9.41919E-02 0.00083 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.26106E-03 0.00300  2.82076E-02 0.00179 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03097E-02 0.00233 -8.89835E-03 0.00577 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.94308E-04 0.09474  6.79373E-03 0.00851 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.14487E-03 0.00383 -1.71735E-02 0.00300 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.80790E-04 0.01994  5.08351E-04 0.08567 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12790E-01 0.00020  1.02095E+00 0.00038 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56649E+00 0.00020  3.26495E-01 0.00038 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.02329E-02 0.00060  6.77219E-02 0.00134 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70204E-02 0.00028  6.88872E-02 0.00141 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12507E-01 0.00011  1.67442E-02 0.00060  1.17946E-03 0.00365  1.50240E+00 0.00055 ];
INF_S1                    (idx, [1:   8]) = [  2.38921E-01 0.00023  4.83318E-03 0.00088  5.09085E-04 0.00859  3.98417E-01 0.00067 ];
INF_S2                    (idx, [1:   8]) = [  9.77395E-02 0.00035 -1.53793E-03 0.00252  2.77714E-04 0.01271  9.39142E-02 0.00083 ];
INF_S3                    (idx, [1:   8]) = [  8.99635E-03 0.00244 -1.73555E-03 0.00108  9.94462E-05 0.02576  2.81082E-02 0.00180 ];
INF_S4                    (idx, [1:   8]) = [ -9.78049E-03 0.00245 -5.29363E-04 0.00434  1.92122E-06 1.00000 -8.90027E-03 0.00568 ];
INF_S5                    (idx, [1:   8]) = [  1.49928E-04 0.12430  4.41626E-05 0.04888 -3.92116E-05 0.04108  6.83294E-03 0.00843 ];
INF_S6                    (idx, [1:   8]) = [  5.27463E-03 0.00384 -1.29626E-04 0.01303 -4.95499E-05 0.03919 -1.71240E-02 0.00300 ];
INF_S7                    (idx, [1:   8]) = [  9.45263E-04 0.01611 -1.64389E-04 0.01090 -4.35453E-05 0.04305  5.51896E-04 0.07987 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12551E-01 0.00011  1.67442E-02 0.00060  1.17946E-03 0.00365  1.50240E+00 0.00055 ];
INF_SP1                   (idx, [1:   8]) = [  2.38921E-01 0.00023  4.83318E-03 0.00088  5.09085E-04 0.00859  3.98417E-01 0.00067 ];
INF_SP2                   (idx, [1:   8]) = [  9.77396E-02 0.00035 -1.53793E-03 0.00252  2.77714E-04 0.01271  9.39142E-02 0.00083 ];
INF_SP3                   (idx, [1:   8]) = [  8.99661E-03 0.00244 -1.73555E-03 0.00108  9.94462E-05 0.02576  2.81082E-02 0.00180 ];
INF_SP4                   (idx, [1:   8]) = [ -9.78033E-03 0.00245 -5.29363E-04 0.00434  1.92122E-06 1.00000 -8.90027E-03 0.00568 ];
INF_SP5                   (idx, [1:   8]) = [  1.50145E-04 0.12408  4.41626E-05 0.04888 -3.92116E-05 0.04108  6.83294E-03 0.00843 ];
INF_SP6                   (idx, [1:   8]) = [  5.27449E-03 0.00384 -1.29626E-04 0.01303 -4.95499E-05 0.03919 -1.71240E-02 0.00300 ];
INF_SP7                   (idx, [1:   8]) = [  9.45179E-04 0.01608 -1.64389E-04 0.01090 -4.35453E-05 0.04305  5.51896E-04 0.07987 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.32006E-01 0.00049  1.22106E+00 0.00907 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33835E-01 0.00088  1.33335E+00 0.01154 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33454E-01 0.00070  1.33174E+00 0.01275 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28803E-01 0.00066  1.04788E+00 0.00727 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43675E+00 0.00049  2.73534E-01 0.00920 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42553E+00 0.00087  2.50809E-01 0.01171 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42785E+00 0.00070  2.51286E-01 0.01284 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45687E+00 0.00066  3.18509E-01 0.00735 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.98464E-03 0.00958  1.19716E-04 0.06317  8.96670E-04 0.02207  8.09631E-04 0.02380  2.13957E-03 0.01497  7.76809E-04 0.02490  2.42244E-04 0.04367 ];
LAMBDA                    (idx, [1:  14]) = [  7.59386E-01 0.02326  1.25899E-02 0.00124  3.06626E-02 0.00063  1.11107E-01 0.00083  3.23170E-01 0.00056  1.25576E+00 0.00359  8.27661E+00 0.01179 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0010' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:42:04 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:36:06 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292124 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.98796E-01  1.00079E+00  1.00314E+00  9.97061E-01  1.00021E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.17023E-02 0.00108  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88298E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05854E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06290E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68628E+00 0.00024  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.42476E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.42389E+01 0.00049  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.22518E+00 0.00038  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.15301E-01 0.00110  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001181 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00024E+04 0.00102 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00024E+04 0.00102 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.67657E+02 ;
RUNNING_TIME              (idx, 1)        =  1.14033E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.78167E-01  4.78167E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  3.94067E-01  1.14500E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.12115E+02  3.27450E+00  2.55418E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.03652E+00  2.65167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  4.80833E-02  8.66667E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.14032E+02  1.19907E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97802 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00012E+00 0.00019 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80522E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6133.74;
MEMSIZE                   (idx, 1)        = 6039.76;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 169.50;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.98;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.72921E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.67925E+04 ;
TOT_SF_RATE               (idx, 1)        =  3.58749E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.13012E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.50473E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.59906E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.52874E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  6.16464E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.16244E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  5.23144E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  9.91749E+06 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  9.33194E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.17069E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.17521E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.38315E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.55852E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.58138E+14 ;
CS134_ACTIVITY            (idx, 1)        =  7.82145E+13 ;
CS137_ACTIVITY            (idx, 1)        =  4.65535E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.47205E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.19668E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.22659E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.34828E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.47677E+12 0.00053  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 19 ;
BURNUP                     (idx, [1:  2])  = [  3.50000E+01  3.50034E+01 ];
BURN_DAYS                 (idx, 1)        =  8.75000E+02 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  7.67655E-01 0.00113 ];
U233_FISS                 (idx, [1:   4]) = [  9.21111E+09 1.00000  6.56383E-07 1.00000 ];
U235_FISS                 (idx, [1:   4]) = [  3.89125E+15 0.00146  2.86186E-01 0.00130 ];
U238_FISS                 (idx, [1:   4]) = [  1.22582E+15 0.00266  9.01413E-02 0.00246 ];
PU239_FISS                (idx, [1:   4]) = [  6.57352E+15 0.00104  4.83468E-01 0.00084 ];
PU240_FISS                (idx, [1:   4]) = [  1.66652E+13 0.02304  1.22616E-03 0.02306 ];
PU241_FISS                (idx, [1:   4]) = [  1.84606E+15 0.00222  1.35760E-01 0.00204 ];
U235_CAPT                 (idx, [1:   4]) = [  8.72970E+14 0.00312  2.82970E-02 0.00307 ];
U238_CAPT                 (idx, [1:   4]) = [  1.03673E+16 0.00110  3.36023E-01 0.00077 ];
PU239_CAPT                (idx, [1:   4]) = [  3.62167E+15 0.00156  1.17404E-01 0.00157 ];
PU240_CAPT                (idx, [1:   4]) = [  2.93242E+15 0.00185  9.50483E-02 0.00172 ];
PU241_CAPT                (idx, [1:   4]) = [  6.61598E+14 0.00343  2.14484E-02 0.00347 ];
XE135_CAPT                (idx, [1:   4]) = [  7.84662E+14 0.00345  2.54338E-02 0.00339 ];
SM149_CAPT                (idx, [1:   4]) = [  2.52936E+14 0.00603  8.19955E-03 0.00604 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001181 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.31260E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001181 5.00831E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3440693 3.44569E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1516601 1.51873E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 43887 4.38928E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001181 5.00831E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -6.51926E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50055E+05 2.0E-09  4.50055E+05 2.0E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.74027E+16 2.0E-05  3.74027E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35940E+16 4.3E-06  1.35940E+16 4.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.08666E+16 0.00048  2.42545E+16 0.00046  6.61209E+15 0.00127 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.44606E+16 0.00033  3.78485E+16 0.00030  6.61209E+15 0.00127 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.47677E+16 0.00053  4.47677E+16 0.00053  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.07151E+18 0.00053  5.50998E+17 0.00049  1.52051E+18 0.00059 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  3.93061E+14 0.00483 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.48536E+16 0.00034 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.53515E+18 0.00070 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.08458E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.08458E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.42059E+00 0.00063 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.82474E-01 0.00036 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.11045E-01 0.00036 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24133E+00 0.00039 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94286E-01 3.4E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96918E-01 2.4E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.43064E-01 0.00069 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.35663E-01 0.00070 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.75142E+00 2.3E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06637E+02 4.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.35549E-01 0.00071  8.31726E-01 0.00070  3.93753E-03 0.01155 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.35321E-01 0.00034 ];
COL_KEFF                  (idx, [1:   2]) = [  8.35604E-01 0.00053 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.35321E-01 0.00034 ];
ABS_KINF                  (idx, [1:   2]) = [  8.42715E-01 0.00034 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.68883E+01 0.00025 ];
IMP_ALF                   (idx, [1:   2]) = [  1.68936E+01 0.00011 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  9.30090E-07 0.00426 ];
IMP_EALF                  (idx, [1:   2]) = [  9.21732E-07 0.00183 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.08555E-01 0.00274 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.07859E-01 0.00122 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  5.94838E-03 0.00705  1.45076E-04 0.04292  1.08767E-03 0.01552  9.02321E-04 0.01607  2.52473E-03 0.00990  1.01452E-03 0.01682  2.74060E-04 0.03046 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.32691E-01 0.01539  8.51147E-03 0.03116  3.05372E-02 0.00044  1.11349E-01 0.00067  3.23106E-01 0.00039  1.24341E+00 0.00269  6.99790E+00 0.02025 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.78511E-03 0.00998  1.10850E-04 0.06828  9.00671E-04 0.02356  7.13540E-04 0.02566  2.02266E-03 0.01497  8.09881E-04 0.02477  2.27503E-04 0.04695 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.44363E-01 0.02441  1.26383E-02 0.00150  3.05273E-02 0.00061  1.11357E-01 0.00091  3.23201E-01 0.00060  1.24778E+00 0.00362  7.94607E+00 0.01431 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  2.92006E-05 0.00155  2.91869E-05 0.00155  3.19929E-05 0.01900 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.43924E-05 0.00138  2.43809E-05 0.00139  2.67310E-05 0.01901 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.71238E-03 0.01180  1.05319E-04 0.08703  9.00504E-04 0.02751  7.13482E-04 0.03076  1.98841E-03 0.01791  7.89528E-04 0.03066  2.15141E-04 0.05723 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.14215E-01 0.03036  1.26311E-02 0.00247  3.05108E-02 0.00083  1.11269E-01 0.00125  3.22874E-01 0.00079  1.24658E+00 0.00510  7.70353E+00 0.02365 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.83076E-05 0.00378  2.82844E-05 0.00380  2.94597E-05 0.04971 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.36469E-05 0.00373  2.36275E-05 0.00374  2.45856E-05 0.04973 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.72756E-03 0.03996  1.47869E-04 0.27477  7.80862E-04 0.09408  8.38491E-04 0.09820  2.03883E-03 0.06527  7.77583E-04 0.09697  1.43923E-04 0.22090 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  6.25309E-01 0.08823  1.27544E-02 0.00759  3.04577E-02 0.00192  1.11848E-01 0.00278  3.22637E-01 0.00201  1.25099E+00 0.01139  7.51423E+00 0.06835 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.73084E-03 0.03993  1.50109E-04 0.27233  7.87908E-04 0.09425  8.59056E-04 0.09602  2.02452E-03 0.06415  7.69068E-04 0.09689  1.40184E-04 0.20799 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  6.31132E-01 0.08699  1.27544E-02 0.00759  3.04552E-02 0.00190  1.11870E-01 0.00278  3.22645E-01 0.00201  1.24997E+00 0.01141  7.51423E+00 0.06835 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.68361E+02 0.04023 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.87519E-05 0.00100 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.40178E-05 0.00074 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.83905E-03 0.00790 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.68426E+02 0.00801 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.49253E-07 0.00079 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.61492E-06 0.00043  2.61485E-06 0.00043  2.63140E-06 0.00606 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.84198E-05 0.00096  3.84368E-05 0.00096  3.50747E-05 0.01143 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.09242E-01 0.00036  6.09774E-01 0.00036  5.33567E-01 0.01115 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07239E+01 0.01471 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.42389E+01 0.00049  3.27042E+01 0.00058 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.15700E+04 0.00273  3.05567E+05 0.00153  6.09831E+05 0.00102  6.51008E+05 0.00067  5.99101E+05 0.00064  6.41798E+05 0.00091  4.36149E+05 0.00068  3.85540E+05 0.00060  2.95311E+05 0.00085  2.40960E+05 0.00076  2.08090E+05 0.00071  1.87567E+05 0.00066  1.73247E+05 0.00100  1.64703E+05 0.00072  1.60325E+05 0.00086  1.38706E+05 0.00083  1.37067E+05 0.00075  1.35922E+05 0.00110  1.33241E+05 0.00104  2.60395E+05 0.00059  2.51666E+05 0.00069  1.81109E+05 0.00077  1.17488E+05 0.00119  1.34880E+05 0.00066  1.28973E+05 0.00078  1.13833E+05 0.00083  1.82650E+05 0.00070  4.26315E+04 0.00188  5.20956E+04 0.00150  4.76198E+04 0.00163  2.80434E+04 0.00173  4.81786E+04 0.00171  3.13380E+04 0.00142  2.51577E+04 0.00186  4.15903E+03 0.00425  3.51704E+03 0.00356  3.10749E+03 0.00430  2.98205E+03 0.00393  3.01616E+03 0.00349  3.30585E+03 0.00505  3.99448E+03 0.00407  4.15396E+03 0.00326  8.34540E+03 0.00234  1.39203E+04 0.00263  1.82274E+04 0.00259  4.89962E+04 0.00131  5.15039E+04 0.00128  5.49448E+04 0.00163  3.55259E+04 0.00147  2.47852E+04 0.00183  1.83299E+04 0.00202  2.15877E+04 0.00201  4.22078E+04 0.00180  5.99008E+04 0.00167  1.20037E+05 0.00168  1.91140E+05 0.00169  2.91357E+05 0.00182  1.88496E+05 0.00178  1.35914E+05 0.00176  9.79533E+04 0.00198  8.82318E+04 0.00213  8.72392E+04 0.00205  7.31213E+04 0.00230  4.95085E+04 0.00244  4.58106E+04 0.00202  4.08107E+04 0.00258  3.46242E+04 0.00238  2.73090E+04 0.00215  1.83141E+04 0.00248  6.50687E+03 0.00317 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.43005E-01 0.00064 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.66748E+18 0.00061  4.04065E+17 0.00180 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.39848E-01 9.3E-05  1.57145E+00 0.00050 ];
INF_CAPT                  (idx, [1:   4]) = [  8.81735E-03 0.00044  4.00080E-02 0.00107 ];
INF_ABS                   (idx, [1:   4]) = [  1.03986E-02 0.00041  6.71320E-02 0.00136 ];
INF_FISS                  (idx, [1:   4]) = [  1.58129E-03 0.00060  2.71240E-02 0.00181 ];
INF_NSF                   (idx, [1:   4]) = [  4.35158E-03 0.00063  7.46265E-02 0.00184 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.75192E+00 6.9E-05  2.75130E+00 6.2E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06413E+02 9.2E-06  2.06691E+02 1.2E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.44292E-08 0.00033  2.59141E-06 0.00022 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29453E-01 9.9E-05  1.50434E+00 0.00059 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43804E-01 0.00021  3.98740E-01 0.00070 ];
INF_SCATT2                (idx, [1:   4]) = [  9.62450E-02 0.00029  9.40889E-02 0.00087 ];
INF_SCATT3                (idx, [1:   4]) = [  7.25072E-03 0.00304  2.82678E-02 0.00231 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03291E-02 0.00206 -8.96142E-03 0.00643 ];
INF_SCATT5                (idx, [1:   4]) = [  2.10758E-04 0.09509  6.75172E-03 0.00786 ];
INF_SCATT6                (idx, [1:   4]) = [  5.16397E-03 0.00334 -1.72482E-02 0.00374 ];
INF_SCATT7                (idx, [1:   4]) = [  7.85004E-04 0.02071  4.91803E-04 0.09121 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29497E-01 9.9E-05  1.50434E+00 0.00059 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43804E-01 0.00021  3.98740E-01 0.00070 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.62456E-02 0.00029  9.40889E-02 0.00087 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.25064E-03 0.00304  2.82678E-02 0.00231 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03289E-02 0.00206 -8.96142E-03 0.00643 ];
INF_SCATTP5               (idx, [1:   4]) = [  2.10837E-04 0.09536  6.75172E-03 0.00786 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.16384E-03 0.00334 -1.72482E-02 0.00374 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.84989E-04 0.02067  4.91803E-04 0.09121 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12753E-01 0.00025  1.02223E+00 0.00040 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56677E+00 0.00025  3.26084E-01 0.00040 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.03540E-02 0.00041  6.71320E-02 0.00136 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70209E-02 0.00025  6.82737E-02 0.00147 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.12827E-01 9.2E-05  1.66254E-02 0.00045  1.16451E-03 0.00374  1.50317E+00 0.00059 ];
INF_S1                    (idx, [1:   8]) = [  2.39006E-01 0.00020  4.79779E-03 0.00111  5.02147E-04 0.00672  3.98238E-01 0.00071 ];
INF_S2                    (idx, [1:   8]) = [  9.77690E-02 0.00028 -1.52396E-03 0.00286  2.74343E-04 0.00870  9.38145E-02 0.00089 ];
INF_S3                    (idx, [1:   8]) = [  8.97657E-03 0.00243 -1.72584E-03 0.00161  1.00184E-04 0.01988  2.81676E-02 0.00231 ];
INF_S4                    (idx, [1:   8]) = [ -9.79948E-03 0.00215 -5.29651E-04 0.00420  1.56915E-07 1.00000 -8.96158E-03 0.00635 ];
INF_S5                    (idx, [1:   8]) = [  1.65630E-04 0.11622  4.51278E-05 0.05444 -4.01662E-05 0.03963  6.79189E-03 0.00780 ];
INF_S6                    (idx, [1:   8]) = [  5.29178E-03 0.00330 -1.27811E-04 0.02031 -5.21691E-05 0.03034 -1.71961E-02 0.00375 ];
INF_S7                    (idx, [1:   8]) = [  9.48362E-04 0.01669 -1.63358E-04 0.01432 -4.98449E-05 0.02562  5.41648E-04 0.08218 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.12872E-01 9.2E-05  1.66254E-02 0.00045  1.16451E-03 0.00374  1.50317E+00 0.00059 ];
INF_SP1                   (idx, [1:   8]) = [  2.39006E-01 0.00020  4.79779E-03 0.00111  5.02147E-04 0.00672  3.98238E-01 0.00071 ];
INF_SP2                   (idx, [1:   8]) = [  9.77695E-02 0.00028 -1.52396E-03 0.00286  2.74343E-04 0.00870  9.38145E-02 0.00089 ];
INF_SP3                   (idx, [1:   8]) = [  8.97649E-03 0.00243 -1.72584E-03 0.00161  1.00184E-04 0.01988  2.81676E-02 0.00231 ];
INF_SP4                   (idx, [1:   8]) = [ -9.79920E-03 0.00215 -5.29651E-04 0.00420  1.56915E-07 1.00000 -8.96158E-03 0.00635 ];
INF_SP5                   (idx, [1:   8]) = [  1.65709E-04 0.11654  4.51278E-05 0.05444 -4.01662E-05 0.03963  6.79189E-03 0.00780 ];
INF_SP6                   (idx, [1:   8]) = [  5.29165E-03 0.00330 -1.27811E-04 0.02031 -5.21691E-05 0.03034 -1.71961E-02 0.00375 ];
INF_SP7                   (idx, [1:   8]) = [  9.48347E-04 0.01665 -1.63358E-04 0.01432 -4.98449E-05 0.02562  5.41648E-04 0.08218 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31761E-01 0.00064  1.22304E+00 0.00781 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33272E-01 0.00092  1.33786E+00 0.01125 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33769E-01 0.00093  1.33966E+00 0.01272 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28327E-01 0.00085  1.04547E+00 0.00674 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43828E+00 0.00064  2.72943E-01 0.00776 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42898E+00 0.00092  2.49890E-01 0.01094 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42594E+00 0.00093  2.49752E-01 0.01230 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45992E+00 0.00085  3.19186E-01 0.00679 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.78511E-03 0.00998  1.10850E-04 0.06828  9.00671E-04 0.02356  7.13540E-04 0.02566  2.02266E-03 0.01497  8.09881E-04 0.02477  2.27503E-04 0.04695 ];
LAMBDA                    (idx, [1:  14]) = [  7.44363E-01 0.02441  1.26383E-02 0.00150  3.05273E-02 0.00061  1.11357E-01 0.00091  3.23201E-01 0.00060  1.24778E+00 0.00362  7.94607E+00 0.01431 ];


% Increase counter:

if (exist('idx', 'var'));
  idx = idx + 1;
else;
  idx = 1;
end;

% Version, title and date:

VERSION                   (idx, [1: 14])  = 'Serpent 2.1.30' ;
COMPILE_DATE              (idx, [1: 20])  = 'Nov  2 2018 00:01:43' ;
DEBUG                     (idx, 1)        = 0 ;
TITLE                     (idx, [1: 25])  = 'UOX Micro Core Assemblies' ;
CONFIDENTIAL_DATA         (idx, 1)        = 0 ;
INPUT_FILE_NAME           (idx, [1:  8])  = 'hpc_0010' ;
WORKING_DIRECTORY         (idx, [1: 37])  = '/home/ajw287/serpent/fmtx_burnup_ndf2' ;
HOSTNAME                  (idx, [1:  4])  = 'lise' ;
CPU_TYPE                  (idx, [1: 40])  = 'Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz' ;
CPU_MHZ                   (idx, 1)        = 33554532.0 ;
START_DATE                (idx, [1: 24])  = 'Tue Jul 21 01:42:04 2020' ;
COMPLETE_DATE             (idx, [1: 24])  = 'Tue Jul 21 03:42:03 2020' ;

% Run parameters:

POP                       (idx, 1)        = 10000 ;
CYCLES                    (idx, 1)        = 500 ;
SKIP                      (idx, 1)        = 100 ;
BATCH_INTERVAL            (idx, 1)        = 1 ;
SRC_NORM_MODE             (idx, 1)        = 2 ;
SEED                      (idx, 1)        = 1595292124 ;
UFS_MODE                  (idx, 1)        = 0 ;
UFS_ORDER                 (idx, 1)        = 1.00000;
NEUTRON_TRANSPORT_MODE    (idx, 1)        = 1 ;
PHOTON_TRANSPORT_MODE     (idx, 1)        = 0 ;
GROUP_CONSTANT_GENERATION (idx, 1)        = 1 ;
B1_CALCULATION            (idx, [1:  3])  = [ 0 0 0 ];
B1_BURNUP_CORRECTION      (idx, 1)        = 0 ;
IMPLICIT_REACTION_RATES   (idx, 1)        = 1 ;

% Optimization:

OPTIMIZATION_MODE         (idx, 1)        = 4 ;
RECONSTRUCT_MICROXS       (idx, 1)        = 1 ;
RECONSTRUCT_MACROXS       (idx, 1)        = 1 ;
DOUBLE_INDEXING           (idx, 1)        = 0 ;
MG_MAJORANT_MODE          (idx, 1)        = 0 ;
SPECTRUM_COLLAPSE         (idx, 1)        = 1 ;

% Parallelization:

MPI_TASKS                 (idx, 1)        = 1 ;
OMP_THREADS               (idx, 1)        = 5 ;
MPI_REPRODUCIBILITY       (idx, 1)        = 0 ;
OMP_REPRODUCIBILITY       (idx, 1)        = 1 ;
OMP_HISTORY_PROFILE       (idx, [1:   5]) = [  9.99204E-01  1.00000E+00  1.00394E+00  9.96611E-01  1.00024E+00  ];
SHARE_BUF_ARRAY           (idx, 1)        = 0 ;
SHARE_RES2_ARRAY          (idx, 1)        = 1 ;

% File paths:

XS_DATA_FILE_PATH         (idx, [1: 62])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7u.xsdata' ;
DECAY_DATA_FILE_PATH      (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.dec' ;
SFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
NFY_DATA_FILE_PATH        (idx, [1: 58])  = '/usr/software/mcnplib/SERPENT/XSdata_endfb7/sss_endfb7.nfy' ;
BRA_DATA_FILE_PATH        (idx, [1:  3])  = 'N/A' ;

% Collision and reaction sampling (neutrons/photons):

MIN_MACROXS               (idx, [1:   4]) = [  5.00000E-02 0.0E+00  0.00000E+00 0.0E+00 ];
DT_THRESH                 (idx, [1:  2])  = [  9.00000E-01  9.00000E-01 ];
ST_FRAC                   (idx, [1:   4]) = [  1.17347E-02 0.00106  0.00000E+00 0.0E+00 ];
DT_FRAC                   (idx, [1:   4]) = [  9.88265E-01 1.3E-05  0.00000E+00 0.0E+00 ];
DT_EFF                    (idx, [1:   4]) = [  8.05575E-01 0.00012  0.00000E+00 0.0E+00 ];
REA_SAMPLING_EFF          (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
REA_SAMPLING_FAIL         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_COL_EFF               (idx, [1:   4]) = [  8.06011E-01 0.00012  0.00000E+00 0.0E+00 ];
AVG_TRACKING_LOOPS        (idx, [1:   8]) = [  2.68878E+00 0.00023  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
AVG_TRACKS                (idx, [1:   4]) = [  3.42743E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_REAL_COL              (idx, [1:   4]) = [  3.42655E+01 0.00053  0.00000E+00 0.0E+00 ];
AVG_VIRT_COL              (idx, [1:   4]) = [  8.24615E+00 0.00039  0.00000E+00 0.0E+00 ];
AVG_SURF_CROSS            (idx, [1:   4]) = [  4.17187E-01 0.00108  0.00000E+00 0.0E+00 ];
LOST_PARTICLES            (idx, 1)        = 0 ;

% Run statistics:

CYCLE_IDX                 (idx, 1)        = 500 ;
SOURCE_POPULATION         (idx, 1)        = 5001604 ;
MEAN_POP_SIZE             (idx, [1:  2])  = [  1.00032E+04 0.00099 ];
MEAN_POP_WGT              (idx, [1:  2])  = [  1.00032E+04 0.00099 ];
SIMULATION_COMPLETED      (idx, 1)        = 1 ;

% Running times:

TOT_CPU_TIME              (idx, 1)        =  5.97343E+02 ;
RUNNING_TIME              (idx, 1)        =  1.19976E+02 ;
INIT_TIME                 (idx, [1:  2])  = [  4.78167E-01  4.78167E-01 ];
PROCESS_TIME              (idx, [1:  2])  = [  4.17617E-01  1.13500E-02 ];
TRANSPORT_CYCLE_TIME      (idx, [1:  3])  = [  1.17983E+02  3.29292E+00  2.57442E+00 ];
BURNUP_CYCLE_TIME         (idx, [1:  2])  = [  1.08883E+00  2.64167E-02 ];
BATEMAN_SOLUTION_TIME     (idx, [1:  2])  = [  5.06333E-02  9.16668E-04 ];
MPI_OVERHEAD_TIME         (idx, [1:  2])  = [  0.00000E+00  0.00000E+00 ];
ESTIMATED_RUNNING_TIME    (idx, [1:  2])  = [  1.19976E+02  1.19976E+02 ];
CPU_USAGE                 (idx, 1)        = 4.97884 ;
TRANSPORT_CPU_USAGE       (idx, [1:   2]) = [  5.00099E+00 0.00017 ];
OMP_PARALLEL_FRAC         (idx, 1)        =  9.80740E-01 ;

% Memory usage:

AVAIL_MEM                 (idx, 1)        = 128490.42 ;
ALLOC_MEMSIZE             (idx, 1)        = 6133.74;
MEMSIZE                   (idx, 1)        = 6039.76;
XS_MEMSIZE                (idx, 1)        = 5803.32;
MAT_MEMSIZE               (idx, 1)        = 169.50;
RES_MEMSIZE               (idx, 1)        = 1.36;
MISC_MEMSIZE              (idx, 1)        = 65.58;
UNKNOWN_MEMSIZE           (idx, 1)        = 0.00;
UNUSED_MEMSIZE            (idx, 1)        = 93.98;

% Geometry parameters:

TOT_CELLS                 (idx, 1)        = 22 ;
UNION_CELLS               (idx, 1)        = 0 ;

% Neutron energy grid:

NEUTRON_ERG_TOL           (idx, 1)        =  5.00000E-05 ;
NEUTRON_ERG_NE            (idx, 1)        = 266497 ;
NEUTRON_EMIN              (idx, 1)        =  1.00000E-11 ;
NEUTRON_EMAX              (idx, 1)        =  2.00000E+01 ;

% Unresolved resonance probability table sampling:

URES_DILU_CUT             (idx, 1)        =  1.00000E-09 ;
URES_EMIN                 (idx, 1)        =  1.00000E+37 ;
URES_EMAX                 (idx, 1)        = -1.00000E+37 ;
URES_AVAIL                (idx, 1)        = 222 ;
URES_USED                 (idx, 1)        = 0 ;

% Nuclides and reaction channels:

TOT_NUCLIDES              (idx, 1)        = 1341 ;
TOT_TRANSPORT_NUCLIDES    (idx, 1)        = 288 ;
TOT_DOSIMETRY_NUCLIDES    (idx, 1)        = 0 ;
TOT_DECAY_NUCLIDES        (idx, 1)        = 1053 ;
TOT_PHOTON_NUCLIDES       (idx, 1)        = 0 ;
TOT_REA_CHANNELS          (idx, 1)        = 8218 ;
TOT_TRANSMU_REA           (idx, 1)        = 2674 ;

% Neutron physics options:

USE_DELNU                 (idx, 1)        = 1 ;
USE_URES                  (idx, 1)        = 0 ;
USE_DBRC                  (idx, 1)        = 0 ;
IMPL_CAPT                 (idx, 1)        = 0 ;
IMPL_NXN                  (idx, 1)        = 1 ;
IMPL_FISS                 (idx, 1)        = 0 ;
DOPPLER_PREPROCESSOR      (idx, 1)        = 0 ;
TMS_MODE                  (idx, 1)        = 0 ;
SAMPLE_FISS               (idx, 1)        = 1 ;
SAMPLE_CAPT               (idx, 1)        = 1 ;
SAMPLE_SCATT              (idx, 1)        = 1 ;

% Radioactivity data:

TOT_ACTIVITY              (idx, 1)        =  9.85925E+16 ;
TOT_DECAY_HEAT            (idx, 1)        =  2.68356E+04 ;
TOT_SF_RATE               (idx, 1)        =  5.56256E+06 ;
ACTINIDE_ACTIVITY         (idx, 1)        =  2.22759E+16 ;
ACTINIDE_DECAY_HEAT       (idx, 1)        =  1.57960E+03 ;
FISSION_PRODUCT_ACTIVITY  (idx, 1)        =  7.63161E+16 ;
FISSION_PRODUCT_DECAY_HEAT(idx, 1)        =  2.52556E+04 ;
INHALATION_TOXICITY       (idx, 1)        =  7.77826E+08 ;
INGESTION_TOXICITY        (idx, 1)        =  5.32451E+07 ;
ACTINIDE_INH_TOX          (idx, 1)        =  6.80658E+08 ;
ACTINIDE_ING_TOX          (idx, 1)        =  1.06289E+07 ;
FISSION_PRODUCT_INH_TOX   (idx, 1)        =  9.71677E+07 ;
FISSION_PRODUCT_ING_TOX   (idx, 1)        =  4.26163E+07 ;
SR90_ACTIVITY             (idx, 1)        =  3.49225E+13 ;
TE132_ACTIVITY            (idx, 1)        =  6.42113E+14 ;
I131_ACTIVITY             (idx, 1)        =  4.59904E+14 ;
I132_ACTIVITY             (idx, 1)        =  6.62951E+14 ;
CS134_ACTIVITY            (idx, 1)        =  9.74093E+13 ;
CS137_ACTIVITY            (idx, 1)        =  5.30464E+13 ;
PHOTON_DECAY_SOURCE       (idx, 1)        =  8.59035E+16 ;
NEUTRON_DECAY_SOURCE      (idx, 1)        =  1.17475E+14 ;
ALPHA_DECAY_SOURCE        (idx, 1)        =  2.96725E+13 ;
ELECTRON_DECAY_SOURCE     (idx, 1)        =  1.37718E+17 ;

% Normalization coefficient:

NORM_COEF                 (idx, [1:   4]) = [  4.66598E+12 0.00054  0.00000E+00 0.0E+00 ];

% Parameters for burnup calculation:

BURN_MATERIALS            (idx, 1)        = 9 ;
BURN_MODE                 (idx, 1)        = 2 ;
BURN_STEP                 (idx, 1)        = 20 ;
BURNUP                     (idx, [1:  2])  = [  4.00000E+01  4.00038E+01 ];
BURN_DAYS                 (idx, 1)        =  1.00000E+03 ;

% Analog reaction rate estimators:

CONVERSION_RATIO          (idx, [1:   2]) = [  8.07459E-01 0.00113 ];
U235_FISS                 (idx, [1:   4]) = [  3.19819E+15 0.00177  2.35685E-01 0.00157 ];
U238_FISS                 (idx, [1:   4]) = [  1.26562E+15 0.00286  9.32591E-02 0.00268 ];
PU239_FISS                (idx, [1:   4]) = [  6.92707E+15 0.00108  5.10499E-01 0.00082 ];
PU240_FISS                (idx, [1:   4]) = [  1.92827E+13 0.02128  1.42118E-03 0.02124 ];
PU241_FISS                (idx, [1:   4]) = [  2.10182E+15 0.00206  1.54896E-01 0.00193 ];
U235_CAPT                 (idx, [1:   4]) = [  7.20966E+14 0.00366  2.20124E-02 0.00361 ];
U238_CAPT                 (idx, [1:   4]) = [  1.07847E+16 0.00113  3.29258E-01 0.00083 ];
PU239_CAPT                (idx, [1:   4]) = [  3.80013E+15 0.00151  1.16034E-01 0.00149 ];
PU240_CAPT                (idx, [1:   4]) = [  3.21643E+15 0.00181  9.82001E-02 0.00167 ];
PU241_CAPT                (idx, [1:   4]) = [  7.54698E+14 0.00339  2.30443E-02 0.00338 ];
XE135_CAPT                (idx, [1:   4]) = [  7.97623E+14 0.00320  2.43549E-02 0.00319 ];
SM149_CAPT                (idx, [1:   4]) = [  2.66817E+14 0.00617  8.14718E-03 0.00618 ];

% Neutron balance (particles/weight):

BALA_SRC_NEUTRON_SRC     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_FISS    (idx, [1:  2])  = [ 5001604 5.00000E+06 ];
BALA_SRC_NEUTRON_NXN     (idx, [1:  2])  = [ 0 8.22212E+03 ];
BALA_SRC_NEUTRON_VR      (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_SRC_NEUTRON_TOT     (idx, [1:  2])  = [ 5001604 5.00822E+06 ];

BALA_LOSS_NEUTRON_CAPT    (idx, [1:  2])  = [ 3504976 3.50975E+06 ];
BALA_LOSS_NEUTRON_FISS    (idx, [1:  2])  = [ 1452332 1.45416E+06 ];
BALA_LOSS_NEUTRON_LEAK    (idx, [1:  2])  = [ 44296 4.43060E+04 ];
BALA_LOSS_NEUTRON_CUT     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_ERR     (idx, [1:  2])  = [ 0 0.00000E+00 ];
BALA_LOSS_NEUTRON_TOT     (idx, [1:  2])  = [ 5001604 5.00822E+06 ];

BALA_NEUTRON_DIFF         (idx, [1:  2])  = [ 0 -7.72998E-08 ];

% Normalized total reaction rates (neutrons):

TOT_POWER                 (idx, [1:   6]) = [  4.50055E+05 2.0E-09  4.50055E+05 2.0E-09  0.00000E+00 0.0E+00 ];
TOT_POWDENS               (idx, [1:   6]) = [  4.00000E-02 8.1E-09  4.00000E-02 8.1E-09  0.00000E+00 0.0E+00 ];
TOT_GENRATE               (idx, [1:   6]) = [  3.76562E+16 2.0E-05  3.76562E+16 2.0E-05  0.00000E+00 0.0E+00 ];
TOT_FISSRATE              (idx, [1:   6]) = [  1.35716E+16 4.3E-06  1.35716E+16 4.3E-06  0.00000E+00 0.0E+00 ];
TOT_CAPTRATE              (idx, [1:   6]) = [  3.27623E+16 0.00045  2.58588E+16 0.00047  6.90343E+15 0.00133 ];
TOT_ABSRATE               (idx, [1:   6]) = [  4.63339E+16 0.00032  3.94305E+16 0.00031  6.90343E+15 0.00133 ];
TOT_SRCRATE               (idx, [1:   6]) = [  4.66598E+16 0.00054  4.66598E+16 0.00054  0.00000E+00 0.0E+00 ];
TOT_FLUX                  (idx, [1:   6]) = [  2.15908E+18 0.00052  5.74040E+17 0.00049  1.58504E+18 0.00059 ];
TOT_PHOTON_PRODRATE       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
TOT_LEAKRATE              (idx, [1:   2]) = [  4.13494E+14 0.00509 ];
ALBEDO_LEAKRATE           (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_LOSSRATE              (idx, [1:   2]) = [  4.67474E+16 0.00033 ];
TOT_CUTRATE               (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
TOT_RR                    (idx, [1:   2]) = [  1.60122E+18 0.00070 ];
INI_FMASS                 (idx, 1)        =  1.12514E+01 ;
TOT_FMASS                 (idx, 1)        =  1.07883E+01 ;
INI_BURN_FMASS            (idx, 1)        =  1.12514E+01 ;
TOT_BURN_FMASS            (idx, 1)        =  1.07883E+01 ;

% Six-factor formula:

SIX_FF_ETA                (idx, [1:   2]) = [  1.38785E+00 0.00066 ];
SIX_FF_F                  (idx, [1:   2]) = [  7.80466E-01 0.00037 ];
SIX_FF_P                  (idx, [1:   2]) = [  6.06183E-01 0.00038 ];
SIX_FF_EPSILON            (idx, [1:   2]) = [  1.24012E+00 0.00042 ];
SIX_FF_LF                 (idx, [1:   2]) = [  9.94218E-01 3.5E-05 ];
SIX_FF_LT                 (idx, [1:   2]) = [  9.96903E-01 2.6E-05 ];
SIX_FF_KINF               (idx, [1:   2]) = [  8.14185E-01 0.00071 ];
SIX_FF_KEFF               (idx, [1:   2]) = [  8.06971E-01 0.00071 ];

% Fission neutron and energy production:

NUBAR                     (idx, [1:   2]) = [  2.77463E+00 2.4E-05 ];
FISSE                     (idx, [1:   2]) = [  2.06978E+02 4.3E-06 ];

% Criticality eigenvalues:

ANA_KEFF                  (idx, [1:   6]) = [  8.07184E-01 0.00072  8.03198E-01 0.00072  3.77350E-03 0.01226 ];
IMP_KEFF                  (idx, [1:   2]) = [  8.06902E-01 0.00033 ];
COL_KEFF                  (idx, [1:   2]) = [  8.07155E-01 0.00054 ];
ABS_KEFF                  (idx, [1:   2]) = [  8.06902E-01 0.00033 ];
ABS_KINF                  (idx, [1:   2]) = [  8.14113E-01 0.00032 ];
GEOM_ALBEDO               (idx, [1:   6]) = [  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00  1.00000E+00 0.0E+00 ];

% ALF (Average lethargy of neutrons causing fission):
% Based on E0 = 2.000000E+01 MeV

ANA_ALF                   (idx, [1:   2]) = [  1.68404E+01 0.00027 ];
IMP_ALF                   (idx, [1:   2]) = [  1.68407E+01 0.00010 ];

% EALF (Energy corresponding to average lethargy of neutrons causing fission):

ANA_EALF                  (idx, [1:   2]) = [  9.76503E-07 0.00467 ];
IMP_EALF                  (idx, [1:   2]) = [  9.71760E-07 0.00176 ];

% AFGE (Average energy of neutrons causing fission):

ANA_AFGE                  (idx, [1:   2]) = [  3.19396E-01 0.00286 ];
IMP_AFGE                  (idx, [1:   2]) = [  3.19500E-01 0.00116 ];

% Forward-weighted delayed neutron parameters:

FWD_ANA_BETA_ZERO         (idx, [1:  14]) = [  6.06026E-03 0.00656  1.33105E-04 0.04352  1.15995E-03 0.01520  9.40204E-04 0.01630  2.50587E-03 0.01017  1.03697E-03 0.01559  2.84155E-04 0.02921 ];
FWD_ANA_LAMBDA            (idx, [1:  14]) = [  7.38362E-01 0.01514  8.16105E-03 0.03317  3.04782E-02 0.00042  1.11582E-01 0.00065  3.23191E-01 0.00040  1.23489E+00 0.00258  7.09868E+00 0.01936 ];

% Beta-eff using Meulekamp's method:

ADJ_MEULEKAMP_BETA_EFF    (idx, [1:  14]) = [  4.73605E-03 0.00992  1.13922E-04 0.06514  8.76044E-04 0.02353  7.19512E-04 0.02482  1.98039E-03 0.01631  8.17233E-04 0.02391  2.28958E-04 0.04749 ];
ADJ_MEULEKAMP_LAMBDA      (idx, [1:  14]) = [  7.63368E-01 0.02445  1.26262E-02 0.00149  3.04725E-02 0.00061  1.11449E-01 0.00092  3.23244E-01 0.00060  1.23635E+00 0.00370  7.90080E+00 0.01466 ];

% Adjoint weighted time constants using Nauchi's method:

ADJ_NAUCHI_GEN_TIME       (idx, [1:   6]) = [  3.03642E-05 0.00162  3.03545E-05 0.00163  3.28177E-05 0.02067 ];
ADJ_NAUCHI_LIFETIME       (idx, [1:   6]) = [  2.45031E-05 0.00146  2.44953E-05 0.00146  2.64659E-05 0.02055 ];
ADJ_NAUCHI_BETA_EFF       (idx, [1:  14]) = [  4.67494E-03 0.01254  1.00590E-04 0.08507  8.88441E-04 0.02844  7.21039E-04 0.02936  1.94056E-03 0.01924  7.98878E-04 0.03200  2.25435E-04 0.05297 ];
ADJ_NAUCHI_LAMBDA         (idx, [1:  14]) = [  7.49115E-01 0.02890  1.26298E-02 0.00251  3.04640E-02 0.00081  1.11463E-01 0.00129  3.23061E-01 0.00087  1.23250E+00 0.00553  7.85220E+00 0.02160 ];

% Adjoint weighted time constants using IFP:

ADJ_IFP_GEN_TIME          (idx, [1:   6]) = [  2.93326E-05 0.00400  2.93255E-05 0.00401  2.58433E-05 0.04660 ];
ADJ_IFP_LIFETIME          (idx, [1:   6]) = [  2.36709E-05 0.00394  2.36651E-05 0.00394  2.08525E-05 0.04658 ];
ADJ_IFP_IMP_BETA_EFF      (idx, [1:  14]) = [  4.53760E-03 0.04192  9.49731E-05 0.26322  8.62124E-04 0.09523  6.53233E-04 0.12779  2.00133E-03 0.06434  6.87586E-04 0.10735  2.38352E-04 0.16177 ];
ADJ_IFP_IMP_LAMBDA        (idx, [1:  14]) = [  8.17871E-01 0.09611  1.27191E-02 0.00740  3.04815E-02 0.00195  1.12060E-01 0.00318  3.23057E-01 0.00223  1.24289E+00 0.01256  8.06795E+00 0.04950 ];
ADJ_IFP_ANA_BETA_EFF      (idx, [1:  14]) = [  4.50751E-03 0.04133  9.89567E-05 0.26221  8.43964E-04 0.09109  6.51878E-04 0.12590  1.98332E-03 0.06215  7.02121E-04 0.10516  2.27276E-04 0.16570 ];
ADJ_IFP_ANA_LAMBDA        (idx, [1:  14]) = [  8.14498E-01 0.09636  1.27191E-02 0.00740  3.04865E-02 0.00195  1.12079E-01 0.00317  3.23072E-01 0.00222  1.24197E+00 0.01255  8.06795E+00 0.04950 ];
ADJ_IFP_ROSSI_ALPHA       (idx, [1:   2]) = [ -1.55676E+02 0.04195 ];

% Adjoint weighted time constants using perturbation technique:

ADJ_PERT_GEN_TIME         (idx, [1:   2]) = [  2.99204E-05 0.00109 ];
ADJ_PERT_LIFETIME         (idx, [1:   2]) = [  2.41449E-05 0.00080 ];
ADJ_PERT_BETA_EFF         (idx, [1:   2]) = [  4.66206E-03 0.00842 ];
ADJ_PERT_ROSSI_ALPHA      (idx, [1:   2]) = [ -1.55877E+02 0.00841 ];

% Inverse neutron speed :

ANA_INV_SPD               (idx, [1:   2]) = [  5.50419E-07 0.00086 ];

% Analog slowing-down and thermal neutron lifetime (total/prompt/delayed):

ANA_SLOW_TIME             (idx, [1:   6]) = [  2.60010E-06 0.00044  2.60008E-06 0.00044  2.60533E-06 0.00618 ];
ANA_THERM_TIME            (idx, [1:   6]) = [  3.88347E-05 0.00101  3.88527E-05 0.00101  3.52137E-05 0.01226 ];
ANA_THERM_FRAC            (idx, [1:   6]) = [  6.04416E-01 0.00038  6.05071E-01 0.00038  5.08935E-01 0.01079 ];
ANA_DELAYED_EMTIME        (idx, [1:   2]) = [  1.07956E+01 0.01443 ];
ANA_MEAN_NCOL             (idx, [1:   4]) = [  3.42655E+01 0.00053  3.27676E+01 0.00061 ];

% Group constant generation:

GC_UNIVERSE_NAME          (idx, [1:  1])  = '0' ;

% Micro- and macro-group structures:

MICRO_NG                  (idx, 1)        = 70 ;
MICRO_E                   (idx, [1:  71]) = [  1.00000E-11  5.00000E-09  1.00000E-08  1.50000E-08  2.00000E-08  2.50000E-08  3.00000E-08  3.50000E-08  4.20000E-08  5.00000E-08  5.80000E-08  6.70000E-08  8.00000E-08  1.00000E-07  1.40000E-07  1.80000E-07  2.20000E-07  2.50000E-07  2.80000E-07  3.00000E-07  3.20000E-07  3.50000E-07  4.00000E-07  5.00000E-07  6.25000E-07  7.80000E-07  8.50000E-07  9.10000E-07  9.50000E-07  9.72000E-07  9.96000E-07  1.02000E-06  1.04500E-06  1.07100E-06  1.09700E-06  1.12300E-06  1.15000E-06  1.30000E-06  1.50000E-06  1.85500E-06  2.10000E-06  2.60000E-06  3.30000E-06  4.00000E-06  9.87700E-06  1.59680E-05  2.77000E-05  4.80520E-05  7.55014E-05  1.48728E-04  3.67262E-04  9.06898E-04  1.42510E-03  2.23945E-03  3.51910E-03  5.50000E-03  9.11800E-03  1.50300E-02  2.47800E-02  4.08500E-02  6.74300E-02  1.11000E-01  1.83000E-01  3.02500E-01  5.00000E-01  8.21000E-01  1.35300E+00  2.23100E+00  3.67900E+00  6.06550E+00  2.00000E+01 ];

MACRO_NG                  (idx, 1)        = 2 ;
MACRO_E                   (idx, [1:   3]) = [  1.00000E+37  6.25000E-07  0.00000E+00 ];

% Micro-group spectrum:

INF_MICRO_FLX             (idx, [1: 140]) = [  8.16022E+04 0.00389  3.06486E+05 0.00156  6.11176E+05 0.00089  6.50484E+05 0.00094  5.98422E+05 0.00055  6.41761E+05 0.00081  4.35352E+05 0.00084  3.85830E+05 0.00064  2.95460E+05 0.00074  2.41370E+05 0.00093  2.08047E+05 0.00065  1.87504E+05 0.00065  1.73240E+05 0.00100  1.64930E+05 0.00081  1.60731E+05 0.00075  1.38571E+05 0.00082  1.37310E+05 0.00102  1.35950E+05 0.00101  1.33497E+05 0.00085  2.60090E+05 0.00115  2.51769E+05 0.00067  1.81046E+05 0.00064  1.17692E+05 0.00106  1.34763E+05 0.00097  1.28883E+05 0.00112  1.13828E+05 0.00103  1.82400E+05 0.00075  4.24676E+04 0.00174  5.15186E+04 0.00127  4.72623E+04 0.00144  2.78822E+04 0.00204  4.79409E+04 0.00182  3.08633E+04 0.00192  2.48538E+04 0.00240  4.10252E+03 0.00387  3.45043E+03 0.00396  3.01689E+03 0.00444  2.90412E+03 0.00327  2.92892E+03 0.00483  3.22721E+03 0.00405  3.90477E+03 0.00363  4.07582E+03 0.00344  8.23548E+03 0.00293  1.37507E+04 0.00290  1.80896E+04 0.00203  4.85091E+04 0.00173  5.09360E+04 0.00139  5.44963E+04 0.00129  3.52916E+04 0.00162  2.46807E+04 0.00112  1.81524E+04 0.00191  2.14092E+04 0.00159  4.19644E+04 0.00187  5.97295E+04 0.00155  1.20013E+05 0.00168  1.91190E+05 0.00178  2.92075E+05 0.00186  1.89134E+05 0.00213  1.36180E+05 0.00222  9.82801E+04 0.00211  8.87923E+04 0.00194  8.76351E+04 0.00213  7.35082E+04 0.00227  4.97954E+04 0.00215  4.61297E+04 0.00233  4.10171E+04 0.00222  3.47509E+04 0.00225  2.74664E+04 0.00244  1.84266E+04 0.00244  6.53963E+03 0.00315 ];

% Integral parameters:

INF_KINF                  (idx, [1:   2]) = [  8.14371E-01 0.00060 ];

% Flux spectra in infinite geometry:

INF_FLX                   (idx, [1:   4]) = [  1.73742E+18 0.00053  4.21712E+17 0.00190 ];
INF_FISS_FLX              (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

INF_TOT                   (idx, [1:   4]) = [  5.40102E-01 0.00014  1.57182E+00 0.00046 ];
INF_CAPT                  (idx, [1:   4]) = [  9.01365E-03 0.00066  4.05594E-02 0.00114 ];
INF_ABS                   (idx, [1:   4]) = [  1.05271E-02 0.00057  6.65129E-02 0.00142 ];
INF_FISS                  (idx, [1:   4]) = [  1.51347E-03 0.00048  2.59535E-02 0.00187 ];
INF_NSF                   (idx, [1:   4]) = [  4.19411E-03 0.00051  7.20329E-02 0.00192 ];
INF_NUBAR                 (idx, [1:   4]) = [  2.77118E+00 7.2E-05  2.77546E+00 5.4E-05 ];
INF_KAPPA                 (idx, [1:   4]) = [  2.06681E+02 7.8E-06  2.07049E+02 1.1E-05 ];
INF_INVV                  (idx, [1:   4]) = [  5.41150E-08 0.00036  2.59534E-06 0.00020 ];

% Total scattering cross sections:

INF_SCATT0                (idx, [1:   4]) = [  5.29575E-01 0.00014  1.50534E+00 0.00054 ];
INF_SCATT1                (idx, [1:   4]) = [  2.43774E-01 0.00026  3.98600E-01 0.00055 ];
INF_SCATT2                (idx, [1:   4]) = [  9.61822E-02 0.00030  9.40345E-02 0.00116 ];
INF_SCATT3                (idx, [1:   4]) = [  7.21771E-03 0.00261  2.82504E-02 0.00239 ];
INF_SCATT4                (idx, [1:   4]) = [ -1.03628E-02 0.00156 -8.89085E-03 0.00634 ];
INF_SCATT5                (idx, [1:   4]) = [  1.80452E-04 0.11771  6.70918E-03 0.00855 ];
INF_SCATT6                (idx, [1:   4]) = [  5.14830E-03 0.00356 -1.72905E-02 0.00330 ];
INF_SCATT7                (idx, [1:   4]) = [  7.75891E-04 0.02208  5.44515E-04 0.08954 ];

% Total scattering production cross sections:

INF_SCATTP0               (idx, [1:   4]) = [  5.29619E-01 0.00014  1.50534E+00 0.00054 ];
INF_SCATTP1               (idx, [1:   4]) = [  2.43774E-01 0.00026  3.98600E-01 0.00055 ];
INF_SCATTP2               (idx, [1:   4]) = [  9.61822E-02 0.00030  9.40345E-02 0.00116 ];
INF_SCATTP3               (idx, [1:   4]) = [  7.21733E-03 0.00261  2.82504E-02 0.00239 ];
INF_SCATTP4               (idx, [1:   4]) = [ -1.03629E-02 0.00156 -8.89085E-03 0.00634 ];
INF_SCATTP5               (idx, [1:   4]) = [  1.80455E-04 0.11794  6.70918E-03 0.00855 ];
INF_SCATTP6               (idx, [1:   4]) = [  5.14792E-03 0.00356 -1.72905E-02 0.00330 ];
INF_SCATTP7               (idx, [1:   4]) = [  7.75952E-04 0.02213  5.44515E-04 0.08954 ];

% Diffusion parameters:

INF_TRANSPXS              (idx, [1:   4]) = [  2.12741E-01 0.00039  1.02332E+00 0.00040 ];
INF_DIFFCOEF              (idx, [1:   4]) = [  1.56686E+00 0.00039  3.25737E-01 0.00040 ];

% Reduced absoption and removal:

INF_RABSXS                (idx, [1:   4]) = [  1.04830E-02 0.00055  6.65129E-02 0.00142 ];
INF_REMXS                 (idx, [1:   4]) = [  2.70252E-02 0.00025  6.76358E-02 0.00150 ];

% Poison cross sections:

INF_I135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_YIELD          (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_I135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM147_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM148M_MICRO_ABS      (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_PM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_XE135_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_SM149_MACRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

INF_CHIT                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHIP                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
INF_CHID                  (idx, [1:   4]) = [  1.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

INF_S0                    (idx, [1:   8]) = [  5.13077E-01 0.00013  1.64984E-02 0.00054  1.15551E-03 0.00442  1.50419E+00 0.00054 ];
INF_S1                    (idx, [1:   8]) = [  2.39021E-01 0.00025  4.75249E-03 0.00101  4.95175E-04 0.00641  3.98104E-01 0.00055 ];
INF_S2                    (idx, [1:   8]) = [  9.77076E-02 0.00031 -1.52534E-03 0.00193  2.69682E-04 0.01048  9.37648E-02 0.00116 ];
INF_S3                    (idx, [1:   8]) = [  8.92951E-03 0.00207 -1.71180E-03 0.00189  9.92953E-05 0.02217  2.81511E-02 0.00239 ];
INF_S4                    (idx, [1:   8]) = [ -9.84471E-03 0.00159 -5.18053E-04 0.00491 -1.33754E-07 1.00000 -8.89072E-03 0.00633 ];
INF_S5                    (idx, [1:   8]) = [  1.29387E-04 0.16200  5.10646E-05 0.03867 -3.95686E-05 0.03658  6.74875E-03 0.00845 ];
INF_S6                    (idx, [1:   8]) = [  5.27581E-03 0.00339 -1.27508E-04 0.01547 -4.98664E-05 0.03186 -1.72407E-02 0.00333 ];
INF_S7                    (idx, [1:   8]) = [  9.42433E-04 0.01789 -1.66541E-04 0.01453 -4.59691E-05 0.03004  5.90484E-04 0.08248 ];

% Scattering production matrixes:

INF_SP0                   (idx, [1:   8]) = [  5.13121E-01 0.00013  1.64984E-02 0.00054  1.15551E-03 0.00442  1.50419E+00 0.00054 ];
INF_SP1                   (idx, [1:   8]) = [  2.39021E-01 0.00025  4.75249E-03 0.00101  4.95175E-04 0.00641  3.98104E-01 0.00055 ];
INF_SP2                   (idx, [1:   8]) = [  9.77075E-02 0.00031 -1.52534E-03 0.00193  2.69682E-04 0.01048  9.37648E-02 0.00116 ];
INF_SP3                   (idx, [1:   8]) = [  8.92914E-03 0.00207 -1.71180E-03 0.00189  9.92953E-05 0.02217  2.81511E-02 0.00239 ];
INF_SP4                   (idx, [1:   8]) = [ -9.84481E-03 0.00160 -5.18053E-04 0.00491 -1.33754E-07 1.00000 -8.89072E-03 0.00633 ];
INF_SP5                   (idx, [1:   8]) = [  1.29391E-04 0.16234  5.10646E-05 0.03867 -3.95686E-05 0.03658  6.74875E-03 0.00845 ];
INF_SP6                   (idx, [1:   8]) = [  5.27542E-03 0.00339 -1.27508E-04 0.01547 -4.98664E-05 0.03186 -1.72407E-02 0.00333 ];
INF_SP7                   (idx, [1:   8]) = [  9.42493E-04 0.01793 -1.66541E-04 0.01453 -4.59691E-05 0.03004  5.90484E-04 0.08248 ];

% Micro-group spectrum:

B1_MICRO_FLX              (idx, [1: 140]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Integral parameters:

B1_KINF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_KEFF                   (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_B2                     (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];
B1_ERR                    (idx, [1:   2]) = [  0.00000E+00 0.0E+00 ];

% Critical spectra in infinite geometry:

B1_FLX                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS_FLX               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reaction cross sections:

B1_TOT                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CAPT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_ABS                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_FISS                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NSF                    (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_NUBAR                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_KAPPA                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_INVV                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering cross sections:

B1_SCATT0                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT1                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT2                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT3                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT4                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT5                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT6                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATT7                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Total scattering production cross sections:

B1_SCATTP0                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP1                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP2                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP3                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP4                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP5                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP6                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SCATTP7                (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Diffusion parameters:

B1_TRANSPXS               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_DIFFCOEF               (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Reduced absoption and removal:

B1_RABSXS                 (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_REMXS                  (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Poison cross sections:

B1_I135_YIELD             (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_YIELD           (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_YIELD            (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_I135_MICRO_ABS         (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM147_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM148M_MICRO_ABS       (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_PM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MICRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_XE135_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SM149_MACRO_ABS        (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Fission spectra:

B1_CHIT                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHIP                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_CHID                   (idx, [1:   4]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering matrixes:

B1_S0                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S1                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S2                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S3                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S4                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S5                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S6                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_S7                     (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Scattering production matrixes:

B1_SP0                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP1                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP2                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP3                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP4                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP5                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP6                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];
B1_SP7                    (idx, [1:   8]) = [  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00  0.00000E+00 0.0E+00 ];

% Additional diffusion parameters:

CMM_TRANSPXS              (idx, [1:   4]) = [  2.31885E-01 0.00078  1.22708E+00 0.00730 ];
CMM_TRANSPXS_X            (idx, [1:   4]) = [  2.33398E-01 0.00105  1.34347E+00 0.00998 ];
CMM_TRANSPXS_Y            (idx, [1:   4]) = [  2.33619E-01 0.00105  1.34134E+00 0.00931 ];
CMM_TRANSPXS_Z            (idx, [1:   4]) = [  2.28711E-01 0.00098  1.04869E+00 0.00740 ];
CMM_DIFFCOEF              (idx, [1:   4]) = [  1.43752E+00 0.00078  2.71995E-01 0.00731 ];
CMM_DIFFCOEF_X            (idx, [1:   4]) = [  1.42821E+00 0.00105  2.48711E-01 0.01003 ];
CMM_DIFFCOEF_Y            (idx, [1:   4]) = [  1.42686E+00 0.00105  2.49004E-01 0.00895 ];
CMM_DIFFCOEF_Z            (idx, [1:   4]) = [  1.45747E+00 0.00098  3.18269E-01 0.00733 ];

% Delayed neutron parameters (Meulekamp method):

BETA_EFF                  (idx, [1:  14]) = [  4.73605E-03 0.00992  1.13922E-04 0.06514  8.76044E-04 0.02353  7.19512E-04 0.02482  1.98039E-03 0.01631  8.17233E-04 0.02391  2.28958E-04 0.04749 ];
LAMBDA                    (idx, [1:  14]) = [  7.63368E-01 0.02445  1.26262E-02 0.00149  3.04725E-02 0.00061  1.11449E-01 0.00092  3.23244E-01 0.00060  1.23635E+00 0.00370  7.90080E+00 0.01466 ];

