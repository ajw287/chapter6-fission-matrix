# README #

Welcome to the scripts used in Chapter 6.  This code generates the Figures in Chapter 6 of my thesis.

### What is this repository for? ###

* Code for Fission matrix surrogate model optimisation
* Version 0.1

### How do I get set up? ###
* First run 'check_setup.sh'
* Install any missing dependencies.
* Dependencies:
Keras, tensorflow, matplotlib, pickle, scipy, hdf5py
* How to run:
Scripts for each figure are supplied, run them from the command line.  Figures are either generated in a window or will be saved to the working directory.
Please look further into the code by examining the script, then reading the relevant code in the /code directory.

* Deployment
Keep the directory structure found here.

### Who do I talk to? ###

* contact ajw287@cam.ac.uk for further information

Code in this project is subject to:

#                                                 Copyright 2019 Andrew Whyte

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
 (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
