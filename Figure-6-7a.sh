#!/usr/bin/env bash
cd code/figure-6-7a
echo ""
echo "***************************************************"
echo "  WARNING: This code could take a number of weeks!  "
echo "***************************************************"
echo ""
echo "PLease ensure that you have set up ssh keys to a "
echo "suitable machine called 'hpc' in ~/.ssh/sshconfig."
echo "The 'hpc' machine will require a directory at:"
echo " ~/rds/hpc-work/ex6_burn_fmtx/"
echo " and a working Serpent implementation that can be run at: "
echo "~/rds/hpc-work/software/sss2"
echo ""
read -n 1 -s -r -p "       Press any key to continue                                                             "
echo ""
echo ""
time python3 pygmo_micro.py ./figure-3453425
cp figure-3453425-graph.svg ../../figure-6-7a.svg
cd ../..
