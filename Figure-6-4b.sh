#!/usr/bin/env bash
cd ./code/figure-6-5
time python3 pygmo_micro_array.py figure-6-4b ../../data/6-5/serp-3453422/
cp figure-6-4b-basic* ../../
echo "Output files generated in execution directory as SVG and PNG: 'figure-6-4b-basic'"
